PGDMP                      	    {         
   datindrive    15.4    15.4 �    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    16398 
   datindrive    DATABASE     �   CREATE DATABASE datindrive WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'English_Indonesia.1252';
    DROP DATABASE datindrive;
                postgres    false                        2615    16399    v1    SCHEMA        CREATE SCHEMA v1;
    DROP SCHEMA v1;
                postgres    false            �            1259    16400    failed_jobs    TABLE     "  CREATE TABLE v1.failed_jobs (
    id bigint NOT NULL,
    uuid character varying(255) NOT NULL,
    connection text NOT NULL,
    queue text NOT NULL,
    payload text NOT NULL,
    exception text NOT NULL,
    failed_at timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);
    DROP TABLE v1.failed_jobs;
       v1         heap    postgres    false    6            �            1259    16406    failed_jobs_id_seq    SEQUENCE     w   CREATE SEQUENCE v1.failed_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE v1.failed_jobs_id_seq;
       v1          postgres    false    6    215            �           0    0    failed_jobs_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE v1.failed_jobs_id_seq OWNED BY v1.failed_jobs.id;
          v1          postgres    false    216            �            1259    16407 
   migrations    TABLE     �   CREATE TABLE v1.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);
    DROP TABLE v1.migrations;
       v1         heap    postgres    false    6            �            1259    16410    migrations_id_seq    SEQUENCE     �   CREATE SEQUENCE v1.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE v1.migrations_id_seq;
       v1          postgres    false    217    6            �           0    0    migrations_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE v1.migrations_id_seq OWNED BY v1.migrations.id;
          v1          postgres    false    218            �            1259    16411    mst_collection    TABLE     �  CREATE TABLE v1.mst_collection (
    id bigint NOT NULL,
    nama_collection text NOT NULL,
    judul text,
    tipe character varying(50),
    default_chart character varying(50),
    referensi_data text,
    catatan text,
    route_name text,
    table_name text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    ref_organisasi_id integer,
    organisasi text,
    satuan character varying(100),
    definisi text,
    rumus_perhitungan text,
    cara_memperoleh_data text,
    urusan text,
    is_multidimensi boolean DEFAULT false NOT NULL
);
    DROP TABLE v1.mst_collection;
       v1         heap    postgres    false    6            �            1259    16416    mst_collection_id_seq    SEQUENCE     z   CREATE SEQUENCE v1.mst_collection_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE v1.mst_collection_id_seq;
       v1          postgres    false    6    219            �           0    0    mst_collection_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE v1.mst_collection_id_seq OWNED BY v1.mst_collection.id;
          v1          postgres    false    220            �            1259    16417    mst_dashboard    TABLE     �  CREATE TABLE v1.mst_dashboard (
    id bigint NOT NULL,
    nama_dashboard text NOT NULL,
    frame text NOT NULL,
    link text NOT NULL,
    catatan text,
    is_hidden boolean DEFAULT false NOT NULL,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.mst_dashboard;
       v1         heap    postgres    false    6            �            1259    16423    mst_dashboard_id_seq    SEQUENCE     y   CREATE SEQUENCE v1.mst_dashboard_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE v1.mst_dashboard_id_seq;
       v1          postgres    false    221    6            �           0    0    mst_dashboard_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE v1.mst_dashboard_id_seq OWNED BY v1.mst_dashboard.id;
          v1          postgres    false    222            �            1259    16424    mst_jenis_dok    TABLE     [  CREATE TABLE v1.mst_jenis_dok (
    id bigint NOT NULL,
    jenis_dokumen character varying(200) NOT NULL,
    ref_grup_dok_id integer,
    keterangan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.mst_jenis_dok;
       v1         heap    postgres    false    6            �            1259    16429    mst_jenis_dok_id_seq    SEQUENCE     y   CREATE SEQUENCE v1.mst_jenis_dok_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE v1.mst_jenis_dok_id_seq;
       v1          postgres    false    6    223            �           0    0    mst_jenis_dok_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE v1.mst_jenis_dok_id_seq OWNED BY v1.mst_jenis_dok.id;
          v1          postgres    false    224            �            1259    16430    password_reset_tokens    TABLE     �   CREATE TABLE v1.password_reset_tokens (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);
 %   DROP TABLE v1.password_reset_tokens;
       v1         heap    postgres    false    6            �            1259    16435    personal_access_tokens    TABLE     �  CREATE TABLE v1.personal_access_tokens (
    id bigint NOT NULL,
    tokenable_type character varying(255) NOT NULL,
    tokenable_id bigint NOT NULL,
    name character varying(255) NOT NULL,
    token character varying(64) NOT NULL,
    abilities text,
    last_used_at timestamp(0) without time zone,
    expires_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
 &   DROP TABLE v1.personal_access_tokens;
       v1         heap    postgres    false    6            �            1259    16440    personal_access_tokens_id_seq    SEQUENCE     �   CREATE SEQUENCE v1.personal_access_tokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE v1.personal_access_tokens_id_seq;
       v1          postgres    false    226    6            �           0    0    personal_access_tokens_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE v1.personal_access_tokens_id_seq OWNED BY v1.personal_access_tokens.id;
          v1          postgres    false    227            �            1259    16441    ref_data_tag    TABLE     &  CREATE TABLE v1.ref_data_tag (
    id bigint NOT NULL,
    data_tag text NOT NULL,
    keterangan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.ref_data_tag;
       v1         heap    postgres    false    6            �            1259    16446    ref_data_tag_id_seq    SEQUENCE     x   CREATE SEQUENCE v1.ref_data_tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE v1.ref_data_tag_id_seq;
       v1          postgres    false    6    228            �           0    0    ref_data_tag_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE v1.ref_data_tag_id_seq OWNED BY v1.ref_data_tag.id;
          v1          postgres    false    229            �            1259    16447    ref_grup_dok    TABLE     <  CREATE TABLE v1.ref_grup_dok (
    id bigint NOT NULL,
    grup_dokumen character varying(200) NOT NULL,
    keterangan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.ref_grup_dok;
       v1         heap    postgres    false    6            �            1259    16452    ref_grup_dok_id_seq    SEQUENCE     x   CREATE SEQUENCE v1.ref_grup_dok_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE v1.ref_grup_dok_id_seq;
       v1          postgres    false    230    6            �           0    0    ref_grup_dok_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE v1.ref_grup_dok_id_seq OWNED BY v1.ref_grup_dok.id;
          v1          postgres    false    231            �            1259    24617    ref_kode_wilayah    TABLE     �  CREATE TABLE v1.ref_kode_wilayah (
    id bigint NOT NULL,
    kemendagri_provinsi_kode character varying(20),
    kemendagri_kota_kode character varying(50),
    kemendagri_provinsi_nama character varying(50),
    kemendagri_kota_nama text NOT NULL,
    bps_provinsi_kode character varying(50),
    bps_kota_kode character varying(50),
    bps_provinsi_nama character varying(50),
    bps_kota_nama text NOT NULL,
    latitude text,
    longitude text,
    kode_pos character varying(200),
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
     DROP TABLE v1.ref_kode_wilayah;
       v1         heap    postgres    false    6            �            1259    24616    ref_kode_wilayah_id_seq    SEQUENCE     |   CREATE SEQUENCE v1.ref_kode_wilayah_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE v1.ref_kode_wilayah_id_seq;
       v1          postgres    false    253    6            �           0    0    ref_kode_wilayah_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE v1.ref_kode_wilayah_id_seq OWNED BY v1.ref_kode_wilayah.id;
          v1          postgres    false    252            �            1259    16453    ref_organisasi    TABLE     P  CREATE TABLE v1.ref_organisasi (
    id bigint NOT NULL,
    organisasi character varying(200) NOT NULL,
    singkatan text,
    keterangan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.ref_organisasi;
       v1         heap    postgres    false    6            �            1259    16458    ref_organisasi_id_seq    SEQUENCE     z   CREATE SEQUENCE v1.ref_organisasi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE v1.ref_organisasi_id_seq;
       v1          postgres    false    232    6            �           0    0    ref_organisasi_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE v1.ref_organisasi_id_seq OWNED BY v1.ref_organisasi.id;
          v1          postgres    false    233            �            1259    16459 	   ref_topik    TABLE     2  CREATE TABLE v1.ref_topik (
    id bigint NOT NULL,
    topik character varying(200) NOT NULL,
    keterangan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.ref_topik;
       v1         heap    postgres    false    6            �            1259    16464    ref_topik_id_seq    SEQUENCE     u   CREATE SEQUENCE v1.ref_topik_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE v1.ref_topik_id_seq;
       v1          postgres    false    234    6            �           0    0    ref_topik_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE v1.ref_topik_id_seq OWNED BY v1.ref_topik.id;
          v1          postgres    false    235            �            1259    24601    trx_collection    TABLE       CREATE TABLE v1.trx_collection (
    id bigint NOT NULL,
    mst_collection_id integer,
    tahun integer NOT NULL,
    target text,
    realisasi text,
    sumber text,
    catatan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    kemendagri_kota_kode character varying(50) DEFAULT '32.71'::character varying NOT NULL,
    kemendagri_kota_nama text DEFAULT 'KOTA BOGOR'::text NOT NULL
);
    DROP TABLE v1.trx_collection;
       v1         heap    postgres    false    6            �            1259    24600    trx_collection_id_seq    SEQUENCE     z   CREATE SEQUENCE v1.trx_collection_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE v1.trx_collection_id_seq;
       v1          postgres    false    251    6            �           0    0    trx_collection_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE v1.trx_collection_id_seq OWNED BY v1.trx_collection.id;
          v1          postgres    false    250            �            1259    16465    trx_data    TABLE     4  CREATE TABLE v1.trx_data (
    id bigint NOT NULL,
    mst_jenis_dok_id integer,
    sumber_data text NOT NULL,
    jenis_data character varying(100) NOT NULL,
    nama_data text NOT NULL,
    deskripsi_data text NOT NULL,
    format_data character varying(100) NOT NULL,
    media_type character varying(100),
    size_data character varying(100),
    ekstensi_data character varying(100),
    nama_file text,
    url_file_upload text,
    url_upload text,
    is_shared boolean DEFAULT false NOT NULL,
    webbappeda_visible boolean DEFAULT false NOT NULL,
    satudata_visible boolean DEFAULT false NOT NULL,
    catatan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.trx_data;
       v1         heap    postgres    false    6            �            1259    16473    trx_data_id_seq    SEQUENCE     t   CREATE SEQUENCE v1.trx_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE v1.trx_data_id_seq;
       v1          postgres    false    6    236            �           0    0    trx_data_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE v1.trx_data_id_seq OWNED BY v1.trx_data.id;
          v1          postgres    false    237            �            1259    16474    trx_data_organisasi    TABLE     R  CREATE TABLE v1.trx_data_organisasi (
    id bigint NOT NULL,
    trx_data_id integer,
    ref_organisasi_id integer,
    organisasi text NOT NULL,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
 #   DROP TABLE v1.trx_data_organisasi;
       v1         heap    postgres    false    6            �            1259    16479    trx_data_organisasi_id_seq    SEQUENCE        CREATE SEQUENCE v1.trx_data_organisasi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE v1.trx_data_organisasi_id_seq;
       v1          postgres    false    6    238            �           0    0    trx_data_organisasi_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE v1.trx_data_organisasi_id_seq OWNED BY v1.trx_data_organisasi.id;
          v1          postgres    false    239            �            1259    16480    trx_data_tag    TABLE     G  CREATE TABLE v1.trx_data_tag (
    id bigint NOT NULL,
    trx_data_id integer,
    ref_data_tag_id integer,
    data_tag text NOT NULL,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.trx_data_tag;
       v1         heap    postgres    false    6            �            1259    16485    trx_data_tag_id_seq    SEQUENCE     x   CREATE SEQUENCE v1.trx_data_tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE v1.trx_data_tag_id_seq;
       v1          postgres    false    6    240            �           0    0    trx_data_tag_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE v1.trx_data_tag_id_seq OWNED BY v1.trx_data_tag.id;
          v1          postgres    false    241            �            1259    16486    trx_data_topik    TABLE     C  CREATE TABLE v1.trx_data_topik (
    id bigint NOT NULL,
    trx_data_id integer,
    ref_topik_id integer,
    topik text NOT NULL,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.trx_data_topik;
       v1         heap    postgres    false    6            �            1259    16491    trx_data_topik_id_seq    SEQUENCE     z   CREATE SEQUENCE v1.trx_data_topik_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE v1.trx_data_topik_id_seq;
       v1          postgres    false    242    6            �           0    0    trx_data_topik_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE v1.trx_data_topik_id_seq OWNED BY v1.trx_data_topik.id;
          v1          postgres    false    243            �            1259    24645    trx_laju_pertumbuhan_pdrb    TABLE     O  CREATE TABLE v1.trx_laju_pertumbuhan_pdrb (
    id bigint NOT NULL,
    mst_collection_id integer,
    tahun integer NOT NULL,
    kategori text NOT NULL,
    urutan character varying(5),
    target text,
    realisasi text,
    sumber text,
    catatan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    kemendagri_kota_kode character varying(50) DEFAULT '32.71'::character varying,
    kemendagri_kota_nama text DEFAULT 'KOTA BOGOR'::text
);
 )   DROP TABLE v1.trx_laju_pertumbuhan_pdrb;
       v1         heap    postgres    false    6            �            1259    24644     trx_laju_pertumbuhan_pdrb_id_seq    SEQUENCE     �   CREATE SEQUENCE v1.trx_laju_pertumbuhan_pdrb_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE v1.trx_laju_pertumbuhan_pdrb_id_seq;
       v1          postgres    false    255    6            �           0    0     trx_laju_pertumbuhan_pdrb_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE v1.trx_laju_pertumbuhan_pdrb_id_seq OWNED BY v1.trx_laju_pertumbuhan_pdrb.id;
          v1          postgres    false    254            �            1259    16492 $   trx_luas_wilayah_kecamatan_kelurahan    TABLE       CREATE TABLE v1.trx_luas_wilayah_kecamatan_kelurahan (
    id bigint NOT NULL,
    mst_collection_id integer NOT NULL,
    tahun integer NOT NULL,
    kecamatan character varying(50) NOT NULL,
    jml_kelurahan integer NOT NULL,
    luas_wilayah double precision NOT NULL,
    satuan character varying(50),
    catatan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
 4   DROP TABLE v1.trx_luas_wilayah_kecamatan_kelurahan;
       v1         heap    postgres    false    6            �            1259    16497 +   trx_luas_wilayah_kecamatan_kelurahan_id_seq    SEQUENCE     �   CREATE SEQUENCE v1.trx_luas_wilayah_kecamatan_kelurahan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 >   DROP SEQUENCE v1.trx_luas_wilayah_kecamatan_kelurahan_id_seq;
       v1          postgres    false    244    6            �           0    0 +   trx_luas_wilayah_kecamatan_kelurahan_id_seq    SEQUENCE OWNED BY     s   ALTER SEQUENCE v1.trx_luas_wilayah_kecamatan_kelurahan_id_seq OWNED BY v1.trx_luas_wilayah_kecamatan_kelurahan.id;
          v1          postgres    false    245            �            1259    16498    trx_riwayat_data    TABLE     U  CREATE TABLE v1.trx_riwayat_data (
    id bigint NOT NULL,
    trx_data_id integer,
    mst_jenis_dok_id integer,
    sumber_data text NOT NULL,
    jenis_data character varying(100) NOT NULL,
    nama_data text NOT NULL,
    deskripsi_data text NOT NULL,
    format_data character varying(100) NOT NULL,
    media_type character varying(100),
    size_data character varying(100),
    ekstensi_data character varying(100),
    nama_file text,
    url_file_upload text,
    url_upload text,
    is_shared boolean DEFAULT false NOT NULL,
    webbappeda_visible boolean DEFAULT false NOT NULL,
    satudata_visible boolean DEFAULT false NOT NULL,
    catatan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
     DROP TABLE v1.trx_riwayat_data;
       v1         heap    postgres    false    6            �            1259    16506    trx_riwayat_data_id_seq    SEQUENCE     |   CREATE SEQUENCE v1.trx_riwayat_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE v1.trx_riwayat_data_id_seq;
       v1          postgres    false    6    246            �           0    0    trx_riwayat_data_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE v1.trx_riwayat_data_id_seq OWNED BY v1.trx_riwayat_data.id;
          v1          postgres    false    247            �            1259    16507    users    TABLE     t  CREATE TABLE v1.users (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    email_verified_at timestamp(0) without time zone,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.users;
       v1         heap    postgres    false    6            �            1259    16512    users_id_seq    SEQUENCE     q   CREATE SEQUENCE v1.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
    DROP SEQUENCE v1.users_id_seq;
       v1          postgres    false    248    6            �           0    0    users_id_seq    SEQUENCE OWNED BY     5   ALTER SEQUENCE v1.users_id_seq OWNED BY v1.users.id;
          v1          postgres    false    249            �           2604    16513    failed_jobs id    DEFAULT     h   ALTER TABLE ONLY v1.failed_jobs ALTER COLUMN id SET DEFAULT nextval('v1.failed_jobs_id_seq'::regclass);
 9   ALTER TABLE v1.failed_jobs ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    216    215            �           2604    16514    migrations id    DEFAULT     f   ALTER TABLE ONLY v1.migrations ALTER COLUMN id SET DEFAULT nextval('v1.migrations_id_seq'::regclass);
 8   ALTER TABLE v1.migrations ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    218    217            �           2604    16515    mst_collection id    DEFAULT     n   ALTER TABLE ONLY v1.mst_collection ALTER COLUMN id SET DEFAULT nextval('v1.mst_collection_id_seq'::regclass);
 <   ALTER TABLE v1.mst_collection ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    220    219            �           2604    16516    mst_dashboard id    DEFAULT     l   ALTER TABLE ONLY v1.mst_dashboard ALTER COLUMN id SET DEFAULT nextval('v1.mst_dashboard_id_seq'::regclass);
 ;   ALTER TABLE v1.mst_dashboard ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    222    221            �           2604    16517    mst_jenis_dok id    DEFAULT     l   ALTER TABLE ONLY v1.mst_jenis_dok ALTER COLUMN id SET DEFAULT nextval('v1.mst_jenis_dok_id_seq'::regclass);
 ;   ALTER TABLE v1.mst_jenis_dok ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    224    223            �           2604    16518    personal_access_tokens id    DEFAULT     ~   ALTER TABLE ONLY v1.personal_access_tokens ALTER COLUMN id SET DEFAULT nextval('v1.personal_access_tokens_id_seq'::regclass);
 D   ALTER TABLE v1.personal_access_tokens ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    227    226            �           2604    16519    ref_data_tag id    DEFAULT     j   ALTER TABLE ONLY v1.ref_data_tag ALTER COLUMN id SET DEFAULT nextval('v1.ref_data_tag_id_seq'::regclass);
 :   ALTER TABLE v1.ref_data_tag ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    229    228            �           2604    16520    ref_grup_dok id    DEFAULT     j   ALTER TABLE ONLY v1.ref_grup_dok ALTER COLUMN id SET DEFAULT nextval('v1.ref_grup_dok_id_seq'::regclass);
 :   ALTER TABLE v1.ref_grup_dok ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    231    230            �           2604    24620    ref_kode_wilayah id    DEFAULT     r   ALTER TABLE ONLY v1.ref_kode_wilayah ALTER COLUMN id SET DEFAULT nextval('v1.ref_kode_wilayah_id_seq'::regclass);
 >   ALTER TABLE v1.ref_kode_wilayah ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    252    253    253            �           2604    16521    ref_organisasi id    DEFAULT     n   ALTER TABLE ONLY v1.ref_organisasi ALTER COLUMN id SET DEFAULT nextval('v1.ref_organisasi_id_seq'::regclass);
 <   ALTER TABLE v1.ref_organisasi ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    233    232            �           2604    16522    ref_topik id    DEFAULT     d   ALTER TABLE ONLY v1.ref_topik ALTER COLUMN id SET DEFAULT nextval('v1.ref_topik_id_seq'::regclass);
 7   ALTER TABLE v1.ref_topik ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    235    234            �           2604    24604    trx_collection id    DEFAULT     n   ALTER TABLE ONLY v1.trx_collection ALTER COLUMN id SET DEFAULT nextval('v1.trx_collection_id_seq'::regclass);
 <   ALTER TABLE v1.trx_collection ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    251    250    251            �           2604    16523    trx_data id    DEFAULT     b   ALTER TABLE ONLY v1.trx_data ALTER COLUMN id SET DEFAULT nextval('v1.trx_data_id_seq'::regclass);
 6   ALTER TABLE v1.trx_data ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    237    236            �           2604    16524    trx_data_organisasi id    DEFAULT     x   ALTER TABLE ONLY v1.trx_data_organisasi ALTER COLUMN id SET DEFAULT nextval('v1.trx_data_organisasi_id_seq'::regclass);
 A   ALTER TABLE v1.trx_data_organisasi ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    239    238            �           2604    16525    trx_data_tag id    DEFAULT     j   ALTER TABLE ONLY v1.trx_data_tag ALTER COLUMN id SET DEFAULT nextval('v1.trx_data_tag_id_seq'::regclass);
 :   ALTER TABLE v1.trx_data_tag ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    241    240            �           2604    16526    trx_data_topik id    DEFAULT     n   ALTER TABLE ONLY v1.trx_data_topik ALTER COLUMN id SET DEFAULT nextval('v1.trx_data_topik_id_seq'::regclass);
 <   ALTER TABLE v1.trx_data_topik ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    243    242            �           2604    24648    trx_laju_pertumbuhan_pdrb id    DEFAULT     �   ALTER TABLE ONLY v1.trx_laju_pertumbuhan_pdrb ALTER COLUMN id SET DEFAULT nextval('v1.trx_laju_pertumbuhan_pdrb_id_seq'::regclass);
 G   ALTER TABLE v1.trx_laju_pertumbuhan_pdrb ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    254    255    255            �           2604    16527 '   trx_luas_wilayah_kecamatan_kelurahan id    DEFAULT     �   ALTER TABLE ONLY v1.trx_luas_wilayah_kecamatan_kelurahan ALTER COLUMN id SET DEFAULT nextval('v1.trx_luas_wilayah_kecamatan_kelurahan_id_seq'::regclass);
 R   ALTER TABLE v1.trx_luas_wilayah_kecamatan_kelurahan ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    245    244            �           2604    16528    trx_riwayat_data id    DEFAULT     r   ALTER TABLE ONLY v1.trx_riwayat_data ALTER COLUMN id SET DEFAULT nextval('v1.trx_riwayat_data_id_seq'::regclass);
 >   ALTER TABLE v1.trx_riwayat_data ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    247    246            �           2604    16529    users id    DEFAULT     \   ALTER TABLE ONLY v1.users ALTER COLUMN id SET DEFAULT nextval('v1.users_id_seq'::regclass);
 3   ALTER TABLE v1.users ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    249    248            �          0    16400    failed_jobs 
   TABLE DATA           ]   COPY v1.failed_jobs (id, uuid, connection, queue, payload, exception, failed_at) FROM stdin;
    v1          postgres    false    215   �       �          0    16407 
   migrations 
   TABLE DATA           6   COPY v1.migrations (id, migration, batch) FROM stdin;
    v1          postgres    false    217   1�       �          0    16411    mst_collection 
   TABLE DATA           )  COPY v1.mst_collection (id, nama_collection, judul, tipe, default_chart, referensi_data, catatan, route_name, table_name, created_by, updated_by, created_at, updated_at, ref_organisasi_id, organisasi, satuan, definisi, rumus_perhitungan, cara_memperoleh_data, urusan, is_multidimensi) FROM stdin;
    v1          postgres    false    219   �       �          0    16417    mst_dashboard 
   TABLE DATA           �   COPY v1.mst_dashboard (id, nama_dashboard, frame, link, catatan, is_hidden, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    221   �3      �          0    16424    mst_jenis_dok 
   TABLE DATA           �   COPY v1.mst_jenis_dok (id, jenis_dokumen, ref_grup_dok_id, keterangan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    223   ^5      �          0    16430    password_reset_tokens 
   TABLE DATA           E   COPY v1.password_reset_tokens (email, token, created_at) FROM stdin;
    v1          postgres    false    225   �5      �          0    16435    personal_access_tokens 
   TABLE DATA           �   COPY v1.personal_access_tokens (id, tokenable_type, tokenable_id, name, token, abilities, last_used_at, expires_at, created_at, updated_at) FROM stdin;
    v1          postgres    false    226   �5      �          0    16441    ref_data_tag 
   TABLE DATA           l   COPY v1.ref_data_tag (id, data_tag, keterangan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    228   �5      �          0    16447    ref_grup_dok 
   TABLE DATA           p   COPY v1.ref_grup_dok (id, grup_dokumen, keterangan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    230   |A      �          0    24617    ref_kode_wilayah 
   TABLE DATA             COPY v1.ref_kode_wilayah (id, kemendagri_provinsi_kode, kemendagri_kota_kode, kemendagri_provinsi_nama, kemendagri_kota_nama, bps_provinsi_kode, bps_kota_kode, bps_provinsi_nama, bps_kota_nama, latitude, longitude, kode_pos, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    253   0B      �          0    16453    ref_organisasi 
   TABLE DATA           {   COPY v1.ref_organisasi (id, organisasi, singkatan, keterangan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    232   �E      �          0    16459 	   ref_topik 
   TABLE DATA           f   COPY v1.ref_topik (id, topik, keterangan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    234   �J      �          0    24601    trx_collection 
   TABLE DATA           �   COPY v1.trx_collection (id, mst_collection_id, tahun, target, realisasi, sumber, catatan, created_by, updated_by, created_at, updated_at, kemendagri_kota_kode, kemendagri_kota_nama) FROM stdin;
    v1          postgres    false    251   |M      �          0    16465    trx_data 
   TABLE DATA           -  COPY v1.trx_data (id, mst_jenis_dok_id, sumber_data, jenis_data, nama_data, deskripsi_data, format_data, media_type, size_data, ekstensi_data, nama_file, url_file_upload, url_upload, is_shared, webbappeda_visible, satudata_visible, catatan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    236   f�      �          0    16474    trx_data_organisasi 
   TABLE DATA           �   COPY v1.trx_data_organisasi (id, trx_data_id, ref_organisasi_id, organisasi, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    238   E9      �          0    16480    trx_data_tag 
   TABLE DATA           ~   COPY v1.trx_data_tag (id, trx_data_id, ref_data_tag_id, data_tag, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    240   �@      �          0    16486    trx_data_topik 
   TABLE DATA           z   COPY v1.trx_data_topik (id, trx_data_id, ref_topik_id, topik, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    242   �Y      �          0    24645    trx_laju_pertumbuhan_pdrb 
   TABLE DATA           �   COPY v1.trx_laju_pertumbuhan_pdrb (id, mst_collection_id, tahun, kategori, urutan, target, realisasi, sumber, catatan, created_by, updated_by, created_at, updated_at, kemendagri_kota_kode, kemendagri_kota_nama) FROM stdin;
    v1          postgres    false    255   �[      �          0    16492 $   trx_luas_wilayah_kecamatan_kelurahan 
   TABLE DATA           �   COPY v1.trx_luas_wilayah_kecamatan_kelurahan (id, mst_collection_id, tahun, kecamatan, jml_kelurahan, luas_wilayah, satuan, catatan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    244   �[      �          0    16498    trx_riwayat_data 
   TABLE DATA           B  COPY v1.trx_riwayat_data (id, trx_data_id, mst_jenis_dok_id, sumber_data, jenis_data, nama_data, deskripsi_data, format_data, media_type, size_data, ekstensi_data, nama_file, url_file_upload, url_upload, is_shared, webbappeda_visible, satudata_visible, catatan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    246   �\      �          0    16507    users 
   TABLE DATA           q   COPY v1.users (id, name, email, email_verified_at, password, remember_token, created_at, updated_at) FROM stdin;
    v1          postgres    false    248   ��      �           0    0    failed_jobs_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('v1.failed_jobs_id_seq', 1, false);
          v1          postgres    false    216            �           0    0    migrations_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('v1.migrations_id_seq', 28, true);
          v1          postgres    false    218            �           0    0    mst_collection_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('v1.mst_collection_id_seq', 505, true);
          v1          postgres    false    220            �           0    0    mst_dashboard_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('v1.mst_dashboard_id_seq', 3, true);
          v1          postgres    false    222            �           0    0    mst_jenis_dok_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('v1.mst_jenis_dok_id_seq', 3, true);
          v1          postgres    false    224            �           0    0    personal_access_tokens_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('v1.personal_access_tokens_id_seq', 1, false);
          v1          postgres    false    227            �           0    0    ref_data_tag_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('v1.ref_data_tag_id_seq', 160, true);
          v1          postgres    false    229            �           0    0    ref_grup_dok_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('v1.ref_grup_dok_id_seq', 2, true);
          v1          postgres    false    231            �           0    0    ref_kode_wilayah_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('v1.ref_kode_wilayah_id_seq', 28, true);
          v1          postgres    false    252                        0    0    ref_organisasi_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('v1.ref_organisasi_id_seq', 53, true);
          v1          postgres    false    233                       0    0    ref_topik_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('v1.ref_topik_id_seq', 34, true);
          v1          postgres    false    235                       0    0    trx_collection_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('v1.trx_collection_id_seq', 2177, true);
          v1          postgres    false    250                       0    0    trx_data_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('v1.trx_data_id_seq', 293, true);
          v1          postgres    false    237                       0    0    trx_data_organisasi_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('v1.trx_data_organisasi_id_seq', 89, true);
          v1          postgres    false    239                       0    0    trx_data_tag_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('v1.trx_data_tag_id_seq', 492, true);
          v1          postgres    false    241                       0    0    trx_data_topik_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('v1.trx_data_topik_id_seq', 29, true);
          v1          postgres    false    243                       0    0     trx_laju_pertumbuhan_pdrb_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('v1.trx_laju_pertumbuhan_pdrb_id_seq', 1, false);
          v1          postgres    false    254                       0    0 +   trx_luas_wilayah_kecamatan_kelurahan_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('v1.trx_luas_wilayah_kecamatan_kelurahan_id_seq', 6, true);
          v1          postgres    false    245            	           0    0    trx_riwayat_data_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('v1.trx_riwayat_data_id_seq', 303, true);
          v1          postgres    false    247            
           0    0    users_id_seq    SEQUENCE SET     7   SELECT pg_catalog.setval('v1.users_id_seq', 1, false);
          v1          postgres    false    249            �           2606    16531    failed_jobs failed_jobs_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY v1.failed_jobs
    ADD CONSTRAINT failed_jobs_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY v1.failed_jobs DROP CONSTRAINT failed_jobs_pkey;
       v1            postgres    false    215            �           2606    16533 #   failed_jobs failed_jobs_uuid_unique 
   CONSTRAINT     Z   ALTER TABLE ONLY v1.failed_jobs
    ADD CONSTRAINT failed_jobs_uuid_unique UNIQUE (uuid);
 I   ALTER TABLE ONLY v1.failed_jobs DROP CONSTRAINT failed_jobs_uuid_unique;
       v1            postgres    false    215            �           2606    16535    migrations migrations_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY v1.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY v1.migrations DROP CONSTRAINT migrations_pkey;
       v1            postgres    false    217            �           2606    16537 "   mst_collection mst_collection_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY v1.mst_collection
    ADD CONSTRAINT mst_collection_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY v1.mst_collection DROP CONSTRAINT mst_collection_pkey;
       v1            postgres    false    219            �           2606    16539     mst_dashboard mst_dashboard_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY v1.mst_dashboard
    ADD CONSTRAINT mst_dashboard_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY v1.mst_dashboard DROP CONSTRAINT mst_dashboard_pkey;
       v1            postgres    false    221            �           2606    16541     mst_jenis_dok mst_jenis_dok_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY v1.mst_jenis_dok
    ADD CONSTRAINT mst_jenis_dok_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY v1.mst_jenis_dok DROP CONSTRAINT mst_jenis_dok_pkey;
       v1            postgres    false    223            �           2606    16543 0   password_reset_tokens password_reset_tokens_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY v1.password_reset_tokens
    ADD CONSTRAINT password_reset_tokens_pkey PRIMARY KEY (email);
 V   ALTER TABLE ONLY v1.password_reset_tokens DROP CONSTRAINT password_reset_tokens_pkey;
       v1            postgres    false    225            �           2606    16545 2   personal_access_tokens personal_access_tokens_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY v1.personal_access_tokens
    ADD CONSTRAINT personal_access_tokens_pkey PRIMARY KEY (id);
 X   ALTER TABLE ONLY v1.personal_access_tokens DROP CONSTRAINT personal_access_tokens_pkey;
       v1            postgres    false    226            �           2606    16547 :   personal_access_tokens personal_access_tokens_token_unique 
   CONSTRAINT     r   ALTER TABLE ONLY v1.personal_access_tokens
    ADD CONSTRAINT personal_access_tokens_token_unique UNIQUE (token);
 `   ALTER TABLE ONLY v1.personal_access_tokens DROP CONSTRAINT personal_access_tokens_token_unique;
       v1            postgres    false    226            �           2606    16549    ref_data_tag ref_data_tag_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY v1.ref_data_tag
    ADD CONSTRAINT ref_data_tag_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY v1.ref_data_tag DROP CONSTRAINT ref_data_tag_pkey;
       v1            postgres    false    228                        2606    16551    ref_grup_dok ref_grup_dok_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY v1.ref_grup_dok
    ADD CONSTRAINT ref_grup_dok_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY v1.ref_grup_dok DROP CONSTRAINT ref_grup_dok_pkey;
       v1            postgres    false    230                       2606    24624 &   ref_kode_wilayah ref_kode_wilayah_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY v1.ref_kode_wilayah
    ADD CONSTRAINT ref_kode_wilayah_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY v1.ref_kode_wilayah DROP CONSTRAINT ref_kode_wilayah_pkey;
       v1            postgres    false    253                       2606    16553 "   ref_organisasi ref_organisasi_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY v1.ref_organisasi
    ADD CONSTRAINT ref_organisasi_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY v1.ref_organisasi DROP CONSTRAINT ref_organisasi_pkey;
       v1            postgres    false    232                       2606    16555    ref_topik ref_topik_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY v1.ref_topik
    ADD CONSTRAINT ref_topik_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY v1.ref_topik DROP CONSTRAINT ref_topik_pkey;
       v1            postgres    false    234                       2606    24608 "   trx_collection trx_collection_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY v1.trx_collection
    ADD CONSTRAINT trx_collection_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY v1.trx_collection DROP CONSTRAINT trx_collection_pkey;
       v1            postgres    false    251                       2606    16557 ,   trx_data_organisasi trx_data_organisasi_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY v1.trx_data_organisasi
    ADD CONSTRAINT trx_data_organisasi_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY v1.trx_data_organisasi DROP CONSTRAINT trx_data_organisasi_pkey;
       v1            postgres    false    238                       2606    16559    trx_data trx_data_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY v1.trx_data
    ADD CONSTRAINT trx_data_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY v1.trx_data DROP CONSTRAINT trx_data_pkey;
       v1            postgres    false    236            
           2606    16561    trx_data_tag trx_data_tag_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY v1.trx_data_tag
    ADD CONSTRAINT trx_data_tag_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY v1.trx_data_tag DROP CONSTRAINT trx_data_tag_pkey;
       v1            postgres    false    240                       2606    16563 "   trx_data_topik trx_data_topik_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY v1.trx_data_topik
    ADD CONSTRAINT trx_data_topik_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY v1.trx_data_topik DROP CONSTRAINT trx_data_topik_pkey;
       v1            postgres    false    242                       2606    24652 8   trx_laju_pertumbuhan_pdrb trx_laju_pertumbuhan_pdrb_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY v1.trx_laju_pertumbuhan_pdrb
    ADD CONSTRAINT trx_laju_pertumbuhan_pdrb_pkey PRIMARY KEY (id);
 ^   ALTER TABLE ONLY v1.trx_laju_pertumbuhan_pdrb DROP CONSTRAINT trx_laju_pertumbuhan_pdrb_pkey;
       v1            postgres    false    255                       2606    16565 N   trx_luas_wilayah_kecamatan_kelurahan trx_luas_wilayah_kecamatan_kelurahan_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY v1.trx_luas_wilayah_kecamatan_kelurahan
    ADD CONSTRAINT trx_luas_wilayah_kecamatan_kelurahan_pkey PRIMARY KEY (id);
 t   ALTER TABLE ONLY v1.trx_luas_wilayah_kecamatan_kelurahan DROP CONSTRAINT trx_luas_wilayah_kecamatan_kelurahan_pkey;
       v1            postgres    false    244                       2606    16567 &   trx_riwayat_data trx_riwayat_data_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY v1.trx_riwayat_data
    ADD CONSTRAINT trx_riwayat_data_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY v1.trx_riwayat_data DROP CONSTRAINT trx_riwayat_data_pkey;
       v1            postgres    false    246                       2606    16569    users users_email_unique 
   CONSTRAINT     P   ALTER TABLE ONLY v1.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);
 >   ALTER TABLE ONLY v1.users DROP CONSTRAINT users_email_unique;
       v1            postgres    false    248                       2606    16571    users users_pkey 
   CONSTRAINT     J   ALTER TABLE ONLY v1.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 6   ALTER TABLE ONLY v1.users DROP CONSTRAINT users_pkey;
       v1            postgres    false    248            �           1259    16572 8   personal_access_tokens_tokenable_type_tokenable_id_index    INDEX     �   CREATE INDEX personal_access_tokens_tokenable_type_tokenable_id_index ON v1.personal_access_tokens USING btree (tokenable_type, tokenable_id);
 H   DROP INDEX v1.personal_access_tokens_tokenable_type_tokenable_id_index;
       v1            postgres    false    226    226                       2606    16623 7   mst_collection mst_collection_ref_organisasi_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.mst_collection
    ADD CONSTRAINT mst_collection_ref_organisasi_id_foreign FOREIGN KEY (ref_organisasi_id) REFERENCES v1.ref_organisasi(id);
 ]   ALTER TABLE ONLY v1.mst_collection DROP CONSTRAINT mst_collection_ref_organisasi_id_foreign;
       v1          postgres    false    232    219    3330                       2606    16573 3   mst_jenis_dok mst_jenis_dok_ref_grup_dok_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.mst_jenis_dok
    ADD CONSTRAINT mst_jenis_dok_ref_grup_dok_id_foreign FOREIGN KEY (ref_grup_dok_id) REFERENCES v1.ref_grup_dok(id);
 Y   ALTER TABLE ONLY v1.mst_jenis_dok DROP CONSTRAINT mst_jenis_dok_ref_grup_dok_id_foreign;
       v1          postgres    false    3328    223    230            $           2606    24609 7   trx_collection trx_collection_mst_collection_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_collection
    ADD CONSTRAINT trx_collection_mst_collection_id_foreign FOREIGN KEY (mst_collection_id) REFERENCES v1.mst_collection(id);
 ]   ALTER TABLE ONLY v1.trx_collection DROP CONSTRAINT trx_collection_mst_collection_id_foreign;
       v1          postgres    false    3313    251    219                       2606    16578 *   trx_data trx_data_mst_jenis_dok_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_data
    ADD CONSTRAINT trx_data_mst_jenis_dok_id_foreign FOREIGN KEY (mst_jenis_dok_id) REFERENCES v1.mst_jenis_dok(id);
 P   ALTER TABLE ONLY v1.trx_data DROP CONSTRAINT trx_data_mst_jenis_dok_id_foreign;
       v1          postgres    false    223    236    3317                       2606    16583 A   trx_data_organisasi trx_data_organisasi_ref_organisasi_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_data_organisasi
    ADD CONSTRAINT trx_data_organisasi_ref_organisasi_id_foreign FOREIGN KEY (ref_organisasi_id) REFERENCES v1.ref_organisasi(id);
 g   ALTER TABLE ONLY v1.trx_data_organisasi DROP CONSTRAINT trx_data_organisasi_ref_organisasi_id_foreign;
       v1          postgres    false    3330    232    238                       2606    16588 1   trx_data_tag trx_data_tag_ref_data_tag_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_data_tag
    ADD CONSTRAINT trx_data_tag_ref_data_tag_id_foreign FOREIGN KEY (ref_data_tag_id) REFERENCES v1.ref_data_tag(id);
 W   ALTER TABLE ONLY v1.trx_data_tag DROP CONSTRAINT trx_data_tag_ref_data_tag_id_foreign;
       v1          postgres    false    228    240    3326                        2606    16593 2   trx_data_topik trx_data_topik_ref_topik_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_data_topik
    ADD CONSTRAINT trx_data_topik_ref_topik_id_foreign FOREIGN KEY (ref_topik_id) REFERENCES v1.ref_topik(id);
 X   ALTER TABLE ONLY v1.trx_data_topik DROP CONSTRAINT trx_data_topik_ref_topik_id_foreign;
       v1          postgres    false    234    242    3332            %           2606    24653 M   trx_laju_pertumbuhan_pdrb trx_laju_pertumbuhan_pdrb_mst_collection_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_laju_pertumbuhan_pdrb
    ADD CONSTRAINT trx_laju_pertumbuhan_pdrb_mst_collection_id_foreign FOREIGN KEY (mst_collection_id) REFERENCES v1.mst_collection(id);
 s   ALTER TABLE ONLY v1.trx_laju_pertumbuhan_pdrb DROP CONSTRAINT trx_laju_pertumbuhan_pdrb_mst_collection_id_foreign;
       v1          postgres    false    3313    255    219            !           2606    16598 c   trx_luas_wilayah_kecamatan_kelurahan trx_luas_wilayah_kecamatan_kelurahan_mst_collection_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_luas_wilayah_kecamatan_kelurahan
    ADD CONSTRAINT trx_luas_wilayah_kecamatan_kelurahan_mst_collection_id_foreign FOREIGN KEY (mst_collection_id) REFERENCES v1.mst_collection(id) ON UPDATE CASCADE ON DELETE CASCADE;
 �   ALTER TABLE ONLY v1.trx_luas_wilayah_kecamatan_kelurahan DROP CONSTRAINT trx_luas_wilayah_kecamatan_kelurahan_mst_collection_id_foreign;
       v1          postgres    false    244    3313    219            "           2606    16603 :   trx_riwayat_data trx_riwayat_data_mst_jenis_dok_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_riwayat_data
    ADD CONSTRAINT trx_riwayat_data_mst_jenis_dok_id_foreign FOREIGN KEY (mst_jenis_dok_id) REFERENCES v1.mst_jenis_dok(id);
 `   ALTER TABLE ONLY v1.trx_riwayat_data DROP CONSTRAINT trx_riwayat_data_mst_jenis_dok_id_foreign;
       v1          postgres    false    246    3317    223            #           2606    16608 5   trx_riwayat_data trx_riwayat_data_trx_data_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_riwayat_data
    ADD CONSTRAINT trx_riwayat_data_trx_data_id_foreign FOREIGN KEY (trx_data_id) REFERENCES v1.trx_data(id) ON UPDATE CASCADE ON DELETE CASCADE;
 [   ALTER TABLE ONLY v1.trx_riwayat_data DROP CONSTRAINT trx_riwayat_data_trx_data_id_foreign;
       v1          postgres    false    246    3334    236            �      x������ � �      �   �  x�e�ݎ� F��ì�����J��2M�
�:��k�I2UU��l8�
9H\j���!8��^�Q'ӏ��G
�ib|����K:�w7���b�y��o!^���_s��r�s���C�̓�㹐"���۬���fw�װ<���D�;��1P{K�����|�A������4?}ƺ�
u��p5t���X��x��ϰ5��P׌B�b-qCS�^��p�J�旱u��%��ox�dO�<a�13k⭟M��=�/C
j�tl��l�n[������?�V\0�2n��P��0�����J2���@U�0.&���M��`Td��q	�f�J1ܒ���V�������&-�����ǥ��4dv���������5�Q��j�j���eL��m�?�-CU� ���cX`�ԝh��9�6�������ٺM�?wK�^�V�(��ג��<����O�
������ll      �      x�Ž�n�H�(�[���B�Q�0@n�T�dW	��s1@��%���dyܿ�;|o�=�=K�2����A��>��ʈ���t~�s�^%/�}�YV�*���_��^��P?�w{�0�a�7O�z�aܛǃqԛ_��i֟_�f�d�?�d�%=�h	��KWUR>'ч�L7_�M"���_���]�O�U�H�D�L�E[f�}R��u��'�[0=�/��P8W�����2�Yp���߻O�ǽ0l���^��2�����2X��3��f�� ٥�}�K�%�xM���^��n����>o��rY��ߣ���}�ۿ���Oc��Qt���AX��׿�磋�d�m�$ߥ�:���7+�A����֛�BA@C��E��uotݟa����d�m��5�Y�����.��d�}ƭ�|O��qk�X�У�:���l�/�\m�M+� ��;Њ׊Vh����v�	�N`����{qvg|=�_�gA�X.�]�K_�P��� ~��)\����~�J���#�SR�O���xy�m��ֻ�>aE!���(^�\�噪h t��i�Gp�������>y�}ݨˏ
&�ἷ�Ɵ�`bo6L>�<�t��kr��U��DO%|qpb6���/JFٻM�����2����sy\�-6��ÿ��#�=����}Nv�a�`F�m�J�x�aC_���˛b�m;�	��M�5�.���z8��}s��h���
?�XZ�8�S|�ր�3L����l��s����lS��	���͚\���=Q�or))�IV�k���Q�ց�����Bg8�7-Y�z0��O���X/Y����`�������I��<���ܲ~�Y��}�۲H�9�9p�|I f��g�
V\|������t8gI��m]���]��Z���t���Ֆ��z\vB�b��E`/B�"L/M�O��z<�βi��b�>ms���o��i�y���,���������ȭ�@\"�P�	�{Ʒ8=F�$$�:�$$Rp����g?�:,O�_��kWe���k����2���KmY��Y(�8E��9���#�w�tkA����Z7,|����x���:|�W�پ��eU4��Q߹�V��p7�xJboh5׼]���$-aM>dK<=7_�]ٗU��J��ot���~Ϲ�#|��#1ӲG���1^R��z�3�G�m�T��Z۽�EX�����E8��o�[�
n��Q�z8��Ë��$����-�7p^��4��X_��L���q!x�W��.�+�5�!���,�V����=g����.4���2p+�0*�?@68�F��^f�,ocLF�^^`w	r�pI�k���+��߉��6<�B����m
^.����"��s)<?	�j��m{����`r=a����]�e�N��^�
ε�5�؃�	��5�'Y���������h�Ѓ�|7�iֻ�>Ќ/�E�'<�m�Ɨ�Z��#�A���ƈ��+��	���̈́l�4�!�O/kÆz��u���^�Q�1O�KZ$p��?ҕ?c���pXؐ]R�l���Lv��.�5�a��A�	��S����<�ſyCL{)�*�������.z��GC�[�v�_��YRko�y�X?�h��F��>8�ua�o�h��4`�>n�i����y�ہ�ȳ	��w�N ���uQ��n�p�<1Z)7�_�FAب�NL7t����#mZU�l��z�U���!��_F��\��W�}�B�q��P��i$�Q�Zdt=��zAX3�U��s��}�U�P�>�;��~{��>(�ل�"���>\^?^!~\�f�=����F�$ IXH-
��#eZ�N
�������薆K�4'�>���X�S��
$0�yr���p=t����>��9PRy6��Vw�'w�x��x[,�wJ�4"��:���$~���&�B�Y�4���3�ag���)�q�.3$����?��/�1�a�=�)H�]8�ݚ\�{A�/�?~��pm��\����46�H��R�����ʿ� 蟤��Y�5�4mڷ�T�Ltэ4i���ڏ��`~=���v����*����+�������W�WG%��Hl�y�XCOK�]^8R9�-�	�>H���U����	.��s�bn+���r$y��!򱴎�0�*.%��`�q��C�}H�#i*E���@�B����4�lV����c_A&aC��Ա���U��[ũ~�^��m^�Y���b��x�����M�Xҭ[�M��{�n�b[&��{~R�0,���^`櫜^�/pgÇS�~O@%��tW�hw�>;�}�SAeB[6uɣV�{EF1.`�qc��uY$���x�풾89p�7�x���ç����	sG����r�p�C"�(Tȧ
��1�
�(p�B�Ѵ]җ*�h��Vc<FAc4F��1
���ؤ�]�h�&���{���٥��kSki��-����cQt�h��it=�a!�
x��?��ܻ~�gɁ'�m8�D3-�z���>�������`�^[�Z��s;���l��w��X�4^�zA��J��E���^1���ܕ�f����R81_�w��eC7^k�1���qJ��Z�E��=Ӆ�Vѥ���� b�"&1tېĄ!֪�������h\������?ߥ��2%0�5���;��QBba�ȋG:�ȆJ0����L�ڇ�W��o%�
yD����B/�n��,<��J�	��p�B�t���1*���Q_����x.���ޮ&�1�-~t���K�L���޷��2��L_�;��> ��XxE�������(��*�������m��6��1��)���k!Bc�zB o�}�dC�����~=e�Os?
�p�^I,��)�W!&	]����6m؝
~�'Mi�wD���k ��'��cVd��U�;F��;�{�k����
Z��q̍^�[��Xp�?y���at�)y�P��-I��
~C���3@�P�S�Vc�`8�x�����D��q�:���|˷�>�-\f{谠�aN����ۛ+�����Ɵ����Yx�z{���z�.E�uӁ�q/��
�7�댖Ϣ��n�� ��ųb�Z\�2�j������a��(�J���dr�(߀--��zO�>��qp��2�cFd)�c��Yek��Y���´nXT���� L�/�j�Q{�Xi����J�Kw�j[fW��ë���7�&�Q[$I�ɪRW�e���Q}(_���[���ZW�[�P4�Ӏ3��Tb��br�"%R���
�*�@D�##��]�zT[;���Ѭ/��7�"�q��Wg>n`�����P+zGJ��X��ʰ�4Atx8$:\O��epD�w�n진#4�P8q��i1�m��;ޘ�����3�1U���Y�\���nV�²;�YvW�^j�}���X����$��"sYޡ�X��+��Ҝ�D.��Ȗ��A��6E8���p��jg�_|�`g�#{ҡ������������^�_K�]�kˈH
5O]�ϯ�E���6�i�j_���ԣ�[�-�#=���+7]����R��.C.[깁~,S%=Ȯ��ꇝ�t?��P��l���Ƀu�i�<=�v�C���LjC���JA�W��/[�^�x�+�z�q�
���yq�Q΢(4E�Ȋ�m2W*ǼL��29�Mx
��qq0�0��\j�^���}�E��v�N���0
�`D���F��0� ��º_���N���[���{+��iR����<����	��w�V�~U���G\��
d�Wj[�
f��I6�6�j?� �x���"�,�_� ������NO�ጟXu
? �]��tѩ�:\�aXw�d8r��>��G^R5����9����W���Ә�c�k.ʃ��	~f9{����иӼ��̒�z��x��� Q�{M�W&t+g���Z�/^i��_���?�UY���I�%a��:��]��p���l��9�:��S�\=���#�e?�����-�۾H��w{�}�_�%��ލ.�EFz��sK��DK� L�S�f�5s8�CO��m5�9���s�T�x-헏ծ}9�����UrGH�����[J[#�l�    �l�Lv���ח�,ٙ"6J�����`���l�N�~
�I�	 A`�u�1��ҍ�y�?���!�� ��9�����c�%��M6?t2�;�윾�}ر��e\��xcғY��G�#3ҟ�;F�h��;���r�md����f�I���z���r�-�W]��Uy�Q<��������SM�i5m��!���<�6C�ݯ��د����E�_c��~�&k��X���a`l�[����)��{>:H���nՉ�b;�cEt`�Z��N�G���%˲K2)�0�2�I������>%�z�?�'|!:�qD��av�Aܗ�dP��jp����qJ[��z����؍�x[6�E���G���k76E��l�i���݆�����l����_0�m����J��c�.�=�l��$i��^��i]$ҎÍ��yZ��x�ug���w��Fv�pEx�O�b, ��qJ��2#\{���W�IS�Utj��1^��Qv�Ы�tp0^�fl�x�Ċ�hPGrl��?�N��\����Vֆ����CUd���A\}�V�[��Y*ۺ��
���e�Ĥ=e��p	������l��Sh��&u=.�N��Nm�1�r@��2R�]X-���������@~o�Yv꜆��+l��N������ԭ2����mk�7�A�ft�D�Z���B��I��٫�����x�K��c
g�u!�lRBZ ��&���,o�b.W�b"eV}2`K��͖��J��}�y�E�e,X$�zO09�$g��[�Z5��~�f�lzq@-j�3��A.hu�jT�ã���8%=i�|�c���c��3�+Kv����
f�w�3rԘ���,����W�GaG����qC��<��=�燎q�Yn����Gj����ص˕���Gj��%��q�6	��q�nN	���kۡC�T�V�&�Z���,L���6:�09�y�r]��A��l5Q[5|���q@ro1�Ö����Tk�TnmҔќua���6�ϙ�v�ag1Oݗ°q��$S�ڵx���N�f����<;���E��w���oڿZ+��۰S��cu�>���b�v�ޜoX��(��\��|vj)7��|��&��͆�,_�f���h�LBh��\<C�E�>qN�;�`�~u����҉�'��~��UF�j��.۠�+n�&�gd�[�0^��v���8��t�䁻*U�Bѧ��0���-�����PDm�$*��l���ST/�Ƙ(RK���O�N2i��g�?TR�#�)
���[p^�-ɜ�Y��>C7�)P���i��Ce|~Aƫ�UzG��h��K竂;���I�V}���]H��t�� ��AK�LWh�f{$]�`�6k\�Kk���U[ ����4��<6xb+|V`��'�������:�(3�]��G��$S�XOR����z���c�a�}G�~��]~\>wֻ���$;m��)7���ć�b�Hq�����%#6xO��K����72�#C�N��[ ��L�����=��c��濇7G�>�6�x�o ��h�4 ���ao�tm����2�V>*���m� _:�
�����l]P��=GZ؆���i����UvZE�*t�m�-�su}�Wٚ��7����vi�v{]����gAX�X��n����_[��Ht�����
J*��d�H�����Y�z�&�LM��Ep���Z¡��<���fZ�D�61���4���z4Ǘ��mp��Ď�Mb5b��	��A��MF�B�5��r0#�hZ�Hð��"��g#{�I��E;�X�9�U˭�ɞpy���w�keEq����.ꓵC�u�c�����l��xr<q�q��r*�S\�]�׉���x�]�tx��@jĸ 5˹����N�LԄ���&����v����Fڐ�R��5�,�4��������N��$�U�(-��r$JyHK�D�~�
���e�(��JY�(	EI%�(5��z=?A���U):ӎ�$7�W�XG^X���Zб�ҢJ�]�&Aة9�G��\����2ê/�V�rF�"Ė�Bd�U�X�l��G��o$a
�Y��2n�Y��U�<���*j���h񦾊C�N�9=��h�Οu����&#��~Gq�4W#�+W�:�r�5�;�Q|>���l�G�ލ���^ �o���n�&5n5�-
��0�Ȩ�J0v���$�'a�[��Dz͟<�tl=�V7�}6����-������������5���!�E``�t�����3��:*�P�
��V��~[�7oo�ΚE��".�17��)P�4�앎�0M/N$�P�{�ǯ�^���D�"��a"BiQ��'����iv���K�=5�^:\t�c�{~J	;Iu2��FIW��͞kR/�3���e�s^vj���v`�ҍt�<�k��tb�{t�ˁ�����14�s�S�3�R�(�@	�澅	��]3'����$-.��I��M���9Ò(r����/R%W2��E�0"��j�ym�����ڰ�D_u��е��R�\��ȉ��o��}��z����ڏ�Na�#{�w@7=�;��f�ր^��*�d�8�o������l�n�t�?nk"뷁�SYȃ�	*��8��?&]rcL�Ƙ�Ksc�w���4��Ao��O��OH�%�?�ixO��x2ꖱd2:"i�d��ܛY�QCV#�tW��(�����|S�;ov��o-1X���E�qr<����KH�d4���IO�"��O�jD�*�Hf�����^�<p�� ��T7
�=�������.�n=7���p����E\>4�P���ʲ�7dY?�8����܋s˽8?�^�����3�4��Q�Rb���![T��ٷ����m���e	��W$Ԟ¿�_� ���!0��uo����ޱ�I� p�;�P���y >�u�_���M`���g@�7��z���
l+d�fK�x�R�A�<���l	��aI����-X���E쀃�E���`Sh��AT��"H�y]����9Fo[(�*G�c��g��s~Rdbt�tgQx�?v���S����2Q�?��>��~��zz�HvG^]-�N����S�g�
�xo�������'�wƋ�.~��VgJ�� �?7�3��_�C��D?�n<�R�kؑ�v�#`
V�_���4 4��2�
@0�b�~���>g�2%+x�POuW�|�6^t��%F�F'TǛ��t�kO��j���j�5��b�/?�<�Nq?��3e"�uL:�zW)�G�o7��F�SZ�P$�^��qһJYF�S�Օ���T�+::�yW)���#�N���H�sȁ��s���l���D���I��������M�
�E�n`�r<+_���`�׭�VZg�x�k�k0�&�;�(/G�a��2;����{S��av.�ٱ�M��:�>lo������3�]n�MJ��?lpNe4�)�uC��T��-Κ���5k�{-�{0t1F�w�-�R��ݜ������# �������cy%UuWQ(Eg?��_���0�xM��d-[���%
�g)�B�yN��]�6@(�r��xc�eA��ɚ��o�Pj8>��.�eUcl�f��j`�]*�gmG�~fޓ��-V�yG��ņL.~���d�ڝ�vy�_�������<9;7�������|�3+B�?]�6Uޥ,�}��lI�Ӑ�]:��6j��e���]����EMd�����(ֽ!A�+rʨ��9��\���3�<����}���yڎ��	��h��է��kN���u������}��4~6M,�S*,�)�=�rzS��8����YBߛ�DX���D��N���jq�Hޠ(=�3�����n�ި���y�JEr�.Bb��ǎ�����sf�%�t�r����v��¶��:]k��6m{�F���:k��)m获�{�]}���m�
`�w���+���^ɟ:�}J~0q�!lX_��@ec��M�0ys�r̋me�C�qQ��d-G���	;���W�����_�;>y�jW��F����!�����h�^i���	��j��h�^m؛sZ����\��Ix�w��@�u��(| �    �Yb.�u��:��,$�2�� �s�	v�w��O0��c.<�cg�F3־�̂�.�	=�c\Y+�+tc=l��Sb�K�a词����8�ǳ�����ŋa�q�)pBj����w� �B�"���y��
v�Ʊ��"=�?!ܗ��jY���5���Nm ��Fv�e�[��y>PJ+��H��'[�;��;�V0�N��#r����ꦃ	���U�9�l�ޢ�-�u\I9�M;ք0q|ɭ��:�un�������G�n�E�K'�׆�ф�|폪�3{9�X�����qs5h��*\�v)q�o����},%B��`�Y�`��O�3lܚ�]�ιc-��
��X���[�{Ǩ�G��Q���(H��ݯ��0�oQC��]k$���[Oh��JD��L|م���Vn=���.>'�@�f����(�-�yC��>���N	l{ J|Kvw]/�m��,�v�_�s�`�_Z�'�nӓ;�~�>��E)ǪR���1�;ϴ����`�_U�?�t��y�_�H/T���M;g�l/{v��C��:��mp���L� �u�8��B��J��Y����"�r�@~d|A8��"+8�����`��B��;@��\�BoH��q�pa.�Ӗ
]�gfv��j�q�V���3J#��������5�v4s/1L-�\L�1br~;�φ�9��؍��H}��^;���24�v��agSY(����GXt��-ԯF��A�QS����*��'�h#.m��YC����`ĵ
E��p���}#��%'������������,͓Ǻ��)���ԯ#e���vy��#�65�/fb��\�n_�k'��j��,�_��F��9t�	���)��Tq	JS��ب�t���娼߳�Y=�grz�o�Ꚑ�g�Jv��)<a�"s�X���������t��ὕ����"�췲��t����֯ #s(�d(HrZET *���u�\�U| 	m�*4*A�Q}�^1��c}WٓQ�,,S��Y`4-��Ъ
ɉ���%?��F��4�9�t[����TW�aNp��í�?e��9���/���E��e�8��,s��I��Z�=s��Z��<�cu1+~�;;��[X�K�Anj]�*�B:��-���!&6�����+B|���Ģ��9�����ԪV^~�S��G� �,����%t���d�~���:UG��iK�5mኰkK��ϰRWM��(�K3�k�у�ci :<�;�w�J
�r�����fq.�n[0�7�̞Q�a�(�^�W��`^���WL��G��`~l�X�\�X!ƈH\�ē}vc�lL�l�<��H�@{�#�����v慟��r2�F���F4/���~i60�gN�O�)��< c�t��|~ �Yov����J)�� ֦t��̤+N3���b��˶�������6W�IW�b,DI�@J2�R_3�;��Ioa�ޞ��K�?fx��p$��M��+�x�xq�x�Ix5��l,����B5n�M�{�5�a]N�h��uq}��E�?��n���g�T��`G[��:��܌����藤̷�w��$Ӽ*w�҂������53YUY�[���[xCrtHɺd����2�a��&N�ԯ����`C��8[eyfGv�}5�Gn�u�}5*Ŭ�y��!KV��_b5~��0M[L<n8v�:���wI� ���d�ֺ�XW4�<��u���X���Ys�ۇ���:�{��~/d'ZP�!q��_b���]DK췶�3!;����׹���uFo?u�du�c 7U��ku��F�Z�%V�ef~�%wL��
�N�Vr�TsɨR �w�-0�J/����Df4�XZr����ܫz��9�PH���^'6S�̨o!�V�vB�����uut��|5��[nL���5y�T�p�2YW��
�t���25�kQك�t<9�)ng��3�w�?��j ��<;:��xn�+�-S������ ��N��a7(T��'��"�����1���b<�Qڰs'�M�Wʌh�-No�l��~BCr+e04��Bw� �m.�c�:�2`�ǨV�G��زml��A��\v,��~-�A�>��q�1�gB��[D�E��3}&�ζ�Rk�-M��i�h�5Z�A]!˰c�����c��Ǭ�ĩ��X�"��Gt�<��{)�=�����Ȭ�����`oT���ܗ��o>� �ܣ���p��-�.J�x(q(J��V6���v�(�\5i��X�Y�`G
�8�}���yG���/�q;��w���S݂�u��kX�A��X���5���j���ٯ�,QVR1��"�#����}WR邭��U�t��qu��z��U�O���l�����֮�7[�5�q�^X�m��H���V������`o��1����ud� ��ŘPad�_�ˇ���kr.I��2%	�)D��7&�T�`BΔ4���:
�iY�h��>{;�"}����Ⱥ,�	*��ؘ����#"��;JuN�J��]=�
�J���lɤ��fr=ao��u��S#�_֊K}��<�E~DU	+���0 "�$b�K�}ru	Y@�Bb�ՈЋb�x�uPkN���d,-����߲(�ʈ�i���-k��A��V|3%c�3s�⊡���݌n{�Bn�*a�� �,v����o�r�	́U��QIi����2�[�/�4_eS�JJ�/�)� �.����
�]���h-�ꕕ�)��M�o J��Jd����W�
%pKp;��n�@S�GQ�.3�d�o*��,�H���Nsa�Fs��)ָ�k��kQ���2�q��]�M(5bL+��n�����-v�����V9,�G���E�x���S�~=�����3�e�/�L>M3�|���M��x���V'_�A����d��R�Jx��a�ëU�eX'[�t�3��)�����귙^��n���3���sn��x�~p���p޻������]|B
�\�R
�Z��	�,%n�\�|$�g#1A�
A �0GYJG�j��XJ>��H��y����2�����4���O5��^N���Z����q���&׭>ơ�6�W���8���"��ɒ	o���U����H�Ԛ����|�y��eM���ϑ�l�Nv:�����s�sF~�������grGa}e
�)wY��;��6�˝Y�`��&T��^ꖌ�i��4�aG[3�~E�����wB����{�y�Z� s#srޥ+x跹	S����{L�zR��x`��ذ�������xɖ���)m��Υ��t�T�?d�6/n��,UKx�TK���?�sPj�;1���{��=L�
�#Y�,�e�.]����.K��[Mby���Q�5�d��x��xj����H�y����qI�:�$ߥ�+K��R�ܩ�+JO���Q�����k����	m`\Wr1�����G�yBk���X��W��+G��K���<u�9SkMuai5䣬]ʕ� wa��r�DE�E��܏�f�~��>M���Yd�%���J��^o^���3�WH�T�bRvVӻ�΀Չ��b�,֝�`~�5���X��졦�S= �#�� t{�JYs����u�\V�c�x���� �x�ssCb��X#BC��:\��Aة�.�3W�(��d�!�JTm6����n�;�n`~s�F���o�꠶z�)\p�9/Em��a,Ʋ!�v�zz����6Oiػ��P�u�f1өW�� ^�GO5��҉��p����ݯ�P�8�p���$����$<���Se���M��oUI���J��md�o���+9V��%�۷hw�N-V�I��ډT}n�AB��OWڷ���^3I��+�w��v�iDזt��sZ1m����u�ag��q�` MI����F�#Ͽ�z�_}C�:��2s�u�2�SUw�f1n(:(~�6v�e�'A?��}8�ޝ�\�ñ���ǋ.������lj<q�	;�(�|���cM�w�{m�%9���c�hI���<�I�Sׁw��x�}P�cߎ'��S7	c�Q�.+x����Z����嬛�Z����➏rӪk,?��    �*��b7Z�g4�pV��ׯ���.w(����X+7I4:���(z_\K��u�K�!��dU��IW#	�ڬs��'����Na3�!L�fm>&ߛa���Qo��}����v���q�ϋr0]s����`z���˓�&���;�^�-ݳT��99:�5�`pR������a��إ�b�4���X���!=d��^�X�8���4	�R���	!I Ѭ�M4������W��N�#�w M���B�Ku�����KW�
�\=���!�ҹ�ِlz� �����R���}�#ꨟ��}�k'���.2���ޱz~긁X��U���Z�^�c��#c��A��紐b؛�R���v��	+�e�Ӷ�+�Q1������Q�&b�� O��\�dR"�CK�;T�y��*�(��Ȑm6��~ ����lb��`��s���ld�p�p�<��G{�{�ѴA�s�=z��,��^�]��\{+&B�ɺ���bR��%ļ���^���t0ʼ�D�����Ё���b�����$��)��u�ش��Ʂ��Ed�ID�����T��Ӽ��a��E,���xc���$��Fe���������FD6k��{��Ճu���U�eAAt �x��׹�$
NL�ė�_�w��q�R���R�U�k׻�z��%�x8�����e�r7y�ދ7@۫g)xQ���&��?Z���.��\8��n��X+bɰSRi�>�&��h�I�\��z�֑�&d짪f�`��=��%��p��r���h?.S.�A��4����\�s*��>Q��°cr�S��a+�o7_W�� S쪜"{bq:fV ���=����3�n-���P4v[K귉8l����	���j�S�m�<�I���O{�ƣ�������������:�>^}��>^���2]gW�m�?�r�y*�۲�c��='ӹ'����P��_F��|H�X��k/���,��1^����̣B�Nwޢ	M�;ڂ��	���	v���*�W�T�J�N��rm��1pE3�?O7�t�UyL��(I0�{��a�`gU��T�ʁ7���y�[(L?]�{��nޚ>P�4�nF$�1��ptq���������_&U�N�*5&�#?I�L�W�����^y^*��Qh����f�/������SV�;�<��/K���k7W���$� ��?⨯��=����VɅo�F�p���c�sQz���5&��(�9N&�Bni�'i��\�e]��� �)��A�Y4s]F�t��p �;)R��f	v�/e���V�C�_��H�R�����XR�%��Q��3ca~z=�K�ٜ*g��[��h�_$1w�N`�<�-�m}L��Y7[Tb���Ě
�c!����!�B�*U �Ш�h���Q51�MO{Gx�c��|,�?'x^�^��x�V�k�_���.Hg�:I����g���\y0���'��W֚����-�g�}�z����O/A�b�X�EU�7��:�W�!���eQ.�`d�)֥U-�:?^�!���^ūPً�^d�.d��S�Kkj��0���j��i2Fg7-�	��`h�`���?U��GL4�;�!�g��%��1fA����u�ac�u6�p}���eR!�������4�;���=k4/�vɬ�]Ufy��kk��g���!"��T�=}���n^G�n
�ɼ>ql��6n��p��8.����F8���<�6�b����^�	�(���\��WU˭�H���à��Ճ�`���87^��㭨ER��,�ǋ���*i
�i�o���hjgIS�&�	F;�T-�Y��cq�Y�U����Q��M;�+ǽ �k0r}5ɇ��\ݬ�s�?��6g�lY����z��	2�/��1�wb;��lQ{NE)v^���<���Kr��q�R����٣�-��щKC�u"�):�
3�`da!�,4r���u7�`]$��d�
[G?e{\{������]f����d��;S�����ωcy$�ᩤ��W&�
���\LY?�4v��C��X��jHծZ!�����>N:����^�V�����Ţf_�Fa�����SL��nbq�Ǔ�[�bt^���"��_�mC2�DRq��	��P:��?2�$�D�Y�b����0,x��
��\s����Rʵ|:�xiP78�Σ��3�ɤ�ʗA=�;�co%�h�uR�WWR�������|�R�Jt�hta��h��~ ���?�K5v=?m����d�5��)�7l���q�c��Ar�E�cUѠ��z|ع�G���N��9��ѥ��Qg\�
�##�iS&��̆u�������~�Āi��ߥ�%*ަd����k�NV���w2K�m]�,���ڽ/����(�Z��`{�����d}ɦ���p��lؙ�ˇ��]lI������F�?u����bd'3v`�I~���'nX1���(XH������� ~�W%���F{AX` u�4���[&ҼS��~�~�V�Cq����\�#o���!��( �/;=Vs8s.��2AB�j��eե$z��@�U���;H�����G�wl-�[Wq__;Jޗm�
�Þ/�`��gg�uҤ�{��o:8��o�����H(;�l?�,�����#ܧ��)ҽ�n����u�C=��^�m� F�$dȀ�/�)u��1[�4;�i;O�]��vV�H)�;�hվdA���5�[� O@ߴ��R�=v�.	�Y���E�l��� �����o>)�����U��K�E�U7 �,���{\�
aX׺���Y���r7�I���ɴ.:
�#�:�;���y�d�L6�$�r�>���K��J����2�"�%�)l��z/+q�L��.)yymJ��/�n$��|�� XG�| ��Tv�&Z�"[�ٙGE(FE����;��� ��ZK҉�.,�)2�i�$�P�r%X�S��3�w �g8q�m��az���1Zb6鞤�����s%��b�\ %H�e3m��a=Y��q�K�E�{Llm�:|kR �w>�
̰4�����v?*Z8'y ]}�X�m<n`�"��!%����D3������v������}=)�S����{��y"�RZ!�����9-[c��BU�n�`����y~��k8%rJ�0���_��C3��vH�`��6l���sUE��lzilT�:m�����^�<��[&n|�}��(V�m�ޠPY@��d�-��'��[V�j,㱏\��Vh���/��]ť� ���2���e��e�J0tCU�R�ޖAM��b0m%:ʯ�Ў��U-K�u�j.��TR��AA��Z*�[[i&�n�s ڰ7/�đr�L�VR��L��X\ME���;%t�/�Fի�.S��.��l�Գ�ao_�����xs���_.�F	�zj��񎜄gl�R�3+���N�=q���`��['�C=�ˁ,^�?ms�������|dY2^�Y���MK�;b|�T뀮^�����uRP�5&zqv-N���!(��$k��7���z_��%$-��)�E��3���[��D���?ש�q{����-k�d[�;�VD6v�2�}��^��r=}`�?%�^'{`J)��4��S[A;|wT�Xf���]�~�ǖ)�3�SB�L�q5��lQ3�s����&[^�cņX�0y��ܢe�3�$��`;�o����#	�T�)��m�ҍZ&f��{�NS����u��o��P�վg�j��v]���T�´h�O�!X�S(`�O�I}�x�IYE4�c���(1�4m!��	n�<�q_�{=X�]�x��W��g���L���|V�������W#�:M��0*<�¼�� (^HՐCq�ty�}-�~m�à����'es}/HC����8 ��!eMc7�)U�ƺA� ��5�H�iǇ�@jNk��]�1iݶe�G�d�:�vr`����6U�sx��a�$�N>k�i����V�D���B�l`�
/��'��q˄g�.̂u��̾�n�j�S<�c�;U-X�g~`k�Y�H��$�ƫ��}�v��o�B
�)���    �@��߂u�,��#yi}"A�qެ���㉹�f��\��m�2É�4j�h`�0?�w�̱�[��j�ŘT��J�q��H%������R����y��sT�oB����{ӝ�=���(�y��( +D����َ�����>�XV�8��1�F�����m��F�6-��zo����?���O��/�����|�����߲���@2M{.�~�i�F�Q�_�;='�ve���٭���	�ǖ	�<9JÎߠ�DJQ�
GwO���d�"��?!�ty���Ł���
�X�����J�T뢱	J[�0���2��s��bY��>����e*�����iFU j�!�_��AX�S>���X�$�^e
BUncep��j�6SR%#�81��E���* ��ʆ�
l�<I���	;^M���զ>��!��̰�3�3����#vZP��2��N|��Fp�lWT.��W��K��릱�@�F?�}}���l1,)���6^P[���nP����FN�h,��m�w<�ԔU�o�%�?pq�#o�S��cF����gRWP�<Η�N�f#�O!�Vg	��F��	"��*R��è�c�y3�"�m�ӏ�z�Ӕ9hm�gnh���}U寡�k0@���^� �uq�K�%5�.����S��VhǕ+�2C6s�����?���z0�����|w�:��!o<y�߭[� [��׵<؛��������|����*�M�&Xr��[��d���"�؎�*w�f�)s��hl�:���ܺ#�!΃��ow6q�Ut{Nd��%�xdO���Ȱ�]+[(|����1fǎ�{�_�o5� $�5�~��eQf����.��]aT�}�ޢ�}���>�:+^C�1+��B���`��-#�_k#'�ێ��B�������I(�`;�_��d-�Bm����<t|�1�X�y��G�r�7:cn��VR�'*��D���4���KL�AX�K�?�!`>I^A#v�#���Z(�C"�$�[5�p���;ޱ�����vd	;��j0���,��Z��&֖j	�m{�#�;��-Xx�B�`v��c�%Rz�g�%�L�7uc\��c#��JȺ�ư�A�!Fs=>=U_I˰�����Qn-���b����/��{�7�Jؑ�wػ������-��]J�_ͮ����W��0��@�P���a�����l�;Wm�ß�O�f�H>;UT������5�_��2c�L�/�V+���h�55�m��?�U�]��J}��{���M�Vm�]!�����C���İ��/�6�a���7,>פ��~Jr�<Cu��蛆��H7�]�pR�1�n�HL018��cb-�9p�Z���Ȕ6)82�9��.q�-�ƺ�	�ܚ�����֑�co(y ��8�F.Ȕ�`V��2�#�&8���I�I_t�^:Q�=T����Y�+H������Puʿ��p��6�*R�|L��سV�����Й�� m8ys���4�(�����IK�j�:��GȰ7,�g��|�/�}��3u��Ja�T�3嚜���V�g�1��	ʔ����y��LEa��"3�*G���~m9	vҡ"��p�-Z4	�&����Fy���pˣE\5��5A������V�㹗�sJ�C��&�a�������g�����T��R�ĳB5�J�*�j���|����OK8�h�B1�#�)M�ʏe6�+�h�і��a�����g�`$�H�F6R���t<��Kg4ЗN)����V�������?	9���.%���1��8�uoJ����ꏆޓ�L�p�G`�	��\U¹y�i�S�tuI�V,i�;�`?S��\�
'C@Hb�⥀�߾N���x�F��|�B���˻��,�B��)����B��v-�����<��ȍ����y�¼YI�e{`��7x8��QQJi��ĆH�DbI��V��xSL��Bb������$��dr�
	�/ٞo%��*��kC
��E�>�U V�Z��v=���<&v��5���'(Lvͥ�]���آ{bCLX�N�j@��c���Y�e5�?�q߼�2�y螶n2���P���v�b[k�Ep$$�@���3�,UP}��U4���ꤵƱ(�Kl���M�%����:~�p 
� ,���%҂�[���ȝ]�A��;MYoÆ��)3��c��%����j�uf���{�H�3`o0�a��d�<\z٣�I�1��ę�z�R����;��C_�2�!���Em��b/lMe�`)��^4�����h��0q����q��~z���zB��?y��X+�`�wk�4��)f��%v�_W�d�wwQ�A������c8���,cu�ow��Uc�Xu�0����s�\��,ss��<�}K�шN�Nv��~Z�mѰ6������߃��x�ئS����	B�,K�F���Y�u��FW�R��R^y~ǭ���e�v��j��R�cB:���OX����Z5{n���(ĩI�Tg�Mف��/Ղn���/=Um��ş��{���H^�{n`��.�-C�;�S,0Ԡ�^���ѿf�]V���+�}YI�^��h T�C�N�w`ǯ�@ͅ��]a�y8J����K��>f~�˽ |v�+��9�7#��F:~��́�nX]���S�ˆ�D�z�)����;[��T�tI��q�_��e��g�l�Ք�}M��z@���'��jC�$�K�ɨ��~]�b�*�T��Slw��b����J�DT�ED�D�""��E�9�l���f�K�`�=@��8��	S���A	4�������5��B7�@�8ɖ2O�1�A�y������up*���,�n���t��Y���\���.�3�2v$9z>-���r|�v�Y����wm�b��Ie%�U8������-��F0	SC�S�C��}��q���w�c�#N��?�h�r����|&a�>�ܿ��[�D��e�9�9��c�}U�9�U��Gu}P������:y�UJ7}uP�Ӷ�I��r���	���ԱOX���;I*��<���*�(�Ri���F�{yE7�e����5�	�x0��uo��D�ޘL�R��6�hbS�]��������oB�@F��5w�kX�oY]����Z+$T��u���j4<��&�	���Vp[Y4� o00y�=X�o���R����-Ŭ[����g����@�>��B���F�m+��֊��T��F�4�z�y@R,g�4H��D�P�Y���p��~��R�,m�rrb�턩#19??����M{QՅa��L�N*h{�M��$jX"'�CT�S"Qamك���l�βs�m����^�A�RJ}�ÅN]`� q�R��߰=�#;���'ZP]o )�3����>�(����������ɬM��sͩ�I�D2���.$#���D�U��ּ�LC�@�:����@i���`��l���y���
����WqNOn�,]����u��sm°Q/l����<ڇ��sȘ&[d�/EɚsM6�d�rEdc&k"�4-a�@��㚖��؆���\0�V�V]�I�ӊ4�"	��|�#\��x>��IoX���x��������f����>���W�:���6s�7h����F	htXm0��U%��ڠA�����������60��8S`�����/W��o�z �/8j�HPb$o$��X/7�Gb+�8Y�B
I($�H�hraU,�h����ýε���d%/���n��⬣{`M��3���6�( �?瀐2x��D�m�X��zZ�V�m�nۼ"X���)�WV��r���N�{�$N�`iu���?}�+?̧�T���ۊq�?}�h���O�����Zdj��(�P*O��9�{zh�lUs+Y46w�T�������VxsGMh�N�sK���]U�4�%����������cF�Ծy#����ZpkKߛ�b_���c���3�.�P섵ZTye1���"_�|��ଡ଼	�H�b!��������-�%����S>�
 �  y�N}c_�ǰ�>��`�o?���jw=�#1��2O�3lҪC�-ױF�5vla{�DEha�4�Ƹw=�u;q5���u�	y�����)��[���t���h���mu��V��-��6W�S�1�|c��Ы�ZD;�+����H���E�4���k1���Dl����$�B� 4�h�B�F�#�ֲ8țao
/�D����xRE�J
�i��R}�uDd�Pb�k'VR�vB��]Ӝ甐n�,R�C9p��?~���$�UQsqf`˰k���v�FMk7�.�N�l�U.ȝ�٥僨�@��)�E2��s���ԕ)�6,=�>���=F��bEB�nH"�H��&�WB����׼>���/%"aoX��W��+�'���w���ﾠBi� sЮ�,��C4����L/Fz1�;�XH$4ATSHE ��`V7�3��cز�c�ϕi0SS�`J<W��M�dx�4Cy��y�x~nfvh�AG����T�<o��U�Tg,(M��U\�I�;(�u�Ev";4���:���#��eFBZv�/s}��uw��Ph瀈��@Rp���k���3s���k:�_i�U���ڢJ��Qu-�]*Q�_�԰��oXe�T���q����w齀&IИ��*YU��(��h+��,���@Y��A�>�w���7�R�Qj(1��R��Q�4�،�f��Axn�ŶT�Ht_����/A}	�W�R�}��1ս;v��pt�$�6t�_nsi�&���V;Ζ5�b[)�x��1Q{C7�t��e񪶏�mv�T��^&J9��	ILX���pvU�TU�^�2m5�%;��R��a���@���,��M#��!�\�j�8�L9��=Ls��T}�iF��3Љ�	�ao.15R�Q����"��)�,)�}κ)S��C��Y�0�y��T,I�)�0��1)73)��/,|7q:��1~��y�hl��% [Vԩ"��rΉ��������5����*��{��&ao�y�pś�_���՞J��OK��{��$x��&��5��X�KW�t�*I���I�e�N��)'�`�\�"�f��=nV�[@k�Q�*��j�}~ĚZ�kb�4�����l�lY�6i���O��$2&�R��K����ZW��p	�O$9�R����д������	�]�K�"�.�j����O5�'6G�T6�ۖ=i��su�v7L_Ґ[����u5�m��D�.Z����5�}pqf�}n`�g4p�}jE��x�TK�o��v�'�ѯ����}ą����v`��&��v���q%�#��E��`(_�H��`]���\��GY�ӟ��t�#���h��X��NCb�`���ã����CWjr�<����򝚩�-��(n���QA�]��_e����(�>�MN~��'������Sϡ�ô��U5
Lvϣ�9��˸�a�﬎]�AO=t)�_�N=��_��W���_���١��+�$�c�Lu)v�$W���Q�\��j�~�����t����ߋ��5J�E�������F�w�z���=��D�Ӥ|a�4"�ô����?��fM�ǋ��د��!���:�݃�A�m�����_��R�u�;���r������\\`��k���]��gv��=J<X���vSI�g3�.?���u<2�/���I$tXR�����y>@��Ȇ�#wU	|�
���P�(ﹸ�e��^5�+	>ӫ�/�H�%W���R�/n=.�-��VY��ϺHٽ?��kሪĤ�N#�i��'�'��e�,�Lr:u�G�
�@U{$���:uH���#�P����9 �\����;�:�b�k5׮X������f��U���%e��#پ�r�/Qᖈ֚H^�Zn��n���^Ce��:�C�^0>��}������c+N@/���e�Ỉ@�SG��(��5Xp���ꈳ$,k�3'��;ϸ�������:gG)�p&xwb��avNn4�_�'�z������M�GZۡV0z0/CE��Cq�W�,�0T{
n .Bywݥ_��o���U����=��K��������c��T��� �0�ƃC�y��s���R3}_n�f+��n�}�$��q֡�o��dp�I��	O�JH�C���|�.z*3��o�/�x�t8,҉����nhЄDM����?�+����/���X���      �   j  x����J�@���S��{���#("�Ÿ�:]�q2顧��ot���p6u6��������-D�?����;H.�.����š�5�y6���ɥZ�Ch�|�q��b�~���n����~�U�w�,�"�އ8�}��EmE(@�`H*�h˅�8(��c�<���\�y��b}p���x|���OI�0M��;Nӡ���Eq�L΃�<�']��������rA�&�ʘ�����*KEr�p��%߂KI5p��k"��H�*I�.�Х+�-Oe֛���YE���[æ��,e�����g��q����[L�դ���,=9��*�4k���1�r7�������xU����<cU�˚��i���_S�[q      �   A   x�3��p�4���tq�FFƺf�F�
�&V&VF&�ĸ�8��|�2��W� <(�      �      x������ � �      �      x������ � �      �   �  x���Ko۸��� 0��ED��y'�>�c;�d̆��T�-�������m]J� EQ<��Z"��(r�$��-��D����N���$x�_�6Ӻ7�{�a\��7��Qn�$,^��N�1��N!���rG��c���$���X��o�Ȅ��$>ȽV����e=2��Qnq2�{�d���>!od�#<��A�%�(�O����3<�)�O�S�*�q�c��,��X��4�ޭƅA�"�"�'6Ŵ���w1զ�A�����?�s�r(�)���-^��t�:J��G��c�Ip<o�.n�����Qn���x���=2L��M����S�_��vt	r,y�9���zo��1H����]�D�7-پ�Qn�`6����֒#Ӭ����[N����}��O�褏�߹#a�\�<�b�O�`��Q�E>����4trv[Nm�>1_��a��<8����oY�*]|O���a�s�Be����;؜M��0u23�)	�cx�������ֽK=0NU��Y����DE�73��P�$��`3�����dc�d�2\\�Y��A��k8��6�1�;���I��r�U�1�d,z�����1��J��Vl����y��9���r��G��1�-�՗�r���w^^;RXm]��6��cU�Y����M�!nS�]&��:���8B��?:F��fN���
$Mfԅ�#w�e�k�f;ڌz�k8}b0
�c0փBB�E�S��au�X�Y�-��QZ�圼�xgd}�V� f�Ga��`)�ۦ�a�:��xy�U��vlVd��-�ނ-�@D�����гK�q�2��(k3�+�-�6�y�5^��2�l�����Q��w$��X��l3�]��N�6bc�*��c��r���^
~(nW�d�{�ϔ�|~��f�KF�A_d�l�|'Ufef��q�\%���y�E���<0�j�Y���cI��U��ǿ���¯�(~��8`搀�m�-vhF��	rUPs�c�d��1�ջ�����ueT5[Q��|�G�������cT���x�ܽu���*.R��>�%b�8��n�<�Mu9VE_XTb&��v�JF���U��f�k0�� ��!t�Z�2�W����Q��Мv%���|�c�´E�������LUE.��o�öQU�M�噈�4���w�ۢ1-F�����`qT�z%�������F"�y:F-�<Z�]�d����T=�a�E�q	6��{��C�8KwYg���Q͑e���\��}�I��kF-�<l���a����c�����,�Uws�pt������~�I��h��]O�R��u�
U{'�ܳq���dj��5����<W�ӗA	��1�r�iV�j�T�tb�v&`n����tt��X�<ߐ�e1���(3ǵ�e�F���5.\��(��Vؠ=e��x ��Rǣ�U;���Օ�S;P�Q�]&�\��;P��z&"��1*|2�r�d�u�W[�U;�f���(U'أ�jc,��cβ�w2�c_G�^vM�f~��-F�7���C|��/�v޸)����Z|�(IU�v��J b�����Z�+��Q���b��C-[Ǩ]F��]BǨ�e�	s[��^X������F]�b�N=�MKǨc�|��E�v�uxy�:l/��I��=XW����uFo1�Xd�&�)���0�>̂b�J$z�wu�:6y��7�|U��Q�9�V�"KfC�(�R2�gD��U5C�B��Q�+�������Ũ�N��_�5��v�(|e�5�¶/���έ_\u/[�sS��6F��1�j��Sa��VY��1�Z$���c��.
��4�ߎ�5��M��N�i0�$XuԨ��t�������:跶���7u˽�V����W���A�g����Ǭ{���E�����bK��n*Ԍz�p8�{�������`ԳQ����I��:�s���6�IW�]I8��l�Q�!�iw�Z�Dphv�5��N؝�/C���R�Q�#����e=��z�e�U�ɮ��̴y�i��Tn��$���'-F}�,e�G�!	��(Q2�s������/�/��5M�Q�TwߣN��6��)mF}�Lwi������yI��(�ϟ���񡳡hV�F}Ղ$�x�J��n�Z��N�F�-ʇ��r����Kʣ�(�q��]�j#���k1�{hВo2�LT�����/���;�Fo$4��d�F%��Ő��L�N����;^�{�ufyd7�86o�cuA.;�6�ݪ *��sҗ�+~��J��W��<��G�v�������&�q�&�G�=�!��~�u��X��Q�n�z�����,!?���1�N=�ŦUS7t�k�������:z�L��Ax�	���S~��u�lNh����d��K|���6�Ő��zY�(G��i��������l��2��_�<��1���5)L�����,�}��Q/�V�Eh0������h��`sa�@-)�������Qq�*��|��_l}]�S��^>��Q��[
���L�.V_m�֡��I��0����m�|�����0�f\� !�$бϦ]榧�G-�Y�6�]�	z���l0�;��	p�Ժ}a�w���Q�j�U2�)Yo[���øF�P1T��4�_7�-Z��J5�[#j�8�/�6�S�Q���AL����O�Bt��j��A�%O����c$�qW�&[�������I���Af�kϕ��M$Ѻ	T3X��N�)M�w-��$�u5�f�u�_i��^�:�c�܀�ߺ�S3H"����E����x�E�E��ѱ�$��w�T��͠c�w;E�{G*��E*D�}��k6�a�Fג�&��c���q�ĹJ���B��ƭP� $ʣ��ۈ��1�d���ԗ�D�MjF��/������8      �   �   x�m��
�0E��+�$:T����RP���ɱKJ5���&E�{��S��p�d�	.֙���}�˸�5�c����;������ò'tP��o��AGxb�m@8"��*�P����Z�V	Gb��D'B���d��՞+г���'QMe���c�\s�ߧ�;}      �   l  x���Qn"1�����h5v�8yb)e��Q������Ĕ�LWE����c�P?�=to�3=��9]�CZ�>�O��yW��#]�C���I�;��* E�x@�6���������2�7���((̛J�X����!��@<��dXWa���6Z���B5�KN��Be.2aG=j@٠Tn6�+�Ǹo�Ȫ�1xMZ��PoP���t��-�Y�@=��,C`��%��P�+�5]��czOR�>j
�^�p����@ʿC[G.�����}�5k2R�g�7�-o4`���q��4䣠 o�ܿ�ѹ	JA�;���!Co��H��]���e:��*�����9k/�1��P��;�[z��C����&�N�g�yv0�%���
h�}��M��7�(�������9����;i�AAmP�asN��g,����)١������1�o�ͣ���˸��w���F�,��l��(rG�<5�fQP[��x�L�t�Z�\�N����FE䵸�h��9�N���'������܃��u=c�[5S6��گ�ۃQ�.XU�d�'4_���Ԝ^�)�m��|y4���4�5��n@������|j��Hُ6��xH��J�	�2��.��9i3�{��Y=]�����})���zѯ:K�i	V���b�$���+_�Э�#��E3+qu�7��	�K�]��Qx��Gs)��߮���&���Od��`�&@3'�:���Y+��iF5�H��������}����PhK�<��H\YI��T�o�hF$�8�c�m�-��(
�af�*��P�t�?��U��4yoѬ�\B�:@jI�$͂$�Z�-�.���L�1)��y���}}zz����      �   "  x��X�r�8}F_��,���L�f���B�6/b���lkS��=�4UTbNBw�t��t���2�E)E�<�����o���y}�2�񹻘����ڣk�4a�M�m_l�qn��=fU%u^�Ql�_o� 3Q9�(;ȃLD���]��g�|���(��^�D�]{>3ad����'���-�N�/�r�G���ي,Ư�~�J�+�]:�ڽ�Vcd�9�Gfq��˃*�L.L	���TeHP%�h��y����X���]'�7e��c����8uĜ�G��kT⠜�����Y�lFW&�P:�L�A4��=���"bac���p��Eý�
E��~��p�!���`B�ty@���#����Ҳ��k��f�]m1?I${+�<��m)~Y��/�Rc��C�IT��[��.�$��~c+'��r����MYɢ� +"���3$�CR��}E��@2K;:<߄�������J�Df?�����B�����@G0vU/�$[���(OJ�8P��qfu�K��ac�G�o��ʜ�#�'��S`�T�S�[�D�?d+�Q���L!XLal���n,ݥ�QZ���N�Ú��d"��dZc��u�J�(�]�*��kܠ�#̛����f�yN�h-�E�vf�5����wt���]ȱt�֑.^�����<�La|Z�����a�S����"����֦�1�HѴ��QT�#l5\�z"�ZZkiUA����A���8/�2Yu7H����m�0zk�r��y�/�����he�P�|A8�0���֪�<��=�_�)SUZ�4����3��h�#֖2���N�����E�,%�n�}���J�)ዱ�Q��:����'�
�˻+��U��u�Ws�s� ��8�'᫳-��L���$�Z��í�b0����c�����͕�]��
/F�q^�����J9���/�ف�9e�jJǢ�`ģ}~���a��U�N�ʹ)�Y�ݝ�5z
�:��ܚ&� 7��u�#�p{%�@	��`M�	#h�P��ʒ����49�����n8�����o�r)A	@�˨��hx�J��aT�҄,�͔��U�,ԉ��P|��Z�`�}F�ŝ	�-��pO��w��m(�}06��3C�L{q]F��R:w�R��d�����^'�2��Dfu��2H1Z���x���<��Sw�6�b�&�x�@�� ��H��r���f��y���as��j�S������z����ֈ�	#�n�lJ����5:�S�`�O�)u�$�G��ö3ňϭ�Ή�tyGv����}���#��"��]n��      �   �  x���Mo�0�Ϟ_�#��GTU���t���֋ٸ�I⬜D��8�vi�Z1������yg,��
�����K�Y]'�vɵ9�<C�����9�x���N_�
�H��I���:O718z���w��ʳ�ܝg �UUۆT��X�%(v���꒝i�iW�o�C�mr��i��k�خ����,�S�`�NmH�c��Й{�S�vҨ��]���Ύ�;�JS�i"򻄂�{7޺^�	2B�
x�W��R�c�Q�xM�����&O�hjs�UL.�d�~(�OB@�/�����d{�t���� x��:8h��tƗ㬺�Z�6��Hv�K�eӀga�]������m/�����]��<�{=h������@�?�1p�l���L=3,���0[{��v8
I >;L�@`�mh��8A��o1L�Eۘ.��Ϧ��	�Cߒ����5�h��>���k�c�5����	�>x����C)AFVu�@dl��ݱ�=-2�GĒ��0���oqzlM�Y�Ċ}�S�j����i@[|!XL�^���P��i4k��JEdYL��NWT���ن����n��C�)���H~�֜�go���b�ۈ�j����!#J�4�Wg�O�/��B����<��4�} � ��]2      �      x�Խk��u%��ί`�|�#���`�=�E�m�HjK݊����["UbW�&Y���;7v&Xǅ�u�;~\Y˥�8xl�V�����������2�޼}�����_�����[B������ի6���\�gk|i�K�B�诂z��o~��g_�����7��$C��&i}��O�?�|��ջ?~��w����W|�㋏�?�~|��ϯ_�����Oo�y��+��?����~��V_��7����������_�?���_��������D|�i��ghK���M���	r���m��������w߽����՛W/޿{��wo?�~���O�x�����wz��7��~C��o���7��o���߾����?����y��z�y(6�d{�����Y��ռTb��`�M$�l��<E�����_}��#�ɿ/���NA��e
����������?�귿|�?`� �>_�3�'*��b��Rz�-��o�E�a�&�uX����'3즇�~z\�Uƻ}z��M�}�������UT���.6Ι��T*ӄX�;o��;2�<�i�C�	��w>�t�g�[��͸N�rܱ��Յ�r�Q��]�4��L�-s�z�n���i�!�cN"�z�V9]�k��E�i�]����X�;��[vg��;��j�z޴J"r�VJL���;O�nOC�z�i-�Y��+�8����a�w,��͜5�H�/M։�z��e�!U��YC�]��J�K�B���wy|(��|�]�g_*�N���_o�+�y|f]2���o�MP��n��D�f�.A�'�]���.�a��ˇ��X�����վ��������o'���z��*�ϛم��Z��(�k�X���n��-=k���X?e+����;�ٝ���G� qM�b�Y�/Y~c���H2���}c�&�\��g�Z�=�g���R�N�)b����a�y��c35s����Wo����݇v}��}�z�����_�����������w�3����W�=���۟>��������������Ɇ�՟?����1��iN����U��+қeOb�t"W��1��;��I�!�.#��1��c(:�^���I�00��?�.F
7��ya���7o�<����ͻ�,��sa�e,,}ai��|E�K���d�LR_���bO*\���Amw�b3d�s�m�q�.T�z���˼�t'o��g� o˼yU�E��&5�[��ix��n��������{���&�}MF�O���[!���"lATt+_�_��^K�'�f��ѩ.ɲtNA�rE�6��ojuMV����X{u���^�uv0TX'k���}�Y�QY�N'L���������Z���V�!�Z��"ܯ6�E�o�r��+ݎ�'	b��犧o;��=�aO_>u#Ā��W(L�5W���O�1��s�;���2��c�d���[��z�f
�+��L�ӌU_��I�gz���W*��9������I�g�Y�X�w�����g�g�ᬆ�����T�p���3~8'~�mykm�D��l%�0���C�f��|�2�ٰO8Δ��CtՋOפ%�K7�tI�]�t[g9_H�˓1����d��s��ho�ѹB����u����C���>:>��r�����9)�.%>:7�ץ����0�{r��T���Z>�����=A'lʲ��yh���{��cNƕ
n� n3~ݬ>��W����b�k��k��{��Iѯ0`�'?Hܗ��f,b�~֦Ā�L�O�aF��N$蝰���q ����i0�髣+x���t���������WO��n��d�^�Pa=q���J�M��_�������i"}2VU��~{�-a�5��� ��y�7�E�A�b��6���+�`�t�n�e[0�����*R� ��F���֧1��*�!�6e�Gr�?�1�ӻ�;N��k-�	3�C��x�,�tY����"O��tm�b�e*,�����y�������y�t�����
�ֆ�
,Ux{ֺ��D����#3x��1dU�>��q��n�c�Q��OBe�z��a�8�Vq��رe����cMYVd]s?ѻ8��Z��e���`}�a�9B���-���G���u�aH�F�'
5X/������f���}�e�m�"���!�;q��[O[��γ��ؕ���\�u�c���/K�SHM2Ѳի�]�>'�>W,�W�n�1��˴e��<9ˊ]�;#�Ѓ�J!G�k9�.Bxy}�������'M��,V�L��.���b�/U�X7x0�xK�޼T%�OWt��8^%�b��5���
L/L^��S��4�:�+80�x�J_EJ^�a�1�U�E�d�\��ھ����uN1|���0ˉm��&N�ɝ��Q|�E�7�K���0�ĔI~�f�pVb�z��M��NL+NL>��2+AҾ� �M�7�$A�U�=�kl)Sy]!<s�8���O��N���vb�zޛ��暸���u*�b��Hy�G"p�D���w����q���ڟ�~NH�/�W���0�\�x�T9Z�X��gAz�+'�*�@z���I؜]I-��1�QC闰�[`�GA*�I�kK>-�H˗��w���)_2\����_̢�N%*��Uv��Nx���7�pև*����u�k3�b?����̸Y{c�~����
7�C+6~�e�(h&Rы�|-��x�~�5��=vǀ�\���9�{겱�&�aN�m*bԋ�(̴Nb������-��_�Mټ x�#I�#�e�"+^��������T�&{�3�_�6��~M�A�ʃ���Vq�x��T�rH���K�U(��eVd��MG��K��K� ~�"K��q��|�D�GM��E֕%�j��!������3k���Xa`�BI���b)H�6��>��}���{���O�T���)�>��&���N;���I��*�z�㾊��B�����|�]t��[�p��J�Ŋ�YA�mNc�yB�=.��X!/�(6��U%$G�*�ő�]ID�0Z0ZR�&�D�Ls�OF��|�Ct�A0���;27|G��*����_����'��<���k���C��J��.�W���y�#�N�v��j�����i��:wz�T���9i%�q��| ��W��8�l�����'�_D�Ҋ䗻D��EL��I0HVS�b����is��mܱ;�(D�ɭT��8����߉*	Y%9(�´�Rl��GXa�{���tҋ��ݝ�*�}��}ؖb;��c���='�d%�}�kϵ���)�K�=��V��)6
�-��l��O�]e�%�c�x,e#K`�L��TJ��@.�+)Y�P�FJ���7H�R�R���'}2�S����/&Zڌ���TR�J���|2��VXO�t2[U���ڂ�={#���UV���8�(�5M2����S�os\Cj]�ĕb㩶R��y�͡O�� ��TlG���2W�XSJE{�3X�\V�8uy��++����g���g���d��0�;�_s\���o'�N�e)XϛV�9.���)�\C��[��X�=-u\m�H�`�y�#B��������T����Xϝb�t�
2�Pa���?�0@ۖ)�E�T��.݌������0�d�����������= \�B�y��|x�/�k�z��k������<�����~�?���|����݇?�Ӈw���������ſ�y����O����~����w?�=ԍmqǀ�2G%���v��_�}�!��[�Մ3���1�",�m=?<��)M���`��,ȗ<�RCe��'�N*�U*�'�r�:m�{�E&��ƒt������4F[�~1�º8���J�h�>��`@���aL[z����ĀW��ąN��E����a��+Ns]����k�=v���TU|,�&:�M�r8��8��8��pf�8��������$퉟H/�*��o��Q^W-Yt%~eb�8���d4I�+�����D�OQ�xK���3�W�O�.|�^|��J\g6���jq��$�%��N�l���9�m�P    a+jH{T������K0�3�`HP>���>�Z�
b�E�J�N�Z�,'Nq��5/�cq�JR�D���'-�s�K�����PZ�ZY�U+�U���%xi�ק�Nk��.�.�?�ޥo����I�t�O8k�-��I�*uF;j��⎲X&LJ��]��[R�pK��,���k�n�If�g���F�t�
�<��x�&����E}g�`�C��<�d�w3�{kq�ξ/�u�;E�Q� ;%K�TW�m�{��A�"�Y�+�@���N�=�+0wŝ��d�os�v��1��[�CUJ�SQ�y�A8���H��Ac��C���1x'��ɭ�Ā��d)��s�gR��V`ħZ"�5�T���&@0O�S����6�r��?u�s2��
���)� ��4��5ǫ�H9d� ���>)-.�-��Z�c�(��U���Uӧ�x�8�4q~ _��66�?W*n	��Z;���c�m����IyM�[��v8�=�Y)���%�u�v�[�1|LZ|L�}5�������fa��1�FV�����s]~�<�J[�c�|G����R�d��d[� c������Ѵ^���{����4�+;qwi^1��5���$c�k%�����5����e�9o %���y�.ֈA�8����dgj�Μ�(�
M�I���*�h{ Ǹ�Z'��v��\0๒�LQd��3�s��r����$Zl���CR�c�Ѿc sϔb�l ɪ����D^$�D
��,��t��q�l�>��=�U���+6��y���}Pa�+��|��1�sR#���nI3��t3�;LFci��+ߔ��1�^Z%�W�ܜ�.y�1@;�W�_�봞����|3�KF�b�s&�x�ZQa�v*^�r����w��v�i�^�����>�X�� ,p�)q��қ�B5��N
n�sЎ�B�o]b&�]����b+υ6>�M��UDێ(��Rd�%Y�tT8�D�Y��*TnMV�����}-�8b)R����,�e�� �65<�%^����{Gd��]��65ĀKUd9����ܾ���E�f8�jIe��JSa`�8��|�|�Py������J��꘷}�	c���<�'��j�)G����T��Cx��x=���0�ڋ_^��˟xU�+��?�Hjm�e:G	��{�qը՚D%�����g�|���?���+>>��%�o�Z2�6ywi*�G	}c��1��[�ѳ������0a�ŉȹ��U'b�v�1��Z%1���W����̎[�C]	��տ���``MS��y�ps�½x���߿{����iq����8�F��7�RŁ�W��Gv^.�
�8�7ݤ�c��{f[5w*�Ua�M�����>���`��ߕe��Vq�m5~����d���lQ���j�s�������4�+��;��MM�Z�7c��h%-�$L���Hu0�t��'mLgQ��1�[���.�a�vX���v~�ģ��X�Ϗ�y���6�/JF[��[�Y�7_�k�3�V�/�	oC~�^S�̩^��0�]'�$����L�����*��.��έ�j�z�����f)�C�Ob�(y��w�f���m������M���Nt�+�W����7����i�g;c�~l�����Mt��З�d���x�t�g�v��Q*�n��Ө�z
c�*o���:�Ix��Sa�����Pq�ڒf:	������Sq{{�Z�'�v�I�WQu�N�[%����,�
�t̍���f�J|W�ͫ�s/l0�kq/pH��ϓi��;�V�xk)G��t���5��#Eё�o�h������c�N��N?�+��l'<۔�j�spv�\��*ȇZ���vaέ깶g�`����9�/�.ffUO��8c���*�1��D�`��"�"{�U?đ�'���:�~8|j�^�~��%�t��:�b�w3D$�\wIZ��U�^cI�"���<����G���p�8NDS�,9���=�����P/���Ť�p:*���C���*����,�+jp�-٫�'����|���뷟^����wyk�x��ݟ����N���|�����#�?���o�z��O?�;4�=pǀ�P�!�פ�:�ՠ���Qa`�*ٲ�&.�����Ua�*�)H[E���Z�*��=m�X?�(Y���/L+q��l���`�X�JN(�P�O�`i϶-������R~�K'�1�.���OqتⰽK)�a�g�\R�ij�d����ѩie��~��pJZ(o&��9���߹"70O������b��ԗy4���q�g�D��`�����;��O'�Ϣ���5M_\�1��4��,uL�yy���;��J�$����UK�c�w����hf��m��`��)��%?�0�B��K�2��J��%r#*����=u�pD:I�d��N*�"�s�^H#^Hv��"����\�}���D�f��Ʉ�o������4�mùܖ���*�5�w荇���.9y�ࣟY1L�S��
ē4 \���P���FC'�B5�]h־gc���]X����Eˮ�n��(.d���=�W�Y�ҧ�B��n�=�]�����]WkϚ1�:J����ISݳ�mB�2u�6��yn�ͮʳ�ںMQRz�B�G�@��:9��a�8���B݇N0xG�_�w���2�Za��40�=�l�&A����X��٭cY0�2T��T�]��M��vRls-����ȏ�:u�R�N%�����[鿦:<k�F��F�����RZ�<���+pw��\���xƥYa�\*�uqr��e���ֻc�s(��gr�T��Og�f��O�ky�箲K�8�<;����k�e�c�.���허�3��m[��1p������R)/�ҧĦ
;Pd�bDL���.���^�T���������9�x���nM���sbE�����F�[��UJ�sxC7��{qwy�阫�^VY��r�|<���^VY�.�+�<,�����G�����Z9$�l����"�Y&^��U�a��eD�/�µqQ��v2�˲"+Y�5��S|H����Ȋem\��MG�
�Ey�d{������X/K���������-*�X/K�����me�Kvs��R���x��KG/�;�;��"K2�.R����K�U�V�B��|�9��C��޺t^��NF�
�WEVYW��0Ra ���A�܅㐾�#j�@,�C�VE܋��GԎ�����24�5��u��^��*�/�؈��X;L��k
*릔y+
@�z�窉B���}յS���K�1BSdq�%��%Q����0?SDY���׬�Y;"^�,'���jy����@�J��es��.���Xa B���=��t��=�wDU<T6�(X��(׭B+�*�������V*�3�`�R%� B	�NO�Ru�s������ɗl�t����
ݩ56>'9A�gapdM�"˘SZu�����⚍��%׵�!c�,-��f����۟P�����⚍��U\�e����.�TG)}��K9\;�L{B1��r"�_^y]��\�^3���"˗9��ns����^VY��J�e��.�JN�p8e��Ƶ��D�{����<�`�t�����Ɋrn��!�\qeu5Q�eɹ�zh���2��R.���(ݩ�U%��ugT�Y-f�~�:��kW���-�Ȳ���>����)�����␕�__���u�X/ˋ,S�p�GW�Қ+��D�-s�����ݮ^.c��(�؝�u�N���zOe-�屜|̪��pҾ��)\�Ƕ��^]}�q��C��AhY��ջ�v��/c�,%�bW�����5�of`�9ʰG55�}������f�zYrN�� }�FRݺ��(]�s�T�З� �����X/KΩR�@_�,���֤�X/��)���*��G����I�R��v�j>�����iG�����������y
���t��.��F��g�&�N˛1�[	o�x[���)�퇣3ം��$���l�s���)���k�#��B����_�wcC�v»��FZ�
�`��ܒŻ1�t�W��֩U�H�:7��i=    ���rC�����U����A�\H�c�,)����ӓb�,�*�K|!)��x�Si�
���.����8�w�Fi�vV�2�|T㖐&�v��f�6���~��֭}N0��
�M�2qY筓�.rQ/��r~�od����i��z��}��X1�;on��^;_���EP���7�Y�-�[�b�q[�j���(�ĭ8�ܴIr����-7)+Z֐�j���u���r����yO\���B
x�MZ�8׺$ŗ��{;
c�7ݤG��K�wx����Ђ
G܍�u��/��t�;��<EU���3���n� *2��# (��^Ti��#� � �E��D�<���|o���������)����VP]��`@��PH"�x��My�J�b`��LЦ-�T�:���?p�RE�[�(��úd��:&������`�,�#Č�h��E�a{�3x+�
�4�Yڔ��0�[���b�.Ӗ��:�
h�P�:W
���۵[��6�ʈPLsiq��v�N�'*�&��bL��_��Ƿ����"����?�~�Ӈ�>=���۟�?~ʣ���?;zܶ����A�!x�4��<�C2])��	��al��!D�)C0Ï�{�6I;��dV�0�W�s�M]�V�+C����}����CP2�k��=�{��p�[E{>���V�9�Ъ��V�nE�1�(4�N�n����	�`e��x��{�!� 10��K���L�'0���sC���d_��h�.r�!��WH0\Y���\���u�9��ME��p4����B�Y3�}�3�`)���ς�18����w�o����1�2��3���u]`��X��b�?C�un��i�����^�߽y����Ǐo�]g�\����c�6|1���P	ֳ���Y�2�q���Waۗ�`��{PG��_�=
��|����Im��`�w*��T��T��8ڵ���-MὭ
S��_��j��z�[h{ܵ��U<����m�`@�/�:Wb��8�N��u4��V9���M���c�ݭ�}��"�Y�rH���L0 +���f��ƺ�o;�7��d�9B��=�t&�SL�v����c
b���y�*n�p�Y}eUH�1�.��/^̞�P����Yi!@�:������
2և�����M�OY6hǇ�9j�o�O��v���g-�άVP�Q�6ӽc�;N����^�S��e+��RNE)����moا�^گ�@M����/�b�1{���;☫|¥���siÒ���jA�u����HI�\a�~$V����N��oQa�vV���0?��Y��0@;	m�P8���P�_���&�B��]��Q�gQ��Tl8��]z�]�m xk���A}*�: �� m#�]
��\��
Kx[�n>�� �)v'��EC�=��7i�өD(���
�����=�uV,� � �9�e�qG(��$�fGe�.�0�\�����
���.G��#��*��z�To\���$cs/U�N6�V�[�]�3X��-*��+3�\��Z�h�_ek�X
��iN��)�� m+��t���ȭ0@��嚜y��x۰�1@�m.��D��`���i̜'�Js�Ua�6�(�Ʈ���E����6Ws�;��E�XOۯ�v�('�SF���U�����+��SywJ���2G����'��ŭO�
��Ey�Ct~OT��M��V�s���	K��L�혔���0�8��@U%[�1��o.	6��!s<�:U��7oMm'�M�,d�9�S�}�׬�:�f��c*0O�<=��XϜ�0�#�f6s��̕0W�|*�sU�
ԵP�R�fc&q�͹ō0�r�>j3g��6�J�V`n�9k�y�O4of�����	�d�dB>N��l�mnWt��W��e�b&�D��m�P� � �9�MS^�4�<:ݍG'Ŕ�PE^-v�j��E+0Oe�K��Y&>��Os�z�T�\56B4n���\ɔK}���E�F
�[�8*���J��q�7�Ur̍�y	��d�y�M��`0�2�O�ꀪ�n��T^����(f�݋�1@���	k���쩞L��A�?�7�s���1@����Ԩ�X�LpN��D�+�������(B�\f�"�(x�z�LTFi�͢��"�J�VX?
���������>�ʻ���� Rz���A�IE�Z���o�#�,댁h�t旷���^�G����=l̾��!ЯUE-��N{�m�)��`n�9��u���A��3��¼��:Y��3w��+`�9�hdZ�ȼ�r0'�G|��H�"��:�~��8?3�v�*��
��b���'�i[������6��&������ m#�Kj�Dں5�h�~D��kۭ�_Z0@�	m����H��/�sO��SE̤}.x^a�v��	ж�6*���xL��[Ǯ`�6���]�zXM�p�9���[��a���>�h��ݩ5_��0/���2�`��_[���EO'����E}��s�r��V��'AܵW��1ϊ*rZ_9W�Dz%�P�S��
넅�>W��y��I��QЇ���8R�,���Cd����Eb���»����;�q��Qx���G���F��==����1��+3ě�U��c=o�m���q��&����c��*���@/�Pe.0?�;�ka~�<���A;h����6�y�c�6]�G�pZ癡��=U�0��	��� ��{�H�8oM7uc曽�a�w�l���ta��;
���tG�#w5M��_}� ����qe�����1��X�ܬ¼��K��殛s\�U�K��4���e� q-��z����ϩ�;�a��Ff񓩻NU	����W]�*l��w�ꩱ����%��u��z��R>e�@<��O����!��Wk�_�1@<
q�$��v���BGt�,w��5��ٱ�����sY�V=d�V�u)�3�uhFw���zj�f��ӛ1��k=��k�B� kb��ѪY��t\��a�v[S�YO�h�U��M�1�*.w�=���ߚ��Ly	Dp��n�qw��2�V��.�D����P
jU&����s�oEE�����Vo��qն�َ�J��	h�D���ĵZa&�..ۅu�<K���3��c�~~u�#�Sϴ��6Gtǀԕj���K�z�R�G ^��Ir2Il�
�]����/O{cy�B�IĶ�5�c�x�O�$�M��ģ
'�kJh� ���tD���W?-DVQP�}L����*���kW��iI�DT��V� yUȗǮ�V�u�{��!���1@^yާ�~���k�<�䍐积Y}�zV�'��qB�|u�#t6��Ί#�����1���Ӄ��3�|LZ`��7,S�ˬ�2�Ӳ%�e�*f02��i�z識��p?갬3]�Y���0���b�Bżj	̲��޴��UUC��0��1�]	w�TV{��4>hY1\�".w��]1��j�1��s-�gŎ3K�)a��*�/SUOԻT�]+�uҞ���Nx���۶k�����c5���u�pxJ�Uu����U�g���[?ޝ����P��T�*���;ߔر�v\�6�'�� r�7�2h�B���9ۡ�1hӵS�F�֜A)��=��� n�x�ɧ��&��)�c��+��ضi:�F�	'�B�uډ�]S\h� � ��|ֶ[$8y��|礪�L����s���I���+j֬��ag	����	�6�v!�+a>�գ��v�̵0����X6y��tq�;{s���;6r�9v�y�Mf�k�?��#'AnCH�#��4}��l0�X�I2���K	7������4���<���^x���8��\�f� � ���g���|C3t��衫ߥ���C4stmā`�wޜ��3/�t�ټ�듸׵P/��nb&�Ʋ{1�+a�mΤ�2�`�x�\ª�yٞi6�~��˅��*Wl���۔� o+��������H��Ux%4
��

ЦV/�y�պL=CS�f� �P��S�VB��$�˔o��K��h�
b�
bX�L�P�2�Naٰ��Ze�    7�n]��,]J�`���Z���g��<�Kխ�ܵ��'��vP� �|y���u�������?� o+��,�K��n�m2(Wh�/Q�m�����U'��)v���n�����[��Q�V.S$�I{�3hG����T6������d�KG�3�Zzm���3�ka^v�d��$
�ӕy�}�����m� o-�9Wl&mݿ���,��ES���+�m����%=��%c���پ_��l�V5aЦ��C���
R�J������|{e��v� �Xx?���vM���������ȩo9�
G�z2�w[�u� oJ6��L��Mtؚ��8c�����tnj�J�`� sS�?���9v�� o+���MJ"k���Nx�ߛ���Qb��FA=�;�P��� ��w)�z���ěX'� �X���ɸ��E�$�`�x⬂�i�%�dWBR���]�x�n��5��$����W�R'����^0�:ߚ>=�3�sS	x�]�p����sS	�[Y(i�B	m$�`��+����^�����"G�8��Nws�X�n%>�����=�=T��w����mC� �$�]tB��i��M��梆�~ʄ���kT�.PU#���}*aR�oӮ� u-ԓ���{ӶM�v07�y9�]Ɖ[@>�?j`��^�o/� �5������9�0+2P�!��i�܈��� � �y�j}� �q�P��)Tj��cԪ�5�"���cPOB]���y5c6��ۥ��X�RC�j��'��ܥ��Th���gN�o����%ꎇg�s��t��t*0䏚+>�%@r��i���/��d������O�����Osiw�{öBe�|��2�*3O�˅`�y��kh��v}.����O�<��A�1�;�5.����M��a�H3WIlc������n���k�̌�rk�����n���m�t���>M�Ɖ��x� n��l�Ʊ ��@���������*��͙�}*�1�[1�<�T,�L+��W5�`�x,��ۄ6��k%	qU^Ӵ+KE'�`=q�#��f^�z�9��t�9�"�RB�xl�E�,���m$� s-����X�v��Ma���i���$ n��*�|��־T]�|� q'�wk����-X���2�����f3�yg0�ϙ���d)���;��&,HO�7ӓ��;��ɴ�39��;��ɴg�`}�G%h|I|�f�����Ͽ��/�e�BYTV�n��W�j00E�b9JqL���K�5m���s��O�.t׵Q0�<��c3ѬJU��܂�I�s��;��g�DL�1�T��ׅ8�2/oq�i�Wc�:Y��D�e��aT��Zx϶�m]�T��E*|��7�qkt�$�m���7����g����ĝ�my�8��0԰�E��'�HM��`�����c�y�O`gv���c�]�Zν7��r��I���i�b{Sc=q�z����������dp�b��'�HK�q���ƵUg̵0�ްibQ�:�C�e:R�o��sY'X3�^=�8��j��c��#�=U?�Mt�J�	G�R�UWe�i
�&�pH�Ҡ��*r��s����.�[X��Vr�8K�(G>Q���}����k�Id+�"�º��2-�1����k��4Ӻ��p�v���@�q�����b׉�d�)�p� '����񋚖JGˢMrB����:$Mj�U��e��n���ac��M	��2�|<_'��g��f���#R�wdP���˜����=c��*���5C��vC� q-�e;���nL�URč��F<�EMT�(�0G���*���&��o$����V��L�.Sn&3o�A�������}&mh���о_#�Q�z/��`�v,��So�zHyE�Q�o�Ihs���ٶ�"ٰ�6��q��m�I�R��"�-UʱG�@�ObM[T0�[�ts���F�L��`�6�v�F̤m{Sc�v�+��A柉/6��j�?��N��b����tK����Hur�a��3����C����r�B�������u����#��ۉ��,e�un6� �$K��'Μp�*�c�8��7�̷�k����yT�q˼R�쭔�H�r�Q��-a�i�z�zE�r�Q��O�ka�ӭ1�1@�
q.b�ܥp�8�.&�1@�	q>T�z�)%�[*	��T*�>��gۜy� � ��{�]�i�c�1�;��?nU{�3x'�=;+j��)�����R�}"�)�c��^�T+�>��M���x� o-�g�ˍc3߂ަ�/u�n��
x��<�b~n�k�	c�����	ŇY����4��wW�\0�;�'0��=/��'�/MW�E0�;	����Ʊ_'�*�c�D�_���Tލ�G0�;ߗ�0�i��Ku[��g0���5�I�F�yg
��B���03����U�^�g�0�����_ԴG�F�	/P'+���2����}���9c���Ige��%ͥ��"ԃL��6x�1ȁ�w3耪��#�#�y���N�bOB�ԙY�9����u��:�
9�:�:�l��� 0�k�N%���y�M� 0�ka�ר�Xt�m��)dq��H�ȓL�։v�ew1��KT����N�9|Z5�1��ɜs��zt��?F*�����T-��v�"����Sn��sc�TQ0��Z�����NTsmW�H0�<ɜ�w�2K�Z,Z-T1Gѩ�N�����������Ywu����Ε���v�ǽ�#����t*���^h[��f���R��}� k'��������
9�=��՘��:�g���ύt*��ev"k���N�z�v��#���5����Il�L��2h��90S/v�ژ7� q-Ĺ9�Ąd�eo���1T�(�AUF��U�dv̎+��;�L���Lt�&c�x�%�h^��L��Jc�g��g-q^[���O�����	JT{�Afb��m6�;�1@\=�'�(�mA���)���T�r*��4�i"��i?W���S�m�=�SKQLݡ�`N{Z]�=;�i����vpm��<�=��@I� � �����mYp� �Xx��:�����uꭇz��1Gц�&�nf��:�gNEw�Q�!ŉU�2�؇�1��n^�Nd��|��5+�SY��DI0� Q�s�NcM���1� k+��|��C�v�Z�gݬ� k�+�̙ubi	O���#�a�x��Io�*� �X��\'�by��]�mi`�DT̑9�����m4�v�z�TA��3���n�un�)+Q	s�b������۵�L��Zx�����.�6�7o�ټ]�;�י�|�9B�����	x;᭟�~����?����w)�9ӳ5n1� �P�sC��8����z��G�^�Y�B�s��f<5Ѩ0@=��cͤ������)��XUfR�=s�ǓC�� u�+SPX������[OQ��B<�t-&�Nz�↉�}&E1ɺf�'M�g�A���Pىb�)Nl��3�/¤uaT��Z}-�%��J�I�sYX]u=��V�R5��z���7��j������Y{���]][E�1��mYӪl�I�6Y��)�� qW��'��b���^a�xV�#atK�9���Za�x(�9�Cͪ)$M�Z1�G&>ۈ/$mw�"�l��ּq&�OQ���B|r���4���؄e�e-$]�T<>�m>U[����'s�j��*��ޡ�.բ㴭�q�b�wdޥ����݄c�|���wi8�N����ַ`��.��:Q�n��1P��ކyK1�ɼ���mo~2̊���C�v�7'�̪�%X'���<U����������ߖ���2���@kg�
I��<�⑉��{5)��9�S|[����M� �g��؞�*�օ6W�6��W1G�7w�6̻���u]2���c��-����	E� h�B{jXS���lh���x�{��:�c8	�0�;ީ�'�)��� �ȼ�R�'�g������97s�|Z��;�]F��Op刁?r�c��_��!�U1�L�0��%�>wg�0@\��    (�ˤ��B��~�1@�0�b�R����'���s���9�������<�1K���wX�q�����M�-��|��$��xw�T�Ee�|bY.-[�2ɏ�:�O5�A\1uQ̢��,q�l�=D���g�q�w�<c	��~bp3����p-����IK�
�7w�zQ�ky0ir�9��2�2������E]-Y�`�m�"B�P������oI
:�FSl9��
�9g����_5t�<��}��r&q ��T�ͻs�5E$�)�� �P��Fc�u��I�n�q��"/�yv�xn�[aH�'��T�}Y�d�j1�ʇ��[Ւ_s��|��|#�(��H��*�Iu�x�C��1�\�����5����I�憙�c���CH6�� q[��b���Tg��8��?R?���T�u����t���f�6���4� �P��B<�%�=� ����c;-sy�c�w�N���7�O%��z�i}�G=�����HӃbߓ�=�h͍j�E���uv��5�i:�Ʋqn�f.��W7MG�mU�
�m!��K룎��fcrPw�������C�;W�i��gu�QLN��Ny�Ӟ^vhv�#��3d'Uqg�m(����I���dc^�_�k>��)�TO{�l,`��X��J��h�&����f�[��kf���;�T�N�m�$=8�<>���
�U�|�̲Z2��c�L�G��
?E�F�!���U��fn�؆��������'*E�롙yb�%�j�ݙN��*�Vk���R�ԍCX��J�q�(U�ka>��OM��!����˳2���Sn=I�0���S+0G�nM�o˼�V��@)�\@T�0��!k�Ӭ|�y���-�y��i��w��b�D���u���5�_���\י`��/��j��cB0�;k+��Ƈe�cbk���EC�"��p��4//�Y��hѰl�"��p��e�;��v�EC?�"����p��<�\C�<�"��p�3����d-��.�5+���ޤG��n
���JJ���ܷ~2�s�̋OubJ
�T���o\��zrG%���o$u�"�q�
QM�ko,-X,/�|��O�MM��C�ca��P��mo"�����:u���],��f-ğ���^UT8v33W��S�B��/s�;#d���&9=��qTmn�`��)������RTk�̛7��r��D�C�]�]����7����o�)���sZ1,�hZ]�1�;<����4�o3�w��8o�y�I���v�!≉K�5s�c��3��|�e���E��:��h�B�(XSi�֚��m�vѮ�L�J�&�}��T�I�2�#�M��C�ca�+E�+i�,`�=*��?Δ߿y����_�z|��~=q ��ɸc` 4����dП2��)CAʇ����sn1���UY���w�N`��[k��R�b��Yv:chۮEP�P7��JLܣqJM��$�vm�� [�{��Y����74��֖��a}H~�Sn�AUK��TY�\x؞�ns��.x�[;R$u/��ߊ*A����Uz�%�`��đB� ����=��.�Ԧ���� ��fֶ��`h ��	]`����u+�;a�)�Y�K����^��of��jC����l4]�_bQOU���O�y����i#J���jC��D�X�i�ӆ1���U@�lC����vQ��-��W��Z�bfN���C��C�����H�8��4�0��=T^��'�,��B�!�����2w�ݩ5t�!����b��^O�z-c�x�'�\˄/t��I��M6b�
s����A1�PЦj�Qn�l�=ک�mUho3�a��%���e1ׅ9;<�2o��:|����OV��f���$�����"nqޚ3�I�$��"�_�(q�����`��/�UY*�Ε��m�8c�x(�5/�4��;YS*Ͽ͑�m�����Tc�!�0O�����e:ū0���V�~�2!žfi�1�xQ����o���ug�׽,M�!�0g���(�U�4b�ޙ��i����%k�W�]S(�:y�
`nuW[B|�8��:���~�	�U� � ��7�0oǼ�RBq����Zao�)�����ڏ�X��PA̸��<�������S��
C}%}��}qI7�D�v�$��sM�:�p*H���8�4��&n��B狸u[Y*R�k�"���2�Tz�qFK��W��Osi�����\:���\�O5�D�r�y���PB.��URU?i0$Mƶ� ���%in=� V�&]�+ť%���4��kU6*S�}�ج��V���^�I_�v�gj����}YN�v�	ړ�bϕY*�r�"����Y%�F�M��3'+�?l���am����UaȰ��SG�SgZC]v��6���~��9B�gM8U�;P�0�9Q�Jn�]BXաtjxTao��<9f[����f	��I�L����S��N��[��#����[���qN����NJ:_v����<=��W�b�TՏ�Q��L��A0Okaα�k0s��M*1W�Z$"��Le!��k��qk��*�5K�!�����Bp���R�I���۲Z���Y�²�s���ܕ9g�\V f���g7b�˜q���_���}�l	�C�W��r�lٵ�����B�~��	����E�J��ԋ�t��`��S ,.F�6u/:�u���i�n�[\~PQ��a�
n�
K�^�QK��L:�zC��L�;oĞ)�o8"QC~=�<�PuVu�jTU�;-j���YZ��hQs���9����`y4�l0�ܗ9��2�,�S�w[��"�f.���T����������I�5��y���"ߣ�97�֋�� �&�h� s���y�N�,��QUa�y�D���C�L�ۡg��T�vNM�(?w&��9z#�R��_���}�'�bҭc��^��Й�Uw����SY}WG ��}���D��1�8�[U�/ߛ�f��7�ޔ*y��Ҷ��1ۇ�\o	�+3쓲qk�x5(ȵQ�y��PGNtP�Nͤ��]��hk��򠪤h���@���՞��`��e�E6A�3��&�n�uW���`���Om�iU��c��/�YS�kRUbk��#��ge�H�1qF_ai���b�y,�SQ�'4:X�Ѽ;��'f^PvF�T����P�n܇�Ժ�KK�/��(b�
s]L*�,Uk�����BpQKP�i-��ɣ�DQ-i}����{��wY���{�������/�}�ӏ�2��x�����Z�g/���X�����c��.��Y���{�	9Q��z�Ϭ��0H��ݗ����&�Ҩ�t��*H�v�U�b*�|�1����Kΐ���]��ԩt��*	�����Q!Y��-��P�A8���B�9���P�_
�h��̢'VRl��R�*"�N�p8�Ƨ�Z"C	IE���$ߋ֝,��l��Jn�U9��
Crh�V��@�p9�w�o0 ���¡���E��1$�V����r\r�11$)I����HKmsn���@�܅~]��3���@S��Rm0$�
�U��ţ��)>��H�Ϯh����|�!!��?��2��(������-��|�u�
C6�m*���
,�گƵ�.vzŹY�RU�(M�fZՋ���|��ͷ?=����7�ƪOM�K�Xohs���'z�Y�:��*chV�C�9MŬ�\�Du�+½�L�&Z�aQ}d�yQaHP^��P�_��.�=G
T����R��,99F���Xa� OX<�����9����#6�*
���N���$�"��Kj���_ƥ��W������<���i�`o�����a��~�XY���(�Z�5�	�EP^|y/�%�e[�]s�/+ɢC�(��F�&��ތ����jbe���Q9�@UaH�)r���}�Ϩ��c����SUOix%P��������e�
�OZ1 &/�p��aK����cHN^U:�JnI����QaƝ�����"L�-����J��e�c@�����؝DF��[�P�B|�X儕��nt��MU���ځC�    �־0@	JEݲ���6���'�~�Y�CK����2z֩�-�!�L[���ʉzմU; +������A.T�KM�C��PL[��&4*��!�,NFL[��9�7�;n�2������ǩ�:�7q��wb�Wt1q]��Yl�̓6m86�C��"l��GU8�8���b�7�n�����M�D��3��D��M_1��E^aȬ����
�E�5�2��x1]���J�7����tS�v�c����l��C��؈��:T�7���8�ѴІ0�����ts#��E-��k/>���j���\t�vS�v��g�)ޥ�(&"���̤���Y���@�����5��ӧX�
CB�X1��;�I�hW;։Iy������Ĕ���0[\mo�jt���}yZ%2�,v�e#^�X�8�q�Ę#�0n�8'iT�~�ŘS�4/sT��CB�X���rIFg�f��b0�׭ڪ*��pt����`�gTm%�����yz���\�
C�-��.��fݿt���=�t�p�+5�1�eC���oI�J-��e;�����ɦ+��5+�VX���ij�Aƭ8����JM�d*��,��{-��r��D�z�������i�
�FOM�t��ҕ�����H����Gm�9ƀ�H�nyEH燱7� {zӦ�!�Ʈ6�)`������TZr6ؐ��.cƐ� ^ԭ5ZT�>�(~�5�2�$y��I�S6�b���C��X��gY�l���wZ�j�U�]<�XTX�=�!�12Q{Fo��	"�(XĴ��ӭb��o�?��n�\�d_H�}�:�D7�5�u�=�'�b����ep�oߴbH�9�L�����BV�o{��eE�&K���Ê��]Ս�FAO~ҡ�k����`H�!��`U���p�^��N�djJEg���a���!Id�2tk�gȠ&1�p$�����d�0$D���}�`��o]dd�K�A�P���D37�7"' ԑb�3(���!I�"I?�*��@W��jOƐ$����f�1��ҡ�)
bH�)Ҁth8��z�T��r�%���h}�TE�z�B������^t�g
0ߔ4��W�Q�V[w7re4�mҶ�*�.g��ǆ��R
G�i2�E1��r=�8��C�`)ꢔuoK�`HJ)l����+D>jV�!A�*{)�Ŧ�G�y�a`����U�=�W�/j�ފ���h[K`D֫����G�)l� +��jR���QA�2)�4�$�v&\Ɛ]�\|���&{�D@f��HVbNeB*��&K��*�����l[�5ZN��bZF�n���[A5)G�4�!��:݌!)�H)�l���'+a�!)V�\4�达3cHH�	E���� �B{*��"�JƂ|sr`��3�RG��	��F�ĬE��� �3�1�2�vW=��Q�C�'D�*�O��Y���M�Ek{	0��ڀ����쩟M�!A^�B0�vl_T�
��⊔=�u�mkJ�_wK3�Y$����F��n����6%����χ��U�tsU��Q-Q��P:Y���z5�1$G��R8���~��jA��*(n��nlE��+']��[��K�LB	WR��À��t�.|Q�?L%�A$o}Ό�0$Ɋ�uwANl_�!9�ȑ�U5|�Q�uiE�"�Z.�4�6��%�(�G�d�r��1`4��h��J�Z�&lT�9��(r�b���`����ƪ ��G�}++�X&�+`=�>UB}`�u����*��a.���|~���/��T�0V*
�M����/{�1$�1ŹG����t׵���(:�J����=�b�#��_Cޤ�?��cH�AjtE��G�$mEG`���g�=mC��Q������Z�4��띢n}���K�uVs
|W'?���CÝ��|lg��Ǿ:���.tt���
���|E�^�$s}����'IXE��oڟ�1�n$ ~���xT|�~�6��s�JT�i'�1H�$*�b�?=+	q%Z�3��3�8�
C����˒��ZX�a�8W��ca/Β�\9i(	���`I
R��rV���C��J�%Df�B� x�q�up%h�Կ_3}m���C��D�_�����w������Vl�ï�|���4���D�TC�×���1$�:�J`�ϯ�8�͟ս��1 �l��J6j3�$*�����
))�݊G|a�VA�V�����~h�b��]�$cH�A܄�S��qA��}��������Pr���C�,Eei1$Ɋ�<�y!��`#�hW��ި�Hۯ��Ͱ��R�����*�\���'n�l^X�!1J��2�q9���
��й ��&)�o�M���[rH��$g)
� ��D��qtn�4��S��
C�Bt�<�}P!0�IQ�h��qEH��"�JE��k��w	�=d������׏?�����ͳWo��|?f��Ͼ�����nפa�
pR�������]�o�bH�!W~�탍�A0$�!���Z�7j�`H�!�OCe@䋶��X�Ӕ��K�p>2���`H�l�y3�o�M�C�(�Z���� 1$$��R�yX�;y�*IIEJ�Ӎz�/�n�nT�д,��]y�9'�T�d8�!?���-��3CRtNQ.�75�v	22���ɐu����be8��nc��FNj]�!YNd�i���.ɊM!�C��<��Mυz*		"�Ʉ?h��o��Y�o�+B\�"~�i
OZKɊ��J��/��R�D]�|�?�|���u�4��C�V�2�5��
C��L�ޒ��Oi�b{�0���2��9��@UM]�C��RE�UYg��!YNd�ϒ��r,;�d�Qp<��p-����7Ɛ� �J?����Q{r�V)�y���j �5i�Uכ,h�a$=ۅw����u����G϶�C�T��#\�B֞ޮ�i�*J��шQ���Z3;�d���ꚬN����9JА�6$�mǗ�k��;�d�SBUq�W��#�1$ȋ ַ/�ލ M|u��/����[�7J�iJ�P{��E(�v�V�3�[�:5F�k�6��)����\O՝0�e����Q���7cH������g�>�~�!a��J�aKֿ�GM�J0���DȿQ�Qnx�s$[�!QV��<8���������n��ir!IB�� ���N_�~)u뗢s�0�%5���u��1$�ΉÜ��Km�G=��n��"^��)�������� 	�+�(��eƀ���ԥϙ����dD���џq�M�Ɏ!Q��I�_S�>w�O��FƔ�3[!K?�v���'9s�{�J�Wm�/]�����n�JN�nӍh�+�F�)��LT'�j�~I���ϡx��r<l�PePv�Υ�{�Uo�ը:Z�*=�k��ݙ�s���F����WEڲ�,Ā,*_�.[q�6o��%�%�?� ��i�[��A�`3�ȯZ��Q�a'bE�Ԑ�5H0$É��\S�����6�G0$���){i�&�nǐ :;G~���o�n�V�DB�Bs����!AI�և��ǵ��z)f;<s�9{1NN܈��bM�÷����M��&%o�����k���6��&C1�L��2t�M���0$���m���K��?�-�QC&�6�n�ܵ}�t[��{���1�0T��3��*׶MW�BhJ��e-(� P~��_�w�� �Ui��i��.�Jw+<`��ʨ(#_L��:0����"N��/w��l�i$��/�{�T�� �ˇM�꓀ꓰ�c6���|(�O>��!�W������ڐTžcH��!�25|ꩵ��Ս*bf;���QY 	V�4[t�$�]Nˠ/���ݎ!iI�Ps�F}�ۇ�5�ntC5��(	{�54�>jC�h���Y���G;O�`H�A���z��>�1$�AR�� �!�Y��=nM�Զ���!Y�T�B�&���Wu{�3�$y��/ަK�2�:�r�l��a`�~��|�_�7bm�v)f/�4��)ߤ����D��R��X�a    �j�43$m�_�Bl�o��5��P��$Z�u��Bw�w�� Z�U��b�7ۗ���h7�S���qm@�������ӇQ��,,�c6�h]/�ڀ�E��ˀDC�0 �x�!9A�(zw�������n3�vɊ"K��&l��/)mz�O��va6��a���/���6�H0 eC���)m[�CRT��kᒔ�)�o��1�-,�
셜8�j��M�~�=�/K:��0$���Òg�c,œ��@57�M�<LRY��K���~e	�EP�`�b���톴c����0o��S���j����`�C&�F?y
k᭩�m�β�Ļ]A�&x	�e�����w��'��y�D����u֥�x����Z���Qs�I��s}`o��k(P�F�KRL��#���mO��b��=o؝)J��3�:�`H���\����r'%�,CQAf7�;T�^��9C�\��[Ҥ6bT0$��x�D��v���O���RC����3���U�j1�!IQ$�7�EYT���VC��4����uY?�A�j� A5V$�?
���C��SMe5�� �v�3��2���WÑ�G��[#22��9#r@,1E_XPyM]zcHP>�a}��(@�=�CB�IW�tꍻQ⒊��)Se���Z��v7ʥ
H5���ʀ:3cHH!�������7^a@š��˞��W�8���1$Kɐ�5w��e���\�!9t �������Oo>>~���o����W�CR�oYSH0$Ո��I������M�O��T[��Z���W��?{|��췯߾������bϷȎ!�N�*4����mvѠ����6�E����SS�o�����O�~|���7����ͳ�2�g�i�~|���?���?>�%��O��������|z�?���??���?=~����o~���vx,�]k��X�����|��]��߅/�9��n.�R(>����:O���X�!!J�r}��N�CRt�rII�/��ѱcH
Q���J�%�?�ٍ6k�޳H���x����s��b���ھ��F��o�E�g���,��U��[��9nX���V��ڌԲҘ� ���P+���b�I"F2��P���jbkvH��XSGt��g���1$�·�h�:ֹ�?���y�!AZq~�`���moƐ S���G���!AY��l�/�&�kǐ'R�X����ݮ�!)�H�d��/��>�`HJ)��͈���ݓn��)0���eb�2�Z�Q��P`�-f#����P&�h��^Ԗ��a���/�s'�
C��y`u��/�zm+(
�i��h� ����1$Ȕ��}�����n�2�YD,E/翃ii���1�cH�+��ʺ0{���C�����-�t+'���CE���bI��❺f�����c���3ւC>��w#@��c�֣k�LcF�1 ��({Xu�
�7��V�	An�kVe��n#Ɛ(-cb[95s|�����ݨl�����mD�fi��X�l���*�3O�%,�Jm�t�?��]���o�N��R4/	j�E;��"���n�$�d)�J��&�,�,)�����d�c���u�Z�Q���P�a'�����J�0 ���0��H��M��5ud������h~�hZ���elI�R�r����wXs�mJގ!QFDi��LI������-t-�Eڨ���h�d�.��;�1$�Έ��0�Nd���������Dw<�̒���݇Ƿ��k��v�"�qL���+I�Eʞ1xm�ll�!9� w��c��;7�������~����g�����o�����Wo��]����&P!~v75Y�j�-&��I�	�{%L���[�?¢1'n,`<��Pc[��_��ږ�g�W!���S{.��a�vּɀg��"�K�+�)���Ll�CL̍�!�gXi�HL��˜n��g�FN&x�&9!2M�+ֱֽ�g�(x�Q������_i��!hc�7�ml@o�*,|��0� ��(�]�_��,V��@��rņ��C�YD�]�aC��q�j����Mm�bH�I�=(jK9�C�L�t݃�}UwF�fEKǪ�<�}ۗ]�:6�Z�����9�ڠ�C�H�̰:�(;nmuƐ� �R4�B�>j�K�1$(Ae]dA����Q��C��R$%mx����
2c@�38���$Y�6����fɢ��|�aA��ZI�!ItX\k��m[%��#B��U�>�-:Ɛ[�\��ol�6�!!N�l��c� '�j+cH�9�҇��$G�'���m��s�bn��[�֦'E� ��%{��n�YƐ�T��6yD�o>�&r�D�NJ�Y�	��hv}W����,%��K�R˲cU���]�:	[�UuU�ǐ����I�r�]VT�߾|C����ԇ�ˍkR0$͋��R�/7F"��4�§�$Q����E�g�^���1$)I�����(�����ʠ���CҔH�̕A},�ߋ1$���*4g����Z�E�!AFq��^��}����g#Ŭ�#6ǧe�y*�{�	�9��M��un���cH�Y�FDeb��黮}w�d�aq�,���n_Cr�ȹ�jC_mvIJ"I�$���Fڝ��QW��_R�$��
�d)�%E�/J��S0$HA�� �(��#r>cA�.�^0$Ɋ�� .M^WKzǐ�|J�S���T�ֳcH�A� ��&w�����Q8L��&�ӑ�-?��z)׿9L.��5��v�ep����Q_�|\z�]Z@�u�tZ���������d�eB�����J�R��?�|�.�틮�[��M�V�g�o^o�itH�Ce���['1$��U�#�Ou�;8p��nL���;���n*m�B�;��);��B�f?;��X���?�۷i�co<BY��`l�gKs��`�!4\6mͽ�`H���ߥX�h�.7���Jc"�D���v)��v�Cr�4����{x�\��3��D���Yo3GQ=�q*�F��phB��
Ǘ���V�0$�K�o���XA4��]�� RX�3�?x.\aH�)B��ч�߆?x��WaHMY��d�R�O�[!W�������)|�JƈU)��`�b�^U�B��~�7��s�T��X�A�Q/���ƝہVX/�St|�6xx����!���"�.�K2l� C2���a��C�M�����0 �f�9!���ӧ9Vd� Zj���e�նcH�U[C��qA�d��0$(A[��b��P<�|�Q0���J�UMt*�2*�\��|$�*�oluSl�o.�CRb���}�*W�bι��d4�Ƣr]���&Mߚ�|��bK��z��pΕi*��h:)F�W���2�0 ���t���=���������Y�!i�H�G�����p�v1����>��C6aW�SW1$%)���G\��a�Z��:1 � i�	��j��\�I��0 ���j�12_Z��	���(�"��?�|�V*��E��'v\�9�#r���s=�������{���3���O!�@��t�w�M�NӤ�A� űu��ږT�8��}wH�.�|6�� �l<����p���h;�o4��t��&8�Hm4�q7��/�(���4���3�? N`ƛ��nZ�����ƀ����|����E�a,�X����O���:�O�Gs<��� %ҡ��ZQ9�MAC�,��O5��
�s�Ĕ�~�FC�j�$j����H�!�eH>"E;YJ�����FC ���@K&S�Q�h��R�`���5�K͝d}T] �6d����NN<�12�2Ttڵ(�F�v'ͫ}LZ�#�+������O�P�8�O�$9_�n����� �!�z��`���
��v٠�NL�!�!ݕ�#5���J�,������b5����\5��Mí��x�L>}��kYC�P!�O�򅍆 �!���@���NwC��2�������FJ������=pb��8,�!�O���� �Bf��0)cAC.�� Be��2,�A�O?�Q�5D�q?w���,��3d��W�j*d����/j2��K�EC��f�~��(�8��h췇Y�)��Τ�    E1%?~����,�EWJ���Q�sb�\��0Hݼ4��\|Ue�i ��6�z�>;�#(
 q> h�-�6A<C�@����$T�d б[�o�D�L>�~��r�A%�����^����@�[�pb3h�i��>)�i�2��M���4�2�����9)��0d�����ibR���`�C�,�_�'y��ѿ`�� �!s�ު.��i*d�ٯU'�� �!s���k�MC�Đ��?���@�@�V�U�=�2��_��F1f*�[LZ�9�]1si�R���!�a�Li��^d�!�e���1�s�P�s���b'�=ϔ4M�k�*�7��)��4�!Jd��E}�ua��u��!Jbʊ07�46[4�7�-�,���|_)�]z+�(����R��{X�DW�T���AY41Q�
1��MbXCˌ�L�D5.Ŧ�d�!��FLL��|qi=S���(���ã�'G8*�6���r6j<�(2���˝���&�!Hb������ъ���lΏ�q�5�N1dn��
p2s�dNW�����l41����ѷ̉o9ˌ���MT��G�2'��h��K1F�7�h�3;�����h*fz�gT���4���)�>��/�YC�Ĕ�qo�j�MO�>̎{��
j�23��5�25��㧤MCÐ�q�rR��[EC������[�'��j��g�h�Og�\>Wy�dA<C�>���>�g�B&>LPC�Ȑ�����ț������1w�d,[��R���g�(�(�\Y�%X��@�Ae��p���Fr��@��j�_�{��Z ��|�a��CpuO�]���PC � M���i�35 �����{�O��t���>a<c�f����EC�P1���l�~wRџ�����L�9DP�`ܡ�?ͭ	'9���i�CuzOFC��KA��>��@�A�d�˶n�Q�GӢ!����G��Љb4vF��@�A��Y�ks�M=<������5��[2wi^+�;)�T����ȯ(_Hg��T������}�Ժ�(TP�	ػ�x6�ͪXC�ȠI_�6� ���:@���B6��kY ��:&�����o���J��p@A�!���ɮRl�]�4"&��\.��!��h�f�6ߢ�-�3�FC4���g�|��"y&M{F��66h4D
�4�:w��PC�ȤK��-���I���A}�y[vj#-R��<S�X���Q�!�b���I���FC])�j�E;�1��m#�j>##��a0�6O��@�A���}]�N����FC �{j��~/9��M��ҙ�y�i�Z����
:='��?|xy|��������?~=�3\�EC?#��P�_>�����fu�x�h��V�%��Cu����p"@Q!�pY	�x#� �='����Q7\�I�0R���~&�v����-䮁^ا��#N�T�D*F�5Gr���u�W�h�Ead�RR�!Q��bU�)i4D�LJ��D}���cij�!R���՞�t���~��O�5�T��kҒ.H��d)�b�6�7�%��B+��x��Ŭ�^�T����SC븩��H1h��^1��Wf�HWPݴ<	f����#R�n{0N���v�BYF�&�f
���k��
����3���@�Ai�u*k*��'9:��hDŹD��
M�PEC�� ]���:�vmf�I��HE���A+�*�d�ϩh���ta}j�G�6"�J��>�`�hOV����ŃV��0��g+F�q4Z)��݃�(R��M/l�j��n��yM;Fi�n��H��j�)�;|�k4���B�1�������@�A�\"[v��WUT�h�4�>��I:��6")&��ǌ�����FC ]As�����.�O-�Tij�Ӹ)w�#q��2GW�������<d�(>��~����h�".�`�]��5	"/�(u��aA"C�,�:�!Hb��=����Ǘ����!%o	s����H3��4}�D�!�bJi�"����0z��(�RjOS9%��"��0E�y�D�#�u��gT i�$�o;,�i_�Hu��_p��$0k��2��.���]4D	�2�Zp��Q"S���ؠ��ڝ��F��45;��80�sgs9*�4���f�FCŔY���>��(�R�} �/��!�a
� <��]����T0ij2��Y�3�{��(�i�{պϝ�!�gʔ��w
j�+�f���;����H��& ����2���IJ|�=�O�XC�5����6e��p����2��:L-}�a��Aj�,�w#�XIuGQ7a�,�JۇxO���bԏ��h�*h�������\�y�J�uR��ƌ����X�Y���O�bRYC�52�=="�?z�f�!Jb�d��65�PY���"��9��Q�����=��QLRdV��~r�bifm� �<�x����{�T�h�Ĉ�%��N
{�nl4��Z�΀�}�!����7�����yQs؜���e��K�5�
̢��̒_�XC,�{z�D���Ql��k��B%F�N��jm:z�eӣ�PT�h�T�<���h��s����[�S}.ߝ��T�hk�d]4�7���Iy�����Q��3'�'�Z٧z,�2�ξ�R��ݓ':��~H=���U��Mj�9pE8�A����P.d�]>x9B��.�سe��Ģ!��-���o����'܂6d��h#(Q������ck���֠f s���S�L3L�b~�D��Ů��9霙����Y%<����`��2�,?ַ�8���Kg�\���V��H��3�j�H��h��v1V�e��(V왖�3*�gEC�� �v�:�Q��ڢ7�x�/� ��d�~�DՍ6� �����E�M3��_��;-k�F�b�����Bͮ��I�!�e�Է�j�p�y�!���9�ج��N�aITyh�%Ϩ��[�"Ӧ=�ZN톊VC�u ��g�T�^E$�<t�E�ЇZ�VC$UIW<C�ش"i&]��F?�'S�
��<C��3��V�D5��)��U��tw�!�c�ϰ����WR]��*�7���d�jݵ:�j�Њ��-2��c�v��j�F!����V0}�FS[��j D��n�uw7�I�f�Wt6%�JDפ;�]r�[1J��>^�4�����d��*.����pvh��jFѢ���	Z�eqb5Q�3�|b5:�D��W��pc�-����Jw'X�W��-�i5Ċ�ҕ��)�<��u},bQ���Z���<t�n5��Dg/zDt�3Q	��="���N�� ���L��i�b�.�+ў��OT���q�,7˞VC4
{�s������H�I&��Ѷ��Wҕ�%{6Q��Dw-͹Yn>��E�]��&U[�(\�i� ۇGF���@��@��i� K�mF;d�T�Q�3 ��*XC � �A���~��nR�N�$*Vt���W��˷��Z�(XL5�[�����l�8���C��k3j"��W��S ���@�A���U<T��I��@�A��K���Ll��"ÞԴ�=ʪ��y��@T����m�i�MCU!�Ƕl�\,���$�&Q�����<��E�MCØ��{��Za(��KI�K��0�i��".��,@����)��<���ߴ�M�6�����{|fM��ЦN
�E����k:�{���Kg����7Y)W�e�ࢺ����G�=)��݉�(}���P�HUP-���>��ӝH�����n�.f)ʌQ4�2�ʇmR�;ܓ8j�a~}@-b���7��ޕ8�jמ)�j��Sv��>ZW��[�n��ʩۗ�A�9�;�3w̡��W�7�nٲi���z ��:E�]vh�)W�����t����h�A��~�t�/�RZĞdt�.��i��0r��ȡ�H���� }(0g�:���5��L1�9|��B��j��jэӜtbMT��DS�w�_pEC_)u~0C��%0EQ��Y�7X������	����O,I(�9�=2���G��,�ã��q��z�V$*|�{a�-�{��LޝN�� �  �}���A<1���!䤓�F厾�G'�P9J��0�b䛨6�����!
E��S�r���sl��jDAa_���W�//O��ފa��T�i���0V� g��EC��w�Sd��ͨ��@EC�X/�8Õ�g�{ZrC������x����������-��~�U���*w�M�uY3�ͨw�=� ���ڶ̲�W����:	�,�x�gm�bfi��FK�u�/~�[�˝]�D�d�����*�8��;�o&�0���[1�b&:�o&���G�bc&:�W�~��q"s&:�o6�X�a,\9k�M��$ͬ����n5 �J��T��E�}�X����h��*i��5q��Z�4�Tm���9�"�m��VC,ìu�g��E5�Z�iL��(0�)'��[�iz��1���5`�F�8�(�
�I(wO�ճS�K��M5+(0h�(�'Op8���j��B�u�!�,=��!E�=)�ҌG�1�^4 �*ʰ'���uv��;�w;?����Ѩh���A7K�{�E�2x�;��TAj~(M����!E��N�dUQ�u�}�5DrL��u(V;?g�|%M�:��<�5D
L�R�@�C�YC�ȴ+�Ų�PC4�{�.T.l�P*�{��lq�sECU)�7`�ҽ6XC�E�:I�o-���-UL��=��&��Y�i�!
Ņ��Hq��j��~��9ʹ�݌�����܋j%Þ����2��o��!P`��}]�-7��w�!Vd��tՊ�K*��a�h�Ea�tIVZ,IF��7�K���e4%��YA����a�ɏ�][�4��i��g��ŭ�V�a�橊uƨv�l�:�V�,�{b��:HT� ٳpD��aO*�)����|�WP��9	�ȝ]Q`u��#��%��]��d�"�tm�i��'\d�|�a���X%.��#��=G�� �j$å]|d5�;�[�T%]�P�V���N/UI�˻��H�eKEC4ô�J���&YC�5X�=S)�\�P�"k�2SWڬ&�!��������}|����~����ei���f^�����j��_���~A�_��_�� O��2�d�J�M*��MM�]�Ie��'��px�ʩ��9�<E�*��U�@u4��@6�}ed���-o���>g�Q ���\���6A(���2�'��x�gV�ϗR�f j*��i& @�ۘ�+��_v��?�JI|)��^&\b��Kğ�D��2�D9�'��z[qT�gse��0\��p���h8-����:ٮF.�_Y�Y�+��g�Ea�]�m��j�29���f�MC_!�V�A3L pˆBMu]�ә��!�"E&ы��$s$H��J�p�g�L��	���ɻ��)㞤��x[L6ꆇ��VE���D&O�{wsg�*��aާ]��|���[V����2��+v��֫�<n�k�XCǐ<�Z���óQ��� ϷMݜ�[/j��qEC��WD�4�^���N�(��s�5jO\ѐ6dm,�Z_=�b[�f�˝��������xx���/?�����7�>�y~������W�/>=h�`Ճzx���������^���w_�޿�����߽|���Kz�un���/a���Z��p~n��!�P���~���;8�Ft���6�+ڱ����ǧ���������鷞��'Z�1	���*c�}����1$IO�c��ƚ}[)��Rwla��������!�b�&��샡�&r�8��ɨ��ظg߄���/gjb2����}��!������vkc��1�:;�^g�rN��f�g��iO}I'����\pS��j�ɷ���.в�8��&?��Oo߿� uÔ5DJ�T������w�bҰ�gm$Qa^%)��o�o�mm5R��a�(k���4�,��p5@(�tM�B(��[�5�2
b��cAC�7u�g�l��G��(�):S�o�l��w�_)k H�£O���$2d��ӄ��_EC�T!���;�Y*jM����ď���J�C1#o��+�d;�4 �p��Y���ԟ�*ECÐ�'�����!����'?T_�� �!�u����a����3Y�m�~�Q4ġ�ߴ�_WJXe`��[�"�J�Zߵ�!�>��R%�쵾Ӓ_JvٳEZQ�j�䲹�u�$v;�X�?��!�bP��(9Ȏ���!��=Ej�=	?2ټGcxH��!Q�j�Ӥ֯wOvT����UӞ*���X:ޡEC � :#�N�mwO�
`0���D��&LWJ~�fw9�N�
QQ�j�;�,::Ӫ���D�L}���'k��*E|�Y1�{w@@�v4Ռ�蜳�ޱ�٦!�f��b�~��b�m��Oz�|>�5D�L�.�q��ÝV{(*M���U����3�RaN����g�Ӷ�#�U����a�+���^��Ž�ȨO˞=�Ġ�cw���>��@T@��4��'�l����E��;X�FsA\X�r�bif�Z���]�];x;�ζ�(V4E|I��KQ�D[��,�R��l�Me7�
��"ݕ�8���u���@�A�N��� w��O���q�!V`����*�N�v�a��h�oniR����Z'.�aj�h�3�ࡘT�հ0�¶`j���-�i4�Q��m��3��,�m���X�Y�n8�n;(v���FC,���%g8��jc3���h4�q3��A�!�g��yMG;J_����C�!V`���D��e��۹3����/�B8�iac�]!��	'W㗊�w�x���h�î Y��M7�A��$�k����F�I��M�=��P�?P��[a,cf�`5i �����i?XM����0�1�UL�ʌ��h���yKZ���,vǾ�bQ@��!ꍆ0�1ӎ_ `�R1��yp:;�h�(���r����@�!�f��{:�[��]}8~���"���t�O���0�1�����}|+¸��v��d[��h����ƌ#hs�h4�	���R�W��x5h3��5Nw��15���|����j����B��yz�Շo>������^>?�}P?����?�?~�������ߩ�������������������o��>��ɻ�?�&��ԧp��ǟ�<~z����'��A�y�g�e���>�t�+��Ϗ���﬋&��O����_vs�����M�q>F}LAP���}�!�&�ʱ:���{�����z��2�{�����͏~�����$      �      x�Խ뒪ʳ/�y�S��>cF���~�=��
*��\����@E�"��O�A�y��$�
P�F[��1�:���N��2떿̬,C�G�GA�y����r�~�������`���{㙍�(N����W������P�������^�K���~��������|~�q�B���J�M)Cq[ԩY��n�9�C^��P3Is?�?�_���xC:����r�8�`��~�l�{�1<��]��l�l/kg���^� %H:��,�Oe��[�gV�pWk������x�nl]D9jH�k�}���B`����p�Ǳ+!��r�a�pµ;C4gز�H!����`Aj��:�ؙGv`{��yaPj�T��bƁ�����M����,�.�V�(����G�U����������
u|�Q���KH�#�h/8�L��fNx��W_Gt��ܡ��	������P�_�ts�b$�sL"�o���(�����-ʰ'QR�YJjM�~���ƈjzi>����A��sW��~9�*�ξ�����
F(���y4 %.�R8tg��+��|�� �Q@tQn(|���F]U� ^2���T���e� j�����i�o%��#�F��ţ�s���=Z�����?��@�EU7���8�&���� t���C�9H۞���~E��=Zg����J�z�X��YuS
ƾ���F�ō�ow*Sˁ�p�pq?py��d`?T'�H}����!k�l0��ӨoO�h��}���qy6x��od��$�����b;EQ����-}��-�e�����r4r�p9���o!��='��^������h�e~���������	Y�e�ݴ1e���3gfL�V�7{(Z���4��~g�|�@����@�<�N1�S�L͜�(pY���\B4�,5��le���@��V�S,�q,JI�e�Z����a/�n�k@1����0��u�6��I�B2�d�<�R�x�%j�jIF��Ш���ߜH8ؓ),UJ��җ��P�(I�9n��j�}��Nx�TcUW��^�nz�V�K�&���a�ڽbk7&���ؤ��ō� �<�Nӧ���=val\`.��=�����j=v��z�P�WMBZ�.����e��͡��@?[`
�9cXV�����e�EG�Vh��r��'�y�V2v���%QV꽠ؒ֝mѢ���0�	c?*���3T�� &��}��Fs��93�ݵ�X����p��@�J,�Q4�{r��.��,N����'?(��Y����n�2;���hF�7�-������[2M,<Y*HL�2a�Zd���a��
���m���,����E�$�G{�Y"G^`FwM=��4��
\�)�,�Tv��);>Po��뿒����d3�����d 6ڢ�6����&��@��?A`I��w?�vF$i�[L��y�������hT�{�%�����[A�Q6���7Y�Gd�ٗ�Ǡ�q�ˣ���#{��.�m{���A�u���5���ߺ��8C�T��iR���6_�
�fGY�y#��q�U�֜#}�:���}�n��4�3L���c������%�0��  lސ���c) �JEy� I}�w�F�Kct��v߾�r1k�+U��zغy�aЯ���p<8�S#�o��RDK����y4 ����D��/�!H���@�H���[�Y�P�8�7��EI��\�:13
|{�����	H�S�5ʍi �^C��Q"@D`3 s����@�Vi�cY"��՛����	�į�ވ2�˖�V4�e������8�����?w)*���<������.J����l)1B��,��Y°y`(g��2���f=Q|Y1���E��ѯ)��C�$�G�)wH\�����D1kf1��YG��P�!#�L�Ʀf��5ZR]��e�`��!��!Ԍw��%BL�S($Q�m��(Ǥ��t�z���и_�|
�W��C�<K�D-�3"yU��l�赝�R'vjo>��Z�ّ��=�[�f��р���	\0�4'\ �j�<��,�Np��>��M� 4�0K�74���W_g��Y��1�
�}ՏTeU�����0�7�,o�
�N����\�x�1�=��#�=�Шdp]F��Ղ f<n�|{��f8���XQ ��\����d-�˻�;�F�xwT���P-�(N{|o�'0 6�G����A�LG��X�i}���w�}׫ �$�i��ᛱ�b3D��L��;��B�v�;}7����ҳ�T
�(EN�ܺ�8����I���-b����@o8��]�������<�����I�m�z' ���Y"�k�����3(Ԏ�=פ~��h+R�MR�N���*�f6�1U��S���g�U����w�����eh ���SI-\�Ƽ\�4Dl������ʃX;�;����:��-"��?c\�W��	�(
�N!?U��+����j���9�1
K�$���tqc�#��	 >�f��󗡃b(k��@��UӪ}2^7������`���m�;�|��	��lñ1}
}��G��C��7��Qv='�ڱڛCO��߉;��Qt<u{�z����3R�@��eOC{>�����	�1�9�e/w����̲3)��G�`o�.fЙ0��Bob�|G���E)�̕q�`�5�-�7L��`���i��o����h��a���fE����~`d`�k��۞9l�G�}[a8qI
'X�{���j3���6��&DK;��٢.�v����vŨ�j�$�O�1�/� �<��\���4��	o%P+�QkB�8�ܷ2����aW���n��v̲���x?��U4ܙ�Z��=Z��`/�4h/i $�6U��������R��2�l�4������5	(d|�C��TMH�Hof^Mބ�����7��u]��RG���z�ߌ�٧�|��D�l�E�f�]̑\�m&����ʶ�Z;����,��"�Y�\�Y�-ۭqPT�������,X|�_
�p��c�b
��Σ���k��_��н�7�z�^1cΎO"��	�M�|.���r�/*@m�#-�0G���{�"g�Y�ݰ����Ӛ�^#C,��/�\�'�W#�֚jG����N]�(���w�]b-����.yz��O��0�f�y���h`tqy�Y�?��O]�1�)�xWT ֡��J�����<:b���� ����j꼑�4Ċ��gsՀ߫�f��j?����^���4 ��/-�U��l�!T���;���oz��Q�0��g��F]��3[/!���p�,ɋ0P|��ܞ��W� kb8r���`����p\��~��:Q�|�B��ԂsgZp`L��i����ڮw��<����R�w�?3����iT�NӪm���t9���9��EgV�*X��T�#>��^;�c�N߰]��5�����a�{~�<��Ʀ&p�f1b�� 꺡�G��ق�
��(�Mr(�Ԉ��%s9Q��C�k�[ݕ"�%��GϘ-��ɣ��s�R�}R���B�zƋ��!�V�]��`@�L��\l��1�,��^��p��J�p��Kn����h�k0������,8�� r�&X�0  K�k��e��b�C�P�rme�S����C|���(ެ�[i8�v��b׊��3W�M��>�R#%N��^p�J��f˅f�yX���C'CZl������eAp��*�'zf�Ʉ�ƃ�O�~{J�4F����P��Na)�+�{:��F�:r���$���5�5���<T�����3'g�%���<C��;�d���R�)��}��V��84�1�Z�+ʺt]�@] .l�5]�Gsʟ��&����&a�h��0nԃ^(:�~(V��-�8�G{�1���ݲ-`������1�¶�q�e���d�����ʖT��PR��v���Y:���O@X=�L���6�|�'�����{���R�FS}g�#iћ�J-Q��Dw����;�dڜ;^k �W    �������)#�cd�#Cn��v���j�,�.��a�����{�T�_�{�O}g��H�??�����%����jR]��.�����4��}ԣG��Vf�&�b��P��ڪ�+mG?t}z؋?z������/i/8�g��0���0��R,!rC��@�s$��ƪab�9o!� z��"
���g��*�m�q�6x	ƹ�.�~��(P8��� ��ǭ�l^��/E��a�� �s�*Y�qQ\���z� ���1 /�:�G~���3�����b=�́���_��2�ټ)̚LJ�xT�nC���]�G�8���������6���zQLa=���-
ϣ����ҫ�u��wqe��B�/�5R�%�{�]��r����Wv`0��2wT��������=%���9�:�=g��P���94g#?j���X�Ӑ��L�>:*r[ceڜ�k�EO��$��v�*��Bw>�+�)��nu	aa�G�3y�R@	ͣ���r6��`!��ϸ5���[gI��9��ZIU��vt�2b��,�\�6��\)���{(���4��	����GtW�i�5�A���fFu��.����YOn���ɐ���e���Uw{�c ~�M0�n<��=����-e(	vE8�~��!#w��ͬeרeSFY�ԝF{�["zS�(�6��V}3�S�O�O@4��dh 7�\���G��B������=�`<��	��1��)�Nb/lJ6䂥�EQ�umN�>7u�n��1�q�PW�+����Rw�`�E,[�]>�)b��y�r�:�t$%[������T[� 6~l�K����Q��(��!��a��bG�������~ �%,�����b`+b]��(�[6�+T�:��4�e�m��~�у �̚34 p�G .{ಏ��2�_ \6pi�_�����`�:��l��Imj�9�
ľ���Z�<Þ�`�]ç� �5��m�3\n��7���1�w��8ΓJ3����fe��r�ܭ�5�Xk��0'��)�G�Z������>�O�� �%��V�B����[���͋������H���[��Q���[q���8L>��%�G>��B������2L�O�΢F�T��r��vͳ�Ay4���5қ�� ���P-J�Y%�~�y�c�!E|l{���ǿ�'�4I2ɤAdO�-��g�4a�q����r��`�m.��W��\����5륧�4�9v:�4�g��g`W�Ea�x�;��b�����9:��׾��K,F&�Ԇ���]�C�ڬ�]�܎&s4��:U딏�Q���ns�*��]J� �h ��ػ#>{9�I�s�m�k�.�I4��ݎLgX�פ�
�����ځ�G�g#qSϠ7�"}�+��-F����=�6�Vi�j��41�+��*�	�F`(�G�F�P�3��p�D�*����*$���B����)-^�yU�d�4������8�`9�I]@I[�l[,X��u�2�U���J`y���[qF5b��R�o�ΰ!��j�ğq� E��@/q�@d�'���XᏅ�/��؆DA'�h^X���ͅ�ோxs�hڎޙ��Y-�a�2+%Fk��m�Q�m�RtI����3�#��G`��Y�j�gO�tF47��s�<�ϯ���}�@Q�H���k�n&,c}�[�x�.��V?���Z����nw���n�7t&��8�~��<����(�y��K����b,zZSI3��^�X����%V����$��5�Pb뇨�{F90�H`�5����� �[�=�J
�N�o�~�S�5��~5�����0��E��T�D�/�)����׏LN�}��/i O�%e8���yDY<.��y��{J�g�0��?�֊��:��W��^^�_�U*�*��{�	�ݢ�S�پ��]sTa��Z���}�P�K�gPs�t�i U%�}��O}�����is�yY��TU�����˷��L&��Rs>͜$��W�Q�1�*vѥ��Rg<��;^����5������P٘�`�z�E�x��G]t|{&Y���x��Cs\8�u<'�a��!O�p[?�Q�Lm��94���+PE��\ΐ�ߚ	$�*Wk�G��r�n�_��ݮ���eee,)fN��"�?&<2�n�X���p�BQw	�Ά��4Zf����W�a�/r;$��*�S_���pn�t�u��4$�b�y�)��L%{%h&�Qۜ��=x6SK�4�<̨7�k|��?��bC����`\��,#<͑U.1�����5�ܖJF�mFxX�Lt&(�=W'�lZ�z�0#NPl(��dz`ld��*�0��!�&�g���T�T��y�l/O��8��F��x���.�H1,��� ��ѿ!`�<~���J�����SVA�ٗ�,jI�C�^�f�j�R��˨��S�R�3j�����Z�7�=x����c�T���j@	|���3+`���F�|���'k�qt',zc�D�2#��|�u��ɲx�ƕ0�-��3G8a��O�?�)�}D�oj���6q�$(�ş�(���4Vdyjv{�}����
�W�uG�n�|˞m��g����\Ҁ��\Y���K�,�S�8�6�}y���~�jN�1��`h7LOx�(��[���j����W��M�}>Q|l{�f���!Fp�j�5����Kyӝ4�ʢ\'h!�8�٠\K2V��\1��xj��G}���"��4b[�\1uSG~
��<"�ߒ��v� �	����M��4�(������j�v��9�T��d��ƭH)�Ry-bL���sw�i|�)g��̃Y�ג������]�OS�}�{��8F08{���S/b��}�"�&e�!�r�����˩LA��Ǻ��� m4� 7�^��Τ��G��h ���f]���C�7�|�0��#9��$Gy�
���Z�ˌ���"8��r|À�[<ft���ռ���gl���Ǒi~a�Lx���4hB �ӊ����,�5�3��`�M�0�ՠ;�7�˭��pU�}�0]I���O`� �<�,1�S�3�T (O�Z��q����*n���X4�]�f�������?�_�2������F���O�a��
� �AX��
��HZ���67J�6�w\Q��R������T�=�>0�'�<@�����Q�#�#oͲ�q�����Za1���4>�xZ&S{^��g�nQ�M�Ȯ6�r�R��3���Ѽ�)�\����7��੬,x�dz�t�$M�dT�:R��n]�����&�l�`G��ȧ����b������St��}D+!�=��R{��S��;����r��iϔ�+6]T�V���ӕ`��A����x�[+E���+ůT�!��g�r�C�lf�ɋ/��l��T��D�ChixH#�q��Klm�h8�*�8��[%^�s��6������0E�r�v�5���<}�!=?tv�=�`�u�����I�o*����%���৤����0������+�[a����� �b���7)Ze�g�{�J{I{�G<�<�X$�~N���@կ�t۰L�,uL�}A�3+>	�T�5��ܬ�n�(q��L��Zߘ��v}un,����T�E��3 �<��=�So�4���g���Ah$���M(U��0���YM\t��bKvW�j��Rk��1�*��NK�`&�2a'O�%��۪�bH�To����`h����M�-F��I�⻼VB�"��C�.�����f�g�BAX�JOp
�.�a�������mւǛ#����*Q�C @I��m���a��}��R�n��m�!�6��W�m���������^�c�($Χ��Ԕ�7tfa��&��n9N����h������e����62	����o���?�ߧ�������o�����Pf���D�1�|� 	T��g�x�U�����"Y�_gv?����B���@�����������8��~�jC ���6�0vچ�;���:[w��U�m	� ,��aͯC�ujo��>��9�    G�;s�;��Wi�Q�I��?��Vu&��E|�T�:߸گAv�SН�r�|:�?��v��e��I�z��A��ˡ��'����_ä���mE��w����nS�(��ѷ���1Q5w��]�^����e�sl���C���	�z��1(�������=V��/[i�`��3�����P��+�L�_x���al���S�G"�����w��vuR�݃n��qC�w�iy�[���P��nq8tP?��Cx��$�ɦnG#�=�A�#m�EHT�C�nh#���>L�t�����(q��/i/��9��.~�1�l/
]��@�ōf����mlO��˫%�����<��?[�/s��������G���K|[:[�����q{2�bv񌯃��Cw]�[�:�J�>VlL�fG#Q&�qː'�v�'��#GN3�>P:��B�YU%q�V���O?�
L�;3׆žN�b_'��?3e��_�pJ����X�W���4�j{d(�8&��B2١d.��j$��T'��=�h�׋Vʾ���T2P�k4����;
���_��!I29�dbI��'e����X���X*����u�&��,��	�l���㹎�BԺ>_�K0�'��03.�?��Q�#~	ϞGn�G���;܅���9�A�@�;f��\��A�s����ӵ��w��s52b��A5~��oBo:bG�?r�eSh�s��间�;�~4��vj����4e�)��&$�W�Q��{~���'Z9'	�����u ?�L�JX�Ia���u�=���)�ն3��՞:j�l��/������>�rI��;q�8KǬ��X;��(�v,/<d��j��i5Á���ָ1ڈ�:V���E��c�v�iT_^1D6���9]�zI{��ܜ�fgR6�Tտ���vi��|_)Ro�L�~<Q��M��2��B�/_�}Xj��='n8w|5�2Dݜާ`��Eq,�:}�	��,k�%9\Dj�.�v�eq�c�N�s���9̶���`P8�G{�鼡��N�A!q�'�Y�� �B�A����~<��4UƯ|��*?�2��7]��z�-�O��H��9ËV4iu[;I�ö��~*(A�"F�Zi��R="�r�6�أ7[�i�>q��6�����D%�؎KJnB�Bq �6�Hr�w��_���0�l�Y�U�r`��@�l�z{�ܰ5n;׆�u{S�8���#IH���ؘ����=G�L�Ї.tţ�������^�m{!�Ok�?w�ax�;
�{����^'�����S����X�oi�?њߒ��R��N1���F�i~^��$�M��	���#�!H�|�l�%���8g�����>�� '��l�8��w���\�=x��5��u��������i����������`����wW%}Vh�(���?�L�����u���ƼV���gV�o��B��[F�_��9 ��-��
Ϯqe��=�5�F��8�_�aQ��SQ�i|��b��W�G��~���x��[އH5~�FU ��U�x<��ﯚ�}e~�����dVP�颽K�EdgE�M��8EV��� w�7�ԓ4;���q�PY���C���z��W�����������oΝ_(����i�CӉ�e�^(2w:�����trg�!.FOZ�����a��f-���T��>���z�HY�=������|z��?�-���zZa��ULJ{��촪F�9����<P���k
 ��4.�q��	S��ۏ^���V���s�*��i����?5��(淵9�f����=U�ܒ�k�Cӆ;e���P�'�B��A�<>u����Ǣ��۞.�7&	�ݭ왉�HQ���bp-�#�%��*�3����ل���/i/�E��bO#�'\G�~'P�dq�oM`ۇ#*��c%���bp����:��_,�wv
UyU���}��?^�Z���e��^;����P�ף��)�_�%0
�v��u��g�O��7T����Z�P�S�I.i/���v7/Tn�X��S�)0������0=���'t_����óﾷ�l�#�&�kssJ{��<y�w�������3�x_�`]�H����ӱ��{���M�D�>*�{c�j�C#�=%���������Z?=��5��2�K��,�_����`��t�}��1�&>u�zt�"<L�)�a��6��ynϢ�{$ϳ�5����~�O#��Tu��NN=���ÕKo��6>46��ˣ�$qwI�L>]?Mn�h�&�=��ᔅ�{����;H=M�z
���5�P��h���:��&�$�e��\����Lѧ�F����|��v���iF�����ꥢMn9CCv���"F�����ȣ�б%0'�r�W^�҅��^3uD�TQ�������d"��
���8a�/}���i�y.2[�b���PVX�p"M[��4QN����U�29hh�
�^��t��Mw�*xF�X+}Aܪl������E��?����)�99��%7�]�����<��;7�24IQiʣ����Xk8��sr��2U2����	<���J@�+��@k��*�>,���A��^h�j��	���Li��0������dN�#�31�i.��p4��$?�"��:��|/<�2��1�>W,��q���D[2V�
^����񤮸�������<�E\b����C�9]�y���x��sw� ����m����"��L���:ǫ9�\�7X��	����ͧ�@Β,���İ>��aCv�dOs�f��l�"Qz3��Ju�y����%?�`��q��.�&�=2'� �8���f�K���%Z��ŗ͟�@Q๷w>���8�&��x}��Y�_�e�{�4�8Ԣ�BIv;�J��L�R�^��#�W�ZY��lt�~��%m�\�����G{���^F�t6��
�
�KJ�	@|��.&�#%��/�E��g�V���o�ԓ�}����08�1ɾg �L'�*�,Jsd�\WD@�v��2�̀f����`��ZQB�=l��`��כ���%���Z).;#n�<���(~�?�h���YPL�ӝ�� �3��ԈFU���*k�q��q�O��$R�4f���D�?���yBY��.N	u���Yl�j�]p<�r��qi�ў�j�r��[Q������<]�pI{a�&�,C'���.��.M3w�q&��*�Yx�ur���Ȭ�j�"~=4["��B���a���|����A�cJGZ���b,B�SX�%��|���)���(X�3N'�c邹*���.̾����&�H�#f�0��g���^�k���g��.�X��o��r��	�;���|�WW92�����X���nXY�i_�y�QgbNHgFx�Ȓ�ȟ<u���k��^r�������0i!�V$� �|�����(6�P�(�TJ�k�G�=Mv�֥P�wͲ�Q�񬣄���k����Yvh��қ���f�Sq�C�
`T��)xU�i�7d��$;Q�1��:>�e�7�4����֓e�x�M��3�����+����dS4v�=?���¤���B�W�^�Ч|��| �ã��)a�%�%9=v^����d^E��Ǻ���E��:����E"h��$O�c�~o�r��P�I��h��&�>��Ξ�xT8�g��S0�}�5X�e�B#i�����$��'IC��qJ� Z�������8>�Df�Q��G�W�|e���y/���6X.��h9:A���l�͘���=kB����A����E���B���Xsכ="�
L텡�S�Y�y�]4gf��ro����D��BZ�7�/C�i|��옢�C��1�#m�gm={�I�ie��V1�P�њ���b�9Hh�bsh�����N�d�,}B���o�w��݉�w�b���M�O��5m��&�h���f�Л����z_.��^H��N	C���aS�<�iwRsA��^�<�t���"U�D�
�X��k_y�Lh5&ɟ��GN���H2�w����2�E���`��������f��uOk��قϨn�\��giS����2�̨L��������������
    �!��f���R��[���������PHY��+��YWvo�#��l�Ԩ���ֺAe�P��>ՉЀH��^���'U9s��.
�sؗE�����ϳ�2�����G?�����[�b��h���|���a1k<�6S�E[�����[w$N�[�<H,��=Ypj��;$�6���^�ć+��.�m.�(D� ��x��eV#AJ�3]��ub*�Y�?�?�Q`�Hq���(\Ga�$#�A%�M���2 vC;�+�Ř�T6���g���X�(,�jqxC�8����^�N��\�^X"�&�h6�}w~�^�EE�����p��BM�K�e�`
�|����;���`
�8�R��m������9��u��òtj���" AQSx�8�'O��X�Z�ʛ�{�Z��p��wvuNm#����;�Q�G.f;�E6s��M\�jM5t�<s N.�<L�y�5���Z�L���$If�2s� ��\IWhM�I�ˎ�nSa�5^3��dt������Ns$AbG7���H�]��d�\����$�!YoF��#0�����-�.�\�ov��ZK����8�~������]��if����oB�����K���s�D�U��P)�8
e&��= 7��Lh����V0|�QU���sS���t���y��RM5�
frl��Ӆ�z��I�#I��Rsk�.�h�L������$>�d��ʖ���Q�0�MR�?��!]�����T�Kua��S��>P&�����C�B �%o�`�E�u$���۱�^2NW|�Ћ�De��{�N���-M1�Ք]��~s������>�<���,c-��@��D�;��=���"-c��DX�h�n�ʝG]\Pf�9����{�Oj In�?��;� ��P'orq!;�Y�Z���Tl�#�o�Pخ0uTHR�m��Z����s^��b��g�={&Ow-_:,O��I�������e�����FQ�Lisr��f�j� �� -к;#������įV�#�˻���&����WQ�p������-�"ہ�=lv�n?�#�oԃ��Ql0�����}���W���k^c������^nOc�4�1(�24}��]MX�J�26z�?̺�ɹ�@����A)�a����f�O�O��tA��py��|�Y�����3d�T3��t��H�����|�ټQQ����M�����G�)X���z�zf$��uNJ{���3G���?2�q�ϡDf���Y�+��>�)m�������*k���2�J�l*ݚޡ�
�^�ڼ�<�0��03�{�l��e���?)�8x5���<�r�բK���t�Jg�o����LU�Zy�yzk�-�Wm��|�=`�<��{T�h���h���%��(�T���rf��:]Q^��,6�Fi��jŮ�1����۬6�����dΧ<.h/\^ԝ���yY㠙<���zi<Og�ۼ"]�-{v9r�5ۓư�'C�u�uOLS[�J��לJ��ij��mO�Ma����|�=�۷S��(�@�?�z��9�,�^1��>�L*�MEX#��wJ��щ2ޙ-�3?^?�9��9Ž`h^hE���o{�Y��)���y�-�w��`-a\���Q���ވ���~��X{q��3����.h/�ݓ�zV�T�k~1�Ε_�Ri��$��R�ZQ]��v�������޴�ϵ`Y��1�R�����g8���QG�0O�R��o�9�3�q���c��8��[P�U���֪4�Ğ�[�X0��Qv-�h�BjY^p��=�!~���4�anj�֘����J4P'�3���f����y�,���U$������@��j��`;�������(�4�^n�鬦����)I�(��9r?Ӌ8��"gn{Si�S��ᛴ*ζ؟f���l��^4�H^��u��i�ձp��W��9��/h��<J�%�(:�;��޵�`�R���|�tY3��*)�/P��$����z�4��86¸�h��.���ח5�9�r�Z.Yk���##L�+����힇h�z��h�n��R':��O�Έ�o��ܡQFm\��8����
��U4���c��,S����"H��/xb3<1�f��&�,�<G�7��;Hs��!jJ/�=����'�<�)WJ�ey���`��(~�Sv/`-���J�[���B�-r[�Zn�=�eQk�I_y�W~�'�Z�Ki��X�9gBOct3*X���>�b��$�׀F��ݖ�#���Ӈ���D���2q4]}~[��s��e���g$�Y&.+ET�EZ����[5m���ӑ����u����*s>�rA{��X�R�6H}�P�l�G�����x=a�K��Xր�s�\)z��W�$��GA`���\5د�զ:G�Ny4��.�����QuN�����	2�<D�AK��������%w=?���!i0���E���$j�EIȅB��+�bo��XƬfy�cVS��{�4�vj<��
[~���f��.�\���Y�?�O,C`���я^hx՜�A!>qZ�1�2�G��������ťqu���2Xbg�&��=�*�*l�ūh}��.P�z&$
��ں2�z����f���sU�5��M������׺�|$&��9����p��i@�7T�j&�)Ք������0�l��/�U:�0�@i���AFX=��F�~�h.�j������|(Q�����2_�
��� �px;L���� �7��{��i����O��W���F���*�����zBZ~{J�"�	����
������X�����i�>��z��4_<����
�lq�{�b���'@S�ڣ1�����cq�g6�z	��P_x���ԺT����1]*e�9'S����7'Ԗ%�V���e0�0���a�R�]����u�FȽUŝҥ��.Tv���3�M��W��`���
 q2lz�P�^n�c�]Y�M�����J�7lI���ZW�h!�*��v���p�f�j�� ���c��3�w ����]���`�������m��1^���5�mD�&�uQ��!�3�a��@?�QH�����b�=ƣ���H��G
�]��Q�LL#���k�~��08C���h<��^���C����]lq�Uot*S�譛숟�]�����X�+!�k��2�T��c[\"_�� �<����~Ep���k�~��A�\F捺�P�$C<[�2b�Qevj�B�{:A0��kJ�ю���9 ���(xF�$�A�y4 �\�/��u�c�{�i|.2���)�E��?�=^=�BǬ��Rn�b�l�D/�֭M�Ү�d�T�Qs���y�Au.0�y�i/�;�]B�]��#Gc8��h|s�4��0��DmN�j|4t����Q*vd/��Ze�2�di?G��vAn�+R���f^@�bImt���/o��/\<{�]����+��
8�����|�Q�#bg8%�C��e�o�L)	V��n��}hc��ĕ=Ma% �պ*��~�@�������:�	���U!>���%��b
:A���nk�,FP��z�C�?�6oRr���U����Gm'��6�{b�7�����u��Zj��0�Wƭ�hfn��?Ғ��}�
_L��S�U'G}�]M��)�G߽�R&H�c���2�,��Itؔ�%�,�bg�0|H�W�J��d'����H�C����Ӏ�� �lO�]��hv�5���bHri$��ζ�'���P��Sjn���X���AegaO#�KҌ�I�n��U�g�2��׌���Wnx(l7��Z�v��6�RP���:�����Eu�LQ�+�.i���PL����ƈqҥ+��!JS$���7$��f�c�ȭ�c����D�e~�K[]�x¾�Rs���||ʒ0��u!���̙�l�3n�]���C��~Y�$����zSR�Q��H"�Ms.{���	��t_527{�_�ˣ~� ͙����B�Մv���I�!���8k���%�$��P��vw�&Tgl7Z��`���x�E=���m��^0"<�yɜ���L���$��&��O��%P�:�¥    *6&����G+t��jB�^+�(��_�4�c�a�"�*C����dU�bP�Eo�iO�o��0k�`	_]���-���*/��ˣ�e��#u�>���B�K`-O1�0�T�8S*�{�f)��ߙ7�j�1ܚ���,�^�vh��A�xw^�a��#j��#��OL�ٰ�0���f{Ȕk_���=w�߱b�,��JY0@g�l����e�%,s�.�Ѡ�-��4W������Z��=��X*��b��B�RΘˣn��3W��j�|BʝT|�.��[�j�5�����b��S�����|&�>����U}�m��y,�(e�s-�X�S�2��U�m�X��dWD���(�Y`��kL��������
��vQ�oJQ��&b�j�*ϱve
Hi��<�,�Xz3'�^@!�N0J�j)�����ᐶJ����]vn8[)���K�Zt���nX
�I`���=���ϔ8��Ԁ���gN�i������0�L���������]���^F�b,ǽ�?�V��{�aw���<V���96�|)�y_�T�z� �;�qi8�z�j�5�Z/�MV�6q�)��Ba*G�����S]�&��{��8c\�v_��tS�dR�V30"�|�����Y��և���F\U���:�H�6{��Ց5M5�<�����^0�K%Ʒ�~4�4<��d8:���
���D�Og���r�,Z�O��δU֛x��m���> f"��<��.w�����s�Z�9��l�˦�	��0���3�b�r'�4�סacfRr{�����P�8�mm�0����ϱJ^k41���F㆓���G�΢ʁq�0���	��p�[8u���dA�;r�9%����@�1J3]��R{���pH^3S��j�����p0�nw(��L�����������/=�wƎGS��h]j�ݟ����.�0e��¨s��`��^�p�;s �<f�GwN1�5e����<�.i)�^����6��b���ۊ��+JqT�t��Hۗ���kĵ� ��u��^�xcx_�gn�'��-�Bf�����%����Zv���V8v�$Ɍ�T��K��g�g��$�� ��՟�'w����ż��LҖ6*���+��ݸvh�D�'�Βr��0m���3p�E|�tl�$��A	G&<U��j������$-�CN��݃d�Ͳa
��,DE�k�Rˠ�yg���
-���I����g�� ��5!�Yo�gjB��Q�LV_��Y[;�H֢��^5M�V�L[!��nW���:h.Qm����Ĭ>,7��&Q�+D@A���v@�m�!�4� 'c���a����0*X�8��l�����m�F��օJ�����;��2���
qz-��B+J3\�1��)3O�Vq��f�
��;v|zӊHH���.���\��`'9Z���oN���93�0��I�l��ɘ�)��v$��}����U#۸=lV��Ф��g�B�����g�C�C��-?������(p��@}�
�r�\3z|p;^�%�4`O���'�qk^�-qƙ����a�<*R>2�Rq�X����0xJ3�'24��FU����[sxz��ߟe?��8��3�0敨&ѵ�4�3��c�B�%b�g`&L�N�� {��!8>� Z���灛>�=xI=�P.��'�Ikl��u��̇�T�I�7�x4��]�v{�����Ԋ�1�	|�4������6t�6��8�c�YP��LFQ֤���ɸFQd�����b����bc�/�AkVv�N`�Xp�	=��P�8��	�^�!�M�ړ��a���ݔ�}=`}��k}��/yGY�7���>.�Ʋ,��0���b`��Ǟ9!t�x����Mv�;%U��'��ѫ3�!���F�͢��=mN����mp���V��~�A&C,�W�
j�z��E���C,t����e-�m3�P���Y0�p�b�Z�&�)n���t%�'t�� �?o�1����YQ;r�{`Z�0�wiD�h���<J.W��	���o#����x��g���a�q�Y���?���%w�Ŀ�n�v���w;�`�u��e�}��������1E)2uN�"�X��3��Y�����Mi�T�=����X4+�%)b����XP[���#R�&s�<C{��<�N�k�2Ҩ�����>^�|��PH'�c��,��J;�q��Z*6�F��Y�,��Ht�[�l���Q�֫�^~�#F�Kp&�W����~�z4߃_��#b:�{��0��!�S)IaV�H�:bi�-vu^���0
�J+4j������uh"���3�Tnr��
��\A�7��u���-�v���ޫ�7�L.Y
ei�Sq��c�_�Jj&�,���`�Dį��6(M��wR녚���c�dSBl���X��Jq��z9��8zKλi|�7fy�uC�u���h�Q�c�jb�5�BOW����ϔ|{ (�=�H����?t��S��	����?UęP1t6v�k��?E�F�+w�7�=h�K�M_���4��wK{� {ڃ;QN�Qr��;ʹ��+=��e:��$�'4���aXw�G�y�*G-�k�Pi6�����U��:!7�O�wM=�p+4d�(o�����qw���& �[��ϯ~{�TA	18����}�1|:�ES�\��[)�͵Q�t������ܾD���Lj�o��N&�"Ћ3gߝ�?�'�gKo�p�b�%	�����0_��W&���BqK&�S%�o�8C�ږzm���(2���Л�
�A�8�j�1�_o��Њ��,g�#�m.��Q�q�S�5l��0a��ܸ��d�=��m��r(��v)(#$�2"c0]e�-p�a�r
��z+_J�6����Y�!]���4 �<Г���*&wD�F���X��CxeRf�=�흐]0 S�UR�埾���a��nZ8��8caE�cm4k�V��jeu@��bKǦ�m00k�M�=�z�t��H鋀�3�4k�6뚤H�p��HnѤ�_�sq+��b&HW�%�L��f��'L�������9�
�&������{��K�I�!�?)�q\0�����S��8�J�k+�	4�לU"uq�bS�^l�Zo��~���&�W��xѶ��z�th"p��Hs��`��D�������z�w�p��հT�M�2��CΝf��g���3} Vy D��]OE�����W�6�pmo��ZO�7������vmM�*����k"v���>�cG�xA�ǉ PQPQ�_�� l���f�ˌ�BY�U��������1��!n48&eZ�/�9T��ih޴��:�LPP� �������X���Z�9���T�;��7��~�Y��ɗ�T֣�5��ܸfm�o��3ٱ�^�V���:��ǚ]�i�h7���q����ߍ�F�e�`�o!|�0�wPT+��BU��ۛ�\����fn#s�~�ߛxb�@�-;�/��W"'t`j`Z�[������I�F"��k���hg�;��� k*`��y�A��9���׵LY6Yt'r�-V��o�
q�Ó�^:{/��_�����z���/����Dfvg�.�FT���x.)@D��+�F��:��#�?$�P�	��uK��)�O�4w��e6��\�V,��b��-@��JP�� i���$��V��n�g?���:'�$����U'��(���B7�e��ͫ����`S�'=I���~���H�ԍ&��z���\��nNĔ�R]؍�h?l/��1�/N�l+lwE��QNk�;"f
w�o4(�2W��H���k[����G�Ed����Qor��f0��L{�7��2�{�YBc�µ�����Չ� .W87�Ѡ�KA+�Ň�PC�0C��%Ѭ�,|��J����/�EPi�{�*��� Ta���c6�8c��y��-�Щ��3����'6m����+i�]I5�g^�'�	C�t�;� ��߅�\)��}p�#���v���ڭ�QL�F�ƨ����I Jl �  �\s9a���[���k7�5��9�+�hZSG¥mW�ބ��4�ᔺW�	
��m�1����"���c����4��8���p�B����"��^NG�����{PɅ>�9�ڨ3jmM>�ugP����r��F0tw�fF�b(s5kM]�A�"��H�c $��XG��L߽wo�B�n�nN�eC�5����o��kG��w�ؚ��d\-_D:J9�
Y"7����t����Y�Rw���T��~�L ��$�ЉP�b�	��M=j�������}���:�����D�J��S��2� sX�hoW
���{� ƽ��rHsE]�h;�&���M�����?x�ɕG���r�Z�:�(B�"X����?�4F!'8��չ�
�N�55���<~NM|����e��
�7���y=��E�L�a��@�Wl���y컳$t]�D��Z1��6�s�NWQ�r��C�Sg�����. ��~��+���a��%��HW�dU�u�3Ƙ���Z���kpk�HmT����+ 5G��{o<9X�p� 2K���8�w�?'����Ѻ���V7�c��7�աv��ܲ`S���H�90���YJ��9ᔙ�^�����$t&k.\~G�䭔q��Zz8UX���9P3%S���,|��#X�#K���=))˝�ej��p�Y.���*��O|��������i3u���Pg�y8�J�� Bj���ǋe{g��щ,t�|����\��t��?4��T��2C��eI��:�[��ʚE���L�#|�)��|����w�b�S&89ے��jA͖�B���_�2e�ˌ�\Z��(�pP�	?��	��J����?��k��}�?�q���-
@0)�'�����˘uO��/����b��wH�=Z������Zȅ�V�������~Z�|�BΕ���U�X�U*��PU]��i��j��	(g���P*�p�w(�k�юā��Bs�����p�v3
��A�:�i5^L��1($���.Р��,�k�ўG�oS�ù��a�P���34�<��=�9ڌb����Q����A{{�s�6�Ζ���3cI|�|qޤ|#n4�{i�*{%Q������|��~�P*�|g�˹vMAgDa@��^<t������lҕ����w&�)�Q�xE|��|CЌ��2�����5k�^�A���|���"��T3ߞ�i\7� �Ir�:smί3���Zޠ���aϒk��k�;�08���� g�q׮w��ooEp*_k����c�0�ƞ؍��2��;Z�N��_�*�p��nH��!oB.��9���^��v�%(�������`ݞ�qL�Wj�U���z�3����.���$�-u>����m���3.@)=G��������ǽd\K�|rTD ��2]�rN��=�>��w����2��>�1;R���?n�U�h��Zb��]�/�A�"�:
�'"(|��L}V����9P�j9�;���^4�ڇIo�;�Έ�U��=BS�V��v{,��� �hP�h����-��/��W�i�b>���"�M�u8����y�s�C��
ݱ؉��gZ�k`%W~?/r2]䥱�|G�'ƟU����F�C�<�Z@�)P[�T�|Z�*���8��v������`s4�a���%�>�ђ�מ�3� IN�41�����dy�dho�'m�ҫZ�o���tl�U��q�i��������,�T�4���Ѡ��,�/z��tK������p�_@�ha�Z�A�=�8�~�idC6�1�q ������ȧ��hP,�F�]���G��(|����<�?��n�o�+���B:�<'�{�p(�1~Z�?j��y1��5���G����ho���إ�FPX�����5r�̷��� � ��4��P��_y8C��i>�j�ɉ
��xv�����u�XM�te��V�-q�.��UT�DZA$��h�#N�MP��8R������8�Y��,�BA��#P�n�=��H��Y���6ӄ�xic��qg��<Y��ѳ���f��O�۫�U6���a��3�L�8PF�L�ڽY��j�,��!������;���}��A��+��j,7K�Y���ԩ3}��&�p�{t#�&�-ǵ㒷C�<Q_�%��$��VjGe���<�7�.Az�ˠ���on�IL�e�d�7�f!Zg��a���:�6w��ɖh��"	��Ǖ)��ZU�lT�|�^�G�<~�%f�Dm���Z06@�v'q�ؿ���` C��ǟ7��j\/���m���q�o}�sVJ@s��PeW�J���B�XxJ�oE�z����9nbh���hy״��/��c?��M�Q��}�*=�S(?[��k
?�.|&}�V����fZZ�	��eP>u�n6��8��!��W�kb���<?����v�QV�Ck�Ay�+�yfMw�k^I���Fb4¡��rr�ѐհD8�և�B�/W�嚓~�O1���{���J*��8�%K���-g��v Z2���ՠwO{��і�����Dʣ׾�J�*�#A��7��-�2,��w?���tM���ϾN�z���c�b8<5N�R^f˭�I1$hQ��.�nE����!��kH��O�Ru�%ͩ��1%��h�.�r����.�ڳ2ၴ���b�ho �ơ�Inѹ����e����	�R%�� 뎽�!�&���}tu&I@y�h���Z�𷳽���Զ|��@RY�������ٚ�={���H��i��t���� �C�To��z����|9��4*��q
486����8��Z�8�$kΎ�U��}��z�	N)O�Q8z}�JN4�IH�ڳ\@M���N$�5���\�=j�F�Cu��
��� ��$o/�o E��m�&��R��!��p��1�QWž�5�r��9!�+U�<��-y�wʒP��*�f�@5�po���]N@h̰w{;!X!G��jW_���t?.��	y"�v�`�ݩ:���.�g��l�%$X���Rl0]R�(	�ި�tL��*\ ͅ��4++e-���U����J_z5�'
�%����ύ"��>��8Od�h��̠��.x�e�w��wu�ѻؓ�EA2�E�]����b��ƴ'�o�fcrR��r�K��i��V�1��M�)_�^��_SF{)&K2�];�ZTPJ��\�UE�HXG�uK�*���*�'�B�V�� Cr]2d���e��Z��՚���dT��R/�Qr�����!ճ�&��8����`l�=����lO����:ЎXe΂?���}d�a�� r��ir���ӂ2�h�`[���Fc���^���
,�S �N�����4�P����ϊ-M��4c�C����!�4a��n����T��׏+�`�����Ah4�L����%��tz�8�SINB���������Y���3��笠zA�P2hv'�J����.: TV��u!�	��G������q�-%��r� ���[�WuÈi����yP������u���6��4I�H��E�e�7Pz#��֢��Ш��W{�.�X;~���_>���!����Ex��}"TN�ȅġ��@o�{c8iN�>P�����@7��Qm�ن�'k�BH�*c���eio �E,ٍaWS �T;@� J�������:��3��6|Ƴ|l�%�L�����cǆ�i��q(i����`�m�F�fn���y��Dl��u�A�t6�v7֨�щ�l7dg/�� ��L��E��������_ʛ�9      �   =  x��Y�n�6}�|�>�)D���-A�vw�� ɾ��^��jK6$���}iY�,ZN� F�Cg�s�3�3�;�ԥ���K��{|��Y�_����o�_��Ən|��0UIF�rʎ������{���)�	� ���R�V�|��uy�+J�HU��H2�_�x����ą�2\"�Ȫ��/8jk��Y��;�CJ�Ö����ݑv��{�֗��!Q~�Fا��e�����I��R9b3�,���J�0JX���@��5��֜�u6I�cϏ�F�gQ4#Hc����\�J�0�E	��e_�ES"�������{[z������!d��.��`�ς2����AZk�0h�/z�=5Ei�)r|BF<`1g�U�/�s?Q�cF\�X� i��ҁ��l[��Eo�x���.��bq�x|�:�!��e���n��
����)!>�#�a�l�����Af{_�K�i�l�OmB������������fJ�HŹ�,F<b�����<���hq2W��)H�.�x9����G,F�g	�	��JG�RB|���\��H��� %��-*�K�M9$(+A�$�	!�#��O�=e�*��*��5�G�G���G.�P9R��Y��02��^����V�?���hu��)EM��*������lP�����0����ޚ�2��#��ZW[�f�1g��2<�ښ;���M�0�#��1���y�Z7���|��8��9�`�y�i�wF���u����{�3�(?�C����p̴����#��a;��ccXg�?&�Mqt;��<6��r���.�p�~��b�g��i3��5�769��5�m����؅�9R2����^L�"C3���9�PďQ����`�iB)\A3�eߍ$\���O��h�����y�����q �?���|��~�Z%�9Nj�	FP��'��u쿼��ޡh,��c��|\A_����>�����A�r�=Tz�� ��yj12*[w���K��a6�'��\Y%�TE&���mW�ʻ��S�
�υ��R2��s�'0@;Oq����(Ɉ��Z���Ꟈ��jr��)��E�^����FP�ˈ�$�s~G��t{4.�#��%�c�s��Ʊo0�v\&��>m/��S�#�FЉ+���N�9�`չB��� W��?7(�(kS��ϛ��Ȝ2g�0��3��Q]!���f+s����{ #�-B�FW��w�)nB Fؓu�#s$�*��z�hT�-F�$�}wOU�p�cZz��[��A�n�'A]n��N��P��y}$S�1�M�I���9���� ^5��a��|�	F`1�7j	��)�������;�1�^��ɍ w`�Q�02�G��Y��X�{B&䵽�آm���x�Ff�G6���5�CsAi���(�>�n�#{���1
e(����<�.�ңZKF���L���Z`�Q�<�K/���Q�ʃ�v&}�,=�n0�ֈ�`"��f��(
.�Bd���������B�+6�^�cz�Q��s�TJ�0��17k%O�k-F\"�f�za�a317k�JՈ�`!k�ܬ�a��b��y���������v|_v�b!G�j��LaTwad�L`�x#�q?b!����,�����f��[E��������������E���Q/߆��24�׼��T� =�(67�(	I7��j�g�(����ѭ'�(.�bs��+\�lbn��sX�b)U�`/����I޽�b�d�����Y���Ⱦzxg�0T������Ű3���El�	�Z��(�������w�Q���N�fZi�E��12/_0I���L�?2)F�JD�ӕ�      �      x���[o�F�ǟ�OA`_r�}�Eo��{lOk���'Vf۲����Ͽ��H��fId���������w��H�4RZG�/�^��E�T='g�o�Utq���s�o�j�!-?�,Qv��YZ���8�>��'i9S���i � �G��)��b��Hk�1�滯O�c��$�Μ�i e-�u��|�8���]Hmt^}�Vɼz����g�{�{��\��e��3��T�Ӏ�[��M�W]�N��LQ������J+��ju*�0�6�E��V��j�̗O��DL��U�\=�'��C����b}����]���=�p��|�mv������zS?���Y��4�bS�Z�m���/맇�jY=m�'����?_ֻ� W�3���E@�T��n���Kdȷ��5�;���̕3���ئiò(�巺���h�Q�� R�Qe��}}��X�YY���/Nd�6a*���<z|��;OՐ�Юh�Fem4�S�C�n��\׫m�����n��O�����4C����
����C�5?�H~������(F�(��5�-�s�4�!��(�����_�_{)*�/Ф+��b��0�Y\����>��ݯ�=�e���u�%�lc���ծzJ�_^��{������ͪ�$���f�9�F���S9��?�?�)��.do��@)Z�#�݉��.(e������^��i�L�#�)�B�֔<����t��4]��^���0Χ���3텓 ݀d�3IC��4BX�`��~���Z4��4DPH�2���S���Ey�F-�>DT[e��ɴ���(�5�T�����例�HClF��Gy
��4���tD�7NˉV�±|�_�JɔS±qh�6�PBKt�[O�wQV��ׇ ���v��;qOCl�JB��)X�@�!�@�Q?�!�A��}X�a	#�0+kY��-���·0�`�T�d�P �Oi��Ǘ���H^go��4�B���<���u����>U�����2Y �C���_��E0���^���Oe��Q�8�P�VȢ.�i�OCdKr#��̠c�4pr�(�ѿ���i���1C�\���v�NS�}g2�
��E%㔜b#�h�|(�)G�2�XBbD*T�̾�H�2G�@��hYKK���g��Nr��R�4�Z���Q�Q�,$����X6M��v����8�����4�G%����PCT��$yT�Q<a�ђ��7��oҖtD�4N�_����=y��'D��L]�_���g�������kE����(�a��Ӏ@���U�,����1��==3�����6���k���a|���?�L�j��9��
�<��(( �	w��ʜ\r�FX��8 �f�r�*>כǚ|��q�]/G���X�a����bJiX��z(S��������<�p�O�d�d�0š�
�1/���f��o��ag��ASJC)�e�Q(5N�i��B);s�k���J�����
p
�+%!�J�oz8s:������Ut9��u^�`�4�(N原BI�kCA�R��`ݑ�i �tD�2N�(];y��f�]xAH�%���hѤ�+�2�2ttʨf9��������|�!v"�iiI��h�"�d�QXQ�D�}at�E���f>�?-�v���,��m�|\�V[?̶��V>A �G4s��N �P�k��b���B���'s�.Z��Cy��4#�&��#�(�� @И�zy��~�V��ɢ�c���R��\�}z ����q
���n��n�v��5 �������y��";Fw�v����˦��ꍿ�>��&�1�1�O�5�l���in?ra����m��BRNu_!5o�?�bK$a
�i%�dn���C/�}Z��H̺����H�&�G7ȣ���x�魑j���RYN�D�Ƈ�.}Z���"�m~�9��gɧ��{?�R���,u>-V�X9�}����X A�a���B?�;-FW�8Ua|��B��6�=�bUK�� ��E�ȳ�� @w�1C-F?@qt�\!�E�sU?�^O�)�R�h����퇊~�P�������0��EUm���o^D�v��e�8��@ƴEx�|Z>&W���� S��X������-��-�n��*Y,_�|t��h����"���n�&��� K�8DE�e	Õ����V	�.��M�Z�Z��[�mh1�0E� �ˣ����a��W��i �*c��ٖ���0|e@�#=֔(a��1eY4��0q���|�5��g���F��E�V�z�t�'�(�����a�H��3�s2��$������Q�e����}��<�C�.4Oђ�A��j�Y`^��E����.����a��{�P��$�,�����3mt_�%���C�F��q<_�'+�9<(�V��"�����Z��,�Ȩ �&�#����V����D�h?u{�v:�ô��F���tZ�	�	E�˧1���'�0�a'�j1���
/�N$Deg R��DAB�n:�b� �~$�3�+!("�2�4�=���gkqf�0�� ��ڧ�4@pDeE �4��p�Qj�Ŵ��H�b�PA��q��FQS_w�m�X���n㛑���%��l��#+�l�b���C-�ᮙ����Z�k�����t�&�x* ޚ�ҡi�Gj|Z�;� ��0#���"��Aq��/�|Y?��D�~y\��c9��W�i�N�i��Yt{3�t\Ff�|�]�zVN��Y��¾�Wjq�2#�>.F2x
��5l.��ܚnk�@�M���aSBs�
�Q%��AP�iq�2qB�ha"�����$mdH}Z\�O�N2Z������$�t1Oqh#>�M�����Ѽ�6�������{Ut�-.3��@hBH��6�F�K�K������i$K�*h!���ҧť#Ą�L1��͔��޶u'E�NC�����@����iq	�i~qQmw	� !��`c
ƴQ�|�LE��c4xw!5J��u���4e�QV�p�qE[Yz�BQmL8�	O/�5���ԧg��4�������e�qfp�qet��T��j��R���P��SO'�
-��Y�NA�7�׊��N��:\�5���+�����}Q?V�?j����S�;�!©=8���s���A�H��an���Љ[s��CW��4��Y�ci���N���i��\]֠�7Y7 h1�ǽ��{Z��IǢ�ޑUb}�9x\z8T��3 �'h�ͳ>��0m�b��3=��yN�e>ň�v��֫q[� ��������=�5 #���@6*g����
�Ŵ��yc��W��>�E����]O��b:,Xjqބ@������n�>�b:cH�#�& ��a��{XZ-��f��Mv[���a�1y���3�=��ŧŴO�Ҽ	��ùĖ���h@䌐� ��Z��mq�}/-�3�e)C8�:�I���ѝ.j1�7����N���UoZL�#)�1������(�1l��b���Bj#��-���G;Ƶo�j���S.eS.�;&�dP�|X.���T��E�|ZL�M(r0ԡ��ZL{)n�S?�FY�A�M��Noġ�~����8*�B��ΧŴI� ��O� ������0��o���E4���[��ZLW{P�?��~�8�c�N�i�*3L4?;�7�/�q�$�h@�Ü"�5}|ʺ�c��*f��b:J����i`(f�ytvw*�\�I�Ȼ�'E�ވ���A�KEz���A�ZI����vh`������Y aʙ�>��ALx�4�h8Sn?�ê�t�,F_�O�U�6Q�B�pdi
��6%H�zﶬa�,M�m8�4@�I��4��鋡D� di
p���i
��B#J�;t_ր(�4Mc:1��i5���۟���>�G�g��I�-@�Ƹt��%րh���t�����뉔�̚4I!R���B���p���C����h�/�Y�Њ|���qJ�	=��j�G�P{�Ě�,������9�'�ҧ��R%)��G�= �HA�Y�� �  ���uRPw��P��P�bP֟�i 9��S
ʻ�]@�h$[
*�j�@9�����V��B��򰲶@�l#���0m�X��-�Bm�9��5����� ݟR�i i��J
2��O��\r�=����N-���l�s`�D��ĞMt�%�4�2��T2�U�5�r>�*e�G�T�=�r�y�4� *�./)(ߌ��u�V�ٹ�!��4ߘh�]x��C�u�N��E����|㢓{vy��Z�W9�g��ٺ��uN�ٴ��4�2�!Uz���M�N��Z��iY��2J'�l�l�i1���Ğ�K�=-���س�~�G����
���vd�P�{z��z��=-��	c9>��Vߒ/��jl��4�48-��v��\�^~І�f�U������\�'b[�l얦��Q�����-�8&�!\��$e���3%H��ϯ���o�ԑ	��'J�8����4����G������@C5�)�$g�FH�� t봘�U%�Ș �6�"���������g����_��|"���
�ʻ������\{C�a���K��\�NK0E���c�B��d�e���g��|Y
a��u!�b���j�|�Q�C�>9�|᩼����HQ0�1<��_�Г��!υ��A��i�d��|�[�X��gMR�"KT�S4���v"�s�6���`ƥ�b~��(�`cJ���,כ�V��qw�a�!�6��C�P{���ȧKYF����@�E�Q�k0G�\Ƹ2��h��y.�cSf~;-�k��##��lp���Jb.o��R;-��A#0&D�L)�Ϗ%7��z�}&H����hɚ��Y��|C&0�V2DqE�J�8��\u���̧��`
c$��L)��|���Z�7@Lcȑ������5]���)@Q�J�iƕe�%��<f�((
qȥ�,���<@��t�SD��d�&*��s�*� �`�e{��V�Sa�������[4�b���0�qC�C[���/FV')�1��R�M���>:�,C׽�0�b��1&��`���)��!�kQ���1��j�E_HR��b�ܧ��.cD����'|�U�Y�"G/�I��i�N�鑠�E�W�g�/����eů_<��Nw_o���f��[���[���t���6P�%<;��Z̗�2FZ��mQy�׍������&apj6D�+�+�L�b��F����5]pV��N��3����]�8���2K�Ǒ��5�p2���u�]~��E���[ݜ_�e��	��õ���ػ0f�ņ�<�f��?/���׷�_��,��~���%���ex����o�����#;2&�T7�0���s��c�@����(NQ�pB{�ð�ՀqF�h^���f���4��E&����u0y�Q�I�G����7qy��o~�K����g�i���cMWe�r?77�ol����w3+���tuk�� �
R� �Q{�ņN�?R���l�ǿ߾�+�|8t�[:��~��N�6�i��nE$��B�D}��KbWo{Oö��e�NPw8m�����i`d-�h�LԎ��첥O�hy�8;�0M�W��R�ߛ�V��Z<-�̒ �|��,y_�|ӎ�eSf�,��7gKl�����-C`I�B�;т5`�b+'k�d�ǘƑ�N4����l�� )r�Myq)�Iw�`��p�9q���������bH��4��^���h�i����1'p&p��7���c�Z��O�D�_UΦrƫD>T*su�c����5T�6������.9"S��ٛ���4P��z��r�d�8Z�����dOY1Yl�(�J,���o��� �i����n���0�,���������{�h �$��B��R�UaH�O����,���h�䌑$rT4�#9�jP��)(?��|���ܧņ�0P��/��n�@�ݵH�����[=��d[�/���Q)���ʺ j��d[�v����o�nt��?o��PeN�k�K�������o��o餇O4�Zl�ӎ�Ӟh.�?�����١��"v��<�	rd[o�K�S}�B}�eמ[zJ��͑��ݺ�&��O����1�� s=�,bd���E2��=I�L�ɯ�[
i��'�T�y��h�QM�
P �$y(D�����>��X�50Jf�^
p(�`[����&y1��x�.������_ �i�8�_hp��s��㴌�LR�������P%c�I��9=(���y�n��P�`��E� �l���S�����������      �   �  x���M��0@�3��G8,��c;��U�
���J�&Ai����J�o��*99�(o<�O��"��c~��۰m�F}l�~��}���?��6�F�m��b��ĐFC���U^�^�,�;x,,�Mӽ(ɘ�B�������4K�A���m��/���`�Sݯ�l`�cr6iWb���~�"��}V�'�������~�[����28{ѩ/��˸�ђKڗ��Zc�C2�a�6�a�Z�j��J�%�$�>�ֆ$}*0$N��2'�JɃ���(��PJ�X�g���<w���~��&��D�މ!U��|oHR�C	�x���֧���h��_�^�)�/1�-��K5dO�<24����8�����z�kZ��^7C/���q�ԛ��o���6��o�3�J%��/�j�eݯ��>��?�%:Mkb(1\�rZ1i.14��n0�OW瑡������ħe�� �\q���Okbh*���i�{v�>1�pˇ[H�?�>�O�1ra������r9      �      x������ � �      �   �   x���=��0��z�� ��ؓ�n�6�v4SD�)��"�$��[<�d301��r��U۟|��@)Q���|�_��q���^�U�9QV-5��-
�~�Z{?��(�K��XoF�Tj��v�{���)Z�oܘ�rZ�ƥ�3��� 7��� ^g����ip?�8�E���DNZjhKys[<��'�Rz+3e�R�}@�+Ns�      �      x��k��ʶ �Y���qjNH�ջ�|�"_}:TTA|���	>��*����s��ث�@f����#�s��s�����6VXMWA�P� ��?0�r�%&xӅ������~���s�X�Y��:#k㬼��x�ch�`h.�~���z~���<X֪�E�ciW��E��o�#5� �Kl�q���or�\��(����7�FP� ��,X  � NT�u�u��
����޼�����9�eq�bb\I���!�)�v��F[���3Խ��B�=rM	��1�l�u����&���;���
�.��B	V�h����n���S���b� ڬR�k�^a�l��7�<��B��������2���ɒ���������p��]���D�-l�J͠;�Őt�}��֤y��!�t�Y08?�}^U
����\p��b��b{�(�<��9�bM;�F=���5�����0>���i�! h�A6�0�۾��܁4�tk�5���M���!�(ϙ��/:�n�JU�;Z�.�luWr����W_֎,=�"xg�. �,X�mH&�-{l���V�Ū��D�)`�ܪZ�W�а�D��7V�j�:���	�rYP��
kHMC5tLQ[�jS���Ct�����4%O6��a����N���4��s�:�Nϛ�V��~�Jp=,�tm	�:1$c�y���9��pK��Z�������P*o$��8�]�zb�h��W�}���������bW��~kT�]L�Zn�4�^U���(�(|X�_��x�s<`y2Y�)��+�(�,z�=�(���bzW�+M֓�����Z��f/�����
9d�`a7Y#
�B����]	a�j��[�B�yⵘ0R�(�7��z�+���Kw�
��&|[M&���F���6?B��q8�������"9����p��P7�c�K�����	�1��f�/w��̘��)�ϕ&�+������1T�@J>���d���w
ς��L,!��қ��Ƕw&d��C(W�*�`%Ⱦ&Vt6���ځ�p��B(�!���1V�z@"��s�)������6��lLq-��$-g�������j)�b�.��x�~��Y��i<��ݍ�L�����d�֮*
�n�)�B�]��e���S�DbXص�I}�{r�31O�+�͂�����%d݆�[���(2�2�XH�X�J�,L�*"�Vc���	�����!5]�W����RS�U;�6Rd��/-ˮA�%��PZ�d�fʼ|x�M����z����Sd!pD@��l,8(��,�u+�����ԬЂ�ك�y� i���cUPP�`�D[E
����[c?X��0�����	w5G��^����B���؛��oSZmF�MN�Ÿ�֤.�5.
�6a<�N�=Gs�y'�,X�sԽ�&(-�:�U�IA6�D����$<\A8v�	_��_�$4�oP�T����*5E�C�+��-�\�Fb���R��x��|7Ɏhu%UiU_�q��C����p����A�=�o�9�v;���1�0���_�6N���[�G� ��'�	���
�l�i-��f���a�w�h	��"Ւ���<����'��E��x,O���Y*c�Na]��c9
M�)\��X�{��[�+�;���zV2�B�Z@�&�L�պ���-����bËaq4іʨW߷��qx�1��tX�M�!�
�'�-��F�g���ˉ�wZp�p����R�H����O?�VC0��X��֧�
�t�i���8�ؔ#�~�� ���F���C},>����|'�,�
4������x� �6�)h;�,񅷂H�4��ڧ�J���Ck7Y��j�dIh�� tW���n_��}c�]a���w�x��W�.
b0��g�Δ�����S�,���/�)2;M�ʄ�2��ڟD����t��j7�	�0��Bl������?:)b�8]��?w��ދ>Mi`g�l�:��3����ӂ��K�,�x�e���e+)��&Q]�]k�asA�T'�w�9(�!Q��\�㶽¶�O�a�;N�SD,(��^�$4[�č��5��f�ʍpn�g�7ã9� }�R���5�����Ҳ/SE�Xq��N-��F�CN�u�qe^ҙ�+�� �`y@�9@��IP���C�C���Љ?|���Ð���K����b�I �%7E�J�s|�wĭ���
[�*é�9C�.T��AR�� r�O����4��j��:�Q�b�x&i�lȾ���q�^"?��uȹP�S�`y����֞ڞ ����.K��4*b~+�0��%�ʡlc��j�AÕOy�4k|�;��ǫ�c~������Pܔ嵾�Y��E�?}nYR1G����v,�3Dֲ��h�_��Й#,f,#(���x�B�<=��Dz��Ǥc9�]�ou#qka�8~xF��f�85v�2x	��m��.w�:=l��q��tJ�uW��3}�Vܨr�_��и%�`�~P1|�׵b�1�)�?���3r(�<�'6N27���,��6]ޔ�5b�?�rAZ�c�������?�=���·��O�.g� �`y��H'�Pϳ.B\[���ͯX  ������ �&��o��
7T_NOA,�?��Z���;���A1�k����
�!
Ho;�<Ai�o,�}�Bkiau��p�@!i�l�^7;8�6���c_W1�Ӕ>��\=�6��]�w����k��(vj㞫���sn��\Ddg�`y�@�ZC'�T�X-o��2��j�l���j�4�ی�6�ѻzC��ųD���GqhK���<pM���ߘc���{�����{�T�׉�h�_=4a��y*��k|�ݝA���Nwx��y��&\�A�0h�C��g�go�K�u
�����#��{���ߪ'�Ţ�g,�5�ؽx>�����0�yh�c��L���Sb��:�j��=�Ȯz��Q^���y��u->�Y
Z��3�,�Кſ6��������,��f)s�X�N0�I��lI��7܍{[V���Zi�vE���k�{'�,4� /�I�����QkO�x�#O���7e����_9Z�	��W�+�|��Dz?+�
�i���Q{ͬE�G���ʏxլM��/=�x6���V�_|��M]"橕L�������O�N���Q_�.*�����E�bH���~'�,��C�
�1�Q�6����Y���~U[�Q�*F�h����}�>�D3>yHF�%��q�*V�P�#��b]E9�/�W����Gr����_�-[Z���h��'���akؘY�+�)��~�`y�e���������
ͼj�#i��9ZM	چpq H��.�U�I�K�ЬzL���BV������z�ML��/U��8�H��(i�r+�ɓ1@�$���XR0h5��&4�z��$��b�	���N.l��aG�퇖^d�x�����E�9�N�,4[!o��앫I��5WK�\�A��~�S�A��:p?���Ev+\g�j��5
(�,h )g�=��K3L�c^w~CGQ]5B-u�:X�Xy�?�JS�t��<O�xfCl��`]���c�SG����2�)s�U���8i�kA�o�_�����ai8U՝�_0�	�N�Y0h��g�R!�v�8���X!
�OV)�XC����~ml�b#�-����Q�EО����35�A��W%�;��h[4�BxZT�l8���1/�b}�w�ςA��J ��h$,),/I(`^E�"	R&�\��$�u	~_��A0A���W3��g���!�{�h���鱘�3���f��(k�T� �\Ş4�ɚ'K5������G��
�� �K��LAY�~�(���q�7���Q9I6��5N�p�Ϻ{<KN����d��7	�S8�ſ|d�bg�vp�ՠW�:Α��<:p�9��b]���E�v�[ץy�R^�O!7#�c�1,x�;pY�gi��F�BN�"l��5��W�L� X��I&I�xr(�|�1G4M�6�i�|~�D�`�`�ӡ.�>P�k��u�R�    d�GL_�謉ay�[�ϔ*��`/b�4d��Y�N&Y�o�
�,���A_�#�w���8�ݴ/ ���t���|��O����*Q��j��WA3����R�%_ H�(�`y y �'|I�0���)�<�/ #$h��r�q4�U'�*;^TvKW���}h��ve.o�^��v%�������oG������>7�5�Ih�BA�=��s�)G inK�m�������,���%ʇ�f����)U�����%�{��-���� �G@�g%S8��Q��q�O�6뒖2V	S��,m�A�u�q�)m73ٯ�:5=:x��2A���l~
d� Q�&H�����Y\�}8��Y��?���zKF
���&�/��R�	F��+V�׌Ƒ��l�'�~�����>��C&��9�Y��ƺM�����k�ʋ%ys^�ɠ�,�+e��mK�ԯj���7��������������N�YS�<���<x�u�CRjP3�*�Y���#[��������)И}km�(|��(T�7Q|�f�P�i��.Zٕ�`߬�m����9�A�Y����+��{a> �N1Y�<`���cQ�Ek�ᒿ�����֋�J�Q�fVm����U��j@���/�/ ��|.P[�m 2
�x�('�z��0)����s�%Of�)�`*�I�&jǼ|���iS#:\�.#��A��{AYZ�9���	�A�6�kz�+>��,�� eb��(�!W.r���Az��v�"_�<�<�� rg���,r�s�`�$W96.Z��*q3B]}��ř0��mcR]����2�r�1�|Q�sա1*�-e�;���'luG��ĳ`����
ei-��H��g�~�g8�X�{;�r�R�&��0I�5X�cW*����L�B�i�Uf�b
G�y%�0�4��D�l����\�f+���K���_�����K��@�3#��W,������#U�3{���e;:F�Ī톿�J��W�U'L���G�����6�\�`m@�3�c�+:*��xB���o}�%H�8�Wbk���� ǚ#Y��{��K����������r��/�uk�
���� q�����aМ�,�'3�q���l�uֶA?�	٩�R+�E/�\�T��i�gp"�����}l!��c���5��c���P�,G�,5��>��W����^7,�<Q�x�va��qL���`�8���<cs�c-˓��q̥�c��:Y���f0�l��ζ�����G��zRcP^�����y��1$�������Ǐ�{�D��Y[�ۂ�V2e2��Z)у�L�}�z�{�l�vm�3�6����i#w0y�JV2H�dh(��u+<c%g���On@Ք ?ɉ}�ldŧ�ꋔ��A��fܗשu��Q�VZ�����{���{;��E�����{� A�1*�tP:U�bC��-g�oZ��y�T��cη�&*�n=7������k���&�Wʠ=҅��}vO���9u��u�ԇz��d}�-ha̕���j
�ByH��,�E�lo����d�~�[@$1��_�VYO0TLba��n��9s�*8�.!/�nsa/�y�	��� �k���J�?�ܧ��Q��ǳ`�,�B�f.�Å]�6��A:�ToAK��5����?�Ҫ��o�X]*TkB]@�Z*�s8v�7�4��t�d/��R������مlh�R���P:���P�2��ɟc�mx��S�4�R�MQ�+kׅ˾�������2?�	�,�SL�T	�6�3��nFz~t�V�)�T��fL�۲�?2D�&�ywyx4t+M��Q���'xw}8�&�9��`�J����2��Ky u}���ǣ�'\���	�%V��1��'��W���=�p,N��۱�ό�K��Vr�����Nd(���We��\���hr"o�n�,5#�Z�v�x6/?�yb<�A��o63�ey�(^ߛț'+��W��g��3f&G�$͜B��L�T�Y��Q�wڎ&�YԚꫳ9����]=rK��-�Mp<�;�7G/�i} ˣ��(m"ї �N�QW�q���_���	����o][b�S4:5Fn�V���Zf�z�}�fC�_�5�����m.����G޳�Y�W��W]���m���s�Y-��zT4"E�z,Sƫ�0g*��z�l�,5n_	��i�n
'���Э\��i��#�ϝ5Kp<�w��!{�9'ϙM����`���QXf%}�sjo���Dx�b�^9'OR=(1�,��?cز[�k� 	�dq��e˦,[�d��2"��5���p�L;R��j�j���JJ\���X�d�eK&�-����v�yϒ��75���� ����|�NS�,m���n��l�ߘ�L�8UKm�%��-v'�^S���)K��LY��,�C�,5���"�	��9�fҵ��|�^L�jC(K9giM�sߞ���x����Ӝ"A=�[���P��QmVQ���=�?Ќ%M��=f��e�-R(uIg@o�U wm��[�� ��I3��/�u�AC
ҌJ���֢�ʦ�3�t�d[���_q
����X.��_�lTEO�n�L����H�/Hȕ7��}_��yzp*�t~/A�����ه�|����M������x"}v0�P�L��33��o/�\�"L���Ui���iP���J`���.9��a��9�~�����\0��m�Y@�tЙ����[5!f�ϼ�8>Ip,�'��VP����o�f�H-�5?R�u1�J��6�*�p�u�����ɚu���g���%�f[���D����v�O=��mJj����mB.�ޏ���l�cĪ*���
�����-�0r52�ScaP{�O���������窐�ьϭ�7L���Ǝ��W֬������8;�)��T�7פQ����T�-��+�+��T�PX3�f�E�]�k���D�ŀӟK�ufP֥�x�^�� �$c����s�5�6DAM��3��R��d���[\!S�9��鯧ǎT(��[��y���U������!n@W�@��e�Ҷ�-$3�h�Orq�����_^�:,i*67�7RM��h8�z[��uW[���\u��t�ň'���$&���GG�P}^��>��i�8WI��%
e�z	*JM��N�4��GzK	��Q'.�/^��x�>�Pbj	Ӭ)�u?��͹��O���D�WX�x"�Nݡ���R� �7a�k9 ����k��f��Y*�&}�kkٯ��1?��W�}���c�C��X�u�~�1�E���q���Yc�D��io$�/�'͛�[�)	��7���0 �E�e�Z/�݊o튵Ey�t��ƙ4�L��1 �g�=桽�<QT	p	�����2�8�m��������u��5��{s�(h���M���ؖ˯��H�R���G��QT����c�=j� &K�#����`�1�b���zQ_���V���]+p3�]9s�7(�-
o^���w�C!+ˣ��(��8DW���H)>tʱ��MM���L,A�Ȍ�UhP�%�LFy&ʔ܉ϯ�P���fc��,s��lP�zaC)�~�K[O�w����-,�*�P�UUSi�������b	��TI��3�c��JD''������o�Dh�c�*^���@�N^�]e*+��$Ո���b�ƪfy�V�]@��%K���[�%%�B�ذ�BUF��*.�W�:OGR�<*}�"י��8G�x*��?���~#H�M�sψ�Z���6p�C'�/���h N/~�
c�t	�
�\�B�_g4�T�#p�8��_f��nt77t7
g81,��ZE�/6�]듛zWkK������L�O����I3Y�<*ތ��+LPc�x�[S
nR�J�����|T�&:�$V�sMA��l���Ǚ�3���P޻�DXq���8�Rм(��!Ҟ,f�T�&�q�ˣ�(��I�":m|S�Z��p�L����T��U�o�W��$�Ð�)�i�������=Em��?~�붓�^�a�PwF�-�zn�Q��6��s%�.7A�S    g9g�Ë�s&�ɺ�2,N.�8��g��9W5>47���P�X�Y���|�)�4K��{�d�p,B�;�'�s[-�2U�7ZwPn�ͺ5_�>;����V,l��{k�R����a����$�9g�Í�U�ߵ��r���t����$�������)C�;�z\�%fgV������n57�
T�[�O��wە��u�P�q�l�
�F8g�;j���Ӈ�S�<�qT�>k�W$L-\k�\�|�.R�)�r����v�#�\X�z�nXGN�%��Sk3�������p����=Q8��C��".�oayTy�~�ߎA���E�M=a�r�8	�=1��������@���,�5Airw�
��ۃ�VX`Ќ©�غ�; Y�<�x������E�;�~鲢�Eɶ��Ze��W���;���ǉ���x��?����_dJ��� ���W֜��YK��U}۟����$)�y���|���'�zE��"��,XմE�����~��?}ԏ�M�Y�V���/[=XjKa�7&�)m�5�phj�i�}ńeo�����.
!�3�-2�w�r��L��6�DPޜd���HӇ{�TŽ�<���0l)�e�,�omxal�ő2kI�t>�V)�*�l�^�P�Cjs(6�z�h�^2k����1,�*��H�q��Kڏl�������7,2K����:��]�2)�J�tBPx������W�.-j��d.��U8ǽ�(b����{��t��,���0yFEk�s�U��E3��Q��OI����C՗�"PI��s����+���{��C���|̳�NN����������a''���� � �e�&&|S\,3[�����4𚡁�_����[Iuq;���� l�E��9�Su;�A��dpvo�O&.˄z��rR���8�(o�������r;@�7��2ثj�g��T��ܹB��A��j����dq�t���ǰ<*�r2٧!Z}�\��MO��~R\�p]u�Ie��I���F>9�B�*]��.��v̢]-�S��fs~S\��t��ހ���a�	M�W�q��Ľ"��Q�g�W�Qi3�U<B������6p�..���!�A������R:���q�f�!�^�Z���ʌu�H'�QI%��m��1%n�]�/9�s�
ˣ3�(U #;�>����t���4�j	v����n?M"���~���+���>ěVB}��KHE�Is3�h��'���=89����܉*�z���ռ�j3�^��
\,�[X��:W*�⻒r�*窐7.�$].[�G�1���+r��/��������������f�=���m�����ϟs�ҏ����~8㟎7Fd\?)�'$�O�`o~���i�ś3��-K�ma#߂z�:�c�Y�����H7��iAub����!Z	c������˭Nc�D��ޤ�>������Z�{׮Q�oc�mM��!21���#�]1��&��w�� Zp����Gf�3;������4�ct,����q��L��euY�/�_@������)l�yW��D�k��W�}ȅ�̲{��O�����>׵�ldI��\|�ljNQ�	
K�e�	��IQ�I)D��,e4mI̾7/o����tW��/��6�����2���4YTM�]�-6�hU�i�X)q�"�6�z-��ѭ��G�������v	U�>�3����Aݟ&H��sBa��"�?<^˿�:�I���z�����n���s�s$�t��lVc��=��f��f�'��������Ƿ]�����"o�K���P�y�oJVg����jH[��S)���*�z�����0/���/doay��Q��0(?���
\���\�_B�쿳y-���,i�m���3��7:��ŎvUF��/���E��{{f5����<M�(6���e�-&ͲP*�̹��O�.�uE����a�o.j�-���N�;�����]?EL6��lLL:Gq�f�p����K�<3�t�,�f��o"�æ�! �_o_���i&�:��v^H2�VK�+��zĖ>�R��fJϳ�¶�¤�W$��~�f�u��棍5t�(Y�'N��O����m�d�-�Ci��[\t�s����h~�^�O��I�?i���pP��x���(�M�}?�e�>�m�w�˂�i6��o��m:Q��Q$�G'�D'��(����&f�x���	��-�����ʖ���Y�`�����w0x�94��
�]��������g8�?:�ob]��4f����?�Ⱦ��z��o�g��O�asu������M�ٜ
-�c��C?��U����/k(�4��,u�C���?!_��S��y��|��eSFw���6�!2 �(S������o�Y~���8ޝ�����_�]?���#�@\�h���
J�XJ�������~��+s��Ơ�����<��h2�1N����tŉ_�O��4���pqn6��+K멶~��"⃟�Y�o>���7{��=;�����}~��_�Id4O�(��Ƴ`y���T�B���;���C�,�S�7|�2��^~C(qtz�>���x����R����z��?#�~sI|��P�����	�g@���;�-]�h{��"7�Q�ڞ��*Dm��L��ŷ���Fosg�(���¶���~gg�0Dv�/m�/��m��{���*w/���kyj��]��<C��x#:�'R���'P�c�׹��[��}B�v��^y���PY	�N>!���*�{��Y�{czfC佒w��*G߄\ek�|�M�F�ѢZxP�u�?Z�.���Ν����G���[��xrr���a�4�]�Y]��~�?�� �0�g0 ��o`y���������*+���^x�H9�_Z���Z�0��� }�Vy�3l������c��?��@��Ǉ�Ƨ��o��|���o~z~�c�'���I�>zq��.�Y$�B���+�=�/J�z�s�Ptzj�g^��x�q��a��ؘ��z�/�{���S����G���>ǀ�+�m���fE!2 ��=\���y*����Y���[<��Y<ǐfA9'`:�*�Ł2��P�ځk-��'%+b���	Z�|��p�tuk)]���S��;��	��c|jyЗ����<K䒋q��@� t�ӹ�����ܠ�����ڸ螕������P���农/_�{���� ]��|���_�7��j�r֒��j�݂����4z�ㄝ롼f�I�T���ץ�۪e��gb��;Mf��,�%ś�^�N���Y�7���UTC�Ī"��C=�mݩ�ZWĻ��~�C�/�`��s��*�����\ �[��D���TO
�R�ݠRg<��A�������@�^!7�P�v
��(R�+jO��y-�y˳d.)j���� w�d�c����S-ot*r�hQm�����`n���j���&&�,�.8�����	I�����\Q?�Z��2���u1�<�ufCҖ�RT�C�`����7F�b�⾾�m�[^�_쥸�-,�R���3�U��^ɱ4���M�\�(���L6�t֔��P�����Ë�܍TV�V��e�w�ʂ�Y:�d�g��:�܃��/׬ZUN����/a��bRV"���|{u]RkB���˝��WV'������	(���=b:��3wLsC�<�]�%���n��F���_��[�浮�w���a������?2�g��d4��X�,���Uґ�J-����;����"'X-��e������$�(r���f-��ʇ�A��r�����b��pf�2E���Q�k%����������f�'�#Z�qm�k1�[X�e���Z�,������}W�lA������I�� �v�ս�%o�l���Y�������l۪C�t]�ԔJq՛��ɭ�`�N"�z,�r9&���
f��KM֔��Э���i\�kx�7�ܩ��P!"d��o����)ϱ���SEM�n��1�ɵ��$Q��+G������I�Uo��C{'i�B-WZP�ʂ�Y>�f~T�Uh�X#
�CG!Sc�p�S��G�S�˟� *����b)����:6c�#�N]���    8����/��8�|B�UWz��$ӈ�H���o`yϱDe�k�ЙRT�q�Z�\���'* �eeh������Z�kM����S�e�&��ǵ�V渪A�aժ�.�i�����݆�G���2C��x,��I����y�K�C���8C�o]��;M�M)T�}��m��~���!�C���}+2�6�;Z�{� qeK�.�D,ρ\ru*�(t�d�:d��(�@&_�o���Fq�;tQZ�{��&Hp:�\��s�,0����y��b�-���Ap�ͦ0k�|�����|����x�\4Ab_D�g���K.��KuK�.(X�B�I+�V���AA��aQ�9⺕O������Ʃӽ�ώ Ml�K�h��G�v��>r�ѺVe���um��Dl�| 6��9*���.��h{�
��}�Ζ�M�������;_y�Ɓ��LW��k���	��d�� �I�-������sF5������@�XN�$���B��ߘ��-�!��u�,eyR�+��xQ�U{a�X↋�ѹ2����pj�W���"����ʢ�c��5�uҗ]-�z℘T��Ǘ�l���<`T�ɕš2Q���g���cc�[���K�7���J�b!-5����;��x�n(>�)}3���U�esFOv2�i�t�mk�q	4��B�������.���3j/w�G�2J�,X�csl�ŢX~|��R�U�9��Ietc(�	������#*v�\���GN�cX��)*���Mk{�I7��h*w˥���b�h>�7
��Z��*N�.��Zۻ�� 8�˱܅%&[+u���-7�K	͓�'4��:�l �&w4��t��_o�(���`q"{����d��/M�A�/�u�`��8R��K�&NO-�Tfa���K��˅��<�!>���op<��K�P�Zn��!�˥N̩H ���ɞ(Hj��U]�N��@UT�h�)s������s4O�L�CL�7�{T��&3˛,Y8��Ru٩�ǕՔ�Ž�Nz3cF�ҋ$�*D�쥩��q<�����N4����Y�'�>QC�RO�|Y7�?�M���԰]H'��hy���G��?��8���zϏ��20Hќ�ͱ��;[��=a/T���¼;<ތ�����>S�J_��������p����4I4��YIn�B|W�YD\��M}�g?�8�$�Ǝ�gǅ�%����aNg�v�9x��������^��kiL��b��h7O�ɩ���� �܊R��o�C��]�Z4�����Vd����b��E��-��ğ���`�H���&ubX!L�p��cf&Տmo:�����![�&k�ղ!��!+����N��r���Zխ]+LI�_��'�`y��%Ug�fG���;�垪�&�R�2���㻢R׵�����h�F�V�_�����P�������&�X{[y���`1Z,��{�B�Ū�D�,@�⺅�SD��fѡ��b: JA��ۖ��j.��d�Qݑ�WS~~��%�n`y��qI��PtM��,	�۰5]j@��R��pK@e�TEU��^���/�9�������A�y;w�=8s�A�!�:V��}������k��")�TbX[Z�+:�R�M�ݦ'�2�T���G��{�}gWS}� :������.>�6��i�:y��3�����U��Hs���f����Q	đI�s���D���Y���2.�Q�N�3ƣ>؉�_m��0b����F�W�ΰ<��8&��Z��2V�[:�Xeً_����S���@�����V�ā�q��tQs��e�qoR8N�G[��4�pa�z�vq�����'��D^�g�`y��q�}��JP�&��[��DA��j�G����n$�M��wQf��-r1Ƶ��/~�8�@s�'W�dQ��Y�Xs%�}��-;�nW,n䊮�:�]����v��{�z�٘k��,�s9��Z���!�Q�,���,C���Y]�W!g��ܥ�V�KF�p�=Q`�P�
�i�����T���ӫZ�Ȱ��`y��e^R~r�&�E_���Q�9�\�I��ġ.�\Wo�24^�Ů0��aG��nM(�ҨZmt��T*u�z���E�ɂ�	��2o�;���n �7@ �A���3�G�_������s���M�*��Nϭ��/�%���eY=��T�׮�W�E���ay���"lW/7��b~?��ǳ�|�M�9�EY:����;$����jY��%�tT�U��d_�w�� 9|B"�7��j�q�@��L|:���h�8��vo�3?���V9z�=��w��ކ:�[������7�A|��u����'��q�gqg��/��4��ne�vZ�wZ�R�ף�\:�>�XmՒ^ ��70H
��OH ҈��69O�:%��5�Y����)���G-���Wv�۠�A�~�����p��Y�_Y�uϢO0�"��3��`�9���}����)�M� T�k�RH�&���;;�K�k��w7�ď*=�-�Z�.��f-�n�~i:i:A���Y��;2�*�ż�,A�8�d��5ٜg�S
��GWz{�eyH��"����{Zg�+�]��zE��Y0�"��"47���_�� � 	 ��~�J$-!OA�.k��R}3��Z��fPj�:�n7st9ux�]��^O�� �\����d�6��,�����p��:ɬBv���ف���<gN�֬5n��q�]ӫ�����\hi�Ҵ}��w����������������(P'3-��HhF^q��&���&hV�9���Y�����`��^iu$�Y�Ų����k����{� �.������}8�4>�����HLҙo*DG�V�$	z�g@y�M����FtW�m����*�[D�@)�Y�ul�~�3�?f�$Kst�����ȭ�;4�&��z�b�񗋪&.��z����ZMݭђ�w��W�d��g�����Ӟ���'{�3{�2}�4�J�/w֥�> ��qFł>W����hz�ҫ�W�5,��T�aK2����X�|�})��*}��f����5�����ւ��N����rpGjђ}p��>�^C��`E*����(�(>ޢ$��Ѐ�	K�&��7�`��7���5w�)�)�b�ùnڛ�v7D�^�k����4p��`&	�+���l�ĝ�!m+���v��~�#FX���o�� i����>@��@��m�d���7U��6���nCur$n�io�iyS�����'1�3x0��`&�T�ǘ�i���{��Ip]���SSƘL7�V��7hmf�&�pV~�p�vD�i��T�iU~zʐmw�]O0��C��cĸ4b�c1B��27xq)�Xs;gGL�����z�-Dm|��UW��F����E�+�1��xq9�1=ƋO��XTPp�Q8��+-'8S�A�R�׆�P�J�N��[���bYR;M�W'�_����O0��C��{J�����t���j����ʙ��БG9��t�(v���C�x8���O}�D<�>L]�<I%7�B�D!c�)j�A��J��Q����4����ۓ�|��4�;�}�)�^�H�����H�B�F�`�F���Q�Z#QE�.v�-��0duP�) Mt{�ń�k$I9���n��k�a���/���|rTgZ�_(��v'5�)K����6�Y0H���2��\�v�Oا �|���9����ѯ�EI�I�9�S:���7'~�w�D�0��ɀ]�E���Ѽ���N�	��d�1z�n���2X�=�6�˂�M���tF�v�3 ��j��BIGkg��F��/k�*`H�?%Dk�j�!��c��-;J�;g�
�`
Jy0�p���b�0k�&���b���Ѥ]����X��~y;(ˆ��Fs�Ϥ�r�p�;27)W$;���f�Q�T��I�J��7�֣�|(��_��b#��3�����F��8N&�aKo��Z�Q���W��J&������-��b# F�q	 6�����%���E2[K�A�(��\�e�"3�QC���I��P�ߝӥ�aC�=*    G%��u����T��R����V�rO��PKY0�;��l�1��q�?��b'�����o�6G�9���T��m}���`rl.�Mq�}:T�ng2zN�:!�^K~�� �P�$��Fk/z�T� 8�3�d��#�'V��Z��ns�	�����>XW�9S�X�R�&J�tA(OL� �P�$���4F�G$�
$�VY�dR��V9 X���e������Ш�����ju�4�5&Z����[��+Xr�ľbɡ�c_b	>b���G���y�f*S��3W� ׷��2dJ��;��;�b+9il����gN�]����� �P�$2}o�c�Wy������c���$t`W8k���%�Y����G�4��GY�)�+�#�|��F��f�ի�}�isa�����fESqܬ�䠳W�L[.[ω���$�NQY�<�[9�}~�O���>�^����[M�%c�!Vu��J��&��F��4�d;�I_�f�=�l��� c�Bz
'�,$=�sA����G�:�｣5>x����+	E	CSp�}�Mӄ'̡��N���n�dכζ]���u����n��%����w"2�Q6�0Hx���|���<s�5 8��]W�}]�U1Q������=~���f���!�f�^YXK�tpq��[R���R�Sq���XRZ}��u����մȂAC�d�ԿƑ!֚���K@�;�y��,O�TBzD�����8�J�ʤ��v;�n�Dw���|`hb&���ԪCf�;2{eĽFe��u�0He�Ã̘�"�u��(,0"��I,��g�5[� i��Ps�WƟ��,V��IdW	V��)��(s������U�9i��Zs���V9��ig/酅Shϥ֜����{�0�A�@�d���L�!�Ư��R{H�g�}�&)��5���RD�MhGS��]�V����|��`L�kb���Uec,��[HD�����a�h�T Y�Bݚ;��+D��1T�ȋ=(i!)�ָ�H:��n"��N���>Vr���17e<��ҚG�d��ޝ0��a-�UvWo); n 	�~|,�ۢR����ҭRP���&?�8wDeYs_�'�����c�'�d�S�
G���fo�q��;�����8�X��MaȚ�S��t��L���̯�4�≇Ы�]l'����K�BHw^�b�!a�)��7x�<�e<@�?0��E
g�,U��sT�9%��;�R���ֽ�D��x~���:�xg�ςA�����+j�����[t�'�v�4��l����2�#o���`�"۔�k��폺�b��6p�MI	��݀n�~G2�W���	(B�����Sk7x^�H(@ɬ�?���+�$n��D�R�֬����d���jC�Xkr��%��*����+ߤ��`a�ړY�}
���ܡ
!�e��ӌ2?�WS��ő����0?�;Zm��:v�۾��V}VZM��
z�;��`=�@�Y
t
�r��\y<�G#�~g�WS�ӕ|�]*��Q�1�>��L�m�=Z�-��/D�t�
���+����gʨ7D�\m�����G��$,��ź�KllM/�1i�M!�Fmu��������}s���ݑ���RY�X4W.���NY0�1T$�,E�b(�-��ImY�91Z�G�j
=���j7�W�nS�q�0=w!�>�ú9�v-G�=��tB��{/уJ!���&N!�������q$4r���@��УMvIL����Q]Gi2�5��nQ9lKQ�SgR��*��w�F8� zP}#�Է4�vx�X쓤�>����*z;�%c�X����kyK.;Ͱ�,:LԸ^?,�,���HxK�^I=� �,ʦ�J�x��>��8%�@�:F��#~=�*k�7�xx���N�W��z�C�S�	^y�_�Ǖ^�*E~��w��
� �s-HT���B>A�?��P@������r����6 ����4����i��ب���T����I�`=��+<�f���!�'��,����~����M��
�cKv�&�zTWlYbۭ����D�چ�^����S�<ACe��R��`�?,��x"Y�9m�D�I޿bK��R`Ƌ���u�������Vgخ�y3o���x>�D�J5���BM��\�Ѭ��r�{u��D-�9�4�P�	�|?�.a���Z�=��]�.�4���Z�n������`4~]�^�a]��P_h>N8��f�>a�<\�	�l<������Ql/��%)��=a�t}B��.Nq���Ԩv'j�
�Եh�b	U �s(��c'Ɲar}�xbq��'������kD�桼�Z�d4r�΂�N�<G3Z�5��s�P"��+&A_k��� �P��>��4۵6���>9����<B9��ʔ)O�^gd��������U�5���Ie�ߍ"�P���2(/��*_�(C���\':�>lojͲ0N�<3� Go��)�iS�
[j)��M�.Χ�
^ṉLR;���)E_Zt_��&�9�C5��\M:��,� a��)|�_'erS��N
�B}]���~Z
F27�;�&P^oE��B�w�]���F~X�L�	���D}�/%X��D����X�$J4` �QʇB��v�2��~��u�jGg�7j�x����j�Y��
��G�-�Ad9t��+���~�&�OLn�����9��RfřL��i��f�.ە�q��ʡ�*��
W�Zy�ό�+�)��b�&��
�}�臫��>�=���)�P��_N!ɛ\o�H�Ix�X[u�a��w$�WV�b�)L�P ���Na��2�P#Ҡ>W~�� w�o��N� �N��߄)M���ݞ����"��^SemNm�L���e=Z���Hh�^Z�\�@d
%PN�j��~s���)�$~�P&���)D	�Au�5nW���ؠ74;�BY�W[�4��c�Ã�[�F�R�}����B���Re�T�G���qn/G2�Dw�(O�ORH�-5����������_�iL�]������f��A���)DjP�N$E��1}�"΃әW�|G��H)�4i��^6����QeS���
�,#}NC�#�W�TT��ɂA��D�5��j�r�,?.�Ӄ��b��哅|�$�4eN-���6dw<cY�����A�Wg���t�l�������z���T�{_���x%x�g��:'����R�M���^Sf�MS��[��H�����P��4��=�$^�&⣫$G �&�_�IV\����Y�$�4	�H=J^O!ʘ�~N_8_յ����ޮmYQe�>��
��i�':BQ@�kt����"����!�?w��35q�KwU�K�k
)92ɜsf��-�*'�g;�.���,K�;%����T8H�7���\�a�}mo�<�2�cL�%���܂T�5�n��U�ܤ5.V�~gbGf�Y�����'ָ�h��w�hD��	��js��y���}�'�/MdrH��L��˙�e�d����n8_��mw����#��ڊ�%3|� ���*��0�I���$HBG�br&��0��^وW�$�^Z�,��!<���'VIZo�p���6��(5a�>��Lִ�ÿT�-��u���_o��u%��y�Q�eJxO����>��q�h*[ /�d�d�m���4 oJ�f9����ݨ����u:T���iC�x;��&V��&~���BN�#�� S� 
�p_3�6��[Y�dXz����PAe4����Үeܐb0SýUL��C����spzjV#uZ��M�3&�/�ٰ��!6����G*�N�C�z��OV_Y�ųc���������*��5Tu��<��<�h����y|�
v4�_���l@�y �HM�3��3��X�yKZ��h:���fge��.�^�g8�i�cH�o%���,V�D�I�`jiN����dc�����.�ʄ4�#��G��Y�k��p�����.S���妔� *��T�q9̗����	3$P�U��n�Y���h�+����k���gO�$��꒚,�����$�r�����T�k���^���`HW�UKt������[��iwקsX�۠8�h��s�    Ť^
9�[#��@3 ��)���f>��^��
�>�'�G9O֌A����?�4eJ屠nv��L���V�+^�\�vd��Y��YI� z�!؛�I���kEN���z݄E\Pe8@\$Š�X7��m��T|=��w�}xJ��J,A��"�}��/_�1��	�Iߗ�����(:�&��Ɵjt�@�L��1e�z�4d
��΃�l B�0O6%
`��ܒ�n��)Uվ$|�|�G e��R"���o��O�>a���[%��دݑ1���:wx�
c=:��ne8m�:�f�~�Ov��N^����L}d6��P�P��{۹�@7����jU7{g��v�W�{���9#�\�+!@����n�N�
[��8�z+)�=�5���*� E�RU7 ��[�w:�E��˅�T{P_.	f���!���{�l/1�x�����.� f�imN���4I"���t�ڼ��umJ�j�����f��AOl�-�kj�vV�w`erWo6 +��
����x[jI� zKMժtI�)������&Hw��R��Ql#�)c�������\LQ��������1�/6������&{����U�V7���s�giC�7o��X��#|%+U5�iJBM�n4�0D�,������'_Sй\r�f��9[j>j��_l��~��R�P<�$k��h��J�&̈́�ng�I8$#�BH���Y�
1�v+Q�B�潭�{K~��b��>��Z���wڅϝ��@��H�Pt/;�<�9x�����$�ipA5��S�+C�$ǐB>,u]˃��S�Oܨ��j��F�?�ϲ���H����'J閳_A�7�U�ѣ�d�^y=ZU��RjW��ivU��8nD��F�Pxnm�fm�F�(nL�2H�Fv �j[��
-�����P[u]Yk=���p?���"�8+���v�ƫ?� O�1��Mc����~�|��H���lm9���Y�L6{1A��L�m�l xT�EA%�+�
�d�Fw"�ݔ*�p�2a\c�D#�K{������|.Ļ��OBT1��ߴ�2�<��iE�K�g��b���V�R��Zi��]R�Ce}�LN�:����/�<w��E~��\�.��|W�{�J��VA�i���$��F��N��߯��:#$��;Ѫ�}��}i�ӹe��tԲEA�-�vX�`�����l�1vQ��c��&�ʾ&l�'���L�..���H�v���k����O��v������!�3�;~��A������ҏKN�e�����q
�z�C��� ��fc;��'����Ui��y�Y��"�
ג���#�"�C
0�[��� �<P ��E�y0b��$�у���8��5HE0
�UJ�~��v��G��,	����REE?���<��i:�Pz��l����V�����#�.����<����wFxF�~fNl =��ft��}�3��/U��`�	s�y��4���O��FD�O�� ƚ�}� ���ز͉O�#�2�&�:\�.m�e!>c��F��u?�&6 o٢��8����w�N�4^��1%O�8S��r8]Bu�kz��@$�ֶ�j}l�<0�>�9Kq��oŖ&�Úӯ�Az9�DC����4�(@H���bC/#Ԯ��w��Ud y&�Χ�=M�%�o5�߲՘8��$ֻ�򢶖�;�Ack�W��Qe�x��)��ۑr�}�<�����iU�X�](ѷ��	�_��d1�b����@�0S����oW�ѮS�t�����z������&ʑe���O�f�(�s��r�;s�K�9�-�90SS/q�Y�~�GW+�ޡ��?9�I��_�oR�Ky�aH{�8�n��Hcr�%�acy6زb�/�{���3j�w�a�5��<.��p�@	x��hr��u�R�+�	_����q�ϒ ��6�|W��"S<G�Ku���CL��Cn�m�<�Dm�v�
0�Q4� /h�t��A�E1�`����<*i���F�&�i�"���\Y���$��;�,�R�d�5������H��$!������:�j]@�w�j`��J{0���>�|R�#Mg�,'
g)"��z�f{�Xkg�K����p��������`M�}�{;�s�H�t���j��'�69��9�`��gH�h,�ej��`�Y.n�1��T\�p]�w�r������F�՚F)h@���j�[��1�s���
��Yp��nL��s�������B���1^�(]YҴ2diE^����@���!�ה�q6��Ğ�a��1FN7d5XΩ�B����Y�W�>�l k����a��=�ē�%�+M�~�:>��C�HY�:��r���1� �����dڗ�y�1k�~�-&�눱F��$������P��ZH�s���g��yn���2���v���H�`�0�B��L�*o�C����3���Ӕ%�y�N`4Iܱyr�)G�e��7�[��/��m7��q�w��ƶ��7`�% ��l�l ?��J�]΅x�185=�#<�1r`�ό����3��aL���(�1�]u�ڜk���=���N��LY�7^�?i]�7�?p����u�d�Q� ���l6x��Cd���p�usQ�kN9@@�sB���{{u3݄t��vƜ�8a����Ρ��[�7 �zbl������"c��X�+	��]���ǃ8h���K᭷a��M��Ã�[l�q@k�Q��+��X[�^�u�RN�:�/�WO�(�6b��'=�B��ۓ�:?���)��_X�n�#�#L�W��Ұ��Z݅7�:�i-�QS�g�nx^�kl�YM�������PO>^�|a�/h��/�,��O^+^x3in��OP��Ǹ�P���'z���q���s��6���V�k�)��`��2�70�r_>YM] ��{x7�e.Ý���ՋFVk?6��ݘ]ƞn���4E��ڹ���$ʚ�� 4̮|�C��^��S�4�Q�W���Yn�-i���p�yo���;��	Z�v�Bz���Z�/=����7+�"�=,���4�8>��߁Ԓ1���ŧ��U�]������>�⭏���6��[��ZRh���ZW�P��[R��$��׮�3�I$�H�6�o��ʳ&C{X�5n��jѾu�'��!Tu2�d�qn�EW9;��T)�4��p 6 4�
�оy��-*H��0�����hᖵ��j��v�\�hd]��,|�-��O[�n(Cl�Y/J��)�~���w�s_=>=C�`����E�Λ��\Ho�9�	gc�}/Ǝ�-�.y��m�{��"�'�M�>�dR.ʣ^dEJ8U2i�����Vk���\�K0�R/7�4��-���Lw��ҟ�g��e�����_��j�+�Gm��o�s���`-���G��F�:�~����wڒ����1hK�Q)��
r�Ơ���ؠ�8j"i����b�/-Ѻ���̣�==���0]��u��n\gc���z�ޯ^
`%�؈(���'�'�U+���
�YY��s'��O62r���ri��
CzaVl,�5�����xn|��G����H<,x;��c�������-��`/*t��gO�I�g��d��k�_j�֦HI�׊x<��A�y
Կ�����f}Y�w�"�xv�0����Չ L7qUk��֒�A�AW@n\(��_�V�O�O��/��D	�S�����"�F�ى�k���/��^Ĵ��V]�+:o&ǽ:�y˾���ڃEu����b]�������k��/���E>Ykb&#g�Ϳ�[A���FL�0�js���w���^��C��e�U&��5U�k� �Nh-��.�e�I1�Y}��Ng�D��*��P	#1��μy���4C�OPl�\{��,	GpF�Np'��jV�!���U�,�-�a��*�ѬTi0������F�ߘe	=k�� �0�KB5��w�R��U�m��O(��#�A�K� @WB{��zr������I��Z��C�~H����1\&V��a�z%k
3�`F��Z��|mx~4����i�レR���Dy�+ҭ<L����u���>�$�H �   �.������T7, H���_8!JM��7���+���'�$,�1C��A�d)�|9�ﴶDS�66������m�nHy�yO�<z�{���	�8�cC�� �0���nE��_��~�~�����v����%@�b8,�9>3���վ�t%B�W�l���A-�iǨK8����Bu�ͩk�;�k�p��;{r���������H��8�_[d���|||��Rr%      �      x������ � �     