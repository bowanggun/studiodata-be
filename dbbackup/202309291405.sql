PGDMP     $                    {         
   datindrive    15.4    15.4 �    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    16398 
   datindrive    DATABASE     �   CREATE DATABASE datindrive WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'English_Indonesia.1252';
    DROP DATABASE datindrive;
                postgres    false                        2615    16399    v1    SCHEMA        CREATE SCHEMA v1;
    DROP SCHEMA v1;
                postgres    false            �            1259    16400    failed_jobs    TABLE     "  CREATE TABLE v1.failed_jobs (
    id bigint NOT NULL,
    uuid character varying(255) NOT NULL,
    connection text NOT NULL,
    queue text NOT NULL,
    payload text NOT NULL,
    exception text NOT NULL,
    failed_at timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);
    DROP TABLE v1.failed_jobs;
       v1         heap    postgres    false    6            �            1259    16406    failed_jobs_id_seq    SEQUENCE     w   CREATE SEQUENCE v1.failed_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE v1.failed_jobs_id_seq;
       v1          postgres    false    6    215            �           0    0    failed_jobs_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE v1.failed_jobs_id_seq OWNED BY v1.failed_jobs.id;
          v1          postgres    false    216            �            1259    16407 
   migrations    TABLE     �   CREATE TABLE v1.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);
    DROP TABLE v1.migrations;
       v1         heap    postgres    false    6            �            1259    16410    migrations_id_seq    SEQUENCE     �   CREATE SEQUENCE v1.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE v1.migrations_id_seq;
       v1          postgres    false    217    6            �           0    0    migrations_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE v1.migrations_id_seq OWNED BY v1.migrations.id;
          v1          postgres    false    218            �            1259    16411    mst_collection    TABLE     �  CREATE TABLE v1.mst_collection (
    id bigint NOT NULL,
    nama_collection text NOT NULL,
    judul text,
    tipe character varying(50),
    default_chart character varying(50),
    referensi_data text,
    catatan text,
    route_name text,
    table_name text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    ref_organisasi_id integer,
    organisasi text,
    satuan character varying(100),
    definisi text,
    rumus_perhitungan text,
    cara_memperoleh_data text,
    urusan text,
    is_multidimensi boolean DEFAULT false NOT NULL
);
    DROP TABLE v1.mst_collection;
       v1         heap    postgres    false    6            �            1259    16416    mst_collection_id_seq    SEQUENCE     z   CREATE SEQUENCE v1.mst_collection_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE v1.mst_collection_id_seq;
       v1          postgres    false    219    6            �           0    0    mst_collection_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE v1.mst_collection_id_seq OWNED BY v1.mst_collection.id;
          v1          postgres    false    220            �            1259    16417    mst_dashboard    TABLE     �  CREATE TABLE v1.mst_dashboard (
    id bigint NOT NULL,
    nama_dashboard text NOT NULL,
    frame text NOT NULL,
    link text NOT NULL,
    catatan text,
    is_hidden boolean DEFAULT false NOT NULL,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.mst_dashboard;
       v1         heap    postgres    false    6            �            1259    16423    mst_dashboard_id_seq    SEQUENCE     y   CREATE SEQUENCE v1.mst_dashboard_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE v1.mst_dashboard_id_seq;
       v1          postgres    false    6    221            �           0    0    mst_dashboard_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE v1.mst_dashboard_id_seq OWNED BY v1.mst_dashboard.id;
          v1          postgres    false    222            �            1259    16424    mst_jenis_dok    TABLE     [  CREATE TABLE v1.mst_jenis_dok (
    id bigint NOT NULL,
    jenis_dokumen character varying(200) NOT NULL,
    ref_grup_dok_id integer,
    keterangan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.mst_jenis_dok;
       v1         heap    postgres    false    6            �            1259    16429    mst_jenis_dok_id_seq    SEQUENCE     y   CREATE SEQUENCE v1.mst_jenis_dok_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE v1.mst_jenis_dok_id_seq;
       v1          postgres    false    223    6            �           0    0    mst_jenis_dok_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE v1.mst_jenis_dok_id_seq OWNED BY v1.mst_jenis_dok.id;
          v1          postgres    false    224            �            1259    16430    password_reset_tokens    TABLE     �   CREATE TABLE v1.password_reset_tokens (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);
 %   DROP TABLE v1.password_reset_tokens;
       v1         heap    postgres    false    6            �            1259    16435    personal_access_tokens    TABLE     �  CREATE TABLE v1.personal_access_tokens (
    id bigint NOT NULL,
    tokenable_type character varying(255) NOT NULL,
    tokenable_id bigint NOT NULL,
    name character varying(255) NOT NULL,
    token character varying(64) NOT NULL,
    abilities text,
    last_used_at timestamp(0) without time zone,
    expires_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
 &   DROP TABLE v1.personal_access_tokens;
       v1         heap    postgres    false    6            �            1259    16440    personal_access_tokens_id_seq    SEQUENCE     �   CREATE SEQUENCE v1.personal_access_tokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE v1.personal_access_tokens_id_seq;
       v1          postgres    false    226    6            �           0    0    personal_access_tokens_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE v1.personal_access_tokens_id_seq OWNED BY v1.personal_access_tokens.id;
          v1          postgres    false    227            �            1259    16441    ref_data_tag    TABLE     &  CREATE TABLE v1.ref_data_tag (
    id bigint NOT NULL,
    data_tag text NOT NULL,
    keterangan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.ref_data_tag;
       v1         heap    postgres    false    6            �            1259    16446    ref_data_tag_id_seq    SEQUENCE     x   CREATE SEQUENCE v1.ref_data_tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE v1.ref_data_tag_id_seq;
       v1          postgres    false    228    6            �           0    0    ref_data_tag_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE v1.ref_data_tag_id_seq OWNED BY v1.ref_data_tag.id;
          v1          postgres    false    229            �            1259    16447    ref_grup_dok    TABLE     <  CREATE TABLE v1.ref_grup_dok (
    id bigint NOT NULL,
    grup_dokumen character varying(200) NOT NULL,
    keterangan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.ref_grup_dok;
       v1         heap    postgres    false    6            �            1259    16452    ref_grup_dok_id_seq    SEQUENCE     x   CREATE SEQUENCE v1.ref_grup_dok_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE v1.ref_grup_dok_id_seq;
       v1          postgres    false    6    230            �           0    0    ref_grup_dok_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE v1.ref_grup_dok_id_seq OWNED BY v1.ref_grup_dok.id;
          v1          postgres    false    231            �            1259    24617    ref_kode_wilayah    TABLE     �  CREATE TABLE v1.ref_kode_wilayah (
    id bigint NOT NULL,
    kemendagri_provinsi_kode character varying(20),
    kemendagri_kota_kode character varying(50),
    kemendagri_provinsi_nama character varying(50),
    kemendagri_kota_nama text NOT NULL,
    bps_provinsi_kode character varying(50),
    bps_kota_kode character varying(50),
    bps_provinsi_nama character varying(50),
    bps_kota_nama text NOT NULL,
    latitude text,
    longitude text,
    kode_pos character varying(200),
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
     DROP TABLE v1.ref_kode_wilayah;
       v1         heap    postgres    false    6            �            1259    24616    ref_kode_wilayah_id_seq    SEQUENCE     |   CREATE SEQUENCE v1.ref_kode_wilayah_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE v1.ref_kode_wilayah_id_seq;
       v1          postgres    false    6    253            �           0    0    ref_kode_wilayah_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE v1.ref_kode_wilayah_id_seq OWNED BY v1.ref_kode_wilayah.id;
          v1          postgres    false    252            �            1259    16453    ref_organisasi    TABLE     P  CREATE TABLE v1.ref_organisasi (
    id bigint NOT NULL,
    organisasi character varying(200) NOT NULL,
    singkatan text,
    keterangan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.ref_organisasi;
       v1         heap    postgres    false    6            �            1259    16458    ref_organisasi_id_seq    SEQUENCE     z   CREATE SEQUENCE v1.ref_organisasi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE v1.ref_organisasi_id_seq;
       v1          postgres    false    232    6            �           0    0    ref_organisasi_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE v1.ref_organisasi_id_seq OWNED BY v1.ref_organisasi.id;
          v1          postgres    false    233            �            1259    16459 	   ref_topik    TABLE     2  CREATE TABLE v1.ref_topik (
    id bigint NOT NULL,
    topik character varying(200) NOT NULL,
    keterangan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.ref_topik;
       v1         heap    postgres    false    6            �            1259    16464    ref_topik_id_seq    SEQUENCE     u   CREATE SEQUENCE v1.ref_topik_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE v1.ref_topik_id_seq;
       v1          postgres    false    6    234            �           0    0    ref_topik_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE v1.ref_topik_id_seq OWNED BY v1.ref_topik.id;
          v1          postgres    false    235            �            1259    24601    trx_collection    TABLE       CREATE TABLE v1.trx_collection (
    id bigint NOT NULL,
    mst_collection_id integer,
    tahun integer NOT NULL,
    target text,
    realisasi text,
    sumber text,
    catatan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    kemendagri_kota_kode character varying(50) DEFAULT '32.71'::character varying NOT NULL,
    kemendagri_kota_nama text DEFAULT 'KOTA BOGOR'::text NOT NULL
);
    DROP TABLE v1.trx_collection;
       v1         heap    postgres    false    6            �            1259    24600    trx_collection_id_seq    SEQUENCE     z   CREATE SEQUENCE v1.trx_collection_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE v1.trx_collection_id_seq;
       v1          postgres    false    6    251            �           0    0    trx_collection_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE v1.trx_collection_id_seq OWNED BY v1.trx_collection.id;
          v1          postgres    false    250            �            1259    16465    trx_data    TABLE     4  CREATE TABLE v1.trx_data (
    id bigint NOT NULL,
    mst_jenis_dok_id integer,
    sumber_data text NOT NULL,
    jenis_data character varying(100) NOT NULL,
    nama_data text NOT NULL,
    deskripsi_data text NOT NULL,
    format_data character varying(100) NOT NULL,
    media_type character varying(100),
    size_data character varying(100),
    ekstensi_data character varying(100),
    nama_file text,
    url_file_upload text,
    url_upload text,
    is_shared boolean DEFAULT false NOT NULL,
    webbappeda_visible boolean DEFAULT false NOT NULL,
    satudata_visible boolean DEFAULT false NOT NULL,
    catatan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.trx_data;
       v1         heap    postgres    false    6            �            1259    16473    trx_data_id_seq    SEQUENCE     t   CREATE SEQUENCE v1.trx_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE v1.trx_data_id_seq;
       v1          postgres    false    6    236            �           0    0    trx_data_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE v1.trx_data_id_seq OWNED BY v1.trx_data.id;
          v1          postgres    false    237            �            1259    16474    trx_data_organisasi    TABLE     R  CREATE TABLE v1.trx_data_organisasi (
    id bigint NOT NULL,
    trx_data_id integer,
    ref_organisasi_id integer,
    organisasi text NOT NULL,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
 #   DROP TABLE v1.trx_data_organisasi;
       v1         heap    postgres    false    6            �            1259    16479    trx_data_organisasi_id_seq    SEQUENCE        CREATE SEQUENCE v1.trx_data_organisasi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE v1.trx_data_organisasi_id_seq;
       v1          postgres    false    6    238            �           0    0    trx_data_organisasi_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE v1.trx_data_organisasi_id_seq OWNED BY v1.trx_data_organisasi.id;
          v1          postgres    false    239            �            1259    16480    trx_data_tag    TABLE     G  CREATE TABLE v1.trx_data_tag (
    id bigint NOT NULL,
    trx_data_id integer,
    ref_data_tag_id integer,
    data_tag text NOT NULL,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.trx_data_tag;
       v1         heap    postgres    false    6            �            1259    16485    trx_data_tag_id_seq    SEQUENCE     x   CREATE SEQUENCE v1.trx_data_tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE v1.trx_data_tag_id_seq;
       v1          postgres    false    6    240            �           0    0    trx_data_tag_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE v1.trx_data_tag_id_seq OWNED BY v1.trx_data_tag.id;
          v1          postgres    false    241            �            1259    16486    trx_data_topik    TABLE     C  CREATE TABLE v1.trx_data_topik (
    id bigint NOT NULL,
    trx_data_id integer,
    ref_topik_id integer,
    topik text NOT NULL,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.trx_data_topik;
       v1         heap    postgres    false    6            �            1259    16491    trx_data_topik_id_seq    SEQUENCE     z   CREATE SEQUENCE v1.trx_data_topik_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE v1.trx_data_topik_id_seq;
       v1          postgres    false    6    242            �           0    0    trx_data_topik_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE v1.trx_data_topik_id_seq OWNED BY v1.trx_data_topik.id;
          v1          postgres    false    243            �            1259    16492 $   trx_luas_wilayah_kecamatan_kelurahan    TABLE       CREATE TABLE v1.trx_luas_wilayah_kecamatan_kelurahan (
    id bigint NOT NULL,
    mst_collection_id integer NOT NULL,
    tahun integer NOT NULL,
    kecamatan character varying(50) NOT NULL,
    jml_kelurahan integer NOT NULL,
    luas_wilayah double precision NOT NULL,
    satuan character varying(50),
    catatan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
 4   DROP TABLE v1.trx_luas_wilayah_kecamatan_kelurahan;
       v1         heap    postgres    false    6            �            1259    16497 +   trx_luas_wilayah_kecamatan_kelurahan_id_seq    SEQUENCE     �   CREATE SEQUENCE v1.trx_luas_wilayah_kecamatan_kelurahan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 >   DROP SEQUENCE v1.trx_luas_wilayah_kecamatan_kelurahan_id_seq;
       v1          postgres    false    244    6            �           0    0 +   trx_luas_wilayah_kecamatan_kelurahan_id_seq    SEQUENCE OWNED BY     s   ALTER SEQUENCE v1.trx_luas_wilayah_kecamatan_kelurahan_id_seq OWNED BY v1.trx_luas_wilayah_kecamatan_kelurahan.id;
          v1          postgres    false    245            �            1259    16498    trx_riwayat_data    TABLE     U  CREATE TABLE v1.trx_riwayat_data (
    id bigint NOT NULL,
    trx_data_id integer,
    mst_jenis_dok_id integer,
    sumber_data text NOT NULL,
    jenis_data character varying(100) NOT NULL,
    nama_data text NOT NULL,
    deskripsi_data text NOT NULL,
    format_data character varying(100) NOT NULL,
    media_type character varying(100),
    size_data character varying(100),
    ekstensi_data character varying(100),
    nama_file text,
    url_file_upload text,
    url_upload text,
    is_shared boolean DEFAULT false NOT NULL,
    webbappeda_visible boolean DEFAULT false NOT NULL,
    satudata_visible boolean DEFAULT false NOT NULL,
    catatan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
     DROP TABLE v1.trx_riwayat_data;
       v1         heap    postgres    false    6            �            1259    16506    trx_riwayat_data_id_seq    SEQUENCE     |   CREATE SEQUENCE v1.trx_riwayat_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE v1.trx_riwayat_data_id_seq;
       v1          postgres    false    6    246            �           0    0    trx_riwayat_data_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE v1.trx_riwayat_data_id_seq OWNED BY v1.trx_riwayat_data.id;
          v1          postgres    false    247            �            1259    16507    users    TABLE     t  CREATE TABLE v1.users (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    email_verified_at timestamp(0) without time zone,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.users;
       v1         heap    postgres    false    6            �            1259    16512    users_id_seq    SEQUENCE     q   CREATE SEQUENCE v1.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
    DROP SEQUENCE v1.users_id_seq;
       v1          postgres    false    248    6            �           0    0    users_id_seq    SEQUENCE OWNED BY     5   ALTER SEQUENCE v1.users_id_seq OWNED BY v1.users.id;
          v1          postgres    false    249            �           2604    16513    failed_jobs id    DEFAULT     h   ALTER TABLE ONLY v1.failed_jobs ALTER COLUMN id SET DEFAULT nextval('v1.failed_jobs_id_seq'::regclass);
 9   ALTER TABLE v1.failed_jobs ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    216    215            �           2604    16514    migrations id    DEFAULT     f   ALTER TABLE ONLY v1.migrations ALTER COLUMN id SET DEFAULT nextval('v1.migrations_id_seq'::regclass);
 8   ALTER TABLE v1.migrations ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    218    217            �           2604    16515    mst_collection id    DEFAULT     n   ALTER TABLE ONLY v1.mst_collection ALTER COLUMN id SET DEFAULT nextval('v1.mst_collection_id_seq'::regclass);
 <   ALTER TABLE v1.mst_collection ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    220    219            �           2604    16516    mst_dashboard id    DEFAULT     l   ALTER TABLE ONLY v1.mst_dashboard ALTER COLUMN id SET DEFAULT nextval('v1.mst_dashboard_id_seq'::regclass);
 ;   ALTER TABLE v1.mst_dashboard ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    222    221            �           2604    16517    mst_jenis_dok id    DEFAULT     l   ALTER TABLE ONLY v1.mst_jenis_dok ALTER COLUMN id SET DEFAULT nextval('v1.mst_jenis_dok_id_seq'::regclass);
 ;   ALTER TABLE v1.mst_jenis_dok ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    224    223            �           2604    16518    personal_access_tokens id    DEFAULT     ~   ALTER TABLE ONLY v1.personal_access_tokens ALTER COLUMN id SET DEFAULT nextval('v1.personal_access_tokens_id_seq'::regclass);
 D   ALTER TABLE v1.personal_access_tokens ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    227    226            �           2604    16519    ref_data_tag id    DEFAULT     j   ALTER TABLE ONLY v1.ref_data_tag ALTER COLUMN id SET DEFAULT nextval('v1.ref_data_tag_id_seq'::regclass);
 :   ALTER TABLE v1.ref_data_tag ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    229    228            �           2604    16520    ref_grup_dok id    DEFAULT     j   ALTER TABLE ONLY v1.ref_grup_dok ALTER COLUMN id SET DEFAULT nextval('v1.ref_grup_dok_id_seq'::regclass);
 :   ALTER TABLE v1.ref_grup_dok ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    231    230            �           2604    24620    ref_kode_wilayah id    DEFAULT     r   ALTER TABLE ONLY v1.ref_kode_wilayah ALTER COLUMN id SET DEFAULT nextval('v1.ref_kode_wilayah_id_seq'::regclass);
 >   ALTER TABLE v1.ref_kode_wilayah ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    252    253    253            �           2604    16521    ref_organisasi id    DEFAULT     n   ALTER TABLE ONLY v1.ref_organisasi ALTER COLUMN id SET DEFAULT nextval('v1.ref_organisasi_id_seq'::regclass);
 <   ALTER TABLE v1.ref_organisasi ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    233    232            �           2604    16522    ref_topik id    DEFAULT     d   ALTER TABLE ONLY v1.ref_topik ALTER COLUMN id SET DEFAULT nextval('v1.ref_topik_id_seq'::regclass);
 7   ALTER TABLE v1.ref_topik ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    235    234            �           2604    24604    trx_collection id    DEFAULT     n   ALTER TABLE ONLY v1.trx_collection ALTER COLUMN id SET DEFAULT nextval('v1.trx_collection_id_seq'::regclass);
 <   ALTER TABLE v1.trx_collection ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    250    251    251            �           2604    16523    trx_data id    DEFAULT     b   ALTER TABLE ONLY v1.trx_data ALTER COLUMN id SET DEFAULT nextval('v1.trx_data_id_seq'::regclass);
 6   ALTER TABLE v1.trx_data ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    237    236            �           2604    16524    trx_data_organisasi id    DEFAULT     x   ALTER TABLE ONLY v1.trx_data_organisasi ALTER COLUMN id SET DEFAULT nextval('v1.trx_data_organisasi_id_seq'::regclass);
 A   ALTER TABLE v1.trx_data_organisasi ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    239    238            �           2604    16525    trx_data_tag id    DEFAULT     j   ALTER TABLE ONLY v1.trx_data_tag ALTER COLUMN id SET DEFAULT nextval('v1.trx_data_tag_id_seq'::regclass);
 :   ALTER TABLE v1.trx_data_tag ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    241    240            �           2604    16526    trx_data_topik id    DEFAULT     n   ALTER TABLE ONLY v1.trx_data_topik ALTER COLUMN id SET DEFAULT nextval('v1.trx_data_topik_id_seq'::regclass);
 <   ALTER TABLE v1.trx_data_topik ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    243    242            �           2604    16527 '   trx_luas_wilayah_kecamatan_kelurahan id    DEFAULT     �   ALTER TABLE ONLY v1.trx_luas_wilayah_kecamatan_kelurahan ALTER COLUMN id SET DEFAULT nextval('v1.trx_luas_wilayah_kecamatan_kelurahan_id_seq'::regclass);
 R   ALTER TABLE v1.trx_luas_wilayah_kecamatan_kelurahan ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    245    244            �           2604    16528    trx_riwayat_data id    DEFAULT     r   ALTER TABLE ONLY v1.trx_riwayat_data ALTER COLUMN id SET DEFAULT nextval('v1.trx_riwayat_data_id_seq'::regclass);
 >   ALTER TABLE v1.trx_riwayat_data ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    247    246            �           2604    16529    users id    DEFAULT     \   ALTER TABLE ONLY v1.users ALTER COLUMN id SET DEFAULT nextval('v1.users_id_seq'::regclass);
 3   ALTER TABLE v1.users ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    249    248            �          0    16400    failed_jobs 
   TABLE DATA           ]   COPY v1.failed_jobs (id, uuid, connection, queue, payload, exception, failed_at) FROM stdin;
    v1          postgres    false    215   2�       �          0    16407 
   migrations 
   TABLE DATA           6   COPY v1.migrations (id, migration, batch) FROM stdin;
    v1          postgres    false    217   O�       �          0    16411    mst_collection 
   TABLE DATA           )  COPY v1.mst_collection (id, nama_collection, judul, tipe, default_chart, referensi_data, catatan, route_name, table_name, created_by, updated_by, created_at, updated_at, ref_organisasi_id, organisasi, satuan, definisi, rumus_perhitungan, cara_memperoleh_data, urusan, is_multidimensi) FROM stdin;
    v1          postgres    false    219   �       �          0    16417    mst_dashboard 
   TABLE DATA           �   COPY v1.mst_dashboard (id, nama_dashboard, frame, link, catatan, is_hidden, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    221   ��       �          0    16424    mst_jenis_dok 
   TABLE DATA           �   COPY v1.mst_jenis_dok (id, jenis_dokumen, ref_grup_dok_id, keterangan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    223   ��       �          0    16430    password_reset_tokens 
   TABLE DATA           E   COPY v1.password_reset_tokens (email, token, created_at) FROM stdin;
    v1          postgres    false    225   P�       �          0    16435    personal_access_tokens 
   TABLE DATA           �   COPY v1.personal_access_tokens (id, tokenable_type, tokenable_id, name, token, abilities, last_used_at, expires_at, created_at, updated_at) FROM stdin;
    v1          postgres    false    226   m�       �          0    16441    ref_data_tag 
   TABLE DATA           l   COPY v1.ref_data_tag (id, data_tag, keterangan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    228   ��       �          0    16447    ref_grup_dok 
   TABLE DATA           p   COPY v1.ref_grup_dok (id, grup_dokumen, keterangan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    230   �      �          0    24617    ref_kode_wilayah 
   TABLE DATA             COPY v1.ref_kode_wilayah (id, kemendagri_provinsi_kode, kemendagri_kota_kode, kemendagri_provinsi_nama, kemendagri_kota_nama, bps_provinsi_kode, bps_kota_kode, bps_provinsi_nama, bps_kota_nama, latitude, longitude, kode_pos, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    253   `      �          0    16453    ref_organisasi 
   TABLE DATA           {   COPY v1.ref_organisasi (id, organisasi, singkatan, keterangan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    232   �      �          0    16459 	   ref_topik 
   TABLE DATA           f   COPY v1.ref_topik (id, topik, keterangan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    234   z      �          0    24601    trx_collection 
   TABLE DATA           �   COPY v1.trx_collection (id, mst_collection_id, tahun, target, realisasi, sumber, catatan, created_by, updated_by, created_at, updated_at, kemendagri_kota_kode, kemendagri_kota_nama) FROM stdin;
    v1          postgres    false    251         �          0    16465    trx_data 
   TABLE DATA           -  COPY v1.trx_data (id, mst_jenis_dok_id, sumber_data, jenis_data, nama_data, deskripsi_data, format_data, media_type, size_data, ekstensi_data, nama_file, url_file_upload, url_upload, is_shared, webbappeda_visible, satudata_visible, catatan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    236   �O      �          0    16474    trx_data_organisasi 
   TABLE DATA           �   COPY v1.trx_data_organisasi (id, trx_data_id, ref_organisasi_id, organisasi, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    238   D�      �          0    16480    trx_data_tag 
   TABLE DATA           ~   COPY v1.trx_data_tag (id, trx_data_id, ref_data_tag_id, data_tag, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    240   ��      �          0    16486    trx_data_topik 
   TABLE DATA           z   COPY v1.trx_data_topik (id, trx_data_id, ref_topik_id, topik, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    242   F�      �          0    16492 $   trx_luas_wilayah_kecamatan_kelurahan 
   TABLE DATA           �   COPY v1.trx_luas_wilayah_kecamatan_kelurahan (id, mst_collection_id, tahun, kecamatan, jml_kelurahan, luas_wilayah, satuan, catatan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    244   R�      �          0    16498    trx_riwayat_data 
   TABLE DATA           B  COPY v1.trx_riwayat_data (id, trx_data_id, mst_jenis_dok_id, sumber_data, jenis_data, nama_data, deskripsi_data, format_data, media_type, size_data, ekstensi_data, nama_file, url_file_upload, url_upload, is_shared, webbappeda_visible, satudata_visible, catatan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    246   ,�      �          0    16507    users 
   TABLE DATA           q   COPY v1.users (id, name, email, email_verified_at, password, remember_token, created_at, updated_at) FROM stdin;
    v1          postgres    false    248   �      �           0    0    failed_jobs_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('v1.failed_jobs_id_seq', 1, false);
          v1          postgres    false    216            �           0    0    migrations_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('v1.migrations_id_seq', 27, true);
          v1          postgres    false    218            �           0    0    mst_collection_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('v1.mst_collection_id_seq', 284, true);
          v1          postgres    false    220            �           0    0    mst_dashboard_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('v1.mst_dashboard_id_seq', 3, true);
          v1          postgres    false    222            �           0    0    mst_jenis_dok_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('v1.mst_jenis_dok_id_seq', 3, true);
          v1          postgres    false    224            �           0    0    personal_access_tokens_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('v1.personal_access_tokens_id_seq', 1, false);
          v1          postgres    false    227            �           0    0    ref_data_tag_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('v1.ref_data_tag_id_seq', 127, true);
          v1          postgres    false    229            �           0    0    ref_grup_dok_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('v1.ref_grup_dok_id_seq', 2, true);
          v1          postgres    false    231            �           0    0    ref_kode_wilayah_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('v1.ref_kode_wilayah_id_seq', 28, true);
          v1          postgres    false    252            �           0    0    ref_organisasi_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('v1.ref_organisasi_id_seq', 47, true);
          v1          postgres    false    233            �           0    0    ref_topik_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('v1.ref_topik_id_seq', 34, true);
          v1          postgres    false    235            �           0    0    trx_collection_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('v1.trx_collection_id_seq', 985, true);
          v1          postgres    false    250            �           0    0    trx_data_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('v1.trx_data_id_seq', 228, true);
          v1          postgres    false    237            �           0    0    trx_data_organisasi_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('v1.trx_data_organisasi_id_seq', 74, true);
          v1          postgres    false    239            �           0    0    trx_data_tag_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('v1.trx_data_tag_id_seq', 379, true);
          v1          postgres    false    241            �           0    0    trx_data_topik_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('v1.trx_data_topik_id_seq', 29, true);
          v1          postgres    false    243            �           0    0 +   trx_luas_wilayah_kecamatan_kelurahan_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('v1.trx_luas_wilayah_kecamatan_kelurahan_id_seq', 6, true);
          v1          postgres    false    245            �           0    0    trx_riwayat_data_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('v1.trx_riwayat_data_id_seq', 238, true);
          v1          postgres    false    247            �           0    0    users_id_seq    SEQUENCE SET     7   SELECT pg_catalog.setval('v1.users_id_seq', 1, false);
          v1          postgres    false    249            �           2606    16531    failed_jobs failed_jobs_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY v1.failed_jobs
    ADD CONSTRAINT failed_jobs_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY v1.failed_jobs DROP CONSTRAINT failed_jobs_pkey;
       v1            postgres    false    215            �           2606    16533 #   failed_jobs failed_jobs_uuid_unique 
   CONSTRAINT     Z   ALTER TABLE ONLY v1.failed_jobs
    ADD CONSTRAINT failed_jobs_uuid_unique UNIQUE (uuid);
 I   ALTER TABLE ONLY v1.failed_jobs DROP CONSTRAINT failed_jobs_uuid_unique;
       v1            postgres    false    215            �           2606    16535    migrations migrations_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY v1.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY v1.migrations DROP CONSTRAINT migrations_pkey;
       v1            postgres    false    217            �           2606    16537 "   mst_collection mst_collection_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY v1.mst_collection
    ADD CONSTRAINT mst_collection_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY v1.mst_collection DROP CONSTRAINT mst_collection_pkey;
       v1            postgres    false    219            �           2606    16539     mst_dashboard mst_dashboard_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY v1.mst_dashboard
    ADD CONSTRAINT mst_dashboard_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY v1.mst_dashboard DROP CONSTRAINT mst_dashboard_pkey;
       v1            postgres    false    221            �           2606    16541     mst_jenis_dok mst_jenis_dok_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY v1.mst_jenis_dok
    ADD CONSTRAINT mst_jenis_dok_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY v1.mst_jenis_dok DROP CONSTRAINT mst_jenis_dok_pkey;
       v1            postgres    false    223            �           2606    16543 0   password_reset_tokens password_reset_tokens_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY v1.password_reset_tokens
    ADD CONSTRAINT password_reset_tokens_pkey PRIMARY KEY (email);
 V   ALTER TABLE ONLY v1.password_reset_tokens DROP CONSTRAINT password_reset_tokens_pkey;
       v1            postgres    false    225            �           2606    16545 2   personal_access_tokens personal_access_tokens_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY v1.personal_access_tokens
    ADD CONSTRAINT personal_access_tokens_pkey PRIMARY KEY (id);
 X   ALTER TABLE ONLY v1.personal_access_tokens DROP CONSTRAINT personal_access_tokens_pkey;
       v1            postgres    false    226            �           2606    16547 :   personal_access_tokens personal_access_tokens_token_unique 
   CONSTRAINT     r   ALTER TABLE ONLY v1.personal_access_tokens
    ADD CONSTRAINT personal_access_tokens_token_unique UNIQUE (token);
 `   ALTER TABLE ONLY v1.personal_access_tokens DROP CONSTRAINT personal_access_tokens_token_unique;
       v1            postgres    false    226            �           2606    16549    ref_data_tag ref_data_tag_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY v1.ref_data_tag
    ADD CONSTRAINT ref_data_tag_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY v1.ref_data_tag DROP CONSTRAINT ref_data_tag_pkey;
       v1            postgres    false    228            �           2606    16551    ref_grup_dok ref_grup_dok_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY v1.ref_grup_dok
    ADD CONSTRAINT ref_grup_dok_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY v1.ref_grup_dok DROP CONSTRAINT ref_grup_dok_pkey;
       v1            postgres    false    230                       2606    24624 &   ref_kode_wilayah ref_kode_wilayah_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY v1.ref_kode_wilayah
    ADD CONSTRAINT ref_kode_wilayah_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY v1.ref_kode_wilayah DROP CONSTRAINT ref_kode_wilayah_pkey;
       v1            postgres    false    253            �           2606    16553 "   ref_organisasi ref_organisasi_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY v1.ref_organisasi
    ADD CONSTRAINT ref_organisasi_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY v1.ref_organisasi DROP CONSTRAINT ref_organisasi_pkey;
       v1            postgres    false    232            �           2606    16555    ref_topik ref_topik_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY v1.ref_topik
    ADD CONSTRAINT ref_topik_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY v1.ref_topik DROP CONSTRAINT ref_topik_pkey;
       v1            postgres    false    234                       2606    24608 "   trx_collection trx_collection_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY v1.trx_collection
    ADD CONSTRAINT trx_collection_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY v1.trx_collection DROP CONSTRAINT trx_collection_pkey;
       v1            postgres    false    251                        2606    16557 ,   trx_data_organisasi trx_data_organisasi_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY v1.trx_data_organisasi
    ADD CONSTRAINT trx_data_organisasi_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY v1.trx_data_organisasi DROP CONSTRAINT trx_data_organisasi_pkey;
       v1            postgres    false    238            �           2606    16559    trx_data trx_data_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY v1.trx_data
    ADD CONSTRAINT trx_data_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY v1.trx_data DROP CONSTRAINT trx_data_pkey;
       v1            postgres    false    236                       2606    16561    trx_data_tag trx_data_tag_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY v1.trx_data_tag
    ADD CONSTRAINT trx_data_tag_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY v1.trx_data_tag DROP CONSTRAINT trx_data_tag_pkey;
       v1            postgres    false    240                       2606    16563 "   trx_data_topik trx_data_topik_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY v1.trx_data_topik
    ADD CONSTRAINT trx_data_topik_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY v1.trx_data_topik DROP CONSTRAINT trx_data_topik_pkey;
       v1            postgres    false    242                       2606    16565 N   trx_luas_wilayah_kecamatan_kelurahan trx_luas_wilayah_kecamatan_kelurahan_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY v1.trx_luas_wilayah_kecamatan_kelurahan
    ADD CONSTRAINT trx_luas_wilayah_kecamatan_kelurahan_pkey PRIMARY KEY (id);
 t   ALTER TABLE ONLY v1.trx_luas_wilayah_kecamatan_kelurahan DROP CONSTRAINT trx_luas_wilayah_kecamatan_kelurahan_pkey;
       v1            postgres    false    244                       2606    16567 &   trx_riwayat_data trx_riwayat_data_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY v1.trx_riwayat_data
    ADD CONSTRAINT trx_riwayat_data_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY v1.trx_riwayat_data DROP CONSTRAINT trx_riwayat_data_pkey;
       v1            postgres    false    246            
           2606    16569    users users_email_unique 
   CONSTRAINT     P   ALTER TABLE ONLY v1.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);
 >   ALTER TABLE ONLY v1.users DROP CONSTRAINT users_email_unique;
       v1            postgres    false    248                       2606    16571    users users_pkey 
   CONSTRAINT     J   ALTER TABLE ONLY v1.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 6   ALTER TABLE ONLY v1.users DROP CONSTRAINT users_pkey;
       v1            postgres    false    248            �           1259    16572 8   personal_access_tokens_tokenable_type_tokenable_id_index    INDEX     �   CREATE INDEX personal_access_tokens_tokenable_type_tokenable_id_index ON v1.personal_access_tokens USING btree (tokenable_type, tokenable_id);
 H   DROP INDEX v1.personal_access_tokens_tokenable_type_tokenable_id_index;
       v1            postgres    false    226    226                       2606    16623 7   mst_collection mst_collection_ref_organisasi_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.mst_collection
    ADD CONSTRAINT mst_collection_ref_organisasi_id_foreign FOREIGN KEY (ref_organisasi_id) REFERENCES v1.ref_organisasi(id);
 ]   ALTER TABLE ONLY v1.mst_collection DROP CONSTRAINT mst_collection_ref_organisasi_id_foreign;
       v1          postgres    false    219    232    3322                       2606    16573 3   mst_jenis_dok mst_jenis_dok_ref_grup_dok_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.mst_jenis_dok
    ADD CONSTRAINT mst_jenis_dok_ref_grup_dok_id_foreign FOREIGN KEY (ref_grup_dok_id) REFERENCES v1.ref_grup_dok(id);
 Y   ALTER TABLE ONLY v1.mst_jenis_dok DROP CONSTRAINT mst_jenis_dok_ref_grup_dok_id_foreign;
       v1          postgres    false    230    3320    223                       2606    24609 7   trx_collection trx_collection_mst_collection_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_collection
    ADD CONSTRAINT trx_collection_mst_collection_id_foreign FOREIGN KEY (mst_collection_id) REFERENCES v1.mst_collection(id);
 ]   ALTER TABLE ONLY v1.trx_collection DROP CONSTRAINT trx_collection_mst_collection_id_foreign;
       v1          postgres    false    251    219    3305                       2606    16578 *   trx_data trx_data_mst_jenis_dok_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_data
    ADD CONSTRAINT trx_data_mst_jenis_dok_id_foreign FOREIGN KEY (mst_jenis_dok_id) REFERENCES v1.mst_jenis_dok(id);
 P   ALTER TABLE ONLY v1.trx_data DROP CONSTRAINT trx_data_mst_jenis_dok_id_foreign;
       v1          postgres    false    3309    236    223                       2606    16583 A   trx_data_organisasi trx_data_organisasi_ref_organisasi_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_data_organisasi
    ADD CONSTRAINT trx_data_organisasi_ref_organisasi_id_foreign FOREIGN KEY (ref_organisasi_id) REFERENCES v1.ref_organisasi(id);
 g   ALTER TABLE ONLY v1.trx_data_organisasi DROP CONSTRAINT trx_data_organisasi_ref_organisasi_id_foreign;
       v1          postgres    false    3322    238    232                       2606    16588 1   trx_data_tag trx_data_tag_ref_data_tag_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_data_tag
    ADD CONSTRAINT trx_data_tag_ref_data_tag_id_foreign FOREIGN KEY (ref_data_tag_id) REFERENCES v1.ref_data_tag(id);
 W   ALTER TABLE ONLY v1.trx_data_tag DROP CONSTRAINT trx_data_tag_ref_data_tag_id_foreign;
       v1          postgres    false    3318    228    240                       2606    16593 2   trx_data_topik trx_data_topik_ref_topik_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_data_topik
    ADD CONSTRAINT trx_data_topik_ref_topik_id_foreign FOREIGN KEY (ref_topik_id) REFERENCES v1.ref_topik(id);
 X   ALTER TABLE ONLY v1.trx_data_topik DROP CONSTRAINT trx_data_topik_ref_topik_id_foreign;
       v1          postgres    false    242    3324    234                       2606    16598 c   trx_luas_wilayah_kecamatan_kelurahan trx_luas_wilayah_kecamatan_kelurahan_mst_collection_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_luas_wilayah_kecamatan_kelurahan
    ADD CONSTRAINT trx_luas_wilayah_kecamatan_kelurahan_mst_collection_id_foreign FOREIGN KEY (mst_collection_id) REFERENCES v1.mst_collection(id) ON UPDATE CASCADE ON DELETE CASCADE;
 �   ALTER TABLE ONLY v1.trx_luas_wilayah_kecamatan_kelurahan DROP CONSTRAINT trx_luas_wilayah_kecamatan_kelurahan_mst_collection_id_foreign;
       v1          postgres    false    3305    244    219                       2606    16603 :   trx_riwayat_data trx_riwayat_data_mst_jenis_dok_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_riwayat_data
    ADD CONSTRAINT trx_riwayat_data_mst_jenis_dok_id_foreign FOREIGN KEY (mst_jenis_dok_id) REFERENCES v1.mst_jenis_dok(id);
 `   ALTER TABLE ONLY v1.trx_riwayat_data DROP CONSTRAINT trx_riwayat_data_mst_jenis_dok_id_foreign;
       v1          postgres    false    3309    223    246                       2606    16608 5   trx_riwayat_data trx_riwayat_data_trx_data_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_riwayat_data
    ADD CONSTRAINT trx_riwayat_data_trx_data_id_foreign FOREIGN KEY (trx_data_id) REFERENCES v1.trx_data(id) ON UPDATE CASCADE ON DELETE CASCADE;
 [   ALTER TABLE ONLY v1.trx_riwayat_data DROP CONSTRAINT trx_riwayat_data_trx_data_id_foreign;
       v1          postgres    false    3326    236    246            �      x������ � �      �   �  x�e�ݎ� F��Ì�����H��2MCD�}�5ͦ	�(�rq�m'� 5p����Ct&;='���<�0܃P�O��+D��K.��n�DQ�^�N�o%^���?��r�K�\p��P=a2�6��R:)2Qh޼�F	ޯft}��S�p'��HE���Jz�������Pj@*������N�=��Й&yb�W���_`k������F���gA��q3h� P����j5,���K%�t�-6��gfM���������ː��?{B�l�n[����_��.�3n��P����ه�$�����Jg��ˏp�w7��L�7����tR�I�﷓Gm���['�g��C�q�ur5Y\�.e����|�Z�r�Q�X4Z��<fo��6�͎��L
վ��3l>0r�Nt��cց��Z)w���{�n�����x_Y�      �      x��}�n�H���+�2`�Ⱥ�$�5�nYAR��$X]�D�.Yl������_�q�+�d]��Y
FD�##"##��˳���*}��ͲY6�ٿ?{���gQ���z'&�������g2J�bt)�i4��J���E_��䛴Ƃ��Q��Ӎ�E��?n�U�V�it�U��t���z��Cr1>��vY��鎸Tٺl�eݧ�*[g�v�
���(�2�����hҠ�e.Z��I������(KL�׋�Z��S����*���<��7iq�:���:kuv��t�]�ஊn�;����-���{�~�e�������f\�xœ���aAF_~�/'g�.�FiQg�
��n�U� 4�(���*��x��W��U|��6��:�v&�����Hq"��:/)�k�M�{����ݲ�\�zÍ_%��?E����2��&,�e^����;���������.m}���
O� �
�Gg7ˬ�a@`��5�:�൉�����MS5��6]4e��6�8���Z�c�A�$>�Ni�5u��w0�]^8k�{�37&gv���"}N��)����}����|ڤ�v��Q4bG4�thp4��Q����8 ~.�&a�ۊ��3Zg�����̻CTZ,��`,n���͖��)B��n$�^�U���YA��{�y�A�`���;�)D�P��!5��M�hѤ�vJ[��j�mmš�nu�#����g���«����M��(�wl��Ϳ)���q6U���cf�Y�m���"��bD���W������.�&��D���,��r�n��#
�wK� [Nj�܌�V����H�z��|�%�q[��~��4��x�җ�Ӣ�Z)��T0r@ɔ�!�d�?��a�09��~m���/+�v�V�M��� RN����/4:���D��P߼g��5>��=�GT���Ͷh@a%9Os2�\E����w7,�v�2�A�%uNF����p��~޾�y�n�Ζ �\�ӺQ�_�K-q�m�@�e*����u��W*���+����8W,_�|%�k�&y	���A�Do&�h�5����&�*�*a�d6c˄��jN�[�.���\h�,B��b�}ń�;EaKƖK�Ɩ��P��&�MFV_��XÔ\���j�|�PwCI~���U~~�<��Vy�c�8m)���c�j���M�b!YTBѧm���6���(Q�uӑ���!j�sh���"V�4�"
�t+&��TjB���	�"�L8�w��I��66}W5k\�z���"}��Cgs��vs�]��<�89T+6{�b�i6����b�\����b�&�����"��X�K"��¡CFӫ�$;U��>5л��+�b_��~�H��v%܎���_k����c+vѷ��5j�iّ��$Ih$�>I�$����LP�M�W�� �eݻ�����Eܑ���yq���&YZD߱D;��u�ŤZ2�'A�!��Uӑ�Qϣ�t]��k�$(�PzNMtA��"k���0��N�A��x>�aK�C�lʡv�n��:wG�~��F�c	�n������LApa���"����6�I�H�lc7��
t��4�Hr�l��+��B?z�Дy�r�f��u�J7a����ߕ��(�Ż����{�̰���	t7 � ��7�d��v�k�B��'����$KS<3�������7`H��� ��6M��X|@5dQ�����Ȗ��e�tD@D�v���I�I������î�v�N�ҭU{�����Vkljc[o>5ʠH�b=kC�y�}a`o�>K�zd�6��a��
[ǯ���Q�\jx�ZOaiao�4�!����1��^w��/u���o�р�sw.�E���'�6�ׇ�]݋��S1�礙M���&���]�������'�o�3l��4x��mn�K��׊�W!�n'�K���I��U�t�BRvj|W^��ܺ
I9���|�Q�b�Ǡ�� �@e�*[|W)���B��6|�V�ӵ"|�vR��Wr����C��9T+z�޶���roE*nᨸ�+�>�c�i*�#��� �6_4�����POG[Z��
�,0_�Ѱ��-Ș^�$���x�x5�a'�����j7�]�A�3��*L�V���\j��-}�Wi�>� �l�iJ�D��-��H���$c��UVD`�|�;:�Z#�x!{PEP��?j8���$<&O�aC�B���ǿb@fg?Ao�)�F�4/ Pq���f~��o���i����m�VT��T���.,��h,�`I���E��� X��5U����9/U���5b�J��*���"��>��ՙd��	�"��wMh�omO}���<(�/�8�t{ʪX�%Gxh��"����4��W�?���&�G�������t��l*�x�(��$���ʎ���v�lx}�T������ �`f��.��2�QhFo�k��]���8d��b��N솃!	C�۸CB�i<6-�VSpG��[k��y]��G�QdC���ڵ��h�TT�P���V��gV� D���/�ן����4�b�����)��ooh����[��!�SV:�޼�������v�eg���,�W+��IN�s �B ɞU��@�<�Uι��.Q�[
a�A�|D�IM����8L��$W���KsH\�� ya<�Ί%����2��m<��[d`�֑�y�_l���vMױ��J�st���T��y�N�p���KH&����ni�p�Rw��Oq_B㨵vg� /%�
C댭�䝝���H ���w��ZL��;X%�� *�v�T{N@T�-���G St�Y��}]�y�al��˺YG�TMM�-��k��]dM��@C� �h�@�v�D*�D$ɤҐ4�+��C4�y�1,�Y�6F[v�M����s����`�����R�@�������!L���~�}����]\%�0�6�
��9��7�7��&h��g�$}�c�?��٦�D��sM}���@��^��;bT�sx��v��}��z��vۑ��P����n92�����]��Jk�B��[�lG+z�}^Ew���ּP�4�Ě�:qy�I���OfA؛���I���jާ����'X�Y�5�����Q�����	u��2I�Lc���E�$����cu��H���+7z_vV��A�
Wu`��6��)����l��K��p�pdi�a:��a���D_�z����"h�Ѣ������
M(��� E�΅\d��%"����<�-�}O�a�9H��-��`�sff������_e�������hWmt��:��Y�Bs[��"�X��H�F�Q�S��D��'l��RcJ�)���M�[��>֑���� l�����j��@{#���кLw�W��)��GhEH��gr���GH#+��I��^ĵ{��3#{���G�m��`R:�,���$��5W�Vdo�"Q��"E�<C�^��:���ǖ���s�f�����ÿД���A?vЧ^��{��'�q�<_4`�~ȿ�X��U�Ee�jTP&��`����HXRaI
k���CvDs�I��:X�Z	0���ݵ3���6� ���}�lg���X��1j+��� �C;E�%��l��JF>��C���=<=��1���z�U��ؑ�&�7f��"��wE w����	��%Fӯ��5�-T��E+V�����n�fJ�筑'C�2�0^sE�PL�f��°��D����R�����O�of��b��3�٥�Q֨cEw��sX�y�c砈��F��Q��9$&B1i]�t:�R�3C���r��J�8��N������[.<�&�}���>XEv�����p���5eN&�9Y�?�Vq *��0x&�& �^�^M:�D��X~q2՞�����`�]�ն���:5�4�XL��`>K��?V>����#���y��/�h��"u�)�;O`M,��G[m��+��Z#�vS�R�T��8k�}��nU�%<:�gZ4��.=7�����>wA'[G�����@��~�5��lT�*�V��&��\uEη{�7�a�;bD��Y[\A�    Z*�+��T	UD��bd�?.º��ʦ�"\���8)��-�ƹ;����w\�mɿ�ͽ�Wx-,_���x.i6;�α�����c�E�9�A��!ta�c�U��)L��#�a�Ӝe�J��$�LO�*AR	Ҕ ��q��#��J���i֕1�[����v;�y鑐� ����0uM��x���v1��H�-�uAf@T�o7�0���h+�wy5괙a�K�9h��4;� �o�u�j�BT�ݸy�T_���&�G�} v��6{��B��WB�g�i��y���|�w����[�М�'�*7KX��*�H�j󗁊�i��;���%r G�nؖ{%����ٸ���'I��>��a{���1��ޯ�dvM~�*�*��`
������~hи}t�a��KcX�`,�,��:Oaף�p0j�r`�T���R����E0A\r��q�E0X�T2�$R��Ґ�慤�In �@�a3�M�Tʀ��@5�_S�W¾I6 ޲~�#�E�oj�X��v����Gh>��p2���5��D�JM+���hZ����r��ϼE�¦&cP�=�T�m �s����-���n`R����A�t��w���;!R��*�r��g
�h���`��f��u�����v�����;�5���(���G_`�rN-\�#G��w��Wh�f&X�B��i�N�2ò��U���1ԇX&�~U���SW�}Ҥ{c��|���6]I�(PL�Jt���@1&+�-́�H��S�����V��W�d�'�j���m�,QMZ�^FMU}�Tw�^�ta�����9tm�H�91�(��x;�񩺤m���7.U��W�$;0I���yj>-�/�sf���p��z=� K��솹�SՃ��.�n�RNU�ǜ�n���-+��jC{<z��J�V��zu[ނx������ם��:��B�2��P�ƌ:ĥ<�{u��<l�-��+��T}nSrй���3-;Mޭ�n4��w�05��鰱w���^�a��q~|��'�������@}'����&c��s�����h�Yr���Ǐ����̣v���p�#���؍p��Mԉ�80���a��{U���h�}��vME�3&�v��-�s�����UA���0l[��[�rW{eh^R�W����^p�O�|.�m�3�~�N�SR��g�v���۝��N���]q���>�"h��p3'��>�W^V���n���f�'ht�K�L�SO�M�]ŝ�Z{e�P7���,0���/�:��q�/�F�Y��wީ��1�¹����8d�I�
���I@�
7���� 4��M��j�����{� �3����V�(����6�ދ���S�b� �=����6�+�'����UQ�?n7�&�Bn=�I��o�"��cFq���p��5le�!B�Y��<$J��E#Îj��� �	��Jp�&���N�27����!������&�� /V#�uc��!�C�j��2��3r��e&'-
���ړ �q���j�x�w2
�i�!;��w��3υ���j�lOk�}s\S��v*āF^x��F�7��Rȁl�^��y!]6�PM'���jd� 4x����Q����&�P�K���v��?9���垡Y��"�e5X  ƛ����BS�&M"5I��͝a��F�t?�!w��jG&L`z�?�#LI �Gr���7��Ykk4�Ib��>�w�1>��Dg�b���rf[���S��M��C=�Mo��3܈�҇8��gN�#D�=~f�D�:u�&��L>�Q�� 쨩��K%5��oӺ��'���s�8�.tQ ���Fwn��$I8R��ꢵ�Xh��G���
����8QhY{��[��eow=�L� �Q�������=}p�r�S�ѻ�U���[v��>@ә��LG�O�t�Y�jL�����A�,v�[&�m,@?������+�`�xl���EX��&k��y��#�|�%ð�ݼE{�/s�(�6��TZR����һg�����8M`C��_U�F�ڮ�-̘��ȷ��6��B�j(�4�,���	/tІB��(���{�^	;�Mz�J�4�B�hp�j74��C����P#:95l2f���7��)2���;Z��B��@�D�M�]��"���x\� ܖ�$#�^zĬ���7���������G1ҟ�``2k'�@G\ɹ#���>b��ʕ*y�a�#�EW��j��DH s�ޔ���T8#��-�'!�#�'� ��L�^l�.|
��l�_o����1�{�b�BӉ1�/�ǰvC��|�5��R9�{��������b�p�*/�ܤ����`�_�N@�g��n�ztԖ�rЇBq�s,߹�60�LgHE�GV{�<y8�Y���8���*��=ә�=�R笅���q�e���Rӫg�(�!ы���跆H�E��H��N�_��7RO���]�e��t����W��D�㦹�Џ�\�Za�ʫn�K�9>z��n�_��$�Nݎ����v#~c4�4�,�Z�E)���ѷ��`�K;��a�ƀ^��|��6IbgG�9 D�)��x2U�5g��Ãm�� �� �H!'��0o" �@��#�Zj�c!���VC'=:��l��m��9��v�-6�R� ���#lKD�l4C����Ɓ��&N�gh�5Ǐh��"�E��&;�}�0���N���Dln=
���R�߇0Mm;iv��Dɥ���a-Q+�ř�=!Ra,�n{�>�U�'�xd��[g�8�KzCI���{�ð�6�pf���[��GwHo(@��<э�v��������*�BP|�����^��;�����٧��l�ah����nT�
q�Z㈺T�i�A��>��=�����u�<>B�����C@L��林���g�Sy���I��������'���Df���� �3��Ļ���N�O��i��MC�k>mG�(ء��^���~-��S���A&՘q7TN���A�x<����C�K׃�4g�`���90��@g�E�׾�=�\I/Y����x6iUl{�L��y4E�Y�h�G����v���-�����L�g<��3��T�+G���b
�u�5� ��2��YK���a{y�;��Ei�pB�%-��@)p�}>�aoO:����G#k�E�>����a"�k���<�y���[�;4�Zo�)��Y����ku�Pd8H4���D4�Jˠ脦S�C�L
Y%XP�R#+�2���nG�W����4d	SO�0B��0KSNzZ�v��2c�=E�	�^������$I�H�D�㧫ra�:���J�j�g����~�m�p�����L�B�
��n4%�N�iœqk��
�J��%�5 I���n����x|�0�T�Q*��/�����m ��MiEr�;�,��z J�^�����WΥ���F��s=O	��-ʨ�s-O���_�v�0��4�@��n�!��ѣ���Q�+2|�	����*�/㤄g$�v���0��N�����!��3��R꜈�r�Ǧ���`��&3���\�֤M�@"�boHi�D_v�8����Я�w#&v��uq9�� �3x������������p�����2��2 `��<;�������X�}u23��=�ߘ�."n�vxg�:VF�!�K�nh��j���W�%q��Ћ���N�k�������K\�!�Ç
+�!��am�!k��!�:p�'^�*v�HcL������j�)v�N�v4 A!]��e�>�]qz�^��9�7�b�D�Zv�N%�F�bL�D�T�h�)U����@���+�b�괓j�(KQ��Q)�S�]K�\�>�Ga��;��G��3>>=�E�×!l|r�ƻV��H��2�?��<壑7���q]�I��oo�4؂�p�]��x���M��i������;��B����m�i�m{��y�f���R�Ct2�d&�g��7��i`Yt�߃��tnz?����tմ� D�� V  ��Z�Q�.�7j='��T�o�z�R���=�+�o�58�7i��y��Y��:�C��ط�ۺ��#;��aF����t�Ys�Lك�|�>�`�*���D2R����8��+�@U/��S,P�`��(�}���<�ͫ�z��
{_^Q�7�~	R#��u�C��4��^Ot[�����)vS��/u���q,���M�e��o3Gq3�ɫ�؇�C�9�<����ɐ�m���ލ�P��Dݻ����J����Q����~� :0��D�x��m�?;��dt�G�S�rO���^�)��?�|�%�wUS`��Y�*�4N�wP��N����J4�E�/�$��< ���y�^���D�~�L���yj�0���wA؁��kx2Ř��3�j#ޅ8��vV�� �|�.�8�b��o�m�~�T�ĉ	9�D��QY�^��S<k������g0u�n���lQ�E��Q��YWgu��>=�������RY�y�s�������`
��³��"�]���s���c�)N����I*����3�����%�?0�S/'�{�צ�ɡ���xD��$�#�Z|P��p��N!&���Jrʰ��M�7#N���x\x��mƃ�$u�C��'��"�+���擒�+������Wݏ��Җ&LiΓ��I0'^L����p�mb�F6�m&d#��	�FIl�|�}�L���@�z��� [ �K�EԾ�k�p�zB"�X\ɸ꽈=�A�ѤAX�c�FE��7���y�,y������'��ԟ�&��&z���l1�P�k�򩇅�<D�l�i�O=3l�$ɲ?뜮o��pc������h�>��t�y9�AA��/F.Rаh�L7��l���aU /�W��4�%[@�u<o�z�=��:%2�Ӂ1�y���M���c��?��g�82VJ���P��S;�@�ԕ�7���5��_<3��z� K�����W�o������)����f!y����2KA,Y�@�B�Y
d'������3��$��uK��!��Cv#����i�ͣŰcN����/��W~g0��t����u~�������A<��z��x*&��.}i:n:�2f�
������sn�
ŖO���`�����%^x����a�S9���kе��|V�Iu��ӕ�����DOL�S�U���M�d4>��Mw�;����Χ����])�a�{�|����E�-�S~8�t����kz�l��*z���Dϓ���n�y׭�`G�z�Xsc�ӛ~���׭�I)�4=�9U������|>��{�����4��fA�Y���(�\/7�#uD�,UIM$5�D"Y���j�|2	�b;� �)��n�LWJ � �� y�g݃j��=�;����)�O,�4(�~�B�� ��YE&\2a�|?�.�4��=2��AX�O��Wu^��;�<��/q��h�����:?���f3�C�a<}����_x)��1���D�Z1���$k�n�z-[��$�Ft�H�8N��b7��K|�	؉�p�%��vլ3��Nd�����~(��	�;�۷�����Փ;������(:���1;I29�E��j���ăʋz��l×�
f ���B.)9�����[�J"��9�=�Rޓ
y�A}Ӷ��a'-�$�{.�Ǜ;����#�'Ff��]�=�2�C��k� �0�¡n��DiH�C����U��0��ޠ�Q�7&�l�5�����O鐙4/=/�G?��m��6װS/�'xc��|�`TQ��j�+�x���c�a�=���?�B �,��B�%)�B4�2�d�!�DF�#E6�9��a��^��Ea|�O�F���3�H�����Իy�W��g������_�g}�m�%%�K°����ܡL�S�k�ipK��U�	qf�@�;�����v�@]t*n�+�<��;T�sA~��(�y>�^�^�D���j�\��s-2�ކO�p�dQ߆QĹ�,��,�â�mb:y.5��ߐN:t����N�O�(�+�'i�J��qxk��ў��7t(-RP���e���O?������B"i�H�"��D.����8��m��85��|�35�����J�N��[3��)ړ���g�q`���H��I����c؝Yu��Ͼ;�Ξ��z{��)ltA؉�Prv�>�D���	u��Oؙ�躱���|�F;_�,�����c�ߐ�`�DX>�o�CD*�T��9Q%Rɤ:L`Ug�T�m�;��Vv�5��C�o������A1��
�l���-��ƪ�&/�#/ P=pᑪ��w��7�kid�����8�qUA���cY�D+l�lK��)KbY�]�%�,i���rYEw$�nt���db<9{RF��/���$	�B�?��5��$>����ᦣD�,_��
���ʯz����}C��,HS��Q�L*f�a&��{
羪�'�lXל�a>��6���qx�"�x�Hwc(��0�&^�M��S��x�n� �Lz:��Pxz�9\ث����)�(���)E��B�kgi��
�Mٷ���.<i7$d%+��&�{���L��R�K��O��W�����Z�h\��O@������A%L��~񩐝>�e��=��`��y���?[57/�=�-��`�Q�i��J.q�/B1k?]U�7���#�����f�Q9ٓI��Ez��v���[���!�EU��M����þcFOۿ,��e��@OO�k�.�X���L��d;�]x�粣�f݄��)"�>QD�CD� �V�B�)�d̞����~����}��C�ȓޚ�0d ����6��9��Q�{��x��-����u�����"T!Zn���;�{o��[�����?����a��      �   j  x����J�@���S��{���#("�Ÿ�:]�q2顧��ot���p6u6��������-D�?����;H.�.����š�5�y6���ɥZ�Ch�|�q��b�~���n����~�U�w�,�"�އ8�}��EmE(@�`H*�h˅�8(��c�<���\�y��b}p���x|���OI�0M��;Nӡ���Eq�L΃�<�']��������rA�&�ʘ�����*KEr�p��%߂KI5p��k"��H�*I�.�Х+�-Oe֛���YE���[æ��,e�����g��q����[L�դ���,=9��*�4k���1�r7�������xU����<cU�˚��i���_S�[q      �   A   x�3��p�4���tq�FFƺf�F�
�&V&VF&�ĸ�8��|�2��W� <(�      �      x������ � �      �      x������ � �      �   	  x���Yo۸ǟy>�y�}ȅ(Q�{j�v*�ɽ`^�XMU[��%A��ʋ$�3@������,d�i�q���t"�"���}'��Od2z����i����P���W1`�M��u�D�q��r�����3�h6S[����L��F�a7���.0_ŀ��(�<�����>.z��{ӗs��[*8��Hi("ǿ��gQ%�Q�Qt�V1����ȣ�U�ﲁV�)��6���G�;�&eN`[�G��!l��d=��4ط�6�%Q���R�,.�m��b�����ш>�ƣ�V�s�ԛ��pu�J�x[�{��0F^�;�	�k�p���ʻ]�!J�?�m�ҰMf�Ƹ��юΪ�����-�a�89wo)A7�.ۋ��r�������rh[ZLŀ቏��h_���t!q����Ɯ�`̀�d��6�c�W~ۭ���Z7(<��z�j��x8�syB�>�\��5<A�Dt%q��S�K�<0���f��6O��Njme��Y�U���[*�.��W��W�^`;*���*����K�ն��U=C�,@�������:~��c�9����]�uf �e�QZ�"�wcV��&��ܽ>�'�K�<�ݘdIT������R��w\��W�?�0q��W3���F���b`����4Dogpː����b�1��p�����6�T1@?��?TE)vBfU!��o�a���r��Gy&zs��ą�?�q1���⯸6��r*L��!�d9
��%F5O���ȃ�ڽ=B5����&n�&��0�쳳YG?�<���8�vyo��65\�-K��r�r>��Kb��0�\��&��,�QUp����e������&��8i3n�0�d)������wZ�=g�N�bvNG� 'x3����7��i�`:�g�W	:g�*0o*�J��`f��]��`�8ƚ�����V��[x9��+���	K�8�x�G� ��F����F=Wŀ���0J�Ωzc���iP��N�^.2Y�uX�D��U�H�D��"�p�Q�N*�A6!+���y*xX6X��o1��(�c"���c�a��x����q�{X�bM(�0�ϛ����Q|
������ C����K"ri0_�T�-,��jj�*v�{C��)�*�K���m���y�ꯟ��ט���<���Q7v�����溾���0p���Jo��$��5X_��4����a�Xd��q��q_ jX7� 8\�Dz{W���Ƀ(��V���]��S�U���ِ5�����(�޾z�Ő^�W1p�Zh�����u8�8��k)`ص���kH��P��m��sqb����צ;m�T1p1k��Sa�a˨f��	Gz�˵B���	w4x�xi���ז�8��C#�ː�%�UO�z.L��Z\�,ףak�mw������{�:,�[���A�W�?�S�c65G��g�e\�g�^�lq9�m���ǱɡJ��s�����b�:�-��YAQF	��ه�@�&���'}e�E�i�e;<�L��Uj��C�*lx�v�� |jj5W��GV�o����x�D��o��O-�L�Ǚ�x��m"znb/���ơ��7�R�E��IXƭD��g�����{�k�4->�lU����ScܠדZ3�-2�ei���1Q1�9yI�(/� )h(v��:�v6�b��$��[��jD���0��1�eQ�;F�@���|��W)tQ���]�lÍ���0�=,��7���Sk~��k���k�W8��3���RT:�x[u��~}l�a5�b(`�Wv�űY;k1T��N���{�f�;@��
���s2�/&�z��e�'�8E���~��j����#z7M?�<Ky���3��O���ET��NܖV+�l1��K�1��,/��M'�n1���A���h˫�Q��U�bx�	G+]/o�뉡�O6%�{�{��d*�K|���6��N��|Yŀa��t�������_t��2�kX���U1c�gn���S��a����zkԓ��.ZM�R�{9Z��v6'��K�<��t����d�(T����)?�蓮���}S��I����o)X�G�T�X��ܭCkj�I���̬d�y?�e�u6�)��(�U%�b�M��MO^�Z~;m14w�f4papiܚ�C���z�2�����3��X>D,�[�_���Ҹ��k�33���f���ᴒ�B�$`+D����i��5���;�V1��>������	���V���K�0~��Hr��}���z"��e���_ �)s�i      �   �   x�m��
�0E��+�$:T����RP���ɱKJ5���&E�{��S��p�d�	.֙���}�˸�5�c����;������ò'tP��o��AGxb�m@8"��*�P����Z�V	Gb��D'B���d��՞+г���'QMe���c�\s�ߧ�;}      �   l  x���Qn"1�����h5v�8yb)e��Q������Ĕ�LWE����c�P?�=to�3=��9]�CZ�>�O��yW��#]�C���I�;��* E�x@�6���������2�7���((̛J�X����!��@<��dXWa���6Z���B5�KN��Be.2aG=j@٠Tn6�+�Ǹo�Ȫ�1xMZ��PoP���t��-�Y�@=��,C`��%��P�+�5]��czOR�>j
�^�p����@ʿC[G.�����}�5k2R�g�7�-o4`���q��4䣠 o�ܿ�ѹ	JA�;���!Co��H��]���e:��*�����9k/�1��P��;�[z��C����&�N�g�yv0�%���
h�}��M��7�(�������9����;i�AAmP�asN��g,����)١������1�o�ͣ���˸��w���F�,��l��(rG�<5�fQP[��x�L�t�Z�\�N����FE䵸�h��9�N���'������܃��u=c�[5S6��گ�ۃQ�.XU�d�'4_���Ԝ^�)�m��|y4���4�5��n@������|j��Hُ6��xH��J�	�2��.��9i3�{��Y=]�����})���zѯ:K�i	V���b�$���+_�Э�#��E3+qu�7��	�K�]��Qx��Gs)��߮���&���Od��`�&@3'�:���Y+��iF5�H��������}����PhK�<��H\YI��T�o�hF$�8�c�m�-��(
�af�*��P�t�?��U��4yoѬ�\B�:@jI�$͂$�Z�-�.���L�1)��y���}}zz����      �   �  x��W�n�6}6�B�)DR�-��ЦiZ#N��e�Re��(���}eKr֌��D>��̜��϶Y�����.x�L�~��o��p��8���w��N$�V_��c�#�����LJ���j���j�K�I�or��ك��6Y��TTh�p�0�\�O�OY������"ko�����o[���C[{�9��l1:�ў*��U9���1�pW��r�Ux����d�/U���P�i�(�.�%��ui*$�U���տ�.�b3Vԥr�w����TХ���:��I���j�f���[�L�QUg|�K�8m�&���?�/���p�"�x�p?T�"칟�̼�_��#bθ��)�<�U����g�Ѕ1ͬ���>(${Ce]��F�w/U�E_Z��xt���w����<�^A$<��n�eđR���z�[��4P�
O�#GR#$uh�W������b��rF�IM1&��50
��j��ˈC�r$SW�Bu4��;���F@�L[ÕWP���bLL]��f��/:�Fq�GO�5Ӫ/��o-f'�����9���k��(��Ʋ]j����Lv��w�}E�m{�1�-��Y�Ү��Di
U��&��bLD�5�؎��Z��!껳�_��1�,������t�![��J	k�<Se�>�l^Ԃ���`�LC���o�Xc8���#M�z���R��rt�=��5�n�����NyU�p�!�G2C`QZg�u^k;���@:F����9�X��_9^ދ����p:zQ:*\Ι�o8n��+r�!ty)�*���t!���m��٢3����#��ǳ;/�T��@�+5vTj̙�O�����`�����Z����'d�'��s��wa�	��i?��\�]�-�e������kg�H���)>`L&'�N��߳�m�%���ÕX�ǅ�hڗ%�K{"�P��8}G��W;pyǅ���h>`,�C~&Q��m��^��i_��f{��i)��<8�V"qa�WOg+~�7�8�X'!q���ta����x�����_�ǻ��߮��a���O�
9C	`:���u�T��˨c8 �-�7�WYpP�JB�m��rœ��]��76�F���=U��~�iܮ�i�S��,�-��0ta,Z|RJ�PJ׍�>�1���}��1�?n�V�      �   �  x���Mo�0�Ϟ_�#��GTU���t���֋ٸ�I⬜D��8�vi�Z1������yg,��
�����K�Y]'�vɵ9�<C�����9�x���N_�
�H��I���:O718z���w��ʳ�ܝg �UUۆT��X�%(v���꒝i�iW�o�C�mr��i��k�خ����,�S�`�NmH�c��Й{�S�vҨ��]���Ύ�;�JS�i"򻄂�{7޺^�	2B�
x�W��R�c�Q�xM�����&O�hjs�UL.�d�~(�OB@�/�����d{�t���� x��:8h��tƗ㬺�Z�6��Hv�K�eӀga�]������m/�����]��<�{=h������@�?�1p�l���L=3,���0[{��v8
I >;L�@`�mh��8A��o1L�Eۘ.��Ϧ��	�Cߒ����5�h��>���k�c�5����	�>x����C)AFVu�@dl��ݱ�=-2�GĒ��0���oqzlM�Y�Ċ}�S�j����i@[|!XL�^���P��i4k��JEdYL��NWT���ن����n��C�)���H~�֜�go���b�ۈ�j����!#J�4�Wg�O�/��B����<��4�} � ��]2      �      x�՝k��u�?�
�%�:u� HcL{ ;�'BʤdFɐ�'��SիVwuջ�݆7מ��ҩ�{W��]k�������z����}x��͋�}��ë?����MU�������Q�>S�����ܓQP���~���}�ٗ������/�)-�ڒ[[R���݇��Ǘo����޾��W���폏޽���?�z�����G����������O�������������O��������������?<�z������˿+ !�����J	��6l�� �w��o�[����_|������#|���㻷�~����Wo>>���A߽����o��y�w������������������'�6S�Gq���P��	���g�O�>i���h�˵)�6��W�/�y���o�}�w�����k�͍%��_C/�9՘SOv|2���r~���0��͸���l��*�?O����H���J&�X�ߙZ�-��+��zR�i텳V�s���_}Q�ul�4��%1p���������ոq��j3w���T:J\?\�R�{�&]|�frg�����l��b�e�r#9i3�WL���N��yh�6�����T�2��Ɗq����N�f��7�Q��,C��P��k/I�!��SR^�wy�fY���jܴ�KJjڬ���Nܫ�S�n�}^�Ԑ�lYkE�ͫ_�c���VE�N.�TYAmn/l��@��Qe�;מ{�jS{�����S֪�@���`�!�x>��Ų��P���ob��F{���Jn_� ���|&�k�/���r1Cm^B:�6}�_~���D_�+��c;m~붌A>n�Z�b�~%��Ou'��ە\�	F���n���ۗm�w�M�#���v�M�l�L��N)�%�=e�_�y���/>�}��ԣ���W�����{��7�޿|�/�<��U�?{��g?�z����>>���7?�{��՛g����?|���`�����w����3��V���w��0Jvx��ϐ���]<C��%���Pz=C[���<�=<C>,:<�{�������`�ӏ���o^?{�����)uxRj�25��(���,��إI�!�!H�g0�|*�tXVu�z�59&��{e�汣�6s��(q{��pVF�˘ 5�툛z�Y�-Ή��6�#�NܥCŴ�M��hٞ~����ץO~�e=��s���	�
jsceS�[� �:+�j�<�5�Xl��Ց�7p��:�Am���l=ࢗXg�x�-W�D���|kk}���
����ch���2�E����̹���E�Mm�����a��?�TY���	L���7�md�-���}�2&���7#���9#һ��#xc�yck��7F~�o̱7Fg3u�?wP�t��ʐ]E�Q�d��zF��5p~�	�\��8����f�p�i�y�f��� j1Aҭrj���\���*A�->y5�Vyv���m�5���x��i��*���[�eоr�?�zӀ���)D�YB>�ZfV�`fe���覤,��"+/j�ʊl	� �/�NQ�c�|p�|���%��k�p�� ok�$'�Fx���|�j'k)Y��f'��;B&=�	|ՀC�C�:�.��2�uh,ph�U�K[a_y�P��[�W���;:�K��%��o�+_�-ճ�oɳ���%}X�w��{ں�;�!/���-Ԁ?���3�cL�Xf_l�.������[l�7����N�R�<��y�b�0Uwp�\s���ॾMr�zs��f�zNAK�����ջ?�����7�#�N�e�w����!�8�;�W�_��.�Qaג���[�(]O7��X;Lp����}T�GP6��g���}<C8��:<C�gX!-Y��3�þ�ӀA��{R�d�^�j�U�'�'����\�m�!��f�QP��Q����O���#xx�9���̹k:�_�2?�1L�4@�c�����p��������Ӏ�w��A�Y9��hF��4���H��b_�4�qT �q��?{=�4��s�XjN����a��i���9�������1D�5�ș֖�ι�u��Ft�oʗ���hs�i��ZҀ%���!Ӕ�u�'�ƺ��O�Cmn�ڪ���$��2�Niv�^c����g-�W��ӀkȵlGmg]K?��l8�����i�Sk:~Wր�������Id�M遮�#:��:�o9�	j��͏NͰ?�G#9:x��������W�!sX�u�Z�4��3�����5�Ҙ�lm��r9S���<b��9
P��N��c��8�GLq�>�1�X��;xĎ�[Z"([�CO��>A����)5gB�ֿ\�25���U�M�f '��x=g�����m/b3����W�Ĝ�I{��ؔ�0�OJ�!6�Ď}b���"���H�38�f�Ԥ�k���2�]M�V
j��T���p˨#��:}�)�4�^�Ͱ�ۤ�m���^A��5�q�HNq/�ȣ��i��u����/%�����0+�4�s��������hǑ�O�ƭnC���#��d��t�YE��aQ�.�?������3�k��?�%�%�◴�,AZ兼�k2�>�%4c+��CKG�0�}2��jLt�]*�'5�0��)��%+~BKȌ�6i�����㳟�=����4@���B9E��L�R�EL�VL�dey��ܔĥᡞN��լ�t��(�_3�:��͍�f���>��P�ynl]P%Sku��q̔1fq�Cn�N����ʙ�ճ1��A[PS&j˷��iǏfi�����mq�N�8�I�)~���ж/��2_�b-@|A�~jbr�o9e?�Ѓ�f��z��ɩ�̓�P?}kKs[ן��ߵ	j�Vkmn�L��M�
PmŖ
G��ɒ�~q�4`��f�Q��-��6����E��-6Äґs8��C�4`_��B��N�WyvgH�[e��OV�,3��Ig��p����E�S�E�� Ö
��j��ak���\��:mL�����ܠŚ�½��2��r:���	��^�(��T���;i�/Ѝ�z���9���cjr������&��yX?wp:�D�1*�gWWC�����N�ʝ�����NNg��BXn�s��a'�&��bS3>c7�*o��2�n0X7Dtp{4�g�j��S͙8�d$�!�e����X��b�2u�a�_���N�P&���$�?�tpr";9�w���rܐX�i�����D�?f���l+��j\1�UX�4>ê����1w`両�o O��c��N��p��|�c��ryleA0&>���4����s���A�N�d���b�|+�1b�����B�rA���i����ׯ֒���`�v����r�uK���t�5k]���9�%	ּuvH�ߴ�����}�jI�=ťC-�N���}/�	�@�é��Vm�˚�Q�|�q�!P�w����h��q^g�6��cD��_�|!�]'���o�����}����~+ȝ�y�����(��SD�k4���.���O�u^�X� ���2� y�{�}Sr��G?��qRo�P��k ��+�Չ'�tp�:`��ʩ�{���OW�u�u�Ss��~~���k�jBF�c����wK�����������*�?���?>{��}�/���o�헯_��y���޿U?�ӿ>��_=�����������>�����o����Ǣ�����H:�/{ϳv���ʰ^�	��H��p�g���P�pӀ���H��?y�^-�=�xex��ɭ(^욦����۳NzS˨���۔������%d�Ψ�b�r99u�\�2���q%y��O~*��j��e��c�ܵoz�,7p'�c���*���mmG��5`[��=����{������<'��d�s�W��<�(���pF]J��΀�4s!1�[����!&�� 7'nq9r.���Y.s`��LH%y��f���y3��|x'6���:j��4�]���m#����M|�ɍ��I֩i�{�Z��:��k��=�c���)AeW��!��⤬�f���KI�oj�/'O���V �,�    �w����{~<@%X��-_:@�IHյ���h#���.�v�ҥa�ޛ�3�\%��.I�к��S���]����v��B	.��;����Yj�Y��
��ȫ5fk�Z��1$��'Z}�t�xK�v������aǙg���Y�n�
7�EK�s�i�0���m���y�gs�4`9�f�JO�d�z`��CW���~E�$��Ӏ騚5ݶYz���$*�t ���~"�:����U�\G�l^�(�֑�#�]�w��Zwo��ߣ�H����fq�t��
j�u��/m�U��)��C�v���No��Z�ب���6B�gvz)F׆h���]]?Y�n6M=YݜvQ���0*np
�%�RU������N6_n6��.{�su�pt�i�$)7W��oD�4N9��w�a�J�J�j���qyl^�:��$�J�<_�HȄdOsULA+��bn��8O�]�.+���������z!�g��(���|��W�ant1H~Xd?,��N�������y����$%or�ɕi�ee3�$�k��J��R$/���1�|*��Z���&�WrG��� ��N?��3ׅ�p%)k�`�lT��i��'�����gl�~��E$f��B(k �&����ᅘ�ɴ�4��o��,�kkse�i�Ҷ�!e�x�M&�nY��?��t���1�� {f�M�ҷj�����Ӏ��v@���5�j�a�\s�_�d������ʲǖ�߸C��MN��J��I�Svi ;��F;`%vS*�if|ߤ�ʲ�F7�Jb�CY�Nعlm7��oq��f�N��'#Rd���T��4;���J�Ŧ��&i��50�<�;����~1����0ZSt�s�&#���{Q-7�ǺGU��O����Nc�E%�¹���ջC�X��w�mQ�S�u�N��@[���uݒν��OC|;��������CX�ܖ���L5���&Y�������N��K�N�0�Q���)��B�VW���.�N�(wsi�;p6�乺|Ȥ�4�%5{�4���P��� u`�\����;�:���}�{�L�i�L �c��ѭ��&���k���p��i�jS��t�r�ԛ*����T3�(%�М5����H6��t:���l�0\J�i��̍Q�i�>��w�Uk�u�N��ʏU�Y���5�#��\})�}�Ͼ�㻷o^�y�3W�l��մ�{��d;��v�B&'������.x8u%��%	J^��$AҀGg�`�<���`�Z��O�mI�b�l��6�ZB�]� ��_ȶK�.R��2���)w1�4Y�"_�ײ���7�}��]���Mp�� Xj��ci���C��U�Dl�]oҀG����)q
\-Ҡ&�n� xb��V�e�.��Ԕ��j�\�C:��~QR�嫑�Gp�@�bs��GOg��C��N1^[{�h��J��L�L\g�q�X�JRt�5b�\��4�v3[���m����É򦁃Y�v��;�hn�6�k��|��gS�f��V��<��4pJZ��ر��3:�z�f������\��4p����4��ǳ�N؉Ů��}Zճ�C�ZU��汎Q�[B���)�nɒ�&�9�b�n����M�����\��c��n�\���+Y$d��՜k��۫
�g撼�s��)B���6{�k��!�� y�R�t���'2��_�*���LeS��L�Ȧ��7E@6��'��.��8 A��v�o����}��NRzY�`�����l�U5ݬnǉ�T�S-V�Ъ��8i���b�In4Ấ}��4`��朑�eO&���/�i���]&��A�ߔ�>�7�~S�}�!����߿}��՛��?���myc��޿����7����y��K�kO����z��������g�c�M~�n~Ũ�f���J���i�����ѢeQgKu�C�T�͍%μ[Ϯ�Xu�5i�5Xg`��9�e���կ��9��i3v�`����\�s��36i����C&yԠT[ʱ�'���7��|�9�=︬�T�V�1y-V�N�=|Ӏ��9�4N�hR�3|�v�W�g6"�fD���2ϥ!-�.�z�ճ�h�h�`�#���6ɫ@Fd���8�<��H�Fd�}��r��;�7�y��H�X͋�Չz$�4��ه\�h���aL�e�����"�բ�3��˟�|H;`��H�ց8�C�5`z�r�u��1��*w�ˆ�ƫ��Gl���sޜ�pr@E��i�lf_�/�#X`�-0ynm@�N�v���wu�D��|�D0�LK�k��d9-��[:�g�nPݏf�u���u��]0-�Y5�B��4�`�b��o��:̅J?��Dw{4I���@�ą��;8���\�?�6�LM�N|� -͢�c���(�_Z��{ �l-ʩz�ëu����CeD��5�VW�h���Gd�$3G��_9�]L)i)Ź��,%�wJ�q]�m���l�I��.�h<�,%�V����������\��Ɣ����Pm/3�Ҁ��<J������!�;#��-������49�|���}� [p��s���ݥӀi�I!��-��Y���AC�
�wpMs׳���>�$x��Ӏ#f�#�_�%֢TC������S��_"�� Eލe!H�(��N���<�Sm�C�Q��/����1��mM����mq��ؓYʂ�d[�;4�r���p4�sm:mn��]�8!��,�-x������P-���;��a�����[[�d�O�+B:mn+s[�O>W:8,�6���?�U$���l�|8����-�m�֖MgۚJ��6�e���ڊ瞫^�;�C���[[�\�7�h�is[��J�o�����j:mn*pS�?�x���S&mn�?eßr<7�����
/��m�l�S�>�z.9�C�a�C��U<5���^ǡw������m����=�^�V.���Z����w������z��ɶ�ᐶ�@Wk+s[g��1`��@�Cw�Em˟����i �顋��J�ܓm��M��X��4hU�'���d�l�S+FA�FZ��B^B�p�n[͵� �gGCu,a�i ��5��A}n��Q��L�im����q~�)s�Xu��iM9~,n]���16D���<�O�����R:D���\�܄��A��"4Z[[�un��A~�@T�CwF�b�>ٔ�z��ei�w�vC��/>YTf��v�������^�To��ƪMm�v��Ƨ���L[�R�;mnKs[T�0��5�������-�mqQCwnum�<j�6�e�-*jjM��m�����6�Zk(��͗�k<7�ւ)�A�ܖ�h7TF���������mn+�wh΍���is[�ۊ��|�����uP�1w����87�~/\��Fi�=�%�%����ᒄN��J<n����c'ۚ���6�������M�0oq����F�	:�]��A��a�J�����^n�����ӆzHmi:2>�}uj�SH�������5����I��
ܖm���Kd���Cm��mEn˵w�����w�����m%n������)��+Q����p�g����r�l��\���5���m�>�'*�1��G�2��"Oƞ����j���-�m��\����];�f^�V���^q,�Ŝ��ߝ�f��x�j�E�쮨�[M����m�ǩV���ښ�ߝ6���m�8�������w�x�F��V��6��2���$��Vdu05�f���Z;�>k3vbls�à�ڌ�����hmQ�7����)�j��ܤn�����F�i������<����K�_$i��2s��5�hz��,�z���\����鹇3$� �g�V_Oڌ(� 4ϒ�q���i}�@�V<G�lb�vt�X�<C�,+Ih=��UЙ�mS����^���uK
���.[�~1��uQ+ŭ�:pk�nl��F9�ie��Y�4���Z��,����M���w���f<�cp;�^�Q6-J��L�u� w`�uM��n9ng��#s�eơ�'�Ss��鴙{=�J�xR�����5�3C���/���4K�b/��33i��'9Z9[O�������$Gˠ�-�)�x@    ���I�%=���ç��i��Nr{d��K��"XTD���*�a*&�e>=���w��l���{̋4��&����z���������i���Lݡ�=�p.�ӓ������ܞ����Ɍdp'���h
�f,("/(8o���J��փNP/(�ZW�&��#?�K�W���M]-��GIUn��P]��.i�[3wn�Yl7TQ�4��s˒S�X7����l��[b��1�r�~��_c�-���$+/^l���PB�� w��ö~Ky�Ws�G��Ѿx��Oo:�w�=��՛�������/���݋���j-ӟ}�������G��=B����V�5���b-U�U��G�c�k�?�m�`����ܐg�i�2?��G�z�w�G�c�k�#��`z�!]=����5����
��t"|I���|�ӯ�/p�r�O�%��T�Z�`���Jɇ[HDa0�X���(%�]z�G�Wg<O�-�O�d�;x�<������xbn	�e0���ⶏ`�_!øi�xb6<1�;y?}��b]� �~�s�':�++ӟY�����j���=����3P�d\��?�M�!��k�r{�?n{�2���0��Kqx�,��ghwuۮ���G0���G����7��������ڏF7k�26JO�.~2�8��6S��J����t�[�W�L�΄5@�����?_mge�yL�ap��M�v�$STjetjrf�%�ƽ�
ۏ՟�[���f�5<m����<e��1b�5�Xh��Ea΄L����,,
c�zp]
��љgښC���w�\k+��N�\���Vg�����R��TR���:�Cw�"�zn�G�[��r�mv��[�,5P��>tE#K8[J���:�؇�.��A�^6r�C��@��\�
~-8!�Y�
Jl�QTj���T��S�P����;��N��_B0���C�w����'��'�:Թ|�O[VI��T����7ԉ���FI�?��{��!��Ӧ�|�̞�$\D��Du���mı������b���ע��uV����t���Ÿ���R;mƮ��ٗ67���>:(��k=ٴ/7nr��	n5�m�u���E�{��� ����jXI��c�N܎_7�2�'o���mq]Ⱥ�2��-�\F������ v`l�܂���8���Ԕ2!s���~I��k��+ V�6���i�;3wJ$���m`�6sג�z/�����1>驟���̭��u_+�� �ajJ�����r��˯�ދ��I��?A����i �1v�·}Y�l߰�4)9����X��4��*4�>�Z��rx�y]	9�	~��x�Q�캣����F��;J� vfl*&������;���&J���Շ�h��5c�_�W�_�rO�n��$_+A��������N�uG�����6�G�r��䞢��^Li�/3�b���y7{�T��2�"���4���*L	]\G����Nܑ���4N�ؤV�>ΌO�k5{�矨EI��ܱI�����>ȧ58i3y-�k�ir;ؓ�5�k&E?��4�n����M
��nz��uX����@tIry�ҡ"g�r�䴪-_�x�P��'l�kʒ���cIp"ry>ss�H����[�W_�܍S(i�<29�x��	��y���6�5����Joq�Ks���i�<�w޲!�"���z�I��k�k�ׅ��z����W��D;��)<�U����f/�'���W��\� ��w��#-9��y_A w���a��R�_X*�:�f����B�@N;"� x`�vّ���ݡJ����w��~>%'�Ws߃Z����FM������r�ߟy�8�ˤ�������m�Vo�bD�"*�v���$����:([�{�u��-ͫ"ϡ7�����t����F��u�Ş��L'뤁'0�T#�켝s�Ӌ�����V�/k���E-�ŉ������ wLN_�Z�B�(�||����[��0���s#��_}Q�#�SD��� %�=@��Z�^��6��m�X�z�����"�&�Nf{�����oYغa�Ͷ�$�t55vo� �alڰ
��xKo�l��4���>r���$� v�{� -ط�-u� �gl
��$^͝��Ұ�t����ߝ�#c�;�v պ���/�b�ѓf`בr?R��9�YGt��D��Ú��w���k��ᒺN云�[��"wָ�T��5��	��������&k�k�k����i ��+�Ms�M�����N�%�j4�����ME���q��8�<�;��\:�ň%7��Sg��2�PO��~�o�S�~�i4�Yܑ�[MZI�<���sS��-��p�+�C������t��*n�����i3w-����X�����a� �n��u1�-Y'ȏ�Ϧr��7K^9�}��4�mۈc�a<�4�]��=�:+���J�%A:�{&���	rR܅!%4p�4����Ľj�;27�Ŝ,b��P�at�s������>+l��:�S� �o�i3�UL����$�����9.B�n��+f�U�8�m 7N�����;A~���4@n��"������T��th���weKt�� zN'6�{F� ˛J8�n���a�Y(���Y٢>Ԋ�4 \˃k58?����� 7������V��C楌��f�!�f�f�u	����R�qyH�֍��R�!
w� �aj��%Dh�ћ4@m���S�qYH���j����w�Å�G� �gn���Ky����^B����uZn�vs��m̼�$����[����fLq�4����Ӳ*,7��:�ʏ�ɦ����aYU ��yl���Z�6����*8�上:���4��������B��p� �a�;X�yp�xpY'���W�N�c��Z����{<��<[1c���n��t*����'���.�����bltCM�M����apqCU�M����ap�C��M�e�{(m�F�xZ]#P��t�����ө��X�m5�	(i ^7���4�������kH����;5�7��g8;�m��O�Q�BNF*�����]!C�M�g���*eB*6���t'_�1��;�܅â�����ŲB�rroI��z����E� {j�{�%y�_oX3H����!W�('��]0��FTw���[�H��ͽ��yNŷӄh��P5���$d�����M���K���a��l-Ӧ��QoR��ھRSTƑ�4��;������JC�a���S?��:������sD��l���s�{��,�¨�����b�ŹC)�M���bl��XpC4�o���ݔ|�q�Ǒ�cS����NC�Ц���	��ҕb�gh]m�yp�^�c�+g^!�P�o� �o�[���Z|�"Y���i-.H��O��#Syj7u��&�̕��W)y��k�1x� yfr��	]/��S�N�提U#��]�^#�� �fr�ݚ^#��wN 7L.�[[)�
� y�8o��z-�}R��I�v�}�ԏ����o��7�[ڈ�rO�'k�;0�]�빏em6pG澡�x=���1����7)P}�.�0���gpg�dɰX�<�s� �6�G�z˗�+崉 �k&_?�ld��.v�����3K���%\�.u�4w2s�1�5���f��X��j�xF�����
N/\�EtH�C��M���{X��1?�5���+gn��%N���1�bT�_�,�
�`Y��[+~�+�W��eJ�c�k&7�}�C��Y6�SoY5�n�����v�7Y�e����M�{^������v�Mc�,����Q��}�Ĭ�v�23�Rzc�,3���f�������,V!� 5o�I؉߶p��qZ���3��v-£��c�X/���ܨF޾Jar=ڄ��:e�{#��2�q@!p��,Il3�Iؖ;��B%��%g�]{��NM2'K� ��}� ��߶�&���/�D��5e�v�#s˟G�T���4���썍*�o���-��4���\8�\+
��Y�����]��/�j�<�q� �a����^4�c�`� �m�w�m� o  �(�4��[�۬b�1i��3���Y=�F)������L[�q�� wd�V��&%��Xੁ��ȴH]�F��� �NK� v;f��J6�6�;���t�ܦ����C�	k �̜�+L*H�]=k��̚!�ɘ2�T�n��mn�,�Q&��5�d��p7� �o/|�v�+�<�G<�עBa�e_�0׳�#�n͞����瞈�ZQ(�s���Nc�@� u���քv	b��+����Mc�?��n 5�k�����6���D/{;�� �a��}E��t��K��m#oc��I�������}-!��S�6�Wׂ�W0m ��{��&WI\��wn`�c�5�B�а�]�V1��X!�GF�/Ԙ�I\�� :�L�u��^F+-w�U�tck =3�n!�raVJ?}����X������ҽ�W�����^����E�W�1� w�D����Nv�`HxH�Ճ�^P%�H^n�=i �5�v5]�g��g�Z8���,�=���g+�fP�.�,���H>�*� �L��O�t�k �Ο�N��4h��s�����v���s�,�K������K�m��я�̤n�5�^_�u�� n���u���6�v]���C� �cpi�se�7@899��@!t�,�<�_0�ky ��9s�M����Z(�C�G&��B�h��ۢ�	�5 �����
� 8�+��u�A���\��0����H��w����q|��F��:A~�l�N.Ws�=i3V8b�&�	_�_)���I䶑oߧX��
9A�p��us�3[���z��p���i����tsw�����+��+I>���#���)�ɧ�#Yةa��S�h�dw1=����V�l��Q��}Y+�=����:m�A�YЌpO&��@�p���Q
@2�7I��mo.�t���d���^JEp�2�q�6���XK�.X/Qp9^(��4@��S�������d��F�z���~��`��� O�Y�
k����dq��X+��xM��5�דY�M�
}�������v����i�]$�լ�1��5�m�[��ye�SW��9�bAaO�B֌1|�l�����N�@��}.<�{�>�]�4�X�%�ZAaO��Jt}h��"'�G&��/s���5��w+�*;�OY�����VrPq��i3x���5\�jוr:��8�7�JAa�u��8�m��c9c� �ar��8���:��߄�j��`��sQ���B9/^�ZA�PK�̳PΫÀW��\P�7�.N	n�j����h=���a�b	��F��q�O �N1p�6���������_�.�X)�x�E �����L	V�'�i���J�r���%juz�n^���i�\7�:�����|G 7�Y���
^�A��4D ���W�-8�� @��S���M�=�wSa/� ��Wn�+���㆟4@^�Oׅ#Hbk��/`Gƾ��m�b�'=.VHةa7�SpyX�����\
3c�1!����IVmƮ��~�"��	b���m���؉,e��X*�5�m�uS� �c��8EM��m�n�MIl7o6I�e�t{�J����j>۸�'�{&�m�/Ln���`�{�Ճ�~$7{����^x��a�I��������[���	�`A2W�'#�4@���PMI�����J-����mZɽ��!���>s�����ܥD�T�����_��h��*��U����0A4>�^�=����Si��&匯�"%I���iP�
k���U2k!w'�ލ�Xܑ��=N_f�q'p��-^se�� N���-�+�2NC�f�Z?��I�Aa������Vrwq�U�ȣ���6�-�]����fp��-�]�zA�p�,k��̘�.J�5h�'�n�����F 9��2_�t'�{*�����}�{��Yܩq��wi��5��������8���I-d��ͯ��|� w�/�~�f��K��k]�q'�&�Ks���|����t��ۭ"V2��W�^O�%��l-d��>,Zl��bd��zJ�Gyd��w�j_�q0'�~�nɲ��@��ҷ˔��<:��j� ��y�(��BNK-� xf�V}g�3�}���~�q?�u��tP�P����1��5@^��>Aoq��� �r��4��R�~u�ƭ��7H�Z?��n^	�3���LD /���7C�N�_�q�K ���)�L�J�%1�����2�FNԣ%���p�X��}+'jv��CmI� y�޲���,Np���M����\ަ%J�[P�{�u�̾�;+"[[j!�oE����O��S��ʗ��~�����T�}*����k� �cl-�Gғ���j���^�<&<ֺA���9�?`PG����~��\H��%�̾���S� � ufj�ϱNC6i3u-d�]|�q:5$`�=�r`E'v�ǘ7� �ap��Z0�@�1��5 n���b��
�tA w�}�\.Zo������;��{�����i�M �ƷZ�I���i7��G-d��0�ڀ��KA� xz�.�Z��ǃ�!� ��n���U�S���k� ��Y��\V�iIH ��.nC�k|����a[V�aˇxp���I[V��� ��O�֬lװ�jP�4cz8cf��t�ފ�A���oWK���7Ƹ_� wd�;�(�X��5��ws3�`���O�e��p�Z$��5Hr�D.�q��em&�U��^�$'����2�%J�K�\��.�V�:���/�o�Դ���ӈ�a�L�Ձ�A+F]��)��j��Z���j��F�z�!��:W��J�RJ���z_5 <����b��Nߤ�,5�]M�����r�d�]qWre�V̡$k3z-d�}�.�����-<�͵"��#���o�����6̝�{H�ap��M��4���N��Z��y��S@� �gns�s?	�IxH�as�9Ɍ��3��[o3� zl�tǂ*	դ#�x( �i =z[�:��	�!�t�)�� zn质��Y��%�4S��1�|��+4���F����?�i ]?��((.F&u���D�i �4�����Z�z�pK��BA��[��]����0d��i �7p�޸P�.�4���N�e�O��_������NుS≖�vɐv�?I����Oj�M���a���.|PK��pK���e�O]5w�sZ�4Ө�j �̜���\A�C�F�p������N�a�'C��t,>C���0�#;����	2�<:��.�ΐv���>]�9�C� u�Ra�OF�;	� wlܭ�{{�+�3P܉�[Y�%Ȝ2㰋`p���jw���c�0(���^5�V�KlӶ"�5���{�e��4���M�n���s�t�&�B7���5������v��&L��ef�j��7nʹ�*Ȍ�x8�C-z�}�A�:�
9�I�g�k�{��OR���Ɖ�4 ��U!�B���";p��M_���a%�S718,2�FM�X%Uۀ!�8���u-�+��?�f�j��Mæ�����2����4�m��]� �:!��l�i��5j��YF�j �7l��YB�;`��r?�~��q*c<���4�wnܒ�����N܉��6]`A�i8�4��7͔Ro0��P���jܴ�T��Z��:
����]�1�d�n��i�p��)��,B��0���d� �%����I�U��X�4 �8}�iqbS�
i��b��PV�q?�)�bY��j <�!p�8��I[����Nు������Mo�ٛc"𶖕�aV�㍽��s�&'S�*a0K6mO����-���.pO�n�0�Pa1��ù�r����Z���:.���%��eF�frص��]���M,B� =x�0
�L8aO֬���J"$6m����_�?1s��      �      x�Ž۲�J�'z=}
�/v��cN(�c��:P������G��( �x����r�$�
Pсs���)T�/+3++�*�؏�j�(�j�����j��td��?�/y��'ϻ��� ~���'��~y��!�������X+��7���)
�)�����O�����HC�V��4wE�Z۽ɖ9�c��X6�F����K������ת�Q�o��<N��c3h\� ��C��.l�	{5ޘ��`㛆�`� ����
J1�w`�`�o�el9؎�V�U�G�)b5�卷�Y��L`���3p- ~��_M�e^X9f���yŜAX��/��f��|�tCH�˦;3������7X���ME}�f ��`d��o���% ��XZ{Ӱ�8��[l�e&.f�Rh���~s�B���;NdѠ\��$�l�k�66os�%?�P�?��T��q:�Ԛ12j*����R����oʡ[?�GM�s� �HL#p��H�Ok*��C��A�!��T���>!B$C����)��	]گ=��T1�8>�vS��V�- �|�Xj����u���X ꝸ�Ԉ5��\�.����y'0���*�Ҟ`6ǲD���7��@wq�U��pJi�U�ӓ��Qj?/���O���Q}Me)*��|F@���}�3��X��>P,%��>Y:�����l<w(a�G�IiJ��x\�Ю�b�"��;�A�z���m���ۙo�V��6I��g89U��NBߖ�� +�Ay�� 4�[���-v��>	&c�[��/(8��E��I=!�8wO:q�	��h��?�N�KI'��mv>	g��x�ae�	��l�j��1�>خL���L �:�Y4(��*�����~yO�(KQ�}�	�SR���H��as���]=x}o�hۦ���ukg�ձ=��$� CLA,��3b��S�������DLٔ��:�ֈ���5�)kR�v�Ňj�`;��@�#Wl�K�1��-"�[]�mD������z��8�pz�-4�4%��N�+�̀�2�Ac���L)���f��ЛVXyB���+rI�S�Ѡ\2��%sW.���E>L&%��N/2�	���(6��5�|�:�*G�,���1l_L"C0�X0o��;�����Ҟ���p��}��	���̷����Z7��y�,�M�V`璽�\X�Z$6�<7?���~'?�J(����w%�~b�X���O�N	(�3�5Y^t��"�=�Yヽ�p��j
�݌��m����AP����"�HM�r�,������B��˹����_��o��,y�4�����S�Ns��y(V`p^�";u�нW�_�B��t��1�O��p�aq�L��;m/4tqx���KK�Ϩ�%�ª��[e�)7	�s�5G1[��V��tU�h�I�ǘw4g�hP���طS�훊zB�Y����Wq�)=���$����I�[�*�F-�>�=�v���Y�=`�!�2hP�=����Ԟ��k��Ւ�
?N����ߓ�l��KlBJX}�4�ew������K�8��f��Q��lkN���+��\Eg.�y����ُ�"���s@69#ӟ(�����'���m�G]����w|�=����������f��~-PI?G^�s��iM~Y�qk��"�_8G��	V�楪x�EU�Y�ٛ�������1ݷ%d���Ʊ����j��e��_)B��ʃ��Ѩ�v&m��B��ܤ��.��S�Q[ÇBv���6����x���R��/����wx�o$�H*�����+��H��7�Dփ"�1*���.K��
d��i���1�\���,��x��e��/�~�`�7����F��@��x��o$�ۊ|,��A��nS?)�r�m��Tfp을(�Eˑ�і��Zy���}Q���������R`{a)_�iϕ��f+)�m�N�^����.�_���y�N~:�������,�co�>�c0����A����HR�7Io��!�g-�����l#�*����ļ��[# �21|+/�|���������yYe��GV`��6��ó��;P�" �#�	�Eˑ���(�Яq��2��'.�_�^���֒ĉ[�s�5���W��2����O�Y��G�D�#Y�;�t�;�&x[M��؜��!�e�3���h�M�3��Cs4�Tz�@���`=�����`S2�[���UW#��ZL:}�Ę������*�.����n�ISq���h9�N�*qX�e׃�Xh?��+��OsT��<.�m���3*�}YґJe���,?*��}�G�=#J���,Z�dҢ�qB�F�T�@�@8;=��SF��5Z!,���X6t�`�o�|��0�ĸ/�_��)N2�d"N�����e����%�ASw7��osT�7q�n�Ϡ'�Kd���c�}v�,W��NW�k�X˓a&&8/�П��P3��g��$C�
��&�G>Q�d�a�V��h�(N�Z4�2��z�"��M�E����D�C����� Y��q�����~���@�3~����*;S�O�=n:2d��Fj~�}wNo�b_�K�[�� 6����΢� �b rsb���h��6
'�(����D0�P	Q�k�&f���__,�n؋�  	�*���:���/A0����¶������v�(���[*���lX���6�O��7�F�wd� ����|�rMa|�v������G�Y�,�b�<Qc�H��]Kmwg��V��춖�*Zm��z���5�������?�;�e�r$�6ħ���8��W��iH�j��;:�q� ٲ����/�f�#�Ų�W�6��;����V��h���������_��~Q��n��/k�Af��8�M���P��������l-Gai�M�5y&Z�,�X���'_�`�@��x���g�m�E�\�+����<����{S�� x�8��x��U�W���a؛��;f�_m�7�-��Z�yޟ�b�X�?��[į��S��e��-G���+����L��%�F�����PQo�sQ���J�����:�Ie_X�x���-!�6z�NU�dUu<������_�߯_��g��"����AD�QDZ+��k݌N����
�PX4�%Q����W�ꩲ�8�#΃_8���7����Z~Wa`������⿬F�5O��qE:��(2S����I���p�+�Iʽ��2�����p�R��X�K�♲~[��Ǖ��>�^���~S$>W+��n��Z���jU8�Eq�I��Q6�&|M��D�ZD%"?4*�-��[��ma��-����S�P��Z�ʮ�%�������ػ���O�R�pHz�!O�ڡ�E�QWղ�ǋ)���.�Z����n{���P
X�+��LQ���Ȗ<`��}��{���Q����6��;�e�r��Z�d,B�G�&tF!��dA)�9�����}$�6�EDN��y�������:���/�;���J"�J����p��������=#� �����(�s�}|�䫣N�y;`��.}* �R�CU����eO�?uIorE���K<��y�r�byϫ((�^t��m��iz�3Mo#t��i�{�Z�o��~�m�9��h<����~ݬ��޴���o��>��83�on����������M���n+�=������d���&G�>HV��~Y�bE" ��=���⾹����. ���r4���bm��b��X���38���m,ç{$ke�5��bL�u�8�������'��W�-������lP�ˢ�h2ފ�B)����cܾ�v/��ɞ�h�%R��]����|s���f���1����D�-���X������XQ�K�����RE���Ѥ^-�[���F�u�2-�ִ:m���㴺�[�L<����I
{��,Z��"�U7��%T�K��
/�����,�+��Q�BU�
,��7�حv�J]�_�4q�K/��t��V~���e`��j;�����?����˝9���/?p (@�? a� f6MDm�ZuӨ��k�_4��ذ�V��ڠ    ��|W�i$�;��q��d�����';�������X6c���
,t�;�n\���9�,���c�24I�$gh#pV��Ŭ>Y40�\0j�*uʇ��
<�V��O�*��X�mf2f���X0�� �h9��p8���1���Z��󊹵¼b��ؙ�ϱ+&�n���.Q�tL߂��IQ���4g'� �{_�T�V���|#X����TQ>V���^��D�u���rs8��yS�*|1$�ͱ���*�$�pi��]bީ�
�}�`�E����̗�Bt��n�P��.�R���̟bF"ů`��%:-mͿ���|X���+��Л�� Ξu�f���Y����Ts�"�v�X��-� .�XU�4��N@FRyr�7���[�$V/J�v��wBߵ�|�ǧg�C/��%��z�o��KgA�0�&��շ+��?�J�f�.��'�JYR����+��xj�4�u�O�����h&+,@��y�o�:�On)��Wx�*Ń ������"4o���+U�Z"����R���(P|%/�"�ogni|��*������O{p�>�1 �x�Ӡw�I2����F���A@�um�,st(z����5FZo$)�z������C�m*Q�Ru�[�+�U�m���;���?�����;��9�{x*:�4��V�T9me5f�|/�Qw����i� O�ad����7��L	�8�a��EU�>@s:�*B�Z�LW���~VZ�+��d��m흨.y�ܺgF��̢�,��U`�F���NzƎ�ji�Ib�{?���� 	 `F��f��"����L:]���� �w��`��v=���q��R���z�'�b�B��$3�������)-L���d�q>Mq�j�Ynx��H�E��ᥪƫ�NS���P��b��:Ѐ �P���ӄ��b�`p,�Q �}p	��ܢ�����������p����ӭ�'�Ռ���kO��Μ4���,Y��K�����-����	L?:ǌ�<�V�+�������8���sB�EM`o=��ٳ�3��)2�o�խ��g˾ a=R**W۷�N٤�bWm�/(;�T��OE���+D@���SjUΚު��&��p�)��*!���+ճ��ܓ�z��q$'ϟ�>u����Y{;��YηEm_<�6��捾'Q]/�aK]���Ξ�9<:��e�rLy�X��7$�y9�?�e^-��FG��<R�|�M��9KO>��l&q��H&��-Hs��G����w������_�F#�;^��Laau��ʹ�8u��-�Dq����(>U���4GGxO3�����$��g>�wM|kk���V3��9^91��teOL?�����t*3�ц��l4%��q��2,�����D���.���^n	�������
u^8�]six�9��B��[J�>������X��+O���m�8U�q�S��+L�i����1�a��xHCZQ��)%kC�*6���hr�v����3�ʳ�y&�3���c�w�{���;���$�tӄ�t���fh}NMw�i�4�m��I	4G�B����p��.���3��i$E{.�,Z�ɚ!��pV��j��@�D,+�|��։i)7&���/�9��f�"��߹�6��0:�����=8�x��` �U�-�S�cd+��W|8��6q�=��X�.s�s����)����o���ٓ�ﲘ�N�%�`����^�?��"�\�0�����_O4��!e�Ұp����X0�3e*��J�&F�e�W�Aè�K���T-������(-U:
l&t���ģ��Y.�~���~�_����G3�gە��L/�!,����v�ZX��������~���]qB�,��ݺ`6OwH���͢��x�Z>�D��e��B���K���
��7R�H�&љ�.�~;�-�_���)8\$�8�ga�	��Qf�(��aí���nb�{�L'��;¤3b����_��Q�Yf��@��5k��%j�K�½cx-���L����,��;|]%1$~�#��F�YU��Xa�S��i��μ�� �n.ʼ%o�n�!j�Bo��sE� gX�Nb�hy�'AQ�x�8]�'�?��m�]j�.gC �z�m�{�fS�P��-qo0��?�,7W�/��YS���yr���݄~���8��ZJ�*�KU5k�2|-�u�,#��JuXm^w|v���l���/V�A�=�mu��h�$H�����&_�6?Y�6�0��7I}B6�!ع��ww�	9 �����b��a�jC�_�M�nm`D˱T���I���fB�����K��'�rr"/.R���^��2,-���n�sڥ�	�Wmդ d����܆�N�Kg-�VrG�נ&G��,_��g�79�dX<	���v~a�V������4oi}ҟ����T�	�{��Z��.n֐Y�����Ӽ�xK�cL-�F9ാ�N}
Z�܄~���E��9):�Vq�]Y�eő�n-��:��w$:gY
����b�9@�2�J
P F-3�m-[(nĊ��:�]ͅ��v��v���Ḣ�ج��$@��q���r-��Ϋy��?��~lTʮ�T�=~�Î�����q���;8�V�����s�.�E�����L�%�:5w�`y&��H�������7�.q}lY��%�H,�W��rT�eM*T�i-�&�<��s^�7��D�q�z<���W1p莢4^��"-�]������w�����l+���b}�=HuwCڭ�X�܉���r\V��̞Ɵ�i�c0c�2S�?�jBTz��6Ľ9k��$g�|����G��w'�6ڊ!� ]��~�Q �ee`;�i���x�����^jt�f�z��#����l{��V���g�6�c�=W�n�k��r��$�������������1��F���;��7o S:��,�;���kl��m��V�d[4�>U���ZT��
`*0�rNȈ4L������%@��g���.]��OU�{�e8���,	��͹jw�+�=��z��Y��0�
ڟ�|�4�r��Rj7R	��*������d�a��-�J�Ͱ�(4��n���w��K��\Ny\�r\֮;I�os8F�f��w�s4�7�D/�x�������~�OgƼ5iȀ��]ӭ
���R��6�J��hj��o�/���˞�+Z���|�~���� �X���Y[�.��r���΂V�����V��?u�^��:[�(��ҩ-���[�R(���ñ�����W�Y�#�-����
�f� ���iP�SjnFm���U���@�;��h9�ܓ=�GJ�[�wnn��3�P,�dH���^���B=lb���+�,�YVU�.뙭���b45�F��mo_AH�ģN4�0˧���>"|�� ')p��3��
�J�+wץ��'�ѵ�ł������Z�P���)2�Bp;�D4�03�n�k�@x_]I���uAx���6}M��á�+5e�SK�ʄ-�Ñ\�DY�pLJ����(E4/3Wm�Sʀw_%	#����h>��E�Ͽ��m�#�3�(���+�8Zh�B{���F�pү�ms?8�CVŃ���E^~7sY7��A�Y.�T���+�_"�
�\�ݿ�����J�+��Bt���BT�^�O���V1�ڴ6s�]ı�f}�%PҦ(6��KO��2l-��,?��I�ʾ�3�A�#�ED��S]E�t/0V*��7(un���hV�߰&fHi�Y��(�*=�Uh~vkE���ƦAe_}A�}��Mab���m�.&<Gh���Ɂ;Vmi�'J-�>��E�'�,Ĕ�%�Ҙ�$T6W��c�k�W����`*=����d�8O(�J�Iz�����+��[�.�AL��g��Q� ]�G7�5[�/}٤�_v�}5ԋ�ѽ*Z��z��"������O�/4ݼ~��8-v��EE�3���4�w�`U)b�>,������}�)�����*-U`�� <?�\e.'W�h9�\*݁�o��T:3���Q�m$o�9�5�AX�5�iD��t��R���B�    �'F����\�?lv��lce�<]L��\�z�i��޴f�eq�#P�L*��Вk��*A��d����`��B�>FoE��ǻY�� _(�a�=�Bȸ�.O��z!x�����Ouw���M���F�8���$d�� � dp�CG=ExQ�U1O��x��Ƽ�&�O?��� MP\��NՔN��h�I�ME+S'���uB���>4a�|3�%
2@���X��Œ���v����κ�o��aY��mT��̞(�@1B�j)�B�,���R��O)M�+g�MG;:�`����!Á�� ��[!#�!t��񨥵WL�gIYscO�T%�M�o��{J�p�ǉ(��E���x�)x��y�5ϊn�xྕa�hDF�o�P�'���	�{�U
i& ��rX�*lo��mW��=�7��p�*�#�){�`��/ȢA��^a����آ`�yBS��qJ���uf�f�l���鸛��7T O��t��/��a.�T�h`���Bs���I��E��1���k8�{��Y˚�k���:���N���kA������T���Ϝ��VnR40�YL��?��
p�ɰɵ$�`x=����Pkce�1�TJ|����ju-7g5:t��J��9��
B6}�K�f��i8�#�S6���W]���ାj���m��@Ԛmz�J�k/��e�M�>[�l�y ��}�QD� 3ce�i�˘�����A�<�f� �se���C#�}�ګ�:Ҁ��x�N��`e[4�&�]q��l�+��6ܴ�)�P����P30iVb���e�-��熸���N�Y�2�t_a\b?�����ȥx�j�-)�w��&^"K)�Lǘ�ܨ��JL�8h��|��M{hÉ�&l��3�_a;��l�=3@�%��_g;���$F�"�{�K|Q�#���Y�4�q}�b�X8�3�"Q����mWz�u]*5��]')�<��%�?��-�VG�����L�\2�ð��*�/b\b�9�����ژ�7�ƨh!�t�,Ī�땥ᐥ����C�kR�τ�]���bIn����o�}�\���*E������f�!bUl���"�w����Ș��Q��I���	���uY�8�𞃵�t�)|.��sXoj�=��<f_�0uk�#�p�ڶ,D�s�tIF���t<��������E�Π'k��+��'�/��x�U�W��Ew'o��J�xC��k�~s�V��+��r�*��^�
K�2T��Ғ@�މ[�/�Avd�����Q�]N��˞g����L���.𳅥F�pNLb�v]]��
�G������Z��d?�J���|n�@�
{�nǜ��5�to[!\�nXD�,�(B�6Ĺ4�cޗ���ɱ�G���)5���N,�6�ח�M�X��C܌�q�VkK�]�[�Ǉ�/���M���v[���~�Q��ir#��'@����JQ����;��>ʚ��`lL,w�W��$J�tC�/�4E�q@��г)t�Z�`F�JWk̥٨�Ͻ�NU+�p�����vj��WY%"��.$4�.ks��Νt�1��}�ő ā[�3o]�H�e�Ru��G�]�����*Ykw캻�
}�,5ͩs���R\�͚�\`�*4��ƴ+�Xҝ���h�'���'�?]m�oNTZ��=B6m�[�$�3`o@�K���H�mH�r8�5y�`I���x�?� �ǽI��O�/\��^���J[�#J������5��Z&JV��WE�W�^m�J� �,�=�.�70!�����$�3z4��-��dg�H��(�(~22���؛m<�[q5�7�+Шw@e� �,�8D�� �*�{�f)4
���O�h
�m��ڥ�6��>�iS`i�����W0�R��"�9�A��E�hX�S��N�!��j��N�QL�%�R
�2\:��1��ZB��6� �G-Wm��.��氫Nzcqg��2_1(���V���Y4�6�A���;�r�Q����Ġr?�GS�H�3��pz\{M��h�����y,h����ݵ,���^�u-��e9{��	��!�>4����xT�~�GS�(�q�YC�f�ӊm�Mz�.�+ʇm)���\�P�R�5h7����e�eic�.��ր"ڃ�4���8��'z:���K��WG��9C*[�`@/�t��� (�������!w�x&4��S��X���A"������dlfp��)��>؈�ѡ��;I[͆�-8��n�ϻ��`�WZ}&��k0o���ػ��� )�q�v���4V�8�6[_���ME�p���M��,�F�@�IG�_�e�ˢAh�E������j탈��?�gS�8g����ؒ�唫���eS��vk줙<U�vG��`��)Z'?ub<������w �щ�.L��)��OO�#[���������Vw��̺s��z�l<�D�L8�E�!uy8�06���ߺ1g�!�b���q�������뫽�V�a�n�e����{���B�b6v���	&Gf8�����h"���Gc�p��G!�Q��� ��?��)�@�Qh;��!�=��M��O��F3�Ng֩��J�
B�6��� �ǮM<���7���;,��V�݌VW����=�����XKv6]p,E���	wRh/�B	�6�L�p꒿��>�kbD�iÉ��6���)&�&p970�\R�f�r�;�ԕ�����U�5�H�ie;؍C�P���v(���n\D�p�:I��tg���h�_��[�!��|
-��jaK:������b6�`��JI2�U��R���e��W�S4�����`�����~x
+a�R�e�֕i�\����Z��J�Xb'ZWTy���(�[
�n�G�2]���rLGK�#�S�YP�����M����������U�#��� �kT�m��q{8�*t])o\���W�2�]}�	��fw��1��tc���"
d��T�du�Z�4k�E�=�'���Cז���L���wj�L,!��N;�"���N;�ݏ�L�� ���N�t�?	K��]N������o���/�M���e8���N0$`�_!J��S�xEI��6">8e��d�*@.��x� �%P��۳��0\TD��o��SV�fF�{Qs��D�|{�������W4���V�ʘ9��t$�}��H\o�=`Lڕc2�P���B��V[�4b�c����W&����(p��@}�
�s��=�p�,K�Ɇq;E�WR��n�~W\r���b{�dP�)
L;�jqY��z���3��'R4����,���I�[s 9
P���)Ǐ"t��K�%Ohv%lT���.-��.K�X[��@�x�i&J�Ng� ����O����|����K��IC�Xx�WR�I}fȕM�!ٓ9ӑ�y��A8>��*m�&:րi_�a�� �O��:/������O�#9��g�r��FjNFQ���W���AQd���V�+�öؚJ�ߝ����+���G4��ǈR������6��$F�ܞ��HZ��x��z��6F�̯���*����cz�X���U��࣏� �OcihNo�^~���1:�S��I�����Z[*��F�l��:큏��E{��Cc\\�պ�Wb^�*�L�!?v�
r�Vh!�#K���~�QZfY��ڒ(4��n�O����Mi�)v��`�]��͜�� �q،h[��rf�A���܋�ҢQ�K-\��|ݴ���q�L�[9##_uW[���Z�p<�t\3�<=ZO6g���w�D�T+ؘ�����jU0����e�}��������q�(2Y���u9����z��8�j��#e[ڙ��Pv]�+;�Ê�FP�,��*|��ĿI�8O�r8���	�p�*ō��ha"3����BҀL��� t>�v.��%�e�^�1�Y�,��H��;��#'%B��ʰ���������T:�r)�3�zh�����>XeO�w�aԹ��p�P��}�����O�aK�b��[���8ܩ�L�'}h"Q���r*3�Jr�U���;	��oE��6F    ���ѣ�'�L.Y
ci�SQ����#��E�ʩL��>$��M#Ј�J[Rsc�Q�8�Aud���"�s�d�%�,Z����:_�F�ښ�8��r:��wS�(o>��Vo55���k|�%�_��^��X��:J!�OW�����?S�}+@P{:=�l�o�P��P=gnH���	oyI\jA�1�#�P���/�(V}0����@\�n�j��B�]���neL8�=9el1�OG9�n�+9���:�ԫ�Ph�6��i��V�6�.]�F`4̉�n/�X/x��KV�+��m~bs�Q��B�����tɌ�p���	ɐ{>���w��"(�!����[�'���.��m�1�:�`ƶ7Z�2v�Ѫ&s�U�uڗ�Xs+=wj0f�]���� �l��xb,��r�+_lu������Х0a>�����~��^xpf4+�K%^n�8�!��JÞqls�JpΗ����+��M��mv�=E�y�1��0�5�QCkZ��+��I���Z��X�y�~�g9��c���b�k�6^l&�t8�_�˲ܵVU_�ii��q�|�I�kRdR֤'�ieU������A�E�Ě�+�Rr����BL"Vq�w~KG��ݻ����8ED�X��˵<-XJYި�U�K��[J`�7H0�[�k-Ư����z�A�f͵΃u�*U��l���T[JtQ�խ87�L�ޤϮJ�$�o*[x����������T��Z������=����b�q���h_0ȿ������7�+'��K�5�7�Pv�|Qo[~GnvG�(P�~���7sz��O���^i.��yIԽNx��޸S���K}��#8�.r��7��0�"<�4�K ��
������X)e�Y�b[����UŞs=w����Pl���x��'��i��I�-��	4��4to�U���:�,��	Lᓾ���(���W���n�N�*M�����jx��=Ep=��(N��Ԓ�\�J�:saw�V��ͫ�Bs���Y�^0+�g��/$~��w��p&k>�[(���dU��o�[h��&�.o�\UÝ���C�^���o��v�V�b:�e0*Q5m�tJ~y/;]%���dJ��4ďr{��6�'�1����dz�d1�:_S��b�r�/$�7�
q��G��ٱ���>b|�u}5u�����t�'o�r�D�v'�.FZp<���	"��(Qy�\�H�4y$�"^��Or��G����P~�Ms��;�g 1�M��1��oP)��) �ѨV�;cդ̶���4�-2tkB���7�Q"�ړ�(���.4(Y��ˬ��)ϝA�w
z�Ο��ݐ�thCۨ��]�AW���\�XL�K�Z⽁lz�̖v�f�w�Ӱɴ�D}9��4+K��
�髳�dq�T�[8���ۓ�p����q�����qԚ�)��--�r�:[Ol�|y�L���]�����{2�A�\�j��B��LZ�(�k@��ժ���&7��g�*7j����n|�m�*dݢDA��N��/(���;68��8p5$_5*�9ZK&�upW��.�m�UP݄�e����_�4�)�+� ���#�\fbYsk�;4�mL߳�[4{D1��d�q����a0�  �wI�b��[oF�{�TY3����R#��w�%e.y!<hC�1��HݚΈ��5mM��F�r���A�x���8
�g,�G�?����Ρ"��������^�����c[s���EI�o���-�GB��P����L5dC�TS�(|*E)�|C��F	!Y,�u��f��%�{A�ts�2�f�XV����V��t�)Cc���A�?{2�Q����%r��p6kN��MJ)ں��ܦ�9��O��	 � i*b
T�XLW���O�f�l�EoM�8��G�ݑ�u �[�f��3�+�o�	-���)�w-�,O���:����.<Tچ�N�/U��p�7^p��0�;(�ޢ�� �S碴~)��u��t���ޯ̏�W=A,wEeK�lnB����K�^e?��r8�5�VK�ΔW��m"�sn�Ϟ�?wg8uI6�|V��J�,���<�v!�-6J4��6,�������N���� ��/0�0,xlXج�Irk��7z� /+��X��%ͅ��.5B+A8��CC�z��$h@$[��c~F��B·7����8�K*�!�����v~�,X��H��nm����M8�X����vv�k�=n^����^a*q�������8u���e[�2E"�Q=�~~pБ�Ku�]˝��˻n�/K�_CN���`��nQn���~�ڻ�e�4�	b*�N5E�\ȼ!v[�Ѧ��u�3|kgZ[��a�X'��_��P��U9�ų�S���̣��)%~ˣ]�}�ϔ�Ǻrs����#�2�'Lg^s��Q��Sr<�O��|��e�S�vv�� �3�a��s�9��	'�'Q���}'\Z���"u�����%���H g0���|���_Z/��a3KKe�� 8��X�z�]��> ���t���/ڒ�A��h9�颟�����ɍCפ?t�.�a��IԤ�()g��e��4`n�T��^<<�ukM�]t�$W��kӬ~e�muPb��#�<d�\.���A�ey��f�1��|���X��Ӏ+�X�-�f��݂g���I�1z�)��ձ�Z�жX��.T�1C`�ʓr�^��� ��;�L7�����?J{�q����]�@��fO��b�]1N_n����[��s�mk5^�T�l��YCqD�������u%�f@7�b�h9��L_W�Jᒒ����o�>�-������0�\XZ��1���:%��Wjs��ւ�������8�?7�6�%&Y4�,3Cܹi�M�.���C��)�����:Q��ހ����Уdv_�:��4֬���<�]�]�=��:E��3�[	���Op���v�r]�O!:�5��\3���y���ن0fĠ֚�'5��C���	
�Δ��Ad�I�`�T���=�_3|�T��8��|9�B_�[���b��7�j�����b���cq�V@�����3\�ˢAdfN5`Xp��5��ɭtw�N�����Z��v���j=�jzj��C[�p�٬�D��c	^"�)E�<��6�nh�zg���2E�$��w�����v/U]��q��[N;��P��PmOj�JGJ �璕��~Tr"V���n����G7�\����BZ�3�[�u��V��C�~�5_S�I�v0w�X{�_+� $�1�"�r�>M��|�h�'��;H���o���!�m��Ӕ�I�P�
ZѠcG�f�]E(*D�խʱ2�ͣ��'3���I7R4��,O�?��o�#!%{��{�)� ��c�-J݆5dK{O&�u�c�cv���eZKX�����h�-�N�_��V���$���gn(`I���ps:�ce����9�Xᨍ�����~���~eI�C��>��"��)*���w��.���d^���4��Ze�����:���f����VҚ��8����'�S�?XQ�0K�{�����٪�Ĳ������]~��$�������V/�i�����#�nG���1��=�b���S��q�������8��}9S���qa�[�N��}o�K��`-�]����K����[���-���R��S�0�Ł,���&q���h�F��Y���I����G��ob�T����<lX�7Jt��l�a�_96U ��l �f����a�i)�ޣ=�Y4ȰL?*i���'ɹ1tҶ�N:�
�|s�b�M'If��Ϋh5d���`��a[h��b�Zn�[u��y�����t�ux�V�W��n��Tv�0J����+jc��C�(֞��=)�����Cr �����k�&A�jQ\����U/}�ŕ�g͛k�Ŷj����Z�HA�S��c+�|�n "}�4E�̍ͺ����-%��/��ЉR���(�pm��AJK�������>�{����~��j���h6� �Ccn��х���0�S��7u5�	��1ڍi����mmt z   ��3W;r(��$�}%i������J���P��4W��}��_�{�/��ecq�/0D�%Yb��bɐ��x��\[Z�Z�]�̼�#�7���"�3֞�iS�~&n�ǯ25^�r K{�Y��?��`��Rd��t���]�w�=Xf!I��b�4��K��r+�n��V��C;^�\�v�����m�\ŹM�gg�%;�ؖ+�#�T�ϙi{K%��o���[��<߼A��L-��8d�,��
�;	+���yŜA�P�
~��J`�0�����x�f��6�����������xcnޠ%5�t �������C �k��m�����6��+�U�i|Q��g�Ke�&.�?G���`YT��dĹ����g.�����[�      �   5  x��Y�n�6���z�e)���K`k�nA��톮5G�%���o�CZ�l���(�Q'�����ɐ��ݙ�����Z������k���o�?���+B݄�M(�3�f��a$��9��j*㾗S��j����쾨L�4�bm�W��L�L�>�$���������0R�KD=Yݖ�G��P��|#�fZ2�;�ۺ)vG�]���ڼF,-�
3�0��V,b�f������'#s�e����(aZ�4[�)��(e�bBw��)o�;�;o�|��'A�#�>�x��xF�&Vh�f\eJ�0��)��.d��e[!�����P���K�|4SO�2CF\�$dQ�z[ mR��'���2���9>ڇ�X�YzU�k�<�D��ć�,,J;�wt �-�6;|1�)�4�#�X1�U�;�t�l�}"�{�ដ]1e$�'�a�:LP���5�9����vi6����nl
=�BͲ�̾�j�N���t�È�,Qsֲ]&�#8-I�Z<C)�È�,�3�l�<��0!K�{X^:b$P�,�0��FB�T��)XFQ��\�l�cA9���=,h�FB1�?q����|o�bj�$73i.2�0B�(��]'�U�#���r�O�]�2�L14������"�a�	>��E��2[�F8 �·���,mN0�Qa���uo�{�a.#�譅w��T�x0�v���ؽ�(�պuܿ�v7ũ�pX���
CP����K�3�(_�C���W�t�1��#H��f�q0���_��_ʶ<��_���rǘ��>�p���b�g���*�ܵC46�s�me���ć�=R������/v�N�a��,FPα�c=�!+8j�P
FP�!\ӸD��`\`+��r���\^�mW�K�#�� �?ڭ�|]�q��%09Nz�	F��'��}�[�@eph���)�F��q#��e����@3x0�Z���9H�e�:��d����.\�M�IL8FNd�P���C����j[w��~�S���0�>.%CK��|B"\I��wA%�NIG�������e
;� �Kܛ�1iQ���A��ht|�Dr3��K�/��a�ǭ�a�\�<�g����4�8�-FP�eʎwڧ��K��ē�#h�*|煼7�FA�A:W�[>�Ԫ����mm����!�0�'�͙w��Q�����JWH�t��ʾ�l����Ц55z��q��0���:y��=��*��y�hԝF�$�mo=oU��cG���W��A��TO0�\nߤ�^rsh�wP���k�k��b�wa�boEzP�U�98���:��F�E-eR_����E�����=����eT�>�l�Q����P	E�^<������u�/a���.�f�BM���v��>�4`��7����R|�~�H#����5<y�a��\ΥG��>��]��L���Y�#�\ϥ����F��B;�>��G���4�I�ֈ�b�Qm3�4FU+fT�*f�F���ͣ���a�j�ܪ�R>�b�ḒZ�3yZ�Q����U+��^8z�ḼZ�25��ŨZ1�j������%��hQ��      �      x���[o�8����_!�_�<�@��Ro���vl�U�9�(q����l�%A��Y�R"�LQ�t������&�HnR��m"�L�TJ\�<����������S).�n>6�V����Jcif*�%�O��U �s�Y�3����bbv6Q뙔>-ҩJQ��X�<WO�
�L2�֧��6$ے��j-��PZqQ~+��|-w�����������E�d3��d�ӀM6O�m�wUn'R2G�������Z�u���S��
�h!��TF|(��<��Q�c��@� �ݢ�L���i "v����L��ipU���O��Rސ���0-#Z�3J֫�V�p�)F�Z�F0�7k>�SD�&Y�A�3�n��FH�B4�>�7��4���3iHg�c�F����wm�OCI�T�o�ڧ���-0L�(.�o;�o�4�TC:�i&a2q_ֻ*^�����cK��E�!,�i�"������]\�l�d<$�!W�C��%��R��>-2Db�p���%"��n�˛ ¢�O�0ɀ���ʿ��Z�v����/���~l��a�Q��V��V�s�jZ�R"י�§E���".�>�xt�_̗~�!Z=K�O�0������P����D"�Խ�n�H�a�*̂��Y,igI��"d`�
��ϩ�#����B���$ �p#v|�����u�p|�Ɠ�c�* Q9��,����5�y �P�����P�*�C|W~�|I8m	ͫ�a���J7Mx�~^?����� ��.|Z�8VL��iɈ��uWn�x�~zy.����s�a�"��w���� ��k"�Q������Vb��]|[�V_���yc�^O�0Ԁ�
, �.�Lܯ/k$׻�4��406-��l���jH	�K��ݎ
�l� 3%&���!��]Eà\Xŋƚ�<�8-BǡM�D�Q�}|��T��ʿ�Q>BK\q�}X?��p�Ӂ�Qr6��O��[X�<���Xp�6<����OΒ�tBn�9�eeA<�ޠŇ��ѹ�ǧ�޵I�	C�>-��NL#�՗c���1<81)ax�����9Y�~�I6Z���lX��w���P��gTT�����C���"@P��/�i��5�!�9kړ�$O��=�����2LЎ�7�<��
�|�B���@�E�N" +���Hȉ��� ��0^��]���1�����F�D�d�mF��ȷU�(�D/H�`\�m X"��" �95�*lo��jQ��QÄ���C`N#�����������p�ZL�ڧE��倠|�0!���7ҢvM��ͧ�ӢL�����+����� �5c�C���}Z�Y� ��#H	��#���@Rܤ>��_/����v/O��},'���޶O�EY��]|�F���d�Ӈ�����Hcig�̔�iQ�8F&ޯ*x���m�-^����Ǎn��{Z�+B �n7Ӧq�E���|q?���4
�Qɥ�^R�jQn�v=���9�xyy�Bާ��+5mL�:�iQ�"�e:�@j�Ya3�AZ��o�1N����M��ew� ����<ė%��}Z?/���@O�
��� !��p�ň�����6����4�lG
���8��
��#�c��)픢�6Xo���*�SL
Xʿ��2-.�O�
�9���Uy8Ɣ�0փ��;L�E,�j��ˠ��97(�U��i�L�9+�N:\�t�}���^�Md��Ź!,�}p�^Msk��>�8��Y�+��z_�������R*r�*`�%��4p2�+6��%ҧ����굤wq)���ˌ�����"�508X����r���\��=�;_V��I�I�jS�����O�f괶������9����j`j�d�Y����X���$�i�����I�PO'%��93����t\d�]Yu!Ƨ�� �+o ����x�x4��LA�m�r$,
h�4�p{�6�i@H�H�e��q��%Ry�x,2y�p�!���@F ?cĢN:�O-�����X������DX*��ǃf5��Et��P�	�r��q<�څzO�p�3�& ��i'#s��̺	��aˉ�z�Fh�8�n0�8��CG]|�!�u ��'��ݤ�i@d��% ��szp��b;�^z�p�CX��F�7���C~=)(�nS'�HO�JP��b�S�9-(�f1L���0�Rj#k�=+(����կ�/<�R��B�c�Qͧ��h`d��g�H��zd��<uz��Qʛ�����Y=���=-�tL ���i��0*�dB/ͭOD9H*�r��~�"a��#i �16��k�����풤>����|q1P,�򁴷Vk50Қ��b>��#��`^9
�k��F�t�e"�EO޲��IK��bf>-�)̋���QH�40�cd���OE�u-'��=���� �<Fg���� �fF��^�h`�]Co~�|�^��i`�]���r@�ɕO"�#.��5g��� :�����`����HfI�`�)đ5�W��.%IF��[�:]#xe
pL�a�) �o������4 ��+S�S�G�) ��2�U&{j_�Q�n��8yRs�e��0�/�����B�OB9��ka��qR};j4�̒Ӿ�4 �;4�g���/)�=n�B���M	]�������@�j�u�� $�!Ȁ��j?�S��b��ޗ���C�}'V?d�}�I��h�,��� [�4�����+Ύ��6��K�� ���d�,2)����v��;@�4���r{@p4����L������� �s�)T�v�F��|g��h�E*��5��t��4"�gk����n)v4��l�v6���> 8۰�Mc�����m��v��}@p�a;�.�)i �ن�l:,s�E��m�Φ3�>r@p�a;�7E# ���v6]�?F����3�!3gH�In���=�t�!�,��yw���E�&��;�8�}-rw�-��Ew���E�
�e;��9�> 8۲���ތGN�ܷ,��J��i��K���l����"w�۲��z��;Z��$[���z�#�g[���:��ݙg��f~G�ܧ*c�9n^�����zS�W��gŪ�֛�f��<����W7��Md��Hڑ�^^$��i������ׄ8��!\Z�8m���dLu��:Z侮��3�5�U�a���r�޿/���ϛ�pOC�V(|��R�D�K�Z]SX�0���������ˋ��xy��͟'�׆W���i��5fr����]��K��)}7�9���p�1@![^}餷��j���~�9�ׅp��`�+���S��B���n@������ҋ@���(�1����?����f�i�x�����V&u,�W�����~�(%�)�B8y�����e����9���_O���V���ܜ3k8��S�y[}_��ա�ǋ��890U����z��~I��WQy/�l5PLMaUT ck�Y�G �:\����)��d,�1s]�v~[��ƜU� ��q��ЛČ���[��0��0�	Q���p�����s��~؍��3f�dtoӬՀ1�%C[SX�d�gg̬���>���p�+@�����z����@�c��̌ͤ���)z˘�ҡE֔s:i��(:#ї�T��a��(�!K[^M�}�V��V�0��0�du8g5\ �;�e�ɻ+�Sa�pf�H��NG4�E��A��na�����������Q�����@
���;�G���mh���P=�`��/���9h��6(�"�#W����9�t�}�#�i�d�U�0*oP�o�-�� 2z�,��möZ��=�ä��z9���~�v/�}o�9�^�χo��?���~����1����w���a
zuOV����x���M(Ӡ2�i����ew<�0
��i�`/�]i5`���4���g�P�'��.��������>؜�_�^�+^��cdY��p:L!V�]yX?V�������/��Z݆b�am�X1v>��"M�<���翮櫛����O��W�>_�{"Q6D�|����(�� �  ����_Nd�R�ofqm��pGF��ݡz�����a��d�ik�ck��bQ��~���8��Eʌ���k5`�#���j�#^�yy���;�q�z�O�?�}�n�O}*���B�.���@'�f�6�O'Y�s�B$gA���V�4�8��HZ��v�^�����D�O��▸C�w�W^���stO�40T����&�M^��:�;�aL�k��>A��x\�{¿���6���
0�;
͋�>��adb5��0N�+iz�Dw�ru�H�����"	P�ۖ���v_�����1X�R��)'�]6t40L�`D��GF���V�i�t ��0�c��Ȼ#b��6�h��r^L��`Ťگ`��H��s��K󀙩S�8� �k8��zI_���^>C�L=�(���9�;���q��/_ˊ�i������T��["�*���F흘�h��5U%��`��s�$������i�f�k�Լ�N�7@.�� ǥV��ag����,�� �Z,M��g�� �h�蚃i��0�0�"�CA9k�����dk��(���>�b�@�a蓥�s�=��a8��5��B�@��w
�w��_df&�i��O������<�H��N�d��G������d�Y�h��Q\)?��M�zH�!�� ���;��� M���      �   �  x���M��0@�3��G8,��c;��U�
���J�&Ai����J�o��*99�(o<�O��"��c~��۰m�F}l�~��}���?��6�F�m��b��ĐFC���U^�^�,�;x,,�Mӽ(ɘ�B�������4K�A���m��/���`�Sݯ�l`�cr6iWb���~�"��}V�'�������~�[����28{ѩ/��˸�ђKڗ��Zc�C2�a�6�a�Z�j��J�%�$�>�ֆ$}*0$N��2'�JɃ���(��PJ�X�g���<w���~��&��D�މ!U��|oHR�C	�x���֧���h��_�^�)�/1�-��K5dO�<24����8�����z�kZ��^7C/���q�ԛ��o���6��o�3�J%��/�j�eݯ��>��?�%:Mkb(1\�rZ1i.14��n0�OW瑡������ħe�� �\q���Okbh*���i�{v�>1�pˇ[H�?�>�O�1ra������r9      �   �   x���=��0��z�� ��ؓ�n�6�v4SD�)��"�$��[<�d301��r��U۟|��@)Q���|�_��q���^�U�9QV-5��-
�~�Z{?��(�K��XoF�Tj��v�{���)Z�oܘ�rZ�ƥ�3��� 7��� ^g����ip?�8�E���DNZjhKys[<��'�Rz+3e�R�}@�+Ns�      �      x��������/���)�����guCAqY��w��x�_���""���΃�y��$S^Ѕ.�^�珙�ݽ5���deeefUe@f Ef����*Q]�M"��,�LK�b>}!�c.ѝ�M������'��~z��(�B��<��k{���F�!�B>�?�������D2����[&�G�6��y��m�3�(3_k1��0z	�~�g�yQ/��ť���;`	�� �)���  ��ȅ���]� ������pm�߃�o���q$Ͱ.��TP���{nF�ZS!���o���4�����%R�#VY{�ޏ��~�� ���bą�h$��a��/G��/V���jM4�!�~�^&���Yn�h�lz�;1]��S���O����Re���h���z/�H�29VGm�ݼ�7l�҂٭ޣf�6�;�PT+Ҭ�B�Ш;�4���_/+�S��v.}�Lq>�}B��}�.& xv`D���!��`�D͜����Ә����� 
��X	\�U5g6�R�֒���4t�2������<o��0o�;�*���p�w�Em�;�s�+=Oq�E����s*B H�ZH�!A!A�%���bn1+|s}�����?�W��Rl,�s`>T����RE��.�.�n�0�m�C ���j�bL�Q�i��"��@SD
q�)��''uWqR_�cc���P�T��!�VdJe�����xQ����%�N����Y�z��qxa`x%�H< �ۊ���&H�p�'�}SQO�L4Gq$�*3��Ĩ���V�8���ʍ'�_���E)���f�`탰[��-�Y_F4$��O��$��'�5�X�$$��O2!��1�/��N��p���Y�lGݹ%���a��@��I���|�l
�'�p����S���ҞP��G��}�&n^�z���C��l�:�*�[Z��a�1��ޡ�9h�	*�i4$��N�'��	*%<�7���A����B����Q8�����K[!�(��덵��u�fi��`_�ph6���dp;���"b��� ��¾f72>��&|�2=U��R����,�@q�%9���6V�*���S|��J�P�_R���FCR�:�b��R����όnN�y�)�R��J��kaR�Ǽ��tC�[pS�[�0K��W���s�)�-#"�R�f��������~yO�*2�9��6��"���n��V���T�<�����t�t,�=�֫���jC�?~I\A���X\9$��3���W�k�Q4��	��W.!����j
�
��[��j��=iK%j�-�e����!l�_�W:E^�X^��n����H��4}SQO��A�ߣQ�J
�Yڮ{+�jd��&j��<Ko�k�;�R�,��UPyE@��$����]y@��({W@�'lz�34侐O6!���5VLq�����v��۹37'jV~���ms�)_�4� ��Ӑ|���N,ߡ�~GD���.�(��y�IPYc>��~5�.W��aZ)JRݪd���,����C�֯*��|�0��d�� m��s�$j�<e%��h0�;L,�����PEF1�
�r�ex�_�JW�L)�]	vˢN��1�R�;y"���� yl� ��BC�9�HH�Y;�N�3��':�"���x�c8
J}q�m��V�ɝA��Oo��S�@�
���� �hH)��p�����/�7�A�"%�����U���N�M�ݱ/�q�SQt�^�Ӑ恷,dr�s�����_V7�i�(/�u���J�܆�e}5^VK�+1N�M9)�7H��y�pF��LK�3ewd�Ġ���G��D�.Z��g�	������і�?����|���Է���M�k/���s�K�1����{��vG�IK�'C��������x�U�O�*����}���������9�����o��W�����~"��'�Kb�G�6x86|l��'��:Y7R�X�NUGm	L�٥j\���~��[�}�#���Ju�?q'{��?�A�����c�@b�����#���I�H�0����0��e+ڮM�f`/ϲ����'C��~NP)�>.�WE���~_
��#�g�����ٵ�Iݜ$_�؍;��ޭ��r2@�x&�s�D�"� ���/��O�1vuF���f*�ICbw�Yq-�._����^n4�H/z��8��&�P��·	Ŝu3��t��	E���'v`��Ȭ�lbt~�]��R�I� ��A��u�z�8��d冁mޗ�_T���MU�X��q��n�����R9����e���[s3����_F��d1���h�H�XVvg���:x_����-�!�e�#�;
���
��?ñr��(�=X	���E��d�q�f{�Uf��r@��Q��2$քe ��Nɣ����ض:V
~�l����ߢ�3v�KH|��G���"���߿7xM\��4.�}���3��7*�eaǣ*�����Q�h��bm�H�?(.����%����a�L���q���Ts��BT�MG��w��N��G��x�������b&��L.b&�0��YCs��(!�6~��HgƝ������M�[�3����+�d3��d�I�dsa�5_"~�WK��Cm���So:��� .�������[[<X��h�6���~��O@��<�uu�S'�3^k>�#ߣ��Ǟ����-~�?I���?!����(<��n���5�|S}߯�_�Ч�-���i�7�e �����g� �g#�#oV�6��q��
�B�]jB���yT��sM��_-����37u����->I��*&�|�i�$I��޿�����}�ꂷݘ�J����"��گ����2�~u�>���5�����jNE�1��a��[��?����+E��
Õ�nS�(�O̍���)�П4�2$Ŧh�v@T���m���B�'��2�E���E�f��W�}M����/*�����E>n� ��E�ީ
�\ZUhv�f�����_�߯W�����nFD{c����:�Zё�p��y(<���a�����WD멲~Q�b惟���w���������A�?d�o�_(��G�i�S#���d퍥2�IQ2���e�δC_	б�+� ��Y�oLJ<Lv��_Rϔ��C�z\���׋�3��o���#�� ��e�#��#�.�y�7�B��������U�YT"ve��c\�=�g�������*�����!�+iP}Q�o�u���׸{��?81�֐�Fޠ�F{c��ZoT,?�\�v���y�žNEwϽ\�o�H=�앱�LQ�a�RX�<`�G~�)�{��������̀��[#�H{c��Zr��Y�����yJ^��������,����N��{���s��=�u<ٹ�X��wP�tZ�� 1����ǫ�C�7���ew�퍅�-������\AA��>]���.�$�P��oh�S" /��ho,�a�Wq����}��=?�( ��{ѱ�������<v�>�����c��f[�a9�ޮ^io,�a�4�W��u�JEۍ�ǋ7q�G{�����D����֑�G�km����~�����O	��܎���
|�T���P�ʊ�l���{���}sW>Y�3��P��xD{��K��	��py,x��ͮ�����NI[1F��b��e��'9�U]{J��b�D��ʿ���p���4�Ge�(:��]$����ę�s������;���3D}�qM&��ѩ¯#(�x'(���Q5 !%�V�6x�½���'�>�[c��u6`���ֵn�0�fz �Xm\/�Lf+�U)�7j�~��ډ����t�6
�����h�_�q�D��%zb�H�D����"�%TCU���.����	],��	���K/����M̗�5���\�&��w�o��MAC+lw�p~�~�P @�q
vbj�$ܶ�U7�������B�'�5��W&��z�^˃��U�n��[�rnN��z����A�+�GgX����n�|sz{��d�C���	�t����T-��$��?��ea���6�Ǣ��p�    ���d��@�T�3�Q4� ����ϵ�^K-�2�XCt��m�l!,�@ݷ�P%g�V��ܮ��|��]f__���G�7���D���n#i���/t*6�sU*�f�b�?^��i]�Kb.��C���3'T9YZRr�? �F{�`��cVׇ�Qy�J�b���������FI��r�=0��|�@�sH="�X-�S$�]$��Z	E�Ij���#�%�����.�F��&T�??i |��D3:��S�xҎIm��|�lH]���ͻ��*�K���Y���Ų�����6$�C���86�F!ͱGQ(棃�Us���U�����o���VĄ�&�����C�����Kw�#�J��2k=ߊ®�q����x�G��ֻ�f.!�}"8X�1����|�bHv�9���r"��s)7�� p�ٲ�d���/�q�U����?�?H�;�?򏐔�&�3�H�:��2웄�O���7E��͛WCC���Z��3j��p��כ�j���*�R�-�cam=94X��4u��xE{��+�t�iP,d�Hף�zE֔��#<l��|�B[6B�mn�eF��̑n�	���Ƃ�s<�JZ����Tͬ`��rI�N�r���M
3�u�U����;�����=�y���0i�7N�pi�XXQ�r�Q���X��1�(ɽ��˟� <@]쯚�Pi�Bq=j�%ڮ�{�=�z�ݸ퉇���%w�K�SyRiD\d�Kְ+�Of8*�����3�8��r9'�������|�������X�4I%��-d�g���u��L4�Nb�Q�Mk����f��!�a�(�l׆fgǏ��N-��Z�1���4�O}'3����$6Q��q�9���y]�di�#�h�N2`]v�,jy��̻r �Z҄ʮ�����Z�}�Q�d.�T�HuFStYɟY���}<���/ �K+Ȣ�Oj�l�>����X��S��U�8�C]���[]ϧ�����EGX���ZדsI[��aC�����ES4��fd�L���t���g]p�2�`E��!D��"j9���44�f�8����|~��o�4Cѐd���϶ �l�P�`�ٮgЙ��nV��p��"$��lU��/1[� ���io<��PTa釋(ʖ������'��b!c�~��WQ��oo���r�X?��EL�9^:#�~R+��p������1eju�j°��/j�����?�w�v�ϰ�⓶E����r��4�W�*��Ƶj�M���p�t��yO���f)��r�G�'%�l{�ʍ�qO�w4���Ը���V$���ְ�}VU��,����6!��Ƴ.���u��/p��o<{v�)��[������!W� d�ѳ�&D'�.f/�S8���Yѵ��uFPT�m���-Pd�Y<l
�g�^����(a�ho<���<���c��r���	�@ �L*�X�k�ފi�����U�{_}��Y,�	�ab��ڤ���&���_,�GMu�f��Y-:���8�����wf.La8��g8���u��a����ު"�������ڢ�¬ͣ���0F�<��_����z)��<��z��␞I�!�'�w�������y�*e��D�j�2�B�N����4��$�ԏ���a�G�������v��S
�����8r\b9牉F
�/�z���1���*��˺�;	i��v�3o��N@�9w���-z�/�o�7�5���ٽ�+���nTZNX/�s�qwښ2֜vC�g��7}�k��L��	d�'��9�#{m�O�c�A��<��e-<�ZD��lB3�a9z�f9�v�%g�8ťf/<����.�B�P���j]
��y�������H�Q֢Ƅ\���&�:���N�MW�jN�%��?l�����y~�JԆ&�[*�b$�rCqX�<E\�9���~�_�T,'3�	϶��K,/9xʘL��L��v�������ݲP��҈9�y~ɺռU�=�'ǜ2|�M >ޭQ.�;#���А�Pe�6yQk�Y:_>F�z%��8�u����h��Ȓ:*,�'a�13p��(=�'�[."62�������5��y{7�R���ܪ-�FycU�c[��K#|�T�M�3<},�p>4=�9u�X�$Y�L&Y�X��Z�^��X�P'�[�V�z���wf��༁�R��o�ב��t��c����<�8�q�/!��2�ey�!��"����6\��I(Y�l�ά8p�" ê-�LN�W�(����&0>�D��]S.�&�"��:�4]�!��Pk��hH c�Pֈx, F��
y������uߡ�P�)�Ț��D�������� `�f�E/Q�.LM������:�n��#���֥)�����͍���ۺ���z�9)�$���VF�7fx��� ~��Z�6���s����,��9yY"�Wh��A��s[`�̦� ֔u��ۼ�99vf�6Pn���M`3<��W�\SZ2QAC:Z�J��z��1T`������B����-Ԍ��Hg37�^�u���dp�Tm)oXC�]�f�� k
�B=?�t�%���K��	\���s%�5QvǾ��-��C?6N5όV���9e���M��1#�Ř�Z��rD����wܹ����I��9��s�RzH+�ll'����%,��*�YN�k���Z��T��#��˶�+ڛ�gx>MR��:;�l�ĪB�q�e <�ϔ��R��Y�t��Ua�U��e7/�+dl�pR�ևjg=QH��"�;�L�M2|���1�wq�B,Bߵ�
����ס��\�B0�R9/�\G�`����vE)�°\���-�TJ�<c>+ :��4���Z�E�P3���/�
�,����_2����6.��3^�4X&[��]�䯺S�Я�b(x��e�P�1�n�$n^�K���F�7��V�.�B� ���F~&��/p<�oџ�L� em����,���mZ�*�C������s�ΩϬd�т��DC <� ��M}%�$	 �f�a�ÅLǷ{�ͮ��O�P���rpw;��������7�����h/��Gx�{�z���#I�;����E��F��)M�>�M`�1g��+%r5<�Y�=�Ѭ�TM�0ಣ���X��)�@8�j������ZK����g�q�vÆ�S�K
7�,��{?�	��f��p=���^�j��U�G�3B��{A�����q��D�	��\�VV,���� ;�Y{�w�j�oy��<��R�iʽ�օ9Y\�$�i[���)�a��i��	��Jѯ����o��ܯ���k�ق(Y]X����t u犜���ڳmn�ꨝ�+����D.#���\-���|�,��$'_�Rb#iG
J����I��h ��b�m��~�����Y�L��ա�����F*w91xECp���f�������̜F�_@�3R���*�bG�ˉ�H^%��xbN���	X����r��R�@�6��U�[g����tH]�m\�t!#�nA���=�&�Hi�O#7�=m���FZA0@�Ԛ��3���R~�ݢ�-,�F���;_T��~�~=LAz$�{
>���f� ��c�E�Zb��l]���I�h����Cw�voд�
�Y�B��
|p�{ꊆ�Sx�p���-Wo1߹.R�4y�39>8rmk�
��N.'n'W��ż���Yo5qTYSͺ�T�������M��DC(A�_wQ��'��4�N={F��ލF��۫�h��{�l��\V�)z�n�5�QಸTEx%���"BIg��u%u�����,29���`E�+Zk��9Q��K��rY�e��̼!)z��k�/����h"��F�}����C�fH�9�]E�	��Z���L�c��A;c��mn0�[��z����[v�֮�/���F+şJ��
v犆����Ұ���ݙ�/`UT� ��}�� 6B�/r�_�ѭ�����A� �<�d�_&a�n��V����&?�.y(�sR}��$2!E�f3ؤ���K"K��6��@���Otk���\jtw    W������,=�f���I��P�IY~�˰ow�]�4��`C�>0>	,���h�4�r�.>��363n�6A3����<���¡�ȃ.]�B�(�|��qa`�4��g��t���u�`��cHp�+9O����j)׫�v�ԑ��XɎ6�^��EIm�O��[|uki�����{J����(:jH�k�F��^N5P�����G���N/�]�~���I�~�So�+�7���.��@��H�^FI͋)MOp[0Z0X�r������z��j:�9���jC������g���+��f]l~an�u����	���T�R�M����qMz�7��82�\QN��7���GN̠H��e���_o+M�!�Vq<ԩV��Y��8�q�a���f��0����1*5@�T���N��>AI����p�"a��7r�.V��<��7����$I��Ϡ�6e4��>���=�/���nM��m~+�����շNn-�E�ғ�Y�'���}Bl�%wTj�u�W�S@Z�Wұl�����~��oݫ
X
�њ���HUDk���<;/��F����ӆ�dq���A�]��F����̝��P���.)[���+n�E����d���g����p�;�W[4.4�v�M��*��Gk*I�+oȬ�;��D��᷎�F�d᭰�F���p�ЛK��󌢻�sFe�]��`]k��jC>%l (:Jĥ�j�>_�f��3���l,Fi��:��H鈵
��+	��^g!�tsXd�����-������oYj�?�_�8���h;��t�1	M���<�({v�*7�7��`��^k"�ܦ�^�A0>��:�������������K��+�I*ݐL :{{I�}�/�#�I�I7�b�Z�+EW;�a�����֪����d��B�T
?V��|�3��zb��@"��J7*���� �
4�V9�f�q@q߫��J�,.j�V� ��|A*�WJ}Ra�Eu^*8�����On�K�J�%���Gq���+��7�5��7�ɱ-���ugR����F�M���U'sfD��9uv$|����HdoR����1��|�wO�yC�,�:����]�d��!�Xg�ګ�:?���l<=ʪ�\q7X:����ꮦ?��7��YKﯛ�X�i��`����$?�����)E�)�禿�Ǵ��0i�7��2���&��ɯ���|G
	�7�ZC&�V��K�U���4Hn��Jt���4�΍{��0�z��;��X�M���+�g��O�!�#;��ԯ����;Z�����?�����'�G�4�x�h$���En��s �Z�7�R���ʅ�:UJN�I���d�����Χ�m���x��=�h I2�躌,�8��y���pd���;ɒ0Z0;����jin.���!��v�lg+F�N�'K�ǹ���%�-�.�~��8�E1�� զ�5��PkR�����Q�3���db�c�Ge?F�R\��f���u�С:�	�=���3��j])�A���!����h��Ȇ�k�J>ʃ@d�Ea�#�?��cN���e�S4��5����c�#
� ^�U���t
�dE �X�#�/uD}٭��j�R���Y�Ai��Izvn���s[k����� �h�%ȴi��b��p@��+ǻ����g�/c�'��la	�A��9jV�%k-A�;[�:AgE�Ԯ�tò�����-���y;E4�4�*�4W�j�l��e����~�F��i!N�%ָ�H:�N_�����RS�o�bW�g��xT��9�Q��� �V3��2������*�0:d��MN)
�eh�f����E]��3yeQq^�'�A���������S����v'�f�GQ���}Qe!C�AA��g9#�+����^�ʓAU�z学�����rs�����|~�28�MT�HCy<�=D���w�3��}]&0 �A`���/o$0�F���ك��iwθ�T�-��n���O�f����
��0C!��0#��9>h٪��k�pcڃ�v�ح�b0���"�Q0F͞?^�MF��46��;�b9�Ʒ�uA�'�Y�N���N�H��(4�bG��ĩ�+���
0Tܫ�1:�pL�FeDKEjL�I^�W䮧��msE��`w�bIU5�{��V�F�=�f�'�U�*�<�=�=���M���i��=e1�dqv��P,��<t&��Hn�=NZ��5�
<�`�C4�f@'�ȥtr�~�<ĳ�����&�Ñ<�Y����A鲂>�ΎE۩�)���!.���/g lr����!�N3^�=ժ��b�\����AD��xY�G�:�bdNl7��6Z�u6�Mu�ˎ��f�]���F���5�BQ,���>��鈎���4B�I:͐� SZ�z�S�أ�~D�&�1�`�;����u�$��`$v���y<�u�����6l��ܾ�#��4���B:�(Lt�B�7]�(���4rr�Y���M����&5�S��Ȫ�����
�T'��7��īS�ۅ�k�n�G���7:�|K*Qwn���l�!�ф�(�U��	��Q���6��N� �rѮ=v�f���w{A�'�����+(�[#�HC(9���)�5��i����$RX]G@A4}D�'�rFo-��*�ne}9�6�0��Ů�G��zX�Չ���A�Y�8�Tdя���R��<�Dḭ�B� ~4�7֋Q��Κ����$CJ�nU�9X� �T�Y���𢄤i4<�c�G�Z�kx��`;8��`D��ѳ	|�A��}v}h��|,T$G��%Kl�1��Dka����W���f�퍂��a�4v<���O���w$'��A�:���i,Dv48��F�_6�΀�7�;�uޝ���n����L�TC�i4�Y:�cKG3���!n͝3��Y y�د��	�</�2���J��]�4�-X��w:N�R�j��t�
F�?����|"��,������S�"�e+ ��rQ���(��!�\,�Z�1;��auL-����,�k�&�rg���d.Iîh%2���&P�GA�������%�u.��˫�C��Y����Y�s~2�	<d�rY�;�6gl�@��W\0
^r�^�\d�0���f9�:>�q�'���{�,`ȁ�$ 3�<���a��g2W�Sr[RW�$�ƥMo;�}A��Ay�Gߚ|AF6��&:�>,wbN�ǿ<�ǀĀ���bhHZv�,��ͺj�f�Q�,	�X���L`	��p�I��+��6>'h12���f����T�����R^;�r3�����lu��V�nR��2?�ے&2nw	-a������A�?�4EB!�����R���sN���~���D� F&b(gl��V~���d�m���Y����	Yb�z�a�4�5_�}v�"�㓏_Eƶ�#7�=p�I���2�����L�io���^i� �t��L���&P�mG6��:Ҋkrڪ��"D�.6,���+C?X~6��~��掛 ����H�໣�@Su��e{���^[һE��/��qMH���SG@|�=%j��Pa��'��#�t$8n��t��:HX�4�;~r��g%�.w��(j3f=a���/��@��bs������	J�=�_�����y$>2�i�ءll#��@)��t�9j��Ѩא�lQ��M�0��C�%��S}�F�3(�v��HC@���4��Yxn��~�������FwO�W���j~[�����;ިW� �ֶ�ۄ}�g�N=�_������F�"��(��#҃-�8�y8v�y�iC�vrk.ҺS
ke�6q
��>;g��?�g�!P�Uy�M�	�4���&��j²j��bn/>����̰�i�
� _I�f����֝�쌦\K֧���p��ʬݷ[���{\-��O�kDC�������b�6V/�w^)�8&��il(�̈́?�1m�4e:�A�d�l4Cu����}A�ۓ��s5�z%��ϡz�d���0��(Y�Ƙ�h�$�ib��<�O ey7c������Zm ��Ҫ�`���G��&�¤Z-�� ������"	~a �s+�K����E{��.d�"�8Jc)�J    6gpZe�6ʇ
T���Y4{>�cg�YXt��p)uRi7^����$L	����0�a+�[F��/�94^l�I���c�s:[�e�۹?�
��Y��u�%mz��l%��l�����*����H�iFNB�b��*|F�W��Y8%���ft-�X>>��\L��.7x��鵪���U����'�5mkbc�/��-�����Z�_>]G����ߗ���&������V���@��9.��DU!t���]Mt�`���t�n
[���+�+ﷹ\��_2UrP���^��u }�����7�C �f ��-�Gj�n�����ғ��g�昆@���Si�2.�bIC��YH��m��3Ɨ�nsk�:̨@����+z��O���G��H���!NQ8���
�=��Ϟa������Q�c�w�u.%�-����UW*,z��&V�^��R������jy+QO���Q	$�y$ho85%Ŧ&�9� v�y���`��ѥ�˵yL�����&�9$y�F�V�t���Ōn"S�J"�9Bi��S���`����\�P�F"(�`!����*&|.����x�7��x��]6�ڭ��r��^��ʡ/��|�9>U�20���F]��Q�����Y���IE�f��~�r#Zt�3%��4���ɇ����
(Mo�����3,mPo&Ks=ȒR`/L���4w�sVI�]�Ma<2����Z#��P7 ��M��4G4�=9;�l}�OTG9֌��v<��%:�1�b?�X���Q��6-27�;m�"���Y#�ٜ��N�RGp�N�h�# ^�H3B�z��EY��*V�+��cZt�HU�)-D8N��H����Sj4D\� �����"Y>Pc���k���L��Z/�z�v?XVa_��-]e}�H�7�s�!cv���y���!�M�y4ǘsB�/���&r�v9�N	�_�&,$����N�7|�g�H�R�x��K>��8�ƶ�� ?T�C:�v�5J4o�n&�`��ac�d&�[��G�>��ފ���Ʋ�TM��(��[n��{,z�<w?lA�IǱ�g
J0�3�P��#_����}ح��m��J˲�:�<&��4���(�*Tu�!F�x+�}��q������]�-|�\����O/!/��`{1�c�+.���o�1�;� ( 8������Xޘ��q�V�ʐ���F�\z9�PHk�ɀ��\{6|��������d�W��'�ZY.��r#��J-7��*��{n�[�|���,��)�wW��o������HYI-k���������1��/.����ٰ��0��|��/'���2�ۉ�`ɤ|F-*�C��U빆�R��3���dAe�MS�
�'O��L篂;��#�*��{���=r���_/�A��0��D;���U�Q7Qĥ�6�Ur�_ H�G�a>��v=���쫖�ʪ3:��ԾTo��a�J�W'9.4�/ȓK�t�;�#�<�1�����������N���M���>��M_�<4�5��w�\����Q��-��ֱEq��c-���i�*����Y3�uX��k��n)�+')����~�o�jm�BC}��F.�o��2�^�,��f{�7�]iI{߭:¦��,l&�Ϋ�^�c�v���u<[E���P�� +�t�(�ŝ���2���W�v�!uP���N=���L_mۼ��Sɥ9��X��b��Шv`ϗD����Lg�=�#։�������S��+�^~�'o8Y㗻��f�Q�E�J�/q>�8)F��P/[r�I�H�#�'�8�j��������&)~���������j6栒M�m��Z��ޚ�:���b;d�v�c#�C3|'����ܷ֟w9�Zf�А ��Ksj/��ݢ;����`����m���w� =i໲�[����'��s�\�^>Xw�#o��x���:ל�������"��+lf�΁_h���e��\�o���)&(������C��.�qkL�Fi�){V���\y�9B=0���׸:/���Yӝ�df�����ԅ����� )L?DA�#�r^L|=^?����;H%��o)i����n3vaM%K����*-"������@
�j��jT�{�,���\Yd�PX7ˠ�[�rCk����YZW�W4>�x;3G4�=�I��nlk�g���{N|�w��LӾN�ǵ^y��~!�$i Rl�Dq	�q���&���ȥW,�!P��` 5�.5��\̇{�Ͽ"n�֭*�h�aȽ����p4خX7��|O<v?%� �톌)�E�S|;�9�����)+{��䵃����{*uk�$+Ս_�ۅ� _~:�q����HC�@.)��J%UL�vŰoHw!�8y&r���O�q�{	_#�o*���X���X�ܲߴ�v3�f���S�C���N����c�v�\ho��|?>��K���$o���~�._ ͑4�1h�\����TdqԶ�Bo��y+XZ�NK�WA~�X��Ǿ��60{���k�(>5���ċ9�S�9��ld�H��q�v�񗦈��o���?8��$+��!kR�Kש�q�Ä��SƢ(l����/M:'���TlKꆭ8�/4��=����<�*�Å�
��˴5���[�V�M$|�]��3�}}�F.�q��W����c�L�Rv�M�V`��f��& �ͺ�\d��v{A��AT���bE# /�O�bb���b�#�EmT��N$4mg�2��,��ˀ4�����l��>n������+珼{�c|�5�DAC�V@�!6h*CE���6O����Y���R����)��r��'L�b�?���+��I
�h��F��Pn�m�F�*��C����6y�	<������ѥ�	N0��mw��9��!�V���v�����_��ƍ�;�w�K�}D�)h4�	d��ެv4k�'c�U�3}{k&^���i��'�蟟�~���+},O�J�&�E�YZ���;x1��F�l+��n4\��S��x���$=\�9o:f���8XO�%����z�G�5��So[��u�A�z�EdI���[3���pn/�ǿT��*���.�5� �� 6ΉJǲ���Y����d`�m)T�V���l�����V����r}qP_�����O�Y����G���SfZ���x��5��O
$d��x�{�1J`g���<�N��&�'����I 
B�R���u`��?^YV)�K�vh�W��܀	�(��:�u�!�!�>�j�s��\H]g�IweD <�"w�$�~���d�p� ��ݐ�j�^�b���J^
�������l�r49.=)?1�녍�G�|��h�D��a�&��lp����Af>˟�ͥ:^s�brun" W�EO�֦�.}�-:�<y ��4`�����~�l�����qi�7�G*��1�$j6JfsK��G��%�Ջ�(�.�)�Ǯ'�a�A��Uۋ����+�r�b٬﫮ϯ�RӀ'P�3T2�v���OzP�7�]Z
�1�O�I1hğ�]�O�+ԥ�T7�-9�x����ɚ6't��uG��!+�l�G$ ��=AC�M�z�n�&��ǧ�o�i( �g�/'�@��a� p6$��և����n���-[���k%P?<�����h�^F���|����� 8ީw��c:���6N�3�M_�,캧��4tT�R���Dמ;v�L'�&%h�GW��C�pO���)ey��̇;�be��ˮ�w�����[�p_��|-�MOnl���Ҙ�1�t<�m�z�Uc�'���,�cn��7(Yc�)+jh�W��V�����t�p� ��l��\q����IF=4��J�ב}��[;Q���'��aE3�F�oc��!�M�g��!e�/ì�3�����d�-�s*�U�t(��y�`?gS�L�d2G���l�ԋоh���[2���#��*��3��ܮ�}����X�Z&5�'��\��#�䆲�g֥R/J�d)�
��6ۯ~�z
H�o<�}7�]0��,z�W�X��}�w�VH�&+����aܷ��s	/�8��i�7M�iq�ӵ"�z)����Ě�e���>TE�#Ջb霦���$KQ���� �  ��?U���Hr������ܗ��Eϫ�N�L�K��gMRֳaG�� �����U���d>���P_�J��|�Wpu=��!(���&��X2^�8SB�8qa���N�j��3����6X凮�w���[O�<���������)� ��}#�FC�#���ǜ�xCEﾙ�G�ssm���>���#���
}UXBZ����~uW+�-��Q��p`1 �z�����O����E��vͦ�Ӱ���ǶGܑۤ����̹�O�b4i7I�^���  C_�@��:��@r��Fq��4�}��R�߬�}!�m�*��t放2n���+�g�+	�>޸�j�?�Z�R�|�/�(����bn��[jL�c4��kk���1�H�2Q�Ɯ�$�	�Jg��4��W]q�����%��N�ް]���;<�ݢ}.�o"��E>Xk�X�����ZAA�3(~��7*�U�h����_R"������ ���3�]�	�-�N�e�A1���t5u<�G�71D�$M�1��+��,�0\\{��c�z�Nps��jd�]ҥԙgO�+ȓ�Q��A%��h���N�'�y�Y�ɳ�	b0v�"W�jy[�9o�����.����b/\GG :H��R�>�X��p�������1������{�G�受}j̍TrF~d.:����3����z��6��q��6�Me�g��3W�
8��_g=E{���]��Q��D
w�OaP}�@�"�)qS���ŅG��+�X܃�$}�L�mR@��1$E%��BS^�J�_Zܴ�z�������ޙ8yƁ��󌎎xIQW�P�h��8�������>�I���*�������wɤ��`	�aX��ݻ�:>#J��6��VF.D�zUE�rA�M�|��6�b��᎟4���W�T�ڝr����
�+��:�,�� ���`�4���x{{� ��}      �      x������ � �     