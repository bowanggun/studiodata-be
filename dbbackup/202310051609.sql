PGDMP     *    	            	    {         
   datindrive    15.4    15.4 �    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    16398 
   datindrive    DATABASE     �   CREATE DATABASE datindrive WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'English_Indonesia.1252';
    DROP DATABASE datindrive;
                postgres    false                        2615    16399    v1    SCHEMA        CREATE SCHEMA v1;
    DROP SCHEMA v1;
                postgres    false            �            1259    16400    failed_jobs    TABLE     "  CREATE TABLE v1.failed_jobs (
    id bigint NOT NULL,
    uuid character varying(255) NOT NULL,
    connection text NOT NULL,
    queue text NOT NULL,
    payload text NOT NULL,
    exception text NOT NULL,
    failed_at timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);
    DROP TABLE v1.failed_jobs;
       v1         heap    postgres    false    6            �            1259    16406    failed_jobs_id_seq    SEQUENCE     w   CREATE SEQUENCE v1.failed_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE v1.failed_jobs_id_seq;
       v1          postgres    false    6    215            �           0    0    failed_jobs_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE v1.failed_jobs_id_seq OWNED BY v1.failed_jobs.id;
          v1          postgres    false    216            �            1259    16407 
   migrations    TABLE     �   CREATE TABLE v1.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);
    DROP TABLE v1.migrations;
       v1         heap    postgres    false    6            �            1259    16410    migrations_id_seq    SEQUENCE     �   CREATE SEQUENCE v1.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE v1.migrations_id_seq;
       v1          postgres    false    217    6            �           0    0    migrations_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE v1.migrations_id_seq OWNED BY v1.migrations.id;
          v1          postgres    false    218            �            1259    16411    mst_collection    TABLE     �  CREATE TABLE v1.mst_collection (
    id bigint NOT NULL,
    nama_collection text NOT NULL,
    judul text,
    tipe character varying(50),
    default_chart character varying(50),
    referensi_data text,
    catatan text,
    route_name text,
    table_name text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    ref_organisasi_id integer,
    organisasi text,
    satuan character varying(100),
    definisi text,
    rumus_perhitungan text,
    cara_memperoleh_data text,
    urusan text,
    is_multidimensi boolean DEFAULT false NOT NULL
);
    DROP TABLE v1.mst_collection;
       v1         heap    postgres    false    6            �            1259    16416    mst_collection_id_seq    SEQUENCE     z   CREATE SEQUENCE v1.mst_collection_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE v1.mst_collection_id_seq;
       v1          postgres    false    6    219            �           0    0    mst_collection_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE v1.mst_collection_id_seq OWNED BY v1.mst_collection.id;
          v1          postgres    false    220            �            1259    16417    mst_dashboard    TABLE     �  CREATE TABLE v1.mst_dashboard (
    id bigint NOT NULL,
    nama_dashboard text NOT NULL,
    frame text NOT NULL,
    link text NOT NULL,
    catatan text,
    is_hidden boolean DEFAULT false NOT NULL,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.mst_dashboard;
       v1         heap    postgres    false    6            �            1259    16423    mst_dashboard_id_seq    SEQUENCE     y   CREATE SEQUENCE v1.mst_dashboard_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE v1.mst_dashboard_id_seq;
       v1          postgres    false    221    6            �           0    0    mst_dashboard_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE v1.mst_dashboard_id_seq OWNED BY v1.mst_dashboard.id;
          v1          postgres    false    222            �            1259    16424    mst_jenis_dok    TABLE     [  CREATE TABLE v1.mst_jenis_dok (
    id bigint NOT NULL,
    jenis_dokumen character varying(200) NOT NULL,
    ref_grup_dok_id integer,
    keterangan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.mst_jenis_dok;
       v1         heap    postgres    false    6            �            1259    16429    mst_jenis_dok_id_seq    SEQUENCE     y   CREATE SEQUENCE v1.mst_jenis_dok_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE v1.mst_jenis_dok_id_seq;
       v1          postgres    false    6    223            �           0    0    mst_jenis_dok_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE v1.mst_jenis_dok_id_seq OWNED BY v1.mst_jenis_dok.id;
          v1          postgres    false    224            �            1259    16430    password_reset_tokens    TABLE     �   CREATE TABLE v1.password_reset_tokens (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);
 %   DROP TABLE v1.password_reset_tokens;
       v1         heap    postgres    false    6            �            1259    16435    personal_access_tokens    TABLE     �  CREATE TABLE v1.personal_access_tokens (
    id bigint NOT NULL,
    tokenable_type character varying(255) NOT NULL,
    tokenable_id bigint NOT NULL,
    name character varying(255) NOT NULL,
    token character varying(64) NOT NULL,
    abilities text,
    last_used_at timestamp(0) without time zone,
    expires_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
 &   DROP TABLE v1.personal_access_tokens;
       v1         heap    postgres    false    6            �            1259    16440    personal_access_tokens_id_seq    SEQUENCE     �   CREATE SEQUENCE v1.personal_access_tokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE v1.personal_access_tokens_id_seq;
       v1          postgres    false    226    6            �           0    0    personal_access_tokens_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE v1.personal_access_tokens_id_seq OWNED BY v1.personal_access_tokens.id;
          v1          postgres    false    227            �            1259    16441    ref_data_tag    TABLE     &  CREATE TABLE v1.ref_data_tag (
    id bigint NOT NULL,
    data_tag text NOT NULL,
    keterangan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.ref_data_tag;
       v1         heap    postgres    false    6            �            1259    16446    ref_data_tag_id_seq    SEQUENCE     x   CREATE SEQUENCE v1.ref_data_tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE v1.ref_data_tag_id_seq;
       v1          postgres    false    6    228            �           0    0    ref_data_tag_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE v1.ref_data_tag_id_seq OWNED BY v1.ref_data_tag.id;
          v1          postgres    false    229            �            1259    16447    ref_grup_dok    TABLE     <  CREATE TABLE v1.ref_grup_dok (
    id bigint NOT NULL,
    grup_dokumen character varying(200) NOT NULL,
    keterangan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.ref_grup_dok;
       v1         heap    postgres    false    6            �            1259    16452    ref_grup_dok_id_seq    SEQUENCE     x   CREATE SEQUENCE v1.ref_grup_dok_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE v1.ref_grup_dok_id_seq;
       v1          postgres    false    230    6            �           0    0    ref_grup_dok_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE v1.ref_grup_dok_id_seq OWNED BY v1.ref_grup_dok.id;
          v1          postgres    false    231            �            1259    24617    ref_kode_wilayah    TABLE     �  CREATE TABLE v1.ref_kode_wilayah (
    id bigint NOT NULL,
    kemendagri_provinsi_kode character varying(20),
    kemendagri_kota_kode character varying(50),
    kemendagri_provinsi_nama character varying(50),
    kemendagri_kota_nama text NOT NULL,
    bps_provinsi_kode character varying(50),
    bps_kota_kode character varying(50),
    bps_provinsi_nama character varying(50),
    bps_kota_nama text NOT NULL,
    latitude text,
    longitude text,
    kode_pos character varying(200),
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
     DROP TABLE v1.ref_kode_wilayah;
       v1         heap    postgres    false    6            �            1259    24616    ref_kode_wilayah_id_seq    SEQUENCE     |   CREATE SEQUENCE v1.ref_kode_wilayah_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE v1.ref_kode_wilayah_id_seq;
       v1          postgres    false    253    6            �           0    0    ref_kode_wilayah_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE v1.ref_kode_wilayah_id_seq OWNED BY v1.ref_kode_wilayah.id;
          v1          postgres    false    252            �            1259    16453    ref_organisasi    TABLE     P  CREATE TABLE v1.ref_organisasi (
    id bigint NOT NULL,
    organisasi character varying(200) NOT NULL,
    singkatan text,
    keterangan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.ref_organisasi;
       v1         heap    postgres    false    6            �            1259    16458    ref_organisasi_id_seq    SEQUENCE     z   CREATE SEQUENCE v1.ref_organisasi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE v1.ref_organisasi_id_seq;
       v1          postgres    false    232    6            �           0    0    ref_organisasi_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE v1.ref_organisasi_id_seq OWNED BY v1.ref_organisasi.id;
          v1          postgres    false    233            �            1259    16459 	   ref_topik    TABLE     2  CREATE TABLE v1.ref_topik (
    id bigint NOT NULL,
    topik character varying(200) NOT NULL,
    keterangan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.ref_topik;
       v1         heap    postgres    false    6            �            1259    16464    ref_topik_id_seq    SEQUENCE     u   CREATE SEQUENCE v1.ref_topik_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE v1.ref_topik_id_seq;
       v1          postgres    false    234    6            �           0    0    ref_topik_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE v1.ref_topik_id_seq OWNED BY v1.ref_topik.id;
          v1          postgres    false    235            �            1259    24601    trx_collection    TABLE       CREATE TABLE v1.trx_collection (
    id bigint NOT NULL,
    mst_collection_id integer,
    tahun integer NOT NULL,
    target text,
    realisasi text,
    sumber text,
    catatan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    kemendagri_kota_kode character varying(50) DEFAULT '32.71'::character varying NOT NULL,
    kemendagri_kota_nama text DEFAULT 'KOTA BOGOR'::text NOT NULL
);
    DROP TABLE v1.trx_collection;
       v1         heap    postgres    false    6            �            1259    24600    trx_collection_id_seq    SEQUENCE     z   CREATE SEQUENCE v1.trx_collection_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE v1.trx_collection_id_seq;
       v1          postgres    false    251    6            �           0    0    trx_collection_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE v1.trx_collection_id_seq OWNED BY v1.trx_collection.id;
          v1          postgres    false    250            �            1259    16465    trx_data    TABLE     4  CREATE TABLE v1.trx_data (
    id bigint NOT NULL,
    mst_jenis_dok_id integer,
    sumber_data text NOT NULL,
    jenis_data character varying(100) NOT NULL,
    nama_data text NOT NULL,
    deskripsi_data text NOT NULL,
    format_data character varying(100) NOT NULL,
    media_type character varying(100),
    size_data character varying(100),
    ekstensi_data character varying(100),
    nama_file text,
    url_file_upload text,
    url_upload text,
    is_shared boolean DEFAULT false NOT NULL,
    webbappeda_visible boolean DEFAULT false NOT NULL,
    satudata_visible boolean DEFAULT false NOT NULL,
    catatan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.trx_data;
       v1         heap    postgres    false    6            �            1259    16473    trx_data_id_seq    SEQUENCE     t   CREATE SEQUENCE v1.trx_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE v1.trx_data_id_seq;
       v1          postgres    false    6    236            �           0    0    trx_data_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE v1.trx_data_id_seq OWNED BY v1.trx_data.id;
          v1          postgres    false    237            �            1259    16474    trx_data_organisasi    TABLE     R  CREATE TABLE v1.trx_data_organisasi (
    id bigint NOT NULL,
    trx_data_id integer,
    ref_organisasi_id integer,
    organisasi text NOT NULL,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
 #   DROP TABLE v1.trx_data_organisasi;
       v1         heap    postgres    false    6            �            1259    16479    trx_data_organisasi_id_seq    SEQUENCE        CREATE SEQUENCE v1.trx_data_organisasi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE v1.trx_data_organisasi_id_seq;
       v1          postgres    false    6    238            �           0    0    trx_data_organisasi_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE v1.trx_data_organisasi_id_seq OWNED BY v1.trx_data_organisasi.id;
          v1          postgres    false    239            �            1259    16480    trx_data_tag    TABLE     G  CREATE TABLE v1.trx_data_tag (
    id bigint NOT NULL,
    trx_data_id integer,
    ref_data_tag_id integer,
    data_tag text NOT NULL,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.trx_data_tag;
       v1         heap    postgres    false    6            �            1259    16485    trx_data_tag_id_seq    SEQUENCE     x   CREATE SEQUENCE v1.trx_data_tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE v1.trx_data_tag_id_seq;
       v1          postgres    false    6    240            �           0    0    trx_data_tag_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE v1.trx_data_tag_id_seq OWNED BY v1.trx_data_tag.id;
          v1          postgres    false    241            �            1259    16486    trx_data_topik    TABLE     C  CREATE TABLE v1.trx_data_topik (
    id bigint NOT NULL,
    trx_data_id integer,
    ref_topik_id integer,
    topik text NOT NULL,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.trx_data_topik;
       v1         heap    postgres    false    6            �            1259    16491    trx_data_topik_id_seq    SEQUENCE     z   CREATE SEQUENCE v1.trx_data_topik_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE v1.trx_data_topik_id_seq;
       v1          postgres    false    242    6            �           0    0    trx_data_topik_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE v1.trx_data_topik_id_seq OWNED BY v1.trx_data_topik.id;
          v1          postgres    false    243            �            1259    24645    trx_laju_pertumbuhan_pdrb    TABLE     O  CREATE TABLE v1.trx_laju_pertumbuhan_pdrb (
    id bigint NOT NULL,
    mst_collection_id integer,
    tahun integer NOT NULL,
    kategori text NOT NULL,
    urutan character varying(5),
    target text,
    realisasi text,
    sumber text,
    catatan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    kemendagri_kota_kode character varying(50) DEFAULT '32.71'::character varying,
    kemendagri_kota_nama text DEFAULT 'KOTA BOGOR'::text
);
 )   DROP TABLE v1.trx_laju_pertumbuhan_pdrb;
       v1         heap    postgres    false    6            �            1259    24644     trx_laju_pertumbuhan_pdrb_id_seq    SEQUENCE     �   CREATE SEQUENCE v1.trx_laju_pertumbuhan_pdrb_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE v1.trx_laju_pertumbuhan_pdrb_id_seq;
       v1          postgres    false    255    6            �           0    0     trx_laju_pertumbuhan_pdrb_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE v1.trx_laju_pertumbuhan_pdrb_id_seq OWNED BY v1.trx_laju_pertumbuhan_pdrb.id;
          v1          postgres    false    254            �            1259    16492 $   trx_luas_wilayah_kecamatan_kelurahan    TABLE       CREATE TABLE v1.trx_luas_wilayah_kecamatan_kelurahan (
    id bigint NOT NULL,
    mst_collection_id integer NOT NULL,
    tahun integer NOT NULL,
    kecamatan character varying(50) NOT NULL,
    jml_kelurahan integer NOT NULL,
    luas_wilayah double precision NOT NULL,
    satuan character varying(50),
    catatan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
 4   DROP TABLE v1.trx_luas_wilayah_kecamatan_kelurahan;
       v1         heap    postgres    false    6            �            1259    16497 +   trx_luas_wilayah_kecamatan_kelurahan_id_seq    SEQUENCE     �   CREATE SEQUENCE v1.trx_luas_wilayah_kecamatan_kelurahan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 >   DROP SEQUENCE v1.trx_luas_wilayah_kecamatan_kelurahan_id_seq;
       v1          postgres    false    244    6            �           0    0 +   trx_luas_wilayah_kecamatan_kelurahan_id_seq    SEQUENCE OWNED BY     s   ALTER SEQUENCE v1.trx_luas_wilayah_kecamatan_kelurahan_id_seq OWNED BY v1.trx_luas_wilayah_kecamatan_kelurahan.id;
          v1          postgres    false    245            �            1259    16498    trx_riwayat_data    TABLE     U  CREATE TABLE v1.trx_riwayat_data (
    id bigint NOT NULL,
    trx_data_id integer,
    mst_jenis_dok_id integer,
    sumber_data text NOT NULL,
    jenis_data character varying(100) NOT NULL,
    nama_data text NOT NULL,
    deskripsi_data text NOT NULL,
    format_data character varying(100) NOT NULL,
    media_type character varying(100),
    size_data character varying(100),
    ekstensi_data character varying(100),
    nama_file text,
    url_file_upload text,
    url_upload text,
    is_shared boolean DEFAULT false NOT NULL,
    webbappeda_visible boolean DEFAULT false NOT NULL,
    satudata_visible boolean DEFAULT false NOT NULL,
    catatan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
     DROP TABLE v1.trx_riwayat_data;
       v1         heap    postgres    false    6            �            1259    16506    trx_riwayat_data_id_seq    SEQUENCE     |   CREATE SEQUENCE v1.trx_riwayat_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE v1.trx_riwayat_data_id_seq;
       v1          postgres    false    6    246            �           0    0    trx_riwayat_data_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE v1.trx_riwayat_data_id_seq OWNED BY v1.trx_riwayat_data.id;
          v1          postgres    false    247            �            1259    16507    users    TABLE     t  CREATE TABLE v1.users (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    email_verified_at timestamp(0) without time zone,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.users;
       v1         heap    postgres    false    6            �            1259    16512    users_id_seq    SEQUENCE     q   CREATE SEQUENCE v1.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
    DROP SEQUENCE v1.users_id_seq;
       v1          postgres    false    248    6            �           0    0    users_id_seq    SEQUENCE OWNED BY     5   ALTER SEQUENCE v1.users_id_seq OWNED BY v1.users.id;
          v1          postgres    false    249            �           2604    16513    failed_jobs id    DEFAULT     h   ALTER TABLE ONLY v1.failed_jobs ALTER COLUMN id SET DEFAULT nextval('v1.failed_jobs_id_seq'::regclass);
 9   ALTER TABLE v1.failed_jobs ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    216    215            �           2604    16514    migrations id    DEFAULT     f   ALTER TABLE ONLY v1.migrations ALTER COLUMN id SET DEFAULT nextval('v1.migrations_id_seq'::regclass);
 8   ALTER TABLE v1.migrations ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    218    217            �           2604    16515    mst_collection id    DEFAULT     n   ALTER TABLE ONLY v1.mst_collection ALTER COLUMN id SET DEFAULT nextval('v1.mst_collection_id_seq'::regclass);
 <   ALTER TABLE v1.mst_collection ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    220    219            �           2604    16516    mst_dashboard id    DEFAULT     l   ALTER TABLE ONLY v1.mst_dashboard ALTER COLUMN id SET DEFAULT nextval('v1.mst_dashboard_id_seq'::regclass);
 ;   ALTER TABLE v1.mst_dashboard ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    222    221            �           2604    16517    mst_jenis_dok id    DEFAULT     l   ALTER TABLE ONLY v1.mst_jenis_dok ALTER COLUMN id SET DEFAULT nextval('v1.mst_jenis_dok_id_seq'::regclass);
 ;   ALTER TABLE v1.mst_jenis_dok ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    224    223            �           2604    16518    personal_access_tokens id    DEFAULT     ~   ALTER TABLE ONLY v1.personal_access_tokens ALTER COLUMN id SET DEFAULT nextval('v1.personal_access_tokens_id_seq'::regclass);
 D   ALTER TABLE v1.personal_access_tokens ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    227    226            �           2604    16519    ref_data_tag id    DEFAULT     j   ALTER TABLE ONLY v1.ref_data_tag ALTER COLUMN id SET DEFAULT nextval('v1.ref_data_tag_id_seq'::regclass);
 :   ALTER TABLE v1.ref_data_tag ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    229    228            �           2604    16520    ref_grup_dok id    DEFAULT     j   ALTER TABLE ONLY v1.ref_grup_dok ALTER COLUMN id SET DEFAULT nextval('v1.ref_grup_dok_id_seq'::regclass);
 :   ALTER TABLE v1.ref_grup_dok ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    231    230            �           2604    24620    ref_kode_wilayah id    DEFAULT     r   ALTER TABLE ONLY v1.ref_kode_wilayah ALTER COLUMN id SET DEFAULT nextval('v1.ref_kode_wilayah_id_seq'::regclass);
 >   ALTER TABLE v1.ref_kode_wilayah ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    252    253    253            �           2604    16521    ref_organisasi id    DEFAULT     n   ALTER TABLE ONLY v1.ref_organisasi ALTER COLUMN id SET DEFAULT nextval('v1.ref_organisasi_id_seq'::regclass);
 <   ALTER TABLE v1.ref_organisasi ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    233    232            �           2604    16522    ref_topik id    DEFAULT     d   ALTER TABLE ONLY v1.ref_topik ALTER COLUMN id SET DEFAULT nextval('v1.ref_topik_id_seq'::regclass);
 7   ALTER TABLE v1.ref_topik ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    235    234            �           2604    24604    trx_collection id    DEFAULT     n   ALTER TABLE ONLY v1.trx_collection ALTER COLUMN id SET DEFAULT nextval('v1.trx_collection_id_seq'::regclass);
 <   ALTER TABLE v1.trx_collection ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    251    250    251            �           2604    16523    trx_data id    DEFAULT     b   ALTER TABLE ONLY v1.trx_data ALTER COLUMN id SET DEFAULT nextval('v1.trx_data_id_seq'::regclass);
 6   ALTER TABLE v1.trx_data ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    237    236            �           2604    16524    trx_data_organisasi id    DEFAULT     x   ALTER TABLE ONLY v1.trx_data_organisasi ALTER COLUMN id SET DEFAULT nextval('v1.trx_data_organisasi_id_seq'::regclass);
 A   ALTER TABLE v1.trx_data_organisasi ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    239    238            �           2604    16525    trx_data_tag id    DEFAULT     j   ALTER TABLE ONLY v1.trx_data_tag ALTER COLUMN id SET DEFAULT nextval('v1.trx_data_tag_id_seq'::regclass);
 :   ALTER TABLE v1.trx_data_tag ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    241    240            �           2604    16526    trx_data_topik id    DEFAULT     n   ALTER TABLE ONLY v1.trx_data_topik ALTER COLUMN id SET DEFAULT nextval('v1.trx_data_topik_id_seq'::regclass);
 <   ALTER TABLE v1.trx_data_topik ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    243    242            �           2604    24648    trx_laju_pertumbuhan_pdrb id    DEFAULT     �   ALTER TABLE ONLY v1.trx_laju_pertumbuhan_pdrb ALTER COLUMN id SET DEFAULT nextval('v1.trx_laju_pertumbuhan_pdrb_id_seq'::regclass);
 G   ALTER TABLE v1.trx_laju_pertumbuhan_pdrb ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    254    255    255            �           2604    16527 '   trx_luas_wilayah_kecamatan_kelurahan id    DEFAULT     �   ALTER TABLE ONLY v1.trx_luas_wilayah_kecamatan_kelurahan ALTER COLUMN id SET DEFAULT nextval('v1.trx_luas_wilayah_kecamatan_kelurahan_id_seq'::regclass);
 R   ALTER TABLE v1.trx_luas_wilayah_kecamatan_kelurahan ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    245    244            �           2604    16528    trx_riwayat_data id    DEFAULT     r   ALTER TABLE ONLY v1.trx_riwayat_data ALTER COLUMN id SET DEFAULT nextval('v1.trx_riwayat_data_id_seq'::regclass);
 >   ALTER TABLE v1.trx_riwayat_data ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    247    246            �           2604    16529    users id    DEFAULT     \   ALTER TABLE ONLY v1.users ALTER COLUMN id SET DEFAULT nextval('v1.users_id_seq'::regclass);
 3   ALTER TABLE v1.users ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    249    248            �          0    16400    failed_jobs 
   TABLE DATA           ]   COPY v1.failed_jobs (id, uuid, connection, queue, payload, exception, failed_at) FROM stdin;
    v1          postgres    false    215   �       �          0    16407 
   migrations 
   TABLE DATA           6   COPY v1.migrations (id, migration, batch) FROM stdin;
    v1          postgres    false    217   1�       �          0    16411    mst_collection 
   TABLE DATA           )  COPY v1.mst_collection (id, nama_collection, judul, tipe, default_chart, referensi_data, catatan, route_name, table_name, created_by, updated_by, created_at, updated_at, ref_organisasi_id, organisasi, satuan, definisi, rumus_perhitungan, cara_memperoleh_data, urusan, is_multidimensi) FROM stdin;
    v1          postgres    false    219   �       �          0    16417    mst_dashboard 
   TABLE DATA           �   COPY v1.mst_dashboard (id, nama_dashboard, frame, link, catatan, is_hidden, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    221         �          0    16424    mst_jenis_dok 
   TABLE DATA           �   COPY v1.mst_jenis_dok (id, jenis_dokumen, ref_grup_dok_id, keterangan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    223   �      �          0    16430    password_reset_tokens 
   TABLE DATA           E   COPY v1.password_reset_tokens (email, token, created_at) FROM stdin;
    v1          postgres    false    225   �      �          0    16435    personal_access_tokens 
   TABLE DATA           �   COPY v1.personal_access_tokens (id, tokenable_type, tokenable_id, name, token, abilities, last_used_at, expires_at, created_at, updated_at) FROM stdin;
    v1          postgres    false    226   �      �          0    16441    ref_data_tag 
   TABLE DATA           l   COPY v1.ref_data_tag (id, data_tag, keterangan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    228          �          0    16447    ref_grup_dok 
   TABLE DATA           p   COPY v1.ref_grup_dok (id, grup_dokumen, keterangan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    230   �*      �          0    24617    ref_kode_wilayah 
   TABLE DATA             COPY v1.ref_kode_wilayah (id, kemendagri_provinsi_kode, kemendagri_kota_kode, kemendagri_provinsi_nama, kemendagri_kota_nama, bps_provinsi_kode, bps_kota_kode, bps_provinsi_nama, bps_kota_nama, latitude, longitude, kode_pos, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    253   ;+      �          0    16453    ref_organisasi 
   TABLE DATA           {   COPY v1.ref_organisasi (id, organisasi, singkatan, keterangan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    232   �.      �          0    16459 	   ref_topik 
   TABLE DATA           f   COPY v1.ref_topik (id, topik, keterangan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    234   �3      �          0    24601    trx_collection 
   TABLE DATA           �   COPY v1.trx_collection (id, mst_collection_id, tahun, target, realisasi, sumber, catatan, created_by, updated_by, created_at, updated_at, kemendagri_kota_kode, kemendagri_kota_nama) FROM stdin;
    v1          postgres    false    251   o6      �          0    16465    trx_data 
   TABLE DATA           -  COPY v1.trx_data (id, mst_jenis_dok_id, sumber_data, jenis_data, nama_data, deskripsi_data, format_data, media_type, size_data, ekstensi_data, nama_file, url_file_upload, url_upload, is_shared, webbappeda_visible, satudata_visible, catatan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    236   �      �          0    16474    trx_data_organisasi 
   TABLE DATA           �   COPY v1.trx_data_organisasi (id, trx_data_id, ref_organisasi_id, organisasi, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    238    �      �          0    16480    trx_data_tag 
   TABLE DATA           ~   COPY v1.trx_data_tag (id, trx_data_id, ref_data_tag_id, data_tag, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    240   ]�      �          0    16486    trx_data_topik 
   TABLE DATA           z   COPY v1.trx_data_topik (id, trx_data_id, ref_topik_id, topik, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    242   �      �          0    24645    trx_laju_pertumbuhan_pdrb 
   TABLE DATA           �   COPY v1.trx_laju_pertumbuhan_pdrb (id, mst_collection_id, tahun, kategori, urutan, target, realisasi, sumber, catatan, created_by, updated_by, created_at, updated_at, kemendagri_kota_kode, kemendagri_kota_nama) FROM stdin;
    v1          postgres    false    255   �      �          0    16492 $   trx_luas_wilayah_kecamatan_kelurahan 
   TABLE DATA           �   COPY v1.trx_luas_wilayah_kecamatan_kelurahan (id, mst_collection_id, tahun, kecamatan, jml_kelurahan, luas_wilayah, satuan, catatan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    244   �      �          0    16498    trx_riwayat_data 
   TABLE DATA           B  COPY v1.trx_riwayat_data (id, trx_data_id, mst_jenis_dok_id, sumber_data, jenis_data, nama_data, deskripsi_data, format_data, media_type, size_data, ekstensi_data, nama_file, url_file_upload, url_upload, is_shared, webbappeda_visible, satudata_visible, catatan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    246   �      �          0    16507    users 
   TABLE DATA           q   COPY v1.users (id, name, email, email_verified_at, password, remember_token, created_at, updated_at) FROM stdin;
    v1          postgres    false    248   �a      �           0    0    failed_jobs_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('v1.failed_jobs_id_seq', 1, false);
          v1          postgres    false    216            �           0    0    migrations_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('v1.migrations_id_seq', 28, true);
          v1          postgres    false    218            �           0    0    mst_collection_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('v1.mst_collection_id_seq', 396, true);
          v1          postgres    false    220            �           0    0    mst_dashboard_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('v1.mst_dashboard_id_seq', 3, true);
          v1          postgres    false    222            �           0    0    mst_jenis_dok_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('v1.mst_jenis_dok_id_seq', 3, true);
          v1          postgres    false    224            �           0    0    personal_access_tokens_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('v1.personal_access_tokens_id_seq', 1, false);
          v1          postgres    false    227            �           0    0    ref_data_tag_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('v1.ref_data_tag_id_seq', 144, true);
          v1          postgres    false    229            �           0    0    ref_grup_dok_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('v1.ref_grup_dok_id_seq', 2, true);
          v1          postgres    false    231            �           0    0    ref_kode_wilayah_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('v1.ref_kode_wilayah_id_seq', 28, true);
          v1          postgres    false    252                        0    0    ref_organisasi_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('v1.ref_organisasi_id_seq', 52, true);
          v1          postgres    false    233                       0    0    ref_topik_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('v1.ref_topik_id_seq', 34, true);
          v1          postgres    false    235                       0    0    trx_collection_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('v1.trx_collection_id_seq', 1664, true);
          v1          postgres    false    250                       0    0    trx_data_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('v1.trx_data_id_seq', 255, true);
          v1          postgres    false    237                       0    0    trx_data_organisasi_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('v1.trx_data_organisasi_id_seq', 88, true);
          v1          postgres    false    239                       0    0    trx_data_tag_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('v1.trx_data_tag_id_seq', 417, true);
          v1          postgres    false    241                       0    0    trx_data_topik_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('v1.trx_data_topik_id_seq', 29, true);
          v1          postgres    false    243                       0    0     trx_laju_pertumbuhan_pdrb_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('v1.trx_laju_pertumbuhan_pdrb_id_seq', 1, false);
          v1          postgres    false    254                       0    0 +   trx_luas_wilayah_kecamatan_kelurahan_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('v1.trx_luas_wilayah_kecamatan_kelurahan_id_seq', 6, true);
          v1          postgres    false    245            	           0    0    trx_riwayat_data_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('v1.trx_riwayat_data_id_seq', 265, true);
          v1          postgres    false    247            
           0    0    users_id_seq    SEQUENCE SET     7   SELECT pg_catalog.setval('v1.users_id_seq', 1, false);
          v1          postgres    false    249            �           2606    16531    failed_jobs failed_jobs_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY v1.failed_jobs
    ADD CONSTRAINT failed_jobs_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY v1.failed_jobs DROP CONSTRAINT failed_jobs_pkey;
       v1            postgres    false    215            �           2606    16533 #   failed_jobs failed_jobs_uuid_unique 
   CONSTRAINT     Z   ALTER TABLE ONLY v1.failed_jobs
    ADD CONSTRAINT failed_jobs_uuid_unique UNIQUE (uuid);
 I   ALTER TABLE ONLY v1.failed_jobs DROP CONSTRAINT failed_jobs_uuid_unique;
       v1            postgres    false    215            �           2606    16535    migrations migrations_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY v1.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY v1.migrations DROP CONSTRAINT migrations_pkey;
       v1            postgres    false    217            �           2606    16537 "   mst_collection mst_collection_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY v1.mst_collection
    ADD CONSTRAINT mst_collection_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY v1.mst_collection DROP CONSTRAINT mst_collection_pkey;
       v1            postgres    false    219            �           2606    16539     mst_dashboard mst_dashboard_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY v1.mst_dashboard
    ADD CONSTRAINT mst_dashboard_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY v1.mst_dashboard DROP CONSTRAINT mst_dashboard_pkey;
       v1            postgres    false    221            �           2606    16541     mst_jenis_dok mst_jenis_dok_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY v1.mst_jenis_dok
    ADD CONSTRAINT mst_jenis_dok_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY v1.mst_jenis_dok DROP CONSTRAINT mst_jenis_dok_pkey;
       v1            postgres    false    223            �           2606    16543 0   password_reset_tokens password_reset_tokens_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY v1.password_reset_tokens
    ADD CONSTRAINT password_reset_tokens_pkey PRIMARY KEY (email);
 V   ALTER TABLE ONLY v1.password_reset_tokens DROP CONSTRAINT password_reset_tokens_pkey;
       v1            postgres    false    225            �           2606    16545 2   personal_access_tokens personal_access_tokens_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY v1.personal_access_tokens
    ADD CONSTRAINT personal_access_tokens_pkey PRIMARY KEY (id);
 X   ALTER TABLE ONLY v1.personal_access_tokens DROP CONSTRAINT personal_access_tokens_pkey;
       v1            postgres    false    226            �           2606    16547 :   personal_access_tokens personal_access_tokens_token_unique 
   CONSTRAINT     r   ALTER TABLE ONLY v1.personal_access_tokens
    ADD CONSTRAINT personal_access_tokens_token_unique UNIQUE (token);
 `   ALTER TABLE ONLY v1.personal_access_tokens DROP CONSTRAINT personal_access_tokens_token_unique;
       v1            postgres    false    226            �           2606    16549    ref_data_tag ref_data_tag_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY v1.ref_data_tag
    ADD CONSTRAINT ref_data_tag_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY v1.ref_data_tag DROP CONSTRAINT ref_data_tag_pkey;
       v1            postgres    false    228                        2606    16551    ref_grup_dok ref_grup_dok_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY v1.ref_grup_dok
    ADD CONSTRAINT ref_grup_dok_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY v1.ref_grup_dok DROP CONSTRAINT ref_grup_dok_pkey;
       v1            postgres    false    230                       2606    24624 &   ref_kode_wilayah ref_kode_wilayah_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY v1.ref_kode_wilayah
    ADD CONSTRAINT ref_kode_wilayah_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY v1.ref_kode_wilayah DROP CONSTRAINT ref_kode_wilayah_pkey;
       v1            postgres    false    253                       2606    16553 "   ref_organisasi ref_organisasi_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY v1.ref_organisasi
    ADD CONSTRAINT ref_organisasi_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY v1.ref_organisasi DROP CONSTRAINT ref_organisasi_pkey;
       v1            postgres    false    232                       2606    16555    ref_topik ref_topik_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY v1.ref_topik
    ADD CONSTRAINT ref_topik_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY v1.ref_topik DROP CONSTRAINT ref_topik_pkey;
       v1            postgres    false    234                       2606    24608 "   trx_collection trx_collection_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY v1.trx_collection
    ADD CONSTRAINT trx_collection_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY v1.trx_collection DROP CONSTRAINT trx_collection_pkey;
       v1            postgres    false    251                       2606    16557 ,   trx_data_organisasi trx_data_organisasi_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY v1.trx_data_organisasi
    ADD CONSTRAINT trx_data_organisasi_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY v1.trx_data_organisasi DROP CONSTRAINT trx_data_organisasi_pkey;
       v1            postgres    false    238                       2606    16559    trx_data trx_data_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY v1.trx_data
    ADD CONSTRAINT trx_data_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY v1.trx_data DROP CONSTRAINT trx_data_pkey;
       v1            postgres    false    236            
           2606    16561    trx_data_tag trx_data_tag_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY v1.trx_data_tag
    ADD CONSTRAINT trx_data_tag_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY v1.trx_data_tag DROP CONSTRAINT trx_data_tag_pkey;
       v1            postgres    false    240                       2606    16563 "   trx_data_topik trx_data_topik_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY v1.trx_data_topik
    ADD CONSTRAINT trx_data_topik_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY v1.trx_data_topik DROP CONSTRAINT trx_data_topik_pkey;
       v1            postgres    false    242                       2606    24652 8   trx_laju_pertumbuhan_pdrb trx_laju_pertumbuhan_pdrb_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY v1.trx_laju_pertumbuhan_pdrb
    ADD CONSTRAINT trx_laju_pertumbuhan_pdrb_pkey PRIMARY KEY (id);
 ^   ALTER TABLE ONLY v1.trx_laju_pertumbuhan_pdrb DROP CONSTRAINT trx_laju_pertumbuhan_pdrb_pkey;
       v1            postgres    false    255                       2606    16565 N   trx_luas_wilayah_kecamatan_kelurahan trx_luas_wilayah_kecamatan_kelurahan_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY v1.trx_luas_wilayah_kecamatan_kelurahan
    ADD CONSTRAINT trx_luas_wilayah_kecamatan_kelurahan_pkey PRIMARY KEY (id);
 t   ALTER TABLE ONLY v1.trx_luas_wilayah_kecamatan_kelurahan DROP CONSTRAINT trx_luas_wilayah_kecamatan_kelurahan_pkey;
       v1            postgres    false    244                       2606    16567 &   trx_riwayat_data trx_riwayat_data_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY v1.trx_riwayat_data
    ADD CONSTRAINT trx_riwayat_data_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY v1.trx_riwayat_data DROP CONSTRAINT trx_riwayat_data_pkey;
       v1            postgres    false    246                       2606    16569    users users_email_unique 
   CONSTRAINT     P   ALTER TABLE ONLY v1.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);
 >   ALTER TABLE ONLY v1.users DROP CONSTRAINT users_email_unique;
       v1            postgres    false    248                       2606    16571    users users_pkey 
   CONSTRAINT     J   ALTER TABLE ONLY v1.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 6   ALTER TABLE ONLY v1.users DROP CONSTRAINT users_pkey;
       v1            postgres    false    248            �           1259    16572 8   personal_access_tokens_tokenable_type_tokenable_id_index    INDEX     �   CREATE INDEX personal_access_tokens_tokenable_type_tokenable_id_index ON v1.personal_access_tokens USING btree (tokenable_type, tokenable_id);
 H   DROP INDEX v1.personal_access_tokens_tokenable_type_tokenable_id_index;
       v1            postgres    false    226    226                       2606    16623 7   mst_collection mst_collection_ref_organisasi_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.mst_collection
    ADD CONSTRAINT mst_collection_ref_organisasi_id_foreign FOREIGN KEY (ref_organisasi_id) REFERENCES v1.ref_organisasi(id);
 ]   ALTER TABLE ONLY v1.mst_collection DROP CONSTRAINT mst_collection_ref_organisasi_id_foreign;
       v1          postgres    false    232    219    3330                       2606    16573 3   mst_jenis_dok mst_jenis_dok_ref_grup_dok_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.mst_jenis_dok
    ADD CONSTRAINT mst_jenis_dok_ref_grup_dok_id_foreign FOREIGN KEY (ref_grup_dok_id) REFERENCES v1.ref_grup_dok(id);
 Y   ALTER TABLE ONLY v1.mst_jenis_dok DROP CONSTRAINT mst_jenis_dok_ref_grup_dok_id_foreign;
       v1          postgres    false    3328    223    230            $           2606    24609 7   trx_collection trx_collection_mst_collection_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_collection
    ADD CONSTRAINT trx_collection_mst_collection_id_foreign FOREIGN KEY (mst_collection_id) REFERENCES v1.mst_collection(id);
 ]   ALTER TABLE ONLY v1.trx_collection DROP CONSTRAINT trx_collection_mst_collection_id_foreign;
       v1          postgres    false    3313    251    219                       2606    16578 *   trx_data trx_data_mst_jenis_dok_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_data
    ADD CONSTRAINT trx_data_mst_jenis_dok_id_foreign FOREIGN KEY (mst_jenis_dok_id) REFERENCES v1.mst_jenis_dok(id);
 P   ALTER TABLE ONLY v1.trx_data DROP CONSTRAINT trx_data_mst_jenis_dok_id_foreign;
       v1          postgres    false    223    236    3317                       2606    16583 A   trx_data_organisasi trx_data_organisasi_ref_organisasi_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_data_organisasi
    ADD CONSTRAINT trx_data_organisasi_ref_organisasi_id_foreign FOREIGN KEY (ref_organisasi_id) REFERENCES v1.ref_organisasi(id);
 g   ALTER TABLE ONLY v1.trx_data_organisasi DROP CONSTRAINT trx_data_organisasi_ref_organisasi_id_foreign;
       v1          postgres    false    3330    232    238                       2606    16588 1   trx_data_tag trx_data_tag_ref_data_tag_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_data_tag
    ADD CONSTRAINT trx_data_tag_ref_data_tag_id_foreign FOREIGN KEY (ref_data_tag_id) REFERENCES v1.ref_data_tag(id);
 W   ALTER TABLE ONLY v1.trx_data_tag DROP CONSTRAINT trx_data_tag_ref_data_tag_id_foreign;
       v1          postgres    false    228    240    3326                        2606    16593 2   trx_data_topik trx_data_topik_ref_topik_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_data_topik
    ADD CONSTRAINT trx_data_topik_ref_topik_id_foreign FOREIGN KEY (ref_topik_id) REFERENCES v1.ref_topik(id);
 X   ALTER TABLE ONLY v1.trx_data_topik DROP CONSTRAINT trx_data_topik_ref_topik_id_foreign;
       v1          postgres    false    234    242    3332            %           2606    24653 M   trx_laju_pertumbuhan_pdrb trx_laju_pertumbuhan_pdrb_mst_collection_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_laju_pertumbuhan_pdrb
    ADD CONSTRAINT trx_laju_pertumbuhan_pdrb_mst_collection_id_foreign FOREIGN KEY (mst_collection_id) REFERENCES v1.mst_collection(id);
 s   ALTER TABLE ONLY v1.trx_laju_pertumbuhan_pdrb DROP CONSTRAINT trx_laju_pertumbuhan_pdrb_mst_collection_id_foreign;
       v1          postgres    false    3313    255    219            !           2606    16598 c   trx_luas_wilayah_kecamatan_kelurahan trx_luas_wilayah_kecamatan_kelurahan_mst_collection_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_luas_wilayah_kecamatan_kelurahan
    ADD CONSTRAINT trx_luas_wilayah_kecamatan_kelurahan_mst_collection_id_foreign FOREIGN KEY (mst_collection_id) REFERENCES v1.mst_collection(id) ON UPDATE CASCADE ON DELETE CASCADE;
 �   ALTER TABLE ONLY v1.trx_luas_wilayah_kecamatan_kelurahan DROP CONSTRAINT trx_luas_wilayah_kecamatan_kelurahan_mst_collection_id_foreign;
       v1          postgres    false    244    3313    219            "           2606    16603 :   trx_riwayat_data trx_riwayat_data_mst_jenis_dok_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_riwayat_data
    ADD CONSTRAINT trx_riwayat_data_mst_jenis_dok_id_foreign FOREIGN KEY (mst_jenis_dok_id) REFERENCES v1.mst_jenis_dok(id);
 `   ALTER TABLE ONLY v1.trx_riwayat_data DROP CONSTRAINT trx_riwayat_data_mst_jenis_dok_id_foreign;
       v1          postgres    false    246    3317    223            #           2606    16608 5   trx_riwayat_data trx_riwayat_data_trx_data_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_riwayat_data
    ADD CONSTRAINT trx_riwayat_data_trx_data_id_foreign FOREIGN KEY (trx_data_id) REFERENCES v1.trx_data(id) ON UPDATE CASCADE ON DELETE CASCADE;
 [   ALTER TABLE ONLY v1.trx_riwayat_data DROP CONSTRAINT trx_riwayat_data_trx_data_id_foreign;
       v1          postgres    false    246    3334    236            �      x������ � �      �   �  x�e�ݎ� F��ì�����J��2M�
�:��k�I2UU��l8�
9H\j���!8��^�Q'ӏ��G
�ib|����K:�w7���b�y��o!^���_s��r�s���C�̓�㹐"���۬���fw�װ<���D�;��1P{K�����|�A������4?}ƺ�
u��p5t���X��x��ϰ5��P׌B�b-qCS�^��p�J�旱u��%��ox�dO�<a�13k⭟M��=�/C
j�tl��l�n[������?�V\0�2n��P��0�����J2���@U�0.&���M��`Td��q	�f�J1ܒ���V�������&-�����ǥ��4dv���������5�Q��j�j���eL��m�?�-CU� ���cX`�ԝh��9�6�������ٺM�?wK�^�V�(��ג��<����O�
������ll      �      x��}�n�V���|�
0G��Sh4�j��e�lA*Oc���,��\2iO�������Kn,g�!s����U�����8�g�����:}����Y5�������~�K����	�����g��p0ŃE<�D���px=�a���6ߦ5~���<��VD+��]�n��9��gU���nS�X��_��Fw�>���2��*۔�m�Gi��6���XaQ@�$E���RQ`_m4���*-��a��'��� ��o�Y�J��n��_��c���٦�@�l�O�����
�p�C���0r��]]����[������;(��v7fq2����` �aAF_�K��t���r�{�U�+��X�xT(� hhx�q4�_���,;ou=�u���bn{�sZdu�_��9�ǩ�~K��ic��@ӣO)|,z̞�z_��X��W�E^�7�o��h� <�xB�	�k�L2�0;��qr=�a90wY����YT�~��)S����a�NaKђ�1��mt�P�������M��;�8�5p���%..�T!D� 4B��4�c8I�A�Lwq����nU�如N���]��{<�ړ�Ϸ��)��\�E=�/ѧ
v��m�>rGɆB+��Y��!����>.���S�g����7�v䳇V�������=�n�u����R��.o�ͮo���6	��O8���h�%�9 �c��_p��-=]���.>6 �M��]y��a�`vn�*�Ϸ͆�ںܬ�U�)�'ޓ+�)N�*� '����7D.����"@.���L�5d��pv�L��d���Ȫ�xa�������J��<����3~�y����iY���8���SV�� �F�v������5�,T�^���u�����aޘ�eP�{�Ak�7�����_��֖<8�W�r����5?�+�"�W��Jפ��3��L��7���P�iwn���w ���>�p(˅���a!�T������ ��������=�[�	"�H�r&�(8D$��aۏ��������?����c�8���3���skX��Y*�8C��9��z-��[c[� l���c `3��דa6���o�~�WMٽ2���E��o*8 /o��EO�~��Ȏ�u�*��,��E66�*_�Eǽd���M���!��Mi��m�x��������v�(L��Ƥ��ȷ$�$��Ą���յl��ɶW(z+c+�h��������)��6AB��yaI�<��ژOt������/'�����Z��/����HB!	�$�K._��Aֽ��b�p��7�'8#���m��Y7u����hS����z��>.}�F����������DB	"@$,�	}�\��$	}d�/��lAd�MДDW��/��/�ꝺ�A��A0.��}�� ��oQ��6@V�ߥ��=n;�9���s�>_ƻr�SW�� ��ꃀ�O�g� ���mK����ҭXL�Zi�ao��g�Z'�E��������<����O·�g��+�r��1�NA�*7��n$��1��&��;�@s�m����ڟҗ�����u�Zp��c\�x}���^��]��9B��`q=i�{�a��:��q�t�a������։Z��z8
��F�yb�7y�H�xW�,���Û}�D���p�+��~!��%{%C�X�'chrW�}\2{�%{@߇Dn�SI��SH�y
�SH�B�ĳ[C�$�_C���~��9�7M�o����򆨳%H�Y`T�da��HK%���i���J4�{L�3m��ao{���wr�W	�Z*���X�AaI������B�t����:�ؐ(���a�K��<��dhl	�$�ąE�'��P�z�h�F���B;��ù�+H����=p�������"�A��F�Q�h�Ơ�5�Z|Rf֖�CmLB*e���PD�[�hF��U�_���^ �=����
������k�..\~Q����Y]��QO����I�z��||I]�{k�~��4�k2��-��[O�q�q2�9�)C$�p�����c���>l��}K�����\��=@�P�s|�cG��4�^|����h��a��yZ���:�~���Sn����=|����X�������yws{��{o����檾W���J�8��1 �Wi|��_�qF�g�����
�NQ\+��`���
ŵ�qq���$���|�nM2��b��j���tz��6jN�W�>����q�˲�c&$WƆP�eKl��b�#���`w��e��d��F^��ؠ��xe�Ϸ-hx�ߥ�]�_=�^�rrk`H�[����R�n���m��G���o(s�]�;��p�6��uꗊG\3XC+�%F.�&^0�� .R�bR!I�"H���P0a���ǭ�c�A�Ƕy/1�s��69<�6��p����\��w�h!K:�2��%�p��$���A��0 ���h��kM.>60��$E�-�y!�(kN�{��+�'F�׵�6����溺n@���c�L�_r�hA2P�L�웗�W�v��z��l�tD��Iu0�t���~XR��:�@��X�T:��;꠶�Cǖ��~�R�鶻N����N������P=���&d�i���l{�J�kk�g.J�<.s=�m�����Ȅ��j��(4G=Ȋ�m֧K�8
��c=��fF;�uk>ei
�f�l^�$�C��v��h磯��{8ev��G�IA�4��n�k�7�v�:���?��Y���ei�}���Y�e�K�<��	b����Kn�iG\��e���:�;��z4�@��j?���Z��� �B�w#P��`{܃T�,�@�_R-���V�{�Ƣ��cO/m"���0��;k4���#�H�r�aǔ}�_��M�Ͽ�9e�ڌw�B߳l<��~fMl�U���f�+�
�\���y�L��t�R�7����*/�u���Y�����ϻm��j��;��e��^f~N�xC���l���VM<iT���Fv;�i��?s.�Oez����WO��Q����$��n�2���{���_F���
�eN���\�
�Dk� J�S�e��u4������j4w.�O|�F?6��I�SS��/D$-�5��gL���ʘ=αE|����/����b�Dl8o=.��R���_�7��P?%�!9���ǋ �-��K�����!ݾl_iR�_���eO�zUz�c���^M��+�ާtS���k�Âi�Qi���.hǴ����0��L���Qv��L&�'����Ԕ~ы-O���(6;ɟ{zc�"��	Ά�iZ���I-�U�}g���<;ϟfx�\�����u���)���_	��7����32��ն�z��f|Y13imX23�`�m}�8�0�nv8m�;۟g6S
��"���H���0�����?�s�b�{b�%vn�F���ʆ9i��{6g!qa�%nK�d�z�BjA�hr!������zPDLoi�}�`��� tw�� I�J�'��ۼ�t�7+5rf��;�owdʡQ2�	��s�7Qb�:�� vB���,v�Z�(	�^m���.���-���[����T=��(X�@�D�
�|�h��F���K#��Q�I[Qd�1��O��F|v��h�����j�z����y<�`G���~]�Ă���^���g�'~����LJ�HD�����f�0&%gĤGe����ڦ�Ѓ�Hw�5ڥQEg���ٖ:��hQz����M�1Z�݊5>�&A��p��/�/���SC/&rn����7��_+l�T�6q|�g�"���s�������.���{S1����}^���W�us��-4�ڒ;�5����`pT��;�_mX8*�;�"0Q�cV�ȇ�F�bK.��F��e�CU�g@�F/u��p�_l�O6�[_rScf�Yo��,�f!45x�E�qa�2vи _�[w�: ʶ�4�hkl|~E�<��#��B������ ��I�s}�o����K�.�d0P_�J/��S���%Q����}�������$�I�C�I:�gm�}��N�?-y�1 ��V���2+���Z�    [����CQ�߱[�d��#VG	/�o��sA�{��\ɭ�=�lyZ�'.�c�+߲^�p��8�A���O��i�M 7������E�D�C{���������O�Ut8�Nz�ꈟ�1��+iZ7���@�c��T��iKuM)2���Lix�yV Sr%����*�����R�hh�F���$��P���Wۼ`b{�~2#�zq�Ql{�m4uLAI�Ң��A$�&���׃ivn���1�+ǍT�rE�^E�&Vo"W�ҳޔ/ǳ�nTr��v-7[����d�2%H�g���y/�j�*n����3_�P���羭�o���"p�osz�J�˿��8B��԰����_17^�3|A�Kõ[��;<�K���҈�:��lr�]�4p.$a��ϡ�$����)�Ӄ���G$\.���ч����$-W�?��%���YxN�I<��:�:�+�f4@��3�E�#�tK���c�uXTE�A��K~#)X-jo�]���n7A���z� �hO���D!��'�d��EX�t<�$|aL�����q@��#;����]8̻0a�E8������{52�]X��EGt�۶s8]lh�٬��͊�f�(?�a�=��[�?^F�����~�O�u��O+~��/+`EF����A���џ��q�׃yv~���(�l��AHۃ
�C��y�3���ȑR�^*����-�yR�Ԭ5�@\!q��jƾ��a'�p3O�Cg��7��v���$^��Lc�<U�C�h�_�}��H����ȶ&�T�m�~���
a��?�7My����o�.���[��rU�_��љ��x��rg�nr:�u21����8q�&#/\X�B��Ƣu�Q_�Vq�I@����q�`�ߍ'�N��^gO2�<�Y�������rZ{G��&��a�4��N0�K'�~��~���$�E�m:��'@;�t�gJ��ut*��>ԍ��b�Ŵl���3P�b)��KnT7J�T,ciۣ$�%����QZ��#�O�����Г0��ȉ�=�"���-
��8Y�#3�HL��Դ��u�

�DF�ͪՕP\d�&���HA��"$avY.�����~!�uT�5p��!�AC����^_��|j��8<�g�WL���M|�/
���)���?����g��	�(�:a�3���v�`���L=���u�T��Ƨ#�Z����Al:>�8j���B�����`�#�{��;�!ûȱ��m<�-�9��F-*+9�ܴ�N��Za:�	�5���t�����N������oY����./�,~6(�/b&,�9���a���K����`p���O�2�)#�H 5	�,1D�o]9,d~s!ᛓ����'VAo�W�G�'������%�k�~.~۶��^���������˖ǋ��0mG�������bU�������ƹ-����˭���o�;�?�74������C�V���z�j��-�yA)p�s�dK<��|�7��N&�P"���M��%�k-�=n'3��x��;X�S����0t���~)���{<�1����a�X���*(�4����E؉�>� ϿD�G��8��̌=X=��N��&f�r�v�fC�	>�p��(���i;C�L���@�0�!�@��u~0u&��k/��;��#���)z��'OZ�}�B?ɍ|���x��hr=�3���_�y�j4%���*����+�1?j��{ۅ����;�z4u����:�0��2�Q6f����NðWZ�����EVm���dI{�gss�I��(Z�5!i����"Ў���N4�I�=�9v"g����m8H�Wqk 6H7@��.n���b�2n�8���8;߲3R����.���'k��ը	��9��%	��2�ɴ��y�O {e2�DC�rH4᠉3�R'(�c�5����� XΠ��Ƴ�4Y���p�c�;曩	ceD{h�"������G����&�ؾi?��R�lG��*%5��΢S(l0��R�	��%������c�{����-?��[~����u[2��vI}|D�,� Ձ�TB�rL�<�y��.��2�|#�M�~����k�RmA���"J�B\��h�@%����D*4)�L*4)A�>$		7S'o�{������G��/�{�?,t�(�4⒗�C/��{�7�°'3���l��e{��?T�s �D�/U�\bYfK��W���칓	͂�0+�5<Q�z���O[Ĭ�V���ɊQ�CW�kȧ(���sہņ%�ʦcԐS�^b;���ĦS7�S��L��O��7�+3�K��ݓ�4�W���U[k�nK�٫2���&�pHU�����e�ao14�����cu����K�΍n��.�[hq!39Z0Qt�a�SΚQ���/͒3���.1r�M����K�Q�Ž��k����!�a��$I:�/h#�,�$ ����\�~�;��������W4�����;���H �h$��EX�t۷���̂����}XL��J��t'�qS�|r��҅?���b�7��T���b���f[��$3���'ٚ��
A�T`=r�1*J{fN=f��^���*#�x��!>�xγ��ż�3%�.��.N����e�Ȃ�!��c��]|c�NYM�ۣ6�Ut���A�U����1H-��V�h�G2��8�=���VŎ^��$HV4��2p��82�.��k���[�C
����B�{�na������X|4sd�l�*X&D��u^�u|NS�%7�NSY��G(�&��Q�C&�Iչ7$�0�=&7b˂�If,�_JXD���;Φq�5*h_e��HP�C�xYX�u�F��+BYׇF������
�W�Bb�H;ö�İ�T�k���b*ʑ(+_��.hF�U �Fo�tO�YPa�c͎J�Fr�<���b�4BӜ`�4c0r�X[�W�-lT��qކ�j���!IdyX Q�����!���T���w���=ؑ&���j⮁��*���^N����<n�5���>q2�Y��N��B�
XX�Dʯi#S���f�A�H�		@���d�G��`�X��������.�g�N��f��Ue#x��K0����֬�'[[�`�b�جɶ�X� K �5�}}5���S;A]7�ݬ=���)�,C8�*��������B�0�J2AB�'/Z�3���q_�bO��Aװ$h4�������;��-taU��z=����؍!��ި�Z���T�� iE��8���u���쉥�c3�gڰWy���Ì9�M$%�}���DqD�+�]9!�X2�j�8�v:^9���
��$C����NP=�$���'���"��W��UD6/�o���iIӜBE��=���t}�4��F�`obkZ/��#��j�7t\ʢ��~�Wq���3����KY�Y����B���K���f~�h휁;�c�ϽA:�/K�%�igu0��`�]��J�N	��'4^��M�eE�N��p`ۚ���N����1��\j6^h�Ĥ0\���xN�8k񟏹'�fk$
G�4�6Fp
z�e,��~aN�F9���ƎD ����r�}������N�6u0���I��rU���d�d���C�h:�v >��	$r�v�{�����i+vZ&���K�SH"�{M���\�C'�V���c;�7xL�k�J�8Z�R��Y]���9�7�ԂSN��a�0�����@�������T$��(k����n�N�f�gmg�d��c?f��!cy�+����t]�f�Ԙֳ��Y���W$%�������Ϟ!�W7��K�LoQz���V�bpo��ʡkgP�T��V�t��)�G���Nv���d �Ѡ�M鼑j��j�^YrT2k����.��Bw]Nb��E5�!��M�H���A)��R��{��Ҫ�X+�3n�y�^)?d��k�R#��B��^w�X��J�?a޸W�e���Z����"��B�rn�|��7��<(�k,הo�5z��W}�W~���ۯ���9�:���S�~�k)�c�*������X,�CE7�G����BKQ_�\x_��� ��x!�N�     wZ�#I}X����Vt��g4�±��	�1#X#BC��:^'� ��0���ղ�fE*�$Ee�@�(T�F*Gw:�PEE��>3���q��Nw;��>��E؝�*#��R����񯲒CO�F;�;ݻה���H|n�C�c�)<*g$	���I�`İ�v��RF��C�R��^��/��R�U
��CR��,I)C�FF�P������>ed�ߣ�Μ<n�$U�5��ܞ�M��h<P����sn�%�3�3������sJ>B�9/'��smq�`'��|�Z�4��^�Z��Z��KϢ��շ�1�E?ws���>j�&9;�+Q��V6v��J��}4���� �����ċ��j %:ߗ��gG��\"�9Vө>;��l�����V���dP��P��Q��ڭ��π�`�j��s���W�X
�O,�[�t���M�k�\�d��a�kUl��|�ם��1��_b�ao!*�f��Oڧ�Jv�U/~Yg/v��@���;�*�H69f�a_��W�/��K�N2�,%�',ӫ_�p4Ґa�v�B
� �)�u� ۗ�Գ�0hݦF�ag�A:��ev���Q9�fl0��m�ga~�9kG�;����8��A�4zJ��¸�1Q[�ZT�4ג¼)��^2EӐ���K"�]��-��%̃�$yyxUR*4ʒ˥cח$�1I_���21�0��R�E�S��IX �&~B��o�}NW�Z�9�qܬ��|�,O,r���^�1h|x��{�ʉ�Y�c
�y�k?��1윘\�%��֛�.��A�l��!VL��W����<H�Ϳ@>/1Cx�b_V�n�{fc�?U��lI^�;�;Quy��w�����0�oà`7u�5����n���s1n�P���VӅ�M���2�wn�>d���3�b�y;�:I'�ka�\Q�kzۢ]��'���O{������������{�t�z�I�W�W7E��6�կ��7<\��R*���O���	��`��������0�[�1�c�R��B�{OeS��R�v�t_�mM`�M���sF[�3�`�a�[%p�zW��:���\��h+���?IT�?�:��%f.1q��K������̅t<�T0� R��B��I�v��`�@.<�T��}�ϕ`"吟R���/�,w|%롗����1rjLX��䊅���L��K�-e����O�D�V���m�=��{�7M��%(�W��ߟd��1��0~�D�?���@M^-���"��=�����&^��������#Tt�br�hY��5�����܈u��H��9��'��!:����*(�	��(	E0&鐤����2�����ټP�	��q �'�Mp*���eRR�;SϵCÎ�أ�WZ�`��?�;ڂ��)�I��t�:��ͩ�d�*�y����ˌT5�+0{s��W����Tֆ6�D�X���b�T'��e�h�H��4�h���q�h�� ���fx
��I�i�j��	��`o2��J��-�OUJ��5X�zZz^RG&� �m|]=�S�9;�wIQ�2g����&svc��2��̂��"7�uQ���-A���Vjc�
~*a�JeO�}������s��:��y�$I��]��I�-)���X��c�J1+I[� <Q,�	��d$}R�	��`���\#�;�B���]������$	�N/o4��{���q΅9�<x������~2�P�y>�ud�=ts��� 4�S�V^��������u��ߥ��!g���V�Ƀ�'A/.C��Pb�Z����^��j��U���#.��o��dz�5{��P���\O�W�^b0{�{ձ���(�^�2%�Q7��=�j�G{���h�g}M9Ch�oJ�<�
�y��T�ƜPJ���E��H>��3�c+��~���B�
��{G�Z1�a;�d�:o�6l��e��>7v��L����Y��T�?�LPB�f{:Ko޷Rۖ��8-�+��A�@=ݘk�އMu7�ySg{u��3�-�j�~8ØYܮz�/�u�b�W�c�ٷ���M��+�����Րz�ᘕ^��P�:G���XJ�C\%쭗����1���5#�"�J�u���:g�ʔ,����%�i��X�=�1�ө(&��|]�]��u�s�h����w�DviY����#���#�.�6�cZc��^I����{��;�"Ց�6�X�:����鸱~������,�]G���:�u�6��te���â��;�g$,d���G�\�_>��ðp��J�M��+�]�_���h@���7>caZ�f��r���ͼe���oY�X�֮�VN�ћ�L�0e�c�P{^����諁�`�^=�n��B�H�1��I�J�\�E��:;%|�	�&գ�SI�O1�l���۰�X"s�?��}�HH_�Eng����w��([����̪��Z#�W�[H1������?��3�WE�!���#����\c�\)|�(�Vα#Ӧ�0�k�`��d�<��~�XL:z��D��8v�>��U�g#(��dk�Һ\�-���P���e�պ܄-^�Q_�s�[�7�I@>ס���da7�4R�*܁��1��T�C7ǃ �U��?�_J�Bi�+��\�6�������3�G�E�Ga�(ԏ=]�^'� �]B�L%�վ��u}I7y �[���X %fd^�*ݬ��Z(�1zz8wR&Y�,�	�V����l��L����F��I���N���D�1����I��J�~���
��v�|�)�b�����:D�ʂg	�'����UQ㎙?�7��L�5��{�����I����Y�p�P���,���M�P֝�V�R��G#�N��a�cV��'�P���T3'�A����Pׯ�}���`�<a'�מ��`3"�6vT:f~��B��z��]sFFJ�oH�1�o&�ă�4��CC@yC��]��i���۩cB�`'�vz��l�9����p?%bO'gN�;������{�=-���:��Z)xC�UȽYyM��-̂���}݆7jp+��N�%vځ�805�<]f����v���	RԱ�><M�D(�.g���F~v�d����M�#)rq�R�v���sR��Cxi�Y�����Ի5l<4v�ٻ{��\p�[3�����F'�8Y�}
�m��5��K��Q�DnM=�ҙtz��=(�Ev�R�$�5���<���/���s �й^L�o��xN8\�N��8=��yw����?h�N��z{�;�����9�����/;��3���ۿI��4kcw�(^�C׉5]���[�βuX�?
�cO��a�O�d*�(�����OT�G�H��#^��,��ܩ:;
�X�~|�T�[���b�	j[����,�*��b�O���%���]o���e.����o�-i&�T i�"Y8uQ,�I�|J	�"*F�U?f�ݦ����َ|�zo3��T����&.�4T�*)#
D��$f|�wÎ7���/��>�5Ŋ*Ǽ����w4媥6�U���tb�c�=�Θݚҙ���]�nBeĜ������8O��M�[s�5��p�%�z#D����9��:W�XU޹��ޏ��"��#����S���J�u��^������*W�bgǕ;C])�����Άp�H�C�����nʔvկgF��j؅�䯡�5bB�d
�Dv��8��ܢ	t��+��ֽ�JM����*Qn(�DS����E%�������p�^���Y���@���s�����V���&:�{�>Q� ����#,:#_�$�R,�B�[�ye�ԣE,9�;�kܑ�M��3�$�'�v���j��H��ԕW�}�9�	YW��)_��r^昛�W�-=D+�$��>�+��|��Q�	M�3(s�Ӄ%�uA���C��nvГ�*�[�s�4u�MYx"�k��.c�~I�P���/�G���u�}q�w8!�t�~<<�=����hv�в,j���T��zD�uw_�|i;6,l���Ѵ�.���X��!�_IE��0`s��R�&��CLA؉�h2���П�h I�Fp:"�1��b;,bɢӹU�	�NH��[9!�    ��,a�%IM�3SM�3 �gX+���'"�o�en�Dv|��d8�x�@��D���㙋��7�ǸJ[KKM�%b�(OSXʠ���;���o�e���Å��Qn-���b����/��{�?�Jؑ�w40����rJV��NWR��h�~�a�Q�A8�xw�I�~K�v0���B�J�?;�T �ҁ�!��\⸋ڰ�1�,���&s�Ȕ<�l�)��&�����hx�1�-[G��DG��C�+m�8�F��u)�����C��İ��/5����	�ɯ�/�I���)����XW�������7��/L8���i�1�L03Xd�cf=�9tR|Z�3Χ������ۖMV-���:v��
e��]��[
2I�X蔃LΪ��ڮrv֎�K'*���D&�iKEv�6ZV����S�������E���L���..I2�KG.�B>�(������K�B��s{2�U�����$�O��GTT4*L��F���E3�4���}�s�3CY��0�}+Z�����u�v�"k�.\�oE��p9��麺�m�IGN� ��ed4�h٥��x&�;�Y�ØG�����c�`�z����ʷ�2<�Kl��C��$�b��(:��_�ŊN�Jg��2'�D
Y(��0n�v{�Q2v��d)Y�C;���R�n�a�>ڒ�<�b�>����[&H$l"a���[$φ�����S�W�}��]����F�D��]���L�B�G��7%�δi%�w�(S��P'�% �I��P9�no�/#%T�\[��K^qm�kJ	�+��d�@�=^
���8��l�+��تj������(�cʌ(|����<�V����:�����]��aos͌ݸQi�+(7.�MF%�Z>Ts���];�h��Ć�*�*��א��iJa(U%VI�;bI�+��TF �%��	�T�7>`bkHi���	l�0�/�0T�-�	���g�=y�:��73Wk3�3�����܆�������R�a�Z��y�3��ˎ��Bf���s`��3RC&���a�֨g�`g�
R�4�����gM�����YF�b�Yc�mQݖ����&�_��-<}X�O6@At��ZךBւ�S؇u�L��"G�����(F`�IQ5i���h�Rt4 A ]뢭e�6�Mq�^��9�7���GE2R�*`+g�|L����_�ڔ�����1�W��d�㫊���"WT�
���W�&&��e���R�p��c�;zf������U*m��z��V:��8$��v?S�.��0	���������.J�=(
��Eq��C�[ڂ�I[i�aW��t���%bx
ԯ��6�9�<؛�͑6���=�6D�ȜNv��~z�m�kǩ��E+��~��+©���g�&~�{���L��\ܭ�:FRc+�Y�\jŇ�W���qk�`o�g;�z�A��4�c�)��:��6fq��9~$�1�JOW�Λ�3�^�%���_*�?9���oY�'#y���<��{���4u�O9�@S�N{�D���_��.����+�}yE�ބ� �phԓ�q�u`Ǐ�P����_�Oɱ���>q�����r/�^�ug��>�7�j�}�8Ù;~�3�nW��n~�"/$b�*Q��J@�hO%�e���'َs��e���(>�e[���%)��Ls�I.��_��*G��|�%��u1Z���8A�e���%�d�y~ݬemJ�������2Ee�wx�lֲl%3Q�_a31ec��A�;�l��Ѧ�+�`�=@��RX��)q�����?�n>ܶ�ά:�/�oq��j�^"���{��G��ϰ�ʇ�P���-����W:���K���~�PUw�=�	[6�%	^u5'3���Ol�a�4�Ie%�U8�����:*�`��«��W��w�M����K.�p�,�TC��T~���$L}�4_��{�z�B�`o�z2�����d�(5�'u|�'['�ɳ[X�)���A��M��X�~��	3��>a��f>�IR�<��K2���R�G ��)��5y�D��d��Uw�R��1�k��DI��o&�)���4�hbSOF����(�;��=!�`#�MϘ��,p�����}�K�5������:t��4.` �eBҀ��+�	��]Ã%�y�h���/x�?��=�`C�>�Ӈ{�B�=���F���(W�%�q*I�#fq������Ǚ&�`"Qvgb�bK�Yv��ݳ<���g��n� '!�T�N�*0S	�nA^����W�0"������
��/���GE�ԈDN6B��
�D��b�3S��ق��,ط���x֎#���>�&��6ge�����oa��_2Պ�f�M����w�����������ƪJlM��}˩�Y��2���.d#���ĎMſox0�� ��u ;!�<�ă�ȣ�ao���h1�W�p\�BE�b��x
S����lSמ��&���U�$/��?�2�����sY��\��%[~�"�1�5�f���x	���q�KH^���L�x!�W�i�m�$�y%%B������6���G'�O��G��O);�B������>��0����ǻ����Ā/a2�E��� 	@�t�l0o�U%�D�A��#9���on�i����q��������W�7m; ��5W,(1����E\nV[�#�D�,U	E$�@"Qv��*�|<�3�p�sM��*]�A9��:����8��D��c�L�4�-��*�e�/8 �
 �<+�b�,�d��V�
Wh���rH�LɰD�z�Tu^*�;�<�8��!�#%�x��}��a��S�K���B��e���̜������$*-P��T�:Yst��S�U�V�hDw�T�c�.�N+܁�c&�`g�ù��~ۭ�Mq��Hb��ѧ�>�7&���2�Y���T2�`l��[db_����Dv���p8��C��j��%ăȋr��m��[3���%B�(9�����-I%�ʒ�
�g+H�<G���o�c�Yy8�;&���{v�����#3��R��!ԫM��cM[Ԟ� I�&�DX$ݣ1\}� �����1!�vS+��]�K�L�y�vo��L/
Wܞ~�r�+ع�C��C�����/�]��X�g�e�^F�/�>g��B���E�,b�EL,Ȗ$9�Qd��	�Lَ$Y��l��*�|��{�T��h����[�	���HSKV�Ěĉ�4�`%��x]}^PB�av�HMG�p4q�F{��:_�-g�4�%�i��fwOԬ�p3_��@n7�TY��N�Y:P>���a��[$��=W��J��n��3�3Y���0�A|e*��,b��omb:qej���t¢�Lu���W���+����Z�����΁��3��)���2X8D�_��b�#�c��DBs�E0�\r���gq��{�q��:fjL��*4��)���f(/St ���c��9��1�Փ*��C������J�~w��;�^ҥ���&p�ag�CË�h��>z�4�,Hȗݿ��\��ߪ@��v�l�%��8����e?#���]"V��B#D��Hſ�U"L�+�֙$�3�N�G��*Î�z�׎c�í>h�I�|B�ͳ�&[���5��ƪ�6/�"�!P�pH�O�����m�g�jJ�-��1�"6�h=�r#<��`[�b$�[�eY��}K�o�^j�[Gyw�m������h|�I*m�R�����I�+=>��f�_��I�J����ۖ�7|c�7���U�UM���5�2U4t�*�b&$3a1��~���*e�n�Whk�.iؑ��§�sv�_>yK���4R�r )4���穔���àk/ ;��J3�瞁N�N8�{u���r��?�RD�"���e�/�6ejp��>�4��%��b�&��=f�f&Ez!�E�&N�>��?/�;�dψ:�f��A%t����S"[c�J��(��{��&a��y�pś�����͞J��WK��s0P�G@Zo��K�:�K,����j]�Jr�\1Br��ĩ5c�N��>{%�ȩ:y���� �  A�B�;��f4�<?bL-�-5�x���c��Q�x����	��a��2���vJm�J�T��=�:��)��*iSEq?Q�S*[U�f̎���������^�~H�f*t@c#�mٝ��xN��c,1�!��1�Gx��uk��uĄ�E�|ygB�0{��̽���N��+L;�Z�:�K�zǕ��3ir(���V�̔�5�u�.�k��̾]>�U�u���˳ D����w�4�pd9̨Z�9й2�o|�Ќ���aב̳\
��x��V�]G�S1��BW&>�]00`�sV%g���{��GÅv]�o����3u�|!��mn[nm-�c�8�.Ξ��m���P�����}������e9�e�_�ʿ)=~C_����<k ���0�p4�������m�Za�ߍ���z(� p��)�����횕f\�Ѿ����
��k��V�<X��[{���Or]~(ۻ��=2� ��|:$�Z����o��q��#���/��]S�����gD�"�N^췬���j��3����Z������j�)[�Rh=�)�FT��񢧕:%7�:/p�lʌ��������<;��s���	�LD���[Ui./���������92�3Ʉo�����صϑR(J�����DO�a'���`�c��:R0���:��O�m�'��/�GJ��^���,	�1�" a���%v�8�c�H�y��!PF��w����}�����      �   j  x����J�@���S��{���#("�Ÿ�:]�q2顧��ot���p6u6��������-D�?����;H.�.����š�5�y6���ɥZ�Ch�|�q��b�~���n����~�U�w�,�"�އ8�}��EmE(@�`H*�h˅�8(��c�<���\�y��b}p���x|���OI�0M��;Nӡ���Eq�L΃�<�']��������rA�&�ʘ�����*KEr�p��%߂KI5p��k"��H�*I�.�Х+�-Oe֛���YE���[æ��,e�����g��q����[L�դ���,=9��*�4k���1�r7�������xU����<cU�˚��i���_S�[q      �   A   x�3��p�4���tq�FFƺf�F�
�&V&VF&�ĸ�8��|�2��W� <(�      �      x������ � �      �      x������ � �      �   _
  x���[o�ǟ�>�����У8qn�Aa�DО�-���m��\Zͷ?�r�N�-�F��x�q��uq�L�Lҷ4�Sq1'o{A�y �����i�֭�ߚe|ļ�U�i��|>D{�Gtl{�\j�#�����G��Hv"�Ax�E"���(�(3n��zd2f12E!���$�!!od�#�����K&a�n�<�(N���"��*Z�4��*�d�E�yHg�]k�jXd[f��3\�^�..��b�2���;�ʑ��f��;�4.��~���$�6�ꎸ�b�A�i�Ǉ�r��{���f���i���]������|����b(��V���-S/Q��rSTw���U�Q���|2�G�T1��ҙ���1�uR��a�ax����b��Z�A�i�C��z�sk�2rp{�-L�]�J�?��;/�瞃�X7k�e`�?�e��ɳH�� �S1�2�Z��2gd[�G��!l�t��ص���m����ѧ7�Vg]�6k��2���`�@�hL�Ǔ�F+Ĺ��O��A�;Gܥy�+����U#O��㌬/֒a� x2��T�V`H\����mZ��bxd�ϸ��ˬ|�h��v8r��JĂn�}z�9o��eש�� �s�r��H��0��a���Ì.�1�	��1gu��2`.Y�E��Dd�F��Vŀyd�{(<��z�*�S6x8n3yB���\��<A?D�t�Q�����Z��|d\oW� �m��½���zW�R��#�;n�`0�ee�|�W�����2ƕ�Đ��]��L.�� �ec*&'�蹤��砋U㻁��@��a�������MSsTd�2�L4����>30]d���S�,�$+��ެ�I��x�qW�3�&>���U���Z#�1��`A5��5����*�!͗zsW��c$0g�Tm��2Lm21�S� �>�����"(sџ|����rj3�Q���\�&�� l~�7!�� �xL)��VWA��G�B	+��|(�ʑ��<�"wZk���`�|�6��ެ� ��s�ل�iW�^������C[ֈ��-K��r�j��cHb���0���n��Dآb����P1�E~�_mW�V�;*�O��rX��T���@�R��U����R�$M՝hcavNG� x������i�`9�g��1�����
:����,Y�b(5,������V�[x�9.��<�X��ĀS��G�pT���6QY�k48N@ŀ��x커甽��6s��a���W��Tv�ZgX�Ĉ�U������w"��h,p���m��Q�Np@A�ƸA���m��K�~f��27f~s�;0oc��.*~�X�Ie�Ź�ͧ��S�z�/%�x@��o�ȭ�z�S1���Z.zbXk�e��U�M��P�Ul���o�ۊ_�����y���1o��x�7����yS?w8���Λ����a���z{�8&�}����g=�n2z��c�u�DE�E}	�e`}Y���R$��ۻ*�M�D�"v�9�B��*�s*��e�hVC�(�R1pܓF��Y6Cz!_���*�!s
fsK�a��[��k�!L��U�0p��J0r�s}.�ܳk~o)p�ƈ�*.V���)l��2�*�E��޺�V�0p��o����0pmy���{3�w�mH����F�4�����#��x��r��o1p+_z+��x��L��g����?���1�������**�+\�*��\�B���8��X&o�1��@]Q[�Nx��gcU�aLI�!;�S�Io��i_�]K8ͽl���鬿K�Fbphw��NП��C��G��������i3?������FK���i�1�b����z���8t�Y�$�:	��*Q1�������xX��k�ߔ��>��S�z���"�}���NA���$����b_���!oW�-�lA�]��%�F�iڡߩ��l�r�1� b}�!��wIu�B��!J�ޫ\l��t|��Z�"��DuͿ:���[��j��+CŀF-��N�m'�CF&�Ŵ?^�{��
�Օ�nsl�.�Z����A�,v�h1T�^��&C�6a�>W1��^yE��C��?���V1�p�l�ӛY�ei�K��cj/��&��t��a���ⶬ�Z%d��ϸ�z�L�b�`8�tj�C�d���w��zG����m0^�f�%����d[`��E�K&2�D���f�]�t}��U`����d�Z<�M��Oӿ�ŪkZC1v�84��i�����ll�[�CA�<�A>�F�_�3C�%E}b9��r�p*�IE֔Q�I7��վ����z����R�9�O4.��|ks�	�e��&�U̗���ƟO��:�۔�KL�*�ر�g}�'�G-�]����d;�0��V��P ���� �L���p��|l�"��-��/A�ji\]Ƶ���a��L��v3�a�p�I]�}��v���y��=���;�V1��>����^9�':����K�~��H�q�b/z/}�����F��u0� ��y�S��2��J�P�R}�C�䵷hi>�1:��4u�W�v��!��hu)����:��P�q����X���.�3^�Y�1�.<���5_[9up�2�� �+*�      �   �   x�m��
�0E��+�$:T����RP���ɱKJ5���&E�{��S��p�d�	.֙���}�˸�5�c����;������ò'tP��o��AGxb�m@8"��*�P����Z�V	Gb��D'B���d��՞+г���'QMe���c�\s�ߧ�;}      �   l  x���Qn"1�����h5v�8yb)e��Q������Ĕ�LWE����c�P?�=to�3=��9]�CZ�>�O��yW��#]�C���I�;��* E�x@�6���������2�7���((̛J�X����!��@<��dXWa���6Z���B5�KN��Be.2aG=j@٠Tn6�+�Ǹo�Ȫ�1xMZ��PoP���t��-�Y�@=��,C`��%��P�+�5]��czOR�>j
�^�p����@ʿC[G.�����}�5k2R�g�7�-o4`���q��4䣠 o�ܿ�ѹ	JA�;���!Co��H��]���e:��*�����9k/�1��P��;�[z��C����&�N�g�yv0�%���
h�}��M��7�(�������9����;i�AAmP�asN��g,����)١������1�o�ͣ���˸��w���F�,��l��(rG�<5�fQP[��x�L�t�Z�\�N����FE䵸�h��9�N���'������܃��u=c�[5S6��گ�ۃQ�.XU�d�'4_���Ԝ^�)�m��|y4���4�5��n@������|j��Hُ6��xH��J�	�2��.��9i3�{��Y=]�����})���zѯ:K�i	V���b�$���+_�Э�#��E3+qu�7��	�K�]��Qx��Gs)��߮���&���Od��`�&@3'�:���Y+��iF5�H��������}����PhK�<��H\YI��T�o�hF$�8�c�m�-��(
�af�*��P�t�?��U��4yoѬ�\B�:@jI�$͂$�Z�-�.���L�1)��y���}}zz����      �   
  x��X�r�8�FO�(;�d�O:�4�.��ވ���gdk3y�=��E�:�$�$����~�l�L%�PR4ރH��O�3����c����O�ٔE�]��#�n���Bƹ#|�X�U�5��#߸�7>'��A���I��;��@����A`�Hx$|J�d/���Ȓ�._�l��6e-����K2�i�9��5b/
QxkQ���3����ߕ�-�oFG��"�tK����ʉrf�H��\�@�j�z�X�,U. &bY�K w�P�ߔ�y ]rx.w�p� >Z�^U���6���Z�v9]�0B�`3�;њ�� �ދH�/����,��E���I�r?%[��nf�h��|pZ���i��b��F9f�m��Ĩk˃D��"/��[+��I~Q�#4��*xo�Ώ�	/��:���#�}��T-�^�
���Q��� A�K����Tnr�#t1��
�O�t6��5�
�����ɈE� rDcU��"�3��;&�A�ŉ2��C]�J�6V���m���,�!ath=�L-?�.���p�����sR�x.S��#���`k
�T�9E�Wz,������V�����6m0�N�X�f�&r���E5n��#,�����E�}η(-m�����Kz�"FXx�`r7�B���v�L�j#%�}�Y����E.���6���Vu⾃p� 0���+����s'E�Z0���u�Oc�]w����F:e����G4A`Pg�2-��LNU�-��`�����-/�E���4X�(-�g���7,�N�X����RN�k�Di9x��'��(�=������[N������0e���{���L)�1����>tw��OXr}��ﮐ�W���^L���#lW�|>�_�l���e�ޓؚ>��n��|�S�a�GG����/IYW������-�a$�e�ܬ�XT��N?P��
\L)3RS:&�F��W�y��n�)����>�כӷVKєRϟc�[�Ȇ�f=��x���8'!q-)�a���[Yr�޶�&�r7��������<��Z>%Htg�Խ�.W�>�,��0����a�JȂ�:�7�O#�K-yh�H0�3�ת��T��z�v��B��f���73V|߆�`~%�N!�n�yt�=b��3�`�}B �&1W�%�}KR����{m�ԟ�m�X��#A4���7Ȭ3Z�s?^�??�?�<���Y��#�v�������@bz�܆�Дsq.�����s�`���)u-%�GڮΆm�#��"��${µ      �   �  x���Mo�0�Ϟ_�#��GTU���t���֋ٸ�I⬜D��8�vi�Z1������yg,��
�����K�Y]'�vɵ9�<C�����9�x���N_�
�H��I���:O718z���w��ʳ�ܝg �UUۆT��X�%(v���꒝i�iW�o�C�mr��i��k�خ����,�S�`�NmH�c��Й{�S�vҨ��]���Ύ�;�JS�i"򻄂�{7޺^�	2B�
x�W��R�c�Q�xM�����&O�hjs�UL.�d�~(�OB@�/�����d{�t���� x��:8h��tƗ㬺�Z�6��Hv�K�eӀga�]������m/�����]��<�{=h������@�?�1p�l���L=3,���0[{��v8
I >;L�@`�mh��8A��o1L�Eۘ.��Ϧ��	�Cߒ����5�h��>���k�c�5����	�>x����C)AFVu�@dl��ݱ�=-2�GĒ��0���oqzlM�Y�Ċ}�S�j����i@[|!XL�^���P��i4k��JEdYL��NWT���ن����n��C�)���H~�֜�go���b�ۈ�j����!#J�4�Wg�O�/��B����<��4�} � ��]2      �      x�սk��q%���W0b���f�0<����ۺW�H�Q̍��8-R-��I�m[�~���*��Ek��qd-�*��H�S+�`��^���~���ë7ox���^�����[B������ի6���\�gk|i�K�B��/�z�ů~��g_����7�u�dH��$�����/_�x��?|�ͻw�|���?�����?�~|�ß^���ËW/�?����?���?��o���E}��_�����7�����������w/^�z��?}����D|�i��'hK���M���	r���m��/�}����w߼����śW/޿{��wo?�~��ŏ�{�����w|��W��~E������W�����߿����?�e�μtb=�<h�=}S��������j^*�^R0�&e6QK��y����_�����/3��S�eA��'j*n�(-���_�����,���}��Dŗ�A�UJ�����Ͱ�;��d�k2yz2�nz���]e�ۧ����ؗk��N�Qy]E��b��Q�K�2M����&(�#����;d��y�HG�qְm�u�qތ��,��[]�+G5I��Mc���2g�g�Va���V��<�t B�g�i�ӕ���\����eM�ݝ����-�ew�y�3��A��M�$� �m�Ĵ�I�����4���ձ�κ�cxi:�x�»��iQ����d�(@��(\&R���5d�嚪���+�zy~���Rz���5y��r��ټ���z����g�%����6��O�&yODm���z��7ѕZi�y�|�x���M^^����/��mp�^�n�|P��˪� ���]�����A��1�F���m����ҳV9k��S��z�.�c��i��qt״, ֟e���7�.O�t� �Ya��7f�a���~6�E�S||9q+e���"���8���W�׋?6S3W/޼}����}xa�*�����������~�����>��u?<���߿~��?>�����?~|��9�0����￻9��"�i}Vx�j�vEz���I���N�
c�2�X~��>�1��e�X?��rxE��K�SC:�#�`�g��H�F��6/�z�^|����x��:K��\A`K_X��E2_�f�'4����*��XƓ
Wa��P�ݦ���mGz�����w~�2oǼ'��G�� 1��2o^zQ��I���'sZ��yA��۫��7?����o��"��g_�Q�/	s��r+�za�Y�m#��n�K��K!�kI���\<0:��%Y��)�Y�����M�����P��b�N����K���
�dm7ߺ�a2�� *k��d��0�6BYy\1�W����� =�Qk�R����h��X��u����$Ax�\��mG��ǁ6��˧n�����
e����
u��)2�s�x�ݓ_&W�s��0����c�]/�L@�t%Tp�iv����4��L>��J�9Ǵٿ�;��L3�K��?\�����L?��~����n��0�tb����-o����ܜ���py�������[�?v�I�ǙQ|��z�隴�t醛.����n�,��yR �tA�]��,�v.]Q�>:Wh���[�|t�7� �G�'�Z�a���5'�`�ץ�G����tV�:�|O����BQYY���Uߓ�'�MY�?4��s��`Ѓc�ɸRa�m�mƯ�Շ��ʣ^3�T@x͜x��|��:)�,����r�ӌEl��ڴ�p����	8�hatމ����;��{<�=}utc�O_ޞ�؞s=��tUWp�����	7��ڛ�֫�*�'NVɷ	���������ۏ?N�Oƪ
���o��%̺��9`�z�!O�f�(7�Y�=��F��~���n���l��ڂ>�YE�Ā���\��4��O:_��1æ�HN��0�pz�Up�)�t��2a�|�O��.�=]Y]�6����M�B�L��ן>���z�CS���~f�Ȃ���L/U�`�p����p[��_�6�U�������ќć�[o7c��+~MV��c��k��T`��Vq5J@�U�f�� 1 ,��*��a�"��L#!p��"ˊ�kN2z��&��뼹��̢/�5;w-c�K��=�)�T��.�5�uk�t��e��T��L�\�պܻS$��3��c'��v�i+Vp$zv$��UG�kr�v�|�)v]�I�d��5��`�+��+����q06N�����Qg��Kj�z�X�t��p-��E�_�/��X���|���oU��������\����U�&�W�P�y	UJ���
nVqK��L�p<�_ܬ^ܬ��inVu2�Wp�Z�}����iub����4ɷ��}9=k�Bcx,W����M��SP��xZ�:o&�C����a��*i��|����S�r]�";\�V\�|ȇeV�}�Av���y&I'��!,z�o�R>��B�Wq}5!�:]��U�� ��7�as6q�����t+Ā듲�t��������1�����?q���m_b�L�b��V�!�r�̱���ЂlA��WN�iX��~Yӓ���6�[00+c����ᝰ�;�Bi���8֖|Z֑T����,�+@T���H�c?��ߝ
9T`�����C����od�U��g�N���f0,	lZ��g���[�,c��s2��ff/����1`�ׅw`�����O����4f���ʿ+�f��z��j�c�ʯD�����w٥`Ā0'�6E6�E_fZ��`@XI)�W!����l�
��)��Eò\�/�/\�a�
EV*ʝ���/U���X?���R1���A���<��*n�R�%Q��%��ʊ��2+�J�#��%Y��J?}��Dָ�I�k"Ā;���"�ʒ����!�5��c����3�1���]a`�BI&��bYM�����#�L�{��}d�Tң�,��'�I�N;�+�I�*i�������x��x�|�n]tn�ާ�pn�'M����YA�m~c�E>H./�Xm//)��~�U�-%�I�*L�Q�]yI��DZ�DN�'�����K�R��Ct��:����27<\������_�������<���k���D�֗����W���ye8�N�v^C�����i��a:wz�T����}%�t���<5��W��8��l���齱'��F��䗻�
{oL�vJ0�XS"h����is�~�m��;�=D�ɭT^��8����*�m%�*�´Rl�ɓYa�{�;�tҋ��ݝb(*x��x�ؖb;�=e�|��='�y%�`�kϵI(���)�,K�=�X
/��J
�)�8��p��f�� K	��2�v<���0���Jz/���ަ����۔������	����Ϲ�����j-m�@z^*�y%�wfn�?y�+��M:�����cmA.���I�*+zJK�X�ך&�}�z�)Ϸ9�!�.q�J��T��� �<��P�'fY[��nqZ��Zouɰy�)�=��i�+^���]��z�d�3�J��W��x��x2�V����9.�����y�����M+���T��!x�-iS�J�����6U$A0���<�!F��a��i�\*ԓ�@��N���JX�s�0�x�U�m˔�"w*Xs��P���zzMTp2������]B�G}�.K�͋�RQ>����5�?���5���_��_��������������߼������������_}��~�o^��������z����7?�=ԍmqǀ�2G%���v���}�!�\�մ8��1�",�m=?<��)M�C�`מ,ȗl�R�f��͖N�*�U*�<�rӺHm���E&��ƒ�9���G5F[�~1�º8���J�h�>ᦠ�`@���aL[�����ĀW��D�N�F�����a��+Ns���թ�\�1�;�SU��b��|6��I0�|��|v�|V����h�S#N-t�Z(~"�\��J���Gax]���������4H��7R�$E��_.jb��?E�V�-)�{�<_�>��T�    {�+q������ŵw�d(��w;���b��0�CE¨��Qa�&�G/���P�!A����p8̋��+��*uO�ǐ�j���8ŕ��ep�Ž*����Ξ��
�U/�7ޓCi]kee8W�8W��z��Q_���1\���k��ڡ�Y�;x'�y�>��O�l�'��za쨥��;�w�0)�gw�vnIu�-��[�dSL4�k�q���'�=�e3�Iu��+8��L��՚�f�i�}�	c��+�r�Lt����p�š:��dש�T-Gy�,a�S]��u��m��6G]K�3s��S��
�]q��:���ۜ�ݮd����P�vTzqs��p8���ا0���l=|��I<�i�grk�1��3�3Y�l�dǜ�T�D�
̃�TKҢ��*�����I|�R���f�S�U���sN�b�BaCU(q�!����F���x�)-��dx��'���ŴE�X�v� E�=U�j��͸/P��k��&�c��Jō#�5S+vy}��c��3;)��x+;�'���4+������N}�Aƀ�I��ɱ�fbI:2�u��,�3F��J�5U�t�ˏ��Wi��`�O�h�_��]J�;�l�`8��8��8�֋U%A�4s�U��&�%c'��.�@��&���dx�$?���4�y��:�-c���qU���1�; �[��p��L-ٙ���C�i�)�\� m��Y�ľԮu�<W��)�,��y�T[T0ݒD�-��|H�b�al2�wd�R� Y�4Ӳ�ȋd�H��%U��=���'vճ瀲
ܓx�&�J#�;�*xł�ԛo�܂!�bN*y�;�-if�nf|ǀ�Ɉc,M�q囂�;�K���
��s�%�1h��x��_�=`��oƀ{ɈW���ΤOU+*�N�+Vq}���1�N8c ��K�!ׂQ�Z�K���1%.=U\z]�&��I����`��_薮K�D��L0paQl��l��i����`��V�,�$+���
g���2k�Z�ʭamS�V0��EG,E��uQ��,�D֦���ks�}b`�,������毆p��,'�ו�w�b��w���'Q-�,uRi*̡Ǽ���*oT��T��V�rS�;a�����2Xm:��UX�J|�|O����PX{�˫�~�s��
^b%~��	A��L�#!u�=��Z��D��޿~������g2�@y��綰� 1��[KF�&�.�O�h"�o,y;\{kq"z6�\v"vUV&��89� ���D��1�Z�$�I_ګ�|��fǀ�졮p��_][s[0������bg���y�^<�����߽}���O4b����[�@��	֛\����[K&;/fn����nR�1z�=���;���0���c�x���|����ʿ2�t�8ζ��RهiF2�܍L�(FN^5U���`Lm�I���Q���]e������T���1�`��W���b��?n:I�f�3������pӭR�t{���L;
,U`X;?݆�Q�t�����<�~W�%����-�&��/ѵ��K��K���!?e�)M�T��@��.�d�[�u��L\��JUv��`�VQ5}=OƀSP�C���!�'1w��o��;l�Q��6�_~��o�P��&:ϕ�+�{}ɛ{����4���1`?���V��&:OT�Kc2L�J��z:��U�� �(I���iT�T��1`��R�s��$<���0`�_�I�8\mI3���{onx喝�=�t��x;�$쫨:��w��[�bfRy:�F�`
Y�G%��+��Uѹ6�鵸8$����4�ܝ�I+X���#�z:S���鑢�H�7�4�N��S�1`��b���I`��mJ5�98;w��j�C�R�s�0�V�\�3P0��J�ח\3����g�1Pbr�ʘq�i"E0��]a�������H����|
�^?>��/�����b:�e�E�ڻ"U��$-�تu�1��y�V�G��k��#�QW�M'��D��k����`�y���}c�b�`8�}���u����m�58����œ����?�{���ۏ/~|�ݻ��^����o��j'��g���C_ё�����W_��ǟ����c�w����kRq�b�ux��0�e�lYV��jQ
�~������v�i-m��Ҟ6A�\�,�����8�B��__0`,T%'�{(�'a��g�p�z���G)�ܥ�mzڌǧ8lUq�ޥ���s.)�45F��J���Դ�tk�Pv�%-��7t��L���\��'q|���y�d��<�L�8�Y��m0ibfe��ǧ�gQ���/.�p|q|�:��<���Mk��C%�O�M��f���%�1�;��s�h43y�6�V0���|���u�y	�F��L~O%~��i�W��:A8"�$V�zi'�Q��x͎/�/$�f���l����S.�>	��|�����d�Է�c�
��
��V��\nK�|W�ޚ�;��Cƀ�M���R��Ϭ�ҩVL��I �O�]�AC}���w���.4k߳�1�tS�.,]j���e�`�`n�T���+�,w�Ss�{a7�Ǯ���V���g�`�{a�ऩ�Y��6�c��x���<��fW��bmݦ()=O���M��b����0G��\w��C'���/ܻ\�t�~�0\�FΞqE6d� xqu�v,S��ֱ,p*qu���Nզ	x;)����]b�G]��_)�W��nz\A���_S��C�� w#�K�xc)�c�tZ����b.Qps<�Ҭ�^.\���9�����l��1�9A�9s�u^a�٧��o���'��<�sW�
�%]K��~b�˵�2�1p�HV��KD�	Y���D�8GE�v�C���d�SbS��(�R1"&{UV�J�X/K*l�f�z�
�EY�j<�Rac7�&M�9���zYRa�ā���*%ʀ9�����=����<��t�UX/+�,Ud��k>�Z�TX/+�,�q�O�
�dm��pe~�_��N6�
�ei��,����0��2"�Y�ڸ�_o��eY����ښ��)>��zYNdŲ6�m妣M�����J���UY�Vf��%[Y�V׎��]��a��%[Y�V�׶2�%�9�p���W�Zå�������zK�%t�
)�wϿ׎ͥ�*^+w!�X��Ā��zpo]:/�r'#m���"+���sx�0�P�� Y��qH����c 衊sV�"�EY�#j�zg��PԚՍ��:�,�J�>6�/�S����ʺ)eފ�����jb��P�"j�Au픧���Rf��YCk�q~I�>=�*��QV���5�iƎ���"ˉ�p�Z�9���0�Rdy�\�e=�"V��(��B�5jm�Q��6
ֿ(�u���J��l𥻼�|����7��T�G)��P����S�T��b��~s9���h�%[-]})g�Bwj����INP�YY��2�V]a�,%��fc��B��u�a�X/K�,��h�i���'c�,#��f��ze���2��1�Q
G���R�y*ӞP������W�_/׫׌�����e��Ŝc0*��DV(��eY��˼�<NY*�q��5�^��0E��#O*�%]���D�u����d�%W�EY]MT�zYrn�.?�L����G97JwjU���G��`V�����/��Ui�e�`��9�,���Oe��w
c�,'�8d%�חe�v2���"˔9���U���
�e�e��k�0����X/+�,v'�E]�S��^��SYK8iy,'�jw5��o���f
�mg�WW�_y���p}PZ���s�n�����X/K��X��=��wM���q�2��QM��E_;��w����^��S%%H_}�Q�T��-,J��*�1�%3 �{03�˒s�T)�W-����5�1�ˢs��g�������`��TA�����O;�>9�z�Qh�@�9��i'�m�mh#݂b�o�Ѹ��Y�ɹ��f�V��=��Fqp�l���8��.-�vG28���m
c��Z��E&�P��������ؐ���.�    ��֭% -�d�n�$���խ�uj�;R��M#mZ�`��ܐ%�k&iխi�kwP�-�X7KJ�b��$�(˱
�_HJ�,��Tں� o%�K�026��ݩQ���L�#ո%���]����𶅷���uk���{ӣL\�y�{���Gԋ��ܟ_�Y�7B/(E�h8��fE�79VC�B������}nTa=��r�C���X|��� &)�$q+N�7m�\�3x�Mʊ�5d��ǻ�q�q�ܤ�ke��k����r��,ε.I�y�ގ��M7�*���^h.k.���w�cݿ���6��03OQ�p������4�B��L��ʼôU�4ň-@dGtQ�=Q3�/m7�x����s��x
xG�=�T.P(�(�1^,jS�x���$�i�:յN?�$�O ܶTQ��V/�n�.Y���	�� )j��(X/�|d�1�9��t�b���Jx��;M{�6%(*�%���˴%i����ڢ��Ε,��v�V2��2��\Zܴ�ݨS��
�I�Ӣ�y��E����7?��Ⱦ���������ǏϿ}�������~�O����B00/CPe�<�!4�v�����GW
�yC0m�``Q�`����C�M�Ǝ�!$��!���kSƱU������!4~i���ᚶyO�,"��VўG�z��Յ{� �꾹U�[QtL8
ͤS���Ch�{��!X�3^���w.@A.�&�(��	!�����b.ٗ�0���w��WV.f-sz"Cp�vN0nS��3Ͱ����l�LB��1Xʣi��``N���a1�?�]��[?�``��������wCXho��X��Pz���x��!h�ª��7o޾y���7﮳T���X���1K>��Z��Y��,S��8�~������D0��=�#x�/���T��Ѥ6[H0�;�\�h�s*rm��Fm얦��V������[��`=�-�=�Z{�*�^r��6\0 �aGE�+1��rl'���:��S��DM�Wd�1��V̾^m�勬K9���T�v&�YV�3�rc�ֆ��C���h��!��k:�)�r�B�{�11P��<T7��Z����*$��
�j�g�fOk��@	�RԬ��j�h�SZG��
�C�D����&�,�
��C��
���7���p�
u;|i�MgV+(�(X���1P��H�'v�]/��_O���	E`)���]��Ŷ7�S~/��`�&��~������=�A�q�U>��~�年a�?�Վ�R���:y��m}��M��Y?+�B[O�}η�0@;+c�H��J��GV���6w(��^Z(֯�
�i�V���.��(����� m*6u�.��.�6��Lw�>�v Pa����.�f��s��
��L7�sz�����ࢡ���Ǜ4��T"�w{��^h���:+b�u֜�2Ǹ#]w�`���2U�l�X�\�SPN��Ix��d�xnWa=o�7����j�1�9���['x+��.����Xka͕f.�s��
��5M,@�4'a��pVa���i:�tz�V��
�rMμ��S�m����6�Ԉu"��0GA��4f΁�w�9�Ū0@�^�OcWR�m��d�NB���͝��d����B�\��)�N��*�VB{ە�|񳩼;��`g���]�ؓn���'�u����<�!:��'*����� s+�9�?�I]߄��v��vL���GX��d����
���7���Ր9�s�*�����&T�ۜ	[��Ձ>�k�e��J���1�'a���Ng�gNu���c3������J�+a>���*z��Z�s� j�1������F�T�G���3�CO%T+0��ڼ�'�73K��y���R��Q2!'^D6�67��
:���C�2n1o��ҶW(c�y�æ)�ts��ƣ�b��Q�"�;Q5w碈��2�%��,�pN��9c=q*I��!7q�^b�dʥ>��Ţz#��-g�K�G���8��*���F漄GP��<�W0�[���u@Ut7TE*�����T3��E� �x�5��w�TO�� � ğ�Թ�J� N���j�j�r&8��}"��YSJ��b�w.3F�d�U�U&*���f�SGO�{+���W��T����a��r�Csx��aB� ؊��Z���t�7߅t�u�����c:���ZK[/�#�il��6f_���ת��b������@0�w���� �v��Ca^�rL�,������0�U42-Md�E���#>�N�M��bm��G���K�	b�V�vy�N��崭��f��B�_}OB{�\a���%�`"m�d�� ?� �ĵ���/-��6��M\$n��ù�~ˌ�� f�><�0@;m�h[@���v<� ��֭cW0@�N��.M=���_��x�G��-a��K{{�D�Q��Ԛ�� sU��^^n�g��z���N�pʢ��^U�X��>z�9c�� q#S�/O����)7�b�d��sSU=dfO�k/{�����L>��>,zZ�w��K���U�T�r��Y�Jڦv���	I���R�����Q�(��a�i�	��dlW��7�c��������.�g�Nm,�`�w��p�c�ox�������	�M�ܪ��ޱ�7ՓQGG�8m}��a�c��*��SF/�P/0?_�;�ka~�ܖ�ρ$;h����6�y�c�6)GbvZ����=UD�0��	��j��{�H�8oM7uc曽�a�wެ�Nst1G��FH{��ԑ/�&��9�a� ����qe��m��q��X�ܬ¼�K|�殛s\��U�K��4���k� q-���|�`�����;�a�^f񓩻NU	�r��W]�����_�ꩱ����`ޥ��꺛���|&��%i{�şN�2+B\�'��Ƨ�c�x��I��+���o�9���Y.ĭSy��c=�M>���6�z�`�
�Rth"����`�����&�P��7c���z>kת����x�w�U�¡�P�)���7�u����Ѷ��1��.�c�U\3zpkݿ5�C����&.q�f@��e�Y���]R�.L��-�c�z*ԟ�Z�I�n{j��
�����*��N˝#�m�������
�d��3�k!���L�?\>\�� ι�Fřg��-� ���G���i[�m^�'�+^/����k�]�����d����\!�_�������8�mS�k� � ğ�If�
_;�G!�N2ה-�1@<���H�I�~ZX��@�S˞
��U��+׮~������ͭ��/�]m�V���U�hWc���O5�35�4�o;�!�O_�����H"
NH{����NG�r���G,}��c�9ݧ�Mg�����2eoX���YOe֧e�2��U�`d��Ә��[1�c�~ԾYg���ek9apOe�HU�y*�e�M�7�i�ի�&rOa�;c��������Nŝ�|вb�2H\�1�bb�I�c���Z�ϊ�g��S�p�[*��>�w��>�V(ӧ=���NO��m׸���|�}�
�xw�1�;o5���}��Qx�����j������s盲;�ӎ����D]n��ڪ�.@g�v��q��t���(�5�P��c�iy;�[!^t�i%ʉ�m��� �
�=�m�N��l�	Ľg�v"k�t�1�:k=���	N�1�9��A9�B�(��c�y��_��d�n}G��ç�0�M���E��J����-|��s� s-��z6�Mރ`�9]��Θ�m�n������i����O�Ƨ�Ȃ�I���%�$MeA�$V~�;����$�R�M�}r��==���;�`���wt4��>�1�;�;��y;0����!z��w��=��]q ���7'o����;�h6oh��$�u-�K���������G��J�o�3��=X..��ja^�g�M�_.��r!����z��6%B0��
�'ps�>�>Ry{�Dْ    B����)��q�p�.S����1@=�������+	x�2���.�97������$S��̱SX6��V��[��*K�R(`���������3��Ru�e� w-��	���T0�;_��yݥ���|�6�O0��
o>����w����K�@�t;3��$y�	��e�]���[3������<r������|Ҟ���Qf{r9��b��0h'������L��^��5����Z��]9��j=���teo=/�h��1�[o��I[��d� m#�~����
m[f{��CI��e���d��2>ۦUM�)��P��������7x�=�A�*��1�;�O�m�]S�~� �$��?050r�Fέ�ѡ���ݖ��1���M��^jq�&�3�`����3�����_�1���O`of��)�1��
��{���Z3c�����f�� o����QPO��4�v��=x�]J�ޥ��8�&�I0@<�%r2.���1��+ ��8��~Z3O"ٕ��'nW!^���>Eǩ7�)�����U�ԉ�}���η�OO�L��T��Fx=\/i�B��T��VJ��PB�'`�ʄ�gᴗ�F�?�>ĩȑ?�������A�[�Op�|�wO�wU8���3s�c[�P0�:�|��,~Z��FSuGa���!��2�z��k�U�T�ȯ��s�JX���Ǵk�1@]�$ke�޴m���Ma^�Bs�@�q���{*i�X�>�W���fW;�;!.o��.�-Ʃ�n�5̊Tf��j��eZ�<�ize�1@=uޡZ�'Hb���ad
��G���yMƈ�m�ԓPW%$u^͘���v)n��Ԑ��[�Iw3w�G��G:�u�˙S���7� �|�������߃#��#�
���Ow	�'n�7c��-�K�8�x�{|�Sm!w<��\ڝm�ް�PY!_�ͧ�̓��r!`����hA�]�� u�?�9co�b�Ne�K>��?hS9!w�)��U�X=� m%�d���Z#3c��ܚ�?s�ۦ�;h�2���O�q�52�[!>�Źq�@8�:P!�|.�����ħ
B�xs&���C��V�A0�<�4ӊ��Uͅ) ��6�����ZIB\��4��Rщ�.XO���㵙��v�o,`�Ȩ��;�y��i�}�t[I0�\s����7���s� sS���sZ��F�q	�[!��2�f��/UW?_0@�	���g.s��������*����d�ك̃0��s�"�:Y
h�B���i�	ғ���d����v2mh�L*���v2�ƙ/X_l�Q	_�Y�����������Z(�����:^�*W��^,G)����}������^xsu����ׅ�6
��¼xl&�U��|[�[0@<	q��|���̛�i�:摊����]��-n4M�jaP'�ӓH|��6�J0�[�����떊��H�O����&>n�n��������3��LRw�a���-OG��V��T��I�i��Y�^�`0��	�̮ݗ`�w,�KU˹�fW]N0@<	q1<�<TloJ`�'NUO�Q#��6sc��N_�T��ii6.v�]ظ��`����6M�#JU�z��LG�z�Mus.�Dkf٫�'�Q�Vw�y��|����ǽ��QI4�hP*���=M��פ�Zt3PENW�ܩ%����v��EZ�̗pql�(�[KC�ʑB��@67G��G�n/5�`@��(��.i�5N��{���:S3-�y�����)T��?�3�]'>�)��,��˂����/jZ�-�6�V0@<q�,���4��! N����2���Ʋ��0Oe�� x�N,g�,;-4A�J�z<��B�r��9嶷�� sU����'j�v�m���Z�v����=�d�1���xT����Q^`��j#U�q�9�M���Hv��岨�h�r]��Lfޚ����i+'�L�
�V7h�}��i������ �Xh�D��r���P�8
���w��ٶ�"��A�,�;�|3��v	�h�*�="���k����Z��k�L4�e�],c��)���c&mۛ=��]i��25M|�Q�Xkjb0w�<S�d�[*�G���[��y���g��ʄ�5g��7,�T���+�N,�e)+�s�1�'Y*\�q�;��8��PI{���if�\[b@0��ޙGE��K�� ���J��T��b�f���W�WT���V��v�g�Sĭ�Y�]����ĝ�Ce�ך�����iHey���q�����{���囦=��c�=����Q��8c�w޳3�6��b�Xϛ���'�9�����Muy�hE��m���{���86�-�m
����QG�'�`�w�1͓(�X�v�0x;�=�P,�����K��|w����~����ޱ�~��t�`���}_n�u��:�<�H��募M���y��}i��6�4T�U�kOp� s-̹�Q�o4�w�`��)�K�33Cϑ�^u�%a}��𘪢���H�h6����d�=�<�]�;�/��0g0�2鬌{����_�`�z�I�[O39t�nP%{�y�0/Lz#٩Z��I���6�<g�#w��^�T��A�C'�-V�d��t��i��9��� � s-���|����>�,���*�9�:�μ��n"� �|���14���6�O��2�;�s!�Y�ÁB��V��H�y���e���aB��<�x�Muvn,�����QV�v�[�؉j���!	�'���nZf	V�E������!:��I�����B�y���r"뮦�`���۹��\ڮ[�w��<�x�N���k� m+���5_����`턵~����T�G�'�!�z��AX���]����A�x���Nd��B��IX�ގ�awd3ֳ�2<�=��)vVC� mzc!f���T�& ��87�����I�6�F0T�(�AU����dv̎+��;�L���Lt�&c�x�%�h^��L��Jc�g��g-q^[���O�����	JT��AfbC�m6�;�1@\=�'є�mA���)���T$s*��4�i"��i?W���S�m�=�SKQLݡ�`N{Z]�=;�i����vpm��<�=��@I� � �����m	r� �Xx��:�|���uꭇ݀��1Gц�&�nf��:�gN~�Q�!ŉ�2�؇�1��n^�Nd��|��5+�SY��DI0� Qas�NcMu��1� k+��|��C�v�Z�gݬ� k�+�̙ubi	O���#�a�x��Io�*� �X��\'�by��]�mi`��DT̑9�����m4�v�z�TA��3���n�un�)+Q	s�b��w���۵�L��Zx�����.�6�7o�ټ]�;���|�9B����*�	x;᭟�~����?����w)�9ӳ5n�7� �P�s�D��8����ڽ�G�^�Y�B�s��f<5�0@=��cͤ������)��XUfR�=s�ǓC�� u�+SPX������[OQ��B<�t-&�Nz�↉�=&E1ɺf�'M�g�A���Pى�V@��}(:#�"L�$FuQ��+���`pY�,��4K0��u�dm���H���j���]�o0T��HeWE����ǋ����,c`M۲�Uٌ��m�~kSx���e3N���$�)��� �^F��8s��)�� �P�s��U9RH�V�b�L|�_H���10D<�|��y�Lҟ��+ׅ��0k!i���	�&>;�ZH�n�x|��|��?��P�Ղ�U��C�]�E�i[s�h5� �ȼK����1�	�F�����p$����ݭo� o]x�u�����cx�V�ކyK1�ɼ���mo~2̊���C�v�7'�̪�%X'���<U����������ߖ���2���@kg�
I��<�⑉��{5)��9�S|[�ީ��9M���n�h���m묤^!���1@\Q���W�u�����
�u�ͅ�ͤ�d��5����.��gi'��|[�`m�Q�B�$�ڮО����^�    ���<,(�-�n�N5*��w*�g��p���0�;2���I�ۙclV;�B�������$ǣ�u�����C9b����T�������oCU�F-����Y7/���n��g��^&������↉ӡZ�,�
�l��;���'U0~��Y�G���!��)G�/O8oɫ�#i���wrUU�EU�|bY.-[g��:�}5�A\�,R�Ѣ�Ĥ|6$�="���g�q��z�<c�Z�~b,9��n�BF�ٵ��OZ�U0/�\�k�sG]�R����w�� �y�������;/aO�yg7b���G5ؼT���Dҝ�@+�8_�vYg����f7N!2��ڛg�!����4K��N�ߗ�J���,镡��N��Ÿj�' ߈&�w�?�=�
fRM'���xv0ׅ9/���Md~�cRa��a���f�����@0@���|�9ԙ�3�F�O">S�O�"�`�x���@���,���JC�x*��\�g��⑉��_Z��>����
�mo>�J���������L�Ga���JۣsL�-�ܤ�]$��Pg��]C���o,K�`��a�R4{u�tt�V��0@��۾�>�8-H�i6�_� uW��ޝ�,�1dGrŶ#��nXqV�Q�������8��e�`f1�;2�=;vRw&ن�� �
qɎ��\H6�����؟�L�������艥(��I�j�)jkj��Ena�f&�/��c���L҃����kͯ _U�7ˬ8&�]s�!▉��H7U�h�1�<<���Z����X��X�sF�^�D��=�a=43O̼�Tͼ;S̴c��ZmU�ʤN²?WҍsE��\��~j$w1��\��M��~r�@b�!ަ�Z-�9�vk
�x[�=�Z�pTb�Hq�����Y�f壬��X�o	�G�M3w_�뇠�%YM��cO�ڬY�rG����Ҹ�C�}�=�H�pl�!�Y[96>,�[cP�,��R�����D0K�-��V���|��{q3�n�h�gU������܀k�fR��.�`&X����b���E�f�|������ԈC�M���XIi����O&bn�y�NLGa�����4_Obռ�捤�C�}!�9@!�iv퍥���Œ����ܡ����c�y,̟�J���M�b��y�C�.s�����1@ܬ��S�B�ګ�
f�0
W�Z�e�pW��\?��$����!��M��6�7�S�3W�jM��!ޖy��q�A�t�d�c��+�K@Ҵ��Ʊ?S��3��|uN+��M��0�x�{����f�mv��!��9�.	�����0D<1q��f�x�r� q���d��x���T��mShk*m�Z�C�-�.ڕ��]i�$��"�5��.I\��a$��).�c�y,�y��y���̱G���Ǚ�7��}��W����/' 6�w��u؜�� �S&�8e(H�0>?�t�-�� �*+t���	��tk��\
$��a2�N�am۵��� �7S��{4N��i_��[���d�rϳ?���������|���o6���s�m7�j�[�����3�m���O5kG
���P�[Q%�y�h��#����8R����������t�� \�����V���<�,��` 0�n�x�#6�0�i	����kp��L=>Tmb���X���k�K�!��j��:/�6�4mD�`�z~�V�b�����4M{�0��V��C�r�mȶ���.*~�e���?\+Y��I�}tc��}����y�Gם�x�����^�D��q�C�1D�?T�]�N�;���0�;<T�]�2��i\�e���a��a��݅�UĈ{��C�Sa�A���;(���i��T�0�M���G;5��
�mcF?#������`�!�0g�gZ�-�T��4b��ɪ�Ҍ����:����8o���j�NAwL\
y��o�8H���i0D��,�i��FҶk�1D<⚗J�Iܝ�)�����H�6~F����\7���T���|B���2�*�U`NE�6�a��b_��݉�n����[��7�h㺳��^���s]��fkf�Y�i����3���{��K�$��;��P�u�Uc܊������cq&�uX�k��`+#GA�b=o
�aގyߥ�� ﳯ��:�vS2-C�Y�=������q-3�y��=�����=�,�J,���-��n0$Ή���I��4uz�T���q�i�|MܖɅ0$�q붲T��+��E0��eL����2�����6�������1��t�ѽ�.L�j�0��ci��\�����~�`H��m�.P��K��zJ�0$M:nW�KK�i V�֪4kT�H���Y1$�,I%}(��(�(��`�eA��.�h��'�Ş+�T���EZ{S�J(ȍV�+�gNVv�Dg���f׹�?�aQ���δf����mc���ois�lϚp�Fw.�Pa@r��ܸ����C����:�.�yr̶^7��h9��� �1��M��a͝����G�M���(���,�Rz�ߝ�t��*1��yz
�U��1�<=����8˙̕�`��cq�`�2�+�Wb�x�HD���<B1�<�%���	U�k��C17�9g��f�s��ޓ�1�e�(Y-�,t�e7�*噹+sή�� �\-z��n0�ܗ9/:�2!��fٟ-����q�j����rٲkO��+Q��:���J�4-��n�L��U1�8�u�4u� X\$��m�8^tp�4Ӵ�Z����������g-�`�@��Zu�t�������]w4ވ=S�pD���z�y�;��:���>���wZ�,'���9,Ѣ(���s>��������`��/s��e"Y8�2ﶨ�qEj�\�㗩ʋ��1��9��3�k������E�Gsn�?S�Mdю��e="���`+:XƓ����r�%=P����M�C#�Ry��#윚2Q~�L��s�F.�ʷ�:"ϙ��yO`Ť[+�2��
�J��3���&��ݧ���� ��:-|����c�q<淪:<.��7CͰ?o�)U�K�mc�c
��%���Wf�'e��J�jP�k�
��O���蠴��IS�{�����Ne�AUI�Z�i��[5�=o��u�ԋ>l�^gR�M�ݎ!�PgE�j?���NӪ&y�u_���פ&����GQ����c�<���l)/+��X���Oh$t�l�yw1O̼<�쌤�����N����u3*���_��Q0�\溘8&TD=X��"&Z-|qK��,࢖�N�$Z|ۓG%��Z������ﲰ/�}����߿��������3�=e��?>~�������^>����������o]"9ֳ.a��z8r��#��b�Y��a6�D�/+\S�%M�QI�t�U�D��v�T�Bc.s9�9��!uUK��G�ӏTaHP��p�~��
ɺLm��0 ���adb���
��R�DS?�`=��b�?�J'Pqt����5>=�bHH*BH�''��^��d!�0 g�MTr���9$W�C����;(�ie_a@�ׅCeQ1�j�/cH����u��2cbHR*�x),�+]����܂i�D����g�E3 �m��j/��`H�
	ҋG�S|T�A�j�];���G�CBl���d��Qp�!4c�[ ���:���l&��T:e�X"�_�kC]��s���.Q�jʹ�iC��=~��Û�|����o��U��B����(���;N�0���t��U�ЬƇ�s��Y�j��22�W�{+�2$�M�R;�8�������P�6��]�?z��0$(Ay��,Yrr�
r'���zA�.�x\U�!�r	��G<lvU�9*)�R�+I�Eݗ�$�˿�Ksݯt#�E#yN�W�r��*Tk�������"�51�sQ��k"7d�����^�GK��:����_V�E��Q,U�TM�/��U�)����2Nq�r�)��S�l�=z�ҟQIg�P�!Iya�����J�,:1$%)zFM��̟�b@L^ḃÖ.8*'6�uǐ��8�t:�ܒ�����;Y+	    �E�V[H����bǀ$������;����w�!1���4�����	+k'(�8�ȍ��E����}a��� �e552�m��O�
�ȇ����e��S�[�C��B1m���i�v V2m%1mm��\�z��&�;�L[�����MhT@�CY����ԃs4o�wܺe:Y7�;:B�S�u�3n�����2��b�hG�
�<��m�pl�+�"E�E��p�q���fo��r[�[�� .Z�Tkfɉb%���b�q���YECQ9��lk�e��b(�lW?�xo0dOY�馼)���4�����;�Y�]3u�Fo�1$#�q(��i�a�11nr���F迋ZL7�^|����b7ꭹ�����s��S�K�!1QLD�!e�I?ǳ�ѯ�tc$%6�k65�O����X�b�g�<v���Ѯv���2�e5Sq�)���a���6�(+��lIy��JdY�4�F���q��v)�1G�7a��qNҨ0d�X�1��i^0�vk3���e�和Ά����`į[�UU7��1$)��(Ϩ�JrGm��B_O�n��Z,+�]���*��7
�{����vWjLc+� �E��(���Z�;.�v6wcy�M/VV�kV�8���;:�3�.�\���[qT7{7̕���T$�Y;��Z0��1$Ɖ�����k/o��܍>����x��+C��[�!)k�2~�5���s�!=����co����M�CP�]	lFS�����Y����0l�!��]ƌ!!�A��[k��5|�Q�xk�eI�b��$�8l*$Ţ��3�%���O����"[ﴘ�ֻ֫x����^{�Ccd������D�Q��i?�)
b�[� 8hUߌ�3�ݨ��ɾ���u��6n�k�f{O��T�S���߾iĐ#r��?������6��
1$ˊ,M��u������׍�$���C�<�T�ף��/B�U!��.=��ွ"�!���Ԕ��N�=*�6�C2���e��`��AMb��H{����\aH�!���~ߺ�� ������q9݉fn�1yoDN> �#��gP�?C�dE�~dUc���U������7�w��
W��Vt7R14E�lҶ��.'9e]�u���AO��Ok�;⦻�~sry��L"E]���]�I�"�_�qRr�f���rPǱ�g�h?o7��~��*����m��Ai�[�%��8k�g�-�1g!ԝ�+	�"(�W�弌
:W�0$Ȉ�a�|��2���"���0���/P�B�G.��s�BQa��7Y*-=)�k��'��5�0�iЩo��i�䛺{Old�t��#�g��/ve�CR\�Rz�/d�OF�
CR�H�h�}�`Ɛ���]:�d+t<E�T��EΕ�x��
��$Mgf��\N�r*�Y����xALg�a���i��B��Q��ㆡ]�*�:��,f�u ����C��bm�Ss�QA��.� /�X!T;�/�V�bIqE�'9�4%����C��m`�`��`��v�+2}��:e��!�1w�)�BU��n�Sd�{AE��^�e�Q"��� �ߨ���y���>�SD�!�)H�bR���a[Q�b�H�����g�0�"]3���*��R��I��G�/*I�"�u]��ׁ`H�)r$/R�n���@]ZQ B��*'}-���oa*�� L�\�u��12�Z��	�sN��0$'��b��q4X��"���*Fv���`E�Jz���I�
XO�5�P�Q�L�e���G�J}��k�gAA���~��K@��U7la�bH�a'��K@�^;d�qEL�Q��q9�u�n��)
�������t�*ƀ��Hl~�א7��ϣ����E��]��v�Q8X�X�C����8jO�Ppt��h�)饖Y��f�z��[b����F��������.���p�h��#�י+1бO�W7ޅ�_2���B`���k�ìo��p�$	��P��M��3�B���[oB4����oئ9FáB]	�6��1�┕]_�����a�!!�Cs��RaHP��kYr6\��� ��b��%�?��+'�8���!IA��/'}p�31��$�Z"0�.��� �'�Wb�Ky�u1����	��1$(I��� o��w�n��n�69�z�7�jMӌ!)J�H�=|YoI�C��ѥĭ��:��9��Yݛ(��f.)�d�6C9���ά�N��Рx����a�*-����!A�*�mf,�I>��1�y�=~<������?��@�8+�nj�8$�RЏ�C��H�S���e��v����C���*-��^-E`�NN�a�"�5>�a����%�DL,��Z��`H�r�j�2�޴�N��%��
�Aw�����1bHN9�喡D��?�S�0$(A���5�g�&V�W�4��!!�|ۼԄ������?����wo~x��ͳWo��o�g�OϾ}�û���.y���0'M�]�e���"�Đ-B������`H�)B.��=u�n���+B9���L�m��~�)�x-���t'�dh�u��
�8��fX1���o;�Qʵش��bHH!���w�U������#�_4݄�(��iY�ǻ��/s��0$H�p�C~$�[�Eg ���2��2\�oJ��dd8[ٌ!/��E1$��pԃ��b/�(�Ժ
C��Ȣ�f3]��<�C��<��M�u`*		"�Ʉ?h��o����o�+B\�"~�i
OZKE���ʱ�/��R�D]�t�?�|���e�4��C�V��5��
C��L��rU�Oi�b{�0���2��9.@UM��C��RE�UYg��!YNd�O���j;�d�Qp<��p�����7Ɛ� �J�����Q{r�V)Wv���j �5i�US�,h�a$=ۅw����3����G϶�C�T�ނZ�B֞ޮ�i�*J��шQ���R&;�d��i�ꚬN����9����)$�mǗ�k��;�d�SBUq�W��ֺ�1$ȋ ַ/�ލ M|u��/����[�7*�i�pW{��E(�v�V&-��F�k�6��)����L��.�e����Q���7cH������g�>�~�!a��J�aKֿ�GM�J0���DȿQ�Qnx�s$[�!QV��<8���������n��ir!I�;�� ���N_�~)u뗢s�0�%5���u��1$�ΉÜ��+9�G=��n�,"��z�'C�$�u�?cHP*�$�c��|�ӗ��*S�>e�:K cH��)�7G�m(.4&;�D�2&)_}eL�W�܍6P:S~�lu��ۉ>ڞ�T+�Y�,^uվt����R��+9��M7�]���l�xc3Q���%5"�?��s(��B�A��;��uWa�aV��[U'���vg^��rb��K_i����:��l��>ؼ5CB��S�����eDWlQ���DT���@5.D���`H�!j0PC��l ��'2��sM��z,^\>����(:*����(�C��P8���>h�ھ����nX=U���S�? �%[�ך류��<̅����)9�ip#J��y4��*���7�Û��m�nC��>�ۨ�8�T��2�ƪ��7�)޾+3�5VVn,-j�h��nxD50��`�qs��Q�m!�c�ɯSFD���P�O�d8�\�6]Y��<�cHV���8�@�Q �[�w��W����aO�8*ݭ��kd�*���|1)?TJ������-f�h8�޾pܥֲ!�E��ɾp�SY�
C�h/6���O�Oª��T�S�졘>���·�_�f3?�kCRm��!I^���xH����>W7�T��,<�O�Ge�$X��l��lv9-�����i;��%�C}̡�n�֠��l�l?�$�}��t��I��g5FO�<M�!AZ񊿢�6�`ǐ SI��k���dE�*R��5�RۚV�;�d�R��̙0��._������eT�=�x�.��t3�pG�)뇁���ZS��~�߈�1ۥ|��HҸ�|�.�cHRI�K�Cc� �  m嫝����-i;�-���?���BC�hM��+.,���C�hIV���V��l_j�ƣ�lN���ǵ��7�7�̦NF��`@����L�u��k�cH�/�t�1�������f�^޺���1$+�,M���E �����?}����,���?���x"���=���]kwIQE
��KR�������l���+�r����&
76�v�����,�\��$:K���HO�����cg6�0IeA�,����1$�A�5�~���Ύu��Oü-O�3���&��i����)�����a;�f�ve��%�U�Ϛ����(��E��Z�Y���!�w;k-cHVF�a'������f`��@UsI.I1�6��j�=y��Ob���x�aw�(UsXG���w�!)F�r��������<E��,BL�Pu�j� dIre<WoI�ڈQ��/�!E^�Y3?�S�KaI
2�O���W]��0�$E���֛eQ9��ZaɢӠ
T[�e�Q|���X��(xȻ[y�O5����+��|���ˈ�[_Gzl�G�o��Ȉҧ��A��|aA�5uIP��Q�!A�l0��m��� ���f	�"$]ҩ7�F�K*�R�L���:kݣ��(�b( ��+�,̌!!I���CK��O�S�x�1���/{6"^9�\[<gǐ,%C��;肗5�s90��Ёp+�o�?||���׏�~���^x�8$��b5��CR�HM��>>~�"��*)�j�T^��{����g�������7�=�y|{A���1$։X���?BC5�͎!t���A݆�h�9��Buj�`����z|�������~���?�y�^��o?n��������?�������c��}|����7��ˏ����g߾�����<>���������k�K���ݖ/v��������?�w��Ųc@
�ǚ�8X�i�`SK0$D�P�O���cH�.R.)��E�<:vI�#�0�])�D���2��ŋ�GI���n_5@v�ST���BR�W��(����H��R|[������~� �>��_��*�Q��:"s�����X�!1I�H�j��_UMl͎Ik�.5��Ꮮ��;���P-Z���G}[:O0$H� �oP�9��Q�ތ!A�:����(4.�C��x{�F_�MF׎!)N�\���]�]CR|�r�6E_Lm}N��� R���Sw�'�0�S`�=�j��d��v�d���X[�F6Q}�teD��NxQ[�#��كF4��ϝp*	���բ�pd뵭�(�E�6������zǐ S�n�����G���dE]�������w[���!Y���^(�����"b	�2(�Ƿx�u��|W�}��%Mw�m�?k��1$�N��rd�X���Vߍ 1K�m{X��	2�qǀ ���aչ*�߸7�wZ%'�y�Y����=�C����m��+�^�w�w��mK�cH�ٚ��(�������<�o�,��*�Uҩ_�X.(7�Wg��:i�JѼ$���䋠r_��%�ph�� +�c����C���Z+�����A��uk�F�WKC�������vӊ� 2H�ü�"j\P7u��ԑE��k��)�h����%�K�ʕ����a�ɶ)y;�D���/%yv��襁е`@i�6�gp��q�������(Ɛ,:#���$k8��޷k���cHV���2Kr�w�~3,���1$.��K�1�V:�0$%){V��q�}�������M.�M.��`�����o�������n��?�y��7?�z������<n0��񳻩��.P��k1Q�M�L��+aR��pޒ��9qc�	-��ڜ��Z,ֶ�?���
�X�6��s������M<+�)_�\iM��Ofbۨbbn<)>�J3Fb�_�t�}:�6r�0��4�	��h�]�����?�D�#��_�?m�JnA�!�lc� �x�Wa�[%�Y�`�E����JGTf��_Z`-͐+6$��"����+���CW�ƟnjKo�C��H��AQ[ʩ��d�������;06+Z:V�n���ؾ���I֒�햗O����F�W�d��IE�qk�0����Q��Q�^��!A�*�"��揚� �D�")i��C����W��(�����ȇ&��1n���0cH�j��JbI���ZKO�`h�*	��r�����n�1���"�O�`c�	q"dkU�9�W[Cr��є>��%9�=��� oK�;sL~�Z�6=���(����-��Gu{�2��"�4��#~�6�{ǀ 
�uR��RO G�軪���d)��\"�Z�5���5���I�R�����/�=�T����`H��SB����#��dL��>l_n\��!i^�}���}�1	���>�h� �Rt=Đ�(�>��Rm��!I�H��5�%��DQ���V��h����D�'��c��^�!it�W�9�����B.B	2"�%�u���ݯeo=)f��9>-C�S���kM0$ȉ EGm�ԭs�n��C����4"*;�N�u��[0$���(we��u�*�E��T�j���cHRI��$����7��,�������%Y�wP0$K�,):APj��!A����E�����	�u��!IV$�qi�Z�;�D�S�v^����C��J��~7�m�-Ŏ��ab�5A���n�)(l�K���ar�7����k7(�3U,Ŏ�b��ғ�Ғ :����R��?�|$m�'�-��7܍VB��G�a�u�m_t݀n��m2/�Z>��x�zH�C�*˅���
8Y�!A4�r9�x��4����|wc�սޑ7��pSi���1 �L�A]�5��1$�J�o�IݾM;{��Ȃ��`�<[�{7�`@���hk����CB�4�.ŢF3t���mWc@�$B���Ki�����c�ٷ�f����R�(�1$&J��Oz�9��i�S��4z-�C�W�P8�t���j�!A^�}�e��
�Ig�zD����	��s��
CBL�E�>��6��s��
CBhʪ�'C���
��T�\l��N��dP2F�Jɏ�����(��C��ט��G��"��z���6����zA��㫶����'1$�!t�]�a�����ۖjlʟL'wH�1�0�G�	٦�~�>ͱ S�
P�w��,Ӭ�C�����E�r'�O�!A��BeK��1���ǞVb�j�SI�Q9�jH���#!U~c��bc}s��������U�s�ݬ0 &��0��0��JDv7i�֤��5[�S�s�LSaHLF�H1��}��S�q�A�ק#� -�����??~��X�*I3E=��C�O���[�1$����.�	���Z�!)�H!�_>��c�*�9�u����7� 0��      �      x�Խ۲�J� z]�)���X+�g�C���T���A������������L@E':�j��G�YK���cd�8��1p���o%EC뭉���u��m^�A�1]���+�Q���/��������o�P�f��cO̭��~���7��0�b�����~�?��8��<r�Rפ䩸/kԪ��Ow̉��!/OShv��q�F��6�x]:�[}G�w�F0�'��D���cxJ��h���s�^O���=���~#%H:��J�e��wd6����(�j�]� ��v)ܝؖ�rԔV��f�}l{��AP�'� ��`o8��A	��h�k�
��
Q�9@�t�Rmיł4-/0D����Df`z �ya�j_���d������E�^���.W6�,jD'+��6[g٥�)ǹ��Z�6��Ak���O�ȃ��D�3�[Y�yVd^i�|є&R���7GVh-�m<h�������#)�c������Ȧ����mJ7Q���Jj,�q��)���Zř�����R;؛a�^I���>߽���
V(���`�J\B�pj�.kW��VA���hb�-�5�97�-MT�x���kR���#%Qm�2ߪwu����y�Ԯ��x5l�_���E����8��;|�����qG�ϑ���fh#+雎����wd��mfn8c)�D�oU���Z�w�`�K���������AaJ�F �.�.����7�bSi���`0d�V�5���T]�_�yd��~�G�	~��'2{��F����1Y�AY�o���׾�\g�\s��g3{bMד�c�Z��paY[����F�,�����WF�=~��P�X�K����+��ZY+}�ݼ3B�������N�'y���@Ɇ�D�<�vgF� �����="`z�3�d�#����X�c,�щ���c���'�������m5��ɸes>���DL3tg�X�X����C���y0@��������d�c�O�F�`�!6>E�$�/O���̭hR���"�"Vz��C}�>� lp�'q/�c���J����
�����`���
���X�H��u�f��q��N�hF��u�ۗU���갨NJ�y@O��I{M��<q�� VD�'f��$ɐ�"�m%�݂�j�ZF�`�0��r��c��]�m�ߐur~�Ѻ���2F���d>����\Il0hkj�)�J����d������"�f���|��B�(���&�4�VW�ꈢ�{RK�~4�RW�u��
aH��-Bi��(��c��(м����6�w�Y��l��P���<\���$jtN56�ˆ�oMw�qP�xg��7V�́D��N���<�<w�+��{H�B�F�-�'�g��o-wlW��ȃ�Ca,�pxL��P0���ݽ��gL�D-Տu?R�M9��+�i^��U8�9n(�cܭ��>�"Cna@��Ed�P���7l�f(��OdHF�Hc�r�;;�,��d�R�2����ʒ���Q�x���a��ads`@���߶�h�Ğ��e�0ׁD���ؑ�n3�0v��.D+��ۣ.4;��U�mv�(�O'J�"�/Y��/�� ���|�G����G":�W�)��CT���%�1�N��j����=�VŵV�ǁ�o��[k��^Ң���,�/��8Z������_֮���T"o,���o`z���
|�m#� ؇��UH��'3�&OB�����7��}_���� ɛy.Ab@�#���X>�s�܎��oJ�/�J[j�/Kg�>���W��q��z�o��/0Wk`$�75���-�{�3�ps̰�@[���ߛeYl�y�������T⧻@��
�9�'I���p*V0�G4��hX�,ZlzE�c߁QG�c�\M\"���&D>���;Ų����sfC ��~
5�Y������3��l g1��R����"a_Y�[��W-�&V[ߪ�m��u�m��:m�N������~Rt(Te�)W�Bi��>Q0.�,PF�a�h^�f�?����(̎�Z��ul���
Μ� ��B�[P�5��0�~O_x�^���(�C�����H����B�Z��:����eǱ��D�n�{k�M���%����Dk r�,���ێy������ n����~��b�JM�*~�]sn�X�����,��-��7E�y�/����!�ДZ�,1M�@c �G=z���U�)ab(�	
�����(�ܷ�����Q�RA�����ƀ_�CF�ʃ&�
�s��ri�U����9�#�X���Ҁ�xL+�4��������<�N�8oh�u�xֹ��mo��(Xt`��FN�Fu���}@�e�����U�~��2�B�&`]�]�9��?�{s4�9��g���_�ab9��@^��38������ǒ;�?�0c���F�ö��-���tqU�gKQ.��%��&�9SL�4�����`@v�Ed7�Pv�E��G��'����n��7:��r�V0c-��-vبK�lw��co��Qu��"��ؽd�a@v�+0�m$S�g�狚*@f�K�#?�4#�)�����!�c���\���Z����?�5��܄�W�5��� �!k�K�&���l�
/ �Ë*����2�g����4Y����o��|`0
�:y>њ��{b�~t��!:3�[G��ǭ�&����m��FiK*�a�O���@�~�D(���rN��b<��:�E��M�Z� @n9/�ޭ_��{���@�C2�����{+�(F׹�*�Qx"t:��}g]j(|��������n�򒢀}�[�0�(�9ۃJҌ����P�@LS�+;�,��}�2I��^v�0t|#�pc�i�j?Z,���:�ХҢ|܌�������}
�.<7&�s{̓��|�C���9H�A�O$��|�A/�
ٍ���,7��=�5l����Q�J�*l�uF�H>"G����_a����{�Z��4��;&� ic5��q�j�7��iQ��b˪�؅��%7��6�N����qG_X�-�@���5ĳ��S��i��y �rG�����ކQ�.�һ�Kڻ �K�AHB�d�1�wp�<5Z�.�vg5�%n�z�x���y�yE27|�
�0VD$/�S��r����%Z��3��t|�oH0NJQD�I�O?��~,�3)�䚘�j渐4�^�@5K�d>��e��רZ��Dtwx�.Gn�}78.^8FŨ�8��(�e�|�X�1)[���̅��~!�e��Sޕ�`�Z�{T�P�"/�����F)O�b��<RJ���#2^�WHb�gܧ�Q���3]��=���5{8TG��*o�5�8ļ[��b�#S_����$b�H"��$���,I ]� �3��5����.����X���n���[ze^���~���ծZ}�9GP"� �ű��.�0� <Kv�Z �a.�@Z`�L�R
��	T����m���	`�vk<�?������Z���yo���h�K�$,\Q�c/5q���Bp�JƘS�E[Єuc7\���[m���bv(ד��lݨ��$Wc�`@�Пŝ�r��kҨu����%������I�/	By���H���~��x�����8�1���fo�����z1=r�^$��c��1fTa�N��<����L��}2O�\+�,"��!��	���F=�kz#�MT���"]�/ބ�R��,W�Ps�ۜ��x�y�X�J�N��n0��V��4�˃�D�{U5�S'�����������~q�yGˁ�	-�W�8@��&�W��бp�;#�+��'t�
E���N����Ӱ��Ǒi�<&�lH�`^���#Ӵ#]���PҘ9�䵙c%��z0��;��6�|�)k�t���:��2Áْ2�>��=`�?��|QS�4K�(�|�/�4#�9c2k#~��ǲx�6���q;����=���]�VX��T<��`n%��4��ߋ\�5���-=V�)��(,��dx�j    `Tl0��Hg����V�m?PE��OV�C�ͱ��IP����n���+"���{�Hg^�v+���@}���4F��y+�rdn�;���e��bk�Pݫ�����9�)��,+:}xE�c��-�X�R��.���R�'j:߄�e@�;�Kx�*ݎ�%���Ǝ"6E@e��on�<~R3���k�`Y�J#,Q��-���x�"�W�^p
�׀p��S%{
t:H��ؠ������\ڎ�qO�f�M�LO�~]�p�t���P�_�A�M�<k"�o]UV��Z�I�X0bl�*[V��_���9���uL,`r:?���"�f��,�[?������}��������M!�����q�
�@�~���y�Ż=���	s~_��ر������L���#������ �Bl?`��)P��c���t�Z�lߩ�q�:Kh��^��=�O����7�������?���p��gt�*�2������3��Ī��"~�T썺^f�W����ɦk�É��tE���~��N� 1��|��Z�zQ�p�Z���V�5�|u>�v�C���_\G_F�O���$9�_썼�G�ڞ��@@�/��{ '����砕w�[�*O+��ﳭ�} ���z��'�vu��n�n��`�al|���S�-��Ot~���A�ơE*X{x�Le2o��a��n��c��x�J�2,O����\D0� ���v�b���̀�1"������ߐ����R�@�mdzy,���%.���F�cd����m�^�������M���ۋ��2�$1��<�+���f�������?��3)�U7��h,q�=��P�z�H�8���c�˽�c��r��$�DMn�D�_�	mY�\��S1�'J���H:��$G5�va�e͞��_���{;ׄ;/�f���?�e��_^�pK��[|-?k��u�g�YJ8�c�`o$�]J]7
�V�f���~a_͹BL��gZh�n�C�6P�@���b��L�_��~��B�dr(�Ĕd?n�&��0�)�f/Q��d���_�/`_D��M��@�>����	Wk��f���n��U��#����{ v�������H4s!�N?�g:��|)�^|��v+�W.^� Y�����er��`�	OcQc�i���S��Yn���2��z�_�ǧ�[л�8М"�i(���8��΃��h�M��#5�S�����K4DlT�zS���bY��/6�8,��q��o���������FuQ�u�cK�O� �4N������؄���H��6�T��˾�l���xP>/l�8J۳������m
4��4��H�e_��=f�4:�ĳ�Noޞ�DT�+^{�i����z|ص'���0b=�s�\�����p*6n��T�g6���/E�;��I覆�_UT�%Yj RKh�DM�V��V���u_C֡��U�^[���[����/�g/��/~G�2�x�5@}�w�bP��1.�g� )C�zl����i	x�=xAUt,=�NڤT�ٷ<��#@�'N���2���9+
_�!c�X��gi�����>�!�],!��9v�0t�J����~����	X9�j-�D����>�'F�[�b����A1䏇��f�텹c`^����'���Wh!͌�I�y�7
�����o��VfX��F��%��L���������Һ���~����B��q���`P�98����]�ġ��E����g��?:�/�[��4����?�ž��zd�o�޿�O�Ð�lg7�7
������Q�D�G#~�8t#�aS�KS����f_[�wG@�'��©R�&��|���]m8�+����7��k��wǯ�}�]A��T���7���ԩݰ��vq�����Z`�PK��s��^YW���Ea)����g���������w7��)s��B󿼍��h
m'��a{����oP,�����Cܬ��ݛՃ���^~C�Tv�>���(��o�'�y'���^o�ψ��\�o+�E�>!��QTv[�#���9�`lv3¯m(`h ��q�P�[|O�z�q�����Ν�_�S_���l+x:���K;�f~[�c���:�K-�A���@
m�n��F�� ��K�9�U|�����禿z�^n�76	�=�앍R����T1�K���G�@	��N����1�M�B�`o�M�l.#x\���qu;EPK��.��؇+��@�ȹ�w:� �}�G���3���c�p�%��e0���bx�������Y�8v�c|{���W���l�*%0
��8�����8_��/��������S�j$7�7���N7/�s,����p
,m?�(�>M/J��E��1�(Yx�=?-�'�R��}�nNao4�G��z��V�8�8�0��=����s��[�t�������)٨����v���ZY�%�-��?��\n��Jx	i Y��0�O���y,�k�) ���;���ć)P·���
pY�0�9��V�����������\���\��
']��E�C=�����οd�����'����h2�r��*�����,7��q�`��4�,c�[6�Q�uyi��-��2I{��cF �Fn`?�W�ST�
o�1����Ǎ�g����Ѥ!���9���}�1K=�7�6kU�&����;�J�;S�BRD썎=�9Y�j����!���jWCI�h���`1���E�������E�/�F�g#�u`�B���U��m�/���������7w���_�a8N�2] p�(�M�c�Gu7���k�4e����Ƥx<�Fl� �v0�l�VĲW֒�
Nq͚w{cٜ Xm�01D�-�J�<N���s��,C�,xc����l��t�Dr�h�ҭ��'��F�k5��Z���+��/����B0A���h�n��˺�f�S��!�	���/�1��fq n&�5MJ�\��9�ȏI���x���`
K��;���Q�W�D����jk4ۚ�EK�k|9"��i������,�1�@���f.���0�'�-^�cO-�R�˚���Kn(�̀���N�Z�$z%;X��E�����_���%Y����a�V��@�^�����!Qz��e���e+�qs�Ov��q8��˵ԡ�"{���<^����޸ܲ|)׋kr\� nx�c�t|��|%�%6�5�[�Y�,$5h�Ti��l�2jU�j����Ve��X��A���
$|N�:�Pt�����dA�u�j�&ɉ�4��!�Z_�o�$��D��G�Z��i������J@��$��}��c`��D��@��I6���L�A�IV� ��T���4���c��7[Y���cc�����X�M�vTC�ՕZy=�q[�����࿌����I@��=����Ќ��uYS�\V7���8��IR�4^�u�E��x.��,a˰��e����޷�9�m��B��K�'�N^�0�,1�7=��Bw��^�V�_���%/�-����J���>v��:�����LF�?�Igq0�I:�O���jl"~;��D�nC�ޟ���a���S�&���i����bLB��Vҹ��1��酺sƝ���9��q���o����!L����6��,鼆t[z7NCU{�܅� o�T�Ob�G��c)���b!�b�{�aJ����g0D��+�۫����������V��,Y���K��������r�{7����8���f��?"��O.fq�&�j��s�	��ϣ���lv3��P�������|5�C\،՚����[�h���:���f�Sq�9�
`U��)MR��No7��I$QȘ�''">���E�,���Z�<��_��̵~7����{��j�+��w����h|���f9��ڒo��lxx�̓�1���ʍ�i���$�ua���<��j@�í�D���ZI��b�~m�R��P2-j_tYJ�s��u�:��*�J�x��&�2��Rh$-7�������@i{cb?NeDn�*�H�%.m	oG�-3�|�+��    g>�wM{g}���s��>Y�	��l�L� ��mL��tks��GƂl�d��q���1,���������1�[��&��P筌�Uj���vQ����8����F~�@���/C�m���욢�S���g�l(��Z0��6�J��E]V�N��<�U���b�=H�bs`���R����,}�d1�*�'�����!���������j�m,��^F����u,�?��q�ԣF!)�w��]ePD�>KRsA��ޘ<霥�&եD�
�X���׻	,��Ĵ}��?����Q$��;��fuƠ�|u2�{G�B����f��e���US��a�����M��f/l0�Q�r�g?��pSRS��o�7�I
�]��:��$��V��
ENa��O�W�A�(��Q���j�K�h�N#���h��B�����"l�(5|i��ʃ��ح���f��BW�ʾܖ���y7�f9��*��?0/4��S�-���B��C��N���[j6�k�X(��v q�'NɓĲk�kV�YxBҤ/l�M�p��5��u�U���H�e�?v/��7��;s[4���U�`~���H�8��Q���4WA�Jݛ�6=�
�����`��T�
�����a���D5`�M����Ϊ�f��S/�
wIP{c�t�L����m�RЫ����X1��l�XX�Nl	]�۸�i��.�����/Nj�Śmc���6g����XcX�N}��x������<P���';?��oW=j��#\)��p��fݡ������a�����+�M t{c��U���r�9�N��<�Ьz�M �+m�	�
6�IC�] ���T�څVF¡Ui$�n'<��՚��#eI"?�/���z8͑����\3�"e{{����z{vs_g�4�d��{��<����޾<%��Q�u�3X�}t.5E������<0���y�5 1�Yf��|!t��?*uX�&(,�+g%�"e�J�Yخ2����	��qkA{������K��F��^�"���3�ͫ$�IM�+#u��c��.��3��I2,��[C��#Y�����I|��㗥-mL��i>>Y�6�0�K�>(u�;m*,�WZ�����hK�D�<�J�^�f�	Z��FA�k�{�%�Ri����ITV�@������z�n\��w�TO`Y
M
���� dc-�C� �m+��lG(�Ě��tۭFGf�Y=�4��17�76�$u�$�
/_>9�#�Т�r���T
d9�ԬU=M��}~���=
���*Ij��=����@�rt���Gw1��s�^O&/u�o,/��I�����������K̘�6�eY<+��5z�ׇ'���%�E[��l1j�3��!%~�yΰ7.��M,�3�٬�(��Z�Y|������Qd?���ݡ{8.�H��;��8�Qnx[��i�_��N�=�1��˫wE/w��3Mr��Mߣ��Tư֯���/ �v{Erv;Pj�fr�KAo:$h������5]PF�o\nM�O<�4��y<G��F{�/5�jK�=��H��&m��r&��f�[x�����5f�%`�zy�ڼ��I�>'��q��>/��.^d�@ΡD�]}�[�kI�������^�c�:���[y�G�ّ�Mm@�e~�U�g��0��0#�[~.ŌȢI|2��Q���q>�1�m��Jp�Y�68�;��qq�Rd�Yu8O�-��㦯�ׯ�G�:�����:�/_>A��iX��
�R&�m��U���v:�P�ݕڕ�vTi��:��K��$����K;���򸁽qyQw��ܧǌ�f������4�=	��_��t[d�z�ȱ�ln.��֑i�޷<I�v�5.��ZӪ�,��������v�����q������8#)�r,��+�y�g�s��[��֜Ţ��Մm0����>;��⃕[_��y���,��)�C�B+rP����t��9�b��_��A��m�	�Z'��h�:�f���;6_����7_A�6�����Գz�ܸ��A��C�T�|6	���'A�F�B�s�_�۾��4�-�����j�t�N�>���^����G�a �<�J��}����1���3z�0�oA�F��W{���G��ٓrI_*z�n�4�]	�u��T�{C�^��0�an��֘����J4P'�+�7��fl�}<�y��Mgf˒B8��9��U[T���c�Xy��R���jJ9�=ޒ���F�gfG�w�d9��4������I�f����-u���~<����X�ᱲ�n�`!��+x3�s��;O��a��s�˗Y�Wo�=�f��b�&�_�qmd���(�"�@M�5�PJj�ۅ�*�D�	��(y[[����Xq9��Kxm��tF��b�<ރG��%�>��*ڠ���ֈ��Iis0c�;|�M{jE�ޜW�*�%��TA�잋�0�T�vt�E�},*(���'6�c�̄����q�s�z+A�������,QZU$ب������` �\5(���8 �f#Q���,`]���>>ڥZ_��|�4�qG����k��:�P��
Nؽf�� N��sMH���fT�$	5}�Ů�AI���z�;�/�����//b�;���^������ǔ��#Ȥ�-@H�s���4gt�p]+��!*������M��WO�Yc9Sۚ���!^�Te�7Wn`o�T
,/����M�~&h������qz�lh1�Ieǫx�H�x&���Ճ�v_�(Z�����.4�_�H���gu����dRy0@�\��RW.%n��$���l�Ð4X��c�FY��F�Ȣ��B���E����PƌNu6b6K��G�4Y�<��{~{�Ͷ݁�w��n�RA����!���D�hg�4.�|�(�'��pM����H.��>�4Aqi\����`�a�*�K��w� ����>��(|+`�MM^.��rE�Y�4@�IwS:���ݨ*w�o5�S$&��9����9
�� ����L�S�)eA�P�aD'��|+]d��4�Q��"#�Pg�ɸ�w�L=`IE�pƙJ�`-c�m�o��-��0��8��?�
3���k���O����@E#f2|�'?}%�OH��/�JD3!љT�
Zc�c}�xj듁�lF�v	���I�#�|�<�;_+̢�}��g[�,�4�;�Q�d�$m�;�cf�خ��Z��-��IsH�����&�t��L��W3�\��,豲�Y��F>�>m7���[�^����$�M�^ҕ���
;S�b�����M�W��`|�
��dش�K"o�?��!�Zu��n��wKlE���Қ���m�jg��+�ْ9�0_u̢����Ɇ18�q��f�&k�����CwjϷ�y��sV;�6��ܘ��):꾂 ���F! ��+Ko�]e�G�AO*�#���5�:�H~��/��4�Pt"e���{
�o�ݍ��V{P[v�Ѷ�����8��Ǻͅi���A���jq	}	�'I���0<�I�+�K�ǯ���4r��[Z[F�� ��ճ�!;�(sP�5�?���gC}RY�\��`Hl�N�wg�+d'џ�d�u���_';��%(�{�I	��H�/�$���1��3�J١�<�+e����v�Z��iȕցr$%:Â�\J`��ao�t䫈��:��"�{1�EQ4.�+����%!Q��M��5}Gi����u}�F�Q[�.Y9:�9ܻ�~�!��\�9%�5+W��Ьi=����s��ٲb n���k`��M��}E;E��pJ&d�$���O3�d(L�~���橏�]���G��r@��Ͷ.����D�	���{V� �s϶!����`�����tb����Xke1���w��3��c��A*�89iU���\C�-ऱV��H���zКu�a�5C+}���ڼw��ʮ��b!-)��O�^�a�y*�b�h���+i���)���#e�d86Q��6�!e ��D������5^,]]ǧ���6�'�$w���X�@B
���˜�g4�    %,�W�V��0��{`�@�K#)Cw�ۚ��?p����Vs}��B�����ذ\s��0�$��a�i��u~W�,|���T��ve��֑����<��7�Xg��[w^�#�'qo�00GyVL�'����fm�qҥ;��%JS$�8���H��`��]�$^��ͅ<7��/�5��	��K�}�fֺ��%a"�;�B
��Y0WT���np���wq$�'�[�;b_����iӟ�'�[��:83��w�N��̈́A�N�Ǻ�6��k�R\��g�\�*5Mhޢ����|��tߑ���n?Θv��lMtH�5�h�?��	�r�]`Gۊ�qg��{�+��؆�#�+.�[=78^�O$Nb�lRߑ��+�jԧX�V�����v�AC�)���Q�����5U�8�doB�20�l�ʞA��(whȳ�<�I�{�h5�X�Y�W�	#�˓�����R�S���~=d��L٢��v��y��)�D��fr����,� ��|��f�G��N���'e@s��uz��Ns���|���Zh�E��_���a�@,O)��P����Ժ��3���f�䨔t�\dj�m/�-at��RM:��U%G�k��5�i��Dܛb���L�m5J1c~bX`�� ^�R��z�'�<I���)C�ǏfP#Β���.�[����8+��Y��?nZSo߶�A�X��<@-O��L�B�wS �Qc	`�&R�͠F��͛��2#�V[i���a��r�U��.�ի�^C���� jyjY�Yz++�g��$��R�� ��Og0��
���u�'Δ�v+ҫ]���`Vt�aT����^�La ��J����B"�>��02a3������1܊���{Y_�G�~������F=���\)s��м;CHa ��ڍ�4��<A�K�i0 ���Xc�N;lc��	��nk2�)�iYf�oU��4�������` �犌�u�[� �Ih5� b�����8c���Ԗ�Ռ���5K��ng��2ӢNW	���g�3�7��T���`M?,��x�N7a��$�_1%Q����j�h`-�{�����Ĩ�;�L_a�����hO6���0F����vyj\�[� �j��D�SlZ��L����T1c}6�5�����R���@Q�S�׬澹p���Č&��P%�5�P�D���E4�0� ���r`�&H2�|��3�N����.\B<��ҩ�[�mT��43�v�]�?S�W0$)`�\�I����pg`\x<�(�2)o��x}��!	�X= {6�8��b�%�R�&���'�i���KL޾bFa�5���\�I0�,�^��OOn1�5E��膖�A�4���:�M�u�<���@�5u�����'�z��l��jĽ� ��u��_aysX��#��/E�G!�[�|[��Ҏta�mX��|ZCk;�	�d�s�Fѕh勫�+��g` ���O���FA.���B�b1�v�\&iC����Ʀ4�l�üLdv��D�'����������+�2@������cI>׃���V}���� i�h�81�2~�1v�vW�V!*�^ϔz:m;�Ig4GktC�(R�-��We>�]1 �\Ҭ��~�&�'�2ʐ	�ŉؑ�73���Tݶ��.E+}�/�갃+Ǟ�L:kT�j�-���_1,6�&Q�+D@A��v {��!�4� 'c�<�A�3��4�X�8�����5������Z��U��	g�n0�Ȱ9�B�^�>ъ�Lw�l|r��ӰU�D��oÌ�@�����O�hY	i�Q��-����6�u�N���u�^3#�� ���t�ζ�ߌ)�rK��D҉�� �M����N�4�N�Ԗb�l�T��N��Y�4�� �7��� ��(
\�(P��B��"���x/�t0�'J���¸�T�AO\q݅�f��tX��)
�u�Ry�X����0xI3�720��'NEi��. =	����(}=���(~ap�A�xBwjQS��s���K+�*}��B�'b�W�L�t�΃��kCp}ZA�l���Mx�xI=�P.Y��+�Icn*�m�);�ӕ�E��v�hrԆm��.*���i�_C���� �C^����O�#9�L,k*@+3c�Q��h4e1oRY���v'���+���
��a��i����>��c@��ǌS��� �i�`՞����!�$�k뫺��U~m5T�m��>����ʼѐ�W��?�x1 ���c������/�e0�N��T}2�dPfF��Զt�S
�wng�z�YFU��������+>/�&�LP~�������'�,���Xx�㏳k�5�����RsY��W�l�����V�rW�W��F�.��+���1������YQ�u`Z�0�w�G�h�4,'J��+Vh�������w0��Z�t}�t\�\�<=>O��64ᒚ4�'���{y�\�A���k�.�V�y2:�.���X'�8���p�G
��M@�Ff�8C�׋����Zy�x�|ܗ���qM�X3,��"���:*6yD��dn�g`o���	�p�U�MX�hic6T���D!i�L�ያ0r?�vi�J%5�YF1ڗY����H:{��'�B�ꨮwFx������L:�P)O3���9����.�>9eK�w? ԥ��0�T�7���C�o��Q[��b�N����4�k�N��:4�n���P*7�JZ��&W��M�p�qA���L�T<{����%K�,M }*N�z�1�R.��d2�,�� �f��N����Z�[�R�)�Kc+t��(TUŤ���Mi�`�`o���6��'kkU��W����[r�M���0�[���ņ���R[u�$6�X��X�߻j)���J����ϴ��Þo��}I�M�
�%sC��MX�_��JK�ڮ����OA�j�`7�s���
:�r�7�VW��܀��9u�L+(�rB��[�_�Γ�[`v������$��ď��z�8M[����Y���@�h�fӚʝ�RB��K��\�����$����aW�3�
�d
�
2�Ȍ�po�M� �Ƚ\���w��"(@!'�����O��/�h�p���J��l�Zm��u]���'t�T��N.vk0!��ܩ�� ���	@��+D\���k#�vO�1�ˇEa"$.��w&���O��8�%�����3.ː6��Q�<u8�"5<���`��TN��n���{�j�zQ��9"sy&�;˾�^Ձh�	J��Ξ�'K��s��ˡD��-�P�H��G�d���qX���ac�l��6"��ա���U�"ݞI]a�HyFO:�:̪�Ԉ̍���*B��?���dRfݽ�]� S�UҸ�_�2��a�u7�p�10�"�̵6�5VeV�ժ2!�q��aK�X�vX�M�$��={9y���M@�H�gk]�uS�%x9[n'�@T��ƅrn���13A�K�-��4�o&[x�������?��TUI�����O�$P��D5��������T�����(���$�hv]sFS��4Ė�V�t�VoL�C-th�~��n��\���Є�썓�
{�r�D=���ߜ����z�O�p��հT�Mj�S�!�I3zd��9��̜0�Q���KQ�ȁjq�FR���6h����V�kMF�+s��܆�����Yh)����a�3���z#���)�3΂z��<�H#�I�D[��91�XU��٪W��zx�3Ep93��(I��������Z�:Mk�t�v��/�f�uXsC�*�J?E�з_��9����0&�������H�4ã��
MV߄��PU5��Ձ{��M<QJ�������Q���qC����/Vn%�צ%B)�,���"�t��]+�`ML܄G^a��y�b��u)S�.���)��7�
q���N�zϏ�^1>纾1ݮ��A6]�Y۾V�H����b��I��%I�|���[�jWN�����x�;>�e~�b��+�����.)w��@��f+s�E�6�ZJ(��2K�0�fS�z{sݢ�N��'4�+3t{J�	�헷�Y!zz�8�d���c�+,�<c� �  jտl�<J��Г `?��ڐ��t� �h��������ZeHL+L���P�}y���h;;��Y�b:����-�Ud��
�電�W q�)�%�A�:��d3Z���P#�tU?�8M���Q��+ܑ��j�,�7S�k�&_��be��о�8p3$�eo΍�0@�ܤ��~��}D�>{I4� �m�Rn"���eڽ��}�*���DA��d��EJ*h�q�Gq(�߈�Ae(GQŢ{.��<ݣ�mG¥m�]Im�s���h8�	]�^��^߽�a�r��e��m��T�Z��$�[Ch=B��m2��G��Obw%p<Gw�4�!cx;Nl��\�0����B�4	�b{�a+�Z�Bt�G�+ˌK�u�0@�<�5�4`��g���q���=N�Q@?c��q����wq1,�r���^zy� &��c/�A��e��*v��RaW��ݙ�)�!��k*�IQ
Q�=D��	!Y4�u����ݫs��-��rmlϫ�X�f���IA�
{H��a��hX�f:J0#o�D��7�ͳ�2���U
Cw���tc|��C���$M�D��J 5԰.�ӞU��N��PN!�1�tv
�����
�{�j
{���j��`VgGq\ijC�6m��"�KS���w���ן\y$9���ʀ���#��Թ0�_Fg1��
�"F��'��%_�=Q��u���#WsZ�|t)�؛�W���Y=�zϙMk�'½����Ƕ;C �%d�����,n,��EX+�#jWnVhb=k�|����v�qK�O�a?x�8Xc����Y'i�v�o��!����X֑�DsI�aQ#x4� �:$�ko<9X%Q�Ɖ4Ǿ���8��$�t{���ݛG����o � �� ; �@S���C�%{G/�-�l��k����i���Dy����-�`�
Q�k)� j������nm��)^����O.:�8ñD�Q]W���*���������)�nk\����v�lf�7^����N1���T30@�܂��r�m�_ܰ|3��vh³�;�/�������_h���by��yȒ��0�hf�#0jb��3eО�������fs�5B�ɚ(��9�7fBh��Д��.S�sn�[:g�KN���8��0���A���v�ǿT�h(��o��X�"q`��t��H�pF��F�Q����\�b���&�Ƀ�f���H	pb��-��� aH���!�l��~β
3���nA��.ʡ��4j�c��3��
�TR:����e�d����׫�(v�=�\#�m,��}��k�
ӬWGP �\`�� ��4�˰���n3�&��k��Y�f��(�f��"��#�o�Xs:��d[���S����c���)��	C��Z�u��y{q��skTY^{���?N{�r)���C� ��f���b���/������%\w�^O�U�:m=��H�^kv�S �=�{�@���<��妯���Z����~}�����	g�>�jTK�P�]��ʬ[������^=<��:f����b�g0�01���f���s����X��|����ju� �[����f���=w)�Z憵<�*��c���k�g�\g` ��<m�Q��>Np���w�s]��`�4���t��r�ksghv�[�z{:�ֵ� t�b��S,�l��`��$M��y�s�5��'GE��s_ΐ�0֧���I�١�qК4'Gj�Z��r��Sy���֩X`�]�˃�f�G�����'g:`�Ӫt��Ѐ4���ެ�N�	����}�n�Z�,"G�1�ժ�D����_"�)4��6�h������}e�bI�#lr��뻃,y�焓��uwѱ��B3�:���WN��KŒ�\���ɉd���v��? ��"˥� n��`HF�`[ڴ�T]J	��ņ:M���z���z�F}C2��� �y
ާi��� {�Ϩ��`%���$&�1M9�,�d���M�5��x~�ׄ�JDhî�j#q�:��4����M���"�i���� u��FBf��F�wg���J�a�E�,�����|��6���M�y�i˴�� ˆ�e`�,�J�^��:���ȸ������4�чw3xs�@�>PkƱ�I}a׍��|C��ڊ8�FV�-��"��IQy�7<��ع����^K���j��z>\<< [U�vI(Z��<��0���$��O���D�B4�������# ����zPv��;��e��R�;�&����c���)�2��O�^:�00�y��%�ᦠ��G0��#��;��q�,�_.���?N�X�j�٘c0f�R�ɱ�o���;��f����H`K�eS5j�,d�@+σ�s���OT6}�n4`|��Ly�]����>	�d����8�>k,�Jp�䣣ơY��lw�����u�j��^�χ�~�Yv���g���գ�q_�y���� �p�ь招|��gh:M2��}�x��L��nu�v}�)W��v��Fk���}��+wXS�bU��Ya��z��|�a─9s�%j�ׂ!���#(������+���4Ő,�ع�Z��g��/{W��j���a�ҷ�Ţ;��XIx�T����3��k�z�o�D��i�+��ߛ�%,%��T�A��)o�k�~��C��`	eW��a���h�I޽Vx������z���f�(s�C��nջey��3Ow�ph)[�ӜE�F����G�i)��A7k"I.q%Y������Τ��9�i�]�/P�2�_.1�W�ܺ����'�"͒
91ʁeɐ(�eh�\G������EO?�z0i���h�Yg��C-]���*�#��dj����hV��˅���6���g�g_�5{���c��Y4QN�R^F��[m���&%�#U^�u�K��0&�*a�6:�����Sl��1Vi̬�#Fˊ��w1(���O*o�?I&����8��(�5<w֮��y՚4�^*��&�u��" ���I�f^��P�����Z�%�z���Z�#2�Y�u�^gG��x�X}������Z�K���[�{x�7��Z���c��ta��w͈ss/�4�]}Mi"%{G�Y���#�/��g=�S����>l%C��$w�+��V�D��F�Z�*�jˊCz�nPڕ�W0�����f���'�/�u�ype����2a����������Ʈ�7A��t����Z��YZ��%f.$l8�D����]� �륽�O���ӻ+�F��Mr����<ߎ)���� �q��˜
3R��n�i�axy��8��3%n=���P� s����b�����8���|̗�`��_n(8o��ȶ�J�-���`�Gg2	����]'�|���i�dr4��By)2k1��j�>��1�$���^���_�R���~�d�O�����i�h��% ���� f�ȋ!��s!Cm�D@���28ԟ��GJv�����z���ɓ�Ц�t�����>��M�K��  ?���      �   -  x��Y�n�6}�|�>�)D���-A�vw�� ɾ��^��jK6$���}iY�,ZN� F�Cg�s�3�3�;�ԥ���K��{|��Y�_����o�_��Ən|��0UIF�rʎ������{���)�	� ���R�V�|��uy�+J�HU��H2�_�x����ą�2\"�Ȫ��/8jk��Y��;�CJ�Ö����ݑv��{�֗��!Q~�Fا��e�����I��R9b3�,���J�0JX���@��5��֜�u6I�cϏ�F�gQ4#Hc����\�J�0�E	��e_�ES"�������{[z������!d��.��`�ς2����AZk�0h�/z�=5Ei�)r|BF<`1g�U�/�s?Q�cF\�X� i��ҁ��l[��Eo�x���.��bq�x|�:�!��e���n��
����)!>�#�a�l�����Af{_�K�i�l�OmB������������fJ�HŹ�,F<b�����<���hq2W��)H�.�x9����G,F�g	�	��JG�RB|���\��H��� %��-*�K�M9$(+A�$�	!�#��O�=e�*��*��5�G�G���G.�P9R��Y��02��^����V�?���hu��)EM��*������lP�����0����ޚ�2��#��ZW[�f�1g��2<�ښ;���M�0�#��1���y�Z7���|��8��9�`�y�i�wF���u����{�3�(?�C����p̴����#��a;��ccXg�?&�Mqt;��<6��r���.�p�~��b�g��i3��5�769��5�m����؅�9R2����^L�"C3���9�PďQ����`�iB)\A3�eߍ$\���O��h�����y�����q �?���|��~�Z%�9Nj�	FP��'��u쿼��ޡh,��c��|\A_����>�����A�r�=Tz�� ��yj12*[w���K��a6�'��\Y%�TE&���mW�ʻ��S�
�υ��R2��s�'0@;Oq����(Ɉ��Z���Ꟈ��jr��)��E�^����FP�ˈ�$�s~G��t{4.�#��%�c�s��Ʊo0�v\&��>m/��S�#�FЉ+���N�9�`չB��� W��?7(�(kS��ϛ��Ȝ2g�0��3��Q]!���f+s����{ #�-B�FW��w�)nB Fؓu�#s$�*��z�hT�-F�$�}wOU�p�cZz��[��A�n�'A]n��N��P��y}$S�1�M�I���9���� ^5��a��|�	F`1�7j	��)�������;�1�^��ɍ w`�Q�02�G��Y��X�{B&䵽�آm���x�Ff�G6���5�CsAi���(�>�n�#{���1
e(����<�.�ңZKF���L���Z`�Q�<�K/���Q�ʃ�v&}�,=�n0�ֈ�`"��f��(
.�Bd���������B�+6�^�cz�Q��s�TJ�0��17k%O�k-F\"�f�za�a317k�JՈ�`!k�ܬ�a��b��y���������v|_v�b!G�j��LaTwad�L`�x#�q?b!����,�����f��[E��������������E���Q/߆��24�׼��T� =�(67�(	I7��j�g�(����ѭ'�(.�bs��+\�lbn��sX�b)U�`/����I޽�b�d�����Y���Ⱦzxg�0T������Ű3���El�	�Z��(�������w�Q���N�fZi�E��1��W"�6���      �      x���[o�8҆��_!`nf/�A<��wv�;�3w�[,�7Jܓh��6��`���E)-�MQ�Z 3�xGzR��d�,��M����J�����6�*_�U|���\���O���D�wI�N��43�̒§E�*�9�,N���z�V11;���LJ���
���T��_��rWH&�Y��@J�mI��|1���](��,���x^��[��G�{wX}^n��"N��Lg2�i���'���*�)��H��q�-��qm�C�\��Bs-�����b_���m<_��1~��ժ|yK�ɻD%��4��7j����-�xٸ�s���;�⏛��ծz��D��E��_j`�Nſ6ۗ��fY�������?7��*����y4p��$�����u�D�|�z\���q��l1��O�L��,�Ͼ�Z�{�7꽔�4�dcT!����K9V��ǻ��D�n
&�������<M��B����1b����r��V�}��5΁�LY��ϑʈ��ש�k�9%>DY곻@eg��Su�hh�gw�(�N�4t��?h~�*�ǧ��7�3:�aZF����e��j5P
G���Cst#�
Z����zo;��8<�5@���8g�h�3��iW�4tEpn��a��5�VC�E��>�=��Ш|X�f1;�0+mX��2ʚ�z�0+w���2���1��#?��}D8�,�]�3�|zҐ�=D�&j���t��2��tF4LˉVL�q̑��@FаS'��<4\)7����Z:샷橸�p��c�3Y�4 �z0�J�����Pπ���48;�>J����n�\.�_C.���A=q$h�ۋ�����&rd�I�|�\;� D9H*~Y�ίo'Q4��=�N��a_CkA�T�o��Nڧ����x���]�ߊ�̾iw�����x��C���V����4��kةe=S��qv�g`�E��z��jh���P�n��?V�I	fMo�l���"׾0��΢�dfR���EsDPh�w�-b������vԔ�>- �^Z<�{D��b��ʗj�w���N(>G��)���h��C6L2}R��"��Q��-G(|Zd��,�8���1Я�\K+����ٖz>� ���/��A��2�
G��k�j����ֱ����*|Z��p�����c�z���x��9�ճ���qX����1 V`��Ȅ& ��n�= �R�Y�i{qB���Y�X�@�ϩ�G^�� ��2��c  �p�r|���+������:�'��*U �rN5Xpj�R������8�����e����o�8�����U�0��9�n>���e����?8�S��i��ǲ) �ocX2�guݗ�2^,�7/���}}t��"tYD���>h�  # �rC��E<Dy�������.�.��]�Z��B���.���]�,�����r�yZƗ��0d-`��gS��VC�� �xDy�m�P̏`���U�0$s���h�̍X�Xs�}�5���-~���m��j�\���i��j��]?-�	�DS-8�W��lf��`�ȼ�C��@��<Z���4OҪ�ɀ�hbˊ��<-J��\S�������}��8�}-��NL#�χ�C�c�sbR���7���dIK�'߰�"�4+���[���nyFE�h�Х^=^J��q=-زF� ~��$mF�Ӳ8-B��-���#�M-Oc���(G���� �h���
�n&r"��3 )-��$og�=-� `�GA�$x�(A�M�1�mU8-J5� g��7,XU��95�*lo��jQ��QÄ���C��F  DK}=���sI;N��o!���Ő�}Z��U1p�&10!�>����(���\Jv��kQ������fx�h���T �5c�C���>-�,Q�~
��.��2�	CP܄>��?7ە[ԭ�����:�uˣ����jQ�;n*�������@0�����~�g���3m��}-������@�з&�͊��F�[U=-�!Fv`8�L	��p(�F%�bzAQ�E�uZ�\ʗ�r��$�p@��B�Ax\jZ��[�ħEyF���(��f�A��>-�B��1N��ĭM����hh�d_�����i��������1ǋ@(Bp��hg���m����fZ�H�d;R��6"h�*|ZTXB�����J)���̋8O1(`*��7�4�0>-*���/.��!� ���u��a�(b�\�';���;��0k�>-�I�8gY�I�˛��w��h?w"�^�k��%�ONS����'�i��9Ϭ �:\!���2~_����M)�d�Y�}8���CY�>��8o�%��M�.Ӛ�BsO�$���`�������
c�r�R���X8)0�#(]���ޥƆ@j�Nk�i )�A�ܖC�OL��:p�͜�����%�O���P靴� ����p��?G�#-�P���ŭwfՅ�p�<��@>�W��Q�g|0a����H�iw��nMЦ>�i=�z8l�ö�AJ�����2y�p�!���@F�t��qu�`~j%�"�mA r�L��a�@�7n%��"�Q>o��u��-F���Q0!Ψ� ���{�i3h��Rt���>l9�X�����M ?N9�P��O�(��ܺ	���ǖ�]��i@d��% ��p?�b;y/=-����!��Y
�`S��`�6��E�N�u�:AFzW��"�)���P���40�����0�O��l
��F�N@{VPm-�������w��t�A]��wi402A�3�a�OC="gC�zz��Q������O�Q����=-��& �E������8*�dB��֧�$��q��a�E�][�@}lG��h��B_�VIR�������r�X��io��j`�5C���TDV#�x\��QP^�,%}����x/Z��u�N�Xj�����H�p^�^�Bʧ�!#����T��嘑�=�N��SG��5�0���BG��5��p|�h;�40�]�
���E �W>��F�x�8'�9cn?��/�x�f_�O�d��V�BYsxe
��вd�I�u[��k�L��9�2@���2ю�O"��28�yn� :��Ye����4 ��-�'Oj�L���l1z�ǁx�y�.<�i@(��tB9�"4N��c�Yrږ�D}�.�S�ۯ��)�Qn�B��?
�>�a�g��(YM�b���U��5К|��M���2F����K�Ty��������ɢ>��§$ݹg.Hwg� �@rA�U���v������_��d*6(�.w4�������Oz@�h��gɛ:"�̝,�N��F(w�gpA�icm4��ٚ���i$�h�J�{7� J�9��i է�� �]R�h )w����GH�C�|�9�l�d�Q;.ȴ�= �s���t@�M�H(u�w�A���8 x�a{6�@���� �g�g�%��h�D�>؞M�%�v#N��O��ٙ�!��Rw���{v�qH��.��|�λ��-r��X�g��o�E���좻Z��"wJ۲=���R�<۲=/�鏜���,۳�|;�:-r��X�g��6��;,۳Uok��E�عe{�Ro��i ����G���C���q��Y��h��
�0������Z�ʡ|�q��qV,��~��oכ��X'Xſ.n��Ld�����p|߾����"w���6�Kk��0p�T2
���k-r���̂pE�clC����������=����5�eJ ?O���~#�n������2&�15&׿]������c�x����'ɯӮ�M��i��5fr����]��M��*}'�9���p�c�B�my�����g�E�	��B8�p��ޗ���9XG!���j@�������F QXu��������{�#�4�x�4����V&uL������z�(%�)�B8y����y�ެ��C��0��F�[qi���PHV0�a
#�XWߗ�]�/w�|>L6L�DJ^?UO��7�D>]�**�E����)��
`l�9���R�+�#������pp��袷��j�� <  5�BpE��|�a
�$f�����,�Հ�5�aL���Ȅ;?�^����\�1�%�{�f��qN/�ؚ��%Cxvƌ��|^�Ӏ�j�s(����\���k�8������Q�j��]ƌ�(����HC8�pE��>/��tcE!����T�[h5PlMa}� &uf���6���G�l?y��a*�<�?�6�i��H�$a8�F:��]>�^l��RTm��	R�M���>:���6o��i����C	��/Ag�l�rWM����U�,���S:�.��|0Y�a�.���;�[���r"��`���a[-R�O�0���y���^�������x�y_�c�{�/v��nG�a����t�����5P�d<=��70��p�i eT&�-����~0ia��n�U�����j��&��i`���+.�-]�yX��_<?g�@���R�q��.E;�S��~[�_�xQ�i�V��~],�B6��pm��X6v.��i��=ϼ������������ӿ�ŧ?��=�(�l.���~�����嗁��Q*��,��fw4`t�����}�� j�iOv�60��H-���Q�N|���]�L���_���IIJ߫����˻g�\j��]:�DO�?�]���]����\�̓���N�f�6��I��`n5@���,H��j���S��Em�w{����+�|8t�[2|=6�W9]'A�xʧ��F.x_p�h4xq��$�w40LØ`� m���q�T7ÿ���6���
0�:
ͳ�>��adbq1t~a��,����n*WG�4m��gI�B�my�����Mۦ���d�Bޜr,1�iCG�4�%
��HV��ي9m�N$u�y�q��H�ؼ ����Aγ)�#/.X6����ip�~i^03u�;ND:H��8��^��^��˗C��������5'p&p��n\���h���п�8\M����4P��i�^�TG5��*�/�]?�/U\0j2D�j�ٛu�w4P�:�� �pd�8.����������td����b�hhU��t7����k������D�x���bֺ����dk��
��JNW���?�U�0tei'����9��#���Y�(5�b�LA�N����d>-�t��L������HU�h$<����F�����d�Y?9D�Ȯ�oW�P=$Ӑΰk��Ã)�h���e����\�Q��ӎ�L��X'���h�FHN#�~E-��$N�����-b��ݪ�Q#�j��Zh~�'s���O��4훣��m������ڟ&T�<��u��c�,F�C-�����EQ�_5>]V      �   �  x���M��0@�3��G8,��c;��U�
���J�&Ai����J�o��*99�(o<�O��"��c~��۰m�F}l�~��}���?��6�F�m��b��ĐFC���U^�^�,�;x,,�Mӽ(ɘ�B�������4K�A���m��/���`�Sݯ�l`�cr6iWb���~�"��}V�'�������~�[����28{ѩ/��˸�ђKڗ��Zc�C2�a�6�a�Z�j��J�%�$�>�ֆ$}*0$N��2'�JɃ���(��PJ�X�g���<w���~��&��D�މ!U��|oHR�C	�x���֧���h��_�^�)�/1�-��K5dO�<24����8�����z�kZ��^7C/���q�ԛ��o���6��o�3�J%��/�j�eݯ��>��?�%:Mkb(1\�rZ1i.14��n0�OW瑡������ħe�� �\q���Okbh*���i�{v�>1�pˇ[H�?�>�O�1ra������r9      �      x������ � �      �   �   x���=��0��z�� ��ؓ�n�6�v4SD�)��"�$��[<�d301��r��U۟|��@)Q���|�_��q���^�U�9QV-5��-
�~�Z{?��(�K��XoF�Tj��v�{���)Z�oܘ�rZ�ƥ�3��� 7��� ^g����ip?�8�E���DNZjhKys[<��'�Rz+3e�R�}@�+Ns�      �      x��k��J�/���)��bb����y�3���""�u�**�� �^�r�˝O2U�]������'fbw�^+��ʬ��_�% <<�j.�Xu����r�rMM�b.�mϞc�b<�1�2�/���O����wN�9��=w`����?��&�ˡ��?����?a��P��+rӦ���-���h7��PP�-֚Ts?�?��_�F��Q0�ӿ��w�FP ���,�  J��up�u�������Y������9�eq�bb\D���� �%�{v��ZC�g��k�;q	��ps�T	��!���U�G�]|-����ą�H8 ~#-X�&.�N�vg��!k���h�L��,"H�۷c{�5C{��"���𾯴̱v%p@�$E'��z/�p��zN[���n�\i����%�����X�+Ҵ�B���;�,���>����`;�l�0���9�}FD�칡b���dc��왍��8>���)� h�A5�(�뺪�ԅ���Z�4ӞD��L���&�$�Yә��Z�v�K2��D�����'yQ���g�+���S<��h�|��iH&��-gh����jATLT�zU0d�ZnM�K�cEِ��)��*&������a%V�Ԧ�41M��dՐV�|SLI�Y���'��W?�wF��E%w{�gi������ vϻ�-gv��zߋp<��MM	��1$cuy��ں�}Cy�J�����)yq_,�%��9�Q��c�h���}M{4�O�E�;�5U�۩Q���0ic{�	3���ayA�$Q�4��|�� �x��d2��RƧAQ�UX�wt-Oƣ�]p�m�.��B�7�΢���6���A���ho��F�#X��IV���Y���Æ�4m9s�)M!�<�Z,�S��+�l�K�Y���h��u���܁3\���X�}�����q�s�G��Er��y�.g�jB����1	���cmVj�/��ɤ9�c�j*�;����m�!Ԫ@J>�R�d<��
ϢA�@@�@� 4*�X��"��w
�g����} Ep�$��b�� ���p\��nA�KѢz�U�_� ���Z8�}s(>�h�><����YO (�.�"���4�<K|��� HK٭|�*˸_���Ѽhȥ:P$A%gf^h/w]sU?�`�}e&.4h��� �o(hz�T?HA����$�ME=� Veq��Sf�)�MX:S(��QT�:Q�Wu��qS���cGϫ`����_�
���b��]��?w3'<�A�#[ih5,��Z�:�3��I���.|���98�d�d�~YJJ<�E��g� �Wg�@w�j���"�iѣܦ���Ӧ�{�"�hܰ*�nԺ� �/ F��
ٜ�~��A>����Z܅5j1Ұ���9]�w��KnW����Rk��1���"�M�&�{��E��������(���H�	M�c�t��R]?k��o|�}�ڕ��HlW׋55()m�)��B���noM��ؼT��7���ћ$���"W}�r��{+՘��tl���pi�	��
��q͙#h��	5W"@�	fP�ًx�:cg� Ho}���R��aI38MA9ַb�
XA��{���\�0��|��ӤU�k���1\��-3�e���{�º���%�z�Y���%k�Y4h�!�$�g�<~���_[��9�ƿ0�x��S�p6�o�`0�ƃɜ�Q��vf�����V��!��+F�� �,�&I����X_�;��'P�q�U}_�)s��(M����h��l5ۊη��H�&�A�>�9�Y{�'?Ӡ9�(��_��ɰ����E�M ��Y��_�ɡ�,꼶��$nM�~�Mh�nF���rGtpQ+��U��_��rS
g�~��`�hPR\�)!��@E?�ǁ{�R-/�Bb�1MRKB[0Џz]lV�r�ڬ�g�.��!{�}d�3>�+F�%!J�c�ΤBF�`mM;�4ј��k'&_�?$g	��@��ɼ���VcߚьQ�;r��Jl�ѯ�k�`CW�͢A��KŋC��3eZ?YmI�)�$��*aB�)�q�ԒMA��y�^�A�4�Xp5	B#iRV46<w��%�����JrG��%��Xv��5atC��Z��%$����V���Z�`,P��J~ݗ�-El�?��3��M|6Z�0b��農�� ]���9��4�=r	�1u9�Å����'A�?�
fBT��#Z�t<d��8�3w��[��9zf×&��ĺ��	�2�G�����{{�z�e��ۢ���}���yꨣ`3k�zl�}nB����z�G
sP��c�E��7p�9�݁�\~��������yӕuo�}s��$�hs@O�O`��U�~�	��'��i�����M�Ѹ"��?蕷��i솖�����:`�t�����C��d� ��+,��3^�߷���Ih)I���ŅS�)�AZ��a��(m�8��n㴉ҾέX-��p���2���q���iЌB:I�8�g G6���8N��L}Z^�].QV���2��%�v����ʰ�7��W�\�"���BPa�5lA�,
�C�.�P¡;��
��t֏HA�Dhe�u�.Q��պ��N��T��e�"4�o��(�Ɍu|�����_|2�WU��4�y�!� �3�H�C�o�`m�sg����{�N�oMe��j�\j�YeSƾl�������;�#���k��qާ�D����Ѣ�	Ei%��KG����X�K��V����q��
4���u��)���l��⻜��C���[Է��)�z;����<3�)M2Ԥ��M�[��v�B ��b�h�@SGǦN��~!���pM��4���2��5Z�%��W�*D�"ě�,`j�V�1�?�ڼ�L����yH��O�Sл'��Pʗ�8K2��4�@����4��-T�u�>�$h� Z\��#������AI��8����F��U�+H�� t"	(v�yIpw���3��$G~�$��`,ae��5?V����f�!zMzS��(O�[��T�f�+H�W�iI@�n�q����=0q��'�����o[�Ȁ��l+�H��	�;=y�w�ɴ�9��@Z��[�c���%H2 H ��$xR�g E�љx��c���(0�(9PO@4e���_��l�����i���Q�@�W�p�,׺���7���
�`�kp�4h��Z��b��lp��f�[�`\��'��y�xF���D�]̗3ln5;^UW�ǅ�^�n�*��J�10�h�׻�%���|П<J�ˡ�#�*�,�K���[_�Fԏ�p�y� y����q�:~h0?����oű���M�����2�nڄ23��̙�Ӡlƍ�Wvy/�G������@��ϢA#��`�1r�]#�~�o	��H_X96e�X���4��E�P����%���1�e�:�/r�!j�^�rd��#+a �]8�m_�~?�4|SQO 
�����+'���m���v��7l�[�L�0֋�#��!�G�^R���Y�?�OfѠY#sh)#ck��װ0a8w. �R���ܟx/�\JRΝ�c���w��/�8��M�1D�ЕkˊЅ�Yk�*P�����&��_0��P/ql��B�M�8:������μ�{�GO�4��$ۑ�*��� �� ].�A=��\��r��m�[�F5X4�����p^.�t�x����ԫɛH����\.�����_?��\S��-H<��f�}j��oΔ�V]՜��mI����yQ��q�:T��_��)���w��V�l	����A'V������1�ju�r�j`̨rm�n���"�K�Y���f΁M���M�	�gOZ[�g������o�:t���]�@��*�zO$V�ϼx߱#8���H����ݭ��s��+��iʪ+��a\n�Y8�x�2�M�`���O2� ����5������c�Z<��P��H��<&e�8����Ҵ���e)j��+��[L�p��v,��٦Tz�a ����5�NF>���u�    c����K��|ߦ�'��X�q�u��Yu����JI�T���&��t�|�W��>\�b����4D{Cg���}�a)�y�y'i�(���~�hUF��DK�82=��U�{��3VQ��|~��������l L�μF��5���R���v[��>��5�F��i�0�V�MOY� ��r��U�$\�Č������&Z�_+.T}���"�#L#%[��L@Kn��4lO��%�U��t��8\�5`�f^Y�ԾP��4�6M�4�DQ�4׬�Q�D_����]#��֋�io�!9��g��V�f�����a�����!�&��\N��4���/���-h"G.�X���!��u.^P��ӚgM	�%�����4���;��[k����:�QVv���T���6Һ�0c�ا����y������L�Y�D�v������z��(2I{��q|����*�W�)W���S�y��t�ʄ�Z�p�,��<�*u;O�á�8�	E| ���N4�����;W�2�z]q�3�������I�a�'�m_������ݯ`����+��I3���ĸ �ho�$5Z�ytI�t����/�cg�|I/����|���;u�����䚞��ؽ�z�4�e�`���Њ���aRqY�t'��2/�$�ϥO��o��j����W��1�g���Yi���v'�WJ��{�$m�j�^bk7�to��̅m�9*s���+�������E���^ݎ�����j����b�q1
Wmb�i^�f���Ba�`
١>�89m}Y�x��vQ�"[�p�+mK�a
t�V�|���(<P>��ɶ��W5�̇�z_PWf��_]����A�i�?;����v������ve��l��ix<���&	k2)�A�ݺ��_�F�4�������_l�M�\��?h�E{�(�:^��ۋ������Й��B`0��h� �N��W���_��ock��/������z�?NQI?�~�c�����!�e��!�~҈�x�SU���*����}�`���ݏ|J�T�1YϽ�����������"n?q�T��q|t��؆�OD�u���i�S�q[B;���R5��}�O�r~�#�|������O����SgVѨ��$���G���@�����,�ԧ�퉊��p1���y,�0~��"~?ǰ�w�����!�\I�?
��C��g����?g׃.{�x�;�� Op�E�T�B�@�i�'��O��v*��������.1�δ�V�W���PZ�0:���(��FD��6T�=�T;A�gD>�0M�4�b}7����9h��w���8�bF�]GN'�ioPcS���)�|:^�Q�����/��yR�?��^����n��s��s������r蠲��������Z�/s��d	�)��G�d?�����(ǈ��d@J�>�#��h��]�ֆ�L��O��a��S8��e(��v�'��3!)Γ��?p&��}.���g�U��-���!�"	��߿7ymT��$)�}���3��7*����fUF���G�"���ڞM�r���FS9�M���<
n�a�h�m�^`�S���U],���CX����~�&����N��Eq�z�O	��&��Q��Y�6-��%B��t:��	*��t��g@�x(W�7�ɥ.����b�m�8���ٓ{j�a����W]z�M��&e�bY�/𱚹��!Z4�����D��?q�' ~B����'^x�^k>�����G�{�+Z�[��~��O��I��ϻ�B��]��m`_��7���J���Դe?(.��F�9OO����= �)��	#ۍki3^q�_j�?ﳸ&8��5�2���_��37u���Z|a���x�U���8������G��M����ƬV���g�7�~%���-x����5GQ�@!W�7���DZ���AkL�w��r����R�/�0T��6��2��m|E6O����1��s4H���h�f�3t�����G���g�~s��W�}M��,4_T�k�����}t�!~�NU4�fU�Cl%��o��}e~�^���fb���J�5��s4��ǥ��o�ǱҾ�#���q�pv*6��+C멲~Q�"ჟ���w���������?�oN�_(��g�i�S3����,�C�h*sF�MN3���C^�c�W|�0��Z~�(qt��>����x��ߞR��J~wJ�^��1N�9$��Y�N�����ƀM�gV%�Cx��S#t�������U!kӸD���%�'|�n?z���{{\�/L�o��wfچȮ��I�E1�븻��.�{��?h�^k�S3�?_G���1d���oDWM�8G���	��x��T�w��˥��<�����2W�)�7 +������7@�߫��آ�k�3� oAޑ��P9�j�U��:�����<M��w�6�n���G�߃����S%�����;���:���_,�w�;`�dV�A?5����W���7�����+�C�h�����ί�
��n<�^� R��/�o���oh�SC��d����1l���1D=.���z�~���܏�W��7�ރ�Ͱ�>���|���O�������ő��p9��y���_ׯD|��y~�!����ڝ���"\C��}��Q��X���z{^����?���w��uM{c�>�L���J��Āt��h�/���|��gz�">O����9����� &�B"��ZW'��Y�t�d�C�F0|�����q�O����Λ��]��+����m|jx��Ϣ��D��W�Ew�����l��9��o��-�Qr����.Q����3	���kAb��@��>�Mh�8�s���_
����*�|���,9�m�j���k��a�N�PY1�H-lj˛U)�6z�|�f�I�4�A�Y�7�x�0#�dYP��_�����i`��Iz�PTSnbzU/�w��,��!�`�J�p��2pf��wr�^h֤���n�������b<�~��� h�v��̦I�mq�n�#��󓡭 �ոr'Z�LT@�0��Z��V�:=¥\M*,
F�Փ}E^�\��X2�07}u
n��Tw6n��6ʤ0>�n?eV�:��N�_>F1�uL��z�s�����q���c�.����Zf����XJ�Ӛ�IGa�Q��}NU|�n�m�(�[#0z�e���Ş��^��X��}�ի�n��K��m��t�U���_���hm�'�▅BD�ׇ^0�^����$�'s\z����,�K瘬��Q�bI�|�B�,��h$�/_�.A��jY�B���V��z~��p��%��Eu�pV) ��*Zt�I4��:��ڐ��bۮK����:���*���k�i���y�ek?	����|���E{c�/��;t�bI�/Vm���/��URc��{�s�[�w��]E�v���@��Əc���[�4��q�*��+
g6SP*s� �-���`C�< a���D$K:�jq	�pM{c����?�"V�?��(Slh3�P�z�>	PD�Oq!ܛ����ͫ�a��j�(��6\b�Y�Ն��d~CW�VE+���v��F�$qI{E{c��gt��
h���z6+���M�i�A��qh�>f,Cr9�
��p@~�)��lL�щ�?�P0✙� Foq�tI�v�Y��â����V-���}c+3�&��Y�9Q�5��slV�,-X�Nx�<N�}u�q
��	A�إI0�/��R,n�"a=l�$ҭ�]�����n��C��O(yi�+O*�X�y	�E{��KdH�:A|�E�B�>Q����H�w>� H�UӐt�ބX>_�Wϟ[Mh�9uf,a1ƤvX�zy���˺�{t�6��;nX-�k�W��:h�g��8�;���~<Hj���1�]����8C��m��;-�u1Ԙ]�dl��n<�(!W}�l�]=j�fQh�'� f�`.�D�Pu�&Z���$�W��OLA*���E�������>��7G�'Hp��\��{�,�����.    ���dS0w�����u~R��Ja.�i/�S��ܽh�D	�E&�,�G��x��.����%,�HZ���՚4�(+œ#I��ܻߛ[� i�:fx�ia�V���g�tt��۳`�ɛ�>��
`Y��H��*c�%a�dEY�7�ʱ�RTqD�x�-/`�/qTot����]2��W�|�j7܍�c�\�=��`9O?GKo��Obe�m�Y;�ٳ&TMU�:�z��C��(��[��幋�3b �4�H�ޠ-g�Ӝ���W��ugf�X��#��{jF?|�X�7�2:/�A����Ȣ�C��iF��+��F0^�#b$����9�9�J���'G�"���6>���196^#xO��?���.yG��Կ�����!W���W�n�ߠ��^��&�h���F�4���گ�[t/��N�H��F�)v����1:QBe��86�fy,����:�rE��D��֔J(ff3����ly�x_��ϡ?�ey%AD���4�c-�&�c������'6�����m�}VsCl�k�w.�!p:8�c��JL�V*��iY���XS5�U��-q���bRKP�H�Ib�Ø�����Y@,Nd����L�Ld������Y�*���XFz���b]�鱭w��$��8�}��A�Y�7�C�4����]���vIJۋ�z�q��'&IH���JjWH��+	��l
Ʊ��*��NK�6���x��i �ι�	���r�B
�D�dy�%�{�P���nX^�_�-�QgҜPΌ\DG�#�R�P�|G״7�q�u�@�}�/�F��40�	�c�������qvN�^G	��K5ǃbp�_
�"Ζ��}�t�W�(��)I*wh)�s�5\��1:CsG8��Ƥ̚0m��b�l:R��	 z�/��d��`9�Z�gS��Il�q�wI�+B�[�h"�3��#�>��?��&I�/$<�.�/!t��<�k<�7S��[�֎���[wd�j��!u�9n�,��S�=�'ǘ2\�9.9�!�..�.Iu:��`��E����(W��d~��g�-��"�#h���
� �q��$iF�d���{!������RS6��؝�v�*Q'KǮZ<������I��1�Nd��x2Ǒ��2�f�w�S	UCR�2�4TH�Q���*6�f���_�;���P��X���F��-��q�\_� X�d�m!d�@E����]DZ�z֢W�ٸ��^7ucgW<z) ��+�lV�W�(����S9.9D��4��.�&�EA@i�(R�1n��אhᔀ`�(X2���Kӛ�O��.�d����{��P�&�&V��.��}[]�D�,��H����:��5Vp�{f�u�n�JYCJmD`� ���/��ֶ0���^tM�љvkm|,�$��Vƴ7��qY��<�i���l�t��?j�8�!i�8cN^�ŁH˕�
��^bÙ�3jӪg�,�������F,3u^�(�`�D{��d�ՐkZS�*pJӮ��z��	��(�#��!Dq��M���7j��y�3�M�����Q�0��҄?���]����k��*�|�I���f��x6�����������-��� ��oǻM�y�*!�c�q̘	��̢�#�r��ū���g�9�Ɠ$Y�=��s��T�.���7�'6{rI��Jl�q��=�Q+u�J�rs9�uE{��e��x��c̟�UȢ��)�2M��gF��Q�Y�i��)kT�b���+Q`Vl���Wz{=�p���(�^-rl�,��縬���rl���6���{w����3l�u��2*x�P+���"��h�[2�+B�\kE[�T�U�z"�ב���"�d����2s����o ��7@ �A�O̡L���W}��uY�9���C�U�㕃Ug�{j�+E��xzI��)U�4���~Q��[~c�Z��v�.,�s\s~&ߟ�ǳ�n�?�-��.�E:�3��W[$?�W��d*yy���RY����3���b�hP ǃ �|_�f< ����ւ�ځ�=lv��~�G=�h��nGs���+�Ś�7�������7�A~���\��bf�O�8ų8��s��]NZ�r�<6{�?���Ż�@+��A��a�d�nK/����W4(
��" i��W��� K��՚�*_���)���u�[�`�5v�;��A���㫣��p����^�u���4�"��3�����e�̼,�dJeCע��IB�Ӵ������l�
���ke�G��P�5�Ca��t���4��4�0���,�y�L�J~ѯ4K�<N0�z9l�3V�)��ӡ˝ذ�	��LS�Z��Fk�-�Wm��|�E��Y4�"��vh��KϿ|�"�6��/�(u���V
�vY�ӕ��a�ǝ�&_/�ֽb��5	v��ڠ��`��^���̽�4�.��P��]x=Y�,�]�yh���NNV�ۂ"���@:s�Y��=��=2��U��l�K���F�)������nsx�u���۸�A���y�3��։�J�9��޳N�y���1�ɤ\ߔ�u0Z��Nqy 7Y�ټ2����k���Ӑ{8 {�}p����}8�4sQd�%�N�e�%��5@���1�4X����7\�B��߮��>����A�	td8��4�V��<�ɚ�m��9:QgJrq�k�
�Hŗ�E�Pq��L6�yo�]�==X�t[5�
-�x��y�K�f��D�\��_w�<��O\>�Ӏ�hp��3���8�ʪ_j���]@�Y��
ys��%��2�z1���9�k�k\�[�� �d���\w��?})�����py5Yam֊4��}A�+���F�"k��������%���������X��,d��!Pu�Ep���S��p:�+a������^Lm�mO�~�w�*;�Bj6��f����aG^4�]w_��8�WZ0�����i�+��!��Ż��sʟ������{`9(F�f�@���l���rF[EE�Ez@�jO�iYu����@܈�μ.�ʺ ����'9�3t0��`&� �}��4g��9��$����.c,��K���j�1�;#��K?��C'��ڸX���XV��2���j�#2������f��oFh�bX�/.�km��i�F~?x�3�����)�Y��FI"��yp�@Y4��C��>_|����������_i;�Y�	��B��߻�r[�B%?��{�K��R)_-�E���[x�A���Ch�S���ǫ��\S�5�/s��@\�r&7#L���숴e�v���C�x8���O}��x�|~_�4	x�FB�s%]2���6o5�pY.��.�3���fo�8��CcT����!r�����d���+��.�_H�(��.1:���B�*������k҃Ea�$�NV���!()򦐫��]V�C�`��V�����h::�C�gy�i{�Q�cJ��a���t
1"s�L�h��N���%S�\?ǲ3�1�� �B59p��	eC�D�ǉ���MX�ҨǮ����xy0������z?��s;g��<��=�6�d�ސO��Y�D�q�g@���sZ���t46p���d?��UI��ц�7�PU�{��=;m��f�B�`	Z�`��4AM�b��*��L���tF�Q#���As��ջ�M��4E�׆�x�+�.��uG��ƅ�N�+��8��:�zDSi���a=:�9�}ul0q4};�H���h4���ƒ����zC��,���
�����O6`� b�h�k
A�/���ϖٞ�.FY/��:,��H�t�Z��')�BY~{J#��Ơ�2��+o�7�68ު���>��~9�	Q2ȢA�!z$��c�5�s�?���EسT���m�S���ci�g7��N��A�/օ�;�u�Pz���9�ud������A&!�$��d�����&��h�f2������z��z[�����Y����)�lm�q#-x�:3��OL� �T٠2����P���*ˑL�P^����nU+�4������sEI�W�:�0Ѽ:+��7^�K�K� ��:�%��3�    \=�$�3W]�Z}!8kYps,ӄJ�>^���j˛m$�&��Bu<��x�[gg����xE4�$ěD�����Ş��S�n�1"U���.�
't�,�$:��>�V�� �>�Wt��p�d?Uwe�xK�w�Ӧ�[7��05&�~_���8T�!�k���PJ�s�/�1�PT��B��$��>��O�W�@>%��j�Lh��)]V�R����;�V&�=�$�Va�5��hǫ���ӱ�h��WDO�$�E���8d.8����_=�9Z��I��XQ(Hꂓ�cm�<a��vC� vS ��Z�6�r���*EuG{U����(d�m�iP� ���6��_<x&�54 8�ǉ�e��/�Yx���	��];�w���x����j���=��{���9���(�:7^WG��V��e�%�`?#`�"�15��Կ��!׆TÈ��w���Y���D�H�q�a����I��n�8�>���o e���D	��~���}��90;m��&e�Vu�4(e��A枺&�q�<Jo0"��I<��g��-G�4}:Psܯ�?�6?X�����]-XnN� N(P�R�=�-�ˎ:j�݊:m���򸵓���-������?�[@ӠH �Y�^s�q��P��c.��"z�����$��\��-,%4ڂ~4�Uc�8KP�L�	���"VzG�:��4���s��h���C1
�
 �U��Sz|�hv����-�th!	�ָ�H:����D���R3�r���1Wa<��ܞF臤q���0��fςevU�)? N 	T~x�o7��V�w��6�bP��s��/8oDm^�^�'�����c�'��观졻c��ƑoH��*CSD�(H��B�n�C֊�J8�@�e�&ʸ_&��5��B���)7�A�9��/�!ݬJi�C���#�]�zp_�� �"0����/R<sVQ^��A[L)iG１LUM��،�N����9���x��,�:> �񹰖�j�X�f7�=�mǏ�J���3?�S<�ְ�FK�A	��`�vw�&5�6���Z0��[�+<��s)�M(r4񘺵v����B�Hz����\&q�2$¥&�'����G+|��kb�^�+�,��P�u�g_a��(X��О̂�)ƪ�v�*�<:=�=���_M�GX�:G��|�*���lia�w����o��]vYi9���+���΢A� �&� t��|ÝR�߃G#�~f�WS�˓|�Q̛��A�0�9�Ɍ׫�W�=X�u��/gF��y�2�+�^�=լ�D�$��=x�?Xe8*ٖ��`�E�96���"�1i5��|��(ɞ�6����k��@��R�$�m��>w����AY4�1�d��p�55��WDy[�g������b��xGnG�ò�V������f��s��ޯ��b[w��s皎�pp�e� {�Y�0�q)�t%��g�#���XM�z5�m�sb\S|��	��W`A��~S�ʜ>��t�X~���e�#�����Jt1s�[Ŋh����Ҫ����Ud����r^���J�U�.3k1yP�:ݰ����u#�.�[�z�A.Yt��+��gN�ɀ���G	��cFAl>��S��Vw-��*�ls9�6��'[���(�+��4�c��o_c�f�H��BD>FA�V�5��E�x���p ��b����a��N�d�ܺ�Є��+թ�3����j�7{/�$͢A� �!�h���!ʃ��,:��G���M��[���ׇ���F|E�̨�9���6{����5�Z0y�?�>쟢�4;ԗ`Ƿ�~4�4T���dy�8)����­��g}π��
�u��'{���m�[L�m����L�TC�Y4�-D:�c�c�k�a�p�L~ h�;�+=a���%��N\�N������IW۳M{^�Rvj��d���7��.u�|bd"��㆓���g!큲��Ne�~�o���bc>��IiG�Ǥ�#b)�qq�3l��7��H_ᒺ��A.!�C��3�1n�˃���=�\���˫U#����D�����3n<���r"r1�7�L�H(�W\0���佢Av!��㟄+���ur{�Σ7� �=��#�C�K�LYʸW�ͪ>���JO�mY_��R�7�� ��E�{��=���4�2�D�cLt\�pc{��q��>8bx�OqL[���PsQܬ�Na:��2ύ����.�L1��Ҭ�
�W�S4�1�I�c�t�˝GA&���S�1�n��K�K��*�+��ݸnh�$C��%��3q�R��+.7d��4��x%��x�z����(�D����X�%J0 ��(��B��fYo���,�%{Ѳ�ɸ^g���2S5#�Tʓn�f��.[L��r���W+c�ۙ����8K%Z�����N�ig1ri�V�M���l[!��nh���K\�5>iV^qL!G��� ��
���@������b�_N1�[\gIB%�XY���~�v[��)i�XV�Q	�p��-�#C\��5pP!��&An�oL|p��� A'����qk����7-K��Q%|^2��z�;ɜW�e�����1˥/D�h�Q���&.o'�����C�#��S�V�j{�(:�a�^�l�k�K�܈�>�:<���g2���ow[�4�(�L�������Ӄ�g{9�9t	x:}�bX^���ߜ�u����Q���������q��V��%ŧog�h�i���/�4���Đ���8�w^Z;��Y
$Ҥ��;�9H�+G5�����l��Ϙ��)q��DT^qSQ &�Y���~���Xu�h>s������L1�i���@>~�b��ƶV^�k�7��MŜ�fD��ѕ��6qi�v�Z�5�?9�12q�n�
��Z�p�y%x�g��;'P��)���I}gh�q���<����|����B}�/
Ak�w�$^YM��z�\�d L���Iv��Ǆ��$�4	�%��e,e7��o���F�J�ҩ��W�}�m��q�*3�0
>���� �� �3'D[��������.�,�3�(� R�۬���^�Z��f�����6�Qi��m���ǕV��u4p�)E�ls(>�c���� �����9$�l�����YB�6gd�6͗��`4����Z'�BS�tg��JZO��+���f$q��,����Ea��|L��a�qxe3�F��xQ`�iq��	����oc�b�A��N�U���RM����������ܿ$�[���kg~����ȐE��ˉb�	�A�P���j�}O�8NS�r�j�)ՠ����-�]N����uʅ��X(�m��,D�4)����sH1P�}��F� 2Y P��[�R���~S{��'_�/��1�4�����e\$���Fxӈ${�'RE*��Nc�6�԰H�tU�U�F��܅#�j9 /E��"P���%y{�C0}FT�w쾲,K���ֹ���+_`V�8���P���8
�r+�k�ա�5D'O�m�R�<R�7��`2��3�]Eޒ�(\n��s���!;�z~���0�q�!!ފ���N��q&2M�R�!��@T�p��$�KsFO�I��P r�	�J��f�υ0?�m�fѠl!�e��nU��qD������.�%��t!���(V�)U�"��d
y���I%Ὡ���~%�o�����k�f��͇���$�a6E��"}�!-��Oif�y\
ݹM������S����v�<?�=�������#��`7@D�d\��9�sOZ���Oɍ��(���1tӎW4���TGP�,���r=9U��4��H�Z�"�Ю9C�ј�x;|�#�K��+��]d�I�6�8��X�"qJ)���qB��PӚ�p4��H����[j$��*�y���|���Ж'��������,���v�_V4~_�K[��Z	W7�s�!q���}���!̃C�"�1��f��r�b�zK�	��Gi�",N��w��s<����H�K%^ⱟ�#cm���}h�4Dzx�Mf�ΠF���͘请
]�?���-���G�]�̬xFl�́nB�� �  
ˍ;|O��3��_� 8'���g
J	����L��@�w�{ح��뺋��r�{�2·�4S_u�Tu�AAq�(��Ag�`�I���S�M�_.v����K����}p���WR�埿����  �E�4I���1�5[i�������-��.��hC��Xl
��z�p��W��\\�РX�O����l�k�"���J=�V��u=N�p�w�F���M�zYiCl�rW$�o������HyI���?����`���MlS�?��3���Yh�`9��@|o�:���㛷jb��]bZU�����>����`@e��m���'o�&B�w.�7���Uf�{���=r���_/�A���w1D;��$Uo�Mv�8�E"������ŭ�@�h}��JRC	t��V��M��b��zORuЛ��/��M����2��u�<�������������8��u�`�f`c���0cf{(9����0j�s�\����Ѫ'�-���c��p��ZR}CXҴYn���L�&]W��D���ݒ�jNA��a/*���B�P�������l���2^=��f�{;�Q��4E�U��ڋ��`­]��"�펧��x�������~����ټ�v��edR+��8�"�5�ˍJ<�@���:�y�A�C���r*Ske��8�A���;[b��:N"a�}�8G�����+�קO��\�Kx�~��pB��HGX~�z�Y�C��ĳI�b$?�\j��T���JvE�U�pL'p�j����)6�MP�,A�<w�.�h��LD=�HP˧�iu�,���R��F0��U`���l��.�%�H��'�m$]N_m3]hp@���rj/+ /�Ew���������S6at�;򠞴P����cߙ��LY3B.
~W�me�)�h=���He�:2*�i
��"f�����]V6�e��`��;����H�����+uԚ��i��j�=�L�RA�����P�r���ߙ��'#;$���.4(t���i"�u��}̐E!}���~�[C9H%�����[I��ۈ]H����P~�f���uX���!<��<�2�W�JI����ô��B�ͯ2��Qs&׍Ɓ��n�!M�xE3�;���9�A�(�L���u��ҭ������!�2њ�u��d]��bq� Ӥ�K	��L�ڞR^���B=?�R�� f�3�ho��W��֭*�iP`нe������;tn���i<����H<�S�!LPv����AO�����b��=��9��Sk�$E�n���*V������B�ݸ�GtI�,�T*�B*�+b��t�Eτ.V��8O�o/�����7Q�}w\���1�Ņ4�������?�����ɈO	w�Չ�����e�~�^5�Eǅ��v�~��oݕ Y��:�8!p���E��<�]7
��.�)�q��q�P���뾺c�W���.�io(��e��;�A��3;-6�yˆ.Z��$���h��ehB�������pR<��KqMjh+�:�0
{��a�[�ƨdovAyr0YA�E�Ԓ�S��!՛^�x.V�Qx�U��
��Y{��ڷ��zH���w���,	]�㡙�J��)�kR�O�r~ћB�Ȑ�Q�� ��u�1��>������H�.ˋI�������4�^�
&V�����
�?�v���e���D�z��@�f����c���Qv�h�Ϸ�+ЁBsҊP�{��2X�;�^��H��0���٪��A�F^v�����Dj�)�_,
R�E���'_N����\����y�^=~pi�,ϑ�MS��Ʀ$AYڶ��h��V��ךuB���`Q0z�^Mݍ�E�~.���k:�6E����=3���\s�^s�:��[7�Ѿ��N��{�E�����
y���pDP:5Y�4EiMM�w��EHɚ�:xKS7��`~ V����W{89X�2���H�'�&reo��#�"��lk7�N����Hrǝ���B0�f������U��M���1
@o0ILT2�)��XEO߯�}{�J�F5k$�t��m��-5#- �HW��A�.����&3m�Yt�$K��ȴ(��1��5�e��y�f��x�{�1`�D�E�<i� 8M��h�kUC�ā��Js����h�8�0(o[`t�l�Rz� �AA��Յ��"���j�#[H\GNHw�8���p���3- �V ��Yt"���a��+ey(r���\Α��;`I|T~r�$�^ol\h���9�0^�N�8\0�����w%a>Ý��%�9]	�:�ᡫ�$'|sS_��/yu?�nO�3utX��@�[����|bl��Q��L2��%L���ln�����W��ϙ\2?��i���Қ��z�D>�q�jk>��✅S�mu_]��)58v<E��i�ho���a��Rp���P�g�)����w�bxU�NL���ӱo�����I{Lv+�agX1�D	�E?rұ�S4�Ĕ���P��=>�3>ؤ! O���8%�Z�9�����}G�{��v��n��o���(���Q�3��E�b��6�b �1\=|����1��ݯSr��ɸ�vWK�Q�®�lzf{����MBSՊD֞�v�LGMJѠh�����{����i����r�3�iK�lv��0�6/�٨����`�k!����x�E ?�����IO&�b��\dW������㏱!��Hq�X;V[W^�+�\���}F�9��MW~w!��W��ҫ�)�B��td_�Gh'^z��?�H
�p %�X�hH�P+�Z���S�Y�鹽���ݶ,t2«n�P�I3��>����x:G�1Uf"�/Z����%KCjrV�7GP�0Z�b�I���jn�+�|�,U��M�q��>��("}�,E{C�u��Di��2����k�~�������������&x��z�xN<���-n����y�<#���r��8�	>h:��b����zJ+�f�M������V�Um�HjI(�Ô�ӳg��wr��O��`?��Y�x�����K(V� gU}�m&�%qh	����:�#�U(��a�$v��*�J_R��b�4ؗhE%%��\�'5������}���X,��s��)����T���{ �`��`�у}�����ֻ��,���֮�9Q �����b�א[��(/���*
����`̯��2��K�`�{ڙ鞞�B� �6��F�Z�A�d�*V<�_�xl��THq����ƞm@�k/2"��B�4��#�-��Ko�.���"e>����^!煾5�,a���\�[���j�J��{Q�;{��L�{1�m�����C�T�*&��k��QT���������$����۴"�G���*��s!�l�x�	��\YOf��s�N!�֏*W���*�T%��u}Q���O� ����&O$��1�ץ��Չ%���X�'�@�lU��[$/_:��r�Y�$�7!k��:X�˗)���Rֶ�/��q���x����n���
6��p�P�����m����=x`�Ӏ���Z�\��<��ꔐ�ފs�չ�4)cVPS�3~���4'�P��)������3d�4�v]�Y�q�OP"Yº-���сET�˒݇�V��3�x^;��ڋ�+fYH�g�	6p�%����O���T%���}�	�tz�����)v%�k���w���6�&�W7��vyH��.��q(#+��2%����t��������;��l�Mu�O��D�řq�:ԭB��O�_�8_�5`s$a�r�f[��Th�w�'�����ŧ���+7l�4	Og���څ�{� a_d��$�_���}��!����d!�rD�aw�Sg�Nl	@� ����id�V�K��l�a�NS�$�}�|�H�?/l2��4�K���"_�'����!�4�z�Ɵ�%�|���Tǽ>���dY,���� ]�yïՈ�ﺚ�P���߬ʀ0e�a��J�����h�����      �      x������ � �     