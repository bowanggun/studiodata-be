PGDMP     
    ,            	    {         
   datindrive    15.4    15.4 �               0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false                       0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            	           1262    16398 
   datindrive    DATABASE     �   CREATE DATABASE datindrive WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'English_Indonesia.1252';
    DROP DATABASE datindrive;
                postgres    false                        2615    16399    v1    SCHEMA        CREATE SCHEMA v1;
    DROP SCHEMA v1;
                postgres    false            �            1259    16400    failed_jobs    TABLE     "  CREATE TABLE v1.failed_jobs (
    id bigint NOT NULL,
    uuid character varying(255) NOT NULL,
    connection text NOT NULL,
    queue text NOT NULL,
    payload text NOT NULL,
    exception text NOT NULL,
    failed_at timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);
    DROP TABLE v1.failed_jobs;
       v1         heap    postgres    false    6            �            1259    16406    failed_jobs_id_seq    SEQUENCE     w   CREATE SEQUENCE v1.failed_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE v1.failed_jobs_id_seq;
       v1          postgres    false    215    6            
           0    0    failed_jobs_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE v1.failed_jobs_id_seq OWNED BY v1.failed_jobs.id;
          v1          postgres    false    216            �            1259    16407 
   migrations    TABLE     �   CREATE TABLE v1.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);
    DROP TABLE v1.migrations;
       v1         heap    postgres    false    6            �            1259    16410    migrations_id_seq    SEQUENCE     �   CREATE SEQUENCE v1.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE v1.migrations_id_seq;
       v1          postgres    false    6    217                       0    0    migrations_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE v1.migrations_id_seq OWNED BY v1.migrations.id;
          v1          postgres    false    218            �            1259    16411    mst_collection    TABLE     �  CREATE TABLE v1.mst_collection (
    id bigint NOT NULL,
    nama_collection text NOT NULL,
    judul text,
    tipe character varying(50),
    default_chart character varying(50),
    referensi_data text,
    catatan text,
    route_name text,
    table_name text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    ref_organisasi_id integer,
    organisasi text,
    satuan character varying(100),
    definisi text,
    rumus_perhitungan text,
    cara_memperoleh_data text,
    urusan text,
    is_multidimensi boolean DEFAULT false NOT NULL
);
    DROP TABLE v1.mst_collection;
       v1         heap    postgres    false    6            �            1259    16416    mst_collection_id_seq    SEQUENCE     z   CREATE SEQUENCE v1.mst_collection_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE v1.mst_collection_id_seq;
       v1          postgres    false    219    6                       0    0    mst_collection_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE v1.mst_collection_id_seq OWNED BY v1.mst_collection.id;
          v1          postgres    false    220            �            1259    16417    mst_dashboard    TABLE     �  CREATE TABLE v1.mst_dashboard (
    id bigint NOT NULL,
    nama_dashboard text NOT NULL,
    frame text NOT NULL,
    link text NOT NULL,
    catatan text,
    is_hidden boolean DEFAULT false NOT NULL,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.mst_dashboard;
       v1         heap    postgres    false    6            �            1259    16423    mst_dashboard_id_seq    SEQUENCE     y   CREATE SEQUENCE v1.mst_dashboard_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE v1.mst_dashboard_id_seq;
       v1          postgres    false    221    6                       0    0    mst_dashboard_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE v1.mst_dashboard_id_seq OWNED BY v1.mst_dashboard.id;
          v1          postgres    false    222            �            1259    16424    mst_jenis_dok    TABLE     [  CREATE TABLE v1.mst_jenis_dok (
    id bigint NOT NULL,
    jenis_dokumen character varying(200) NOT NULL,
    ref_grup_dok_id integer,
    keterangan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.mst_jenis_dok;
       v1         heap    postgres    false    6            �            1259    16429    mst_jenis_dok_id_seq    SEQUENCE     y   CREATE SEQUENCE v1.mst_jenis_dok_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE v1.mst_jenis_dok_id_seq;
       v1          postgres    false    223    6                       0    0    mst_jenis_dok_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE v1.mst_jenis_dok_id_seq OWNED BY v1.mst_jenis_dok.id;
          v1          postgres    false    224            �            1259    16430    password_reset_tokens    TABLE     �   CREATE TABLE v1.password_reset_tokens (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);
 %   DROP TABLE v1.password_reset_tokens;
       v1         heap    postgres    false    6            �            1259    16435    personal_access_tokens    TABLE     �  CREATE TABLE v1.personal_access_tokens (
    id bigint NOT NULL,
    tokenable_type character varying(255) NOT NULL,
    tokenable_id bigint NOT NULL,
    name character varying(255) NOT NULL,
    token character varying(64) NOT NULL,
    abilities text,
    last_used_at timestamp(0) without time zone,
    expires_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
 &   DROP TABLE v1.personal_access_tokens;
       v1         heap    postgres    false    6            �            1259    16440    personal_access_tokens_id_seq    SEQUENCE     �   CREATE SEQUENCE v1.personal_access_tokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE v1.personal_access_tokens_id_seq;
       v1          postgres    false    226    6                       0    0    personal_access_tokens_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE v1.personal_access_tokens_id_seq OWNED BY v1.personal_access_tokens.id;
          v1          postgres    false    227            �            1259    16441    ref_data_tag    TABLE     &  CREATE TABLE v1.ref_data_tag (
    id bigint NOT NULL,
    data_tag text NOT NULL,
    keterangan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.ref_data_tag;
       v1         heap    postgres    false    6            �            1259    16446    ref_data_tag_id_seq    SEQUENCE     x   CREATE SEQUENCE v1.ref_data_tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE v1.ref_data_tag_id_seq;
       v1          postgres    false    6    228                       0    0    ref_data_tag_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE v1.ref_data_tag_id_seq OWNED BY v1.ref_data_tag.id;
          v1          postgres    false    229            �            1259    16447    ref_grup_dok    TABLE     <  CREATE TABLE v1.ref_grup_dok (
    id bigint NOT NULL,
    grup_dokumen character varying(200) NOT NULL,
    keterangan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.ref_grup_dok;
       v1         heap    postgres    false    6            �            1259    16452    ref_grup_dok_id_seq    SEQUENCE     x   CREATE SEQUENCE v1.ref_grup_dok_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE v1.ref_grup_dok_id_seq;
       v1          postgres    false    230    6                       0    0    ref_grup_dok_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE v1.ref_grup_dok_id_seq OWNED BY v1.ref_grup_dok.id;
          v1          postgres    false    231            �            1259    24617    ref_kode_wilayah    TABLE     �  CREATE TABLE v1.ref_kode_wilayah (
    id bigint NOT NULL,
    kemendagri_provinsi_kode character varying(20),
    kemendagri_kota_kode character varying(50),
    kemendagri_provinsi_nama character varying(50),
    kemendagri_kota_nama text NOT NULL,
    bps_provinsi_kode character varying(50),
    bps_kota_kode character varying(50),
    bps_provinsi_nama character varying(50),
    bps_kota_nama text NOT NULL,
    latitude text,
    longitude text,
    kode_pos character varying(200),
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
     DROP TABLE v1.ref_kode_wilayah;
       v1         heap    postgres    false    6            �            1259    24616    ref_kode_wilayah_id_seq    SEQUENCE     |   CREATE SEQUENCE v1.ref_kode_wilayah_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE v1.ref_kode_wilayah_id_seq;
       v1          postgres    false    253    6                       0    0    ref_kode_wilayah_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE v1.ref_kode_wilayah_id_seq OWNED BY v1.ref_kode_wilayah.id;
          v1          postgres    false    252            �            1259    16453    ref_organisasi    TABLE     P  CREATE TABLE v1.ref_organisasi (
    id bigint NOT NULL,
    organisasi character varying(200) NOT NULL,
    singkatan text,
    keterangan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.ref_organisasi;
       v1         heap    postgres    false    6            �            1259    16458    ref_organisasi_id_seq    SEQUENCE     z   CREATE SEQUENCE v1.ref_organisasi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE v1.ref_organisasi_id_seq;
       v1          postgres    false    232    6                       0    0    ref_organisasi_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE v1.ref_organisasi_id_seq OWNED BY v1.ref_organisasi.id;
          v1          postgres    false    233            �            1259    16459 	   ref_topik    TABLE     2  CREATE TABLE v1.ref_topik (
    id bigint NOT NULL,
    topik character varying(200) NOT NULL,
    keterangan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.ref_topik;
       v1         heap    postgres    false    6            �            1259    16464    ref_topik_id_seq    SEQUENCE     u   CREATE SEQUENCE v1.ref_topik_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE v1.ref_topik_id_seq;
       v1          postgres    false    6    234                       0    0    ref_topik_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE v1.ref_topik_id_seq OWNED BY v1.ref_topik.id;
          v1          postgres    false    235            �            1259    24601    trx_collection    TABLE       CREATE TABLE v1.trx_collection (
    id bigint NOT NULL,
    mst_collection_id integer,
    tahun integer NOT NULL,
    target text,
    realisasi text,
    sumber text,
    catatan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    kemendagri_kota_kode character varying(50) DEFAULT '32.71'::character varying NOT NULL,
    kemendagri_kota_nama text DEFAULT 'KOTA BOGOR'::text NOT NULL
);
    DROP TABLE v1.trx_collection;
       v1         heap    postgres    false    6            �            1259    24600    trx_collection_id_seq    SEQUENCE     z   CREATE SEQUENCE v1.trx_collection_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE v1.trx_collection_id_seq;
       v1          postgres    false    6    251                       0    0    trx_collection_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE v1.trx_collection_id_seq OWNED BY v1.trx_collection.id;
          v1          postgres    false    250            �            1259    16465    trx_data    TABLE     4  CREATE TABLE v1.trx_data (
    id bigint NOT NULL,
    mst_jenis_dok_id integer,
    sumber_data text NOT NULL,
    jenis_data character varying(100) NOT NULL,
    nama_data text NOT NULL,
    deskripsi_data text NOT NULL,
    format_data character varying(100) NOT NULL,
    media_type character varying(100),
    size_data character varying(100),
    ekstensi_data character varying(100),
    nama_file text,
    url_file_upload text,
    url_upload text,
    is_shared boolean DEFAULT false NOT NULL,
    webbappeda_visible boolean DEFAULT false NOT NULL,
    satudata_visible boolean DEFAULT false NOT NULL,
    catatan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.trx_data;
       v1         heap    postgres    false    6            �            1259    16473    trx_data_id_seq    SEQUENCE     t   CREATE SEQUENCE v1.trx_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE v1.trx_data_id_seq;
       v1          postgres    false    236    6                       0    0    trx_data_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE v1.trx_data_id_seq OWNED BY v1.trx_data.id;
          v1          postgres    false    237            �            1259    16474    trx_data_organisasi    TABLE     R  CREATE TABLE v1.trx_data_organisasi (
    id bigint NOT NULL,
    trx_data_id integer,
    ref_organisasi_id integer,
    organisasi text NOT NULL,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
 #   DROP TABLE v1.trx_data_organisasi;
       v1         heap    postgres    false    6            �            1259    16479    trx_data_organisasi_id_seq    SEQUENCE        CREATE SEQUENCE v1.trx_data_organisasi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE v1.trx_data_organisasi_id_seq;
       v1          postgres    false    6    238                       0    0    trx_data_organisasi_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE v1.trx_data_organisasi_id_seq OWNED BY v1.trx_data_organisasi.id;
          v1          postgres    false    239            �            1259    16480    trx_data_tag    TABLE     G  CREATE TABLE v1.trx_data_tag (
    id bigint NOT NULL,
    trx_data_id integer,
    ref_data_tag_id integer,
    data_tag text NOT NULL,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.trx_data_tag;
       v1         heap    postgres    false    6            �            1259    16485    trx_data_tag_id_seq    SEQUENCE     x   CREATE SEQUENCE v1.trx_data_tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE v1.trx_data_tag_id_seq;
       v1          postgres    false    6    240                       0    0    trx_data_tag_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE v1.trx_data_tag_id_seq OWNED BY v1.trx_data_tag.id;
          v1          postgres    false    241            �            1259    16486    trx_data_topik    TABLE     C  CREATE TABLE v1.trx_data_topik (
    id bigint NOT NULL,
    trx_data_id integer,
    ref_topik_id integer,
    topik text NOT NULL,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.trx_data_topik;
       v1         heap    postgres    false    6            �            1259    16491    trx_data_topik_id_seq    SEQUENCE     z   CREATE SEQUENCE v1.trx_data_topik_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE v1.trx_data_topik_id_seq;
       v1          postgres    false    242    6                       0    0    trx_data_topik_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE v1.trx_data_topik_id_seq OWNED BY v1.trx_data_topik.id;
          v1          postgres    false    243                       1259    24701    trx_jml_museum_bds_pengelola    TABLE     C  CREATE TABLE v1.trx_jml_museum_bds_pengelola (
    id bigint NOT NULL,
    mst_collection_id integer,
    tahun integer NOT NULL,
    kategori text NOT NULL,
    target text,
    realisasi text,
    sumber text,
    catatan text,
    kemendagri_kota_kode character varying(50) DEFAULT '32.71'::character varying NOT NULL,
    kemendagri_kota_nama text DEFAULT 'KOTA BOGOR'::text NOT NULL,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
 ,   DROP TABLE v1.trx_jml_museum_bds_pengelola;
       v1         heap    postgres    false    6                       1259    24700 #   trx_jml_museum_bds_pengelola_id_seq    SEQUENCE     �   CREATE SEQUENCE v1.trx_jml_museum_bds_pengelola_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 6   DROP SEQUENCE v1.trx_jml_museum_bds_pengelola_id_seq;
       v1          postgres    false    259    6                       0    0 #   trx_jml_museum_bds_pengelola_id_seq    SEQUENCE OWNED BY     c   ALTER SEQUENCE v1.trx_jml_museum_bds_pengelola_id_seq OWNED BY v1.trx_jml_museum_bds_pengelola.id;
          v1          postgres    false    258                       1259    24717    trx_jml_penduduk_bds_jk    TABLE     >  CREATE TABLE v1.trx_jml_penduduk_bds_jk (
    id bigint NOT NULL,
    mst_collection_id integer,
    tahun integer NOT NULL,
    kategori text NOT NULL,
    target text,
    realisasi text,
    sumber text,
    catatan text,
    kemendagri_kota_kode character varying(50) DEFAULT '32.71'::character varying NOT NULL,
    kemendagri_kota_nama text DEFAULT 'KOTA BOGOR'::text NOT NULL,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
 '   DROP TABLE v1.trx_jml_penduduk_bds_jk;
       v1         heap    postgres    false    6                       1259    24716    trx_jml_penduduk_bds_jk_id_seq    SEQUENCE     �   CREATE SEQUENCE v1.trx_jml_penduduk_bds_jk_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE v1.trx_jml_penduduk_bds_jk_id_seq;
       v1          postgres    false    261    6                       0    0    trx_jml_penduduk_bds_jk_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE v1.trx_jml_penduduk_bds_jk_id_seq OWNED BY v1.trx_jml_penduduk_bds_jk.id;
          v1          postgres    false    260            �            1259    24645    trx_laju_pertumbuhan_pdrb    TABLE     O  CREATE TABLE v1.trx_laju_pertumbuhan_pdrb (
    id bigint NOT NULL,
    mst_collection_id integer,
    tahun integer NOT NULL,
    kategori text NOT NULL,
    urutan character varying(5),
    target text,
    realisasi text,
    sumber text,
    catatan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    kemendagri_kota_kode character varying(50) DEFAULT '32.71'::character varying,
    kemendagri_kota_nama text DEFAULT 'KOTA BOGOR'::text
);
 )   DROP TABLE v1.trx_laju_pertumbuhan_pdrb;
       v1         heap    postgres    false    6            �            1259    24644     trx_laju_pertumbuhan_pdrb_id_seq    SEQUENCE     �   CREATE SEQUENCE v1.trx_laju_pertumbuhan_pdrb_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE v1.trx_laju_pertumbuhan_pdrb_id_seq;
       v1          postgres    false    6    255                       0    0     trx_laju_pertumbuhan_pdrb_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE v1.trx_laju_pertumbuhan_pdrb_id_seq OWNED BY v1.trx_laju_pertumbuhan_pdrb.id;
          v1          postgres    false    254            �            1259    16492 $   trx_luas_wilayah_kecamatan_kelurahan    TABLE       CREATE TABLE v1.trx_luas_wilayah_kecamatan_kelurahan (
    id bigint NOT NULL,
    mst_collection_id integer NOT NULL,
    tahun integer NOT NULL,
    kecamatan character varying(50) NOT NULL,
    jml_kelurahan integer NOT NULL,
    luas_wilayah double precision NOT NULL,
    satuan character varying(50),
    catatan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
 4   DROP TABLE v1.trx_luas_wilayah_kecamatan_kelurahan;
       v1         heap    postgres    false    6            �            1259    16497 +   trx_luas_wilayah_kecamatan_kelurahan_id_seq    SEQUENCE     �   CREATE SEQUENCE v1.trx_luas_wilayah_kecamatan_kelurahan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 >   DROP SEQUENCE v1.trx_luas_wilayah_kecamatan_kelurahan_id_seq;
       v1          postgres    false    244    6                       0    0 +   trx_luas_wilayah_kecamatan_kelurahan_id_seq    SEQUENCE OWNED BY     s   ALTER SEQUENCE v1.trx_luas_wilayah_kecamatan_kelurahan_id_seq OWNED BY v1.trx_luas_wilayah_kecamatan_kelurahan.id;
          v1          postgres    false    245                       1259    24685    trx_pdrb_adhk_2010_lap_usaha    TABLE     C  CREATE TABLE v1.trx_pdrb_adhk_2010_lap_usaha (
    id bigint NOT NULL,
    mst_collection_id integer,
    tahun integer NOT NULL,
    kategori text NOT NULL,
    target text,
    realisasi text,
    sumber text,
    catatan text,
    kemendagri_kota_kode character varying(50) DEFAULT '32.71'::character varying NOT NULL,
    kemendagri_kota_nama text DEFAULT 'KOTA BOGOR'::text NOT NULL,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
 ,   DROP TABLE v1.trx_pdrb_adhk_2010_lap_usaha;
       v1         heap    postgres    false    6                        1259    24684 #   trx_pdrb_adhk_2010_lap_usaha_id_seq    SEQUENCE     �   CREATE SEQUENCE v1.trx_pdrb_adhk_2010_lap_usaha_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 6   DROP SEQUENCE v1.trx_pdrb_adhk_2010_lap_usaha_id_seq;
       v1          postgres    false    6    257                       0    0 #   trx_pdrb_adhk_2010_lap_usaha_id_seq    SEQUENCE OWNED BY     c   ALTER SEQUENCE v1.trx_pdrb_adhk_2010_lap_usaha_id_seq OWNED BY v1.trx_pdrb_adhk_2010_lap_usaha.id;
          v1          postgres    false    256            �            1259    16498    trx_riwayat_data    TABLE     U  CREATE TABLE v1.trx_riwayat_data (
    id bigint NOT NULL,
    trx_data_id integer,
    mst_jenis_dok_id integer,
    sumber_data text NOT NULL,
    jenis_data character varying(100) NOT NULL,
    nama_data text NOT NULL,
    deskripsi_data text NOT NULL,
    format_data character varying(100) NOT NULL,
    media_type character varying(100),
    size_data character varying(100),
    ekstensi_data character varying(100),
    nama_file text,
    url_file_upload text,
    url_upload text,
    is_shared boolean DEFAULT false NOT NULL,
    webbappeda_visible boolean DEFAULT false NOT NULL,
    satudata_visible boolean DEFAULT false NOT NULL,
    catatan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
     DROP TABLE v1.trx_riwayat_data;
       v1         heap    postgres    false    6            �            1259    16506    trx_riwayat_data_id_seq    SEQUENCE     |   CREATE SEQUENCE v1.trx_riwayat_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE v1.trx_riwayat_data_id_seq;
       v1          postgres    false    6    246                       0    0    trx_riwayat_data_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE v1.trx_riwayat_data_id_seq OWNED BY v1.trx_riwayat_data.id;
          v1          postgres    false    247            �            1259    16507    users    TABLE     t  CREATE TABLE v1.users (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    email_verified_at timestamp(0) without time zone,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.users;
       v1         heap    postgres    false    6            �            1259    16512    users_id_seq    SEQUENCE     q   CREATE SEQUENCE v1.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
    DROP SEQUENCE v1.users_id_seq;
       v1          postgres    false    248    6                        0    0    users_id_seq    SEQUENCE OWNED BY     5   ALTER SEQUENCE v1.users_id_seq OWNED BY v1.users.id;
          v1          postgres    false    249            �           2604    16513    failed_jobs id    DEFAULT     h   ALTER TABLE ONLY v1.failed_jobs ALTER COLUMN id SET DEFAULT nextval('v1.failed_jobs_id_seq'::regclass);
 9   ALTER TABLE v1.failed_jobs ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    216    215            �           2604    16514    migrations id    DEFAULT     f   ALTER TABLE ONLY v1.migrations ALTER COLUMN id SET DEFAULT nextval('v1.migrations_id_seq'::regclass);
 8   ALTER TABLE v1.migrations ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    218    217            �           2604    16515    mst_collection id    DEFAULT     n   ALTER TABLE ONLY v1.mst_collection ALTER COLUMN id SET DEFAULT nextval('v1.mst_collection_id_seq'::regclass);
 <   ALTER TABLE v1.mst_collection ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    220    219            �           2604    16516    mst_dashboard id    DEFAULT     l   ALTER TABLE ONLY v1.mst_dashboard ALTER COLUMN id SET DEFAULT nextval('v1.mst_dashboard_id_seq'::regclass);
 ;   ALTER TABLE v1.mst_dashboard ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    222    221            �           2604    16517    mst_jenis_dok id    DEFAULT     l   ALTER TABLE ONLY v1.mst_jenis_dok ALTER COLUMN id SET DEFAULT nextval('v1.mst_jenis_dok_id_seq'::regclass);
 ;   ALTER TABLE v1.mst_jenis_dok ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    224    223            �           2604    16518    personal_access_tokens id    DEFAULT     ~   ALTER TABLE ONLY v1.personal_access_tokens ALTER COLUMN id SET DEFAULT nextval('v1.personal_access_tokens_id_seq'::regclass);
 D   ALTER TABLE v1.personal_access_tokens ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    227    226            �           2604    16519    ref_data_tag id    DEFAULT     j   ALTER TABLE ONLY v1.ref_data_tag ALTER COLUMN id SET DEFAULT nextval('v1.ref_data_tag_id_seq'::regclass);
 :   ALTER TABLE v1.ref_data_tag ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    229    228            �           2604    16520    ref_grup_dok id    DEFAULT     j   ALTER TABLE ONLY v1.ref_grup_dok ALTER COLUMN id SET DEFAULT nextval('v1.ref_grup_dok_id_seq'::regclass);
 :   ALTER TABLE v1.ref_grup_dok ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    231    230            �           2604    24620    ref_kode_wilayah id    DEFAULT     r   ALTER TABLE ONLY v1.ref_kode_wilayah ALTER COLUMN id SET DEFAULT nextval('v1.ref_kode_wilayah_id_seq'::regclass);
 >   ALTER TABLE v1.ref_kode_wilayah ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    253    252    253            �           2604    16521    ref_organisasi id    DEFAULT     n   ALTER TABLE ONLY v1.ref_organisasi ALTER COLUMN id SET DEFAULT nextval('v1.ref_organisasi_id_seq'::regclass);
 <   ALTER TABLE v1.ref_organisasi ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    233    232            �           2604    16522    ref_topik id    DEFAULT     d   ALTER TABLE ONLY v1.ref_topik ALTER COLUMN id SET DEFAULT nextval('v1.ref_topik_id_seq'::regclass);
 7   ALTER TABLE v1.ref_topik ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    235    234            �           2604    24604    trx_collection id    DEFAULT     n   ALTER TABLE ONLY v1.trx_collection ALTER COLUMN id SET DEFAULT nextval('v1.trx_collection_id_seq'::regclass);
 <   ALTER TABLE v1.trx_collection ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    250    251    251            �           2604    16523    trx_data id    DEFAULT     b   ALTER TABLE ONLY v1.trx_data ALTER COLUMN id SET DEFAULT nextval('v1.trx_data_id_seq'::regclass);
 6   ALTER TABLE v1.trx_data ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    237    236            �           2604    16524    trx_data_organisasi id    DEFAULT     x   ALTER TABLE ONLY v1.trx_data_organisasi ALTER COLUMN id SET DEFAULT nextval('v1.trx_data_organisasi_id_seq'::regclass);
 A   ALTER TABLE v1.trx_data_organisasi ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    239    238            �           2604    16525    trx_data_tag id    DEFAULT     j   ALTER TABLE ONLY v1.trx_data_tag ALTER COLUMN id SET DEFAULT nextval('v1.trx_data_tag_id_seq'::regclass);
 :   ALTER TABLE v1.trx_data_tag ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    241    240            �           2604    16526    trx_data_topik id    DEFAULT     n   ALTER TABLE ONLY v1.trx_data_topik ALTER COLUMN id SET DEFAULT nextval('v1.trx_data_topik_id_seq'::regclass);
 <   ALTER TABLE v1.trx_data_topik ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    243    242            �           2604    24704    trx_jml_museum_bds_pengelola id    DEFAULT     �   ALTER TABLE ONLY v1.trx_jml_museum_bds_pengelola ALTER COLUMN id SET DEFAULT nextval('v1.trx_jml_museum_bds_pengelola_id_seq'::regclass);
 J   ALTER TABLE v1.trx_jml_museum_bds_pengelola ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    258    259    259            �           2604    24720    trx_jml_penduduk_bds_jk id    DEFAULT     �   ALTER TABLE ONLY v1.trx_jml_penduduk_bds_jk ALTER COLUMN id SET DEFAULT nextval('v1.trx_jml_penduduk_bds_jk_id_seq'::regclass);
 E   ALTER TABLE v1.trx_jml_penduduk_bds_jk ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    260    261    261            �           2604    24648    trx_laju_pertumbuhan_pdrb id    DEFAULT     �   ALTER TABLE ONLY v1.trx_laju_pertumbuhan_pdrb ALTER COLUMN id SET DEFAULT nextval('v1.trx_laju_pertumbuhan_pdrb_id_seq'::regclass);
 G   ALTER TABLE v1.trx_laju_pertumbuhan_pdrb ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    255    254    255            �           2604    16527 '   trx_luas_wilayah_kecamatan_kelurahan id    DEFAULT     �   ALTER TABLE ONLY v1.trx_luas_wilayah_kecamatan_kelurahan ALTER COLUMN id SET DEFAULT nextval('v1.trx_luas_wilayah_kecamatan_kelurahan_id_seq'::regclass);
 R   ALTER TABLE v1.trx_luas_wilayah_kecamatan_kelurahan ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    245    244            �           2604    24688    trx_pdrb_adhk_2010_lap_usaha id    DEFAULT     �   ALTER TABLE ONLY v1.trx_pdrb_adhk_2010_lap_usaha ALTER COLUMN id SET DEFAULT nextval('v1.trx_pdrb_adhk_2010_lap_usaha_id_seq'::regclass);
 J   ALTER TABLE v1.trx_pdrb_adhk_2010_lap_usaha ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    256    257    257            �           2604    16528    trx_riwayat_data id    DEFAULT     r   ALTER TABLE ONLY v1.trx_riwayat_data ALTER COLUMN id SET DEFAULT nextval('v1.trx_riwayat_data_id_seq'::regclass);
 >   ALTER TABLE v1.trx_riwayat_data ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    247    246            �           2604    16529    users id    DEFAULT     \   ALTER TABLE ONLY v1.users ALTER COLUMN id SET DEFAULT nextval('v1.users_id_seq'::regclass);
 3   ALTER TABLE v1.users ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    249    248            �          0    16400    failed_jobs 
   TABLE DATA           ]   COPY v1.failed_jobs (id, uuid, connection, queue, payload, exception, failed_at) FROM stdin;
    v1          postgres    false    215   ��       �          0    16407 
   migrations 
   TABLE DATA           6   COPY v1.migrations (id, migration, batch) FROM stdin;
    v1          postgres    false    217           �          0    16411    mst_collection 
   TABLE DATA           )  COPY v1.mst_collection (id, nama_collection, judul, tipe, default_chart, referensi_data, catatan, route_name, table_name, created_by, updated_by, created_at, updated_at, ref_organisasi_id, organisasi, satuan, definisi, rumus_perhitungan, cara_memperoleh_data, urusan, is_multidimensi) FROM stdin;
    v1          postgres    false    219   &      �          0    16417    mst_dashboard 
   TABLE DATA           �   COPY v1.mst_dashboard (id, nama_dashboard, frame, link, catatan, is_hidden, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    221   �i      �          0    16424    mst_jenis_dok 
   TABLE DATA           �   COPY v1.mst_jenis_dok (id, jenis_dokumen, ref_grup_dok_id, keterangan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    223   0k      �          0    16430    password_reset_tokens 
   TABLE DATA           E   COPY v1.password_reset_tokens (email, token, created_at) FROM stdin;
    v1          postgres    false    225   �k      �          0    16435    personal_access_tokens 
   TABLE DATA           �   COPY v1.personal_access_tokens (id, tokenable_type, tokenable_id, name, token, abilities, last_used_at, expires_at, created_at, updated_at) FROM stdin;
    v1          postgres    false    226   �k      �          0    16441    ref_data_tag 
   TABLE DATA           l   COPY v1.ref_data_tag (id, data_tag, keterangan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    228   �k      �          0    16447    ref_grup_dok 
   TABLE DATA           p   COPY v1.ref_grup_dok (id, grup_dokumen, keterangan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    230   �w      �          0    24617    ref_kode_wilayah 
   TABLE DATA             COPY v1.ref_kode_wilayah (id, kemendagri_provinsi_kode, kemendagri_kota_kode, kemendagri_provinsi_nama, kemendagri_kota_nama, bps_provinsi_kode, bps_kota_kode, bps_provinsi_nama, bps_kota_nama, latitude, longitude, kode_pos, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    253   mx      �          0    16453    ref_organisasi 
   TABLE DATA           {   COPY v1.ref_organisasi (id, organisasi, singkatan, keterangan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    232   �{      �          0    16459 	   ref_topik 
   TABLE DATA           f   COPY v1.ref_topik (id, topik, keterangan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    234   }�      �          0    24601    trx_collection 
   TABLE DATA           �   COPY v1.trx_collection (id, mst_collection_id, tahun, target, realisasi, sumber, catatan, created_by, updated_by, created_at, updated_at, kemendagri_kota_kode, kemendagri_kota_nama) FROM stdin;
    v1          postgres    false    251   B�      �          0    16465    trx_data 
   TABLE DATA           -  COPY v1.trx_data (id, mst_jenis_dok_id, sumber_data, jenis_data, nama_data, deskripsi_data, format_data, media_type, size_data, ekstensi_data, nama_file, url_file_upload, url_upload, is_shared, webbappeda_visible, satudata_visible, catatan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    236   �.      �          0    16474    trx_data_organisasi 
   TABLE DATA           �   COPY v1.trx_data_organisasi (id, trx_data_id, ref_organisasi_id, organisasi, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    238   n�      �          0    16480    trx_data_tag 
   TABLE DATA           ~   COPY v1.trx_data_tag (id, trx_data_id, ref_data_tag_id, data_tag, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    240   ћ      �          0    16486    trx_data_topik 
   TABLE DATA           z   COPY v1.trx_data_topik (id, trx_data_id, ref_topik_id, topik, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    242   c�                0    24701    trx_jml_museum_bds_pengelola 
   TABLE DATA           �   COPY v1.trx_jml_museum_bds_pengelola (id, mst_collection_id, tahun, kategori, target, realisasi, sumber, catatan, kemendagri_kota_kode, kemendagri_kota_nama, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    259   ��                0    24717    trx_jml_penduduk_bds_jk 
   TABLE DATA           �   COPY v1.trx_jml_penduduk_bds_jk (id, mst_collection_id, tahun, kategori, target, realisasi, sumber, catatan, kemendagri_kota_kode, kemendagri_kota_nama, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    261   ��      �          0    24645    trx_laju_pertumbuhan_pdrb 
   TABLE DATA           �   COPY v1.trx_laju_pertumbuhan_pdrb (id, mst_collection_id, tahun, kategori, urutan, target, realisasi, sumber, catatan, created_by, updated_by, created_at, updated_at, kemendagri_kota_kode, kemendagri_kota_nama) FROM stdin;
    v1          postgres    false    255   Ӹ      �          0    16492 $   trx_luas_wilayah_kecamatan_kelurahan 
   TABLE DATA           �   COPY v1.trx_luas_wilayah_kecamatan_kelurahan (id, mst_collection_id, tahun, kecamatan, jml_kelurahan, luas_wilayah, satuan, catatan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    244   Ƽ      �          0    24685    trx_pdrb_adhk_2010_lap_usaha 
   TABLE DATA           �   COPY v1.trx_pdrb_adhk_2010_lap_usaha (id, mst_collection_id, tahun, kategori, target, realisasi, sumber, catatan, kemendagri_kota_kode, kemendagri_kota_nama, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    257   ��      �          0    16498    trx_riwayat_data 
   TABLE DATA           B  COPY v1.trx_riwayat_data (id, trx_data_id, mst_jenis_dok_id, sumber_data, jenis_data, nama_data, deskripsi_data, format_data, media_type, size_data, ekstensi_data, nama_file, url_file_upload, url_upload, is_shared, webbappeda_visible, satudata_visible, catatan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    246   ��      �          0    16507    users 
   TABLE DATA           q   COPY v1.users (id, name, email, email_verified_at, password, remember_token, created_at, updated_at) FROM stdin;
    v1          postgres    false    248   �%      !           0    0    failed_jobs_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('v1.failed_jobs_id_seq', 1, false);
          v1          postgres    false    216            "           0    0    migrations_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('v1.migrations_id_seq', 31, true);
          v1          postgres    false    218            #           0    0    mst_collection_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('v1.mst_collection_id_seq', 617, true);
          v1          postgres    false    220            $           0    0    mst_dashboard_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('v1.mst_dashboard_id_seq', 3, true);
          v1          postgres    false    222            %           0    0    mst_jenis_dok_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('v1.mst_jenis_dok_id_seq', 3, true);
          v1          postgres    false    224            &           0    0    personal_access_tokens_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('v1.personal_access_tokens_id_seq', 1, false);
          v1          postgres    false    227            '           0    0    ref_data_tag_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('v1.ref_data_tag_id_seq', 166, true);
          v1          postgres    false    229            (           0    0    ref_grup_dok_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('v1.ref_grup_dok_id_seq', 2, true);
          v1          postgres    false    231            )           0    0    ref_kode_wilayah_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('v1.ref_kode_wilayah_id_seq', 28, true);
          v1          postgres    false    252            *           0    0    ref_organisasi_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('v1.ref_organisasi_id_seq', 57, true);
          v1          postgres    false    233            +           0    0    ref_topik_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('v1.ref_topik_id_seq', 36, true);
          v1          postgres    false    235            ,           0    0    trx_collection_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('v1.trx_collection_id_seq', 2658, true);
          v1          postgres    false    250            -           0    0    trx_data_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('v1.trx_data_id_seq', 310, true);
          v1          postgres    false    237            .           0    0    trx_data_organisasi_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('v1.trx_data_organisasi_id_seq', 90, true);
          v1          postgres    false    239            /           0    0    trx_data_tag_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('v1.trx_data_tag_id_seq', 524, true);
          v1          postgres    false    241            0           0    0    trx_data_topik_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('v1.trx_data_topik_id_seq', 31, true);
          v1          postgres    false    243            1           0    0 #   trx_jml_museum_bds_pengelola_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('v1.trx_jml_museum_bds_pengelola_id_seq', 1, true);
          v1          postgres    false    258            2           0    0    trx_jml_penduduk_bds_jk_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('v1.trx_jml_penduduk_bds_jk_id_seq', 1, true);
          v1          postgres    false    260            3           0    0     trx_laju_pertumbuhan_pdrb_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('v1.trx_laju_pertumbuhan_pdrb_id_seq', 50, true);
          v1          postgres    false    254            4           0    0 +   trx_luas_wilayah_kecamatan_kelurahan_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('v1.trx_luas_wilayah_kecamatan_kelurahan_id_seq', 6, true);
          v1          postgres    false    245            5           0    0 #   trx_pdrb_adhk_2010_lap_usaha_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('v1.trx_pdrb_adhk_2010_lap_usaha_id_seq', 1, true);
          v1          postgres    false    256            6           0    0    trx_riwayat_data_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('v1.trx_riwayat_data_id_seq', 320, true);
          v1          postgres    false    247            7           0    0    users_id_seq    SEQUENCE SET     7   SELECT pg_catalog.setval('v1.users_id_seq', 1, false);
          v1          postgres    false    249                       2606    16531    failed_jobs failed_jobs_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY v1.failed_jobs
    ADD CONSTRAINT failed_jobs_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY v1.failed_jobs DROP CONSTRAINT failed_jobs_pkey;
       v1            postgres    false    215                       2606    16533 #   failed_jobs failed_jobs_uuid_unique 
   CONSTRAINT     Z   ALTER TABLE ONLY v1.failed_jobs
    ADD CONSTRAINT failed_jobs_uuid_unique UNIQUE (uuid);
 I   ALTER TABLE ONLY v1.failed_jobs DROP CONSTRAINT failed_jobs_uuid_unique;
       v1            postgres    false    215                       2606    16535    migrations migrations_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY v1.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY v1.migrations DROP CONSTRAINT migrations_pkey;
       v1            postgres    false    217            	           2606    16537 "   mst_collection mst_collection_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY v1.mst_collection
    ADD CONSTRAINT mst_collection_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY v1.mst_collection DROP CONSTRAINT mst_collection_pkey;
       v1            postgres    false    219                       2606    16539     mst_dashboard mst_dashboard_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY v1.mst_dashboard
    ADD CONSTRAINT mst_dashboard_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY v1.mst_dashboard DROP CONSTRAINT mst_dashboard_pkey;
       v1            postgres    false    221                       2606    16541     mst_jenis_dok mst_jenis_dok_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY v1.mst_jenis_dok
    ADD CONSTRAINT mst_jenis_dok_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY v1.mst_jenis_dok DROP CONSTRAINT mst_jenis_dok_pkey;
       v1            postgres    false    223                       2606    16543 0   password_reset_tokens password_reset_tokens_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY v1.password_reset_tokens
    ADD CONSTRAINT password_reset_tokens_pkey PRIMARY KEY (email);
 V   ALTER TABLE ONLY v1.password_reset_tokens DROP CONSTRAINT password_reset_tokens_pkey;
       v1            postgres    false    225                       2606    16545 2   personal_access_tokens personal_access_tokens_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY v1.personal_access_tokens
    ADD CONSTRAINT personal_access_tokens_pkey PRIMARY KEY (id);
 X   ALTER TABLE ONLY v1.personal_access_tokens DROP CONSTRAINT personal_access_tokens_pkey;
       v1            postgres    false    226                       2606    16547 :   personal_access_tokens personal_access_tokens_token_unique 
   CONSTRAINT     r   ALTER TABLE ONLY v1.personal_access_tokens
    ADD CONSTRAINT personal_access_tokens_token_unique UNIQUE (token);
 `   ALTER TABLE ONLY v1.personal_access_tokens DROP CONSTRAINT personal_access_tokens_token_unique;
       v1            postgres    false    226                       2606    16549    ref_data_tag ref_data_tag_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY v1.ref_data_tag
    ADD CONSTRAINT ref_data_tag_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY v1.ref_data_tag DROP CONSTRAINT ref_data_tag_pkey;
       v1            postgres    false    228                       2606    16551    ref_grup_dok ref_grup_dok_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY v1.ref_grup_dok
    ADD CONSTRAINT ref_grup_dok_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY v1.ref_grup_dok DROP CONSTRAINT ref_grup_dok_pkey;
       v1            postgres    false    230            0           2606    24624 &   ref_kode_wilayah ref_kode_wilayah_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY v1.ref_kode_wilayah
    ADD CONSTRAINT ref_kode_wilayah_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY v1.ref_kode_wilayah DROP CONSTRAINT ref_kode_wilayah_pkey;
       v1            postgres    false    253                       2606    16553 "   ref_organisasi ref_organisasi_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY v1.ref_organisasi
    ADD CONSTRAINT ref_organisasi_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY v1.ref_organisasi DROP CONSTRAINT ref_organisasi_pkey;
       v1            postgres    false    232                       2606    16555    ref_topik ref_topik_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY v1.ref_topik
    ADD CONSTRAINT ref_topik_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY v1.ref_topik DROP CONSTRAINT ref_topik_pkey;
       v1            postgres    false    234            .           2606    24608 "   trx_collection trx_collection_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY v1.trx_collection
    ADD CONSTRAINT trx_collection_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY v1.trx_collection DROP CONSTRAINT trx_collection_pkey;
       v1            postgres    false    251                        2606    16557 ,   trx_data_organisasi trx_data_organisasi_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY v1.trx_data_organisasi
    ADD CONSTRAINT trx_data_organisasi_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY v1.trx_data_organisasi DROP CONSTRAINT trx_data_organisasi_pkey;
       v1            postgres    false    238                       2606    16559    trx_data trx_data_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY v1.trx_data
    ADD CONSTRAINT trx_data_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY v1.trx_data DROP CONSTRAINT trx_data_pkey;
       v1            postgres    false    236            "           2606    16561    trx_data_tag trx_data_tag_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY v1.trx_data_tag
    ADD CONSTRAINT trx_data_tag_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY v1.trx_data_tag DROP CONSTRAINT trx_data_tag_pkey;
       v1            postgres    false    240            $           2606    16563 "   trx_data_topik trx_data_topik_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY v1.trx_data_topik
    ADD CONSTRAINT trx_data_topik_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY v1.trx_data_topik DROP CONSTRAINT trx_data_topik_pkey;
       v1            postgres    false    242            6           2606    24710 >   trx_jml_museum_bds_pengelola trx_jml_museum_bds_pengelola_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY v1.trx_jml_museum_bds_pengelola
    ADD CONSTRAINT trx_jml_museum_bds_pengelola_pkey PRIMARY KEY (id);
 d   ALTER TABLE ONLY v1.trx_jml_museum_bds_pengelola DROP CONSTRAINT trx_jml_museum_bds_pengelola_pkey;
       v1            postgres    false    259            8           2606    24726 4   trx_jml_penduduk_bds_jk trx_jml_penduduk_bds_jk_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY v1.trx_jml_penduduk_bds_jk
    ADD CONSTRAINT trx_jml_penduduk_bds_jk_pkey PRIMARY KEY (id);
 Z   ALTER TABLE ONLY v1.trx_jml_penduduk_bds_jk DROP CONSTRAINT trx_jml_penduduk_bds_jk_pkey;
       v1            postgres    false    261            2           2606    24652 8   trx_laju_pertumbuhan_pdrb trx_laju_pertumbuhan_pdrb_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY v1.trx_laju_pertumbuhan_pdrb
    ADD CONSTRAINT trx_laju_pertumbuhan_pdrb_pkey PRIMARY KEY (id);
 ^   ALTER TABLE ONLY v1.trx_laju_pertumbuhan_pdrb DROP CONSTRAINT trx_laju_pertumbuhan_pdrb_pkey;
       v1            postgres    false    255            &           2606    16565 N   trx_luas_wilayah_kecamatan_kelurahan trx_luas_wilayah_kecamatan_kelurahan_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY v1.trx_luas_wilayah_kecamatan_kelurahan
    ADD CONSTRAINT trx_luas_wilayah_kecamatan_kelurahan_pkey PRIMARY KEY (id);
 t   ALTER TABLE ONLY v1.trx_luas_wilayah_kecamatan_kelurahan DROP CONSTRAINT trx_luas_wilayah_kecamatan_kelurahan_pkey;
       v1            postgres    false    244            4           2606    24694 >   trx_pdrb_adhk_2010_lap_usaha trx_pdrb_adhk_2010_lap_usaha_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY v1.trx_pdrb_adhk_2010_lap_usaha
    ADD CONSTRAINT trx_pdrb_adhk_2010_lap_usaha_pkey PRIMARY KEY (id);
 d   ALTER TABLE ONLY v1.trx_pdrb_adhk_2010_lap_usaha DROP CONSTRAINT trx_pdrb_adhk_2010_lap_usaha_pkey;
       v1            postgres    false    257            (           2606    16567 &   trx_riwayat_data trx_riwayat_data_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY v1.trx_riwayat_data
    ADD CONSTRAINT trx_riwayat_data_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY v1.trx_riwayat_data DROP CONSTRAINT trx_riwayat_data_pkey;
       v1            postgres    false    246            *           2606    16569    users users_email_unique 
   CONSTRAINT     P   ALTER TABLE ONLY v1.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);
 >   ALTER TABLE ONLY v1.users DROP CONSTRAINT users_email_unique;
       v1            postgres    false    248            ,           2606    16571    users users_pkey 
   CONSTRAINT     J   ALTER TABLE ONLY v1.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 6   ALTER TABLE ONLY v1.users DROP CONSTRAINT users_pkey;
       v1            postgres    false    248                       1259    16572 8   personal_access_tokens_tokenable_type_tokenable_id_index    INDEX     �   CREATE INDEX personal_access_tokens_tokenable_type_tokenable_id_index ON v1.personal_access_tokens USING btree (tokenable_type, tokenable_id);
 H   DROP INDEX v1.personal_access_tokens_tokenable_type_tokenable_id_index;
       v1            postgres    false    226    226            9           2606    16623 7   mst_collection mst_collection_ref_organisasi_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.mst_collection
    ADD CONSTRAINT mst_collection_ref_organisasi_id_foreign FOREIGN KEY (ref_organisasi_id) REFERENCES v1.ref_organisasi(id);
 ]   ALTER TABLE ONLY v1.mst_collection DROP CONSTRAINT mst_collection_ref_organisasi_id_foreign;
       v1          postgres    false    3354    219    232            :           2606    16573 3   mst_jenis_dok mst_jenis_dok_ref_grup_dok_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.mst_jenis_dok
    ADD CONSTRAINT mst_jenis_dok_ref_grup_dok_id_foreign FOREIGN KEY (ref_grup_dok_id) REFERENCES v1.ref_grup_dok(id);
 Y   ALTER TABLE ONLY v1.mst_jenis_dok DROP CONSTRAINT mst_jenis_dok_ref_grup_dok_id_foreign;
       v1          postgres    false    223    3352    230            B           2606    24609 7   trx_collection trx_collection_mst_collection_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_collection
    ADD CONSTRAINT trx_collection_mst_collection_id_foreign FOREIGN KEY (mst_collection_id) REFERENCES v1.mst_collection(id);
 ]   ALTER TABLE ONLY v1.trx_collection DROP CONSTRAINT trx_collection_mst_collection_id_foreign;
       v1          postgres    false    219    3337    251            ;           2606    16578 *   trx_data trx_data_mst_jenis_dok_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_data
    ADD CONSTRAINT trx_data_mst_jenis_dok_id_foreign FOREIGN KEY (mst_jenis_dok_id) REFERENCES v1.mst_jenis_dok(id);
 P   ALTER TABLE ONLY v1.trx_data DROP CONSTRAINT trx_data_mst_jenis_dok_id_foreign;
       v1          postgres    false    236    223    3341            <           2606    16583 A   trx_data_organisasi trx_data_organisasi_ref_organisasi_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_data_organisasi
    ADD CONSTRAINT trx_data_organisasi_ref_organisasi_id_foreign FOREIGN KEY (ref_organisasi_id) REFERENCES v1.ref_organisasi(id);
 g   ALTER TABLE ONLY v1.trx_data_organisasi DROP CONSTRAINT trx_data_organisasi_ref_organisasi_id_foreign;
       v1          postgres    false    232    238    3354            =           2606    16588 1   trx_data_tag trx_data_tag_ref_data_tag_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_data_tag
    ADD CONSTRAINT trx_data_tag_ref_data_tag_id_foreign FOREIGN KEY (ref_data_tag_id) REFERENCES v1.ref_data_tag(id);
 W   ALTER TABLE ONLY v1.trx_data_tag DROP CONSTRAINT trx_data_tag_ref_data_tag_id_foreign;
       v1          postgres    false    240    3350    228            >           2606    16593 2   trx_data_topik trx_data_topik_ref_topik_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_data_topik
    ADD CONSTRAINT trx_data_topik_ref_topik_id_foreign FOREIGN KEY (ref_topik_id) REFERENCES v1.ref_topik(id);
 X   ALTER TABLE ONLY v1.trx_data_topik DROP CONSTRAINT trx_data_topik_ref_topik_id_foreign;
       v1          postgres    false    242    234    3356            E           2606    24711 S   trx_jml_museum_bds_pengelola trx_jml_museum_bds_pengelola_mst_collection_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_jml_museum_bds_pengelola
    ADD CONSTRAINT trx_jml_museum_bds_pengelola_mst_collection_id_foreign FOREIGN KEY (mst_collection_id) REFERENCES v1.mst_collection(id);
 y   ALTER TABLE ONLY v1.trx_jml_museum_bds_pengelola DROP CONSTRAINT trx_jml_museum_bds_pengelola_mst_collection_id_foreign;
       v1          postgres    false    259    3337    219            F           2606    24727 I   trx_jml_penduduk_bds_jk trx_jml_penduduk_bds_jk_mst_collection_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_jml_penduduk_bds_jk
    ADD CONSTRAINT trx_jml_penduduk_bds_jk_mst_collection_id_foreign FOREIGN KEY (mst_collection_id) REFERENCES v1.mst_collection(id);
 o   ALTER TABLE ONLY v1.trx_jml_penduduk_bds_jk DROP CONSTRAINT trx_jml_penduduk_bds_jk_mst_collection_id_foreign;
       v1          postgres    false    219    261    3337            C           2606    24653 M   trx_laju_pertumbuhan_pdrb trx_laju_pertumbuhan_pdrb_mst_collection_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_laju_pertumbuhan_pdrb
    ADD CONSTRAINT trx_laju_pertumbuhan_pdrb_mst_collection_id_foreign FOREIGN KEY (mst_collection_id) REFERENCES v1.mst_collection(id);
 s   ALTER TABLE ONLY v1.trx_laju_pertumbuhan_pdrb DROP CONSTRAINT trx_laju_pertumbuhan_pdrb_mst_collection_id_foreign;
       v1          postgres    false    219    255    3337            ?           2606    16598 c   trx_luas_wilayah_kecamatan_kelurahan trx_luas_wilayah_kecamatan_kelurahan_mst_collection_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_luas_wilayah_kecamatan_kelurahan
    ADD CONSTRAINT trx_luas_wilayah_kecamatan_kelurahan_mst_collection_id_foreign FOREIGN KEY (mst_collection_id) REFERENCES v1.mst_collection(id) ON UPDATE CASCADE ON DELETE CASCADE;
 �   ALTER TABLE ONLY v1.trx_luas_wilayah_kecamatan_kelurahan DROP CONSTRAINT trx_luas_wilayah_kecamatan_kelurahan_mst_collection_id_foreign;
       v1          postgres    false    244    3337    219            D           2606    24695 S   trx_pdrb_adhk_2010_lap_usaha trx_pdrb_adhk_2010_lap_usaha_mst_collection_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_pdrb_adhk_2010_lap_usaha
    ADD CONSTRAINT trx_pdrb_adhk_2010_lap_usaha_mst_collection_id_foreign FOREIGN KEY (mst_collection_id) REFERENCES v1.mst_collection(id);
 y   ALTER TABLE ONLY v1.trx_pdrb_adhk_2010_lap_usaha DROP CONSTRAINT trx_pdrb_adhk_2010_lap_usaha_mst_collection_id_foreign;
       v1          postgres    false    257    3337    219            @           2606    16603 :   trx_riwayat_data trx_riwayat_data_mst_jenis_dok_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_riwayat_data
    ADD CONSTRAINT trx_riwayat_data_mst_jenis_dok_id_foreign FOREIGN KEY (mst_jenis_dok_id) REFERENCES v1.mst_jenis_dok(id);
 `   ALTER TABLE ONLY v1.trx_riwayat_data DROP CONSTRAINT trx_riwayat_data_mst_jenis_dok_id_foreign;
       v1          postgres    false    223    3341    246            A           2606    16608 5   trx_riwayat_data trx_riwayat_data_trx_data_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_riwayat_data
    ADD CONSTRAINT trx_riwayat_data_trx_data_id_foreign FOREIGN KEY (trx_data_id) REFERENCES v1.trx_data(id) ON UPDATE CASCADE ON DELETE CASCADE;
 [   ALTER TABLE ONLY v1.trx_riwayat_data DROP CONSTRAINT trx_riwayat_data_trx_data_id_foreign;
       v1          postgres    false    3358    236    246            �      x������ � �      �     x�e�ۮ� @��c&�x��I
m��`z��gS�G�4MӇ��W�9�
�T��Q��:Z�냊�s� �g2�Cx/�(o��*.��Q$�U�Q�7ozp֨��x��)H�����Yf��{�5�$���Ǭ���nz{Sw���YF"�����#�)D������@��RB~h\^C���y��t���3X���`�����	�Ū���kC���0�� Pֿ�=k9\��o���:UXA}����-ڛĞ���Q
J�rl���"M[���~x�:n�`����6��|j�����E��� e��[uP��Q��m�'
2�?�z��s!�l?ߦl8*m̩�*踒v	_1?.�N��"�k�--נL�ZͰ<kT_%6��qZ]�0��W�a(3�Dy����9e'�?a��\��\I3=�����{��\p���D����sM�9�S�R����� zf�LlQRNb�(��Hy���)�X��]	�ݖ�䨍����L���ݺ�m.��tho..	f5����c���a���ɤ      �      x�ͽ�n���7�ٹ
��hƢH��daǓ��3��� � ���iꅐ���i�a�p�d�S�,R��,��tUVYU��w���ُ�j������W�ٿ����?���y��3�1E��z��I�9�p0�i0�^����K��g7�:���_8�l�gk5�`��q�XV��%����s���`�~�S��g?��,�}����z�(v�GYe;��>�}˶Y�����js�a4��h|�L�4����ƽZl��>{��xZ��>���>��[>��M�vF��-޲9���삻���e����j�-����_��:z0�8�������K�2��h����7AV��?���m���l3���$�q�X�A���K;�x�v��]�hx� ���x*�\6�,x��/���J�����_���Z�rw����8��,_�B��i�0ހ���i��M�H���X��~G0{�b����a�oi2���Ӧ�K�w�W���f�������3������ܯ�e;}��-0��@�7�3���Gub؉�N:1ى�Nޅ�s!�\Ʊ����(۷���U���۝����9��;˓��<w٪�%X��W[\����8��G�`�;*��'^Z
G�xo���)A���N����>������*���+x��l���5��#�e�����3�1Ռ�f�cqD�e��9��b�!oV�:��q�G͗=~�f����ҝ1?���Yp~��<n�齅-���V因�a(������K;�5=o�e���\��"��������f��wy���a��Oz\��"�e�g|3�s~ e����ɿ��4�����KK�y*��O��na�O|����3���781~̖ٺ׀�����+ߡ���w+s�fk>���N��.�ٲ/�oc8)sX�%��k�.�\�=�����	�S��1�T3��3�y���t���W��v�}��ӿ�9���̇O����+N'��?�OY���	��!�O{.B<U�f�3~����|���ﱶ{J�+\/:�W�WȻ��N�c؎�wՎA;�QɅ�?�����ko�x�e�O;��i�&���_��ˬ���ŷ��V\4Xg��0\��&����K��K���W�1�\2��6=�C�Z8��2[�����m�O��r�i{k��}CHk�#��mҢH�|R8��au�X��K���ͣS08�-�'6\�C	�c/�w���t�W|����A�-Q���p�w��s��Iq|�l_}��\#|w�^�I�A�$�ڿ**�s�����o�"��E��i?�]���\c%�;����L"9�]�(`��oY�� ?f�����7�A>*g�(�Aw.Ј�\����w�t��k�S}��à�>��91�^Zw!���{�s�N��s��x���]��Q7�>��Ym+�F�YUf����ǻU�v��%��~*~��K�{�Gn�#xX��6�]�ї�	�>��~��U�/��a!ڇK�>|�������nĨQ���\��/m"^Tm�>E��0RS A`��
:|��t�
���$R�v9J�O\��:v�.7͈�j^Z����T�i"��3��|�_�ԍk3946���ڠyN���r�rz��<h�$��ϸ��o�[�X�Jh��'�ۘ���G�O�k\�/���m���D�C�Jο��� �aa� �j�{���3��Le4;f�É�+���=H�לێ���W���k �P��X�>V���V�D��R�whu9�7F.��姆o�^C��;�P���+����Ǚ��3�U�N ����r'���t|݆�q���pԔ&�HS��T���({?�e����ص-����(����5SsX��a����cxfЂE)������0�=�{�kA�ͦ���h�.������:4�X�y���S��Z��������������]��~B8�'�
+�n˷�\�1���!= �ϳ��[������_f�e�WJ���/#��2ɵ��;�k^�%��0/`<����+W����ڹP��ݜ�r��_�Uz~<�^Z_��p�B�Y����l������6.���K ��w�WtZ�U��v���G�#�����HnA\��\��H]���dG�WtdԱ��:p�8��b�8�j��x���ȹ�{��{�%Ӕ�~�)�E� �t����2yi�k�Fg�����i�v�_���r~���cH�9�\���P%Z3j���5ӭ[^ W�j/i}w�r��/�+���5aǾ+�ڡ��-�*�]y�sC&C/��&�NMS��b&%���e�~y����`��)���ki)\B��z�S�wl7�IKS;1މ�ZLmS�c7iMmC��k�)h6�r:쪜*qQ���l���pE\�Y%��ì؈O\���3�;3љ���a�e���k�^ڱ�pt���K�k��z����ֵ;E�pھ������M��/p���A�+��O��\%�X���d7NFg���
O�j5�^Q�ހy�Y-�I��5�6��'p��t~��G��r���*��%�l¥�ąMީi�0��G^Z2n�v~�7)׾��޷��J�>��1���4Qt��^Z��1�%~��W�y$���ɞܐ4{ͥ�����[h,�?���p�!R��`�@�/-�'�GrÝ�-{�l�|�~ڱ�ں#M�9jnӧ�F����o�o���*��7?CS8H�w�g�;�ߛ�&.��uxi�zxGM���X<��K��(�������ف��IhU7�~��u�y����:�̅����?|$�9��ȶ�߸��,7������Z-���e�7���I	p���d��[���6�m����o\P�j��駇�g�	��=j���9�A��%S�~Z��]��C84�w��(_Kjr���������oˤu9��l���cU榮���f���|*�3����翡EN�
���4�沿3�XlF��4}�V32��l/	��1���Qp굡|s������f�g�9r���p��q�6�25�E_~��΋>R�.�>Bh�y�P��f�w�Xyz]�����ტ?�	Œ\��g5'0I8F��F�l���P�C��p��U���>K~��}F�H!���,�
�phG�^sx�=�Cw�b��i�s:.ȁqq]-=AH/��Po���&����	�yF���#Cǩ�7ȁ�94�<�Q]�!���������=�5U耥�2|����g����M[��C�rh��)I��9���b6���O;��}��C��p6���?���okR����l�Z��	4�.�������ʎ�m�U���a;_�Ǧ�d�:�g�����b!:<�(���]�O�k
�'_�o�:����{vh>9�%Ö�8��m�������{��X{ʖ&#�Z}Z��������x��!�7��3�y������v0*h˝���T~����o����E�J~�FT���.P�.��#<d[͍�M��	���$#��57Åf�`Ă)F̌��,�÷��e2��j��k��t*���0��w��}D_�2"
B3�D.)�o��5.���^�_��M�ZX�p���hWJ���pO��ْ�1�LM�L0�Sܵ%L̙��#L��%�)	��=��XފO�T^��%���s�g�Ac>��}��:{�Y�Z>�y������(�½�W�Ç��~�E�WI	W��p%��ߍx9K�G��e�WD�	���2�W����*߆��{l�
Y�Te��~�����B���
��V�2XL�o{�G�3�`�V���~�K���.�V��=��04��ç�E/-Ѻ�Ol�?3�zZ�K2������u>��xs��)�Ba�=h͐�᥽W,z<����^F����,�1���-vZi�ʷ\-_W�����ȃ��OX��[�g�&�	Ȧ#��1SkQG��0˷�
���`�&�x��"y��"þ�S0�k��S
ç0�Oa�)L?��R�=��^Z���ے
����#�FHy�&^�M�ȁt&�͚W����    ~�n��DI\`DE�;��`��k;��)�.�ΰ7J������@M)yQF]PԄ.Lu9����a�D��*q>�a������f�HTl�C{+E2I� j���~�a���6�����6���`���r0��N����'+ ��k�=V-1� v�!0t>Π��	���:�qk<��u��]cю�\M�sŅG���=��:\ }y�=���J�*��ܬ
A;��7���y���|��c��M��I�Á�c�h�����̮d���«E�C�a.&�#}4��<+.�g��cU�)������������+��.�n���O�l�X���~�W!�ڍ��?�ٮ�?`Hm��aGp$F^Z�͏�W������gȴp��9�V/YA֖�MZ�<ɁW��_��������(��N�v���r�6��;:�;�o�F�����k��kJ: f-���0�},����$J�Q�/Tz�t��%��w3���~:�W��9�s�J^k��m�s�`7G+�J� a��9����N�Z���ǆO<E2�q��d�.�^g�X�X�))�9!��ˆ�{H˗�2���D���M����߭�(�b~P���>{2x�:Zʎ!x�����`����Q+Uk��o-TMl� kE �p����d/ob��J̭��u�'�O�]�_��q�Է/$ӂ�E�*��j_�z̸Ƃ�l�:T��>R�������pP��!�,u�H��KQХ�Ș~�9�Q�s��|Nsj���Z׌����y%�v��5���iҺe'�����z�Yn^r�C�}��c35���m r�){���b�s�w"��hfKn3h�jf�zv�5(;��W��)��w�[����3����H� �op ���N�be��G��Чk��{-�'?<�rF�0jxvol�ϰ?���c�!��r0�Ҏ��Gi�W��h��a�v���o03ϻ�`v��op0O2�C�E���5�����E?Nҳ���]V�/��>J2z,����4�1�}���2W�fE���|�_���5LKX���&�ua�
W�W����J�RvƁ��6�3ݟa��+��Ž�}c6�A;�^��SXt�|����e˷0�K屗�^N�qdg9=P�Q����G..��u�a�~{!����y�pI�.�A�n	�FlI���3I���Dߚ�Y�|W�kS���И����Y�z/����n��W-���sK���6�c��<u�l"�a�Q(4�Յ߉�l�B�)�ء/C���i���W��L���l{��F�R����s�I[��u�_�o�ݥ����ï��I���+�����7�-�k.�� �ı�_tCZ0�~�`��	�S+����	�_v�F\gߚ�3���tg����0Z���a��	l�}Lq�f�����W�Tg��_��cEK'�zK�8a
��ogՁ�s��7v ��C��[������'OZ?)�[�,�v����'�����O�?j�z��I}�?�r�k�^���ڰ����6�th�ëŒ��W�k�Q�Y���nw\^�H"��?r�m��Y��G~�7�1������S����2����^����r`�5���*�$�vr��K&$ T}M;�I����	�9L7�0yd�։DVd�IKG:��y�L��*���	�{��ma�A/�a����%���Q��2v���!j�v]Ѩ���O��"��/xo.�5���qQ�b��D�"�o:�:��b]n�Y��&�� a� �~�;�4^���v *Y��z�XkH��Oe�f�/��!)B���H

*Λ��gnm�X%�>aLٖ@;Oʁ4���e0

y:u�u���=i�z���U?�xtt���a����c�}$���W-�*��ğH��p.�;F2��^��%F�z�ďM�`�&�%RM�G|���c9��Y�߮�n_G��.�SL��W)zio�ل��տdF~�w&��RP@��ҕ
�^;� ~�{F ~�{���ߴ�>��=?�v�SjDf�
zȩY%��N�I��$Lwb�
q[�WS6L�^����+$ �vf!z<<���E������m���	[�I�E+�"��O-ÞX�F�G�����.�oF�XÑ"�B"
��'�t���C��er����Kk��}��Ar�̷)�# P2o��epU����h>Π����I�F���ކ��9-Pf�W����@~��Ѝ���;��c���٘���/7O���:'D� D��܌"��X��ȒB0_{��C��X��9�#S�q��s������0Ʊ�, �)��b/�wP�dhF�P
ʣ�,���-H�����l���PR7�G�N�m�%�$��9�^����Ǎ�>F�UCԸhhdQ���&����壺�*����I}�V̨�=	�pjգ1h��x�����A/�E
~ʶ�'C�ҋ��B�M�wl1*Q�����
�;�uc|��  �u+ڱQ�I4n�
0���tV �0ȏ��nZy���N]�;�#�S�C��8B�ϗ�u���,�0DO9�C�F�|�J/�g3�l�]�೙�l�S>�)��k�S���i�* ��]�_��)[Ջ���r�`2��5i��~����\L�ҋ?�, �^�_gǅ�+[\꥽�P�₿����pcg�h�Hby�Zo����=���rH�wPq��2��2���Y���"� d�C`��'v�����d�%�-�h�,r�C�֠�#X��@� ���'��ֹT+��R����m���{X���Z��T}:�jb�٠��g�0<��${{C!���/���}PD�<
;���4a�D���曪�E��1nf�N5���k�U��'�����d�no^5���Su�8�S_�s�dp�Cu����-�
�-�ֱ�;�����wG��b��V������f��c���;W��|-���{o﯍���*H���0_͌�m�c��[1��x�msձ��T��H9�"!�K/��.�M�E���@��[��juTb�?/�K/�;���$W�_��	 YGN�?E���gVm?U6��"M��7 7ҵ ��@�h8�2 ���t�-��c��W�G�ˤ��<�Ff�9��z!�U#~j�FxI�_��Ks�C4�j���Y�p�����/E9{�-�ZE|L����q�>�\<ˉM0�=����D:F�3��8����Ĥy��/Y�c�i|�c��M�7����n~��"��숲�`�[��ͳM}�x:0j7=.v�f��j!�J(T�g�^�$[�7��p�����n�+�t`�����Jp���Y�c��D�n��:��F1�V⛼X�?�n&�
%,�yN��+�
J����YpkV��e��i���r��/�P���gO�3�0q��g��V>.���:�n���1%>��l�TK�������+hQ +uڥ��j�h-���4Vbo84��1^�R�Fy0�|�(�Ҏ�f�&d�� ˛�CU�@h�+���Ǒ��IZc��70n+������w,8`��[��PŨ86c�L�� cK4Lҩ6�I�U"�;0����(���g$N�v�,��7���B:�l��6D���d�g!}��Be��0ҩ{�@�A�X�%�T�8�JTn��~� 
�R?m��a���^��f2��|4��R2�4�F��~i9����C�u"Ll4W
����bL�V|��k�6c�����:�x���� �_�ft��ha�hrө����D^Z��)�"�e�/N��Sˋl��Ɏ� ��2"4RY�{F?����5�^�A�-c���8i�7�Q�����.O�(�A�yy��q�]햹̤<��=�#`LG.҇�5��� f����t�e�y*|~(%�:���y�����*���_���a��D~��-5�L�$?����|�O$��D/ʅx
��Xꗙ�IOa�f=���y5�d��d`��c  }�u2��e$������XQ��M��|໌�|�;�1,�6/*����lh����o��~[l��ةA�
 L�̤u0P�S;�FXlV�R�����VQ���,3�A�R�    $9�8��@5p�m�`@y�{v�os���,��r<�
��S�{h��B��}m
!���e�[X�g�n�^b7hQҎ����+Zf�Nn6�lr�;$K���ķ��(�' }�8\�\-�7f�iC/�G�;��Ɔ�M�`�nr��+9���A�J�8���u��ik��R�*�{�]9��x�^����o��u�ы��)��Ҧdi����H�G�F훭�\�2��ZW��t �/CzU�ճY.,'��|A�Ap�*QM3���|Y��8Fq0���&�Kd�7y<1@rh���+]�&�O���8¿D�UHSXs�"��bo��'h̛������
���2������^H�K���TB�o� ̨Y�Š�c�����%gwrnכ��^��z��@�\�I;B�cr/��J@G��S�c2c���y�SGL��o��8=CC�=]ӹ���]܄��� "
7�Ȫ/h�z�bc������*B�Q7؉���='��/>�������)�6M.���vd�T�V��d��+�F7�x��n�K<�m��K�*��ah��yO#V�'����^uf-"�S��6�wɱ�B�i�,ݞ�\v.�b�g8L�����M,�����b��z�t�6Q:��3�T$��w���Ų� ��2�+�o��yL�|Oի�����v�嗾X�\	�h�N4��D�J�Hr��|��Y!��!l��&�O+��QD�2���t��`襽��GV�M]Y;pK�6��/Fh-�t�B&>>�?NT����g���e��z�4����r~�EB�	b���3Q�m-�ı��^Oi
��r��E{�:��irFZ���0DĪ�r2eM�
+;��.�2��	�%Rd3�V�h֬�ĀS�uUR܃A.�Ð����v�X���"�Lu�����:��,ɪ[�s�g�P�^fl�m6i}BH�s�@�VX�?d� ������6�*�ą���K�`���7�	�Nj�`V7K��ծZ�1�TS������+Gi�
~���f+w2¶Sq�!�5��)��_��_j�R1]�����
��
��ݰK�,��S�d��*�����3b�8K�,[Lc�Y2ƤA�	���7[m_�+X�.e��C���T���.v*�hڑ��8M��l_A� ��Py�����ɍye��PM�n�-(�~���lV���ޣi�����u�Ȁx�.0�����]���ջƦ�ݤ��
'�}���°<�f;�t�W�+�@/7h������Vg�֓�%�=��w ����*�
�P����
�ٺ%O	u�<��	�I�<��i�=7���C�e2i��.�ȲD�J�:E���ʂ�lmW�lN��e<\�P/�C��?�i���8ٰ&�S��6��a~͊}%<�^�'��#��:���<���²��2>vE�e��ء�1�\��=5Y��r��η0��|��0Oȿ4��| b�:C�����]�|����u[+!T���,��h���p�M�,`����E��(%�6R��J�v�v�G;���E�E�^AJX(��΁I�¥̪�R*�j��[.4��P~�@3�����-��w:m���N�}lZsLZ��	cg�N������������!�,�f��ɭ����� ة�}�ݟ�?�T=ι�|���h��kZ��Ʊ�@?�`O�3��#�ne(b��Ѻ؜�\�d�����Αڒ�����8�I�O !� ܾv$;^�g�k����0�k�N_�����杬�ɠnM'ڻ��ӁT4�2�0�����f�K%J���:]�}B�|B�'$>!�	%#?��E_&�2�ˠ/�}��m�A"L�f��I;:UJ������u������!���wz�4&�]��ޮ(4�F�"@ ��\՛��7�ݸ�e�}��7ƋH��K�lCɶ�kȜ�e�J}���?��8\C����
�{*m�X����a����@TԖ��f�a�浉��:��ʞ��nP��:���C�nX�\Ɖ��=BƓ-?���ذ�{�#P��]nn�B;�4.7��#�-�D��:���&K��?/����'�z�O��98a�����"RB�:?����Lp���������ۿ��-4�dK��7���K>D�Ħ��q�����������&-��a��(Ŀ�557���6�0Ѧa� [��Π%C��BfM�u�8��>�O_�3k29o��>���[�?Q�[�����K��|����=? (��������=�}y QCA8�GɛwGԠ/qb�l�>^��9��� Q��D(WW�/KN�λ�ŤS�e0��R��,
'�X�����2h�����iк�w≵���q��W|0�\�ͨ���(}`�[&��|(��ۛ%&��b_Z["�Eo6�P�v�c�(5���k�21��+	��/6�ahb(�Wʚa����x^)������C��<�ȟ�N�S�<�f���� ~�}��!B�vlݿ����/`��W�e!�[
߂`�%*���Ү�!B������!�ݘ��D7�s�n-q色Z:�c��#~Xu�k1��*�nm%r��Rf6��Em��Fuh]^��^��O�?������:J�"�X��E�q@�cV?&�u7Pz���>��-�>R�P2���)@@���t��L#�A�Zw��h8��T[Gm1^Z0����$�����u|k�@���S(�J`���ώ�x�h�v��&���b�r* 1���v!ۊ���gY��D�L��6�HE㸴�­�e<$l���"[<K��V�%�	W�M�+��`��?�������5��`C�8�ï2����<T��.���59�e�N2�Q�ʿ�P���#rleg�Z���W쬰:T�/�{0ݣ�C�z�GI�~%ߣ���R�J����{�� ���uH�^ө�LM�l�h��6�	�"��θ����
��6��hPP3�S}�J.�-C�Z 0��%h8P����{3�vZ��_��׽�;o���-��zY��E��
��~.z�7ᄣ�t��Ͱ?I�{ٿyΔ�F�H�U������y�h��A�#�f@H���<G��?���2+㉂@\������d%��0�VK�I���I�� ���|��j)p@����L��Ĩ9������ryiݑ��*�� ��p)]8`*�5(0�5������� ��py�k������&��(���P��Hq|a	[X��.Dv�������GQ�x0�C���u%�e���C���~9�@E�c��T���8��o�E�r�_���1�e/XDI8���� >A��ɔܻ=I����r[#�Z��:Z�6$�F�\ɋ��ŀ�v-xQ٭(eɔ���}���Z4��Jb2,QB1&�(p�S����ǋ-����K:��-;,�t8���ܲb������=^|���Y.�H��X��=W���=Sv
#��7b��%��7bY�UB�#?�g"�ʑ2ߐ��v�� Ä䆂�
�28��������~��yA��\��l`���\͹H���k��������Ġ�N��y������L�s���VR���!?y*4�\��*��[V7@:�� ���݈���{ h�Qt�n��fK��-3�2նyE��xi��r����R*h�yӼ����k�̪���\b�%c���dP�U��T��J�Vð�^u\)�{�%b������+���U��L�����}���~	g�Bb���W���|�s�B�;4zK�х�.LuaF���H*(ۡ��W���J��r,5���m{C��cKq4hǚz�\��+d�| �qu�N[תP��n��mҡ�n�oKT���R���6�(cD���o˗~��!6��z��J���v�j�bP�mٚ�/��2g�ސ���Pܱ�xdЎ)q��)U�j�ri)��omd�ot����������k��M�_b�^;�PEm�W�ᆽh�^��颽k���U7ݸ��T��;���89{����^D�Ei�"?>��p#�aX�9W��Y�Q*�����M�(�^�U��p[��sy=�    �j�F��<���d��f��[�Z33�
�ͲU���t��n�m�(�L`��B�>��e~s�*��Jt�j���u��t�h�tK�Z2j�0s
���~Z����A�fYN�iKg�`
�H��N���/x�o��Z��E�SA<��v���~��$:^��s�h�q����Xz9u�e��8Aa 5�ܮ������;g��6��Ak�`ׇ>Q&�gL����f�:��_�\��t������i�M+&fX*&v˞�R�Z�l�XKR�ܺI�,����T���S���o�VA�e^\A�>���b��;Js��h�����5��Ү([7���E���c<�yS�>�O�(�4�������؄}6iNB���E�8�?J�B��DdrI����ك��s`ٛJs*$sy������?��b��'wSrd����Gs�|<��Ǧ�a�����9�ҡ��)�^~���J"��O_
"��iS�` Z>�GD(�AKS�G�������T��� �7;�5��>����P���$�������N�l�S+�n�7A���:�B��>d�\�+���?m��?�������7̃T&:�ja���c��5[���ev�\��O��8|���,>�Z6����w{�u���[�S���ū���{2�	��{��z6�G�iBC/��F�'�Y'��  >���Oծ})D��+v;��N]��QK�A-������M�q��Q��Wa^q@�&��N91J���`Nj&��,j�x�Uj�C;<f:��t6�	l����W�C����]Я�(��Od�W{dU�6h���7�){�0�_b�5;/yC������~eƯ������A;:`2����B,43�ۛFvQ8`
a��g�m� ���,_��B��?*�.��0���a@Ch30�ˌ�2x��`W�1�� �[�%��v(�X�&�H�~���:?�R��9�88��b��G�T����k�p��1�t��ʻ7hG�$�+��R����ξW�MT�U�Ѡuɸ�RK�z�S�U��C`��/���S�'Sg3�R��Qc�ujT��c"uc��3&-��A���|W8�C�I+jY�W(I��5y�ƣ������x�74bF�l�*�V{) `���#/������\=I����@�A�m�jc��=p���$֩�%,0x���k��f]�&�tRk�2�q?[�ʾ �:�ڣ@m�C~�D	=�o{`�+`�t⥝>I�D�.����<�^����$ ��i��j��\�rC��m�>��� u�E�κ����PDe=d��7_��j�΃�� ��)+W�ND��]C �p(k�M���0�	ɮ�Vb��c�佪V������H!5���V˿�u��>Ͽ^��?@ �J�X��	�",�|���MH���š�PoP}S7�R�Gxe�amd�*�I�L��V�D~4�7�p��Z��K�+7[P�Q%ҙ3�IF�����^�RG|���/w�>MڢɃ�׏P^�c��-Iqk3TjQl�,B�d��EV����(��=��\d���/���5�Ċ�^�1x��%D��*2����������B��)�"kA5hv��S�@�A��3a�뺧ӷ��Y����|�Lx��J�	C��E��79_�]��e�k��"Sv��l�D� g�+���9ל])��W���@�0@P�3�2v�&�8��N7R�f��sH,�bDB��y+�Bz�+��~U�ȃ�{uG���QH��:�ߑj�tH�r�:�E{?<�8�܌������������2����O�R,��l=���U�yS����S7nI���z����s1Ct��}��g���E ��w��p�.+|�܉:���KQ�e1��(4#�D��#�)C)���K�YB��
|A���1Z����S�������d�u+�n.	�:��r=�O;��F���$�:%2.�r�5%�q+�_'Α�B��|U.��ƒ{��~�1e}����_���ek��X `��2_�B�}k�o��N���#��4�]/��޲�j����W��/���31K������kJ������r.\���|&�ɏ���>uoW3]�>�#��풺�d��P�|�`������)6ToW�����M(�؟�Va �r�#hNidڥ-�{X)��P���8�f���ͤi㺟?�����D#�lƿ]>���u(K���ė��-[�Y6)�x?�
��gq%AtE��]I�wzD��G����7T�S��f�"	�Nܘ��5*eNi�\���SU��oD����D�E��Wz�ղ�\���Ĕ/1ť�	��V
��+0źKϊJNLr�e����&91��$��p����h6h�(_q<�Y������k(�$Jl;+(Ruoz@ Ě�F���b���K󚀭�q$Zi�����z��,�����~��Vd�G��qe���ƴ|g2x�e�u�l���`:�m��]ۈ�h�ܜ�%ۍ)��J|#ńK�ڨeN혵M��@d�!I"�Ȩ�x���
�S�SA�WW�$Z�уz��0jb�,8n�!R��UW�� "�����U� Q����C�zVs3�\~;���1����xk>_G��!���x���ʯL=׊��pD�\��5!z�s�CBl�ܤ�[��Z�(�d�%^f���N
k�S����~�~a&���w6<�8ƞKE0a�	mxg�#�`��MU��K;��Aa����E���0��P}���^S"3�Nsq�K[oվg%�)��9a�v�ȟ`���|�!�'��Aua��?�	���hp���N_���E��p7zs<�� y�_[a�� �J}�x�s�@����m�֒&e�-:�8/���)*~x;��v q����Ī�i��[Ӎ�ր�z��b��B�{sh��0ii"�g{=fŷ���za#W���ED}�\�;l��x��47i�ⵣ�����T�����$�F$��)�3~i�Rᣬ#�JZW9"���ܯO>������YɅ̟x��"��8$��<�iЊ�H�a�+	�Gp��m�X�3S^U�G�YC6^p�彣(#�Gi�N���w�R� ��K5O��]��/�o|@������O<�L��ZK�_G�hkwX;�����C�T+��E���H���|��v�O𾆹�#\�z���@r��q8��'�0">�0����7�c8qjL���S�����;�_���Q?�:�n������-\S7GФ�L�eЖ��M6�����K�h�I�N�*���ݔ�\�t�{�Q�)� (
V�G�tQ'
�ġ��FG8ܴ#���F�Ĭon���;l��;��=- �*��>�w�[fd��9��>1�4�v���y��EV����Il���"6�5;���*�h��f9߫t��XN>����+��<W���
��!Y������)�!�r{*=aV�|T��N �h>�4���ylz=-ڑ^ϴ��
����kzM��FDkӤ�r*TiH��0���@צqA�J�*�~�b׊�E`��y�p�v,��{~��{[�N2C��A�������G	�ƤGg����r���Χ��&���;������W��pv�����o�{�Q�t
U����@�@P]����3_������a\��RwM�{x���	Wt��%?�'�t��!=}�,KZw�N�4��������ˑM	�?�b �"e&�xi=6�Tz���E@va7�P�'-��	f�*#�ȿ���wQ�Y�{H�,�3g�O1�1ݏQ?��/ޏ�p-����uuZl���T�K�ݿ�M�)_� r n9�%8G�y9,�Y8��}�@@�1b�āq͑�q�:�$��4YF�Ǌw��	EJ�E�W˼Ϊ��l�W���YN������#S�6�J���@�hO ����sѴdb��$�J�sT ]��y� ���*����|�ԁ��Zth�~GV�e��W���7*R���Å���V�
1�+"3���ݭ4��M39Ǣ)��]0���Bjg
�+�!���5�BT����K�Yq�6?�|Q)X���<.�$�Ϡ?�����t`    �J����nx�ι6��M�ڡl
�R��;�V�u�w0�z���YI	m
��dm<�`b�+
��-��8������I70aA��ʼ���¹עO뛠�X7��JٽEA���"l����%]jb�qMZw�-[��uK0V}�	q(�@��:�S�����ߠ��B�}|��q9���w��M�h�J�0�VO�n$"�L�I{5��2����7hGV�T�i�2dg��x*��v����@�o��V[������f�7%�U1T����%�1��y�O�N�u���
���F��mE ����>]}-g��(��bL�\8Up�A���(�PW������+9R�J�v��/�Y��U��в�rX�UY�1�R�2��O�~j��v�8���3��c�|jBqDk���N��������{צoI|)�j��>ui^�4��O���%�Sa�,)`��sBq)����=�˾��˼��]�ާ^�{I�q4�\� Q�XJ��k*�E7�Վ���,Oe,z�r��%����Ψ��3�T�=K���6��,��8&�Q�-�+'�,>�Ԣ������vZ𳓣$�Q�r�=�ƌ��6ͷ������\%�~5��h̰1��`�q�
�c���u�F���9Q�QE�>�Je�ʗy����1�	n퉗�9�`Ґ+$s��Z�w#��t���7#��Bm*��n�NF�K�����s��K���%�����Eh@*E`�?�V�X���ETO�I���g�d�=Y��������¹���^�z��a�/�a�aE���0�<�aMV�F6�1MڻY	b��`��m��
#����s��D�Q�Ȑ$�y��Q�C3˶���K!�"�zi]�`StB*S�"�t�/��r���[���.8)Hq��U&7�3hW�k"�kp����k��-]�C��=)��u_��䰃��!w�`q:١I��@_�^�ɱy�yE�v��3-�ee^��YƐI(�-OC�4`�-���ޅ=���B����ٲV#�u�Ў�(�Y���@B�J��o�
#��لF��t#!�P0�y�u;��ڒ�#��D϶�@� ��J/��`c���:����9`�|� 1�-�b�7V"/q�*�Fdq�,~��\��H��ҶB�U̠��9��a�ԛ�����o"2��D���^�i:��#.��!M�f\��\�c�_����^����k`�C�W/��@fk��@�'Y��P0s�x~�\�1��g4s2v�ȶ�k��ς����|rZ�ᒵ96�MZ4Q���i��s���ky�7H�����0��\��ƥ��"���|r	��g8T���PEh~�M��ߙ�
-������E���:G)�|W�A@��{�Q��e�K��Vv�ܩԎA;�ڵ��\���uv���-[�"�{�Ct%�����5h�T��r�	�BB(��O�R��Jp��\q�m����C��8Y�{ӱH�h�˛������!��s��aH~��o�XbZJ�hݳ|��
���`���?L�V��]Uj��cu���{0h�K;�.e�n	�]��ʝLt���Fj_�ʼ*��:�ڡrm��}@�����~2N��^��x/��ޞA�L5vs��&m\�6p�`�v�=��A@2{AA�*Q��aS@���\����5h��j�����dG`����h��a6?�r.�Ch�4Y�֊���Ί>�AH���Ӥ�?m��%��
X� ���ܣ=��R�u�B��9̗
ըd�D�r�z����1�9Js��W��C����b�ɄWB�]�0)j��P�6x�o��ՅX���sR�j�z���#4�z`(�`3��$�A����mK>����L>B!dpVéJ�8&��d��:���78A\[�����&���
���J^�W��1]D�p�>�٤�à�������A�
��O��>�	5ە|��v���	L=����A��	-�.R�E.�#��zh��5�W\�eUPV�ek���E�;��S�d%�TВaK&Z6�q8UךK;�d��ƶQS^�1Ŝb�k�TC�P�ࠩ�_��U��h�N�ug��F��e���`!� �e�y�ʥ�� ��ĺ�ڻ���Vl0��v_���fdA4��caS�2��J����P!����|�Է�'�,B�ui�J�p�fL3�CJ�-�̚m`��2zi*��e�n��*��Kh��aQ�li�N�lO�Q���"BCj��b���:_._q��-_�mM8�A�����$#�J�&D���e���֫�2�]�Q�o�ؤ����Q������iWE�:�/��>g�|���]��>П�RB�R����;�w��e(*:��:��o
�S�a��q8�dZ����&�������_�o��'U�����9Tb!w8��?a���(��Ši;XL�W���j��5F �0[݋���C��2[݌���5:j�(- u���C9��1Lx,�������ūyo{0�nG��X�
C�V{T��1�^Z�h��%��q�H�x V��a|/o�CFt��,a|�L�q0ݬ9��4�^Z�R��=��Xӷ�%#N������*�n���駡�ϟv��}d�N���k�Q_�uE�_?�@��Bm�R�������j�	���S�BR�2���	��٬v	��u�X0���(�`��,���R�:0�d���ig���VA�Z��1���e���}\�ʡ��3޳����`H1|@��K;�ya�Wk~>}�C�n�_��*��;��؍#$Z�b�j\�J@��*�XC�T�A;93��3U��� �d�=��%����6;A>�v������f������*ߏ��=�ſxٶ��1�a4k-�V>Aƃ�41����Y�ZK��le��H+�l��b|���K;�-�Ϩ���q-&,qGd��B���\��_�`y��$�+>����DGTd�	[��qN�gt�Q������P@=>�,v`ǥ����%󂇵L��PtĤu��������V^Z���[P���[R�[��AN�pt���X�#�Ku���3D�0��6\�,�%D�����ۉ[^�h'���XG��bkKp��OF�&��wvcFb{i����3Xq��6=���L���S�O�N����ӡq�2�z�y<`E�q���S�M%�K�h�b���X/|ق�U�Cp���E՛�E�ߘ���z�����]��Fg�gJOxv)���;���%v�un��|��-�y����L����E
[�b_�R�Ԡr��v%;<fC�ow���%��Al�<��η�5Π�x���Z�ǅQvZB���Z�/��E_7�#D�Lu0q�Z����f��7F���Ǹ�EV�Td��SXh�?*v07ǀ��B���3wX�E����c�P:��x#/���C�w����Ӵ�[=Q������.�3��P��R�v��A���X8���w�QNK��K*�t?qr3�>>p9��nJ.�4,&wp�}�^�qV"~�GY����*��\�ꙩ���`q7�8��C,��vR匱ua�V'�2�r���o�=jdT��.�U�U�x̏K/�t�ҍ�7
�s����b/d����n0��gQQp˥��^b/)�c�ߤ����H#����VA�����D�myiG*��D�w�p�X�iu��M�����A2�R�I{om��ҩUf�HKѮCDN����c��X�-�Y��g�Y������l,���K�i�>���S���#�|���e��'�>
i�ٝ�����!ƍ�o}��n�](�i깱�3t�;��^#_�Э�*i����9d��E��}�O-��A;=����H��-+��o�Z��LZ���h28{4���9���r�x�S�Bh,�փ���v�����%.���c�%$. 2�]ueؕAW��B�te���HU�*Iҡu��F��y�*�-Dp�U��M�e��^�s������E����}�f��-��J�hG�[n�H>�AF�=TU�����������t
b�CР��UZ!C �`�TC�ClE�WU���R��0|��������pe�)�ox
�W[�    �,)�7��h=B�����m�9y�hڂ�OvD��̥2�P���cqף�H" ��F�s�Ȳ�J��yp(&�����$? �6^Ŧ�a�N�Hr��P�ݗ������gD�r�VB!�ʓY��EQ��}��(ē!Os�i���٫B
� �.��N^z��	F��]���PԴ_���I^��.n;�#"?�
d��l����b5#}��Xa<��tx�U*����X�`Έ�U��W�p8�r���~@�y6}V	[T{(��v�*KT�LB*in52R�_.��U��3�����tg�:��
�Vyl���V�7��"�����1��r�R��|��{ܣ����0o�����U}9�We	g�y�8v��%�6i'�3O���Xuۈ����Nb1�[��X�'�/%'u�jNa�9�DJ*�����XwgF�����9N.��ER���wp47 �O�44����88�b�Yn^���m�����wֹ�Sك���Js2i�c�7�U-�^�� ������d~L-�#�vT������M�n1������L��׬�Kc�z̀LrS�~���p����2)����I�F=��uvF���d�5��݉��S9��l[�������:�/�@[�X�\8��F? ,+�4кX���̨��������>�p�R7f�h'[�"�P#�h%0�p
48  p���׊��S��	I6�y<���� N�af)�m�&�+������,e�/!�W<2�X&�k�V4��w3� &�n�Z�E�iMK&����x+?��(���
�3�����:��o�#E�p�9�*)���G\(fډ�(M%��^�u���+���>��<���@�vc5������0�B��>��t�f�i�q�����T��aJwM.�v0�0HN�%=�$C�2���n'�ą�m�bE]w[���u����}M%���#���M%V`�I�%��,�I6���4�h�-V��0i�g�Bd:YE�+v�cY(F��Et�È]�Ӡ�1�X�í@�yp��I�!�u�s�� �zA��oH����n_���V��<o�/��"q����%��@����ug��b���0��>�Z��[�Wb��t��.����h���se�4�;������h�8�g��O)7����!2�W�ĥ����I��������+'��V*��G�7q3�=���nɒLH��dʥ
`⥅�-/���� Y�qbȉ	N-���b��ط"���-w�Jy
����l�h�~#C����#A�O:�ک6���F�A���~��k�UY�cG[W!��hЎ�`���n�����d�ܱ��»6h�U����PA{M@p�<��͖B��^R�:�W:������Ēy[�|�G��2ȘHp��4~7�s�U�4���|��+u�U`�پe�:���%�������R��y7B��F?��߲
Fc���q	&�"��N[���R�#�6���C�.��ud%|��p�X�N��&����Ef%ZbSaY����M����h%8bS�Y��P>9��NZn��z��r�zc�b�*:,���K@����PuU+#O$��/V��-.��JG����W�cB_�s��='V��A;-��8S�0Z�8���� r�O���j{��j�M�OC(�j�)�[B�[f��J �7*ن���Rׄ
f�u>Zɋ	^��]2���!D_�Ñ�vr���KT���
?�j5�8p�Num���Y)ۅMW~dƏL��2-��Ў���!%R��䳊_�\=����j��!5��++���`Ԣev���A��(�P�����������m3R�Z&^B/���Oj�}���*���.�.!t	�K�Ԡ�v��5�.���ܡu�ݰavKo�4ߤ��$�o.�ƺg4�DCS۴�S��@��k�a�=ǨlH��Tp2WN��_[�?���uԍИT��
���ty/�)�	���M�
�fXSw�T����*ۡu~��7�����aªm�LG�ԅC�<�с����o����9��-+�C�<A�e��b�ρ�5��:晤��'*3�q�d'���u���<wn�ӻ��o��U�Рu?\�^�<�f��b�J��j���#{�����#�0٥)�
������%���|B˙��"m��<��V���Ψ0�ݮev#��S�d�u�=��4�/:��A�j���)ʭ$�K&�u��Ts�]'�5CK:i��D~���w���ݍW?����%���N�S��EJ�^Z�O �� kq���|��*�g�ߺ5ݽDR�dl�FZ���oOkv� -��e��n��{��%�\���[�5��LweеeY�
̡u\I������?yp]m�6	��oΰ��ѩ����q��J�z��'��0Z.��ٔ����$��f�����"h�����i<:��p���*�>�"Z%����0r$2��2�HT;��UvL����]&�/�(�(�?Q�s^�P��#��h���`��Sr�BD����T'�:1�D���:5�
഻��%gO��O���6���5��	�5zӀ~b���D1'Af��2j���Ҏ��3�7�{��	HϽ��S;Qש�/E�k�
��B8D�~0�4��Q��J���NYPĬG�Il-}*�R!f���੐��ڠ�8�8>��4�I8�����%��_怂���%��sV�t����A��|C}ݕ�/�����o�C��>+@�!��Y/��f�43��0cČqfL1kV ��e�ZG��/�9���O]
޺����4�|���tÖy�u�����xG�7�n�<�a���
` jZ�	R�#��{��+�bk�Y=����ev#��j�Nq�D�%�
��� �5�l��#���Q��fL6k�o��%�Ѡ�-T�l��m~B����T�[S`�Y5�����0D��fC����l��qaȅi.�<;ͥ�M/��2�-%�N��Ė��r��@_b���.�Q�)l��u���|�˙֏����!h�
:��9d5�ѣZ,���G�UP1�s݋�B�O�R��!�s����l,��H\3�N::��9�,A+S7�- ^-����f	��I�M��?4����ԉA'fvbf����(�v�!��!���j��ʥy�>P�s(:�f��{0у�=:Ӻ+�hG��Q;W���h��b7��X�B��^=�P��KGJ[s[�ּB�+���u$%��-skL0`Ƞ�;��`�v�7��n��:S�`�n���\:Sq`��]CV��3�%۵Lx�`e���IbG�	�k�\��O�sT��@@���5GALB�$$&�`R��EO�{2��D���J�=%h'|�3�����D�]��a�ɲWu�2�Õ�?\�S(k) �.��-�d�%`�M;VH����1�����9�n!Hk'������#!�43f0;4m0q=��E�&Q�J�c�<�'/�ȫA��qk&rk��x�\����m�▅�����Wf��zYP��YfL�|���:j�a,���OQ��)!��ֿ�U���5 f�V�Ɓ5���kk��-'V�k���-c������e+Ĕ�`��o9q��r���
��<�^r.� 8g�ڣ�8Xv�D��QO$�BT��x����!=�U���k+�"_���)d�]�@B�OQ2<��S�[�F��ܠuz+6�̧�{��Emh��nfF�����V,k���dm�3�zi'&�D�Ttwd���9�8<Ρ��5h'�ӮF���]U5���EC���_�+Vpg��yY�d��}̈́�O�B���oW��_n͢���w�竷���,�k�N^&ǜ�ͻ�
�e��&��8}���;��4����_U���^����]�z�s���=n�]pC ec�(1����^\����P�j�w.��Eȍ��k�!�y.á�l�:����P�����P�0l3�g[!�^�����$�)q��V�f�<��G�T<x|~��]�)��X�D��вC�?r�I"k����W��P(Ȟ�n�ۯ���x�����hJ^���$Q:���f,h���O�g�� b  �������T�a�t� �=k�1�Z8O�WC���f-D�L� ?��q��e�Yw
������e�3h���HPI�����ɹ(���G���?���K;l����<J�v�m<=�,�zZ��&@k�m"�)��W �u�&ņ��pj����q6ٴ���b�h]!��6��[(�%��`W�c���Q�������:�m���.��F�phQ��z��N����1{i��m���{��P��rK��9�(۲�|�T5$a�z��T�Q'���`p���X9OT�7.���C����,k�F�y�E8�T������ �'.�� "��G.n�#�K>¨8k�~ZO�{��P?�WpV��t�Y3��b*c��)3�������h'���x��%N��Т?��}t��j/�Q�زvqt��Z�N\������]�*1����u�S��j߆��.��-ʨ��кFI�Z�KsuZ���c�P!����ܒd#���7�#Y�$�Ҏ��٨�m��jP5C8W2}�����n�1�����W ���+�֌Z3՚�ۗ�1z��a����\_D\A���f\�Gy�_�;[�Y�����Z�X��EH,B�E�,PF`%t7ƻ1�ƌn��L"�5oe�*q�2ю[ڈ�O��Z��._�5?*�3�c�7j�F��Ui���nq@��pЁ�\=\����R��3�����#ؤezSW<T�c�g�?l7\RY]��'B tr�`u��+���H]F�"4X��:�c��
�BЏ���b�]�A;rm�F�p&��R0WA� �n�.��Vn�.K/��"�����/~] 0��0�.�4���P1hGU��g׀U�1x��)�!v���si\W�V27J���o������QF���'���
�V�	����]ve���ؕQWN�[����m��HA�:�c+'#�F}bߠR=�	�#�i�}��e��r�P6�.c�a���L�)�kY� �������[���S%����E���(jF��Y����A=���9U<�᳘~V��A��=��~;%ڠ�*��DA0}�uC�;| �|���P]�5�E��4������"���Ǐ��rg����o���Qw .����f��
��u�8�#a1we��X��Uh����׺�y����3џ���׵��вvC]�٥����aJ�Ԃ���3Q�iXM�q�x���[ۼ�Ecc}j	c��X��9��$A;��ò��~ҷm^@��Ư$Xæl�b�vk}�"���$�\ ��B��&8�,^���9��	����1{���^/�0���s�X=in]A� ��d 밞��$�<�ڒ���
<[V9���L��n�dh����|F��CV�Uy��gE_���e�V�B�����2/Zu{��bT��YZ�,��V�$P�;��ź"��b�V��P%{;���A{/p�ؖ�,�!��\�?�Y���܈/-�O<L��;_��} 8(�uS���Q*��på���ӣ� �e@��M�(�Ex�|������QD�.� �I�\\�a��2�\F��|�̣��cOaM��_Vl��Pu�����2�Á�nк��+���i���RZ>�}t	@� �&���b�zNA��
�4���WeA� 
���kiA͝��m�!�/���?�BG�J��䁴z�->���ڒ�l���
��.��Hf{M��p�޷,�|lUU7h��L3�Ox<=%�n6EŅ =�B�Dx��"��'�l�NG �3���'x�2�Ѵ�?���6Ǩ���,�����P�-�	����z1
�nLuc�[�J%�dr�ĉ�N<)���W|`�yol�)D�'��v�q���ӟ���O�      �   j  x����J�@���S��{���#("�Ÿ�:]�q2顧��ot���p6u6��������-D�?����;H.�.����š�5�y6���ɥZ�Ch�|�q��b�~���n����~�U�w�,�"�އ8�}��EmE(@�`H*�h˅�8(��c�<���\�y��b}p���x|���OI�0M��;Nӡ���Eq�L΃�<�']��������rA�&�ʘ�����*KEr�p��%߂KI5p��k"��H�*I�.�Х+�-Oe֛���YE���[æ��,e�����g��q����[L�դ���,=9��*�4k���1�r7�������xU����<cU�˚��i���_S�[q      �   A   x�3��p�4���tq�FFƺf�F�
�&V&VF&�ĸ�8��|�2��W� <(�      �      x������ � �      �      x������ � �      �   �  x���Ko۸���0��ED���4��T��0�VSնdHV����uqH)��~�%�ߍ"�,2����=?Q����'2���-Ӳo�����LX0qB��rI\���/Sco�B�a�۟���A( �϶�0�d��g�*���+E�ykZ�)�'S1�lFf2���ߓbH(���O�`��4)�7�IyH<�)-O�N�h���&�Q1��,���㝇4���F͠a�MU�q�i�q��b�-���gyM����PaS�әq�x������N\W� ��x�:f��̚���Q�Z��t����;1-��K����I+��%q=�@@�K�hg�<��b�y&���w����e7T1h`�y���ٓ�Y�̓��V1�8d���vH#��v'v�b��$zxP{�/v3'�Ն���{m�,T1��$N��PG �*g��Q!��2l-~TeU�Ć`b�*9�/'hH,�a�*� �x�v��B�_�( X��|�<�P����҇��I8q,����.�C���1hb�u��z>\!�E -�xϿ��?o"z�C���MG���c3��|�$��ǵ�P{b�*	���x4�L�`φccXu�@H�b�����M��32M�>,���A�%M����Ny���u����b��?a���L�>-����Z��/w��*FQ��b?��7O�������Z�^�g�&���H�W����BL!�\�HP��'mx�����S1�?6�x����Mh�2פ>K���d4F�q�&������ɀQd<F�h=*�8*F��Eai<`u�X��t[��b�1�2�u����Z0$��\���pNK�#�l���k��u���d�쌇����h���1�Fry�%?p�p�^iS��K��g �a9�	�#/������[������X�c�	&��ÚB��(�E�ocʋjD���X�Q���K���Ւ���X�9n�!��cG���Qx�^���Hi�K3Mޘ��ļ^.�(�m�mQ!Bk+�2̺TM?#��U�"7�"V��W�AS�ů=V�J�l[m�51C�L4����N_+�1�5����~�0*Ѥ(EQ&����7��bT����t�ܿv�QK�;�x��1�H�Euʍ����7�m�Xu�ӈYx�뉓����/="�4�E�Q����a��LG�(�=��uJo��b4`$��5e��zD$Ʀ��3�8��?V�	�����듯�a�lj�.C�
,DsQ���V��m�1=F������rF+�����+�DFo�|J�b��tg}Fm�܍Z�]H2���<lm�~ԴU�����:��y �����zpe�lMl[Ũ��"~2GHb���2j��n�#�`�w��}������f����O�(����A�t��Q�$q��R����QG��Y���4���U����QL�fū��˨�e�E9�����h)E�tB����&`f�����T��X��o�n�2��aԑ���r�F����.i���b+l�0�{�v��P1��u<:n�s*m�n̼ցz�:A=�Rp���@��Y����Q'$�����t���E�4�}Fѡo�\t�
�6�*F�,�tǳo)�uR�氤5[7�1�H�9��dܥ���"6kSY�Q�����7�/.�]]Jj� ��Q�!�K�z%P1���\�51�3�vU��2kSt3�qT��>YƟ0w�����_?��1���=F���f�ܲU�z&�Ǟ�i����g�����bԳ��;G�K��p���=F=���,=�E�K@��,Fũq�%�����Q�%w��Ʒ�� 
��} ��si���ld6D�r� �Q�?� �W��
U�z�2GP��S���8�.~��R@�u����Q�
ۡ#�z�~Q3��[�sS��6'�R1�j����p�k��f��mG��ͱB�Qw2x�،��ؖQ�%��	�ۆt�}�45�A���n�0�d����m���F}��ު�a����h`����߾np����1�d���n �-GLwW�e4p��c���Ɂ��U��;�.����y����\a7��L�f7^{.�c4���^ߥʑݮ�e4@؉�	�j���z�Y��n_����X��U�l�3m�g�0��ל�6���ġ�hh�%��d7&a�%$�!#w_���n����[�t-���=�z}=�j>��mr�˳�0� �D�h萗,}O���1��::�j��h(Z�l�nEI;"��P���k�H�E��~{��c4�<J1�>�t�U.6��v|=F� Z��g�DU�v>n~�hv��5y#G$��4�%��t�n��0�02]�g�x}4����<�[�u˱��:� W�c���U �i�Ĝ��Ƅ]�9� #?y�/��p�\�6ɠ���ud��g�i�g�3��x@j?�0�i�3��LJq�4�ť�n	�a���{@Ǽ8�M���0��$���lSq�������fG����_k��lNh����d&�Kz��o6�Řn�zY�(C���h3�Η�翍�����a1yL�bc�2L���m�E�3���Qkc��"t-�rJ��mG4��`S3h���E���襶�*�YEԔ�i��X�CW�F���e+���9/��P�R���fۋxd��ݹ3ȯ�(��Iw�����!
��A�ý��:���h���'�h���ܙ�� ��Ϟ wL���qLy���y*�[$���l���<��4C��,��u3ۢu�Wr�Ѯ\��+?p���6�S�Qݙ��AL����O����E>_� �'����1�ظ+q�O�p��@q��4|=F�m���u���n�ӻ��2ت�W�S�}�-�5)�wY�e���_y��^�8�3�܈dػI�2H"W����Ӽ��ex�ER$��w�Qs������Wr::n{OS+"oB�Pp};J2��f����{6�(�7�\��7��,-E
@�B�6��$��#���oj:��A�'���z@{���I�(�,�&7���r������0Hxd�E�_v���jIs7
�{�e���RJ��:E�      �   �   x�m��
�0E��+�$:T����RP���ɱKJ5���&E�{��S��p�d�	.֙���}�˸�5�c����;������ò'tP��o��AGxb�m@8"��*�P����Z�V	Gb��D'B���d��՞+г���'QMe���c�\s�ߧ�;}      �   l  x���Qn"1�����h5v�8yb)e��Q������Ĕ�LWE����c�P?�=to�3=��9]�CZ�>�O��yW��#]�C���I�;��* E�x@�6���������2�7���((̛J�X����!��@<��dXWa���6Z���B5�KN��Be.2aG=j@٠Tn6�+�Ǹo�Ȫ�1xMZ��PoP���t��-�Y�@=��,C`��%��P�+�5]��czOR�>j
�^�p����@ʿC[G.�����}�5k2R�g�7�-o4`���q��4䣠 o�ܿ�ѹ	JA�;���!Co��H��]���e:��*�����9k/�1��P��;�[z��C����&�N�g�yv0�%���
h�}��M��7�(�������9����;i�AAmP�asN��g,����)١������1�o�ͣ���˸��w���F�,��l��(rG�<5�fQP[��x�L�t�Z�\�N����FE䵸�h��9�N���'������܃��u=c�[5S6��گ�ۃQ�.XU�d�'4_���Ԝ^�)�m��|y4���4�5��n@������|j��Hُ6��xH��J�	�2��.��9i3�{��Y=]�����})���zѯ:K�i	V���b�$���+_�Э�#��E3+qu�7��	�K�]��Qx��Gs)��߮���&���Od��`�&@3'�:���Y+��iF5�H��������}����PhK�<��H\YI��T�o�hF$�8�c�m�-��(
�af�*��P�t�?��U��4yoѬ�\B�:@jI�$͂$�Z�-�.���L�1)��y���}}zz����      �   �  x��X�r�8}F_��LY�m0oP�,�*���Ei{��x)ۚT�~���F���J�I8���U��>N����s/�J߿�����q��\��������]��	#lN�{��!�܄�x��2N���-߸�7�$��^�vvq�H�"P��&��0����qD���i\���	#AϷ/j)��M]X�N�/�rt����ى<��H?�ܕ����l�y�1���-�$U-���TiE�0>���Z��>��U& :bi�I 7�P�ߔ�Ny ]2x)"q�9��V���*E���h���y�lFW&�P:��T$ZS���y���e��0���(�Y#	[���f>�]��!, ��NW�<��G�n*��	#�[hu5cq��NdE�8�J��R��ե��G�Ӹ�	�o��z�s���W�M��B�ҡZ7U-�A����Rw�TAJ��&�����|F�jl%Z��T:2�5�
��,��ʈAr�SU%�"��3����D�m�Jέu�+5F�T��A���ǊΠ��F��Sb�����o5z'�!����8��kh�cS+xׅ��T�"�J5�QWX[q�\���qC��a'U�2=
�Jef�7�k��-��g�"�>g�(-������kz�"4F��[й��!��M:C:y���>��"r��ie�,4F��jjz�:��A�ah�`mC��m�ٓ"Ka����[�]w�@��FZe3���G�@`Pjg�ERT�3YU7�~���r�1z�t<�y�/�3ҭ��hEi�p��(�a��J/�ptG��r�LUV���{8�wB��2<`m�ba���y��=��_��~�T6� ��Tߐ�>%<��)��cW`ۮ{��C�������y��ګ;�s���4�g᫓-��L��y��ѵ�b$ZNw�Fx�+�{�������*/u�l��	#�4/3�gzEĢRM����]���eZjJ���aģC|:���n!�V��i���n��h)���q�X��,4a�GO�3~ow����ה�0��ڊ�[Y2�޶�&�p76�����u�<��Z.%Ht�*�1Z].�?~Y�4a`;�q�x��u���F��4\s߄/�1�wU��U�!�v�e�:�
�d��o����	#��B*�B*]784�1��#Xf�ȼ��U!F	8_�o�?�ރG;u��6�\�B܄/��~�d�	�]Ms��}y�}y��ܰ���#�;;��uD�^�Bb$�=bi��9�K����F�=b��[m��^�gҍ~�:i�;�9F|o�\-�Oq�X�5P��8�n���i�;��n���T��36/�ʮ�����	#~0��H�;�&iɟD}�M�Lt�6b�9�o"1������R�
7�g�1]��f��;�#��$����?�      �   �  x����N�0��>O�e���f&Bjj)�`�c��$��IT��{R�#P���W�����1�E�B;r�;�n;��P69�;��b��=<��������R��r����\��4i@�ݖNݙ�*��9�J�䴪M8��9�;��d��ʖ�E��D�e9_�4��L7����?_�y*B,�>&ʁҐ+t����c��3i�`M�Z�MgF�F�R׃r�n,p]L@F�ݵ3�W��<���-la
���5�gڴ��M51U�b�K�BPF&䧡P�#
y2p_���T�s?c,h��2�k@9�ڕc�:�Z�6���l˷�yҀJ�Q��w���k�񵗈��UG,���=��8| x����n����!��\j�nU2��d���tV^�����e:l�Qtv���~��9�@��l\�Ŕ���|�ҥi��p7�m�)>qG�4yw�v�G�ö�>*�y���<x-����j��d��n׺>�xN-�d����T���yk�(6KU�[��x
Su?����A�ɤW��1[�lM��R�4`~!赳���y:�^[�Ѝ�=�v�ϼx8��5��ٛ桴��e �Z�"����C����i{�'x��-K(v��O������������&Ƞ�f�U`��n��^�b�r��� C��� ��z�      �      x�Խk��u%��ί`�|�#���`�=�E�m�HjK݊����["UbW�&Y���;7v&Xǅ�u�;~\Y˥�8xl�V�����������2�޼}�����_�����[B������ի6���\�gk|i�K�B�诂z��o~��g_�����7��$C��&i}��O�?�|��ջ?~��w����W|�㋏�?�~|��ϯ_�����Oo�y��+��?����~��V_��7����������_�?���_��������D|�i��ghK���M���	r���m��������w߽����՛W/޿{��wo?�~���O�x�����wz��7��~C��o���7��o���߾����?����y��z�y(6�d{�����Y��ռTb��`�M$�l��<E�����_}��#�ɿ/���NA��e
����������?�귿|�?`� �>_�3�'*��b��Rz�-��o�E�a�&�uX����'3즇�~z\�Uƻ}z��M�}�������UT���.6Ι��T*ӄX�;o��;2�<�i�C�	��w>�t�g�[��͸N�rܱ��Յ�r�Q��]�4��L�-s�z�n���i�!�cN"�z�V9]�k��E�i�]����X�;��[vg��;��j�z޴J"r�VJL���;O�nOC�z�i-�Y��+�8����a�w,��͜5�H�/M։�z��e�!U��YC�]��J�K�B���wy|(��|�]�g_*�N���_o�+�y|f]2���o�MP��n��D�f�.A�'�]���.�a��ˇ��X�����վ��������o'���z��*�ϛم��Z��(�k�X���n��-=k���X?e+����;�ٝ���G� qM�b�Y�/Y~c���H2���}c�&�\��g�Z�=�g���R�N�)b����a�y��c35s����Wo����݇v}��}�z�����_�����������w�3����W�=���۟>��������������Ɇ�՟?����1��iN����U��+қeOb�t"W��1��;��I�!�.#��1��c(:�^���I�00��?�.F
7��ya���7o�<����ͻ�,��sa�e,,}ai��|E�K���d�LR_���bO*\���Amw�b3d�s�m�q�.T�z���˼�t'o��g� o˼yU�E��&5�[��ix��n��������{���&�}MF�O���[!���"lATt+_�_��^K�'�f��ѩ.ɲtNA�rE�6��ojuMV����X{u���^�uv0TX'k���}�Y�QY�N'L���������Z���V�!�Z��"ܯ6�E�o�r��+ݎ�'	b�VK�Vra|��7�������4M~�����?2�b��Ö�:���������!��C�~��~��Cd�i��"����f�
���kO�k�t��Irz��֣+�:��^���I�q�Y��#vx�x����Z1�-ܽ��x'z9�Kg ��N���������ٖYa�[���n�P�����a7�4p�)Ň誗|g]���31�p&&qɕ�m��""-5O
Ā+1�K��e�Υ+���a���
��X�y�5�'��${��W�=��Þ��x�x�|���*iG|À��YY(*+k�ؿ�!3�C]�)���}>v�q���9��*8��8����p�_yԷG��ߞ�^���S'E���5q_.v�I��Y�6n=U�?�-�·���Ғrǁ�r����?����0������s�5���
I[�<�ƻY{���upE����N�*�6����ӳ?<~�駉��ɤVa�>��q��Y�?��XO;�)���5��GT�(sۯt���ͺ�6�m�x[��g��H=�pm�+cX���I�00� cؔ)��)�!��e̫�ͫk�L�!O�_żj�?�b\Vy��7�SRJ��a(޷MS�+�o�q�am�0��R��g��Ϲ��C�[_2c��+^CVT�c|��v53�5�8�$\���0OH��smGް)��[��@8�t�eE�5����قu�\��
fї���3�1��y�B���Q*�^���*3{�P����Cj��k�')��]fۆ�)����Gi���+8�<;�����5�7;D�x��\�$(۔��U0�Ms�Ms�n{ը�����L[戍�l�%�3B��V�9�7\�#tb���5'�5���o0�,���>i��14�X�K�-�
��P�Kfe6�/�&Ɨ��?�+��WM%��p�yIUJ��
NLq�J��L�k<i���^���;���iNLu26WpbZ�,������tb����4ɹ��}9�k�b�W�c�{���M���P���1ˣ�LL�t/u�{À#SR9%�*�JAU/׵)��c��iő�WQXf�r�|��zg�$������Y�cK9��
1��[űX��|�Lt,淿s����E���.f�cѭ�E�x<R�cX&�oӳ�ǀ�K?���s��}��2�߅1��Z�C���2�^'�82�9B9I�a��e�s�Dj��M(�[m�G���'�ߟU*�9֖|Z֑t�/a���+�S���H�c��Uݝ�9T`��<�/���C�od�bC�D���̼Y{c�����>7�*v~�e�(h*Rы}-6�x͆����Mvǀ]�����k���&�aN�m
Xԋ�(̴nX����ï��_�Q�<x�D��e�"+^��������T�{�;�_�6/��~M�A�ڃ���Vq�x��T���H"�%���

��2+�Jl��W�%Y��J?}��Dָ�D�k"Ā����"�ʒf�z�!|VJ	lk�뙵�n�l00E���	s�$�kB�wx�B�@q�=�I�(u*GQa��Q�S_���J��Qa�Q�$kO��=��qE�u��#/�#_|G]G��X0\G⧒` u�rdV�l���P���ǥ�^�K#�����}7A	zѳ�jq�hW�@'����В�5�4bg��	�����3��� ό�ᙡ��XW�5TF�r<���F扟n]ü�z״6pƀ�^�2]PV�+!�u����kXc�d}Mc��͹�[��a��*ᅳ��aP'������a��L��=����U� ��%g��`:wI�a���Cu}�N�˵�m���1J!���On�Ҹ�����Odx&TIy*�7!?��U�b�O�
ܓxU�!��^�L�����0�U	�Ua[�]�L��m�pv���$l���=צ&0�A�a,�47VI�o�X�'Z��ث��3�6K|� �X�G�����㩤Z��l-W��x�<��'��o0���$iK?��O�d�,�u�?�M^j8��I[�$m�lЙ[������6�d��1��r�F�U7���)-qbqZk�t�뙧<�渆Ժĉ+��S��
��C��{kA����Ύ�Յ�汦���g���>�xq��v�WV뉓-�*�/^=��M���H[a�w޿������N���R��7�(s\�SySx������9L�*-zZr��T�1�<�C�xG������r�P?��;E��*dΡ���Va��-S΋ܩ`�]�r���5Qa����o[�w	%���{2@�,�6/�JE���_^׼������~��y���������~�?~���������������_�Ӌy��_���>��O�������~�{���Ge,�J6���UG��D%X!H�(�C��Ig�)�c�KDX`�z~x\uS�.C)��3Y�/�b�J�z%W,�TX?�T*<��u@��㴋L0`��%�3��S�h���b9�uq������}�M1\���k��,���AA[#$�'��s������VyWisÀW�����y��T{.��ũ��Xp1Lt>���$p>{q>;q>���xq4�)q��9I�?�^���$�p��0��Zr�J���.r�z��^�ZW�51E͟�l+�Ժ=�g��X��H*����\ٕ��l�g���⊬I    2�J:��v�r��sێ��QԘ���d釣�`�g����|���}8�E��Ā�r�j��GY��YN��Jm�J2���^���nagOZB�窗Nw�ɡ����2��V��le�K�Ҩ�O��.J]*J�5~fEI�,���T�d�p��'Z�ɓ��0v������L��ֳ�P;������-Y�)&�5r�8��ғ�ϲ���:��|y���jMvQ3�w�>΄1���j�y&��T�f8���P�}_���w*����vJ�0Ω�Tۺ���6E~��WR�3s��{�W`�;���D���<�v%c��������p$�Ñ���>��f��c�N�QM�=�[����)��R|y%;�<Ϥ:U�0�<�O�D -jک��OM�`�ħ�!��m�9%S��:�d�/�-6TeWR���i�K{�h�wTp�!��OJӁ�i�������$n٩.z���o�^*<.O������Ml��ϕ�Gk�����~�B�g(.vR^S�Vv�Nv�iV��.w�}���&t���c_��Rjd�뺢Y�g����\>����E�#O��֧� ����q�F���*v4��=�p4q4yq4��!�Z�F-'��J�N\�]�Cf9fM�����ZI~&+(~�i"�|u�[ƀH������5b�w �p��&ٙZ�3g9
��B��Q0�
���1.�։}�]�,x�$;SY��L�\����` �%�[*����Ŭ��d����3��#@��i�e5��4��;<K�$�{�%��O�f�e�'�Mn�E0wzT���6�:�B�ŜT�dw�[��$8������X�>��7�Vw��V�9�>7�Kc����:��${�t;ߌ���w��I;��VT���W���>��c��p�@���4C���5(%��5cJ\z���&�PMh�����������C[���Z��0paQl��ʇ�i����`��V�,�$+���
g���2k�Z�ʭ�iS�U0��EG,E��uQ��,�D֦���ks�}b`�,������毆p��,'�ו�w�b��w���'Q-�,uRi*̡Ǽ���*oT��T��V�rS�;a�����2Xm:��UX�J|�|O����PX{�˫�~�s�
^b%~��	A���L�#!�'=��Z��D�o޿~�엏���'2�@y��綰� 1��[KF�&�.-;�h"�o,y;\{kq"z6�\v"vUV&��89� ���D�Z�1�Z�$�B��jA0��k�1`+{�+\��W����i*�}��n}`^�Ͽ���wo_�}�3~���'Ш|��&W�8�����>��˅Y�g��ơ��}��y�lZ�N�*���9��'71,?��7�*����ƯT�a��сL6w#�-����WMUc.9S[bR0�`��t%CWc�����R��f8��ŕ�i?����ƀ�N������"��w|3�t�#�ޥ>,ӎK��O�a�x?+���>��ߕ��E��c�_p�:�9��Kt-q�@��*��"�m�O�kJ�9��0������xݟ��?W"�RE��%1عUTM_ϓ1����,�xb�I�%����l�|����_?���)��ۉ�se�
�^_�枀c3?<M�lg؏mq�������Ҙ�S�N�lծ0@;JE��ty=U�@aX�T�\�;	ϵ~*�Wq*W[�L'!�ޛ�{*no�#]��ގ;	�*�;���a��V����TA���Q6�B��Q�o�J�yUt�fz-�ɴ�y2�2w�`�
o-�H���T;�vz�(:���q͵ӷ�v����{E��g��_�q�Νk�Z�P�T��.̹U=���;��%��̬��g��\�2f�w�H�z}W�C$�C�b��8�b��32�B����C���K=̯:���N�yYgQ��n��D��.I�?�j�k�!i^���'򚸣�H�q�nǉh*�%皸�'�6p��cߘ��4NG@��z��~]�v�y��}E��%{u�$������~z��Ӌ����.o��?��ӛ���}C��o���7t$~�7���W�_��g|�������;���T\��t��9*lY%[���e�Z@ԟ¼*�_e6i����bZK[E}���M�%Kq3�i%�����U�	���	A,�ٶܱ�v�q�Q��/w��3Fۅ�6c��)[U�w)�<���K
?M���Rs1:5�,��/��CI����<=�;W��I�qw|^���2�7S7N�l��pL��Y{�<c�����Y�0�����2�F����:�!�b�yǀ�P�㓃d�2���jb�v�N���>�L޾Ϳ�=%��gf^B��})Sƀ�S��DnDE�U��N�H'���^�IeT��x.^�c�i�ɮ��#[����˰O��8�(�ì?7�0����� ������m8�ے9�U������1�u�%'�|�3+��t�Sa�x����qj��P�h`��]�f�����b8ݔ�K��y�h�5�-�Ņ,�7�G�J2�]��\�^�͹Ǳ+�6���`��j�Y3XG�^98i�{��@�M�X�.��08�-��Uy�X[�)JJ�S(�h蹘`A'Gu2�g3�]���	� �(��.�3]��_+������g\��$��A\���T?�u,\�J\�j��S��_��N�m��s��QW��W
c�թ��Wp+��T�g�Ш0���R=�XJ��=�Vz��K�ϸ4+��K�ס.�C��9<�zwxEP�AΜj�Wp�����lw�I}-O��U�cIǒg��X�rm�v�%�����FV�m+Q0�Q��]��P*�%Y���Ta`��T���^�եR2�˒
�٣^��QV?�ϡT�؍��_�uN���^�T�(q`�r�J�2`o�y�r�#��.�a8s�ˊ"KY�⚏�V"��J"Kb\��qœ���:Y���(\���W+�����zYZd�"�ī���2������EV�6.��ۮC�zYVd�"k����z���^�Y���k[��hSa�(/��l�pUV���e�Vֲ�õ�7׶Ee�e�Vֲ����Lv�n5�C*}b���p���>`�wǀ�RdIݥBJ����c@s)����]�7��:1�#{��[�΋���H[a ���J"���F*DC=T};H��p�w}sD��z��ժȂ{Q���ڱ��>T��fu����B��R��q�k�)P{MAeݔ2oEHWO��\5��@(Q�Ϡ�v�Spa{)3Bh�,����8�$J�V�g�(+�r����4c�@�K��DV�X-ϜZ�T�R)��l��څ��c+DhY{��������������F�_�Uha%\E6��]^Q>�J�z��[�ģ�@(�C��)Z��|�`N���\^4��������Z�;�F���$'��,��	WdsJ���^�Y\�1\}����0d���E��l��4yc���^�Y\��S����L�{�Ř�(��O}~)�k�<�iO(�zYNd��+﯋����k�zY^d�2��ڍb�1��
"+Y鲬���e^�	�,�ָv�~/\�"�'�.�^����:YQ΍R2Ē+�&*c�,97X��^&�W���գ��;���DQ���J0������^�w��4��a�e��Y�b��է�]�;��^�Y����˲\��ey�e������SZs����Ȳeյu�����e��E�Ң��)V�Z/c੬%��<���Y��N�7��w3��ض�ޫ��<�~}h�>�-�b���z7SÎV�e���DV,�
�{������8G�ᨦ�����������X/KΩ�����(C�[��KrN�������=��e�9U�論��]ۚ��e�9e��^���]��u0�[� ��B[ͧ�Z��`=�(��S��������6O����nA�ׅ7�h���,���iy3x+��ox�88E��pt�VP��D�#��p��6�1us�`~�"[�b�U�����nlH��Nx�ځ�H�V���[�x7f�    n|��V�:��)]禑6��Q0@ZnȒ�5���ִ��;(ܖ�s��%�wV1qz�B��X�w�/$�vou*m]a��ޥ����(��ΊR&�j��Dޮ]ߌ�Fx����[ߺ��	x[��Q&.�uҽ�C�#�E\��/��,���"m4Qv��ϛ�!x���}�k�K�>�����f9���Eq�W,>n�_M�e���^��6I�����&eE�2U�������ZnRֵ2ҵVH� o�IK�Z���Ҽ}oGa����a��� 4��5ZPሻѱ����y�n}�����Q8�xf�v�M`�CE&�pe�aڋ�*v�b� 2�#�(󞨙Ǘ������v�{�9�y<����
��?(A
I�/�)T	<BLP�	ڴE��Z�_~��' n[�(�u�E7sX��RRǄ�z������E>�p���MS�H1lw� o%�Sᝦ=K�x�PR�eڒ4]�Y� m�JV�J��x�v+�P��i.-n��nԩ�D�ޤ�]W�iQü���b������Vd�����o��ӧ��?~����OytT��g�@���y!��!�2o���g�``A����+�<�!�6�M00�(C0ef��q�!�&Ic��������{�������*Cpeq�ؿ���``J�pMۼ'}nr�hχ#w=����=GZu��*ۭ(:&�fҩ���!4�=��������; � sI�T�i���Z{�``r1���|�E�;��
	�++���9=�!�n;'�����f��]XH6k�?��~�,��4�Y00'c��а���c�P00�T�P��b��Ի��,��UV��g(��MU<���ya���7o�<����ͻ�,�k=�����t�҆/�2�*�z���2�T�2��_�*,c�2�t�����G���/U�q4���N�7�*Z✊\G��Qۂ��)��Ua����V��"X�{m����������E�QQ�J���	3������*'QS��Y}̾���W[d�"�R��7U��	dE�Ur�̵�X���m����3��6G���ǚ�D|��ܮ��^rLAT�0U�ͼ ��!���
�<桲¥ځ�ū����0PB�5+-�Z�4����Qa�B��P=�C�8�⽉�)˦� ��P=�Bm#��M~�)��@�_���Eә�
�2
�f�wԽ1�`ǉ�_��}��S�l��@BXʩ(e�0t�����K�5�I���`��_l?f�v�u�@s�O��py.mX�u�c�T-h�N��s[)i�+l֏�
�����i��-*��ʘ>��>�h'��
紗��+��zڤU�Cu��50J��!�0@���C��K���� o-�]:�O�] T�md�K�����\a�� o+��G���;���6�h(�'��&s:����^a��ڼ�'��ʃ�`�5��1�Eם$���(�L�%&�+���Sa�w��(�y�[�UXϛꍫ��dLc����Ɇ�Jx��|뺸E��ZXse����\k�� ���lMK�9�I�b<�U�m�v�N;��h�B�\�3o��o60h{��%5b�?���Q��#M��s`��]i�}�*Ц��ؕa۾(����jnsg��(�i���.�D}ʨS9�
����v�_�b*�N�6�Y�t�>�������c]a�6�(��/㉊���1��
s�aR�7ai�����3�g>��d��2x{��%�&u5d��\�
���歩�D�	���6gcju����uYg���~L��I��������Sf}���fnz�=c���J�O�~��^a���\*��l�$n�9��&U��Gm�`�L���S	�
̭0g�6/������usa2���,}�L�ǉ�M���⪂�<��P��[�ě(����`�9ǰi�+��܁G���褘2}�ȫ�NT�ݹ(b���y��4��'�S�i�XO�J���F��Mܠ��+�r�6u���H�p�G���Q@i�;��F�J.c���9/���>������V��)�@P�U�
�룼c�3�L�{1�{!^:aͼ��=Փ�0@<�'�u���3���x����	�i}�|et֔R��E�ˌ�Q$oUo���(��Y��Q�S��
�GA5���6U~5��gXy����D�Co�P2�"�h�Vk~:���wa�e�10-#���������B�����W00�������i���=�5̭0睼.�B�8���s� �P���S'~y殽u̣0g�LK�wQ.�����O�iSdu�XG�o7��g��n�X�U�]^����D9mk������W�ēО� W�m�vI-�H[�� m:��( 5qm���Kh;���r�[�E�p��2c�(�����+�B[=��F����� ���u��Ц��KS�i��#�1�ѷ~KX;����'m��;��0�\楗�[��^�k�S9����WU#��肣�w�X�0@�
��$����t8�YQ%BN�+�
�H��jw*�_a������?o��#)}<
�p�_GJ_�E`<~�a��ؼÿHl����Ax�R�3y�6.C0�;
ov���ȹ�[ި���G�:::�{xe�xSb�j"uw��M�M��2N[�ıU�w�V�wq������C~� s-��gq��9�a� m#��tڦ9OvЦ+�HN�<34�����;a>[`���bo)���n�|�w�7���Ӝ.��tG��Ѻ��(u䮦��}�����T<�LU�����7&�뙛U��"w�5���us��z�
s	��"����� ��8�@��v��9�|� s#�9��,~2uש*V�����U�U��@=56�ԝP�x����_���YO0R�g�L]��^��T��� � ��|�jm�+;�G!��q�θ��6�V(����ںf7Q";ֳ�T�#�{.kӪ��֪�.p&�M���ZXO-��u{z3Xa��v�Z�`M��}7Z5+4��՟"��NxskJ7���m�J��<�[��."����[�1@=�)/�n��m6��Q���*��%��� 1@=�OA��$]�=5t����Ⱥ�[�����<.��V2�1@_ɼ?��H�z8c���O@+�$�����e���giT�yƘ^�b�ϯ�p�~ꙶ���p��Rm4�rIYϽVj��k00IN&�MPa��ra���io,O_�?���6��vB�)�d��6�c�x�O�$sM	��󝝎�X��꧅�*
*=������_�;�r��W?-)������ �
�����j�N|�P5�v�0�k!��T�o0�QcM���c�����5�OQϊ�#����7NH���t�Ά��Y1c��7�;��}z0�t���I,S��e�{��Tf}Z�$��\Ō�Af==�Y��1�=�G�u���:+��� �TV�T��W-�Yv�4{Û�_��jh�f��@0�+ᮟ�j��Tܕ�-+��T��.��+&6Ym;�a�����qf�:%W[��e��z��ޣk��Nړ�1��	��x�v���a��w��~�w�NnOI����3���}��Qx�����j�^��s�;�ӎ�����D.�fX� mUh�b�3g;��8� m�v�҈Ӛ3(Eѱ��ĭ/:��r�D�6�vw���6M��H6ᄂ�^��N;��k��`�����v�'���T�C�i�PxwN��1�<	���|E͚U��#�,��Z�?�צ���"� s%��z����9c����_=�&�A0��.�{go��6{g�C�?��?ϴ�}���v�S�ad��$�m�zd_����~+?�SQ��~)��>������Qޝ�T0���;:�y�����������oh�N�=t���v{�f���8�N�7�b��n4�7�`}���%�M�D�Xv�#� s%̷͙�\�,�KX�0/�3ͦ�/c��RP�M=Y|�!�m���9PT�Ժ
O��F!�]A���8O�Z��Gbh�����Zy
�Jh��c�r�m�y���^APA    k�)�V��),��V�L��ۭ�T��K)0W�\�������g|��ղa��������*��/�pؼ�R�sx�S�'�m�7��si�Ӎ�M�
��%�]�m���`]�����2�.�vݭ�`�UPtk9����e�d>iOp� �(�=���F��Q���v�(�b�[K�m��z�z-�ˮ��\��D� s�2�������q���୅7�ͤ��W2c����r�h��Ur��-�=�֡����d�v2����mӪ&�ڔzz�TSUAJW�x����o��l�֎���'�6֮���c�w���9�#�V��PO��nK���MɦU�)���[��g0��|ۙ�M_�z�`n
�'�73����m����IId��1��	��{3st�7�BT�(��pgj������.%R�RBr�x�$ �9�Y�Șd�OB�Up?��$��JH
���/�M}����ԛ�� �|s��P�D־}�X�[ӧ'r�tn*� o#����4s�tn*� u+%�^(����ve���p�Ks#���T������n�z� � ӭ�'8w���'໇*���9߱�`(`�d��Nh?-[����0�\Ԑ�O�p=�5`�*��j����O%�A���cڵ����z��2qoڶ�Ҏ�0/g��K��8q���=�4�G�t���Q�m�ĝ���r����]7�fE*3�c�C�2���4�����:�P��$1N��02�J��p�Zռ�WDӶqL��I���:�f���u�7Tj�W�󤻙�ԁ#��#�
�����)�m��`�w�D���Lv�������N��Qsŧ�H�7훂1@��!]�L���=�����;�i.�ζbo�V����Z\�Se�I�u�0��xM� ڮυ`�:ݟ�����7h1x���%�������;�i�*�m��`���U2ۍ�qt���1�[n��֟9�mS��M��r�ܧ��8q�ĭ����8� �w�����>�w��p�S!w�9��OE�!�v��� �a���b�i�R�������|��F��x�$!��b�ve��D��'Nu����̋\O;�7�0�QdTJ��ͼ����žT�-�$`���\��\�K��9c��)���9-B#�8�ĭWe�O��ڗ���/ ��n�3�����X�tV���bf2��A��A���9s�w�,�c�]v�4ӄ���fz2�Br�^;���a&Br�Z;�v���/6��/���,���@��W��峌Z(�����:^�*W��^,G)����}������^xsu����ׅ�6
��¼xl&�U��|[�[0@<	q��|���̛�i�:摊����]��-n4M�jaP'�ӓH|��6�J0�[�����떊��H�O����&>n�n��������3��LRw�a���-OG��V��T��I�i��Y�^�`0��	�̮ݗ`�w,�KU˹�fW]N0@<	q1<�<TloJ`�'NUO�Q#��6sc��N_�T��ii6.v�]ظ��`����6M�#JU�z��LG�z�Mus.�Dkf٫�'�Q�Vw�y��|����ǽ��QI4�hP*���=M��פ�Zt3PENWr��B��eyb���J�a���g �v ;���ãW��m0 �l�U�[X�4�B��!ֵ�u-��fZ������0�?�Y�:�L9E��o�d�<�=~Q�R�hY�I���A�s��T��Im�
� q�L��-�:?l,��)� �T�������̲�tyD���� *1?q�S�y��g0W��^b~�fh��n� �e�l����m�I�J
c���߈G����� �(�5R�w<������dw�X\���)�e��d�9�1�<_��r�Ϥ� mu�v��kd6J[���Ў�vq�MT)��;
���0	mV�;۶[$�ӦR9�4IP*X$P��J9��H3�I�i��
xk�n��3��)v��ڦ�.ֈ��mo�`��w�=2��3��F�[���1��	�T�?���n�X��N�;�~�b{?2c�v(^V�_�Rhx�yް�R�{D�x;����L����`�d�p�ęⰂBer������vpmڿ`�9�3�*9n���`AJ����P�;���%�<P�V��R�;���u-��c�5�0�[!�E����'��2�;!·�TO2��tK%�ӐJ��'��l�3/���|��7M{�3x��{z�Ǎ�jq� �$�ggEm;Ő��7UʱO$6%s�׷��j��'Ѽ�ro ୅��}�ql�[0�������.�M�X� �|c�'Q��m�a�:a�v�{�:��01�;ߗ&>��x��滻��c�����j�x'�=���8����uBeq̑�����ʻ��x���F7m�y��n��מ���Z�s{�&6)�h6�L� uS���ff֜#���K��,�1U����H�h6ᅂ�d�=�<�]�;�/��0g0�2鬌{����_�`�z�I��O39t�nPu{�y�0/ty#٩Z��I��:3�<g�#w��^�T!�A�C'�-V�d��t�֩��9��� � s-����n����>�,��R�9�	�:�μ��n"� �|���14���6�O��2�;�s!�Y�Á��V��H�r���e���aB�r9�x�Muvn,�*���QV�v�[�؉j��J	�'���nZf	V�E��*���!:��I�����Bs���z"�΂`���۹��\ڮ[�7v�R9�x�N���k� m+���5_����`턵~����T!G�'�!���AX���]����A�q���Nd�����IX�ގ�awd3ֳ��8�=��)vVC� mzc!f���T�& ��87g������vS2���;��H~~�
��N���qevx盩�}��N�dB��ͫ��I�^�`�����%�k;�i�^��:A�j�#��Ll��Fw�2����$%�- ��a�u"��5�
WN�ݙ&6M��0�'�u]|�`��-��{j)��;��iO�+�g6mX��m��'�'}()���ؔ�-.����Y'���X�N���CO��;�(ڐ�����2�A���̩�9�6�8��^f�p2� s�ͫ�É���tc���f�|*kם(	�$*�c���i��P�#&`m�����x��NX����"`Mw�9�N,-�)rVu�7B<=	�R% qޓ��P,O1�k�-�ܕ���92g2�u^���f��.XO�*��#wf�>w�m��4e%*�b�P�?���y�6�I0�[��x7��ަ���9���v���:�O1G(�\�]U=� o'��S�ݯc���xX�C�.�9�az�ƭ!f��
u�c��'�b��TO�� ��ԋ>k]zN�ӌ�&��B���`���!4u�`�S6����L*X�g��xr�T��be

�����t�)�� q]��B|���$��@�0@�0�o��(f!Y���0उ�,�7�W#y�*;Q���0ŉ�QtF�E��.��0�W�����dYX)6i�`.��� '����W��܁Q�Q�u�C�]�TvU�u8k��w���k��2ִ-kZ��8��&�6�W �
q]6�:XLR���+���a$�n�3gܜBP+�8'q�Y�#��iU+� ���g��.Cē�W�a��!�$�)*�� q]�O���ۜ���aⳣ���떊��ͧ�aK���d�Y-X[\0�;ޥZt��57�VC��̻��]���pl�o�z��.G҉�;����օwY'j�m�9���0��0o)&5����m��O�Y�z��x�ޮ��d�Y5����9��j�����Ғ7���2�z[��[�h��<S!iۛ�1@<2�~�&e�2G}�o���w�i������SA�ں����fR�*����ކy��ֳ�Kfx>�w����԰<�h� mWhO�c��ԙ�� �|xOz�YG q�"x��;�3�d8�vT��wQ���R��16�C1�Fb�ՃOK���a��(P�	�1�GN�xl{��k��?D���*��    �I��������B��z�T]H��o;�&^lYj���D�5{�x0��`VTR7���9f�q���9���]���)�%7��T�d��N��
����O,˥ek�@&�QYG��?�+�.ʃYT��%Ζ���(w�,� w�N�g,a��Onf��|4=q�e�B<�?i�W���f�U��1J~-&M�6G�V&U���>r��+�%����ZD(�Ե�a�9z�-IA'�hʃ-�=[a6'���^w���N��g;�O|^�$n� q�����ywN����;�V 
q�h��2 1I��8n��Bd��=�C��~+)����/�Rm!�T�PT�b�Z�kn\�oDeu�	�Q3���{h�;��P��&2?�2�0��0��p��{��T# nqU�Ӝ�Lҁ��#�@�G�'���J���`�x��AVz�,����� 
�T�ǹĵ� �xyl�e.�s,�ީ����󩤝�XO:����2��Qܡ� izP�{Қ�'��QM�H�u�Ύ;��0MG�X6�� s�̥p���趭JUa��-ķ}i}�qZ�<�lL��P��;]\�c�t�9Mr@ݰ�?J�����)�q���n�b�wd�{��*�L�E�1@<�!;���l���+r�G�?e��i�����KQZ�?M�DS���Lu���t�L<_t��*�I�-����ǧך_���o�YVK&�]s�!▉��H7Uᧈ�1�<<��!�Z�������X�sV�^�D��=�a=43O̼�Uͼ;ө^W��j-�UY*��q��\I7��
s-�g^��	��1�\?�#zyVFu�zʭ�"��x��{j� �ڭ)�m��܊�Q��#ŕ�J��>d����2�vc�%8�8��}��Z̖h5���=u޴f��A㳗K�:�����B±yL�xgm�p����{Ll�Auw�h�W�����E0K�-��V���|��{qC�n�h�gU��<��܀k�gR��.�`&X����b���E�f�|���ܛ�C�M���XI�����O&bn�y�NLIa�����4_Obռ@򍤎C�}!�9@!�iv퍥���Œ����ܡ����c�y,̟�Jѳ�M�b��y�C�.s�����1@ܬ��S�B�ګ�
�nf�0
W�Z�e�pg��\?��$����!��ͭ�6�7�T�3W�jM��!ޖy��q�A�t�h�c��+�K@Ҵ��Ʊ?S��3��|uN+��M��0�x�{����f�m���!��9�6	�����0D<1q��f�x�r� q���l��x���T��mShk*m�Z�C�-�.ڕ��]i�$��"�5��6I\��b$��)0�c�y,�y��y%��̱G���Ǚ��7����W���ۯ' 65w��u؜�� �S&�8e(H�0>?�t�-�� �*+t���	��tk��\jR��a2�N�am۵��� �7S��{4N��i_��ۮ��d�rϳ?���������|���s6���s�m7�j�[�*���3�m���OukG���Q�[Q%��MeyS=�%��!���2�'t����F"�/xB7J&�� `��J�G\jJaV'������v��z|�z��y�5M�:|C��Cս%�#n^�k�i�O� ������u7�Y�i���ae�:)	C%I(�����y�]��z�$��{�:����d�p�q�Pu\1�R�7��;6�vU���6�zc���Z�̝pw��\a�wx�Z��e�Ҹށ�"����D�2��
 z�
�������Կ�mwPL{G���<�a%�:�i?j0D[��ƌ~F\caI��{�C�ua�ȴ�[*��'i0�<��UC��0;Ie��15���B����N5I� ��;&.յ헷$�~�4"�qU�ʴse#i�5�"
q�K%�$�N�
C��osd`?#c�`y��Xa�y*�Sa>!��`�NU�*0�B~U�ݰL�y�Y��DL7^@T��f�V�Y�u��`��.�Y�53*j�,��2@&6@�b�4zɚ�Udץ��N����[a�Ֆ��{,�d�k~��zl��(�0@��M�.��1��4�}v~VX��nJơeh;��'��2T3�e�=ϸ��a08��TO�������ꪨ�0Hԥ��~����0 -OQ��	��v�5q[�8_ĭ�O�"�(�&.����ŔJ�:.Th)��jk���,����"H�]ۛ����j��q :�y�JP���J�� ���ض���NuI�[OIr��I߿�wiI��4��R�Z����i�16� ���%��[c����?�i_֥��c��B�s�
�=�rhjV��bj[_������#嬂Q��:wrU������<yY���e��m�l�<K�#�yքS��s��
����d��%�cP?I��@��v�Γc����9~�@˙�����Ϗ,���HհU�>�S=i}g�z���ԙ�eWa�y,��S`�!��AU�E"�d�� ����3�y]���sūEB���T�b���9/9��O��[��b��)�9�&7��J,��6�`��-�E�j�e2+,�9�P)��]�s��e`�j�{s�!��y��	��5��lѷΖ�;T{��,Ȗz��^a�z,��筝P��i�]t�8_JL���A�i���;��2Z�nS��[�9͘��ֺ�������?+�����Ԫˤs��7�ͤ�F0����#44����C�U�aUW��AUU�Ӣfy͘��aE�)GLk��	}!�GK�C�}�s.���9�y�E=�Qk`�@�LU^����`�y,̹�����W��?/�=Z�s�k���
�oB}v0'����w�[��2�U���K�(�z��dn���<ϗ�Nm�(�u&��9z#�P��_���}�'�bҭc��^'~��ə�Uw���SY}WGD��}�O�D��1�8�[U�/߽�f��7є*�{���{t��}ج�3�H5)(̯,̰O�ƭ���(�F0��BY�Ai;-V����v%s���ʃ�҆���"��zO{"M�!ꖩ}��Τ� �C�]�Ί��~BǱ��UMT��!�PgM!�IMT���kr4����#/���u���rPV�!�0OE%��j�`ن��b��yy@�YL5K��C��U�S�f��/,-e�4�`��*�u1qL�z�T�EL0�Z��6Y�E-A��I���'�J8J�D��������ٯ�ea_���݇��{���|��O?f
{�x���?��Wj]��|��c��݋���ߺDr�g]�n��&p�D�G��N�zϋl0 ��w_V���D�,J���麫0 �V}�t���U��r��,9C����Ώ
J���<��x�<�u��RYa@���:,Į���
C�@|)��ca0��Xk��K-��8:A����j1$$!�ⓓd|/Zw��V��U�)�VW��+ɡ�[��u����3��� ���ʢ.bԮ_ƐZ����qɅ�4Đ�T$�RX�W�"-����-�r�u���f@��M�^l���(*UWe�ҋG�S|T�A���];���G�CBl���d���n�!4c�[ ���:���l&�lTzI�X"�_�kC]��s���>J�����iC�?<���Ûoz���o��U��B����(a���;N�0���t��U�ЬƇ����Y�j��22�W�{+�2$�޳Rþ8�������P�6��]�?z��0$(Ay��,Yrr�
r'���zA�.�x\U�!�k	��G<lvU�9*)�r{+I�Eݗ�F�˿�Ksݯt#�E#yN�W�r��*Tk�������"�51�sQ�ޫ7d�����^�GK��:����_V�E��QNT��1�/��]S�)����2Nq�r�)��S�l�=z�ҟQIg�P�!Iya�����J�,:1$%)zFM��̟�b@L^ḃÖ.8*'6�uǐ��8�t:�ܒ��ُ�;Y+	�E�V[H����bǀ$������;����w�!1���4�����	+k'(�8�ȍ��E���    ���_��� �e5���m��O�
�ȇ����e��S�[�C��B1m���i�v V2m%1mm��\(C��6�;�L[�����MhTb�CY����ԃs4o�wܺe:Y7
�::B�S?r�3n�����2��b�hG�
�<��m�pl�+�"E�E��pV���fot�r[9Z�� .Z�Tkfɉb%���b�q���YECQ9��lk�e��b(�lW?Ao0dOY�馼)���4�����;�Y�]3u�Fo�1$#�q(��i�a�11nr���F迋ZL7�^|����b7
�������s��S�K�!1QLD�!e�I?ǳ�ѯ�tc$%6�k65�O����X�b�g�<v���Ѯv���2�e5Sq�)���a���6�(+��lIy��JdY�4�F���q��v)�1G�7a��qNҨ0d�X�1��i^0�vk3���e�和Ά����`į[��97��1$)��(Ϩ�jdGm��B_O�n��Z,+�]���*��7*s{����vWjLc+� �E��(���Z�;.�v6wcy�M/VV�kV�8�>��;:�3�.�~���[qT7{7̕���T$�Y;��Z0��1$Ɖ�����k/o��܍N����x��+C��[�!)k�2~�5��zp�!=����co�����M�CP�]	lFS�����Y����0l�!��]ƌ!!�A��[�5|�Q�xk�eI�b��$�8l*$Ţ��3�%���ϲ���"[ﴘ�ֻ֫x����^{�Ccd�����C�Q��i?�)
b�[� 8hUߌ�3�ݨ��ɾ���u��6n�k�f�O��Tu���߾iĐ#r��?������6��
1$ˊ,M��������׍�$�:�C�<�T�h���/B�U!��.=��ွ"�!���Ԕ��N�=*�6�C2���e��`��A][��H{����\aH�!���~ߺ�� ������q9݉fn�1yoDN> �E��gPwC�dE�~dUc���U���!I������c�ӥC�SĐ/R�E��p��Ʃ0$(A��KY���ҩ0$(� ���fU-]Q��*��`�)i< G-'�֣T���n��h
5ڤmAU.]�Bˏ׽��J�d \�b7�z�q�7/����$R�E)�޸����(R���W�|ԬC�BT�R�M����v����7[�:%zH��_Ԯ����Ѷ����WG3��叺S�h�!AV��<�@II���eR*	2"h�I(�L��!!���z��M�����D둬>.ĜʄT2�M�JWR
����/���k����?Ĵ����M�<��jR��+hHC�/vu�CR\�R��/d�OV�
CR�H�h��}}gƐ����3�#f7A<��T��EΕ���
��,Mgf��^N�*�Y���=|ALgZc�e���z&\�:��O��U&/�H�����8���`	
"��O�0F�S��
C��b�`P�ؾ�Z�1$�){ ��֔,��f	�"H��Ⴭ���z38�A�mJ,)�)���Li���t�Z�"K�t�(�7,G�j.cH�9�p�)�F��(���UP��Y�؊*!WN�4$���"�����?ÇU!�]$����J��H�>��SaH�I%�ؾCrL�#��j�t�<2 �Ҋ"Eҵ\Fi<�m}#JVQ�Dɪ��c�h̍ѐ�%����Mب�s�K�!9Q�������"�7E�UA��?�+�VVu�L�3V�z�}�0 ����(,��?BU�{�(\4?
"H=�����^*�'��a�T�;�_*��!cH�+b�s��ۍ��ku#HQQtt�`/�{T1ĐF�'򿆼I��oƐ /��&芔���Iڊ���_=��'�^{�0��ףD�������i�%�3�;E���䗘7��$��N~����;G����\	R�}uu�]����%��-F��,��I��FAGO���
u= ޴?;c(V�H@�&D���>`��mڑ�:*ԕ�x�Nc(�\IT���{zV�J��g,9gNqD�%	��%gõ�x��q�<y��^�%�r�Pj��������nEg!�bɕ�K��Ѕ A�����J�z��.f<��4Q�;�%����o��M�ۭ�&�_��&S�i�1$E�����/�-�bH���B�_q0	�?�{%c@��%�l�f(IT�ٙ�oRR����< 5��P)�6\�1$(A%�ЌŘ�G��IƐ /��	�����:��z#7K�(g���ZC2�$Y���bH�Iyj�B0c�F�Ѯ���QJ��_卛aӫ�9�ɉ7,T����g1�O�>ټ�Cb���e8�rB��ɡsA.WMR���6��1��^!I�R�9&@ɉ"����2�i����7��"�yb��B`x����*㊐&E0$ĕ�|��b���{�����ϯx����g��TE�~�ȟ�}��㻷�ݮI���0'M�w�e���RĐ-B������`H�)B.��=��n���+B9��ʀ�m��~�)�{-���|4�dh�u��
�8��fX1��7�"|;�Qʵ����bHH!���w�U�����+��_4݄ݨ`�iY�ǻ��/sN�0$H�p�C~$�[�Eg ���2��2\�oj��dd8[]�!/��E1$��pԃ��b/�(�Ժ
C��Ȣ�f3]��B;�d�y>c���TD��	�v�'�8@)�d=��W��nD��������i9��_l�;�Љ�V��#~t�b?a7�i2d����"e�
jL�i�4�%������aI1e8�s6\݁>����;�Y�����ζ�C���ҟ%K7�Xvɢ��x ��Z,�Qӟo�!AA�~��5���䴭0$(R2�>"=2$�@4�k�N��7Y���H>z��D)U�n3l�叞m�;��2��G�>��=�]+	�2"U�����5�fv�22(�*���5Y�:go�s��!=mH����/?��w�ʧ��Ⱟh�-F�cH�A�o_:""��A*�����_0��W]��o��Ӕ������P����Tg��\uj�v�Vm�S��;Q���;a4�P����n�oƐ %���7.�}�=�C�t��Ö8�叚V�`	2"(���������u�H�
C��ypv��#1�������B�.�A��C����R��/E��aZKj,3�?�z cH��9C�ڢ�z�;��)E6�2"qS���{ݟ1$(A�1V"Q>��ˌAq��K�3u�%�1$HɈ͛�?�6��C�t���2��+|�F�.��)�g�B�~��DmOr*��,�~�ڞ_���G�_)����Lݦ�.PWn�6S����Nt����?ڟC��9�x�R�ʠ��K����0�0�Qu��Uz��e�3/b�91�46g�%����e�Y�YT�^]��nl��!!J�9J�A	�2�+����f"*�_����N0$Ċ5�!�k6�`H��ṦPq=/.�m��`H
�S��
pM�ݎ!At(v��X4Hm����q7��������)�C����㿏kM��R�vx�Bs�b����4�%F	�<�r�o%������MJ�6S�	��~�mT�M�b*�a�tce����o_aH����:_+7���[p7<��:LTmJݸ�k��鶐�1���)#�c�a���gH	2U�m����Д�1$+�ZP�A������-�[ҫ�HWӰ�'\��Vx��52t�QQF�����%_u`�`m�E4�z�_8�Rk�	�"H�_��ҩ�Q�!A����E�'�'a��l*ީ��PL�|�u�C¯r���ӵ!�6�}ǐ$/CRe<$j��Sk��U��v�ˣ�@� i��vI6���A_VM[�CҒ̡>�P���wkP��j��Q�>kh�}�
��р�׳���v�&�� -�x�_��T�}�cH�)����5A�C	�"H)zܚB�mM/�C�h�T� M�Q����TgI�2*jo_�M�Fe�t��ٔ���f�`10�j��o�ژ�R>�^$i\�S�I�1$)�$J�������    ��NifHږ��ؖ�X��jh��!I�&������!A�$���c��o�/5}��n6�F]��ڀL���	fS����Y0 X��l�Ѻ^ʵu��1$ȗ��ta@�?�Cr��Q��i3�/o�fx��E���M�"��_R�􊟾���l������_K�m<�`@ʆ�Rڶ�;���"���%)�SZ�hKc6[X��9q��~��h;��{�_�t.P]aH�%ώ�X$�'��jn44��y������W���䋠r���?M�i�:A�'�a������tC�����L�~���[S�۰�e3�w��2M�
�˪�gM���Ns�D�"KS��K��������1$+�氓������t3��P��9�$���n�G�5۞<��'1�
w{<ް;S��9�#f|t�;��#c�xwQ�NJ�Y����n!&v�н|�s2�$�2����ImĨ`H���"����������0�$�g������bC��H�[oԋ��^s{�0�d�iP����~�(>�T�j�H<�ݭ�<ŧ��jz�OA>�g	�eDŭ��#=���#�FddD�sF� Xb�
����$(��(Ɛ�|6���6|�Q�L{l3��x��
��w��%e)S��|]���Q�n�K1�jߕufƐ�$B��!�%@�'�)o��C�y˗=��q�-��cH��!�j�t�˚�ٹCr�@8�����Ǐ��||�����������r߲�4�`H��������+R����!��H���7�������o_�}���ǷĞo�Cb��Uh~�#4T����AG�a��m�����q/T��V�����������wo���ӛg�e��������:�?�3�>|�K^�������?��������~���~z�������?���XZ�֎��D��+�m�b���[�^�s~�\,;�P|�9l��u��6��CB������n;���"咒N_4ͣcǐ:��ߕ�K�,�m֨�g�t����U$a�<EŚ�+$�}���­�ȋ��*ŷ5X��x�
"�sܰ�eݭ���e�1�AЏ��Vn���D�d�����U�����Da����R������cH�UѢu�sԷ��C�������;�Q�ތ!A�:�ˍ�(4.�C��x{�F_�MF׎!)N�\���]�]CR|�r�6E_Lm}N��� R���Sw�'�0�S`�=�j��d��v�d���X[�F6Q}áL��~Q'��-����A#_��N8�����jQ_8���VP	�"h�x
���i�cH�)S��v�}���Ne	�"�.X�^���軭c|ǐ,Wfo/�ua�t1���z����Y۞A�!Q�kk�1cm1䣮[7��,=��aѹ&�4����ް��媠~3�(�i��Zr�^���gu{@0�Diۯ���K�����Fe�&�eǐl#�5K�ZRd��Uٝ�@�(#`�h�DP����L�ܯ�xcu��m�6yIP���1$�A�`�(HuK&�p!K�	Vb�6)We0(`I��Vn�%k������⍒���j�����i�Ad$����EԸ�n�̭�#+�=��F�S�1�����KD�b�+���ڌm��v�2"JS�dJ�l1��Um�i��,�m���Gc�w)o��Q�!YtF��I�pr�9�&�vǐ��#��c���g�><��nX\��cH\q�V��>\aHJ,R�L���h�fc�ɡ��A�Aܹ��߽�����㷯?<{EN_=~x��o>�z��횜�<n0������.P��k1Q�M�L��+aR�pޒ���occ�,�������Z,ֶ?���
�X�6i�s�����MF5+�)���[M���fb�Hbbn<�(f�J�Db�_�t�}>�6��0��4�	��h�'�����?�D�#�BR�?m�Jn�3�!Ȕb��x�Wa�[%�Y�`�E����JGTf��_ZE-͐+v�������0*���CW�ƟnjK9�C��H���P[���d���^����;06�Y:V�nt��gؾ����Ԓg�)��?GZ�cHi^U�V'e���n�DP*�F��Gm{�2��"���,h�^�?jZۼ`HPA���m�c���^AfȢ���"�$k��}��!YtX��<,��]+�1$��km6���mu$bDȕVG��E�b��K~�탍�F0$ĉ��}|���^me��"GSJ�ח����7�-��T`m0!akk���C��b��k1"��=�����p&�h��g����(D�I92K}z|����֖"��D�~p��\Y�`�(״K�Vw_K���p�R��~x�!IVN�ˊ
��ۗ�`H��1}���}�q
��y��Y�����H$�F[��6��$J��C��H���K���;�$�"�s�`���c@E�:�X�S�cH�i��2��D�{1����^�ˌ7�3[[�1$Ȉ ^�K����v����l�8Rw����54�v�5�� '���R���ݶb��"Kӈ�t�pP8}׵�n��,:,�ܕ��_�M0�`HN9W�_�mr��!II$�ϒdڃ��H���骺�KJ�du�A��,%���A�}|
��"�3D�"�cD�g,�E��$Y�Tĥ���;��O	�yA�jRmv	�"(]����n�Z�������u:��姠PN/%�7�Ʌ~���Юݠ���鋙��A�KK��NK��0�uԞl����p7��X���oԅ�}�u�as�ɼ�O������M0 �)�,ޣv+�d!�Ѱ�y���6�`W�ݍ�VGyG^�p�Í�M[{ǀ2eu]H׀gǐ+��gu�6�p�G�#b8l�m�li����9���F���v�c	�҄�p͚�۶]i�Ad��S��.����cH��ܚpϛK}�rƐ�(ݷ?�m�(��9N��M(_C!��m�ު�y��͗�+R&ݶ�UD
k�v&���|+	1E�9�0����]�*	�)���]
��s{�
CbRs�ey:��V�A	�*�>�l_lԫ
CR���a���^cΞ�U� 2�;��=�۸s��
�y�X�Z��3R�4Đ[��wI�m cHF^<l[z��(2��!Đ�,!'d��a���
C�LD+@-ޕ���L��vɢs�j5�=.ȝ�?��"h�Y,��ǔ�: 
F{Z���SNeVF�+U����T���n�����cHJ,R�r�TyjT�9������<�XT�+�ݤ�[��O�Pl	T�N]ιZL�!1MG �h���Q�OQ��^��X���g������aTZc1�0$�i��_�9>}N�n!ƐZڇ�|�&�Jx�j!���"�~��K�7�]��]� d�$�6aZ�CQ��4I2��#UM,F��K��:)�%RĜ9�'�o�JB�!9���{���9��W�cDN^�y�Q�Gk� +�4�� A��;��`��r����z;��?r�����P�h�=k��tq�&���M�-_[�Y��t=����!9�\�6g]�u�ۇyi?;;$���u�i���������-4̱{(��4����1�gƂ��E�>�?�_�>����}�(�."?׊��h�i����~>�i���n[�\�8w��P㣥&�l�_�t�X��kK�[KI��~��iD��9-�+�)��4D�L�y�}��5��n��>�j.��d��i\s7��D*��z)�F'�v7�}L���&����N�P�8pN�Vr ����K);AC���j�d=����ub2Q�@iS+�lԀ��P�dD�����S���P�U�����Z�!�g���q^�����}{�v�D���/�Q3ݞ��)1D�������fJ���=x�8��Nv�����`�!�n雟&e�!�aȃ�>�Pp��e���1곆(��ˮ���1e��x��~3�� �A$o��d	PC���ۯ}Ev5J���֏P�eAѷ�_0�L1�#��(�)��/�G�Ee��(�Q��J    ��JΉ�j�88`*� ��Wc5h����2(�N-��Q7z�݅AQ �� �ݰ�;4����DC�!Hha �qX��D�_��Z�#��C��w�_K��n �O�pb3h�����1)sh�"{�f�At�_���{17�A��^���CCː���R��9�˪!N��8�s/�q���x��޽UC��$4���g�j��UC��ٻ��Z��$1��{������P�[u9����e7�@acDi�j����膑�}k9�ڡ�0�1��4�9d�!�e�~�1�s�P8sK��b/'޽ϔ$���5D	���)i�4�!Jd�z��e�[���QS2¼�Nc�E3y�ݢ�R���ja�>�X�D1D�k���aUC� ��_Ne��0D-V���a�bXf�fB/j�ƥ�tれ5ġ��[�̋K{����)��o\��W�ӫ�7�*�5����b�x�!Pd�_w�(��� �!���lύ�V5�p��Ks~d����]s��Cd�>��''37N�t�����NC�ѷ��;en|�Yf�'�/K%{lqv.s�\���ˑ�U�Q�V5�����p�8WaBÈ�}F)�CC�Ș:�[v ��e2k���"���X�sh�����ޕ|���(�H��j0BAt����b�thb"��]IꎿV��2��wݖG}�8y-V�?k�D߾8���]��h�"{�ԟz����oD���`��D��^�6�13�Ky��l���bu�[�H���3�G)ŵʕlt*�:4R������76��4�������٨ ���]L���ٕ�+5�҄�?�^\`��`��@5�L�-���ܦ8Ƃ�!�g�lPLN�]5�	#��`��IE���%Y�bs�����CU�[
9~��=4���?}� ����WAa�>��@�A�d߯�`�n��UC �@G���� ��8)�gE�X�[m��E�'P�n����$��浊�) ��2�@U��;�}����t��Aut��^�?�cS�NC��@-'`��+o��0�b�"���Pl�@8]Gu�Z?�bt�ײ@T�t Lt��]��t")&ɺJU��rAA�!�n$aW)��/~:���'��e�~t�Y��[4��~��i�FQ�>�]��4PC$�$�g�}c�NC��Hr�Х���"E&=�b�Lϥo����;ز�P�i�j���j�^fF��(�)�mO�Ew�u��F\u���9\o��P�i˟� �/m�:�,���[^�
@��K�!E�3��__��a�J�j4gKw���� ��ѕ5
t{w������^�����������F���kD�k�����V���r��,1L3��c��/eԝhT���CA��B��^�F*A�gN2,^��F��D7)�HU���KzI�6�:���K(]�b�B�;}q���'R1�n9�P��?WY�s��XF�,%�Z�U^���5%��H�I���R�j�Z��i��l��T:�V�8��7�D*O�-iI���ik)�j�^�w�%��J��X*�����f*V�݁eѧu=T�i�$�W��yȬ�j����i6�oj=".��E���2�5a4"��#�!T��k!]3���@�AI
�:�5
�����wwEQq.Q�g�&�s��!Pb�nW��^�v�f�M��HE���E+�*�d��j����`}j�׳v"�Fz�>�`�hoV����ËV��4��w+F�q4/Z���݃:�(R��M���'����j�5�;Fm�n��H��Z��]6�:�"��X��F�eW��(1�K����M���׋��(I�K�NC$Ť:zH�O_�?:�t�2��]\�\Z�Ҵ�q"w�+q��2G7�֋�ѫ�iy�bQ|��~.����j��\��kd�4���z�5�QR�uPC������߿~���Y%N���%�*�ȳ�f,��4DQL��P�=:[����!�n���t��lk�b��ڼN��(v`�H�	���`�%���+�N������fQ<SD	���|�UC��(�a�]o��4D�LY>�����|5R��i�I�>��\����X҄'>/�=:QS�>/G}:Qt��} ΃O��0�n � ���XM��%#��� ��w9��iFq �}��U�;YC��ԃw
j��e��!ft��3e�*MK@j���2��|Y)���8Yc1�li�$m�A��G��n�����ajk�?R{fI|��J�Jj'��)kdT�>�-�T�T�~�H��@��������j4��U�!�gP�͓���j�خ�j���V{�,�c���Dk���"�<�q�N*5�g�"{�[�ꋥ��@�A�η���¯jW��K3K����`��^����
홬Ћ�;�r��^*;�,��v�а���:�������XC � j�Z��=[��00��X�YԳ�Y�����%���Ù�0��Լ5�#/�@��j�Fj�/:z=�2��PT|h��eƗU5�R�*��l<�\�1��n��E�A�-����獥ݲU�w^XC<�g6#{�R{�ԮJ�'��.&!�3��ö���A�\�v	ȁ'©*?�-�� ��e�j���(�],��G��Q�������-�͝�*���Q�tHr���6�Z��-�7(XC Šr�З�'3NeXC0�0��X��v�u�M7�Dň��t��K��8�в�@�AuI�G���gҸ����o���0R5D�Lz+�<è��=�v�g�UC4�g�CP�Ee���j���ΥnwlԎ��j D�6>��`"|F(Qš��_P��qrQ5D�L{�f�;e�(^��X�$�Sju���5D�L��7����{�\#�<�j�g��%Q5�M�<�Y�4,�5D�L{F���C��h�Cv�CϠ�����HT�����/�׽�H���x��{�5D�Lz�Z�~�o�������g��7�_���8k��Y���{���x������H���ju���0j O�.�=��j�/��z�"Ӟ:��ｆh2���j���f4��ý@T��\gp�Y�dF��DwSB�t]��o+W�U��J�E�H3��`��'�桸jf�$�������EW�%�	Z�Leqb5Qa�3�|"�|�j��O�9�V���à�J�[X�7��/��5Ċ�ҍ��\9�}�uc�bQ�8�z�z�y�"�k�Cu��>��ce��@gzDo)ݼ%*t-љܖ��հ��!����OT螝;=,w˞^C4
g�SH���^C$Ǥ�
{�2�k����tӂ����(Q��{��<,w����h�iO=��I�^C4
g�3���#��&bm�@T4��4gXK�Fd�T����g�!�fu� J������0�H��n:�r�'�����ɤ^C,
��w� ݫ뇉ˡ!�c+}��&B�|� �5
�N�R�C3�/��(2�|a���a�:;4���p&5���B߰f�r�_��:Q+���5rh����^�&W�vz5	��IT>����W��hphcSzk0�Ҳ������\)zi�FQS�Kk�P`��D��.u���?Z&v��@�A*r__X���M�u�BEw�����ߖ������G{�]V�ojmYA���a�h D5��LJi���(J[��7T5R�
k� 7�Q}s�r��E�rR:m��,^E��!��P�Q�LJ~�--Gm�`o�UC�|w!ꦖC�v�=߽�8�9�T�Ҧu����!�o�v��^�:gv��9t�{���!˖CC�Ȥ�t����С!R�Щ$���נ+w��U���g�k�A(�5A�MF��"���@�/��}9T���2�s���JH��[��1�᫆�T�_(Qh��a��* ��h���q� W5D����ݡ�(�)�z��s������>z��Sb8y�����ӯ�o�pT��Ϝu]\i2ܬ���� �
�Y�����s&�N7�Mu��;��vybJU)S�I7q��}w�mC�(c<��Y    ?�tXt�TQ("tݳ��/��k[�^C 

����~������߿[��a����3,]a��Q����8�=N����F�8U�b{��O~=3~��%7T��ۚ�g������/~����׹v�hUm���}��|���aԁ_�=d����ھ̲�W��a$O�(�����˯{��4uQ��f��/�ց�rwϕ�Hv]?LN���!�c�z��ä�>j+�2�7����a�M1���#��l�y�_5ĉ�t]?lα��X�99j���zZͬ��П^�5 �J��U��}u�Z�ㄬj���8�%��.M�z�4�Tkv��ı���k�e���~�~���5��M��ÙrPQ��>Oӫ�@�A�'�RS�fT�3��!�o���r[Z^=;5��ɠ���(����p��ά�*2J��yݱ�Xz��UC,�gR %�G�9�^5 �*�p&���uv��;|����
:����hT5�R���zˋ6��d�>wc��2��P��8O�!E	y�u����z�a��������!�o$q�C�:�#XC���'�d9��5D�L{R�P-[5D�hqV����*��j�B�ᬀ�T�8�rUC�(|�_@��E3E�MK+ǁ�P�pK��e��zY9�|X�i�!
Ņ�J����l��q��9I��F��D�n�E��������2��=M�(0H�^��m�ft�!Vd���.�K�܍a�j�E�tIv�X����%,n֭w*�g��4}J_ӑA����A���~���@�A���4}�פ�mq���2��<U�K�1��)[��U�EQ�L,���(G�.Q�d8��Nr��ܝ��j�B��D�����U�rͶҿ�����&k���[sM����"C��ǈT5Ģ(��Y���ƽHT#��#��?��k���Ʉ2[�?�t��R�dx|���Y��T5D3L{8�t��5���`�L�b�Z�J:$YC�Iy\m}��� �A�?���}������~���kmD�}�7��5��T3���E����A俁���7��o��pTd��C���XƮ�j[�r�ԓYX8�9�q�|T�1����Te��jTG�8(F�]F��0H�����sf�!Q_�fp<�thB��L��n���o�a5o�x~�Z�%�x�!Hh�6�@��ј�+���^v�8q�LI�(ُ��-.���i�7��UƖh!'�pB�C��G��LVfP-N�K��\��2NK��u��b�L_.��,�,���sŻ�0����7z�5q�'�pX���7�r{�à�&�eC���CS]�ҍ��� a�"�h �%��� 	u#�$
g������X�n�j*��g�����b�Q7�$\ߔAħ�L�����9TR�ܧ�x���Ĵ��u1H�Z���>��YC ۞F��}�bAC�$*�g5^��4�(=�<�l������&���i��~"�H�����T�"?�~qn�Q[�DSڐ��t*=�b�7���}��������?z�����������?���?�{����~�����ڼ��z���������O_}����}����ǯ����������o�_tO_i]�q'��Ea�d�8?�����E��o���탃hDw�� ���j�������7���������h�1�R�.��LT���\^S�����SallٷLɞK��o���1�W��4���N������|�T���b0�e��CAC$�G18��dAl���z18��YC����U�)�s��7��89��3��:�/����gJ�ɓo-}�!в�8�������߽~�Z@>S�)5Ru�/>~���eҴ�gm&Qa^#)Ƭ�x��K��H1H�A�IQ�(��$N��nz�� U�&� �qT`Al�HCA�~�!�cH�S_�V/�y$k�♢eq(���D��+Sr H���O���$2D��iB0�^UC�� �Wo��۬��V��^�_	]V;~(UCŌr�oS���b� ��)*fMF��?���TAC�o^�'?YC� �7?U_�� �!�:�bs�0|�]�x����M=�3��8��w����B-V�Ჾ^C�Ȥ��֛v��L�)5R�^덖����D��;Q���&N.�M�Ҳ�����5U���~|���Av��T�(�)R����d���%���D5��L�Z��e�t��ޞ�PT���T�ݗ����i�9ѽ�%�ݖ�*��)�|LT��Z�4S�xD���Yvr�W��JV��B��螩a�5D�L����5DI��|�X5Xzw@@�v4�����c��k�CC�-b�q��b�'ڵ�z�rg�5D�L�8P��n�=�&yC�fU�q�5D�LzT�S,��YC����0ǬwȪ��Gǰ���o�,��˽�Ȩo�޽�ĠzbW��1��@T@��4�`���O�pA�����j���\X�J�bif�V�׏��v�vܝ-�(VtE|im�KQ�D.G�:�,�R-��h���4r�"E��Jq
��:;�<��8Q���˟���q�!V`�n��*.����R��+���Ka.u^P�:q�/S�NC��A��jR�O���mŴ��/��4�Q�)m��3,}�l��Ctbif��������:���d�z��+Y{a,c��`/�E�!�k�3�1��0�1��k��qu��v���X�Y���q��,[���ݹ����e��4�I��B On���#w�x���4�Q�aWXY��M7�A��$�k���R���I��M�3��P�?P��{a,c�~�M����F�٤��j��GŖ�@�9�Tqs4Ik=)��˵o��X�#G���z�!Lb���e�� &�#w�4΃��G�C�K�#���@�!�f�ng:W�R��r%x�!E��躟1dWa,c���M�1�Ua\È!���;a<cuҐ8�6�Z�NC��M�V04h3��5Nw��1-���r5|^�p�ӻO��B�O_������۷������Ͽy�����ު�����������_|��Q?������?�����������_����_������7!�S��ˏ��ɗ��~���_��5��8�?���w?��B�|��������E�{��?���w�i�7矠6Q��0�Ǩ�)���ڷ�/˭�Y��~�ň�n����׶�7�9���ZRk��/�k��s��`4<\3h�8�*�_�w�X�A�zOb�k�j8\��;�h�8s3�ҤUеL��H3��^\M�Ѿ���4P;�-��=����h$O��z�,�;���+�r�ȥ�-JXbV-���NC����}
;Qb��Űž��5�䌉)|#���\��u@��Q|!���o�o~��ÃRO��zA@�!�f���ey�i�b���W�'���&J�)�K�)�3$%K����K�L�!�c�n���}+ƚ��)$�("��@�R��UQS��@5L�;���F�Fs����%1��Py����(�)����ji��@Q�ubc�vz"��f��dҙ.������fe�÷:�i�U5��U_�e.�:��������՚�6'z��Ӎs�s�g�[�wo��DהM2ţn��;sw��3H�#[��K�NC��@������%2%�}���o~y�0a5�My�������yi�FL�k��
�߄V/\HX��yĭ")&��"ϻ^�i�HX��V�l.���0����T�Q�"r����@�AZR�R�NC 
g	��.�oe��l��o|y	�v*��(���Z�Qd�l����?�ԃY���5�4�J��b!jZ[X|�Α(�κ�(�yT�+v�T�Y��Z�iJ/;�t5�X,s�F��p��@�A��i[:��vݸڬbYfi���p��t�Q�8K�D���Q5��O � ��|UC��@�'������@�A�nծ�T�ub%fi�U�\z�U���}٪U{�j�i���$+�d�z��wퟵэ�d�@:�ݤ��1Lz�9tn=��h�i3���kk�/sʴ,��94T��� V��3�3��M��楲��]\���0Y9���6�>    sq��Ta"cJ�� q��nvM��1B����s��Tͫ��W�(y$o�7�LLE�!�j$�7[��=�I3I�9�j��jw�H��X�Y<�>�׬!X�,�Y;LVM�K�P�!�c��p�#��H����τ��̤��Gq��E�YC�ȴGqܕ[���"EWo��Z�/�]�{:P�rVw���Q�E5J��Q�)?�!XC�e�kJ��Ss�������ӗ�7?���'�ޤ�lF��~*.6�'��/��t�=Y��U��l����h��,�g����>V� tby~2W�yb�dO/UL��h�i�М�sy!�L_W���Q5�O��~����4����N���nzk�����7��y8��(�(J����\e�q��5�:�E(���������!��'���v�xQ5�:�E�<[~Wѓ�K=T�!�cZ;/L;|B�嫆h1ڡa�r�����w5�sĨ�^6���D4;&�YC�ȴR]�r��EC��4=��{ʚ�n������=�h��q�� ��o+��a&1�N>y�F5��Vƕ<7HIJs�:Hߜ��T�[i����"���|�!�aZ�[:z'�%��q�!�eZkCA](��i̬�9��V:y)m��%�Ѹ�|�ܸ��7Li*���%Ƙ �� ����M�:�Ĕ���6{I���%gFr}b��U���g6R@qc&�5Dэ"���^���8�99Xm���?VI�r]�!�eR~���e��t��b���j�^T�����7%�pO9�a���y��
����;�,0̵'�DA2x�>�#�l���)��hC��5K3&|2=~��X����6#~2�{cRS�I"�/I;5�۟9�.�̼p�
�7Q��S��L�ha�FYJ�ٖU������*���#�1#z�Q�:�+�M4��)������ u�QG�
nS����E��+V�|���d�)[�;�����]S�u��`6QNŏWv���&���GL�+5V�TAoQ�Ʈ7�t�3�I��H�)�6���6��f'�TAm���K1�3t>E�*��O6;#��VE�����r;��n��oO��G��h��E���̷9�h��W�`T[9�G��6�g�,Ve�n���`���'��4�S1��IW�8���1�{-8��rt��!Z`�g�$}T,Oq�j���&��y�Z5K�&��e��g]5@��i��6�ҹ*1YC0�0]ab�,�p6�go��6+���<����l��h�M�;m�tT�v�oި��VZ�S|4-�����2E��?Ǚq�jo�6t��-���H��B��㡆h�i��D[�<Tj��h�i��i�])��-0�6��|�,��-2�4���ԣO��`�a���h� �:���n�*�G*M5�(�],k5DSL�M4�+���fn^%w�Y���M��Z�+�NC àZ<�$�����6P��Ŵ�j��N�;��j����X9-�F����M��ֶB�T�u�Ş��u�i�2�6[�V)V��j�"E&ɪ��j�㝆H����El���0Tck[ř�ާZV���k��ٞI�g4;z��If�����g\��v"&=�|�f_�P���O=�\r���H�IO=��7���3��g\��;�(f�分���X5ĉ�i��K�B�Mu�;qR���%lS�o�j�C5���Q�a�"���1R�֐�6i$*U��{[���Z՗]�NC$���՟.C��|�!�aHq���Sܥx���6Ju�_�u�8�(BH n�ATA<C��_J����iBa��k@0�6'ܤ,���vW�m��.�t��*���Mn��q�z
�� �*j-�4�<�Xڤf�n��f��PQ�=�(w)/��<"�u�����ݵ�����cn�����g�dD��	�GZ5ı�#̵�]�!�c�1"x	i�͙�6s�jh���q�32�b�4D��pV�.�s=�i"���h�%5���Dՠ��w�Pլ瀰�(n^UQSd#�q��ܔ�*��g�zT3�+�;QS�|��;��(�Q,��k+�NC���힯��YT��3�8�lך�W�i^�Q�8�RI��z�I�"tI��rUC�Ƞ�N2�C���@���{�D =~�U ����⫝QWkשW���}�i����p�h-x2{�dt��{����NC$ä��jFUC$�HO2
��p�"9&=�(�0�o��(U�:n`g���m���>��3	�zֿ�sf�j��a)]zvv�jIi!HOO�nv��յd�b'|Oi^!U�Þ9��/ǭ;�(Z<�?:7g��]��غ��Gw���i�d�I�p~�﹛���z��?'�HݎǩE��3��8Bg����'�3O=#�Y\ws:�P_R��D4[��F\�g�7�k9�g�`�w}�hԞ�=<͖���7=�u(uOE�vy����!ō3#��o^?�������������R����Oߔ��o>�~�5w!�ǿ�߼�����1=NJXC1����X��3�s�)I�mM`���;�v�*/�9�XC��!yd�,��Ks&��I2ScSǭ����.�b�v����gH�M��
����8�!Ld��
�j҃����n�a$W����fm�Xj���,xY�J��p)(�4D�H"��4�[l��+k���"J0P���!�iY��XT�#��N��e�r�B�9�XC
]7GU,�����7�x��݇wr����)�T�<?�,z.��\t����-�2u-C���H�[�l
x���ȼR�d�g<b��]ݴZ���ԝ���LM�Ӏ�ox��ԝgծ�$~@=��YC@�@��8�s�1C��e��gR�nJJ��x_5D�@"<V����	:��(Ҡh�:k��E���cQ<��z`����@v7!�ڝ���(�y���D~�v�K���66k����t��J�ͯtCf�VC�����C[ʱ>�uXw�YC0Űz	ʦ��Ÿ�-�ݸVQ7ݗ-^�L�ͺ������k���&
RE�o�"wc�B�l����٩T��?g��2T�d5�j962t��th�gM�j���?SG�|�t�U�!�g��*u��V5
$�}H����]��*M��FV6_���ַ�tb���f��u���w�M�R����TYL�}�bac��:3�Ѳ�0�aċ:3߱�0�1%�S��.�[�Sf�j�EABz�d59Uu�۪K5�^z�d5���?k�F���嶮NC��M���,��`-�SF�yP���£��1����Zj��?J�3�s��R���&�>�@Z��Ż،�Z�ck�D!�?z=S�J��T�4t{���8y�M�:L���kw�NC���-إK���NC,
	����j#~A�k�� ӇG/h�Tbac�W\�X�?�Y�ܸj�E����)��T�*�z��h�fw�R�ڟ%R~�vM�A{sM���L��ϗdǆC��c�J��\ɔ�,�v�u�u��U��=�to�l�;��4D��vk�4I�-��ݫ�h�i5+��9�ly�iV�"��CZOX��h�Sg=u��J
}���QEe8�oԝNLS����TY�d���d_�|��84D�Lӏi�huh��G�������d:��CC8�p�ŕ�EB^�Ї�x�y�����Uˇ�x�y��賫����nT���D^��QG�Sj�:4ċ��ޟޒ��);l�%�)�9��S�T����4��ܰth�G��\E���3���CC0�`�dM4�U�ӛ����7=u������x�y�QS��?f����!E���Lơ����3F4�'�i�a:4�	#��W�z��>PF������j�:c=4�I��mO\�]_���>A��<3���}��U�燆H+���R�t�/����'!�ʻCC͏���Y��W�f*���-�d�n��B�i�qː�\L�_Y����Ct���^��?��!��~�q�ZC|���X`o��[*��] �2Ҙ�:4D�� v�G�@=j~8;O�� �A���:Č�]� ��-�D0ք�o�Γg{sc��*�x��I)�m� Ӏco��T\y1����\}��y�CC,�,����rz�Z��<�j��� .
  @<��1Q��_�[����1��*�5�♟gUCό�즗<`,a?4�̨�,R��%������x�e�Z��B�j�A~w�ȹ���� G��x��y�-�� Ʊ��1Ge����Z�XoF��,��WUF�sk+��Z���VZw'�G�V[�)��X��%�q8�}h�e���E�HGMc�Y�H�H�L2�g^��y�^5���\�V���U�B#�ϴT�S6��d��E����b�� ,wT�L�s����;�����o_�������S'�w7��F����gT�[5DS���A�q�4@�׮����G-��s�1��.��XthcF�<v�~�!�c�*}��6������X�Y��34�e'\;�R��q�R5Ģ 9C�fa"c����CC��0rg�G7?�D��;�r�č�S��8�9�#���w,�((�G~��$u��0F�i��;4��#��4/Ȫ�0�1�:����T�?KPC�M�٩Z��mYG�$�!T�i�����v�D���@�[R̬��o�R�b�~PLk C5�����2?��Y�CC���덦kׇ�Y~9�bvT��#?�ザh0���1�G~`��CC�0r?�cn�5�q�)~`����k��Cu&t~g}\(f��܇�P�j��/dԏc*k$�%S�: ��Q��ә���m(� �*ӵ�z�z5�栩5m�K��ܷ���784�� ��&�c��CC�qp��al�ȃ�����1F	��e�n�#.բfs����V���!E�>�'y���ء!Rd�8���>�8�"�F��U�U=�zU$*IL�qZ�X�|�j����(�Q,;�+��UA�3�V�֬��Z�CC à�d��J��z��2=K��v��p� �b��<�F]�Ǆ%k����/"x]��uQ�8P1��G&�i̎��@�A�V�:h��@�����uдn� Que����g�z��!�bg�^�:kZr�%'UU��>�.�� �jhjuhdT{�m�̼ڜ�[��<C�*���e4Me��w��Rm�q�����J�j�噥�iA�7���yFV5Ģ q&%�$�q�q<�E��Ɖ.b\��%�M���%S��!I~:;/�@T%�Zj2�K��n�5�]��X�Y�%��r��7�YC,
�#�0����&L�R`ڏ��!�e�tuC&�IX��5�xu�����0�1�0D���s,UC�� ݁�$`Z;Ub����sid4�Ƭ!PbP>f�p5:���M�=&UP��H���G�FV������Sz��wh����3���~����!�L"E�3	���5V'�� �!�x��-ѩ��i��@��Z�4}I��"��l�G:|==��{E�Y���B�4a�bQ�0����!Hb�w0�(nn����
z�f�_b�����T(�bif=s;�:4Ģ�p&��ʿ�Yޜ3n^�W�,��F�+�F�IC��5P�{�Z��x`�E&y&��Vcd�� ��!V`մ�X�-���Af�X��BQ�8S_�z���1ݾ0�_�7RbT͊n��3�����v
}�>��C��)�/خ�dL��O��P�Qy%��F�!���i�� /en�R4~4�&R^�z��S6l^��=��77�z
R~?sa~��H�i��{q,s��x��{4�H��@��Z��~S˹�lՍW��3)�T����$�B�y���8�������{�$Ĩ-,����.�����;Ŏ3�ǲ-��--������Ĩ�y!��j��Q�`sA����m�D_�2����!�bTi������H���쫆x�yy\����)m�xV�=Ō�!Ō3=����84����6���`���lvx��MAs�]���|��w���3HE�>���P�'0G�Aer]OǱWY`qjo�^Q�8+��M/�vnj���$��q�ϓmN#U� G���ض}yہ���0[5�Q�Q�̺+P��8���h�Ф�z�nv��ԼgS5D�aA�yKC���9Ӷn��2G��tәZ��5��]ڳ�i�W5�����LQ�^69�~��0�1��ǩ�p
���C(�=[�4\�iʾ)��������ʲj���W�S���k�Õʋ�<>%����UCp��37�tS���73@c�S�J[\鞘�O����8\b@mX�A�U5�R4����w0n��q��h�P��]�$V�Nkaw���E�zT�����$ƈ#`�g�U��̭��
I�<ϫ)�[��պ+����=;�j��rբ��˸�L�%`��Xά\\>�XlN{���Z.o-s�A�}�Zq�K�������N1:�g�<�T�Ld���aǐ5�	���K�C���buH��H8����v��L��8�X_��-���2��Ik��X�3��g�/�������iK��-�b�L�p�9��fNv�����!	��r��.G��C��Y��T��'W�)!��1HZ9_�+&��7��r������ �]`ߛ#c�q��bEf�,5��7���m���$�����n}24�����:�NL��&����/@��m��	�v^�"'����z�/�v�y�/�����p��Q      �      x�Խ뒪ʷ'�y�DwD��"vՄ�^��EE���**^A�}�9���IN&���U:���}"V�U5��#oc�r����G�G^ձ�jca��d��h�JCD`������N�p@����W���y�������-���qV�/o4���4����{����������"�,ZI��N���h��!e�Zb�E���#��͏��Q0�ӿ���FPo ù�H$�!��.� ���pco^��o[�$��$�D�_������)�vF�ZS����ki{q�`{����#F�x����q?����B��h9 �!��j�1q����3�4{ٲX�7��`�j�B����^��o���}�Hj�T��bP�,NEb��J/]Sc
ŵ7��Q#
�ɺ�U8nf�abk�:�� ��Ot	��d-�hf��;��S�(��Є:��5,�,w�U���Z��� G������h��� ��Q���by+$�̢k�^�D���4;�j�鶯�:��]\T�{g�kVs�ܒ��糗�_p��p��o�E�R�c)#g~�d� �KP"��DL��E(��#r�Q�%�×Zu�wY���e,/iUIꕖ���Ệ���������=���h����G_\ՃWU��#���$�>�|���
lnck��/����x��ޜ�8�w�%r���mџx��h�j� mm�۫l~R�Ň�����Ϣ� �� ~��?���
c.��6��p4������ѤZ�<�C+���$t-�L��H��SI��_��*u�r%ح;z]y��_.�+im���x���j.a[_(kkLm{�\�F��A0���/��3-~�>@���2竼���\����ܘ�m;i�q���/�]q��F��"��>A;��Y��Y�iՆL���$�TS�jӤ�ܨJ4�U8�[��\���R��*�e�
_�z�>ꅯ���#ʚT5�f`�.�٦�R4����\Uj�~�Iu>I5Im����u�Q�tY�;� ܓi"QJ���W��P��,��9m/�j�}�9vp�T4kV�2�����m��Nd��&���5�*v���~"N�%�I�D��FRY�`�s���� 7.8��ƚO�h�p��8�b=r`��&c�H�y���2����H�CT��.0���	*+p�ы���.�c30�a�c��S+{�_θ�TU�}�Ю��3���1�8���t-X:Rr ��:ɭ����s�	0ݱ�%\���Z�ƞÖǔHb��B����c��G���A��~����	
p��?Z�!U���G3^�׈���[oVr�V��6�Sv�����:m�g;��)r�튎?{L^$�F�Y���y��k�Ѧ��U�ЂNCe�����Ϫ�A�K��+�:o>�7���?�`�#ijC~Z��m����v �S4��f��n�A���ֽF$��/(	s3�h}���~�sp8r�ٷ�z}'j ��Yޭ���fLş}*{� �_-x<C���8X;��aPC��ę[$��O�u��e:��4�^@�oG���h����$W#���Y�7��c}���}3��
өN�t"�	���K��r�2�7Y/Y4(=6k����=�k�j��J���������@f���E�;%K��r�>nB܂+�&9�ˣʔ_��}�jJկyW����PV68mz:xP��C� ��#���!bMnh�!����F�kYI��hǆ�&��|'���H��9�ӳ|�}��H�&a�	���:h�X6ںp���V5���~96W�~_���x{,�:u�����Z���P+��x�fepxl�b��	|Y� NF9����X;�"�p�'�P�y�|��>VCp��XE�Xb5��&���\bS
��q8�;�� �GFdk�r���E��0��}#�,��VC��j (��x���Ii*��GSe�
�Cc7^u�� �$�ɹ�:�}�X7�O �~#o������z�v=Ti��iP��*\s����s����M�6�tEiꕏVL�U��Q4���Ez?-����Q���,Z����h�-�h%�)�qĠM�����zU�S a��摥��Y8ʨ�����U���"��BP��9|�Y��2��Dm��<\�a����a�Z��B��Z�.�-�e`��h�u]�U�-U0��/�D�����Q("R�}��g87Q�^��j��Qh����!��H]$O�f�����:?�`��أm�N��h!tZ��S��?��@��͢A��k��;piv�����y����^l���?��]��}�eyI�76*��7_��é1��C�Ue]G���8����<�l�2��`Y~L.d4���rM���6���(�Љ��"�y̳��؛wD��>�cEa/g<"�<p�댫�/��g���8@o\�{�H-�i^��B���'��x�A-[�-8=D
ƅ��WK�J38�J�R�(�^��u��Ko�ތ@K�0`��d��B>��4m�H�ɻ)�5�3o��`���t�o
�#���9��STA=���+���7���h9h;e��'�]@��q��b�ݭ�C���<�@�$Hͷ�$`q���\���
�`�P�5)�2%�:���|Tc+Rк���u�����)�Ic�)Zp��"����X��e�ڂ�t�$�p�Π}�R�$��~�{��f���+���9�#��OU5�J����?�0<�&bTօ]w���!��ƹ�f���V��8g�����P(��U��t�i,eu��
���0�:�{��`g�ZQ�c��B��x-x"�D���/V��k�3+R{3���g��F3���[ǅ�e՝�ꕡO��,�#�Ɍ�큄C����j�j]������QY���q,����Р�������-۷�W��ͳo>{!�+���Jl�9(���m�ܴ��"���ۋ���V�ڳ����o��G��酖#�l�`�Z:�xu�\��X#��_)V��]���sr9x���; C�<��V�jC���n@�tC���Qt袘W���o̅���F�X��2·��B�+DA�Q�*���z��so$�F�Y4h�S�X��]?�p���dX��i�?u�MP�h�<l8�&��gaw�u綢�ǵZͲ.�3>�FPY4h���_�f`�
y�۷���E���#�Tmʼ'�m�����;�bm��;D���֬�S`4T�E�{�'���eѠy�7��#������,I���ذ�>�ԚX�h*픥��fԓ曅Si;�VIZ�%0�eo���r��n�g,}�<h�i�ތw[�6����e�E���b�N�2��-�:|��=�G; T�C�b�xc���	jy�f��˖�7o+w�ۺ���<H�O�E-�w��f$�޵��7g�u��j�3Z�b��%-v��9T�-QSs`y�2��Q��T�>�ʵU�=��T_	����%��u>/���*<��%�\��,4��l��j{7�-=╠_��$؋���,�M��/��/4���/�PI]`m�wƱ]҆��48�Ks����Ӛ�'���D�B�����O�"���S���u����Ϯ��M��7�M�՛M}�ه���L�.>˙��H'w]��֪�j��H��	:ri	���`��+?�k��`�}W0��F��m�s�|���տ���C��ҳ��yp���l����^�M5@Ɔ=pfV��-aOÞ�bjD'Fi�eaC�j��L�:���d7Z�`��gk�G�P\����|W���FJ,��r��>��ޥ�|5�?�<\�iq"ԋh��^��k�ɛ'��;�?S��7�x˨,���Ux�����b�R�y� ��\&�G��=�%Ch$��h9���#�|s4ۅ���F�>z~��q�����2"|"r���&��~�A2���Ҳ�hQ#I�ǔ^*�VSuV:��Φ�[+R�h�����x��,Z���{�:+Z�A���z���!J����p�A3y
�_��c8Y��G�w�$}�)�ؿ�H����b    7pl�V��W0o�pzB_m����0���Q�j�l���#�W�^[Y��'�q��6��
��G�^��(V�Ю������"���E��Nko�W*'K�v �,�K��c�.���� :��@�*�&zj�I];���*��!(<ְ���)m��C_�{���p����ٵȞ��E�w]�����N������<�Я� �ɤ��]Y��}+C�4�'X���IU]���I�.�W�0g��xA{���d\c���d���"�����${5��SHxTJ��:���.���U~$��\�Pu��+���Q���6��i�[\�c�g�J�CI�=}Y+u�����u�ɝ��H��^G������ǋI,��Z*@�YQYfQ� -���^j�9?�X
g�\ͦz9�U�>C���A���4���F�e�r������Edo?�~8C��'��� -�,-��(�T�1����U���r����.خl�[�'�?��״ ���w�{|պ_��G�l����5��-�d�ǵZ����n{��u�鬹���Pڬ;����S����@�mez���u��u���0���Az&Z�n���ߋ�*ׄ���YZ��̳'?��x��c�M�J
gqGV��P��&�e����+z
~�f�����x=�J�'�@�4΀n���@�����0�~�у�w�wM�
����bdj����E���$�
��x����"�Ή���F�4���Q��ɝ�pѸL����TWP��\̷��B��Y�/��Χ��I�eX�%%�B��a�*�(,�g�$#o]Z"�����h�S���_�?@���3�t�$R��4<��R�M��iџ����k��n�UilH�J�7�IJa�pw;���M��p�E�,4�#���)��;{d��W�� .����`E�\����+����p��'[I>�Ή=<����i�6�.,���%��b�NQ���P�{,�i�\�Y�9��e��L�9��&3�v�(H�];�������8�8�M(�]CW���3�	m�,�,u>��U��EW�.pI�?��Rx�O��*�����r	ÛR�Bl{ǅ����X��y��m�xǛ).�[wf���#NXJjT�h9�r�)&��{]"��x������|T��N�7��P�1ϖ���&@�SpWD�駇�l���pgZ5zլ�Y����+��(T�Y���Z��],hޣ�'���S��������� �q����n)��h��ƿ,��$;�=ؔ�_ǘC��w�ڂ�=~�=]c;���P�?�����A��H��vy�E�೴�3�W�H���ӝl$��O#��eQj�آ�~-=�O����􇈮rc�K��.F�_��5��6��ZY-E�����W����n'(]Q��f~}� �B��-G�Y>�W�S�$��\*�����':�n�A\�L�t_*ぐ$G�E��/U��p�ў/�c1��K�Mn4T����]��[���Cz��Ic�L�\��alh�cF���ѵ_t�"߳�����au�]d���������P�p	۩a�j���Sbf͞�S
>��]n|��v���ș4�c0Л5ʟL��Pu��^h9@0�@9�](�{�J˓�	�å���Y��?�l��N�D�Eok\k��C{�J�~iN<�@�o��������g�� ���3AAK|�gR\i
��M�Y�mz��-�j�)&Z�).Ho�o��q���F�:�<������A� �.c��Ĕ�|t�@D�E}r�%�N��MF��\��s�޺�4r�ȺfSv�N�C���ڐ�)-��
���!�ﱬ/���F����{�m=��Ľ��Ef�r�J�w�qI��  g��7�kS#���s˽vfz��9�(����'�����)܈4;��;o���Z�aٟ|U�|@m�����ŧp#��U���tֱ�0��S-|�E{X������/�}>�{/�C�$ql�P5��W00��z�+u��5j��u�V80&��!�c�V���̃N_ENѠ���J�|.n$�%�ݥw���)~���X�t���� ����\�&�˭�NA��g�u��V�A�������]��;�� O��Z�s���~� �7�̢Ak��4l�Vt����(!_�������Y���b:�MB��cAU�*��Ӿ"�LU�q<�&Zv�3�`�}NX��u-r���+��9^�d��׷{Ԑ��p=[�g|Nȫx�%��(���~"����zW_�F˷[�;n��~VY:������>ƛCM�3�����f�[�"�5kޮo���Q.8T��1�3f>AB=*��\V �4}fh:c�@������ӷ�ى '��!�ל�L8�*����x9X.��A0m[��3Y/���\[7&��0i>�M�$�*Ӆ���,8CY�;��F��n9n�|�����޼xi��	,������;v��W����?�b�~z`�c;@����ޠR�=���p����_�o��袳��2tc���!g@~n%�la��sQ�@{�@�ط�f��Z8uj���Y��$�WY�]�2�(^�e�)s27*m��TC;,��i-�n��Ɏ�j��<c�5�P8��,Z6.{��k�o�����%Ii(Ѷ#@Zz��$C��-�ALD���Aȟ"ONV-to`~�p�U��y(���ta(1��p�@���D-��/-�~iﶬҒ��p�+5�� ��$W2h:���K��$QW-��be�"�Y�9+�X����G�B�g�^�Ǫ�%%\E>�θ���=� x�/=[j
7�̠3(:G��o�kV,�}(��\�E��l�Mcs|lb�<B���R� �Gp#�.n�>t�K�,>��pĚ̬�R��/��B�6X9��T&��R�yhT��Տa��pDf G��^��bf|��M��h��NCqg/����T%H"13��ػ���I��I���t��q��X���^�*�w�ye\	f�Q�ܡ�O�)<6�8���Ϣ� ��+��W�+hݽ���M O�o�z��1�|�/z����Z���i�$Iu����b���n��7(|�y,�U��1g�蚖dV4��vaS�N��#�|O����Of�āS���	��E4�Q��Q|���m����"��~����PY
�^k\���F���f�2�� w��q�� Ǉp/�̺����]��(���J�p5~���8�7�=ϲ��i*de�������d\�
�+��:�J���u-�Z�]�?<�icet�q:E�:�7��p���_����d�ޛ\�I���J�7cPNQ۵#�����J��18M%'��9���+4wml�Zͱ�{-u��4+|�37G�9�c�^���zZ�Pؔ�����9i���1�Q��  �h9����L��
� ǝ�*~�q���J��B��k�K'�Zԗ��c�0=�>�"(4Y���tn-e�R}��%��Ӛ��֮6�-�,\Ro�W����A�,�aQm��7�Jv� L�k��8h��+���AP�[�\Thk�N�����L��\_h9��u?uE��B�0U�\)�	Z���E����(v���("�pW�#��[
�~j� �7n��q��{��t�/<݆�U���u}��M9,1�a��Z��e�.)�����.�Ԩ��q3�x�Q�N&v^V�m����F��S���s8�9�J,��5_E���:���?ы�0(����.Zw`�8�j|�p����?�E�~�.Qd��\\�Q���%#����#���|KG�lu)>��7����5"z��2Z��>��J9�Q	��u�0�o7�
D����V���cM�����B$��!T�k�*k���(��trh�8_V��\���]��J�GO��>:Ի�]��-紣�uQ�w��Q���p؝O���{p��Pw�O�i�>�r(=�v]K�\����',H<�r���!�\��MW�Ƚ�Z�+m���    �f�0zde;_T�A�գ-8�B�FQ��-�h��M08Y�"i�d�I!��6�Ti�����2T��5�ۻ3� 趸�M��5ߖJ��}!鳎vM�v2{s�s���Uk���:�c���/���p�wVA��ࢫ}p��7��CDhUfi?���_YX�G���*��͸�1�Nm��Li�(m��̚�8m����n{�FIY��$�-�5O�[�o��z�����`>�ɘ7�ˢA8�e�*���4���j���t�(T�B���ĉ�_�˳��؝��՗":�ɰ�������[jѭ�����Oڡ���F"�~�3�FG���]Ξ8��J@^��"a��c�v�0	���� ������b-�/]eܡM5�9>�=���px��K��Dw��a���"�m�:��3�$iԈ,4���C]z���W��Ex:�i��W�ܣ�|0O��Hɿ�rFV����S��w�J�J��f��VW<`|�pl�Tdh�ה�O��+�����֬����Vp��6����-G���&>�H2�M�QWs���F��u��7l�ͱ��#�h�v%���YG��3?�_�2n@��P̂h��DR!�W����:in�>'nkcy���G��U�����ڌ��a�m�KN���:R+�7ϢAc=	��Q툛НEg��,Zi#�����q�$I3��S��T�Yqx�ݘ���I���z�tƳJٛ�=wWEB����?�؎�ç�ނt4���q z٨Hu,/��y���K�6KJ�|vg�;Z�����s���K�)�,?x��p�iEL�&P�{M��$�������I����60]R����++�0������Q��Z��f�QX*ǽE��׊W��#�/1�X�p� \�-βE�B:=T�e�#��LU�!Ψ�d���t�*o�9@oA�(h�>�F�>��+,[л�ڭ�ƮP��f�� g&��^h(���+���\1�i�Y�'Ff `,��}�
g{��ث�])�o�mA�s��WI_�Mv������>,g��r�1�ޑ������X`�	��h'�S�%�Y�bi/օ���zt�/g�� �ݹ0"�6���]x0,ld�Ý�:qL84ͳ�[]<@��wH�89�C�>�E�fY�K���DY��B�YXT	�B�/�~a�^n�U/9�&�y���JG�=`d]���Y4h��f���et3;@��� �;��\ֈ+M��9�0�vE:�k�ҕΠ��'�wQfA�fTN{l�Ωݮi9|�����POAg�����-��!%8��3����Z�Qh��������ۛ��I2��������8�j�SƲrd�����ʚt�� ����Po�c��we>*�\���wq�z֙;k���ӤeO�
8������)�f��p�%��h0&��"���h9@E�j�b��A����B]%���0��:�֤�l��#�]�fV���l��HW9��m�Y�}��qt[I�t���o"We�kz�"����!�aWI��eV��:�gLP�wJ~3�mElYM/��3�;-|>j�8fZ����z^-G��(:є�;��A��A�N�R������_/����X�_�a[�Է���L7/x��k9���u�zuF�w�d��Q�/��~ш�x�RU�L�*^���e/�/skz��v_�I��t�\���#������߿ ���w"5�*{����{IF�=�7� �TuԖ����z���2r^fPK} -���?R݅�_����`4*�0I�����G�:�,�M��t-Gg��_��z񑆬���P7_-����w}���2��/���`�7����F��,`�/A\�͈��"?�T�*�jS?)�7�ѷ���1� '��3rM�Q���J��`��~�ܟ�������R^���w״�J�����w�?�N=_��zte�!,�E��x���N���0��X�GO��/�2�uJ%��n��ICb��Yi��.W��Hj�0ٸ}sނ�	_NA�}�9V��1�=&~�aVZR����ǖh0�<t����;pq�_�r�����.~�k��u ��F�����6�'���Ւ"�ۅ�~k�Y�-��9`1	�!=���.�mٴ�!�����|��)�<�6E��e+�����l��jΆ5~d�S�H}����L%�7�ɢ�(&���^e8v��7�O���|e�F!X^�q�/ST쟙�_��G:�R�>>�?*��u��j{d(�{�l-G���Z���H�-]y�:s�E��]�BX�M/�l����_���%�Ip����n�I�͐$I�{?)kp�F-�p����Rw7���/ST�7I�nяpO�L슖;��<%ֺq�'��N�`t��J�t�*���	�{��%(<k:�ˍNZ���|���IqP�;�B:=���� u�����յ��wGƞ��5*d�A5a�}w�l�RW_<�����q״�<�8�W�1����INDD)*nn�'�ÿV��<< T�9��u`?Aʕ'��4�m����߬�>�vsxĐj_�ז� �˽ .��s��k�|V���Y:%�|�Z�K�x�я#(�qO��5���2���u�f{�o%\��ncN��D�3�oCի��n�\��}��,ZБqsɢ�:A�����nFK�5Im����u�Q�tY��ZCC9�N0 ���K�E��P�ΰ���	�{	�x�|��Β�M�$����&�G�4���)�$�ԉ6W��Pm�E����_��{��6�G�њ�����w��,Z0YC�7
�jƢ��U{	7�(%�4GgoK�j��I����[U~�c	�8�]��|�-�O�
6X��ۣ���e��{Yq���u�a&*~�&��F��b=$��5+ģ�u��y��(�� {?�h��隒*�8���E�BO� /ї���,	�#׷7RE/�p0��[��Y��\��-��j��B�|0.h�oD^����r�V�N�7@�,ԇ�tœ������~l��b{�O�k��!߻_8����W�2]w��k{
�cF�I	����Dk�$/�N�"�_4K���(/�ϒ��9�|S}|�>d�o�E���:Os6�9����s�3vF^L�9��_�5;ޥ&���2�j�C�\�3��w��mh����M_����P�O�뫫�?+��_|�Y����}ӺE�vcV+=��3��7�~%��������@�隖K��&k\	��Ki��������	*�e~.�����ܸ�9{$>��7�3/�����{�*h�eU�x4ף�o��}e~�����dV�����������HNS�v�72�>� �mj�I��Tl��g��Ce��f�$~�G�x�7��/���	+�P�_�;�Q�oO�?Қ�����$��h*s:�[���tr���W�')�j�o�_���G�{�}�O������D|\�W������m�C��iE���7>0	-G��iU	������5��&44 _��ėS�ȗ����/3gw����9�}eZ�c��ʟ�Q��em��[�Wu��J��[�syh���HB״}�|�R}F���S�~��8����t�_�$4�ae�L�G����J����x�7(_����B_k�#�� �h<�����ok�s�M��Q0>Q��	<����}4�����*y�F�3�������`��f�_Y�i �$������_��?��C�����ݹ��h�����-���%���u�ܧ ������C��-{���s�kZ��>�A�-���+����# ����}%7t_���� ��}x�}|Z�O��y��-ܜ�r�%���~~Y%"�ǙE^�/��.:H�X�l ���=��?֦x��^��������������i9�Y����#*^�($�$}�b�iq�܏��HP����r����a-&���"�?;N������=�u��5��c�.���F� ���.:������[z���A�>�����‬�8^���B��9
�~�Ӎ��єE��b���;X=	�}v���5��#�    8t (�t#9j�w)ܻ_�K��_#C�r~��}���u�1;3e���z�`Q;ިJ��J��'I��Mf�rL�f�/���z�׳,h-eU�ʰ�j��-�0%�9;�F~�C�'`�J�:�|������ ЀLB��Vf5M�ơ�(�Q� Mm�x�ƕ���b��|�(�e J;���	�rT����3]:��$����2�X��AIn��e��ܽ�e(�N�eU��,���Y_P3V���V�pP\Q`֚P��I9_j��Dŝ�× 
n�FRY����SL��(�`�>sv��y~��J��D�9;�,�>���ׁ5�e!9�&������j�;*��.1f��ky�T�7�dZW��P������lj�"��`���;ѷ'4�F�n����pF6V,��-᪅2^�T�j%��n��j�)�E��Wst<�ns���P8���9��O�6��${�g��5�3�SP*K� ��V�pK,] W���&C<2'� �(5�%��5-��k���zx����}׹��A�t(~���q�g}����5��M���NwQ���)^�׋�R�{�B+)ڱ:��fY�>�T:���oF3Y��� 
U,g��R��]V�M ��ӗ0�w!N�g=%�JZEH_�8����,E��iEY��l��P���I�������;�KP����@�e�w��k���(J�tFM�nW�z��2���j������؏�w��|���`���P1�(��jC�3����FE���*kXQL�I�p��R�4�2w��/�~��σ�~o��M�Q�
���ۮX>����8#WZ��j��;I������:����ج	���I���N���{�
��G2:�؟q��O��ZY�\��f�jK�S{�;�z����	ǎ'C�P����T\#RP�"�h�8y��`��q�!I�U�^���8�
���t�U7ZQ���9q:�m��?�<G�4����t�h��r�r�ܤ�gy�%�E�(/������0��w��5��9�G�Co�ԅO*}�-E�e�'�r���(h!JQ�V��>�گ���$t'\~Q�gH��#�О=Ov�07�@e�͒�U��ɼ�@\���W���U���֛��9զ�(�!T�H8*#|
��>��F�6�NԆ{L1N��$��Y�|��o�YF0	�h�U��?�S۬��[�̧ۂ�/���䧵�������Æ>j�œ�6|���ĳh����e5��˫
*�?҅UA/X���MM],�F�Ǝ��=�����'H��XΏ� -i�K�h��K�Ag��yc0���u�J4ʳueb<%i8g�I�rl��W~��𩼀E��؊'��I��F�g�|�w�|gk�NV���~��1��x��~��X[^�m�'6k��)U�+v��}�e9A���;��}c.w��*0Y�K��2�3_ĳ���s�K��h+DＤ&�/$�}���	<��V�S�y̷���X��yG�'�8&�r~V^���SZ�^��M{pLq�$B���c#��%�𜉁��%X�'R��J��}��(�4jI���&D�C.�6˘Sz�S�M�b�M[�>���t?��n�H��f�-vѴO;)򹠲h96�B:e�)�9�h�B"�4�|�ъi)5&N.��GN�aX��Q.a��V��eX��	��Di�K1����B��>���Y�ǪN@[��2pC/m:�6w^��Jfq±o��(���ؗB�߰�&)F���4�Z��K�ʉ�g��4��Dv�`?h�Bʙ�~���{U)o�'�(�Z��u��XZ�/O��ӽ�:�t-���8�ʩ?�$�4��$)E@��y6\��=u�����0q���R{�]�O!��t�p�9��ۙ���F{OJ��o�2_��҈:��bܪh�jwHm�ˢ��W.]"\\�\�:T�dA��c+
�PK�F���3W]��u�-���#h�]$vAqd�$H�U������`��@.��Qk���y{?���s�̓���+��#�S��6]�r�L�Q8Z��8��P�%E��E�	h�bM�˺���%���k�廳�?/ ����)E�ƛh�Z�P���\G� X�c�/���4E���e���4�ͼM����ԼV�}oU�.T �UG�[�I�y�t�+Z���V՚j��e�@;�((4��)��.^lh5$W8�v^�u,�P���i-de�+�&���u���`���Ț�XA6z��mu�S0<ER��ki���l?��6'��қ�9����\�pm���ۻ�Q�1�fw֫u��\{(�ͥ��9��5-�e圃�l����foB�����G2$M0��M/*RZ��Y���R�v�Sƥ؉�'7*J���~n��BO'Z��ʮ��5��`8�#�?K��?'M��X�H���Yz��Y��ޣ�?���e˘��8�����h�����t����5�^�.�=�L�9�o8�E�q�ᡆ4�ew�[p���M�Ǌ��Yr/�:ō��"P�s�Q��,b����:��o>H��q4NF�`�X� !ǚ+%��0h�ag�,���@�h��T��j*n�o5k��G\Hf�Ň㊖�Lr� $p\�����<nZK�O��Twg�x�X+�\]�a�Î�ݚP�š,�����H=st�p�ۣ����`���$:��[s��g����8�� w{hy�2���0����FA���\���6����Q.J^^�ugN�Y�3��!%�YyN������&����Wq� �W��ӥ�����}k�z�NO�����i�_���n�C�1x�Wh;Q��F�����^fO�4ų8��s��']M��r�<1��7��:�9�;_-���Q���Q�d��fIO�O�K���9>+�܉O�f|v�S��o��ZK��ק�<e����XlxK0�*;�l��96����va������u낓�r|�br�(�w�#��}'S��[y�'�I1L�xg3k�Q�p-��Q�^X�RM�WӻtA6kI[��g�3�#�3S2'��i6�O��f	�Gy�����g�g�6S�e�K��{�eyHݹ���҂w��lW8�;Zg�{���h9>3T�?��	{`�@ǒ�~��F*"��|a�5�=��x�^n��|�/�
=�`���:�6��ޮ���d/�<�h9>��NQ�۸���L�~"k���cǙ�Î��׉�/G�<g�'ִ1���q�]ݕ�VK[%��kv�n{=k��'ئ����-�g:_�g�����P� �9����\ψ�	��֌��i��-�얼nqu$�:Y���2���s����|���\+2XwX���1<�Y�1�E��$�٦L��I�H��w{��:�A�*Dg��3��KF�+Z�d���#��-��A�/���$�q��{�O�
Ͱ��&n�Pq��\��e�[O���iV�XTh�����327xԉ9�ҩ����{?�À�hp��3��y+4�u��^G{�<�V���L5JN���b@�JK^S��8�;OD�f��Nk�ޟ��@u��px59am���CA�*��Z�EVɅzX�Լ!�F� �c�P|��w�RD��eƟNkJ�ݟ�$����x�z`����;���5s)�)�l�
����77��n�G]�m��ޡ8�s:�U*	�3|��s�+�;K�R���f���Q�s8^iN�� ��d+1�������}�庳��M�"ŭ8�."�l
R}�y&�|�Z��km��tb�Ms�ޟ�(�e�&����1�N`�t������ꎹ��<���6j�bu�IYy���}v��&4�T�vtj�f���U�$Ű�O\�'����!���a$��sQ���P]�����Dr��
_�~�ȢA�2ՠ�a|����'��+��{g����������	��h�xO,IZ�Ny�����3<��]B�<E:�%z⣛R������I��xŎ��b8�#����������f����������I���%��4Q�hzJҼ٢�U��+�0��P����汣���l�5t�;j=�5��^n�\�r�T*�k��    WX*9784���F��ن�&} ��ej�qJыxSH��x���.�W��fWi��d�Ƴ�Q��7�Svg�WLI?! �΢AAdZjE=�V��S�����x?X�b� F�F_$C��^�NP.4N�_p�xE���2a6K�>�������[��N�	��x�iu�ݒUZK}� ���eD�,�+���"�U�O�R��q��d$?��:>���įN7�V.�a�&�r�f�B5��2��C.r�\�� ̊�+��;��ia8n滸4l���F��헔���Fw���Dq�"��������
XI9;%�R�tw�A5yt�����J�M�2��C�p<Fs�V|�R���LwW
1�T�ZO\o�JaOFa�,���V�b�z9���s���z��*òHш�VA��|�ZO(����bȰ���"^�:c�p�ѡ|{��oyb�=��$<B�d� ��Za�-�}Od�E`�٠����m3��Zi3`���
��t7��3���@-���c�T�{	�zE�f+�)n�V[�t_YD���4�is�`L�X7k��:�a�����ު�����̘b�R�*7V���33���2��,��!�3x_ ���rI��x3���	��zUE�4�Ҳ6h��B+�%Y^��I�	��y���-q��t��r��:���9��d#X@�<sՅ)Ԅ��U�Ǎ-h�L.@�ɦ3)Wۋ�Vr,��)T'sj��[�0ȿ���(�A3����e�@�(po0�T�X>ʥ.����ק���{�� �f�-P�hN��j���Tw���f��-�ZF����L���C�
'�^�~{����R���b���Ee�r��~Gp���{�~��a�|J捺�P0�%ʆtA�Rb���{�V&;}�$�va�3���x���������F��FrY4(�L��dG��؉��}݋�s��bE� aH�'�G�gZ�9�q�	��m�,��z��6˝ʺ��{zQ�hi�{P�KL��#Z��|����<�8� ��Q�hjƗa,����jB8r6��gp::�:%�����jyn-��a�[�ݒ��֔V���E��E�ѳ�z[��(��pA:�]����+��
9֥F�����G���N�bG"��^G�RR&�f�����!:K�	�CSŧ����"�A����!����ۥ:�A	g�m�bt;ˣ$ѡ�)����~v_k���OwГ3��s��bE{����j{J^y� e���ח:�����M�W���b�*��<i�%#?w�}�1���u���U�"G�ʮ��$`��j�C�x}���2I�<����mB���Gͪ�b�(tgK� #z�&�ZW�������1߁X84����9
,��Z3��1�OYSQ87B��@K#�x�ۚ��>pD��J����Wb���]'��Kk�_�fD-�kw����5�W�U��t|8�%P���1��nj�߯B˰�~��-����# ����>��)��i���������N0�ތ��K7��C��)"�W,@隸c�Щ
��Fm�LUa��;]/��������^=>e)��]Hh��,��J:v�����.� n�+�\�H�˙Ey�2G՝QҞ�/�2Ui�Uw;�>>�*Ʋ�ϟ��h��,���V���b�՘���7�t'���^0Z����Q��Wd��#�;�a�T텿��pST�	Xl@�M?���mH�r�e<\xI����B�`'�&����\�%q�2"��*5����l�k|��jb�Z�k�,:��P�4�g�a���*E��f��)���z�&�|���d^�WS��������0;*;Zl��:v&۞��V=VZ���k����4�Z�b�bD)�p������h@�����c�\H��,�~��v��`Ƃ���	W!=\����g&��[t��RJ�=Ԫ�D�$�o{�D� �d8*>*�� ���%6�&���4[�M>�Mm�����g�}���G�����bI([4W}��(�}#�,�6KA�p��T�OD� �$*���b�2y[���Щ�4n#���K�ǁZ簮��]�ѻ��%lpp�e� kY�^��TR��BH��GBc5��W�j�5�d�Ĥ�x�m�uf�.�)��m1,s�T*��b�9�n���Y�R�ҋ�;����>p���D-%�z��S2f�U����-ZG�RJN=�1�6����>>��}/| ���V�Lh�Ï�������m���AP�2C�h��^O�ɚ��4�8�S�դ�ނ�l���[���qeT'j��=���BB�l~�݈j�v�����wn���k��Y��\u�$��6e�&T�^��*;�=���Z��kQĸ,d�cEF�[�5k��k5�� "ި���o�Ip�C~sl(�|�W��VU[���vk픉:��fK������)Z��>Ub<���w�3&��@�g�I�0��N)�\
�h>X�p,7����ud�����r��]��pG�?�P$�̢��t�и�6�j�ݪ1g�[>�qIBe&�?����ڋk��w�FИ��jg����"_)۵]m�l�#���c���h"d���	���]�B��*�m�$�'|;�!0}���tIJ{jO�]+����a{�֤%w�Z���-��� ��6�U�7������&k+�n�+��"�Ci5��p�X:sn2����r���;�7gL�H(�g�(��į��AV?�kb�t{{7�������Қ��#v{�b�2�I��m[Um8;�:=�wem�Ja\��v�P;�1�3k�� ��:	^a����=��G����|?�-mJz~K-Eq��څ�dT��<7VH�b'�L3�p�I��3�^9�h�ۏ՟�'g��̢�JD�:)s�bLm�/��|������?T��іt�r�+����]�Ϙː-��8f"Ē�X�9j�[�Y�F�����M���8S�Ś�U�%�� �,�m�m�q�a�?��L�ioX)O{�geߛ]2��&�ۛ��L�i��8Kū/ # kg�L-�3���WZ-�Q;lG!G�^���B6W�6�K|ڪ<cXBn�+l�4|�A=X�W�!�>�C����"���b�7��(,�D�py��j6��kKF����r]������0�e�
Qx-��D+J"���[f8H�VR����	RZ���=79��YY"�n]%}Fm&����e5\��PhN���%2�2�	�u���dL�)�$�t$�}����Y�:�5���hԢ74;�Z��@��bq�iǮ��a��H��3C|���G���*t�)r����}Y�d�q+E�ORsQ��miη�^��x�^i\�i0n���6�s�u���fħo6�h��O@U��o���۵-+�4���^���a��D��""x�� PQP�t�?����O2U�
.t���s��J���:efe~	HO­Y,�Š�'ܜ�G�&�=����"%/��;?U]����
=GZ_@[t�*���kCp~�a컎�u�f_<���\V(�N��Ä����n ��ْe��u�xz�G"匝"�Έ�k�1���Lh��o�!���}��c�e	�&R��
�k�l2�4��QW��$�*�!��O��i_Sf�:�Q��q���~u�'4��7ꏕ@������$��=��9&)�}\X��L��<��Ɩ�d��Ƴ��Z}!I"���W/�&�Q|,׎�5 0�9�}�]E��O�C�'�J�eڤ���)�E�L����(D�Ԫ��듼�֨�h��w|^XA&G,?W��j�~��E�Ĳ$^����eLn`.^�W����K6������z�~��W[a��������Lh��2�bf�Qr���E���Q�wiīxY�l/N���vd�0Gl�O�����a<��5��:n�>OO��M��&M�Iw���__��v�E��q��e��>Oz���Q��(� $�]NsI��d-��5Eg��h���n���z�>j�^�!T�j�
U���_<<�or�9�J�)v���s������S����c�Fdi�����֮mܤD!��z��Ǉ�u"�a���Ĭ�k���[Fw�5^�ó    -8畣)�i�߰{'�!\�"��On@i�F��������jVk�v(��Qm�s�<+�<��h����<>���uh<[���H�\%+�R�
׳8څNR�l��2��g�^��=X�$�P8Ч��K􈕔rQE5�$�4�|�
Qۊ|�1\ri�Rf&N��o��H�T�|J6�!��-�}�T�+q-1k�4R�+��%�-�wӸ7��IJ�$���
/\U��X9Yhp�=����5D%���=-?�p�f.�Yp_Z�C7z�xEn��LP��*��-�FTE���-�L���tk�f�#�,;w_tĥ�
�V7�Ҁ��5�����ITb�f��v����+Kc�?���1�lv��c��R�w�>T9�d{��vW"2����*_��A(�:��� ��7�KXdFO0�%NV{��������ì.��1"���}�9|M�H���I������(:�&��ʞ�d�KT�6�ξ�Z�`**)�T�h@|O��xp�XnEp7��TjJ_�?Q��G ��ĕ��ˏ\<�k/<�B0
M�[-�*g\�!e��u�$�Zt/��p*��3m�@'�C�T&/zQR��sxv̕�0z�m��{� Gs8KY�m���3�r�<����2,����W�	�6�ؘ�v��}UY�|��U�q�[ۈ��Q�9r��󎐊wR7R�ѓ��Q���џ=XE(Q�g��L�ͻ��}�&`�J7��[&�5̤a(��&J�8�Kk��ݪ�5�)Lj}]���|��(s��U%Z;��;"�
�7i��u=�e�-��춒V�DEK
����mf�x�-�{�o-<E0�������*h�.J����>q�@Ţ�sH
��k�U>����p�-��~�s�@!�y͚2��y���NM	{j�?!&���:`�C��bvh*p�ह�>��"Q����ƃv?�~�O�X���%jX�����!B+�A3�8܂scBb�1��1�cR;�V��m���z[Q��`�Egm,t���t�3&l!�FcRf�e��8�3k�7�M�
/:�"�)x2�*RA�Ê�Z�9��DU�9����7��3�%)�-��G)�k�+�qAMa�k��3.�+�G*���j�aG�]�A4��Q�1.Z����>P��L�-��Fv D%[��
-X�&�o�ry3��Z�Ձ�3��7�D)�[v�_���DN蘩b�:�2��_G�w(Wk�Mח�y����d��/:XS�����X����eʲɢ9��n*�l�MQ!�vxR��Kg��b���`���'a���mߪOdjw��%ǈΓ�%�H?C��N��k�i�H�q<����z���6�/�4w��e:��L�V̇�|W�[�Z5��Z�M�Y�(�֦C���?L)�_�)e�p��v�U��Ƌq�p��k�L�2c�fտm�<8��ICOp���\jC���{�	�^�����͉�0]T�s���v���k����O�q��.qɝQ�Qۘ􎈩B��D\f���T�����V�����8J�٬~(q؛��I�����^i�����<�Y\c�t�i�}�
��}� .S�7�р�KA+���`�*��s�$Ѭ�,xV�����e�~Z��
�n	P�0�k�*U4�8b�%Y��
Gr�S9�Qf\�������'�]W��]�sEE�YN��nh�;���?��\)���w�<�vvxi��Z��']�L}T��$v�F�Jt�\s9a��z,���k7�tk�c�H�N���רK�.��'c̼3�H0��΄�Uf������"���c����$��,qq�t	��]]E4�Y�:������&���,����
mUڇu�_�&������0tw�fFb(35���� J!�w���Q�AR��lm��ޜ�7n�M�l7'΢!
}�-�k1��Q�˃�2��h�<U����RΈB�ȍ��2e6]n40Ka��.Q�
6��O=�	��NPd"�Xn`S�Zmnַ��h׭[�Ϊ87A���l#~���I�HS���w�f��)�P��=x�^�I9���.X���������?�$�^��H��^ʀ���+�"t.���)�j��H�:�x����lМ�B�/h{��3b���P�.S@?��>P����7.�g��t�{����Ƕ;��%d��S��f.��2jV1���u
��e�t1����~���(�o-�X�tcaʬ��j���nTQ5�%ԌJ] GsUlâF�&hL�sPT���b�@0
óh���L��D>��'\���:U�:0����:�ݽ�Y��O��fp��S�I��:��Z�j{M0Pkg�K���+&\�#T�Vʸ@B-��*������3%S���,|�$ё�h��K��z�����I�0�C0T-���U{
��p]�ǻ��9Ε�z�^���8&�p�9�BiA�Tm)r��Kv`����,x�|����s�{���_����r��)B�.���A���"����Y�}NΤ9D�jg�M�3�����/�����,��t!���~1�2�)}o]f4 ��:E9�KN���8KpV���u|��M�%����[|�m�`0*����9�;)��i��bb���DO���p�=F�F/VC�k���8aH��&!�L��~AY���Yš"�7U�EX���9�a�f���r�����4P!q����M)
p�X���̪w&X)�om���C���:-�/G� $���.Ѐ��4�k�ᙇ�oS�ù�bE3��=R���,�̳��3�p=�	T���5�ն�͹�8��s[�V�ΔƑy��y��Y���� �5��u�x��{��������R�;�\�#*�/��`���×lo�l����6<�GΈ3&Ժ3?��s�#���a 5���h([
_�*Z�Iw��I�7��ӝ���m�e�Q�F�(��3�f�:�j�%��[�q�0P�,I_;�1���ae4�Y)Bܵk蝃����G��ך)���XGM��C��[l�q@��Q��+�ڱ������9I�پ�z�:G���]{��}p�`�`ݞ�q���K���j�odu;�Zࣖ2�Zzx:�k��X)=G��������ǽd\K�|rU�b,~e���pss>�}���=���<=�#z�iW��>�\��uίF_�%�2A)�p���D�/��逡Ϫ�=|;'�\.�;g�!�����~l|��1��=�@�N�%��k�c�x)G2(E�w�ly�~�$_�$��*���4�����:3`�s��{���
�/Gd7h+����1�5��+�_9�.�R�n���ƟUb���F�C�<���:�Vi�b-�I5l(�-y��[Y��m0Zs[���6G�)x���C-� x��ߣ���	��~ӑ|�,K��!���MV��E�ց�,��&_�����9���yMLJ"yЍ�L���/H���H��=���� �nikٯ���3f��@��޳�)����kHk��|�Z��R�$����?���(���ܐ,�d�G_���͚�iLHn�9��gc�}/F��-��.~�����E�#�I��>�Қc�B��͔=�h�n�o��-�A����ip�+�h3��
E�?�x�������,�����u�XMw%��z�s�.�6�
{ĭm�+�h`���z[}g�����!��q,�~�Q���Ơ�B�ݠ�ؾ�I
Ca�2�ǵ�&��K�U��3���
]����ؖ��owܺFm��y���1ScPM'�����`j�ae4�t�ޛ��jV`��	����l�����'�Л�����r�3�2��r��1��:��#F��`1���Xyy��%1�e4 �R=*��u�gY�L����AGs.�_���>MQ��/�͂��DUl�q�WZ�m��;w��7(G҇�J�?�N��kU��Q���z9�r�aꔀ�������Ԃ!*}�k� 8�\���<vR$M�����{�fN�1��]���Y���V���lI���X�Z#���Os�"�~c���|�i���(��,����U�o�� �  O o�4� �%��0��ew���'�c�g�wo���k�����_QLV �Kg�f#m�3� F��^��|mx~4�՝Օ��AR�}kr"<�Ҏ,t��'�5�$O��F�4���rr�рհD8���j_���5&�&��+�6�~��3�T�#,��4��hN���v�[�%1�^���hb�Ss�\Z0���	�\��+~�D�(Z@j,�>0$�y�aa���������<|�u��л?�\�� �r���2XnM�]J� F
j�q+�v��0�]�!��.>2�e$��}�֥ᒞw��xU���]ʵge�����_]F���j�~����;�ߎ`�f/ �`-U�xW ���uh061����ԙ$ ��{���J������'�Imˇ�8�լ�u�{!1[��g4E�V��S�V�������p`��%��N����ᎊ�7D��M2�38λ��*W��,���#{�Dv_���z�S��4��^��k����,#&�jN$�V'#���{Ě�������D�
B'q3H	��R�K���n�o[*0=�\.�9�?j
ߓ*͞ԓ3#$5�ʟ�q�%�<�Ni!�[59�;`�bp�����^n@Ȋn;;!.XG&��9"�/}�D����G�mot�;Q&��z!�<a�/.�"�+���N�`����U$�V��\08���"�R`Ԣ�3�;�O���T�:?��/џ��� |m��i���EX4sEk�z
��O�K�%>y��*�(�>yC��9Q���O��ipE�	�i���!������i��}]t�hE��l���ֵ&wx�={�;��EPe�,�dI��k�W�
H)��I�(�X�9IS*�T����H�ɿ@�U��I�\u�k�N�$zL���^Tx�$àh2H��;���bGj�t��U7Ѥ��I�H���y&iGɞ8+�M;t��̌f��{P����$A�TU�~ۈӂ2�h�@[����
YQ���6W`�I�� X4�]I�N�	��^���,�R��"HR�6P�Ǟ)�K��j��k$�x�S�zq����`{z�	G�J6)�d_���N��s8���$���;�;;�g1��g"s�Y���r ��ٝD+��&��FdZ�`/��\���sL����8������&HA`����0�Zk�G�Ź/T�Z�k�ު	�ur�^@�D(H�'�hXiF�Ŭ���@���U{M�r���v���&xh#Rl�a_��Ӽ��\�@
�����N������*�M;���̤1B�5ztC���5~�%~�1�_񲊴,�E,9�AWS �tw �A���J���:*�3��6xƳ��,,�}�7���ǆA�������^$���M���n3�#&t��"^�������ۈް�Փ��nHO_�=�E���^�1���������� ���      �   S  x��Y�n�F}��
~@]po���0�&iR�v����Y��H	��"߳+��e�C>���}������ݘgSwy�l6f���&����~����+B!���*�2�fR�0�Su�u^�0�q��'S-�j� i(v[Tf�t.�����rř��})����d�y��H}i�%dGV7�y�Q[3�Ͳ(�I�H1������h7�vg����$:̴�a�H3�>U�M�ܭk�����I����(aQ�f�8�Q�b�D�җ�A�����<	��1�a�C�3�4���i�u��#�Y�2�.��.�
��-������uiv@�����2���Ї,	�L c�\�H�!���y
�2���9>�#.Y�YzQ�s�<�D��ćW,L���[:�����|1�)�4�#�Y"OZ��t��,�4��w��=�bJ��'�a�<L��a_59�����٬Z9+���zD���&x0�&�+�]3%Sd��`#�D�X�vL�`�$�+A��#F<e��p`��#	#���qb�F)!>@�3u*�a$K�HP
��a��Ύ%˦=���X}����Ќ�'��e��L]L��䊣�#�E�cF(�����u��N}Y�l/�]����)�B��[��|�k��i���jP����~J`�p��f�L��9�B�DW�j��N7�,}_��Q[�0ұ�f�`�9f�C������q�^<7�)�(G��Ac.1���(8u[������p�žn�+8f��h`܇T�zС1,����˦<��_�s}�eL`uF8������yF촙Vn����ϙ�2�i�2�ad�$#�;�o��A��ЌGd#h�1�C=� �j�P	FЌCq�w�	W��	Sqݧ�O����v�<��adu�8 ���T�v�,v{�^��G��#(���|�:�_�����h<5{�N�K||A_����>�֫��Δ�j��f�� Q�y�0�*;w���K���l�O���0rJJ�TE&�ܬ�:��wS��χ��J1��S�0@;Mq����(���R�������jr��I�I�����rt|���*f(I����,'��K��:r���ܻ��i�q�[��W);��C�T�s�t�:�����GN�Au����;�U'��
�=��1���&�0�'�̙;LǨF��bdU�洷��������
l�Ъ15j��a����h�<��ɮ��9"Ug��=�~�]AG��S��_�ؖ�햬cǛ�FP��;��Sy�/�7�|�$[�1�M�I���9�%�^k/���07{W���p�����W��-*��Ϙ�V��J�[�eT�>����h���	���x��6]���Gm���#;�#�U|���������7�aF�����Z���E�2��y�B�W�"� Ws�Q�����z&�<�`A*��ҋ~�`��`��I/=����P�3�`��-F�m`&}����(B֊Y��J��?�(B��`��U8�w��Z17kA���.s�V�L���a�%bn�*��6s�V�L��-F1�V��Z��*�w���Xkyh_a�Ug�!F1r�ƛ�4FuF����7���!F1�[��Ȣ�N1��mv�N�U�y����MAج�o����M����m�Q�,C�y�k��I��C�{���vsI�]���SV=1��d	ŇQb/�a�p`�˖M�-�<5��(A�"P��jz����[�!F��@V����E?P>�ܫ�w�K%]��l4�(��a�7�(�N��
=F	`�=�pu�ͽC�dv�7�*{`.��}��}��I�=��`��I�0JC&1:bQz͕�%��Ծ7R���C������nd��      �      x����n�F����OA`n��`�x�;���VKjR{��*�iI�F����A�H��ɨ�<��G�Sd2�y��>K��mLr���e�^�O�sz���X'g_.?w�6�����S�&[dUHS��1�"ͪ�.F�^� �8�h�B됦l�c��<��}}j�M!�l�}H)�H�'�����
��M��?�Uz[����g�{�{��\��U��/tҀ�[f�U�gS�N�L�������N[+o��zu*����Ter��W�:�]>�1�iV�s�����w�!J�i �/Z�kmrZz���/���f�I?��~o6��$�Zdz�@cM�
��l�'�zY?=�������}��;�����9����4p�	��ԶL��ߟ���o�k�s6�ʅ�>i�eY�rx��oM����CEHHwFU������2��������D��
���/��ǧz�|���Q;��©���n��\7�m�G�؁���������4G�m��$��y�6���"���׳�
�s�X��"��@vL.LB����+�<�������e��3zM��!M9x�����������#k���@�2G�*9�>��@�5H'7�jW?��//�{�[��NΛͪ٤���rs�\m��.���t�9L��t�z�J�Q<QnN�ཛ�J��-aj��蕪�J�8"�r*��L)��돁SA�f�b�u�>�i�h��{�,ȴ ����P�*K ��F�@��.$�M!My�
�<ѹI��iU�[K<C�r�o<����sL|"�<Y���ТTxy� ���q�]r����Հ�cP���e@�0���@���!d��Yߘ帖+���Jռ��#��ǹ��!A>\і�S���D���g!D�%Tұ���p ��(>٣"�
�DH���bj+�h��@*;��iZA�R6b(F��k�TL9e�0-�UZ*h��m��~ Pc����5d���ف���89�)X�GAj�!�E��}HG0�I���-K<�Yy��0⠢+�l�g�̚aD4ZE�T�������y���W��C�u�uN�m���yY7�]�S���*}m��{60�X��O��g�с�B*�?%L3,.J�� C�"�!x�%���(��5p
�h��6˗�(eEc����W����u��;���h5c�(p���Z���@�L96
���,��4z�7A+i�8�Hc�ȷ�cb�-�hYrѠ���S4���bi#����j]HG3g>L�yjѫ�xw8��5�r���)�����G���47��r�S� 8�)Z�| <ɰ��Gǚ�~�`���
֕82����k���$�c4��<b5�Ӵ1f֑��X�i4`��'�ݞ<�@����Q�,��|��4<��㛁���!�A+t�a�n#I�
i@��t0���C,����b�!:�	f�I��f�Y/W_�շX��.?�#eL��.�9���zyl��hb]�)T9�0���Bb1D.9���?7�ǆ|���f�^Nt:Y5�à�5�fIV��l�4Pr�8�aMM:y��ːNŜ�z�Ck��9b�6B��&��ܞ�0-C8�=�ɘJ�ڷAk����iJA��Nκ�?�Y�2�p6�u��UU�SҔy%	��V�a�0��)��ѱ_�]�ƨh�tvL��yeoF��DU�����S�����QuH������-"X��f���)]Rg� �!��y
 ��f�<"�|ܾ�'z�9��ez�G��e�5ٱ��	߯��Q�m�@2鈨n�FCJ��Q�_�7qi ��tDT7M�u������2t����v{��9��#3t��Q��k�`��%r?�l@3��`/;����#J�Ӡ��(��.i��h�5�5�����{��\��-QI��_!��]Z/2�@�L���1Ф���2ڼ�w����"���E�)�"?;Z��5 ��.-��{q�ՙh���g��:� ����^�RR�mg�/(�5���9C�����G��T��h(��������+�a� rH�Z�x朴Z��y��f�9�����L<u�Gqq�)�F�f�8��F�^Hw�c��I�Nh�%����u�޿l�ɘ+��ٌ��{!M����Ji�?�q���Ϣ�Z���O��_[�����%��U4i��?��^LHS:#
���J�&�M��ϒ��&Y<�����5�yS���m�i���T!M��u6)�_n?|N��Y����,��N�9걽]d>�)�U$��Nh�'�茖H���k
! �R�Y�i�����XS��%*\F{�%g?F �_Tk
M U£��#��h�?6���4�D��@A���N��C'��k� 
8���z\C�}]o�����Ģ�	-��i���^���i��~ܭw�Gp0�*�)SNdS�F���꺮�k��^�B3���h�ː��d�'��&M�� +�4D'�5������i����:���7��`��s������Ԁ)2,�lXA����2=�׻�@.��er�gW�:�^SixJ�P�wk*���G�91eU4)�0qM����5��g����	Ox'?��f��3�'�(��h���a.p4��dp3K.��c�q"�
;��c�`z�h����<M�f:�N� ����<��%�v�p����a����;�1�� r�aL�L��5_w���t��䄑�o��ɚ���a�)��4��� Æ��7�#**�Ň@+Z	��������H�8���E �sQ��i��>,k�DP�i�eB���i�<�E�Y+l~�	t�BuQ���	%D��Tt@P����4rrSA�"�&�� �CE�q�mU��rK���D�X5�N#OQUD pN+�
?�����EVRӄ���LC��.A@�/��n��k��mB3��g=�D���b8��JS���(v�d���K)�~�}��������AW``���୅��6�e6���!허Q@N��T���������n�����W�����zr����M����]���������p�*A0�����u`��>O�����?cM�3&fH h[����ϝ��f�4UB�l9�p,�[ÐP�j)ō��^S�g��aC	(��h�j�S�e?'<�T�g��S���VVdF�?x5�TYb�5�q���M�9ꔎ�Ҽ�6�������y]��#MU���ŋ !�ŋp,���%�M�~�H�4o~���E꿪��*O�יc��s�)śz=���9:���n�.�):sW���}��M�.��� �3�dLE�.�����X�]J�2�~=w�)�e�9ʪN3��>�^�P�DVNd�ӫcMt�,i�Y:G&��jvy���s�Y�g\�\,7u��^mC� D�<)���ݔp��)�҈9��"��S�������N�T	:\�5É摦4���`�~�<֫�����S:�&��=8����z��1�Y���f ��?�ˡ�]�U�2SXg����G�:=��G��ppuY�J��� h�)�x�����S���v��#�!ą4�6��Fp� f2�����<Ҁ�#\��� F�<�����}ҀЌ��a��n���%2IIò����"X�2BhK�*r`���9�`^5E�	Q�� �}���ay�`o��w�`Mщ�ʈ�&*�8¼-������4!������{��g�i�� BX71|�Kl��o���M?�%��V���h�(!�u���-E?I=Ҁ(!�%���p}�����4EëJ��F)tH��o�?";������N���U���M�a����X�&V��i�2^QH-`� t�)ڳKq��`\�&�����j���g�4��NStڔ�g�鐦�E��>��NS����"����d�E��HSt�� ��Ȁ��H�!�h�C��S&�N�$��֒"hc�8:��/���1F���?��Т�r�8��s<���j��h�23lr{v*�h�c8"Pقb�Ex/e���.y���U�"�)��K���	i�6P�(���S�^bF���i�� }
  7ӈG�������4�0����@S��F4FէPi`������Ya��s!��E�x�<�l9sn?����|�\��[5֔.�!*S��[��L1�BՂ2�n��E��Ḗ#,SԞ��/�ٟ�k@�-BV���",-SDY���L��}Y�j�2Mq�$��G�i�f_C~7v?�4�!�O9ܢ3B�6�٬1>[d��k@��%
�����z"�=�(-R�����XN��~�	`��R��ܚ�@��hE>��Ƌ8'wC����V?4�5��!G��aC�)]�g<� _�4�4g����p�9� 2��C
�R4�,������c �AbĠ|8<� �[
*�4�# x4�-���c�����A�g�TrNn)�:�X; x��{vu�v�2Y��[��5��P�O
2�)Ł��}R���@���A�г�<�F�=��i �yP�g��7E#��S��A��a��O6KA�a��i ��DV�o�F@g璂���k���pr�.ɇ�9���{vpH���Nx�g��Y8m��{vu�v��`^���p�n�)NC�ŞMkNyH(�L�rЛ��5�7rx�g���eMqF]/�l�li���y�g����@S�WË=ۘ�~�@�� ��n�;�J��î7��h�/!�C���}���~Y>�S��q�����l������/���n�J����I�Dlǧ��Ҝ��w?X�h������py���������3�����8r��,XW�8Q��1�^%7��~w;1�������G�e�L�`g�!�O�Э�'�&�Ș�Mq���͇����zw~�9�����/�O�Y�dv�F͑L�����7�.Zv�ܽ��u脱S��{�P*N� �/���>{Mq����p�q�^׫��1ZG1���l@�D����K�D�Q�����_�yy�=�ir�Bh�EM�L���w뿖�Y�����
�-'�R�!w����?��f\zM�GD�p�S���U��r�i��֏��3-�����&��ID"��dU���^ŵQEE0���zDp9�|��]��k�D8�x!�m(�i��oGF\1\��$/p�B+��0�qz4J�5��pF`L�b�R	�K�vO�*x�/�ђ��I�^S�bI+���"j%cxv!���|^�]L���PJ�Ter�\�V�� �1���壅�^S���1�2E(����p�qU5h��.O��&
�bri/��r4�k��1E��"��1�BE8Ek�Q/.�+G{�^y���Ty�0~�e���5���#7�0�1������bbau�bZcN ����o����w���aƚ�Ɍ��M�ߟ��C��Xn�����Y��t�s!M��w��.�*;��mV�fy"��͍�a{M�e|-&O>|�;K����Y���>���[`��cz��,7�Mr;�k��o���q
��<E�㝦8�4c��4�r�H8����vr��,�O͆�x�hu���>Ș���(���CC)Κ5��=��=����ȟ�WQ��:����|�c��~����oMz_�h�ִ�~������õ���8H3���,�}�����������˿��/�^��D����y�Q�M���\�6�#c�J߆�w4��;�2�m�'z'����B{�ð��gA'���m�n~4��_�(�.r�E�h��׀)Z��MJ5��]��z���gN:�@�����U�w�������^�F};b}8E|7��)~M)}����K�<#��G���,-�R�=jc����>�)�B8��-���7���6♐��e"K|!a�{��Kb�`�@�u��d⛠�p�.3��?���;��vE��:*+��U!��c������y
����0����r4ei�r2K"�m/�����L;��M�!�d�BޜK,q�a�@�u�%
��L�>�ъ;�8Y$g��<�<�|w�A���e��Hɐ�l��ȋ+�M�ς5Ҕ�s��K����7k�h�D���C��u�"��1!�r"g�<q�������i���N��T-x�(��J���F�h��-�dm2��C��t���&cd�@�i��c�Բ��jo�\1Y�A�V�?�p0�?�@�L[8ɡ7�φV�3�w 4pl�A7�q�R�H�&a�(fm?���4�|�'��@�[))U��ou
C)K�xG0c$���-��B�@�3���T�L{���o�k�R
�#���P�4e)�";қ���w8���HGܼ4M�[�L.�+�����HG�5M+�������^C��2�9m�A/5�����f�<��c);�g�Ӕ�;a�m;����L���õ;Ի,bw��s��,	c-��?�?֔��hi�u�[�����䵺�{��Ȇ4�<�&�1�93��X	�"_����vxg�@S��AZ�<�NI].u*�K�tK�In��Q�O-����oo�d����"�1�yE�h��I��p(ޠ$w�����m���g����'�8G\q:M��*--����Ok�5Pr��t�g[�����o�h�7��5pJ�H�����֠�ܟ]�G3����}1��);����b�iW�Mb��빴��>��XC3�X�氆�v|&�|�s�r8�7�@�-����4���֍���x���J��C��i      �   &  x���Mo�0@���q;d�eK�m��]4�m�12ñ=8�����(i`P|x�Eʢ��H E��X��2nė�mں�ۏO��SI�g2�I%��V)��7*�Eѭ�:6<&yl��0�6e}R����R:/Ђ(�P>�0��Ves*�7,9
�ͪ��#�ViS=s�<� �~U��$�pMa��bQԿxq���c��u?�%�K1$�j�����M�f	m6h5�|�V��)�������)��,X9UkL�6ŐX���<R��V���>T���]�+�?g��&��;0������0���Y3�KA��F;0T���W~�K�R��Zw���8�C���]��Wⶭ�����c�.ۆ���n�kŻ��/g�c��g�)v�)���?��`��Y�}�bs6�4Nk`�1��rZ>H�b�,8y�`ڍw灡r�����`�e����:Nk`�rp��i���}a8`�����y�MLn?S|a�����j���&���7Oߖ��c��C��c��f��5�����"�W�W            x������ � �            x������ � �      �   �  x����N�0���S�\�����
!�{Ǎ�F%j���^����!%��@W��=�8_���u2��s�Mn��o1/���'�z]�ݦ���ӕ[��e�]O~�9�����nzY���i���V>;���_%�j&�L�Sa��Vʨ��q*&���'������#3$j^\U��M/��-�o߶H�M�l?�I&���5iTC@�%�	@�%��,�☆��W ���
)�dTC@Y$@��
�<�!��� �TB�V$Q	T���U��@��­ʷ�a$�訆1DPˇ�H�PQcȠ���Pyc��n�aLT�:��Cbd������E5�n^����+���O,�@���	I�>�a��hT^��h}�`�YF���r��5�0��4�ĬʭA��5L0HϏ�S�"��NCr����v(�Y�$K��n�?�2���*��N� ���D3�\"�	[��0�(1?1,�i "��z��af~"$�m�%�j�S�c��e���&�j�#���I�0�3�
��;�~vO !ݟ<ߙ�6��+��]K	JR�T�a�4V�_�L��t�G�h5��1�X�	Sp&(陂��0gk��r*Ju˹�ITC��Ǻ䋏�����<<�u�oMtYW����܍9e� 	>Q˷�@�9��x�S�a_�V��<#:k+QEx;9f�K��NM�|362洳+56����(���[t��i�q�����=�Ϯio=���r忺+����#�'i���T�0�(��ϪhÑ�2��Q���>���0
���aG�}�" ��B�`�!V=
���(���*�a�Ar��湆���!�/�s���0����0� ��@�hc��awF��(p��j_e�f�,�n4au��v&� {���]P��m�a��8�ｷa	�f$��N�H��w_$�2bG��l�ki<�(�#`�"����e���H�ve��Z&�G|8>::��P�      �   �   x���=��0��z�� ��ؓ�n�6�v4SD�)��"�$��[<�d301��r��U۟|��@)Q���|�_��q���^�U�9QV-5��-
�~�Z{?��(�K��XoF�Tj��v�{���)Z�oܘ�rZ�ƥ�3��� 7��� ^g����ip?�8�E���DNZjhKys[<��'�Rz+3e�R�}@�+Ns�      �      x������ � �      �      x�����:�'�7<��;�g툯j����o�`�6�sN806��W?������ds1���UU���L�U��Ė�))��S* ����Z&��Xe����b��3MU�b&����k�1��,����s���o��{���*2�r�:k�,����(��4��\����+�
���2�7/�M�V��6�ӳ|�=ܰj A��XmR�� z	�~�e�?D��O�Q�/8���� ���i�,  J��!��.� b]�+����~	־m�3$��$�D2���]A	A S���
wՆ��z��v��s����$�����^���}��\,��o{%�-H8 ~#�_�&.�v�vf�f�!k����p�H�j{!�a������a��� ���$��+-u�]	�,Iѱ����K� �,������ZG�)��tnkt�i����+��4m=�?$�2�����á�e5w��΅�)�&�������CȞ8V�}k كlxX՚Y��ǧ9�=%��8�b%p]WŚ:�C-WM�fX�Ћ�4lBB�9�Y���n��$���Lt�y�o��{��Ru�y����c*��p��O�e���d��ٲ��I��\��B���	J�"�2V)5������_XA��
f�fjŢ���a%V�jM�i`�Vo�5]�]rMU0���J�P�<X�?���Rj@�O*��[<K�,���x�=/���Y���})��0{14%�Kǐ����j�t���v��b2���V���}����W��F��!���_t�5-h*(*�^��gk8�ַS�[�b��rC��Y�r��J��n��������x|���W/��(3��vt5Gƣ��w�m�.�XO�7���o��
l��~��Pi�,`�"G��,��$���o{}��a4+!M]̜hJSH<<	F��%�J4o��X��n���Z/������ ����5XB}=&�������3�Tf�����	k9��F$��7��ܬj�e�N&��@�T��K`�}cܚ:m�9jH�k�J����F�i�,`!��V�v��o���Y��\W�(Zc��b%�w�{���ؾ�x�?��CgyCL�by@!�s�1������V�lLu-�^�%�'������~��|>��Kԁ��,��4���nZc&�|�n2Sk+���ݶr==�ug�d5���)�C1�m�Ҩ��<8ʙH'�ͦ�`����VȊ�5�ƾs6d�y���zc!)y�J�0]VEL�#������	�#bC�6�R���դ�nh6tNe$�9^��&5�TzD��Bs1Q��C�k�[ݕ"U
��Ϗ�# �g�hY���M[���a�8��e+������:Q�2�E�Q�*\r�Vl���|.8Ɠ����K1�� �[�N�dH8�h9B�.�\�6�5z���7C�۠��j�6�a�-Km@�ޟ�V��{"�_{L�$�F�i�,�"�[cMP�htD��Z��D+:,y�����mo��#e�G���Z �&��_�ZVſ0�vc�z������h��_�j�u��Wψz[�Ժ�t�C���G���N~�0<��sj��e`]�uǟ�樍	�єv�E`�Ι��!^�Я߰'̪V���>��ϴ����l���%[��
�6���O�����F�i�,�C:�����9����;��Q�r���]G��o�uypF0t����̶P�s��D�iQ�Fr��q�4��y1�F�\t*�FX;��e�]���X�R9W{p�%�y¼���L�A	B:ϦH:s{{G�jM�Y$$�^�N?���u�3��m~���������R��&�-�3k�Ù<,Ox���q`ZU*~u�pU��Xۮ����iѓ��b��G�z#@-Kt����u9�-�4&N���kk��M��s�c#۽��"����ﯨ��d�皕&�� Y0݀����t�b@��W9����3a?��!0����a��
5�ȋUʥ��Or�n�u��p���(6���� �8�g��7�p]�"9�\(���Ci|}O#8�`b!����!�F�n0.����b�U�~��=0 �?0"[�#�oŇ�4�đo�F�R!�T�G�4�.�F|�P,I�,�	�F$A��n�����/�}};�t�X�$�ș�ڋ]�X�O�5��B.�, �� č �G�Td��%-� 'ؗ�侩��4��Y��C$-���C��/����4"?��\��6��Ƕ�����N��D�P�{-��n����uǛ���q��U,���[�v�*���|Ҵ����98�d�x�~ZJB<�I��k� կ�ȁ��6=: JӂKyܦ����a� ���F���ȴe�#0�V��@�'xS��u*�8�v�O(�E�]��VHA�X��m��&ͻ
�ǉ�+n��P�6Z�[�,W���=�po�F��� �N���zAV$̀K�]��G����k�@���h�|X/�T����CѦ���pkk8���R�{N߰s�Fk�Ԧ��ڃ@8mk�J5�eu�Q���?��b�Ђ���.�U{��������HȎ���q�[{l{���M����gJ�;,i�)hRE�V,`�A�Kp�c/�����O.�4i��j�?j3|�̖����j�}~]TVƂf]r����cÒ�4��4Z��zii����~�AP���m`�y�d5'1}�:���Al=��}ѱN�>/�f�B��w���YAB�t��]c�	H�0����W�[����=�6�Va�j��<1+��J�	��$�p2��!�O�_���Zˈ�>���Ϲ�G��c/<�UP#���N:�oJ�2��\%'��Ρ@)���]O��r�xdw՞�9�'t�O��hY���&9Kϳ΋���[u� ��7D��0Þy�4���r�D���/[������[)�⠘<m�ߢͬ�&���C����KV�����[XEmB��@�`��Y�B�b�p�e��+�c���.����Ә>6\% ��QƷ�p���eŪ�[�a�U�M�1(4�ؙ4Zh�ľ�s�;�p��j�m�'�pF�]�:�&���	��)���h�U��P<pLN��	�����?�L��������tG�]y�Ӆ��� ܭ�C��M�++[í⹎��R��<�1�ayM˒h���ճ7��������=����X�'��:�^���Nt�<B��4�ܡ�`F�fO�(��[-k�S��g�I����`�8
�F��F���$��P���M-h
L�����U�C2��tSߕ�8�_R�6���I���EdKAٷݡ���ڵ���ʭ�D�(>��%	&C�L�vx*"�C�>؟%)�����B]�B{�l�x w�c�UC/�[��-��s�jSݲt+b�������z��$����hY�6s	�$�������Ks�ݽ�H
�@R(s8��7�?�H��dN�Ӱ��;3[�v��J��FI��$�{#�4t١ nC��#��+`ʽ���x�#����՛����dY:X�m�f[��VS�ݤ8���>�=T7�9H9��iY���I�iH%��>tFS4�����z�?�;E�E��#��͉�o�	��-_/����\TK;g�m�3�Ԕ��3�
�F0i4()8��{;�R5'ׄ�T�Vڂ����b�����f��E�;�#$啻bdY k!��#��}� D.�VՓr�/���Lr�0k	Բ0�����j�[3��K=rGΔU�m6��~ub�Ϡ"dro(A�"e��U�3d�p�֒��
H���	��p���l��#`)'iIj�	�*i�yJ	�"���I8����h"V�����V���`M� M�`A�$cIŠ�j�-L�I�h�V��� bL���Jnݗ�-ElZ�e`yf����l��a�4�y&��#A-���­RG�.a"V[��p!p��z�~�[/C�	��p>��B�E�	�R3dߠp���s����-L�#]wz�@F�������>���w֟��Sa_�穣��ͬ��c�s|�plx���گ	`�2G.+{uv�����wˍ��+m<\���r?o:�掻O ;x#�4Z�����@�F����w�V��F    �4��NX�??z����^6X��e�/�*�"?�\2e�A�a��&n��7��J�xZ��l0��vs�{�I�8A���,~gjb3VI�P���d�(����s�1j�	�_��Th�b�����w�{��H!�{��㶘ɶ�� 36��f�X�+�4n/�\�U�ъ'e��)�ú���<6E^�_h���Z;����ؚ$.�Y��8���(�XC}T��^"p���P�q8�P!3^�p
磹�k��l�쒠gZUzѨ�Y�N�=�-��)�W�i���Z��q]�_R���f�mdND��\Z4�i5@��@M�"n/`�%<��G&*,��$��=ؔd4ǘ������K~�]Ue�㾡K��WM~���Ƀ�p�|I�=PѲ$������c�*\�l~�:|���oEQh����UW� �w]��8��$���j<r�崯5��nwU�(�C�@�����^�����	� �L��mY��� Qw��V�8K���Vx%=�<HtC�f=�0)t
y�v�E��/J�R�W���:� <E�W(ޅ�%����R64v�ZK3�V��VRB�(�]���@$��(�Iq$���v{W��vE�bͮ�U]��z7��^+��su��!��*��A�	Њ�V�Z����UP���
�<�	h�� h��r�a8.�#��J[^TUvC˵���W;`��}������h&�����qP_�a�%�� �Sa����h����t�6�X��p�xa��-�����m���s+V�)0����=D��\�iY��$�<bE<�X�C��=���]P!ʵ��'P���Kۮ�veXa
��DYV��,��BPA�9p�i4(�=�cdT0tf`
�32��$"��� O@�ȕzg��$��f���"�7(KJ�^y�����x��\Uu�q�y�!c�z�������p)�]�6��Q�xɛS'�v�H.�٬�)�㥬;ڦR�Klw*�W�"+/��*��e�fPx���,�R�.��NN�O�/�B/�'�'���a�5�����E4�Q��Q|�Sb�(ִ{^}.�R�����ڦ ��`
�ejR���z��������QL-����R�Bg��{ʿT���2��5ܨIj3�eL������V�W��F�g �@� t�`H�B-�6��BϞ�m���>2�Y�7bMU5i>
�6��'�(�ԉW��Pm�DP�v�_��;��4:�Қ�nk��E�:��,\@�hU3�%����(�.<9��־	���ԓ����v8�@�"�>�ʸ- k�G�"��>[/b<��-�����=N�TG�|goVs�t�ca��)'���<[W�S�^z T"p�t�x-8���i��z]�r\���;%x��M��ƨ�Ӊ,Mj]�"LrY��	��@s[(=���H7�Ж�N�����̑,�\m�^hPHP����p$����B����Y��K`��޿���?S���	$4w|��u52xs�i;f����j��V�T`�Fu���j�m��U!�L�<A�o�FˢcF(�������gjG[�����A�(:�ӧ��������kN��k}ۛ����Z�LZV��o�W�y�;�V�q�/�����&oѲ$
�������8�x�:Y[H����ْE��٠�~ϗT��i�gp"�>��H�ޗ-���!��+z���M3���/��K�jr��3�E����ڡ�
OTF޶��G/=��P�_-KB����{%{ÅgN|'���͡˓	w�Ke<�R�#p�"On���K`�Yo�\z�XL�k�bmݛuՠuzhwV#�3hmW]�����~�+����ˠ�������?��p���hq#�Q<2r[�
Ȍ��y�!�c�
���c	W�Q���O����	A�8��ϖ�<g�~�98�]=\�b�(u�}1$�*-2eg�V��h��c��d�~��&�G�!�.8�=`��$G~q	p�1��AV����8�f6��kқ*��9r��{R�W�πC�|�㚖$T�6��;b����/���C)c�o�jJA�(��Vj���&��t�����L����zU��}��FO�D %1J2(��i�<��?�@׌ ߁D1>/d*
$��2�/�0��aW��]��r:͢�Ћ����U8�5Kծ�5rz�֟��s\�5-��w*��{�<�ChV��oI�tU�O  ��c���Ú/b��?�f���ae��q\0���T#wj4�z}���(Q⟁����.
Ѡ�9�-�!�0���c.���/P�1�9��!�=�T�������[D������LA�<2F�h���2Μ-rΦM(3�]��3c��͸����.���������e(y ��i�,`�"G3����Q�u"�N��k���U��V�b�T�Wꕗ�7V�rrY�(HL�@��E;�U��JZ|&,�(����r��a��Bi�r!*�:�<mr9u�����ǡ��nzk��I�*R�EU+��j7��+ò����N $�,�SL�k*�ʎ�ï����:W+��J"��fL�]��`�M���,���j\�m���j9�۫�c&M��spҥѲ�����R��6BА�<4��x����؃4C��X���<:?�޷�9�c	p��n��H[�_�������%g"C���NV<{`s��������ɑ�V�E�P�fg��e��������G-8�~��a�b���i<�Oz%	�=����,A�4s��87JԞ��	K�(��=�ZQ]m2�Y��ϖ޶�B�f��?��-�k�����=��eQ��v��g�J�0\F92��}��n�����.[�qQ4J��n�Y����LY�`�_,��Y��4����'I<B��	)�mï�
k�˺>��ᦜ;�� �U�S�L�	aʔ�{��^YjX}p�8���m�NDˢ�N(�-Š[�64͡���{��HKp<�w�#��k�J�ę5���yo��APd%c�q�Z�o49�|���ٜ�=��#�-K"��?�s)�;��|}HK������GX�鬓����Y�Y�8��_i��������E���A��B�P� 
T����b$���B�}����;�)�fke1�X���!笂3�X��8��{�X�d�����홇�h]�Y���$N�����rf�R�\fO��Qn�E��a��vs>wjU�@�}�Y��11��
���@��i�,	�xt�25dW���T=��_�6tCR�J?+@Z2rV�M�A�,q��;�����Ap�q�Y(.~v6K��(���'&!��Yr�P��3�D��抮�+U�3�7,:a���������4�'>����k��6E^�hp2���7E�s,G_�âW�u"*�����-�Ŝ�2K<��K�73��,�T ]9��cg3>�ꃚ�L�e���X�����#<IP$��Op56���&3��T3�^5_��θ%���>�e��+p�C��=���)��jph���/,���8\훊z �$((n�=Wq�	 �6�Fi����ح�5&�k�����!�G�*^R����F���4D�4pɴ} �K9����!��J��<�����?	U�
E)�̭��{�����8�d��qF���ږ�U� V嚌Ҵ��+�����4�%�1��P+rl���M�8:����m�μ�{�K�h�m�#-�̨x�^P#�^.�A=�l�4�!�p�E�"��F�C���O·�TM<f��z2�����'���붮�>~q�$|z.jA�Üތ�w�����3�UW5��5IB3�'��M|k�1�D���[K{��))�t*5_�Q����yE�-��q�͜��*��_[ΎX�>�(�����ju���* �;W��NJ��y�~t��,�@8lXN�� �xs01Z��//CUY��a�0?��Í:�w^�i�L�N��x-��
�K�ت1@$�:�58�m
F���&iE�=�V�Z =��_,�8cDY��	XY��J+ ���x$OPU"�~�$�    ��o��8y�Z!>��1f����^Ӭ!t&�k�W2��^�mz�Vd
�����,�{��n,NfE����T-X�X3w����Y�]����;^�"����v�	\��H4�V��� E����t:����O�^���d�噛��S�:���Ƈ��+h�[��i��/�E�}�ފ3�B\�_\hY��e��~�D��)�a����$�Y�����KLK�����T�K6���ƪ��ņ��[W.��M>��c*b��|�dDˢ��(��.p�a�P|),
/����&�Cz�Z��]׺M=���q����>t��B��^�	�Ǵ���p�{Ū��sV4��8�C�� �׸#G7l���T�I�|B���J��� m���x�[+,���۶���,��!ߓ�g��}���"Z�lB�y�jz�g�󴴽#�K3�f��Ѩ��܈���q:��q�ހVA�\�6ֆ�h��,��;��.��6_��ǫ��4&�8�,�׌ro])�%9�OBc��sv�<�k�h��u�%����H(�@�`�i45)i~��[�՚/-�מ�������Ѕ�Z�б&�8Ρ�B*��/�b5�F��`��&�1Щ�8g�1G_����;�`��NN6�6=}��uJ�pU��hG1櫂�o�󃤻��`ЙM��p��d��+.�R4A�"��Ls����O!��S �n�P������w{?�+{�x�&�8Yk��|��=N/-��5w�.K�L5$4�2�{��x����Dt�n�a�hYts �G0�.F�<�8�������h8��������%)l��+���^^�mq�-k�)�	�!�s��kZ]�����?��+(ͽ�{b	����a5�9�nw��U���0)%�f�s�DqΔ���|��g��&^Ӳ�>-�v?��$�P�)J�p�����9��H��4΁Ԩ��ӷ&/1�����2%w��a`,��Zu���,�S�3�R�JnM��n�M[��po4�t�,���]��s�JMt_G)_�*uM�P����j��F����1h��ٿp!��@#%�a�[[Tt�͂c�pV�����q�c̷7�Bb�Y��%i�S�|~�Ҥ�4��E��\��J����ϝg�O�$hYt��uK� �##���x%^	��~!H�M\d��e�8���i�%
�8��Q�l��O��C4t	��)�h?�?Ok>H�O�,q��?Ms�7�i�\��0��D�ة�%e7[/�r�\W�zCZ<���'ڀ}����64�Fˢ[�P<_��H7&�ONi�[8��Ǟ���gbd㱢��3��gzΨ��Bq�:#�g�Q ��|oi�b��E�'����`����7�i�,��ݥ
/�I]����}8f�N�����h2֊&�3��MȐ�1z�
\&`�3{��
�����V�^Y�1�۶0��j����|���R���V����k��pGD�KA��%�>����v8�|(���!Hg�,�ƸO�!g�f)4v����m�,�h��u}MeV[����v���X�U}�P�l�Pz��d�W�Y��"���g#Z%�AQj�CA�a<����,���Kx�o��\D�����)����cL�wu��BY������=���Ne�B�b�M�l�vǓĢ���Fi%魪�W\/Z�ܾLن�τ6�W�ׅ�E��Qꧻ:�Āq�9�K�4'��l�Ȍ:��&�>�+"�yWJ.s[P
X�fJPri<&\�s�t�
��!A��	�c��k7��w>��sN�9���D�c���X܉5aȩ6�f��~.�%o&�e���]��?x)W�d@��i�,:8���@���,:�p�j	*��y�Q���]�$0k�0�!H�,�3P��n�PS
H���w+A��=!�ܙ����kbQ��ή�qK�PC��M�ǄǞ�>�F�GW��-%	�r������u�)�|���3w�"��[�\iZN�������m��ݢhp����<pi	͸8qN�wMˢ[� q��������n�Z� ����r{�=���aP�	�嶯H{~EP����}���f�%4iF�g !��4Z�6�"����{�O7���U������|�!��_�\���k���/2���~a��v{S��R�x�a�1}��E]\T6�I��̋5�YΥO��o��j����嗫�F��hYt�
�����yd��ğ� CҦX�YBl��u����幰�6G%�&��rs�o���q�}�a�.�в�*��7K�J�� �q��Bh��آ@�0�jg�T�`����;��bY�D�#O�-v��i���v�
����Aɟl�\EJ|@������6k�����)���E�\������K�l���`@�!�,�ݣ�>�y���%�0'��?ޭ�`���EqdH�r�;�JJ~Lqo3��2 ����W	Z���R⛢��S�.WDk֌f%�h$�Rte@d[�$�q��S �)��W�b�.6�~8�6u����E:��8a�m�V�����~�5J;]%WO=ר�At�n�x	=�NC���wHݺ���~��^A9{pt:'��={u0�;C<�|&ԋh��8v��Q��I��F�G����4�s�ie�i���t�NU�9+�%{V�KD�`���c�it�����E״�`�T���HV�2�3p�ӯ�o�K&��K7�8�D�_.�S���X���Jǚޖ�5�B0�ij2����ߎW��(*��GN���_g��߈[�.�e�=k(`���B��]J�\�$ׄ�w�?���(*�ݚ�!%Cs^�+t�6+9�*rNf�	�j�A�F�N����e
�v���B׮o��в(���K�t��J�D%l�_�h���'�w�j������ϙW4�)}���Y~ �����"����u��r{�p丽kr��V���Os^ٛ,�#꙰. Ξ�5-KT��K��&�5�8��"�	W@�Bi�=|��?��(��'�&Z�>Am(s���Dʙ�6�b�]󗢞�/G�Vo4��Bp��q���?���I/"A��Ny�OQ`����4�p{h��rU�֥�?C�_�t����X�w߰Z�ķG��L��e����4:u�_��ū3��xC$���������F$X��2Q��8����_�1��2���҂��J��:Y���򯿥����W9Eܾ�M8t��k���6�6#z`��uÇOUGm	,���Q�/C�e
=��>��.��-�]8��:y��D.@4*�0I��[z�#���r~�o44�4Z:_�m����9��ҧ������)�7�{KyA;�?�����Qx,j��vϗ��8�t�Y)����VtM)�b��S�.��bX}~S���Q*Q�tK����L�k���j(�2P����Ɨ�;p�������F��Z3�f�#h���p����,��?�>t�y�{3����?��]l:��]G���nͺ�e�{GݞFL��Ӂ����L�?��Pu��r�6U�d}ϋ�v�>�8�H8��Y/�6*+|�ߚ������1�o�#>�r�H���v0�3D�ܟ眱 �P�y�VkÖ�[y�'�����(���"�J��$���@
��d%�sz�kZ�&3�%V��qm	��9��/�����L��b��I\���3����`G�*��Ǉ�G����=2�����kZ��2�M�y��&ݲPX�Ĺ���.�Vu��웎`�/.*�%�L^��;�����U?$L6E�l$L:Cq�f��hO�GO	�̸ӑ0{���I@w�~D $~�������L�x0l�i )��B;)ť�SKv)�|ե��ta�/A\�3+��~�fnm� A ���w��(p�7N��o��>��e�w��k͇B�Q>c���3��њ/ɋ��ӿ	�7�R��6jy<K���9�|S}߯���M[����hY��ı�i���X�(``��	�� ��9�p3�
��hy�	���̢���:��̔����؎`�:sS�W8x�8�����b��g��������{4��ڼoR]�m7��r�\��"��گ�p_2��t�>��(꜍뚖��L6qRs�@�d}�}Bߚ���    �?�P�)�U��M����X���C���}M��|��aS�|'H���C�f �Qƨ��ٹ�/�,R�s�h&>����ǋ���}��={�*gӪ��C�JD��4����~��'}��Ġ���״,�gh291��YŎ"�����ȇÁC�6���M0���z��?T�H��7�T�˒�����Z�:g`�
�������#�yhF�o4�F�2D��Rg��19�(�+�y5���^ ���|aQ��d����)��HY_�R�Ǖ|uJ=_��,N_��,t����瑖e@���3��]zy�Z������U!kӨė�W/1_/��_��������*���B��?5�>)��fw���wO����syh��礟״,Cf��F�#�W��=��G�ש��K��<��+{f�<R�LV��ĲĿ��Z%?�}�M�L(��5�,Ce�-WŚ�(,��!2�TQ�������4�]X	��N��,��3����`��a�_Q�4�U�iU���~�����oh�#�ӚW�,Cgh����9�?]+��Kx�H��O�o�ꇆ�7��!@��]_Ӳ�a�wQ�b��Z>>�@����b����_��1�>:F���c�;���I�v��H�2\�!�D^�@���W"
7z�_���j�����5�;�G�E���)�����_>S�\����5-���\�j��A++
����ʟ��]�`���E���-���|��) �qb�_�Z���NI�1���`���K���q؏����Λ��]�ÕK��p�9'�5-���Z�8մ���x��_���3N�cLt� M\tKd|(��V[�g���{~�F��F0P'8�4M�q�٨�K�����$�>�[c(Sι��u��j�~�S#PV�>���半p�H>���$L��4Z������ǃ�'y�����ؖ��c��JZ	�P���h�MLC7�]ߌ�G/b� ��C��fߞ�_?sV� �$��E�Lk�T��S�M�Gi���^�+u�t@��Q"�J_��*���R�������yI
|E˲d&N���Sⳛ�=qv��8!�Y(����
��b�S�}5����;T2��O��A~����}8JI{_+��2/�1�~�5银��"h�&���t�,Y���U����:��3�=琸�eY��}��+�"9�&���`�ɞ�9-�k���Oj�S�!9_:xޘ���*҂P����FSi�,Kg�	7��a�P��[����::�\tY�����a�s>��!TJ2��>XE���JUI�kFt�d���$}��;W/ֵ��D�,���L����[�ԁ'�Ԇ���V]���t�Y�a�X�n��O�mo����$ �ǵ!x��?2��e�A2��m�P�����)��Sݜ���S.���/f�����\� p5~t�_���eE��N�pfa6_Q8�q��R�/���k!6������Bn0�y�,�(�%��5-˲���?��<{��?��L���������q6#?�~�ͽIt��w=5VY�%p�ÆC6�}�ak����UVK�Egį�������4Z��2�G7�^��)+��\"+��|�A���D6w�"q��	����vW+Û\=]���=�t��qaJ0��jQ���7��>ɇs���4Z��3lZ~�c�j����BƮ�b���̒��N.?r�/ �Fgn��	����*��fK"���%��a���F��ph/�A�$���0���F$E
��D-���H�ltI8:S�Rb.����~��I�@�xX5uI���Mh����K���ZpN��K��>�VK�4�5�����t�:��;nX)������'�
���hY��Na`���9��>�����%q��_��� օ@ev���Q��@\���Ηw��Y�/���c��Bs�$�hY@�-Ѳ*�'Y��}����/D7�aТ8�w�$�y��o=M0	����>q.���^-�^e=�l��.�\~��M�����K�j���T�>v.� �F�x-ˑ��b��.8'�*���,D��*�y�6u�Т�qXM�(=��~��)��q��E�h��&�>1_�=������g���3�}�5X���H�<]���S���9��iY�����?�G([N���ݹqJ�t���|?{�3�n�;�u�X�]�u��Ǆߣ�;���7���m�Y۬�3'T���u����]��0X����u��#b ��m��e�Z'@�&��\E�ƵfϬe��<^�N�4�$���¾y���y9��*O�,�<�Z�f~���Ҭ��cO#97-�p|�)�A7Pz��=8�8�&
�m|-�16�^w��3�q������Կ��}����h戞�4!:Au1{YƜУ���eCo�z{X �~%ע{%u:o7��M�������*�����汜�,�e9�GZgR�8��ьij�N唋���K?g�1,���6=d��V���X��
��Xi��s1X��]�[�xŹ�f�KVutA����Y�t���H�\���*1�Z��O��M�ϙJ�.�P�&���D��|t�������!�2h�ŉ��~=��d~e�����u+R�ꍵa�5	/�k8=���_�U��O�#�ϗ�\Ӳ��x�%�"Sic�!�C�[,�<1�\���b�+�s"'ih�G��N��d�'����x�?�;��� ĝs������I}��&K��n� �[�ݰ�3Kq繣Τ9���G���䩮��p<�����_wt܇�ڙ�:�>JCi������,��:�Y���.��[^t��3_"���.�|�Q���l]
	�s�9�tmk띡�#�y�
�<cPFU���ߛ0�����G��_��1i�,l��*�w��Z]�Ħ�C/)uE��M<-��|��ї�M�0�Ť��h��_B`/��#���Luo�[;"P:�nݑ��ޒ��A��UD�Z}�O�9e�4Z������\�J�}lvQ���i����((B5�J��E���1��Ϻ[���E$G�В:*,��Ԧ13o.7��P��d� �mr�)�}V��Z�i���E�cW-��]΍ɠ���'�hY����}�d����t��y]R��dR���['V嚬�"�N�b�r��_�;���P��H�E��F��z[x��0�/��|�ޣ-伬�%P�gq�B�."Mc=k�+o6�5�U�M]�Ye�ކ� ���H;���V?�E�EW�,Oe�8�B����^�OU�8�Q
yݐ���-Ե*-���-�(�/�P��A�C�D�xl�hrO�]�|���T�
���e���a�V��0�S$E39�s�_cyg�G��l����d�R搪5B��H³yo�km�C�K�E��i����rUb��P�n�aD��t�KKB��Lj�ߤ/H����+G2$M0׷�_��\i3?
�E6�9<3�6�:�'�G�`+K�^V��e��s���:Ѳ<����rUm*XN�h�*�_�왟(OQ,G7hŅ.6�|'ڨ��Hg37)^�vF�ø�5}H�A�r'��o ^U9�,����x����(�7�M�ey6�E�y|���|�p���8՗V��$�+�6qs��٘F7ZF��G7�>���}��h<��(M�gd�cͅt������q\1�ٓZ�3b���k�Y�j���-jG�1���+Z��2�6R�p$w��?8�Y��O��ӪN�B�N��KMY�B���^�D�YAc˧ǥ\m���cw��B�G�-�F��|�K�=±�����<�=��z	�a�}��;�Q���j��9�F�o�@ܮuT�r�n��R���7�و�(�L-K@��zq�	j���� �sb�D/���譿��s��M��*N�-���/�j��K�Պ�v�L�z�Q6���R��Ѳ���v�.,�/&���L�?�)�g9��r~3]�9p�7�t gJ���H~�/wr�Pr��93��j��d��7+։% 2<�@D�o�ь������ǁ��T�w��ͮ��O��N�7����    h�w��R��ԾQ�?�/���-��Kf���D.,��8�@�S<��8�0�����-�Kc�G,'�a�>�x��%|58(9�5쒌��,�	P�QxE�"�����@�q��$�)�R���jS����<e��A�u׼%%��-m��{����f
�p#�����u���4�"��S�F<s�����y����	�]�fN(ŷ&��M�����F����沽VF˰ԡJ��w�"�W���g��S��c���f}��#�����+�4�LJ�^��y�l1�P�;t���7�ԙ��X-�������~��ڋgXd� �F�,�>m��j�����$@r�/%I)(]�e�NW���}��\�0[�
�|� ��zj�6�|���?5S�ˉ�+d���iV��^��V�4�]�y�8CuGV�ۂ"'0;������ؚԇ�=0���y��lj���z�.��%CO[���	�i�rn�Y�3|j�{�;����s$t#/��E�y�J�)���I��)�k�����@nt�:�yy�܏��qO�pOC��@��C�;�����,����"�-1Ig�.-q\��$��Ł7�'N��p�2�^�|������i�}��������-���P,�ѱ:S�#׶6��k�b��eg9�ɺ8�M�������f��LKK���<�%s|�h�K�A��].�s���i@P48����u�80K�e��*w>�g��3�猩j�zK7ꅀ^缦��q	nW���$3D�M�J��L�����H��˫�
k3W�^���V��;rY%]u��Ԭ.�Fo�kz_x�E�K�A��zA����RX�?EI
'�q9��M�a/X�v-ojamk���Sf����S��k�7�m�;�װw�}a��t"X��D�gxg/�<W4�;�A�Vﲊf�~�1�	�û��3P�4��	�>��������fA�"= �ZO�i��'v��q#�;�H+�T[E/=�����X3dR��Mr�ޟ�,O��p=>��2�dځ���ήJ��3���X�Ugh��Q*sM���]�|�[�z�A��2��3�%��/#4I1,s����57Sv�4@#�
<i�Lԇ{� �J�CVs�^�H.|�8�x��4�� ��>_|���K'��+���g����r��Jm��
��p����X��V�Zj#����E�Z�G�� sធ&M�|�nr��V�����Tq	�OFQ�wD�2��z��"A� �����ތw<o^�/]�<I��B�D!��	i�f��<�����A�����vh�*ӑV�E�u��^�,{9#yE�p�E��6�P�-0:a������)�r�;�I��P�Ak$� t1�%�g�8|�$7��/�����P]�hG��A���r�����]�(����L�Ѡ0�!F�djY=��D�OPR���YfX�b�`FFo�%C���NP64N�^p�xE���6a6�����K��˃i�*Э�����h���9�4��<z�A��/��E>!�Β��i����}��������~��o�UI�ǀh�К�*��OM����6JO3g�	�`
j!5��PKD�B�eKW�So6���`��upi�\�v�nq�+*Mq	�k�i<�ʡ�awd�B4.4(v�M��ʉ գ5�$�oЬG�p!G��[��F g Aӷ��4{�p����X�e��T��;���B!��P튫��`# F�Q
 6������)���ye��K���k�E�H�t�j��+	�B����.���A1,�%��7V��5�6����>��~9�	�d�F��C�H�����{�#O/a�NP�f�&hsПRβ9��}vSY/�$jso��:�j�ԡ��������W4�$4$�tC2����K���h�d2����$��J5�vm���ޮ�7�d��*9S�вԹʍT�1��Pb?1A�LB��H7*�o*��[e9�I,�����\����^�W��RAh渂$�+�6.3�2+��ן�K�%h�K��K��\=�$�3W]�@_�\T}�؀�4�R����q��rgɱ��:_Ϩ!�{��ٙ!�7^2	�M"{;ý�'(z
�Fd�ְ���
'�4�Xb��g�~��� �f��Q��{��u�he>Vw���f��)M�Fo��F�T����}�
��V
�^k���R�[�b��E�ѲPoe���Mr?�?{�{��r�����	MQ6�B�=�M�ݩ����$ɶ�1(LF;�Vs{.t:�a�͑���)����hP������ɏ�s��{Gkt�<���+y	C]p�}�M��'̾��N���n�d�W�֦Qj�W�P��n��%����w2�~m�hP� ���6��?<x�k� �8]t-C�2�e16�c|N�����NGf���!�f�R�Ys��wq���S���*cq����(_P�]�����؏�H�AC������!׺Tň��w���Y���X�H�Q��0QJBʤ�h�[�a�D{�7��ghb���|�Z��>����:���2}��#�2��AꞺ*Fy��(,�`D�{���ݷl9���S@�q�2z�ܼ�X���;���؜NA��@�m��Im�Xtj���-�Fx�m��_�v���9���Xh͑}���5#	4�A�i���8�c����.��"z�����$��\l&?ZXBh�	�h
6*��� ߙ�iwE���@uBYi���cq��h��]�"t@��P����r��te?�"d-ĩ����I�s�雛H�_)5���F.r�9�*�GŞ[��7#jA\�ğ���_�W�����@�燇�v�ɫE~�-�*���r^��������3�D���>{D��Ģ8��~�[��:����0�xC�?T�"bP�x��l�C��r0�@�eT'ʸ_&Ky��%O���!7���ًǧ/�!ݠG�C�݇&3�]���.�) b�b��ϜY�W�sP�)%��;��r��V��H���p�/�
?{�g�O�A�����k�����kvc�����[�W�?[�y�G�6��hA6(�6����I�v����ꏁ����d2�"A�p	E��G<&N�]�y��B��{�~ņ�.��Y�B���\R֣��jU�e�ȕJ�vK(i�γ�0|
��A��iO���	�*�z�*�|=�=ʼF�&�#�I�#��|�*���liaط����o��]tYi1R��3��o�N�A��M��	f��wJ�~2�V�7��M0LWZ�B��uj��pf$8n�Gh����B�S��93�$��4�4^�4�5�S�JU�r��O�Q���mY�Y8�cCk�x	�I��]�p���zQvն>��j��>l��%�Bɢ��caOG��7�H�A��!I���Ԧ�]�(�噣��_�G�Q&o��pt�z��eU�%���+|\�a��~9q�#TP�FG��(�x{:��s�K�
�Z:ѩ{��/{ϕ@IfU#��Fn�դ1�4�U��/FGt��:[4����T�o�L���,�H��d��+��z	��|�����"��6⋎������lK��A]?�i�3�b��7Z�wF(d@= ��2��N�k�M�'V({�ԃ���q8�»-���Cs2�ޙw���&#��35��GI#Ɩ�u��w���Aj!Z�0��%d��n�eɤ�Sq8]g���ݎ��ڣ�8��d�9(�f1����7��us6��%/�:xOջ��BT(�|��]�_<�䉊|�փ8 oE���]8�r�K�l�۵�yz��2�;��GC�Fo��z!i�� ��fO�UJ����0#ӏ�#�я�qnx��Κ��s�-V*�"2���9(}n�ݞ/�я@���N�C}v"'���/C5?�H�g��2�:��Wm)���̟&�Z?�h�`B����`:�^��~�����Ld���2� �9�1�]�	*�p�"~h�+���$ �uq{s��;2Z��=Z�G��Z��v�e�Kfgv<���.u�|2P ���%�t�g���d˃1�+�f�
�F�$l�>�0\��x���k    ��߈&,0��z��4��;ZRWҰ�@ �9�=�,�q�\/<�X���b�(h���Q�}j*�O�A���b��9�1�@�zV뮘ZWv�`8}�佑u����\+��]~z���''� �-T�ʉ *S��7��iO6��3VW�%vh[������0M�SC�^ˠ��G�C�LT��z���؇�^8�2��+��1�A�w�~Dc��ڞ
a����j1ka-��+$E��ݢ�F�G�?xG��gD40�z�
��0�K�^������blc^k��mm���E#�*���f�Z7���ª��w\n��eL3Y$�x�z��r�z�s�L����s,`%�0A@mS$�B��~�����O0�Y��o1^0�v����VJG�vk9꾣,��e�d@Y�|�.2v��rs��c�|V&�,8vp7��lFj��v�G3��(�9������3ffs�-{�wS��W`�����Dv��d����H����|p3�$os�Y� q_�B{�?�&�:�ְ��BK�Λ`AJ毝�(�J�DFԈ�A�s�����L��F��NQ�IИ}8r��0^�DR�"6��[0�Q�B9�4�Ѵ�]�5f9�@$"��0'�[����8�30LbE�29F�#��v�ά�:g����	׫5��f/4&�q��8ȯT$�(���2�(�L������s���ǹ����D�>A�&�щ��������٨9��41���>s�$~A���D���&���r�~ѓq�(μ0vE>i�棣���Ҏ�tAß�j>�T⩵�ω���;n*$ `�d@E����	�U7NC������L1�����xQ�����:J0[�=�Z6��.�NO�Hb���������{Jq^3P�&��$W��X}�����R�wNB��;�?G��R;��rѡi�����Scw�׵٩Q����u\�$�N4������$`�Lr2��;s�I���ZBx�ߎ(���qE�s�a��	Ѭn\�H�9y<�؁��7�,1�(J|Ń�(
 �@r|7�[������z��,�1y��	yQ��Y���tn�:�j��;��#�������k�X���;q4ↄ	��9��\m�v?���	��@	7��	:~9�:p,��uV�����K>
������{�~��W[q�d�������9� @]� 	]�d��E)X#�蕭t�.+������u7���6�ĩH����}>&;aKMXn�'�=mw�A�//��ez��/�#2%��y�(�JxO��	�K/���<��T�A^��z�;��2ҁ�-y����7n�>��k�t��{�ӆ�NR�]���M�Z�BN�#�� �)��(�k�X�k���S6-�x�c�PA4�����.m\-�`����$Ǉ:'P*�a���ԬA�l��VwL4_܅#�i!�Cd�R8d��RipīWLu����+˲xq����.� ���Z���F8��̪<�ƚ0O�V?�jy{L�Mf��x�,&�"��Ԕ8SJ:ST�aޒֳ4��^V�s�s
ʎg�^��{8�i�cH��2���l'�D�K:�`�����ĸ�$9�X>�tvm��*!M�$T:��0t�~�¼�#��-���˔�]�ږ2D���ʁ|����Ռ�(ʚj��YiWk� Z՚(g�|�Y��3jY�_SҲM׿���3I���ɇ"�0/eZ=A��W�g>�Ƶh�����01�B?3s}:�u�F�'�<?�_��&gn�Ȯ2� �3�	�g�u��թ$�)?Q�;�y�gܴ����!�#([��m�[�g�k�{X}��*�"���3��]I� y�#�k����]��Q�̌E\�d8A|��U�̬��\��= (���Q�?���Sj$��T~.�ec�r@���@�L���,���j�F��4ٴu�Ԡ�Rfb���{�Ӑ��H��<�UL��IR� �ǯ��f�	�J]�K�/����`�������������O����V�W>��pd�Cc<p�]����H��_N;������/�������Ln����,�>��V�3�x�z,��,W���{�_��{��a��12���b(ֶRk���b�4�����uk�筷"��b#`�9vHD_}�P�M��*��`*��AgA�ȼ�ri�j֗˜��7��������Oҋ�@,"^y�vtyʆ��jZ��k�4M��1=����>�yFS��Ѥ�7�՚���8���Cc]K��j��Y���ի��d���.�xGR$x]��jU��Y)���w�� �Q�KJ���FjW������_��)���dJ����O���_l�6���f9�D�ץ
�o�� �_}�#߼�R=<��ֵ����	5I�d�e��)Z����O��F�n�;W��ߪ��࣎��{�A���|�O(x���e���y�ެ��ʵ��>	�d�_h���S!���n%�]%6\>�JF���-68cQ����8|�_���W��ɕ�+�i&o�o }�M�?n�f�÷����ة�)���+���8��	35��Z7���o��gQO�[$_�[��t�ů }������g�㮂��t{)uj�qÏt�.�d����{�o(�fo�*}�F��o��2H�'n��q��;�V�Gam����z�B�p����&�� �l���x�'��NLt�Z�a#n��c$R��.�X�)~ֻ��^�_��F&o�6�2`x�T�eN%+��)��%���Ԋ�8g¸��Y�� ş����䚿q������Ư��
X^�zٲ���%��I1�a� ���)��Z�R�CUL}բ��eU��}�4�p�ܑ��
㹂'\����r�!�܂z�=r:��?8�v�qx�2|��2ڌ�7��D�A���6�.�o���20�S˖9����nуE�4�$��X�g5ax�;�<i�Zٗ�m����3S��K�j4��@Y�!�͏�t��lwI���QNW�337���2`fಲe.�o�N��gLPqR�����q
/F�C�÷A�Nۭ����ee4��b;x5q��El�*�H�������!70w�u��3�<@��??D3vT1%��|-ʏ�{;��f��*%Z?��v��g��,	�7�{���~���<�7���K!�c��2�_+U�O�DH���K��=�Uy<3�e�������ʜɀ�X�/S��{��t;7����w�LӾ%���Z�<�$_��0� J0�b0�^��Jk˶'!qN��4���p����W��d��w����T�ɀ��{˖����`!��N��i��i�p�cJ$���)��j<]�갗��O��o�����0��!xKq��oEE��q��7� �^�!EߝKZȀ)�Kʖ��b˨"ԮP�;�CH�	\����N�g������[*���hJbӜW��wݤ��
�:c�v�j�ŋ�O�v�MF�U�������|?�W-0ba�PV���O����v!H#)��>��8�6��R���?�u�іn�:Y�`AO綉����D=��;F`������ùRj����3؜�-�y0RSq�[�a凩W;?��r���1��<Ф�2n��!�!�9���Ψ�xK���l�U)�f_4�L;�g�84���W_a<���*%��˲=ȍ��*�)�	_���籯ϒ��)�f�k�֐%�#�6Y&��!���N�!7�Wt	����a����(�o/�h�|��Õy1��`�Π:����V�!�e�&)���Y��$4�{O<ٰ�0�!�"5�����?2n>I(l�~���9U&p��d0��=X�@S��۠6G�ޞYNT�QE"t�ċ'�,Z����0�R�3^q���B��20,5��S����3U64�~�7������Ȓ���%eEcKP�~���������zO��ё��us�w��\;�'��5J�Bk��Ed� ��VV+`ͭ�h�Yv#'�^��}�9��'.��z��'y���pxP�|eI׫����U`/� bk�b}]�g��L�Y#�Hc�tC֣圚.乐�/�%����{��� ��jkw�F�{� �  ��e�'��a�{!�󇡓��uM��->Fc���9Q�|,#�2c7�[L�S��uH^�������zL�sCQó�ƺSc�|�|��γ0��̬�̴�ᵨru+��*�c<F3�},8c�_�Ls�\��I[8��$q��ɱ��D$FTy��Vin�;S�Ϸ��J�֡�%�Q����/&`�`(��9�u��d_ZZ���p-�o�s��1�� w��X�W�����7 g�Ô�;�Q�c���Vٜ����s�n��MY��^?���W�����h�:ɢ�(0��1�X^h ��s�\T�ӍA��so�z=r����f���Z3��ycq¬��y-�c�+�7뉱e��G���SZbŨed6wa��+�'q��J.��^�i����F�n����sG��Ě�l9w�6%G=��۾4M\t<��|ڈ����K+�]ߔ���1U&N�/��E�z?�a3�*����"2���.v�� Gmm6����P�kl�&�͎Ȁv S����gV?��K����M��ɋ�#f ����	��w����D�ء�<�6�a}��f�Pϯ�2_T��20���1�=1���'�)`5�>�؁������n�$#Gُ�A詑���40,\WնHv^;V�L��I�؁�ѕ��P���^|rJ��9��j�=mW���"�Mn��ȟ����#����H��3-�k4&��~�d��lVZ���e�/�?�Y��7�����}d�->����6�z�z�Ԗ[�<K�!��W�h��n�w��Ш'"Z�TZ��[R�v���kw�=����<���A�Zy�f� S��B׬�C�΁�,���P7���ֹ5}�콆�r��JÁȀ��*-���[�`��R���G�9�Q=޲β_W�o�5��N6e���Sn�n��x�
3�hB"��̺xi��/H�����[�~s����3|Q���YDw�&�X3�bz��yN8[�x �{)v\l�p����|��k����?h�L�Ad�rY���H�*V+WѲ�Ȟ�u�����pFT��օ���~����?��Ͽ��'�4�b�)���/}	�j�/G}�ln�s���b�U��#�lA$?6���;}I���_12�KQ)Cɗ����֠���ؠ��k"i4����fZ�?A�R���= �`�����Ol��:
vǭo�w�T�j8�ru7Lz�z�=�J�Q&�g�'�u'r`B��oVN1�ܕx��'���U�M�Ɛ�Bص��N��q��>ě0��	���h1ލ��X{y���Ȳf�d�hp����ݳ4���	�"2s��O�rm�*I��&Ϣ<AP�-�����VI������I�S<���Y�i��D������y�H�X�Aw�>L\(E�/�u;�J�O��/4��D	�S�����"�G��M�k����|/b�B_k>n�^��ۃ�q�M�H޲����`Ѻވ��:��._��E�ٽ����"a���䓽&�a
r����7;(�p6��0N���^m�M���{s���4{h��쇣ڄ>�V���3H�)"�c���<(9�?KS�+~V�����P#1��y�ǁi�f)���<��^�E��l����ծ	Cl���[�[����&�Ѯ�i���y���Fp�Xe	=k�Ȁ����j����R������i�QN�G��t���������@8�>�'�tv���$���{�#�pE��//s7*Y[�9� �zue�G�����wN�3O�F�ۓȯ�n�a�?�_�y�F�Ad>G�v�.o�!�@ݸ�@�&��R��r��j������j�,��!v�x0<Y
�qľ��w�-і���.��y4��i���R^p�"��^��+stfK�aC����h껕��=�eZ�a��O���2��L���lR�a�{���3�|C�K�)b �^��u-�xG����]�)��.=r��4��Es�7�ᒝw��x�0�ջ��˛��ȫ�P���2�������>���      �      x������ � �     