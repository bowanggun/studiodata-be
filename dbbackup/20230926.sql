PGDMP     0                    {         
   datindrive    15.4    15.4 �    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    16398 
   datindrive    DATABASE     �   CREATE DATABASE datindrive WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'English_Indonesia.1252';
    DROP DATABASE datindrive;
                postgres    false                        2615    16399    v1    SCHEMA        CREATE SCHEMA v1;
    DROP SCHEMA v1;
                postgres    false            �            1259    16400    failed_jobs    TABLE     "  CREATE TABLE v1.failed_jobs (
    id bigint NOT NULL,
    uuid character varying(255) NOT NULL,
    connection text NOT NULL,
    queue text NOT NULL,
    payload text NOT NULL,
    exception text NOT NULL,
    failed_at timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);
    DROP TABLE v1.failed_jobs;
       v1         heap    postgres    false    6            �            1259    16406    failed_jobs_id_seq    SEQUENCE     w   CREATE SEQUENCE v1.failed_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE v1.failed_jobs_id_seq;
       v1          postgres    false    6    215            �           0    0    failed_jobs_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE v1.failed_jobs_id_seq OWNED BY v1.failed_jobs.id;
          v1          postgres    false    216            �            1259    16407 
   migrations    TABLE     �   CREATE TABLE v1.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);
    DROP TABLE v1.migrations;
       v1         heap    postgres    false    6            �            1259    16410    migrations_id_seq    SEQUENCE     �   CREATE SEQUENCE v1.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE v1.migrations_id_seq;
       v1          postgres    false    217    6            �           0    0    migrations_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE v1.migrations_id_seq OWNED BY v1.migrations.id;
          v1          postgres    false    218            �            1259    16411    mst_collection    TABLE     �  CREATE TABLE v1.mst_collection (
    id bigint NOT NULL,
    nama_collection text NOT NULL,
    judul text,
    tipe character varying(50),
    default_chart character varying(50),
    referensi_data text,
    catatan text,
    route_name text,
    table_name text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    ref_organisasi_id integer,
    organisasi text,
    satuan character varying(100),
    definisi text,
    rumus_perhitungan text,
    cara_memperoleh_data text,
    urusan text,
    is_multidimensi boolean DEFAULT false NOT NULL
);
    DROP TABLE v1.mst_collection;
       v1         heap    postgres    false    6            �            1259    16416    mst_collection_id_seq    SEQUENCE     z   CREATE SEQUENCE v1.mst_collection_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE v1.mst_collection_id_seq;
       v1          postgres    false    219    6            �           0    0    mst_collection_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE v1.mst_collection_id_seq OWNED BY v1.mst_collection.id;
          v1          postgres    false    220            �            1259    16417    mst_dashboard    TABLE     �  CREATE TABLE v1.mst_dashboard (
    id bigint NOT NULL,
    nama_dashboard text NOT NULL,
    frame text NOT NULL,
    link text NOT NULL,
    catatan text,
    is_hidden boolean DEFAULT false NOT NULL,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.mst_dashboard;
       v1         heap    postgres    false    6            �            1259    16423    mst_dashboard_id_seq    SEQUENCE     y   CREATE SEQUENCE v1.mst_dashboard_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE v1.mst_dashboard_id_seq;
       v1          postgres    false    6    221            �           0    0    mst_dashboard_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE v1.mst_dashboard_id_seq OWNED BY v1.mst_dashboard.id;
          v1          postgres    false    222            �            1259    16424    mst_jenis_dok    TABLE     [  CREATE TABLE v1.mst_jenis_dok (
    id bigint NOT NULL,
    jenis_dokumen character varying(200) NOT NULL,
    ref_grup_dok_id integer,
    keterangan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.mst_jenis_dok;
       v1         heap    postgres    false    6            �            1259    16429    mst_jenis_dok_id_seq    SEQUENCE     y   CREATE SEQUENCE v1.mst_jenis_dok_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE v1.mst_jenis_dok_id_seq;
       v1          postgres    false    223    6            �           0    0    mst_jenis_dok_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE v1.mst_jenis_dok_id_seq OWNED BY v1.mst_jenis_dok.id;
          v1          postgres    false    224            �            1259    16430    password_reset_tokens    TABLE     �   CREATE TABLE v1.password_reset_tokens (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);
 %   DROP TABLE v1.password_reset_tokens;
       v1         heap    postgres    false    6            �            1259    16435    personal_access_tokens    TABLE     �  CREATE TABLE v1.personal_access_tokens (
    id bigint NOT NULL,
    tokenable_type character varying(255) NOT NULL,
    tokenable_id bigint NOT NULL,
    name character varying(255) NOT NULL,
    token character varying(64) NOT NULL,
    abilities text,
    last_used_at timestamp(0) without time zone,
    expires_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
 &   DROP TABLE v1.personal_access_tokens;
       v1         heap    postgres    false    6            �            1259    16440    personal_access_tokens_id_seq    SEQUENCE     �   CREATE SEQUENCE v1.personal_access_tokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE v1.personal_access_tokens_id_seq;
       v1          postgres    false    226    6            �           0    0    personal_access_tokens_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE v1.personal_access_tokens_id_seq OWNED BY v1.personal_access_tokens.id;
          v1          postgres    false    227            �            1259    16441    ref_data_tag    TABLE     &  CREATE TABLE v1.ref_data_tag (
    id bigint NOT NULL,
    data_tag text NOT NULL,
    keterangan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.ref_data_tag;
       v1         heap    postgres    false    6            �            1259    16446    ref_data_tag_id_seq    SEQUENCE     x   CREATE SEQUENCE v1.ref_data_tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE v1.ref_data_tag_id_seq;
       v1          postgres    false    228    6            �           0    0    ref_data_tag_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE v1.ref_data_tag_id_seq OWNED BY v1.ref_data_tag.id;
          v1          postgres    false    229            �            1259    16447    ref_grup_dok    TABLE     <  CREATE TABLE v1.ref_grup_dok (
    id bigint NOT NULL,
    grup_dokumen character varying(200) NOT NULL,
    keterangan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.ref_grup_dok;
       v1         heap    postgres    false    6            �            1259    16452    ref_grup_dok_id_seq    SEQUENCE     x   CREATE SEQUENCE v1.ref_grup_dok_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE v1.ref_grup_dok_id_seq;
       v1          postgres    false    6    230            �           0    0    ref_grup_dok_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE v1.ref_grup_dok_id_seq OWNED BY v1.ref_grup_dok.id;
          v1          postgres    false    231            �            1259    24617    ref_kode_wilayah    TABLE     �  CREATE TABLE v1.ref_kode_wilayah (
    id bigint NOT NULL,
    kemendagri_provinsi_kode character varying(20),
    kemendagri_kota_kode character varying(50),
    kemendagri_provinsi_nama character varying(50),
    kemendagri_kota_nama text NOT NULL,
    bps_provinsi_kode character varying(50),
    bps_kota_kode character varying(50),
    bps_provinsi_nama character varying(50),
    bps_kota_nama text NOT NULL,
    latitude text,
    longitude text,
    kode_pos character varying(200),
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
     DROP TABLE v1.ref_kode_wilayah;
       v1         heap    postgres    false    6            �            1259    24616    ref_kode_wilayah_id_seq    SEQUENCE     |   CREATE SEQUENCE v1.ref_kode_wilayah_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE v1.ref_kode_wilayah_id_seq;
       v1          postgres    false    6    253            �           0    0    ref_kode_wilayah_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE v1.ref_kode_wilayah_id_seq OWNED BY v1.ref_kode_wilayah.id;
          v1          postgres    false    252            �            1259    16453    ref_organisasi    TABLE     P  CREATE TABLE v1.ref_organisasi (
    id bigint NOT NULL,
    organisasi character varying(200) NOT NULL,
    singkatan text,
    keterangan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.ref_organisasi;
       v1         heap    postgres    false    6            �            1259    16458    ref_organisasi_id_seq    SEQUENCE     z   CREATE SEQUENCE v1.ref_organisasi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE v1.ref_organisasi_id_seq;
       v1          postgres    false    232    6            �           0    0    ref_organisasi_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE v1.ref_organisasi_id_seq OWNED BY v1.ref_organisasi.id;
          v1          postgres    false    233            �            1259    16459 	   ref_topik    TABLE     2  CREATE TABLE v1.ref_topik (
    id bigint NOT NULL,
    topik character varying(200) NOT NULL,
    keterangan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.ref_topik;
       v1         heap    postgres    false    6            �            1259    16464    ref_topik_id_seq    SEQUENCE     u   CREATE SEQUENCE v1.ref_topik_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE v1.ref_topik_id_seq;
       v1          postgres    false    6    234            �           0    0    ref_topik_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE v1.ref_topik_id_seq OWNED BY v1.ref_topik.id;
          v1          postgres    false    235            �            1259    24601    trx_collection    TABLE       CREATE TABLE v1.trx_collection (
    id bigint NOT NULL,
    mst_collection_id integer,
    tahun integer NOT NULL,
    target text,
    realisasi text,
    sumber text,
    catatan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    kemendagri_kota_kode character varying(50) DEFAULT '32.71'::character varying NOT NULL,
    kemendagri_kota_nama text DEFAULT 'KOTA BOGOR'::text NOT NULL
);
    DROP TABLE v1.trx_collection;
       v1         heap    postgres    false    6            �            1259    24600    trx_collection_id_seq    SEQUENCE     z   CREATE SEQUENCE v1.trx_collection_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE v1.trx_collection_id_seq;
       v1          postgres    false    6    251            �           0    0    trx_collection_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE v1.trx_collection_id_seq OWNED BY v1.trx_collection.id;
          v1          postgres    false    250            �            1259    16465    trx_data    TABLE     4  CREATE TABLE v1.trx_data (
    id bigint NOT NULL,
    mst_jenis_dok_id integer,
    sumber_data text NOT NULL,
    jenis_data character varying(100) NOT NULL,
    nama_data text NOT NULL,
    deskripsi_data text NOT NULL,
    format_data character varying(100) NOT NULL,
    media_type character varying(100),
    size_data character varying(100),
    ekstensi_data character varying(100),
    nama_file text,
    url_file_upload text,
    url_upload text,
    is_shared boolean DEFAULT false NOT NULL,
    webbappeda_visible boolean DEFAULT false NOT NULL,
    satudata_visible boolean DEFAULT false NOT NULL,
    catatan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.trx_data;
       v1         heap    postgres    false    6            �            1259    16473    trx_data_id_seq    SEQUENCE     t   CREATE SEQUENCE v1.trx_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE v1.trx_data_id_seq;
       v1          postgres    false    6    236            �           0    0    trx_data_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE v1.trx_data_id_seq OWNED BY v1.trx_data.id;
          v1          postgres    false    237            �            1259    16474    trx_data_organisasi    TABLE     R  CREATE TABLE v1.trx_data_organisasi (
    id bigint NOT NULL,
    trx_data_id integer,
    ref_organisasi_id integer,
    organisasi text NOT NULL,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
 #   DROP TABLE v1.trx_data_organisasi;
       v1         heap    postgres    false    6            �            1259    16479    trx_data_organisasi_id_seq    SEQUENCE        CREATE SEQUENCE v1.trx_data_organisasi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE v1.trx_data_organisasi_id_seq;
       v1          postgres    false    6    238            �           0    0    trx_data_organisasi_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE v1.trx_data_organisasi_id_seq OWNED BY v1.trx_data_organisasi.id;
          v1          postgres    false    239            �            1259    16480    trx_data_tag    TABLE     G  CREATE TABLE v1.trx_data_tag (
    id bigint NOT NULL,
    trx_data_id integer,
    ref_data_tag_id integer,
    data_tag text NOT NULL,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.trx_data_tag;
       v1         heap    postgres    false    6            �            1259    16485    trx_data_tag_id_seq    SEQUENCE     x   CREATE SEQUENCE v1.trx_data_tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE v1.trx_data_tag_id_seq;
       v1          postgres    false    6    240            �           0    0    trx_data_tag_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE v1.trx_data_tag_id_seq OWNED BY v1.trx_data_tag.id;
          v1          postgres    false    241            �            1259    16486    trx_data_topik    TABLE     C  CREATE TABLE v1.trx_data_topik (
    id bigint NOT NULL,
    trx_data_id integer,
    ref_topik_id integer,
    topik text NOT NULL,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.trx_data_topik;
       v1         heap    postgres    false    6            �            1259    16491    trx_data_topik_id_seq    SEQUENCE     z   CREATE SEQUENCE v1.trx_data_topik_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE v1.trx_data_topik_id_seq;
       v1          postgres    false    6    242            �           0    0    trx_data_topik_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE v1.trx_data_topik_id_seq OWNED BY v1.trx_data_topik.id;
          v1          postgres    false    243            �            1259    16492 $   trx_luas_wilayah_kecamatan_kelurahan    TABLE       CREATE TABLE v1.trx_luas_wilayah_kecamatan_kelurahan (
    id bigint NOT NULL,
    mst_collection_id integer NOT NULL,
    tahun integer NOT NULL,
    kecamatan character varying(50) NOT NULL,
    jml_kelurahan integer NOT NULL,
    luas_wilayah double precision NOT NULL,
    satuan character varying(50),
    catatan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
 4   DROP TABLE v1.trx_luas_wilayah_kecamatan_kelurahan;
       v1         heap    postgres    false    6            �            1259    16497 +   trx_luas_wilayah_kecamatan_kelurahan_id_seq    SEQUENCE     �   CREATE SEQUENCE v1.trx_luas_wilayah_kecamatan_kelurahan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 >   DROP SEQUENCE v1.trx_luas_wilayah_kecamatan_kelurahan_id_seq;
       v1          postgres    false    244    6            �           0    0 +   trx_luas_wilayah_kecamatan_kelurahan_id_seq    SEQUENCE OWNED BY     s   ALTER SEQUENCE v1.trx_luas_wilayah_kecamatan_kelurahan_id_seq OWNED BY v1.trx_luas_wilayah_kecamatan_kelurahan.id;
          v1          postgres    false    245            �            1259    16498    trx_riwayat_data    TABLE     U  CREATE TABLE v1.trx_riwayat_data (
    id bigint NOT NULL,
    trx_data_id integer,
    mst_jenis_dok_id integer,
    sumber_data text NOT NULL,
    jenis_data character varying(100) NOT NULL,
    nama_data text NOT NULL,
    deskripsi_data text NOT NULL,
    format_data character varying(100) NOT NULL,
    media_type character varying(100),
    size_data character varying(100),
    ekstensi_data character varying(100),
    nama_file text,
    url_file_upload text,
    url_upload text,
    is_shared boolean DEFAULT false NOT NULL,
    webbappeda_visible boolean DEFAULT false NOT NULL,
    satudata_visible boolean DEFAULT false NOT NULL,
    catatan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
     DROP TABLE v1.trx_riwayat_data;
       v1         heap    postgres    false    6            �            1259    16506    trx_riwayat_data_id_seq    SEQUENCE     |   CREATE SEQUENCE v1.trx_riwayat_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE v1.trx_riwayat_data_id_seq;
       v1          postgres    false    6    246            �           0    0    trx_riwayat_data_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE v1.trx_riwayat_data_id_seq OWNED BY v1.trx_riwayat_data.id;
          v1          postgres    false    247            �            1259    16507    users    TABLE     t  CREATE TABLE v1.users (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    email_verified_at timestamp(0) without time zone,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.users;
       v1         heap    postgres    false    6            �            1259    16512    users_id_seq    SEQUENCE     q   CREATE SEQUENCE v1.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
    DROP SEQUENCE v1.users_id_seq;
       v1          postgres    false    248    6            �           0    0    users_id_seq    SEQUENCE OWNED BY     5   ALTER SEQUENCE v1.users_id_seq OWNED BY v1.users.id;
          v1          postgres    false    249            �           2604    16513    failed_jobs id    DEFAULT     h   ALTER TABLE ONLY v1.failed_jobs ALTER COLUMN id SET DEFAULT nextval('v1.failed_jobs_id_seq'::regclass);
 9   ALTER TABLE v1.failed_jobs ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    216    215            �           2604    16514    migrations id    DEFAULT     f   ALTER TABLE ONLY v1.migrations ALTER COLUMN id SET DEFAULT nextval('v1.migrations_id_seq'::regclass);
 8   ALTER TABLE v1.migrations ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    218    217            �           2604    16515    mst_collection id    DEFAULT     n   ALTER TABLE ONLY v1.mst_collection ALTER COLUMN id SET DEFAULT nextval('v1.mst_collection_id_seq'::regclass);
 <   ALTER TABLE v1.mst_collection ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    220    219            �           2604    16516    mst_dashboard id    DEFAULT     l   ALTER TABLE ONLY v1.mst_dashboard ALTER COLUMN id SET DEFAULT nextval('v1.mst_dashboard_id_seq'::regclass);
 ;   ALTER TABLE v1.mst_dashboard ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    222    221            �           2604    16517    mst_jenis_dok id    DEFAULT     l   ALTER TABLE ONLY v1.mst_jenis_dok ALTER COLUMN id SET DEFAULT nextval('v1.mst_jenis_dok_id_seq'::regclass);
 ;   ALTER TABLE v1.mst_jenis_dok ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    224    223            �           2604    16518    personal_access_tokens id    DEFAULT     ~   ALTER TABLE ONLY v1.personal_access_tokens ALTER COLUMN id SET DEFAULT nextval('v1.personal_access_tokens_id_seq'::regclass);
 D   ALTER TABLE v1.personal_access_tokens ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    227    226            �           2604    16519    ref_data_tag id    DEFAULT     j   ALTER TABLE ONLY v1.ref_data_tag ALTER COLUMN id SET DEFAULT nextval('v1.ref_data_tag_id_seq'::regclass);
 :   ALTER TABLE v1.ref_data_tag ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    229    228            �           2604    16520    ref_grup_dok id    DEFAULT     j   ALTER TABLE ONLY v1.ref_grup_dok ALTER COLUMN id SET DEFAULT nextval('v1.ref_grup_dok_id_seq'::regclass);
 :   ALTER TABLE v1.ref_grup_dok ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    231    230            �           2604    24620    ref_kode_wilayah id    DEFAULT     r   ALTER TABLE ONLY v1.ref_kode_wilayah ALTER COLUMN id SET DEFAULT nextval('v1.ref_kode_wilayah_id_seq'::regclass);
 >   ALTER TABLE v1.ref_kode_wilayah ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    252    253    253            �           2604    16521    ref_organisasi id    DEFAULT     n   ALTER TABLE ONLY v1.ref_organisasi ALTER COLUMN id SET DEFAULT nextval('v1.ref_organisasi_id_seq'::regclass);
 <   ALTER TABLE v1.ref_organisasi ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    233    232            �           2604    16522    ref_topik id    DEFAULT     d   ALTER TABLE ONLY v1.ref_topik ALTER COLUMN id SET DEFAULT nextval('v1.ref_topik_id_seq'::regclass);
 7   ALTER TABLE v1.ref_topik ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    235    234            �           2604    24604    trx_collection id    DEFAULT     n   ALTER TABLE ONLY v1.trx_collection ALTER COLUMN id SET DEFAULT nextval('v1.trx_collection_id_seq'::regclass);
 <   ALTER TABLE v1.trx_collection ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    250    251    251            �           2604    16523    trx_data id    DEFAULT     b   ALTER TABLE ONLY v1.trx_data ALTER COLUMN id SET DEFAULT nextval('v1.trx_data_id_seq'::regclass);
 6   ALTER TABLE v1.trx_data ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    237    236            �           2604    16524    trx_data_organisasi id    DEFAULT     x   ALTER TABLE ONLY v1.trx_data_organisasi ALTER COLUMN id SET DEFAULT nextval('v1.trx_data_organisasi_id_seq'::regclass);
 A   ALTER TABLE v1.trx_data_organisasi ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    239    238            �           2604    16525    trx_data_tag id    DEFAULT     j   ALTER TABLE ONLY v1.trx_data_tag ALTER COLUMN id SET DEFAULT nextval('v1.trx_data_tag_id_seq'::regclass);
 :   ALTER TABLE v1.trx_data_tag ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    241    240            �           2604    16526    trx_data_topik id    DEFAULT     n   ALTER TABLE ONLY v1.trx_data_topik ALTER COLUMN id SET DEFAULT nextval('v1.trx_data_topik_id_seq'::regclass);
 <   ALTER TABLE v1.trx_data_topik ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    243    242            �           2604    16527 '   trx_luas_wilayah_kecamatan_kelurahan id    DEFAULT     �   ALTER TABLE ONLY v1.trx_luas_wilayah_kecamatan_kelurahan ALTER COLUMN id SET DEFAULT nextval('v1.trx_luas_wilayah_kecamatan_kelurahan_id_seq'::regclass);
 R   ALTER TABLE v1.trx_luas_wilayah_kecamatan_kelurahan ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    245    244            �           2604    16528    trx_riwayat_data id    DEFAULT     r   ALTER TABLE ONLY v1.trx_riwayat_data ALTER COLUMN id SET DEFAULT nextval('v1.trx_riwayat_data_id_seq'::regclass);
 >   ALTER TABLE v1.trx_riwayat_data ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    247    246            �           2604    16529    users id    DEFAULT     \   ALTER TABLE ONLY v1.users ALTER COLUMN id SET DEFAULT nextval('v1.users_id_seq'::regclass);
 3   ALTER TABLE v1.users ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    249    248            �          0    16400    failed_jobs 
   TABLE DATA           ]   COPY v1.failed_jobs (id, uuid, connection, queue, payload, exception, failed_at) FROM stdin;
    v1          postgres    false    215   2�       �          0    16407 
   migrations 
   TABLE DATA           6   COPY v1.migrations (id, migration, batch) FROM stdin;
    v1          postgres    false    217   O�       �          0    16411    mst_collection 
   TABLE DATA           )  COPY v1.mst_collection (id, nama_collection, judul, tipe, default_chart, referensi_data, catatan, route_name, table_name, created_by, updated_by, created_at, updated_at, ref_organisasi_id, organisasi, satuan, definisi, rumus_perhitungan, cara_memperoleh_data, urusan, is_multidimensi) FROM stdin;
    v1          postgres    false    219   �       �          0    16417    mst_dashboard 
   TABLE DATA           �   COPY v1.mst_dashboard (id, nama_dashboard, frame, link, catatan, is_hidden, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    221   r�       �          0    16424    mst_jenis_dok 
   TABLE DATA           �   COPY v1.mst_jenis_dok (id, jenis_dokumen, ref_grup_dok_id, keterangan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    223   ��       �          0    16430    password_reset_tokens 
   TABLE DATA           E   COPY v1.password_reset_tokens (email, token, created_at) FROM stdin;
    v1          postgres    false    225   =�       �          0    16435    personal_access_tokens 
   TABLE DATA           �   COPY v1.personal_access_tokens (id, tokenable_type, tokenable_id, name, token, abilities, last_used_at, expires_at, created_at, updated_at) FROM stdin;
    v1          postgres    false    226   Z�       �          0    16441    ref_data_tag 
   TABLE DATA           l   COPY v1.ref_data_tag (id, data_tag, keterangan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    228   w�       �          0    16447    ref_grup_dok 
   TABLE DATA           p   COPY v1.ref_grup_dok (id, grup_dokumen, keterangan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    230   3      �          0    24617    ref_kode_wilayah 
   TABLE DATA             COPY v1.ref_kode_wilayah (id, kemendagri_provinsi_kode, kemendagri_kota_kode, kemendagri_provinsi_nama, kemendagri_kota_nama, bps_provinsi_kode, bps_kota_kode, bps_provinsi_nama, bps_kota_nama, latitude, longitude, kode_pos, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    253   �      �          0    16453    ref_organisasi 
   TABLE DATA           {   COPY v1.ref_organisasi (id, organisasi, singkatan, keterangan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    232   c      �          0    16459 	   ref_topik 
   TABLE DATA           f   COPY v1.ref_topik (id, topik, keterangan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    234   �      �          0    24601    trx_collection 
   TABLE DATA           �   COPY v1.trx_collection (id, mst_collection_id, tahun, target, realisasi, sumber, catatan, created_by, updated_by, created_at, updated_at, kemendagri_kota_kode, kemendagri_kota_nama) FROM stdin;
    v1          postgres    false    251   �      �          0    16465    trx_data 
   TABLE DATA           -  COPY v1.trx_data (id, mst_jenis_dok_id, sumber_data, jenis_data, nama_data, deskripsi_data, format_data, media_type, size_data, ekstensi_data, nama_file, url_file_upload, url_upload, is_shared, webbappeda_visible, satudata_visible, catatan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    236   �)      �          0    16474    trx_data_organisasi 
   TABLE DATA           �   COPY v1.trx_data_organisasi (id, trx_data_id, ref_organisasi_id, organisasi, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    238   ae      �          0    16480    trx_data_tag 
   TABLE DATA           ~   COPY v1.trx_data_tag (id, trx_data_id, ref_data_tag_id, data_tag, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    240   �j      �          0    16486    trx_data_topik 
   TABLE DATA           z   COPY v1.trx_data_topik (id, trx_data_id, ref_topik_id, topik, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    242   �{      �          0    16492 $   trx_luas_wilayah_kecamatan_kelurahan 
   TABLE DATA           �   COPY v1.trx_luas_wilayah_kecamatan_kelurahan (id, mst_collection_id, tahun, kecamatan, jml_kelurahan, luas_wilayah, satuan, catatan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    244   �}      �          0    16498    trx_riwayat_data 
   TABLE DATA           B  COPY v1.trx_riwayat_data (id, trx_data_id, mst_jenis_dok_id, sumber_data, jenis_data, nama_data, deskripsi_data, format_data, media_type, size_data, ekstensi_data, nama_file, url_file_upload, url_upload, is_shared, webbappeda_visible, satudata_visible, catatan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    246   �~      �          0    16507    users 
   TABLE DATA           q   COPY v1.users (id, name, email, email_verified_at, password, remember_token, created_at, updated_at) FROM stdin;
    v1          postgres    false    248   ��      �           0    0    failed_jobs_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('v1.failed_jobs_id_seq', 1, false);
          v1          postgres    false    216            �           0    0    migrations_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('v1.migrations_id_seq', 27, true);
          v1          postgres    false    218            �           0    0    mst_collection_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('v1.mst_collection_id_seq', 284, true);
          v1          postgres    false    220            �           0    0    mst_dashboard_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('v1.mst_dashboard_id_seq', 3, true);
          v1          postgres    false    222            �           0    0    mst_jenis_dok_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('v1.mst_jenis_dok_id_seq', 3, true);
          v1          postgres    false    224            �           0    0    personal_access_tokens_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('v1.personal_access_tokens_id_seq', 1, false);
          v1          postgres    false    227            �           0    0    ref_data_tag_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('v1.ref_data_tag_id_seq', 121, true);
          v1          postgres    false    229            �           0    0    ref_grup_dok_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('v1.ref_grup_dok_id_seq', 2, true);
          v1          postgres    false    231            �           0    0    ref_kode_wilayah_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('v1.ref_kode_wilayah_id_seq', 28, true);
          v1          postgres    false    252            �           0    0    ref_organisasi_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('v1.ref_organisasi_id_seq', 46, true);
          v1          postgres    false    233            �           0    0    ref_topik_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('v1.ref_topik_id_seq', 34, true);
          v1          postgres    false    235            �           0    0    trx_collection_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('v1.trx_collection_id_seq', 398, true);
          v1          postgres    false    250            �           0    0    trx_data_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('v1.trx_data_id_seq', 209, true);
          v1          postgres    false    237            �           0    0    trx_data_organisasi_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('v1.trx_data_organisasi_id_seq', 57, true);
          v1          postgres    false    239            �           0    0    trx_data_tag_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('v1.trx_data_tag_id_seq', 349, true);
          v1          postgres    false    241            �           0    0    trx_data_topik_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('v1.trx_data_topik_id_seq', 29, true);
          v1          postgres    false    243            �           0    0 +   trx_luas_wilayah_kecamatan_kelurahan_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('v1.trx_luas_wilayah_kecamatan_kelurahan_id_seq', 6, true);
          v1          postgres    false    245            �           0    0    trx_riwayat_data_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('v1.trx_riwayat_data_id_seq', 219, true);
          v1          postgres    false    247            �           0    0    users_id_seq    SEQUENCE SET     7   SELECT pg_catalog.setval('v1.users_id_seq', 1, false);
          v1          postgres    false    249            �           2606    16531    failed_jobs failed_jobs_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY v1.failed_jobs
    ADD CONSTRAINT failed_jobs_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY v1.failed_jobs DROP CONSTRAINT failed_jobs_pkey;
       v1            postgres    false    215            �           2606    16533 #   failed_jobs failed_jobs_uuid_unique 
   CONSTRAINT     Z   ALTER TABLE ONLY v1.failed_jobs
    ADD CONSTRAINT failed_jobs_uuid_unique UNIQUE (uuid);
 I   ALTER TABLE ONLY v1.failed_jobs DROP CONSTRAINT failed_jobs_uuid_unique;
       v1            postgres    false    215            �           2606    16535    migrations migrations_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY v1.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY v1.migrations DROP CONSTRAINT migrations_pkey;
       v1            postgres    false    217            �           2606    16537 "   mst_collection mst_collection_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY v1.mst_collection
    ADD CONSTRAINT mst_collection_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY v1.mst_collection DROP CONSTRAINT mst_collection_pkey;
       v1            postgres    false    219            �           2606    16539     mst_dashboard mst_dashboard_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY v1.mst_dashboard
    ADD CONSTRAINT mst_dashboard_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY v1.mst_dashboard DROP CONSTRAINT mst_dashboard_pkey;
       v1            postgres    false    221            �           2606    16541     mst_jenis_dok mst_jenis_dok_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY v1.mst_jenis_dok
    ADD CONSTRAINT mst_jenis_dok_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY v1.mst_jenis_dok DROP CONSTRAINT mst_jenis_dok_pkey;
       v1            postgres    false    223            �           2606    16543 0   password_reset_tokens password_reset_tokens_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY v1.password_reset_tokens
    ADD CONSTRAINT password_reset_tokens_pkey PRIMARY KEY (email);
 V   ALTER TABLE ONLY v1.password_reset_tokens DROP CONSTRAINT password_reset_tokens_pkey;
       v1            postgres    false    225            �           2606    16545 2   personal_access_tokens personal_access_tokens_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY v1.personal_access_tokens
    ADD CONSTRAINT personal_access_tokens_pkey PRIMARY KEY (id);
 X   ALTER TABLE ONLY v1.personal_access_tokens DROP CONSTRAINT personal_access_tokens_pkey;
       v1            postgres    false    226            �           2606    16547 :   personal_access_tokens personal_access_tokens_token_unique 
   CONSTRAINT     r   ALTER TABLE ONLY v1.personal_access_tokens
    ADD CONSTRAINT personal_access_tokens_token_unique UNIQUE (token);
 `   ALTER TABLE ONLY v1.personal_access_tokens DROP CONSTRAINT personal_access_tokens_token_unique;
       v1            postgres    false    226            �           2606    16549    ref_data_tag ref_data_tag_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY v1.ref_data_tag
    ADD CONSTRAINT ref_data_tag_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY v1.ref_data_tag DROP CONSTRAINT ref_data_tag_pkey;
       v1            postgres    false    228            �           2606    16551    ref_grup_dok ref_grup_dok_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY v1.ref_grup_dok
    ADD CONSTRAINT ref_grup_dok_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY v1.ref_grup_dok DROP CONSTRAINT ref_grup_dok_pkey;
       v1            postgres    false    230                       2606    24624 &   ref_kode_wilayah ref_kode_wilayah_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY v1.ref_kode_wilayah
    ADD CONSTRAINT ref_kode_wilayah_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY v1.ref_kode_wilayah DROP CONSTRAINT ref_kode_wilayah_pkey;
       v1            postgres    false    253            �           2606    16553 "   ref_organisasi ref_organisasi_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY v1.ref_organisasi
    ADD CONSTRAINT ref_organisasi_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY v1.ref_organisasi DROP CONSTRAINT ref_organisasi_pkey;
       v1            postgres    false    232            �           2606    16555    ref_topik ref_topik_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY v1.ref_topik
    ADD CONSTRAINT ref_topik_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY v1.ref_topik DROP CONSTRAINT ref_topik_pkey;
       v1            postgres    false    234                       2606    24608 "   trx_collection trx_collection_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY v1.trx_collection
    ADD CONSTRAINT trx_collection_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY v1.trx_collection DROP CONSTRAINT trx_collection_pkey;
       v1            postgres    false    251                        2606    16557 ,   trx_data_organisasi trx_data_organisasi_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY v1.trx_data_organisasi
    ADD CONSTRAINT trx_data_organisasi_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY v1.trx_data_organisasi DROP CONSTRAINT trx_data_organisasi_pkey;
       v1            postgres    false    238            �           2606    16559    trx_data trx_data_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY v1.trx_data
    ADD CONSTRAINT trx_data_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY v1.trx_data DROP CONSTRAINT trx_data_pkey;
       v1            postgres    false    236                       2606    16561    trx_data_tag trx_data_tag_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY v1.trx_data_tag
    ADD CONSTRAINT trx_data_tag_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY v1.trx_data_tag DROP CONSTRAINT trx_data_tag_pkey;
       v1            postgres    false    240                       2606    16563 "   trx_data_topik trx_data_topik_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY v1.trx_data_topik
    ADD CONSTRAINT trx_data_topik_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY v1.trx_data_topik DROP CONSTRAINT trx_data_topik_pkey;
       v1            postgres    false    242                       2606    16565 N   trx_luas_wilayah_kecamatan_kelurahan trx_luas_wilayah_kecamatan_kelurahan_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY v1.trx_luas_wilayah_kecamatan_kelurahan
    ADD CONSTRAINT trx_luas_wilayah_kecamatan_kelurahan_pkey PRIMARY KEY (id);
 t   ALTER TABLE ONLY v1.trx_luas_wilayah_kecamatan_kelurahan DROP CONSTRAINT trx_luas_wilayah_kecamatan_kelurahan_pkey;
       v1            postgres    false    244                       2606    16567 &   trx_riwayat_data trx_riwayat_data_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY v1.trx_riwayat_data
    ADD CONSTRAINT trx_riwayat_data_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY v1.trx_riwayat_data DROP CONSTRAINT trx_riwayat_data_pkey;
       v1            postgres    false    246            
           2606    16569    users users_email_unique 
   CONSTRAINT     P   ALTER TABLE ONLY v1.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);
 >   ALTER TABLE ONLY v1.users DROP CONSTRAINT users_email_unique;
       v1            postgres    false    248                       2606    16571    users users_pkey 
   CONSTRAINT     J   ALTER TABLE ONLY v1.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 6   ALTER TABLE ONLY v1.users DROP CONSTRAINT users_pkey;
       v1            postgres    false    248            �           1259    16572 8   personal_access_tokens_tokenable_type_tokenable_id_index    INDEX     �   CREATE INDEX personal_access_tokens_tokenable_type_tokenable_id_index ON v1.personal_access_tokens USING btree (tokenable_type, tokenable_id);
 H   DROP INDEX v1.personal_access_tokens_tokenable_type_tokenable_id_index;
       v1            postgres    false    226    226                       2606    16623 7   mst_collection mst_collection_ref_organisasi_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.mst_collection
    ADD CONSTRAINT mst_collection_ref_organisasi_id_foreign FOREIGN KEY (ref_organisasi_id) REFERENCES v1.ref_organisasi(id);
 ]   ALTER TABLE ONLY v1.mst_collection DROP CONSTRAINT mst_collection_ref_organisasi_id_foreign;
       v1          postgres    false    219    232    3322                       2606    16573 3   mst_jenis_dok mst_jenis_dok_ref_grup_dok_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.mst_jenis_dok
    ADD CONSTRAINT mst_jenis_dok_ref_grup_dok_id_foreign FOREIGN KEY (ref_grup_dok_id) REFERENCES v1.ref_grup_dok(id);
 Y   ALTER TABLE ONLY v1.mst_jenis_dok DROP CONSTRAINT mst_jenis_dok_ref_grup_dok_id_foreign;
       v1          postgres    false    230    3320    223                       2606    24609 7   trx_collection trx_collection_mst_collection_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_collection
    ADD CONSTRAINT trx_collection_mst_collection_id_foreign FOREIGN KEY (mst_collection_id) REFERENCES v1.mst_collection(id);
 ]   ALTER TABLE ONLY v1.trx_collection DROP CONSTRAINT trx_collection_mst_collection_id_foreign;
       v1          postgres    false    251    219    3305                       2606    16578 *   trx_data trx_data_mst_jenis_dok_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_data
    ADD CONSTRAINT trx_data_mst_jenis_dok_id_foreign FOREIGN KEY (mst_jenis_dok_id) REFERENCES v1.mst_jenis_dok(id);
 P   ALTER TABLE ONLY v1.trx_data DROP CONSTRAINT trx_data_mst_jenis_dok_id_foreign;
       v1          postgres    false    3309    236    223                       2606    16583 A   trx_data_organisasi trx_data_organisasi_ref_organisasi_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_data_organisasi
    ADD CONSTRAINT trx_data_organisasi_ref_organisasi_id_foreign FOREIGN KEY (ref_organisasi_id) REFERENCES v1.ref_organisasi(id);
 g   ALTER TABLE ONLY v1.trx_data_organisasi DROP CONSTRAINT trx_data_organisasi_ref_organisasi_id_foreign;
       v1          postgres    false    3322    238    232                       2606    16588 1   trx_data_tag trx_data_tag_ref_data_tag_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_data_tag
    ADD CONSTRAINT trx_data_tag_ref_data_tag_id_foreign FOREIGN KEY (ref_data_tag_id) REFERENCES v1.ref_data_tag(id);
 W   ALTER TABLE ONLY v1.trx_data_tag DROP CONSTRAINT trx_data_tag_ref_data_tag_id_foreign;
       v1          postgres    false    3318    228    240                       2606    16593 2   trx_data_topik trx_data_topik_ref_topik_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_data_topik
    ADD CONSTRAINT trx_data_topik_ref_topik_id_foreign FOREIGN KEY (ref_topik_id) REFERENCES v1.ref_topik(id);
 X   ALTER TABLE ONLY v1.trx_data_topik DROP CONSTRAINT trx_data_topik_ref_topik_id_foreign;
       v1          postgres    false    242    3324    234                       2606    16598 c   trx_luas_wilayah_kecamatan_kelurahan trx_luas_wilayah_kecamatan_kelurahan_mst_collection_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_luas_wilayah_kecamatan_kelurahan
    ADD CONSTRAINT trx_luas_wilayah_kecamatan_kelurahan_mst_collection_id_foreign FOREIGN KEY (mst_collection_id) REFERENCES v1.mst_collection(id) ON UPDATE CASCADE ON DELETE CASCADE;
 �   ALTER TABLE ONLY v1.trx_luas_wilayah_kecamatan_kelurahan DROP CONSTRAINT trx_luas_wilayah_kecamatan_kelurahan_mst_collection_id_foreign;
       v1          postgres    false    3305    244    219                       2606    16603 :   trx_riwayat_data trx_riwayat_data_mst_jenis_dok_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_riwayat_data
    ADD CONSTRAINT trx_riwayat_data_mst_jenis_dok_id_foreign FOREIGN KEY (mst_jenis_dok_id) REFERENCES v1.mst_jenis_dok(id);
 `   ALTER TABLE ONLY v1.trx_riwayat_data DROP CONSTRAINT trx_riwayat_data_mst_jenis_dok_id_foreign;
       v1          postgres    false    3309    223    246                       2606    16608 5   trx_riwayat_data trx_riwayat_data_trx_data_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_riwayat_data
    ADD CONSTRAINT trx_riwayat_data_trx_data_id_foreign FOREIGN KEY (trx_data_id) REFERENCES v1.trx_data(id) ON UPDATE CASCADE ON DELETE CASCADE;
 [   ALTER TABLE ONLY v1.trx_riwayat_data DROP CONSTRAINT trx_riwayat_data_trx_data_id_foreign;
       v1          postgres    false    3326    236    246            �      x������ � �      �   �  x�e�ݎ� F��Ì�����H��2MCD�}�5ͦ	�(�rq�m'� 5p����Ct&;='���<�0܃P�O��+D��K.��n�DQ�^�N�o%^���?��r�K�\p��P=a2�6��R:)2Qh޼�F	ޯft}��S�p'��HE���Jz�������Pj@*������N�=��Й&yb�W���_`k������F���gA��q3h� P����j5,���K%�t�-6��gfM���������ː��?{B�l�n[����_��.�3n��P����ه�$�����Jg��ˏp�w7��L�7����tR�I�﷓Gm���['�g��C�q�ur5Y\�.e����|�Z�r�Q�X4Z��<fo��6�͎��L
վ��3l>0r�Nt��cց��Z)w���{�n�����x_Y�      �      x��}�nܸ���W�emd����jcc��`�q�հ={p���V�Aj%��t������S�"%J}�3�]�*�Z�*������j��_�y�]V�*���'��W*�.ޔ{�0i`w7�>��� ��������:���S/,���K�q�^�x��[S�`	�'�*.Vqp���s��Ue�
~�[4^�'��X��xO\�d�W��2�q�N6ɶٰ̢��)
�LE�)��6�4hr�J��m~�����z<��"���")���[q�k�BlW���6�. R&�}\&�.��/��U��n=�[�f��_��S�pt=�?/��`pq�]e1v�>-�<.��*�i��f��S�tפ��(����_��1_���Q=���2�⭷��b�mx��sou7y]_���£�;�³�Ň�2�JX�P�-�MZf�k���?ɶ*�}p/�<�'�K� ��^���z0��F��mL��*�}𴏡��if�3����]�ӺL�*N��d��b�%)��6^7�X�HE#�D#r�G�^�
Q2��{���z4���v�^]��C�Ƅ0Ty����B�$2�⦚"l�TE��c��+lC���tݳ����v�Z����H[�)DfQ��!5��M�hѤ�vN[��j�}mš�ne�#���Y�ܝ��L^ph6-�r��rle�c��C/l��_�i_C�0{J�]s��0�5"�-F�{�*����N?o��0�x�����a�i�5W ��\l�F�YM��Ѫ�P�=�Oi�����v��V�[�����`�T�5T/Q�ǂ�=��D�%c��6�����c��
>l����MZC�HU� RΩ����v8'Q��3�7��dy�'�X���qp��*P�H�Ӝ���A�8��Ꭵ�>�!\�{�`N�����??��\������f���Ē�@:���(򯛥�8�� 񗱸��3g�}_��J��$�Z��\��J�+���\}�*� �`���Fz3�?M�&|��!�����_{�LX{�� r��������OR�+��,3�P0�Xb_1��	�.��%c�%� c���P��&煍�� ���)��3�����#A�%�}����\��}\����]ĨO�V�wE�I��|�e(����q���T-��k�d0����@.bM\@� ."#.J�bR�H�&�H(�P*BɄ=}7����aC�wE�������R�w/޷�������A1{����}K�J��S0��O5�fa�8��JUp4��X������ټˣ�Ć���).0��NMx��(b�KE,���D�a!�`��Ñv������b�>�g�*@eo�-!�H���F�vJU$�H�"�L�Pn��ף+/�Uܹ�����A܀�͢z�t׈���6H����Z2�F^�1c�ݪ��*�k�)������ rC'�ZS��������GXVs�C/L+8qʰ��>]Vy_�C��F{{����R���k�ͨm���5c��^ث4�F=!-U�=!��	����\�J郝�	�.~������_���<�_.�ʾ������fcV���mqz��P�+��ڭ���8Xr�-S���s薳Ѥ-gv�'f�7%ǣ��p��վ�[�bŘ��bKU�5`ό��3/��6���6|D�dQ��ɷ�Ć~��ec�TD@D����G�I���Ňu��}�����"UZ�'�]'w�m�Ʀ�0v��FY�Ԏ�>o٬�0����E3=��&[Z�鶅-��WD�U�(��\jx�Z�aiya�y1����o�����r��������b~�x��Z�wsY,�n�?��	�>���N�c��dt�Of2:�-3��c3��h��V�B�H�x�#ΰ���ॵo�5�}P.���(
^��C؝P.��6Xg�w[q�.8�j��{P���27H��UH�߮J�Uҡ����A�^ؑ�U���Rvv�Jٽ�0-��2�a�Y�]��J���2�-�8C{�zF���������fT\�T�a穸��PbC4�tQ����c�Mi�J*h��H|EF���� czI���Ow!�i��`慝�چ�f�����N�f�0�Fa�7p��?j[�������fٲ�ߙ��T�9�;��T��~DC��u��a�|i�;:�� �x!��\����?j8����?&��qC�B��`�_1 ��_�7�P�Y�����_R�*?��w���4[�*v�l��j*Q�l�k4���$`�p�?��Ӈ�H��?ǣʘ`��AlRi�3ReW�X$�g^sPz�:ͺ\5�Ud��n�>�-�C^.|��{���n�I��0���Ͱ�B䑰z�f�.�A��?4�7?�_vߒb�y߱�$bU�X~�DZ�s*;4眻M����S���ˮ؀,���k���{�P	��]���3����װ��"�"[��$Io��M��д[M���g�v���u%�7�q5j?i��â:PyP�|uG�Z�)Ϭ��D?�ߞn?y��-�����V['T&�?����ܟ�+��g���5�J��۷4^���Bs�+a]�f	K�\���р~�Or�iH�8+�%�m�A˨��E�v�"�����G�t����	��HKKr���?7�D޵J��cAi�X�{�B��?P<��� Zn���ZJ�U�}���w �Wm�O+����\Q#h�L:����/M ��NI���2@�^K5ܩ^R<�}��ڭ���H+�5�Z��v�c"�4o:��fk1�j`�������R8QA���'��ёW��7e�<�����M�	~+����%��XA�"�6�c�U\D��". U\��
�4��$�JCJP��0D�h���X�pld��m�A7�/����n6Z�'�=İ/� �[��bB�bSc�A$T,���M���vF������㝪@�	���0I_�P�+OL�2��H�e��"��z�=s��,�	���c-��S���[f�!��ێ��B���vˑ���{we;*mX��%F��`����m{�9�yqZ�a���HrQo�G/�M���m��桚�1�������|K�:M�kjj���y�D������IxT���k�!���ޢk������2����Ā���U���D��S-�̺cKgxJb��8�:2�Zp��?�4�0�J0��j�/I�O��� ��s���l��58�B
$$-Hѩs!Yjd�Ȥ)��a�za�9H��=-�`�sff������_y
����7{�U+5�I@�a�H�F�wH(y&b���)B��!tb�	["�Ԙ�Ɣ
�g�F���a`�@�-��a�6�V�iS���1��Ƞjeh��{����M��"$��39���#��������$�C'�ڂ�r󙐽�9����lWൗXG�+��$4�(�l�5���H�Ŧ�$�HF�&O_{��æ���#���%ͦ����Y��)�r��/4�q#o�O���i�^;�Hk4���ӯq�b��CQY,*��`� X|D@!���TX�����i�Ծ`h�Nh�&j�U�j#�]|x��F�?������Z�m��Q�K�5�}m�~兽vhǨ�Ĵ��?�Q��g�0x�ul������[�~�XԞv�$���I�Yh��j�"�{��|�]����X��*�dڈ�5W.�w۲ڀ�ig�I�5L;�������m�0l�3Y��¦��@�edo�雉}�؆��LsrewTmԱ��I6)��4���s������Q5s>HL�bҸ%iuTMi��R*�W)�S��z<���~��n�:�M��T�������4 U= ˅p����5eN&�9���Fq *��0x&�M(. �^_�Z�D��X~a4֞�Ǆ��`�ݦ�.+<��:5+4�XL��`�e]�+M�G��k{|����ϛ]��8t�	��%�B�h��ԹQ%Ucd�n�ܡ�
�g'M���^�궄���
�ݹ�0� �ʤ��A	'���·�@��~�5��lPf�:�-��h�u�:�"盽��k���QC5��W�v-��Ε�Mt����D��bd}�Z���ʹT6=�6��g{���8�'v��}��ٔ����t�W��MN)NЀ�    ��j��)��ۯ��?)!�_�����Z;���]i|L�Ǆ����"�a�Ӭe����I�0cK��� �$~c���6����h쇵eL�Vc�c��n/`��5�PIHD�`	F�J�v�5��ávn��t�n�j[$�̀ �Z�n�ka8��V���z�j3�Η�3���iv�&>�Lk5� 
���q�L�T��[O���>�;�B[};�B��WB�c�i��y���|�w����Yoߜ�E�*WKX�*�H�jsIO�a�4�n��Vq��9���U;l˾�D^_�Rm�cV�HRQo݇3?�����uL2��+7�]���J�
�3��ƴb�M���{4l]hع����#�t��Lc��(4�ڌ�X?n�h����gq�E�\��{r̅�"�L*�T"�4��y!�dҞ�s^�Du�}���J��1�S�a�$oY�ҀS��
K<c׮ߎ����G0�=��X�f{�(Z�i%�r�Mky�;]�W�É�(l��$ʓLŴ�]%����K��&�	-�D��x�i6�i~�
��u����R�)0H��3;a���fT����L��q�;^�o��,n�^�H3#|�%�)�p5��qG��NQ����P��m�l�w����5+�YuN�.C}8�e��W{��1��'0�'M�7��I�q��kՖ4��T�D7�
c�2���錨y<�aX���]�y�#M&zr�v���;f�j�"w�L��#���C���V�cXϡm+�ωQD�ǻ�رH�O�&m�.�q���=�^�#?�Ȝ<=C����ϙ���:âl���,�j;�fv�Q��n�r���9U	s�]�_���HP�Ftt$8�������*�[���]��[�Ʈ�Ng��Tՙ�5CI�ah��C\����q@ٰWw����f�����I��u�#:�p1}�e�I��Ս����&��16tN�l��;,T1�O7�:�1y�����w��l4���R'��o�Ԏ���6�D�I~��-F�g4�6�+�I�h�n��X�h�VǑ�3��'ث�&�@G3�cLڴ�
���(3���S��l���l`ݯ�Y��Pl�a۸ԥ�ڗ��+C󒊗4�W�/�
X���c�ao��	�s�pjN9�V+Ln��ț���N���]q���>e#h�U3G��>�W^Vǣv򩟳�v���c��K�D�ySO�&M��V^-���I���_s�}�x�z��^��i#�,����H��i��	^�`'2��[��FD	�ģS��d��Ze�zve�v�a'5L�`Q�=U�ə����V�(�?R���x{'㏔�/��?О�!�k�z�<nq\TU���v�`��!���d��V�� �;e'QG��H	_��u8��=�Θ�H)��hd�I��@0�_���d!���Y�&#]Wio��P��H�d���`5��n�<f|��B��Z��]cFN��d�Ea��>�]{䅝4��C]P�`��VF�m7dG4����v⸐,�I��h-�oNk��9�N���ȩs�h�Nj�C�)�Ȇ�%�ɝ��e=�t¢�l�F�rO�g�/̂����-���ջ�_lWN��;M�\�e/�}R�b�*O M-4��a�$R�t�p*ڙv�`�A`�g�#r�-�d�xV��#8�d�x�$���x=-�4�FE��G��6���謮�3�����b���l��r�g��r��1Y�'��\s�r�=B���c1�$"י�59�3yoG������B�/������˪��{�O��s�[�"CtA�!��n� !I8�p���iմ���o��G�ӡw�;��|{̲vFw��\��0^o�ơvҨ��Q��ӭJ��;\)�t��q�B����e`��x��(������.4�GL���ݺ<���>?p���������b�p�re����ښ��?g�I�f;<�L�_�y��[tp�2Qse��qS�IeM*��g�\9�L-�I��	lȔ��
�HֻM������u�V��С�F0��3=5t���%#JD�n$�^7WÎw�ޫ/��>U9�ׂ��[�u<3�׈VNG��٭a���� }i©����\�x���po`'��p�Do���_@�U�=��&�X�XnC�a��Ή�=a����]��aK�? @�OΌ0�4�^�1����{q8��s��⸼�[�Uڋ?5�E�ϓ��`.s}�˟�\_�:��|�����vn
˩B�";W��[n(tV�:]�{#I���YB��۱��g�w��_�d��.w_i`O]1��{���������`�b��C`J��>��tE����O[���p�WJ��6�>������8�8dr��;7�r6qm�{[�a���s��tIP�<��
x���sW�����^ӷsC�޲���D$QO�̮ͫ4Gz�ح��<��%�"��3�4�����V+�^l��R`��=5��9���M�����Pé�1��?5�ф՘<�Ju�&׃�[De�����f�>Î^�
�]+M&��=�f�5ǻV��XQXB��Z$j��B#c����hт��B�U��"���H|Ldn�Y��P,:�r�t褢;>"������
v�U���x���"�����Ex�� �o�fn�v|��0�]<&������Z�>������)b����U�!�_��\�0�[SȚ��"�ijӻ̰3^T��T���	�8�������t������9y��:cK�u&#��q�3��=��v�f�O��^r�7���E6�E�g��ܬv�qU��}�Y�P*�о��׸Љs�ag�;��5���|K���4�H}����ֈ#6G�����i
$���9��Z����<<A���7��C@L�`��8�#�&�<_��M*�|EI|a����|OB�L"3��`r�W���tg�\4�`gȧ���y�����57Ì�X�e���zT��xC�[:�j̰�`o��T8�8�|[��Ǜ^s�S{0Zӆ��\��Gf�o�C�-����ém\d��gS��)M�v0�:��h�mn/B��{O�guj�x_ ~ ��
O/��O��1���Ŭ���kӹnM&f(���rޯ%���asy�d�EYs�L`KZ5�#�����#��&v��mPϦ��,����"�N{���ĘG����m1A�cs�u6��XoW*�'�بS�,�A���ߢ�T>	E'4���fR�*3�B�Y	��^�t;��Z���l'K��x|��m�G�Ys�Њwk�O�\��)4N��¦oLE$�H�D�&��7ϖ{��EF��8v_���s{�9,`��I��&�&�2%
iS)8��a�;ӧ���-G+�*㋖{� `$1�d���3���/��T.]_R�K(^��#(�mJ+�K��T3���,{�+�5{';�{�\Y�u���0�
���ܢ�
�0�	5���Fk�s�P��4:j�Lb����^xU�,���p
m�$���QΦp*žmG9�Q3���6��x�R֔:������qs})�+��D9F �%U�LiSDg�������D�+}�@,��z�g�ۡ;�Κ^�$a@�^��~�qs�:-C�WX|�L�P3��3R7`���;������^� ~u23->�ߘ����"l�v|g{:VF�!�K�nh+|�hY�W��q��Ћu'�a���X1u�WLN3�����a?TX1�k��YK� a֑#=rRmY��G�-�E��TcLA�eL׺
���}���U����"^����^����uL5��_To���5�>hv{���6ͫZ3����2�pQ-~E=SG_1������+��r�4�Q�8���葹r���O���GX]n𒘹�QGR�wS�����K������D��� �� ��jg��FN��{��:ڼ~c����C�K�mx=n��`o�6|8d��i���׊e��yB#�l�����P�u�)s}G�a3�?��}3�����(�YR~��;���hN�����x��u^��E+���d:;ŢNQ���ظ��E��d� C  ^����m�;�^�L�&w<�ֻ��pn�ĉа`o���Tt�A7��+���@�߃f���o:��|f�ڷ���7k��;���� ������\�$A���g��,�B����W�6橪7�p���kܧ�Q=_}�m<-��+_����:�oniv|�G�-� ��ۘ��)��sȀ�pL�8�~A�&�5�w��#���f���!r|���~pd��.ǣ���N�)D���K;���a�y��y��v��0��Ϙ�O(�-�����z�9m+gR5���E�,��#H�_�r_T& 1ٝ�rz�9K��;��zd��f����t��%a�z�K���f���2�G3�6�/ä��g�%mv�����b�X�4��x����]+�.7(x�D��R]�4p�I"	��
��ۜ�;!>>
#=��`VkEf2�yWO�<�`��t��8�٢`�j��&���L�
g}~���7��WW�I'��?�.S��]U���LW�g%WE���g}��P���?}.S��]}�OL�'4_��{�z�䥵`o��W8:vآO;U��y��I��dK2�<����Vψ��_�9g�l�&��'̸g<��Q�{���xO��%�'s���cA�}��򚂇�w�x��Uv��%��&�׬g���`N��'�a�7�i��7ʐ�d6�&	l$����Y��8���=��R�L������Q����!�	d��6P�J�Uov�"���°>��T���p��W=�y��9���7�\�h�$+R�h� �����=��tC�n�ʧw6<����@�U>�Գ!�L$��l�3���4�v���3=���+P�ANHz�QǓ�˩�4,�2�-�+۾>�?Q�@N6H���*~A��B�"S'#��T_iE��b�3'�ۂ��\q�w���l�Jiw�m�{����c9��ʞ������/�Cu�[����&�Z�+|�������Y�xI��?�ⓙ� ��L��w!K�,�Ď���7ܙ�CO��zcyH�!����G��8l�2c�)����W��1����^�݃�J�2������L$o�@���[8��`�T-7�~3l��V|�>0l�bˇp�V0��Ҝ�%-^x����ǅc9��̫׵�v��<��Rd4^�Ҧ{�-�Q�I���Xzö�Ұ�=NN�����sZ?^~�?|xQez7n���:2 ���$	H����[U�Nttر��#������W��Y/�4=��)V9����>]�n�~ ���6�,(9�[�B��~Ķ���j"���&�H$�h\���F^XX?�u�}�-�:V�!H����sPM��g��u>}t��3(�
���ݖ�+ j��"6�0d��V�JW��a^/,�x7E��:��0��{�$�u��+���m�G�4���~�#xx5 Y�](�NZpvʃ�u���J������Ϗ���F�v#�c�j+w��{P'�r܄��о�c	|ۭ�M��5
�#�<�����P=sd�}�D ac([={d�{�D���;��\Ԃ�~E��{4;��UVYJ<���9��-cX*�D�غ���$RY�#P���,��g�8nz�v�B����rx�{�|�>��hd�d�"J��\n�!�ZX��HC"��H�{c<��������<2�·����!�߻B�-2�j��U$��'������v���/��ޘ.*��A�~��嬸?	L!� ��xbX3�B �,��B�%)�B4�2�d�"�DF�#E��9��za��)�Ea�p�Y�`�x�K��^0�;맗�0$ε���e�'^W��()`�|(��:h4�^���[B���V�6{��R��p��jڪx=^�'��f�Ce=��6��O8��~�k��>TR�VE���"S�m���fe���@\
�B�n�BX,��&���Rө��E��?��������������!�;ˁ�1��9 �>�Ci�r���̽��~��	�'��@~�<�H�E�\$r�ȥG5����vh3��ǱIN��4�tjp0-�k�P�)BBך�S��\��>7ɍ;�fo�Gd^�*L����Ϫ�OP��K�2���.C7���;S�.nѧ��	S�"�Nv�	;s�V���2]���w;<h��@�'��X�3���老���;��J"��aNT�T2�XS�<����>7��z�<Xe�
T��ډq��A�^�#Ű>V�$�x]��h�$x4V�iv8���T�t�{����J���Q�*kb9�U-�BԵh�r%a�`[�a̷$~��oI�����:��o����{������Ymx;`����%Ih^qz���G�O��W���b���]K���|��+,��ȿ����6�+>� MY;G53��I��$f�)����`0�~]s����_ڌ0��E����,��Mݍ� �̐�x;�N���=jt`'ԩ^��30�й�b�^���hH�^��O)�%ݠ��x��o�~�o�S�������b%,V��|�Y��a�^*ziѻ���v"�w�_#��;�Ξ��y��U(9�Ɍ�->��G���a�BO������:�{����S��Y���[��%آ�<�d: .���K�:�P̚χ��0šs�Hš�C�vR^�h�x�����N��h��!��E���7Y�ѧ��A<��e�~D/[<{zz�� �a?`��#>��^n9w1���,�M�Kq��0�DQ����9kLi0%cv�?*�F_ְ����oy�{?����¶�F�y<'�7jwG��e��s��Q���P�*D��<ygB�p�YC�װ�;���������{�"<      �   j  x����J�@���S��{���#("�Ÿ�:]�q2顧��ot���p6u6��������-D�?����;H.�.����š�5�y6���ɥZ�Ch�|�q��b�~���n����~�U�w�,�"�އ8�}��EmE(@�`H*�h˅�8(��c�<���\�y��b}p���x|���OI�0M��;Nӡ���Eq�L΃�<�']��������rA�&�ʘ�����*KEr�p��%߂KI5p��k"��H�*I�.�Х+�-Oe֛���YE���[æ��,e�����g��q����[L�դ���,=9��*�4k���1�r7�������xU����<cU�˚��i���_S�[q      �   A   x�3��p�4���tq�FFƺf�F�
�&V&VF&�ĸ�8��|�2��W� <(�      �      x������ � �      �      x������ � �      �   �  x���Ko۸��<���lr�)깳cO��N;�̆��T�-z$跟Cٱ$�3��S���_�E3��f3�����r�۷�wk	ʬ��P:��d�	�dE"�I!�D����2
'��'c�S�4�G����Ϣ:<ǹA�-G��&��"��g���F!�b8d��e�.q��б�?����G��� ���|f���R�q�8(+rK��'ߣ�E���Gk��:��;Igx:G<�Y�����#t#O����'kŀq�t���T�Fʒ.�Ϥ�ڴ4�c���W�>��Y�W?ZAs;� �[ʃ��x����a9�	��c��s����6�1`.���5�W���.�1)尠`!��0���2��t,�ʠ">O�f�|�6-
�P\�V̀d��q�+�����Ѓ��<)�">$�.Ie:,�к>��n�<��;���{�\zRM�"�'n�p�ȪX��_����������[���Վ��d!��t� �乢������w����͋8-e���1;d��GG���s�ک��G�<SӘd��(��U�ћ���W<��O�?���덫���m��pӓ�a��,�c`[�|a6�t|F">�&z{��ajS�1����	\��*J���"�
ٟ|�;ޗ�c��1���$)�M��0�k��&�I+����Ԓ8.���l(骑��|�&wFk��0}>[s�t�e���G�Y�?���'y��{k=p��$o�:�K���~�C�����푻M��\�0�١���&?�/7��( \; M]9,�y�a:�"Q��R.c�jﴒ{:β�A�w��a��������Q�s,�������
��J:�����lU�b(宎�c�)0��^0�eZ1��@ԙ�C�b����X��ĀS��.o�:x6���*�'�c �u|�ͩzs���m��@��M����׬�6}�r��B� ��I\Hz'SLg���c�Xds��PP�1��c�βIv2}I�^��1�Tиy�F��A�%�K���"6kRY���q�/�O��T�n_J���X:� _QG����c�aa���İ�P��1p�ܛ�/C��1p<���`�h���O������UWw�ǯ�{��ES?w����.�����e��+��pu\N�o{���y�4�����*K�2˓��2�?�b�
%���=�!w�x�[�T!v}j�{*�VU�vC�(�R3p��F��Y5Cf�@���k�!s
�y%�0p�pޱ�R���\E����C	F�u�����;_ͯmN�
�1�j���[�e��f�x6�Ff�˳B���w<��xߍ��Q�8��� �ې�#�UO��� p�}Z<�,ףak�kO������k�<, ��e�����B�J�Wnpx�st�6Y&e���ŖP��Vh��p8V�+}�r���X�<���w�*(��@�i��:�S�Io6�I_�}�p�w��%�i�Z�����
>���?��b�t|����F�O��聮����Z��槝�|���A���^F{͋C�A`��L�xg����5�������Onq���]ӴX�U�[�����xA�7�f�d����`RPn�c�&oq^`R�H�:�v5�b�$�&[U�Dܦ�0܋���0���	)�G����I���ʇ������ �AK_dڛ�Σ���_3�֚�cÊ�&0��e]���2�x���b���x=�����`�AC^?ٙ�a�r��P�:�A.?�M���T -�*����P�����;W3���JRY�� ׷�f���l=�7��-ɳ��q�t����I�I������B��`wT��*![?�G���1�ˁ��lө�[u��$�>�;�&��q��t��� �l���4�O���P= �۽�5�^2U�%9��7��¤`��c�� �s:�������/�y|��5,V?�����W�zx��i�g����ll�[�CAN��d�Vk�����PkI�'�X�?��-\��{RQ5�[���)t�_a�z�P���U��`s^���%�W��ud/"�11���~��ϧE�������#
&dC	��g�>s��Q;h��-��ٌ.�[;�b(���W_�[���|���l�2Q����/I�je\?Ƶ��Ê�q���f����|��F�,�hD��N��s3�T'C��,G�����?a+��      �   �   x�m��
�0E��+�$:T����RP���ɱKJ5���&E�{��S��p�d�	.֙���}�˸�5�c����;������ò'tP��o��AGxb�m@8"��*�P����Z�V	Gb��D'B���d��՞+г���'QMe���c�\s�ߧ�;}      �   l  x���Qn"1�����h5v�8yb)e��Q������Ĕ�LWE����c�P?�=to�3=��9]�CZ�>�O��yW��#]�C���I�;��* E�x@�6���������2�7���((̛J�X����!��@<��dXWa���6Z���B5�KN��Be.2aG=j@٠Tn6�+�Ǹo�Ȫ�1xMZ��PoP���t��-�Y�@=��,C`��%��P�+�5]��czOR�>j
�^�p����@ʿC[G.�����}�5k2R�g�7�-o4`���q��4䣠 o�ܿ�ѹ	JA�;���!Co��H��]���e:��*�����9k/�1��P��;�[z��C����&�N�g�yv0�%���
h�}��M��7�(�������9����;i�AAmP�asN��g,����)١������1�o�ͣ���˸��w���F�,��l��(rG�<5�fQP[��x�L�t�Z�\�N����FE䵸�h��9�N���'������܃��u=c�[5S6��گ�ۃQ�.XU�d�'4_���Ԝ^�)�m��|y4���4�5��n@������|j��Hُ6��xH��J�	�2��.��9i3�{��Y=]�����})���zѯ:K�i	V���b�$���+_�Э�#��E3+qu�7��	�K�]��Qx��Gs)��߮���&���Od��`�&@3'�:���Y+��iF5�H��������}����PhK�<��H\YI��T�o�hF$�8�c�m�-��(
�af�*��P�t�?��U��4yoѬ�\B�:@jI�$͂$�Z�-�.���L�1)��y���}}zz����      �   y  x��W�n�6}6�B�)DR�-��ЦiZ#N��e�Re��(���}e]�5s� ��33g��}V�#���(�t\|�����>ߏ�E(�M��I��M�7b�xO�{��!�҅1����&+�Z��o��\�hq�*j�]V�*UU Z<\%�"���!k�#u=_Ud�U�X�0���u��d?���#�x�V�s��*�Q��Oj3w<�*�؄�Z���*/LO��JM�E�ta,}�KS!A�꽽���uI���.��F��o.�:�.%<�)�aN�ګ���R�3�NU��	��]�|�Y��zS���<QN>���@,b�E��P-���~�0�J~	�l�%�rrZ���m��*5m�=+H�.��haյ��I!�;*�*v��{�"/��b�Ǔ�E��C��4��{������GJ%R:v�nU3��@*<u�I��Աu��kNH���#�]��i4}&5Ŕ����(��Q'/#9"ȑ�]��
���@oo(ʓ!������+��.g�Ř��X͡�?^t��	>��k�U_z�_Z�N8�M���cV!<�-�ą1!�Qp��e��F�����d;��ۊ����cL[��7]lJ�
F�)T飚t�k�1-�4��3�?����C�wg��c",������t�![��J	k�<Re�>�l^Ԃ���h��C���o�Xc8�F��#M�z���R��zr�=��-�nϫ���NyU�p�!�G�@`QZg�u^k;���@:F����9�X��_9^ދ����p:zQ:*\.��/8n��+r�!ty)�*���t!���3����Eg�1��}G�"�Ov^��P�y��Wj�Ԙ3���%��ӹ�����+��9.WWO��O:��j�o�(\W�~~�1�~s����	�t�s(K?;+&���N�c2<���gu�P�J�)/7b�Ƣy_��/퉈CE����W~ځ�.�Ԝ�E�c�k0��=n���ԸL�b_lw�����΃p�cn#Ɛ�i�t����v���u�ɆK��X[��'Ki����)p�;����?72�	�Z!g(Lg����.�?}U�ra`���� ��|�P|����d#cƢ����xOU�4��-5�`n4�e����.�}��1�?J�<�      �   �  x���Mo�0�Ϟ_�#��GTU���t���֋ٸ�I⬜D��8�vi�Z1������yg,��
�����K�Y]'�vɵ9�<C�����9�x���N_�
�H��I���:O718z���w��ʳ�ܝg �UUۆT��X�%(v���꒝i�iW�o�C�mr��i��k�خ����,�S�`�NmH�c��Й{�S�vҨ��]���Ύ�;�JS�i"򻄂�{7޺^�	2B�
x�W��R�c�Q�xM�����&O�hjs�UL.�d�~(�OB@�/�����d{�t���� x��:8h��tƗ㬺�Z�6��Hv�K�eӀga�]������m/�����]��<�{=h������@�?�1p�l���L=3,���0[{��v8
I >;L�@`�mh��8A��o1L�Eۘ.��Ϧ��	�Cߒ����5�h��>���k�c�5����	�>x����C)AFVu�@dl��ݱ�=-2�GĒ��0���oqzlM�Y�Ċ}�S�j����i@[|!XL�^���P��i4k��JEdYL��NWT���ن����n��C�)���H~�֜�go���b�ۈ�j����!#J�4�Wg�O�/��B����<��4�} � ��]2      �      x��]moǑ�L�
�u������}�%�r�b�@�LZ�H"y$���uOU��v�m���z`mU��tW=�f�;���(�����L۳������o��~�x����W_�����Qƞ�|n��*m��%bּ���o����_~��o��1E����,I����p{��l.o~�����ͻ�?ܼ����]]\��tu�p����w�n��������O?�����o���������~�?�����=�ۛ���"!������UK�j[T����ڻ��^��y�x}���,���rs{s������������������͏�w��g����W�?zU�Տ��zq{��ǵ�v뽈q��R\�;��4���_k�*��Fĸ�hϴ�U��EM������݋׷��L>.̆��"ƅ%���m詼�.aNmm�2�����b�4���'}���1Qe�q�}�0��U�̤�?��;S�9ŗv��ZmU�λ�cU�z���/��:��p�ɥa�k�UN��/�@ﲐazǢ��q���V���Q���*�Ҹ'nr���k�j�}�Q��S~���r��qͽ"��s��c�y=hE�k�.�W��p�;Lo_,������M_g�u�#�z�z�]��"ռSR�� �˃5�iH�;+�̼�F]�UǸ�L��N�7��yң����֊"ƭ_��yeW[��4�T1�D���<8�6�\}��V{c�t�L��Y��M#u�����p�ps�qj����
]��?��z�����○���7W�;�]�9u������ۋ�n/��~^��?=���L}�^�+���Z�����T|k�[DLXC�5$|��k(�M1���s`��g%<�5��a�	kpg�Z�����؍��y��ޝ�]��n���a���	Z&�2������[�ni���р��m�J��e��筏R��Nq/u!Ǐ�]��v���.V��A�8PoW|�(b���]a&}
��X�́?���ˆ�i!b��Ul�/����ߔ=����m��0_o|�D�+
�W�t������K"&�(x�T��>Y�V1nޔ�l���!�"v�r�L1A�GY�3��*�}��2��	�2ȂGXd�K�!K����U�3�v���V�wc�	��GRy>d�)���I�r&He��;�Ю�u/�.���`�D�Ǭ�&���f�@;"���~�W�&p��Y0F����>--O��yP(�<�AdD}��!�;ǚ8e�A�d�Hj��مR�H�����[�YO�켽��f���`4��D[�|����rhy����&<�`��I��L!�I�s�an3�x��Q|\���C1����b�lܗ[�(���hPC�zT�}j����6���x[7$жp��ɍ�m���0���D�����IEO�H�t$n]��r��ґV�#E��Z8�l�Wapk�v$�T��	T��5x\�zk�}�	k�����i��Y�!��+L �4�O
�{����ON����?�/�%��		G�DL�2���{�t<�j�0��"ۈ6�s�l��D˼�m�0Aˈ��L/������l��d^a��Q�p�� ��Ѵ4`���Fv�I���FӞ
�	��"�⼽lcy Q�a��%d{�٭�"&K��)b ��a��m��0��3(ˑ�>fӹ'~�	��J� �n��1��	���x�+C�9:�ƚ�f�Ƌ�U�U�z_#)�r�����=X����j{u�ݯ0���\����\�&?m��> ŧ�+Ӄ<Wp��J�@�z�A=:ܽ�HjX��a>#��F9��D�DL�r�	I�>N�0������hy:��p�2F�
�VE9�=�q��yQ"&��f"\1;a\��6��
xb�)`$����&�āx�ك,���Q|���Y���yk1k���tR�>��@������	�ο\�2�r��	��"�o 7�x�Ӫ�ND�/b����azϘ�Sj2�����R��J5Q&pŎ�b���4*5�m��qj�S�)5L�'3��w�F@)8CJ���)�I�g��6g�0a���%����uo+��t^8�C��1��O��2�I��Y0o�
��{2F�T>�0A�p��9~Rv9�rx$�Q�������5��L�n%��'{���q�	Z&�?!�o�#�-�� &����7�S����e[j���)%���L}�d��L���@Ij��t�d��"&�$lv*����d�a��VEfLK���=��3�j�U�M�h@�GY��l�-c� +���d��$[�V�����gh�J��)⹃�	_r�X��k��#�[mEL�f(s|um�d���"&�z��I��-��M"&p�(ː��-��
"&Ȋ��
�jg���,���Gd�!-�����x��3>c3�Մ��:��|��V��1�%u�Q̙S��	3���ԝ����ڴ^�"��OJ60��GU9H�v��������yĄ	b�ztش1y;�]�([�e�<�T��a��/���c�	���f�W*�KH��tK�&�z5���vf\0��c��zF���@�x��$�?��W�@G.���5*�T�G}`F�0���DG�zg=��tv�
�HJ�E{!L'	�MGZƣf1����츺Y�7��y�k� &�]I�٫��K���Q `���Ќ�Ww���y�5A�OR*�V����竵�?��]U}���˟ϯ_����������������F����^�~���������������tq��~�	C�4{��3��	�{q�{mN�De�XڄD6'4B�ro^�o�HL0p#	�p���>_�(�E۳���d,ʂ���S��ܾ{�9an䎝�.�8���^-`Bn���j0.:Z��nˍ r�ɕ��zjd��xV7c��O>�f\�)�~�&���7�|�}�Z$�	�I �ēo���$?��R$9��S*�f�+���C�R �rC��<�e��H� �d`�ɤ�6���	zSJ�B��s%L�F��J2���T#��9�"wr�d���Lt�.�Z�Bp��<�D�=�8.��&`f��Z��>#���$�C��.�`��$�"�;\s�j�	�tF[*_��LKU���`���β�i�ڋc`Z*cg\ T�P֠���cqe,��NQ8R��{E &p�d(kJho��Ȃ	v1�X)��zR�L��0�0A�LF���b�I2)�6:]k* &����@F��̨���#u�:T�M钊n�[Q[y����	,Q����@��e�9�cP��-(��Q0��#�7����QN�l�6]X��H� �KӨ}R,V/������%"K�m��Y��M�&L`�2Y�P�C̝�eMj��������V)tt�1X��~�EY�SO�5(��H�)jUE�s�QU޴KVj��ey��1�ҕ��j��&��0.+�,�ۚjB�,'Ȓ:��{4��Y���=���E��J$LS����V�"�ee���}u��Sm�aLVM����Z��)K�M}	<D��yo����8Shn��M��-Lh1ݎ-˧�roS�K�o�Q���:���0��R��&��`4�����\(,�]��Ӡ���j��/Wj��P�osb	�Ɋ�t���]�a7~s����������'
%>�~�k�Os@&DuEu�kTgC��Z?F=���* p�15����h`��J#P��0d?08R,,�*��.#P��A������uyؑ-���nS� <l��Ű��֍#��7ͺW&x�4rEa�>L�2B�>#��b[Թad�Y��`��J#W�pqv��¬%ϊI4s	�bj��;4͇Lp�)�5��lW�<gڷ�&��l���c������X��/8g�=*��}zy¤F���Y��JC�`�k�9�l^��C!���� x���o��4�K���85�����y7�~ o�D��$
KA9�����Ph�a�ǡ15}�7_��/��-q*&�oOa\�\�Lm�œ���;(B�Z��ڴ3򬨻���^�a��aNq�J^K�PB������pN�0�q�`�j3���SYl=r�>yy�P:�b2ON� (
  rU��T<���]��:��:��y�.��s���
��80��� 0�C��`��خ芕�$�涉��+�.>�BC�n��馎TJ��
v�ޡ�&8(��ؖZ7�yr��<�E�ÀK�5I��Ai�Y&x=��Óy����_!���iXy��3h��_Z9���~#�V4��V�<p��)L�R�� %:�Z��~Ehʵ�X��,C����[j�v� �˲$+�'�{�CoDr3b��S�.�d��[̿˟���!����,�>Y�|�
�(̌qz�7$m�]���'����\�컟a<(Xa\V"YeuuY��O6��	�2�2(+v�+�C+�ɚo�}BT4�>g��c�j�Cܓ�E���3�.K� �˲$+��ط.�x�0.ˑ���Tߞ7�m�D��IV½��)3�mC���@�2}^=s��e�2`\}ʆ>��w���umf`\}ʆ>���)׹����nh��@ZŮ��r1�9zL�[P�]�D�u͑��Tgv_/������z/b�E���!�]�K�w}�Km��ee���}�q��dQC]�5���nh��(G���msC�NY�GԂq�	�7a��*�F�9I�QNsJ��H}}�T%�y��{c�# ��4,?kQ"-���zy���W�Waᅲ`Ь�nn�(�6^%L`�P��e�>�F�fc,��O�,O�bgʨm&pJ(+���ӆ�����z��-����5���ų��C���~9xS�l�#d��a��7�й3�����2��k��^�՘v�aLV�W�p|��{dY{0�s�qY�dA2q���������2$���{���e#�	�,ɂd�PS3:e��}Y�z���}��ƾ���=��`\�'Y������2���Y*��)z���S��v�a\V$Y�K(w���K�Wk��j ��=m�W��W&YЏ��i���� K��Itn`v���:�.K��ˢs��w�C���={��܀���Næf��3*�	&5��}�|�|��e���lC�}�{���+���qY�d�����[�o�!`\V Y������5���aU�T,�C���}�4/�p�4��5���ϓ�S��(`��j( �l��[��f/��5�k����{}���?��?�	�������͵�qk��ei��p]��+�k�;�bw�:�#�����7�.�����l ��9�4 ^0.��)8달�~�9��qYtNa���e{�ﺖf�˪��~��.�oU�RK��Վ���vn�d�q��m���͡OW;���9���%�B׋&����ɇQz׀K�7`�ޚ���Ao'�-uiԕo��.�j��W"&�lI�8|ok6��0AoGz�m�'}����pH�	z{���i(���Ĉ�H���4a��tG��b�0���k����g�Қ�i+6@�51�;�Y����u��I�
j}�M.c5jG��8W���&��[�L6�ӛ�QF��uM���N�x���������%����o��s�	z;�{��l�Ը}��u���f�{���xE��#�}��0�];d�0��Lj��<�i~�C�X����!��3S����73`��tɁ��U����_��_��K̠���M�[��0Ao����j]o��z��p�	z�Kn�Yb��^Q0*�lT��i*&����^o��w;^�Z��9/���쒓Ǣ�ʆ�}�L�;3�k�f�%���z�'���i���1A�Hz{�{�9Ȃ��	z'�{pC"��M�`PD2(�n�>k�b	P�4[�&�l�q�[�r�;]CJq�%U�fUc�=p�֤wF��0/βaǄ	z���]��z���l�΁�ҽlpG���o�����iK�22Yy���[�ʓW��w���b��<��6�Yi{q����J��7�ﯮ?�}x�G^���'LXB�%h\B��c	�+M���HK�\KUsU��l��E���DK��{�#p�%���`��%dZ��%m�r	���#�/���q?�"�����Kh¸�	Kд�>��aɽVu��������ͅS� �&�}�ᔮ�$��D+x{(��0%8Z�=>\z�%4m�	�@3V��Z,���[��0a	t1ca9���*N��ނ<7U;��]���,���Y�&�+�=����H�X���G�\-;i�g5xZ�O���N��І�֐q�?n�CvF�������t1��~l���:b	�n���͛������~wӯ��m��0AˈZ�0#�L�زF�q-�_AK�m�1���D��z&�	Z�3��u�?EG�`�!��q�ks�v &�Qo�S+�۩6ə0�K����+���~z�uk����3��b�'=�Π�i3�	��o
ӓ2��aW+��%(u�V%|�fg���)���w��
(����W����d%���S����Wȶ��=�6��6���p�=m�����+LhJa��$]��Ͱ� {�{�b�򰎡skЦ���	] �/���'�����A)������Z옾2#���̈́	%�ي�kO��[a�{�G�g�d��Ꚑ|�޻�e�`B��@�wѬ9�\��	}b,�A�E���n��N�]0&�/>����I�d      �      x�Ž�����'���)������kq����@EEP��ED.�����r�$�
P�F[{��{u�6���deeefUe�ď�j�(*R7G�����?Dwb-D�V#˟��t�����"�?�8��u�-��;��?&2�����o�0��_����#/�9[��'�lwb��p��"�_G�� 	T��e�x��U�ٓ���r#?
ߖ�(���rߖ���9W��ϿR�?����� �h? \{�<�����S&� =��lݦ;��K�q[�_ҹjX���~[�;�j���JuĿ`'{���Q�������K���~�?��?P]����oŉ7�yCI��)���h
Ke;n�
�p��l�LD��kǜ�%�C�v�\X8��Ĩ_�/��p=� �����J��o$�ۊ|,�����nS?)�r�m��Tf0�����<Z�$�2��-���j�6�����/*�� N�փ��@)�������J�}��������z��u-]B�$�?L�s ���k�N~��7k?��8��䏽d��Q��o@����$�}�T��ਙ�x��}Qe�e�p���2(M&��� yID������mD1�Hӌ������Cz<�(�R����Lde9�;[���L�� ����\�x'�<Z��n73���0�(�����ŉf����Ml/��emIbĭ�ߚWt���~�~����?R��Y"CYٯ���_�a�N��5Y�#PK�3�|˜s��������9�Cz�`0@@��>~�I�AX1�[��؉�Vj-&��J�L���OH��Gh!y�N;���y�IgMj�d����?��k��o䚰طyR���g��oT�eI�C*���e�Q�o�<��Qw�ɣH&+J�U��H�f�!����ŝ{Ji��J+e��r �&(�-���R��e��U?�I&��L�I��l�y{�ԀZ���=�*����m�&��-���wk	Ǵ��o��r����:0A���<�V�����$z F����d`^Q#$��� �L'��%��!����^�cIi�Ui�M�E�� 넏����� Y`�q���?N_��� ��<~�bQc�j����}�Y�1%Z'��荎�;��}��9���s ���8�G��
8��b rkb��,���cW�"Լ�>L�����0J`@���eD*)�gk�l8��7�Hz�\��o2��p8N���oց�����~~F�yq��M�7�|�-�C.j�*�ac6*)Ce�ݘb��g��Y�e�~G�<�<�[��l���Gh�9�\��(�n��,F�,�b�<Qc�H��][kwg��V@��ⶖ�&�m��F�mk�x҆���?�;���
�W�(��l�a�Ϋy�4�|5��|����8@ u�84G�؏q�P����X�����Np���������IT��Fk~�_�/���Q�(��u�QЖ�� ��k����D(�y	}�|`ށ��C+Phv��
h��<�G$ӳ�U`V�6p
�8�i��^��.5����e\�sM����e7���M�����`�O���Z�C_e���o��﨩�yߤ���n�k��yF�}c�W��
�o9�_�O-���G+PxV�UM��Z��v��ш_4}�,�my.�7u_)�5��Y��>��K����#h���ީ�B������z��Mӿ���_��gFE�c����V���H#�p����>5@�'�4�<�A��\=U�'s�y��>*��þal�f-�;`@����c��y���<5���+�1�@����S��4��ߙp�+�I˽��d�7k��鈥�=�����3e��x�W������̴��"�����Q�v�<�(*;���
��0N6�&�i�M�ڀ����K�vh\�[�ͷG�����[}{aL}CE�3��V~�/��O��mk��[���t/������԰��I:�V����OS���x�8jq*������c�P���^(���*u��x�7�Wɟ��~�Mό&�x��<Z��Z���EƑF�Qm;���FR9"�(�P;�'_����%����ȩ�7o2:t�:�"XǓ=�łG�S8��ȫ�eF�s��xUH�C˞�^+�G+P��қYI<	rf-૳F�g�.}& �R�CU����eO�?���j��V���x`w���}��=?�0����k��'����ͷf��5��(~���{�Z�o��~�m�9�h,���~��V�xo��`Ꮏ�xc\H���;7�8��=��_kS2Po������KMJ��w�v$Ǵ���\m��D%:��<�*�{���}s?>Y�3]@bwL+�ć.PN���)C��{9g`F�|�\FO�H��Pk�%�.��$p�Tu����'��W�-������lP�8�G+�d��R�����{��xxh��C3ݳ﵄C6r�;�zi�]�wg3�H(8O<�`�`4�6�F��u�ST��������/�-S�y{Mb����`6���Y聼��i�R2��KB�n�*���')��"�h:��n�YK0�����/��d�7�H�W;RA���%���BW� �����u�	9����p�G��,׾�쿓�@�R�!��!v��'`�[A`��������)��~ �B�m� ���Q?���_4e������Ƥ�x�]�F��,��?�l�V��[Ғ���(���Q�V`ٜ]��:�M����"�4 �a����c��I
8��F༪��Yc�h��`4��t����yz��5��Պͱ�g
j���X����A��
4}#�`*�|s~�ѵ� ���#D5�qbL�˹�*�l"R�KT�V�o��(�/(��n%�{_�THK���0%X�J��\S?V��^��DVT�n�9���lޔ�_��Ux�%}�D*#kL����w�v��b�)�m��k�=��J��(��h�_���3���̟j�"įh��%<-oͿ���|��$�s20L��k6�9{4kT����[�.���W��7��x��\�]~��4�̘ ��p�7�V��]��Z��:@V����g>`��8���_2�K�_���ƥ�΂ġM+��wj��?G+�fũF�';jUV�҂l��5~k>�}�u�.�y���`�y	��f����)-����r2	@|�>��zT^��`h	�3�H�Z�Eh*(���y��n�<RlU[jf�g�g-�o���d�Ӂu��l%J�A@!@�:��L�9<�,��|�:#oBY�ޤmc����l[�M���t�J���O��z~���Q�/3��F�0@\������ u	��벦d���̠�U7w�I&��h�O�Q��������az��s˰�Չ���^
�9�m��A����[���~VY`�+l���D���NЖ�W~N;&� �q2�V`���+@Q�[.<����Mz�u"�L�8��?c��Y32z�A�Mć�NW l�<���q2�]�?���8�0�/��^�I�����G+0����SZ����c#��q>Ms�U�Ymx0���T��ˢ�kH��w$�����P P7���4��?�Kq�'}p	�wn�#aßl�pCN�"��������^y�:���3'�%�FK#o���02{&C+0X��wˏ�1��]\��W��/�����8�E7�U�Ǣ	�&з��<��+�B��Um+���e_��f��4��oE��E����v_�pSm&>�ę\X�L��D�|�-	�6�L�sLx/P��|
Qų��ܓ�z��1OO�?W}��9�j��v�.�ے�/x�������J�6�a��|㹳������ #�<Z��#Omt���W����-��Z�GL�p�"�71C+6��|�[�Lb��L��g[��4a���w�tߥg�[��~Q���xS��U
��Ŧ>�_�4�8i&��T�~���SE�?��8>�{�̐��K�g�|�����9[�g��s�^%�_ӵ3����1�ө�,Fs�є�Ǎ��˰�&��3
���c8��|��<Z��NC��(�    ��ZK�K�q<�g�2��ia�,_����a�Y���c��픦�t ,{�?s�Sl*���X�3�V�'e�EP&�T�94������Y/Z�
����6��gOc���a�w�{���;�N�i��	�鐋���Ɯ��d4l�u�mi�Io��b����W0�׮���3��i&�{.�<Z���Ӌϫ�ĺ�X4� �*T��wZƌ�y��K����H2�wn���2�A5��x&w�U9�� �k&:[4�xO5���e�bˀ	�#�����Y&#��#���7�MHEH�K��ߑ�%Y�+B��K"A����^�?��"�\�08�1(��=�_O4�� e�ʰx@�I(�Ù:��f��V�f�:�k󠁢��N�D*�V`�kG�<jԖ&�;*h&0���È�y4\,�lG=�ҿ�/4M�zj϶��,/�!,f���v��h���c����a_�$�+LȣȲkڕ�V��t���I�<Z�M�p��'2m�
-D"/#��-�2��h���Fg��$��$Tp�����t��q�Ϣ ��0�=ĉ�f@	��X�����
n0�@�vʓΈ)���B�ZD�g�M��[�֪���S/�
��by�K��d-Ǧg;���%M��x^���<_n�MQ�c�$�(w��Z�]�;���`��8�J��i�S��֜��:�bq�aY:����p���*��p��l��.���]Ά�RT�`���fݡv����X���������f����diUi(��\F�+<��&��4]h�Y��R��`0��"jH2
 ���p���DC��Pl^wxt��l𪎔D}��#�V׿0�p�#	;-s�L?DJvx���:<��/�I�َ�K`�Ź�}wW���P�u��_=t&6������[�
,�c�fv��꿶����s���4Aa�X9�)�T�(M�V�	�6G��m��[sڥ�+�ت�A���!rz:�
,���*6�����H���y��g��9nr$ɰXn�9����8�=I�'�#~Y��Ƥ?-g���j
�� ��~�]��hCaq��,O=�{���;���
l�x(Q �zѝ�&�hF~bk�G�E�x�7�*n�++F  ]�l�8�����paG�����T�e)���yl=Y�X�� ��eE���K����-a������l7��g�� ����+Z��[*I$@.�y��r&-��Ϋy�;\�qlԪ����?u�Îʻ�L+cQlt��_kR�|e��9o��bZ���^V&�����^�<��w$�r������7�.1clی��d�H,�'u	n9��bU���
m�K�ɨϬ�)��s�8�z,���WQ�p��=\���"{�=8n���a�E5�*Gw��X�q�������N�-֘V��]���4�DO���(Cӷ�?�j��z��>ļ9 k��$g�|��n�G��w'�6۪)� ��/�24
/py�N8�,h���x��2�����M�z��#����'�L^�)�ҳ�6ܱў���P�z)\n^�d��݂��
\�arA�ۻ�3��Jd��;E��7o S�ݝ�ä:��z�<��Z�l˃�֧J2n��Z� �r S1�<�ᄌ��$>�O��(������h�KW�c��S���2����"�U�s��bW:lzjo�
<��y��0�
ڟ?|��i����e�/ev#U�OW�@e��@��ɈC�նت,�a�Q��&�cձ�v��F&s9�qE+py��dE���o�ɣ߅��4�>�8���e1S�/_`��tf�[��h��5]���k\.���UkZM-�����v�s{E+p��/>������6G9� ��w��3삜3�f���5g>����r�Oݪׯ���V#�x��/��,|9����
���""~���e��p��#�(-���a�g�N���xu�N��=�m�����k���x� �{r�g�HY�ŋq��3�P,�fHM��^ۓ�B#j���+�mo���j�lf����ԝ:%xh��}!}�:� �<�J�n|�����1��O=zF���B�����Me���cv�q��/�j���ުԺ��T�{!~;��4�07�n�j�Ax���	��jp�ڌ��������LmYTG98�#�l	�><��:T^���P�i ^n�ڬ�����$H� ��e�~�qyC��c��{��M5kW-�N�n���0��E�m���b�jX�Q���+��f.��W4�;τ�%Q�#�Np�b�*�^�ݿ����J1`����O�� T�Q��^�hs(��شù�&⸼-���V��Ò���/=���ѵ\�k��&���?��/"�>��*ڠ{��ֈ��Ais�?eG[|��{bE�ޘU��*�D�鮂�٭Mi T�utj��ޟ*(���
����3��x�x���M�Qt�Q�h)�*l�������h S��6��b�?�`��(~�);����R�4��v���_/N�܁��UA�6IO���[}vk٥4�)�q����5��{t3&X�� ���&]�;(��à^��p!�������D�?�f��t��}�R�p�| ��|N�3���k%T�GEZ;��fg�i{�=�S���٣:���� W��ɕ+Z�b�J�w` �5Be3��gކ�v�!�I�r@�K#F�^θ�7�$O��P��'��w���U�:]L��\�e��zNoZw�0}�0A&�G��h)u�|�"�Pr����~0I!��o����]�,J�/��P�	�? 2f���!�Y�=zȉ�E��S�]yǇ��4���݊�;+��$d�� � d�j��v�����j�
�ɢ%�	�y;9�O?���p���t_���	��x�I������ӨB`"��R�ч&��of�DuS�w�XR�x�.�Qa����Au;�ʝ���B�n?�'��9`���Z
�� ���f��Rʒ��0��N0Y�O�T�0\Gi��[!#�!0��񨥷�L�gIEwqƙ�T-c�P���f�>%d�`D�v�ɣ�w��<��<��+φ���}-�0�Ј�ߨ��O_����z��L@��ը����H�8���z�o9��U�F�S�$�,_<�p�[�YX�Ǟ~���ҳCS���1����ufB8b�R��?=6WnX���ƀ
��ӝ����R0�%�� �73h�^[�t�X�� c`�]�h����p��j�9���p/i�!75{AW���Rة�?�Ϝ�dVn24 0�X̢�>�o
��d��Z�d2���1�`�\����1��*|��VQ�(�Y��VҲVqv+T{!���%C�M�,�#�.����W]���`��n������
�5{���u�[�6�NX�fKr�;� ���2
i `n�,=-v�3�~07豥�D�<WƟ��$4�觯���s �3�L���w�֎=��oZ����V����ð�N��6ߏF���ʓf- �ݽR��r�|n�K�Kp�$�G+`xn��+�K���^�^����[M�%#|�,��%��a;�(�W5�7��閦}\�O�\���H�Q��;S����;��� �s�_��u�c�{Ob|.2�'�'R�K��{�=�Lǌ��Rv`c�lKD	/m��]��7�\i�)G")�<iΥ�>΁1� WG����י�?s�,��(�Ʒ̊�2���XN��$��G;�&o(�R�B���Y�ըJ���"+5��[lHU���NmNK�50kZ���}�1s���W`n���5kB�΋|�Q�3lg8%�C��e?'�R2&�v��eu���z+������O���.��Q�H�1���[U� �s׶�r|:)�K2�E?�SЉ����}����:�AO��W��O�X�d�U����Ew'k'��:�B����洭��)Z�� �fݽ��vm�=��%�ν�_L��3�˟E#�]I��˞g����L��&&�eF�'&�I[�ִ��K��J��	�l�����~$ʝ��|n�@�
}Go眘��H���V���a:7���I.�����lkzr,���wJ�-���f���@�Z����4#nA    R�$���_�W�����x��&��n�-)Un��Lݬ����jr#�u&xYY՝W��z'n���(ϋ)��؜��Ѭp']�!�Q�"�$���D�lcDv=��x��7��l$�sO�iZ�-��.�w���Z��,	�DR@���\�ds']a�|q_wq$�'�[�'b_���e������.HaO흩H��Gr��r�G'�C]_I��5��Gx�����h��5Ԅ�`�M������^0Ye�qƤ=�k�M�͉F�{�q�P,���vV�;!��R��#�{2�F�9,�S=W/�3NbIoR?����X5�,X+Bk�]	r8ݠ���(w��Q�����5U�8��W[�24 6�dπ��&�<ڽ��=I���@Ìy�%<e5fd~q�w?Y��>�f����FXO�}��Sy4 -�0� �k7������Y
��o�'|47��ە�>��>��S���)o;�!���!���r�|������`yFi��:R��W��mϝ���4K&K�,�e�\!sf����م� �ՕV��-J�氫Mzcag
�*_3)Vzn�Q��yǰ<@�g ^P)E��	)R�qt�P���h��Yb/�ץ^S�P�0��}g)x�7z�Ms��Z��n/Q
�c-���{�S���>4� �j2+�?�h����YC�Tf�Ӫc+Mz��]�WR�JTcչP����k�nB)@�3˲��]Z�����iP*5K1jO�t!mT�����9r�\����^v�"^g����s_�"��ܭ�� ��F�\si�&��z�������x����d�A(LF	�w���7[�8"��>�V'��z\��L)q��`ެ!�4 �uSV��	P����4���h��&mVZ�q�)�aM�0��6MKb�x���@�ɕ�� -��G�2��)_C��[��"�F����`�����%��)W=�K��vk��2բvG��`ó�3�F~j�x�?�&�3!��@���A��0���$j�xz�9��V�_��ވ��{��)�;wW]/ڑ��׀H�� #�h8\:f��֌9�L����c&H^�T�X�����V�Zv(���S���5��k�W�`rd���kP�[�&���X4v0�Fz�(U�m���<�A�8u,�W������tJ�bk���g(�:�YG�M����m03���M�ī��7�����,��V`݌�W��0�=���㱳���l��X�����	wRl/�b��W�(��䯻�����f9��w���_=8���.����K�lX�w�I/�h�I��]M��5�4�m�q�*2���34�֌�i �c['�WX��W�m��3}��l�Ϡ�A+n�U��%���Mjh�c�2A��lF�(�-=a�{��F��}l����U�炅_<����w��uZ��MqX٨�Y���D�
O��5eq������� �A��8bI>��Dx�>j����`�I,Z�H�L܃d����)��� L�k�]�����=��5Z�#��k�A���G�+���-!�
w�#5�=p�Q�L�/Nā���q�H֠�jجw:����LL��6���2n�Qu�UCtީ��X4�&1D�@��v@��!�t�N�6;x8�3��$�Xs�r}�=�#�9�
z��4˵&?��I'�>w�!���
qz-���*J3�س1��)3O����$�V^d�
5v{vv,5��M]U��h/�+)ZW���o�_�S����@~�6��v0��G�-��I'vx<3d7'�ڱ?�t��bFl�X���\���~ԓ�I�|��
\l(P��B�"�@O_��/�t�aO���+���T~WXr���b{�dP��(
�vvbiY��z���2�'24 �����H�[��ۭ9<=��X��-3�E���;K�НZ���̩,��⒮��X_`�@�
X�7&]��h �ckʧ�G����(����������M_� &�����^Cv&s�#��
�i����D��TX��Qz��3����C_������O�#9�L<k@K3�Q�1o�5e>kPY���Z���-�&�
�wgAm�j�J��>��c ���cƩvn�%�G��l� �fO�x$m���{��z��5Fx�_[Ҟ*��p�1=mV��$I�_���b ���c.� .���囻p��8%M��+Ȍ�h����uJa��v���^�Q�=4�����z��J��� ��ȏM��R�Z@y`�Rxz���?�(+����L}I�bu���s�[9��2Ka;Xv!���Wl�8m�4�-���9�� ^�M�E�iу8ߥ-�9"YN�\6�X���3b���DDw����N�I�ʃI�uk������̆.\r'M��f��:�|��A��ߗ�lz�σ��t����69ơ(E���<")�.4 ��L�q�h��#u[�Y��Pq]��+�:�Ú�FP�pE(*�u�\��}�9q��0:ϰ+�[Tn4��D���'���������Շ��e\�D���9�� ��[&+�/������I�P)I����>�"F�*8��+C\ʳ?�9���x�M�g��2��ǻ�0�\J�S�Q,ћ�PYJ���ްU�B��[isL�'mh"��3�Tnr��
��\A�;��з��֡���x����?�K�BY� �T����{Č�rQD%�I�4�e��1�tb�֗��uJ&<.��`%7���*&�\*ٔ�p	6�V��<+V��b���YM�_��Co�y7�����,oR������b�,�|Q��r�ʿu�b���*����?S�}-@P{:=�n�Kn���NY<gnȞ��	kyYX�A{ebG�����_�@li0����@\�n�j��B]���nmNV`N{r&��b��2��r���+=��e:�4D~Xn���qҴ�m-M�N�&G#0�Dn�"�^��U��:!w���t�8�m�%A�p�A^2��9�%��t !�r�Gx����S]8��dr~����e�|�����uWb0cۡ^���a��+ܡBUw�D�Jmn��N&�"Ы3`߃Me0��KDX����FJ��X~�8�/^
!�.g��=�3C(Ncɠ����g1��]e�3�m�"�H�����70�Ӽ���a�p'S�ѓQ��g9��Nsy.��[�>�^����OH��֞�%"��s��ˡD�}���C���"���aP���~cm�v7�}ա�)��e�&]�I]h�IyNO��:̪������o���=�W&e���wl��F������m#�w��q��(��k�Yc�Q�E[�*c����p��t�	l�&��V�ڋ�+,��6�^h��y��y�n��g˭�6Ul��E9W���(��x�>[�;i��L��$���������(��&J����$0��d�����Ƒ��U}�������|�f:�F�r��r���l!5K-��4�#rhA�M���7[�L��u�<�0��
�\h,���{�����q�ܷ��� �Gp]l���or�a�Er�4�K@��
��	��q9��]�����lDՙs=w����Ph���x�z�O����<-��({
�40o�u�?.t�YP���'}�"���G�����#��*6�&v�V��F�C5<�Z��"���J[�$Mk��f�N�C���p:T�I��b��ܯ��b���Z��/��]��]h����y�w���j����MxM�ބ��M��j���s�׋x`� �-]�ϯ��*2L�U��>_�*~u��lZ$���b*��4J{��6�'�	����`z����y��)K�E�{�F���M�B�����?'���#�G�O���\�Kz둟MW~��/�O�fw���i��$�K� "�&*oV�9qH�G�+��@��i.��,�|�(��in�r��$ʱi��oNC�f �ń�J1#�10��ݙ�&e���nLs�C�&D� �^)�
�՟�G�t7u�,t��sV/^��.ϝI�w
z��O��nHxF:r�n4���砫dqd-3,&�%&VxoP<s�]N���4j2 {  �9!-'t�f�^a1}uV�B,�sE��3�_�؞lF˿��c$�J�]���dN�M��hy�VK�l3q�f`�ՙ�1MV��Y�=�dƃ���պх���"|0�4��hb��Mo��6�Un���"���8�Y��v��&;�RTA��wlp�r�Ք|ը�h#�Xtw��2Ow).l��F�����G����!M�*��6���ng�8��X�����j��{Nrwk �G��NF�Ĩ^y���E	ϱ]2�e���֛��#�6L}��Z#��w�%e-�rtЇ�+bF��U�10+�m̀J�v���A�x���8
�g,�x<���CE������^�����s���nY��~��V�QY|:TD�4t7�fJl�s5���gR�B�7���h��E�\G��L޽w/h�O7�k#{V��6�gW��V�m��L�C�x��ٓ��d��.����y>]�7u �p�N�MW>��O��	N0(A�T�0X.`QC�2?�ZEn�Kކ�p
��P�����<o��Qs�Я0�������PK�=� �<���&6���`�6l7Z!i
����3����#�$����@��q�:����,f���B7������Qgx�+ծ�n��M��Js��s��RƱW�.���y=���3!�&�&�=������;C �%���Y)F+3���1o��A����m�Q����a�6�W�a��*R�=��`9�K�睤���|��EmՅ��T05E^jW�� �C��z��$��8�n�Ʊ�(�3�?'�S���q�ڙ����o@� �� [0e�����4K���������?�'�S��}���|c�`��+L%.W_� Ss���_'�h�X$>ϫ�tdq�c�����.��rg�@ʮ��+��7ǈS:-�9�s�[҆���O[{w��=��:ELeөfh��"$f�5�l�X�<ӷwv`µ���}�{��e���<\��Y,�:5YTf��7�3|��>�U���d�:�[F��ә���xM����Ϥi9��<���.S�s�=�|�2^9����I��ag&z-���/U!$�����%޷�Hx08���$�̿�Qq�C8�K[��� 8����z�]�)>NLU��:�/̿pK���[��\��ef,Mo�&��[uQ�h��9P�㤜I�W����1��&K%�R�(�s\�ޤ�E�Hr�?�XV-�k�n��Q�>�9�B �P�r��.�b?7�y�u�ۄtW68��i�\�G��4�e<w�5�n?"��dt�qX���c����c���.��1C��ړr��^��� ��;�,7�����?N{�r����]� �fO��B�]1�/������!�\g�Z��>U�:�2zD��0����薏~���� f���
����& j�����o�>N4������P�Z��(u�#+��
]��9�t��~]�1wLI����q�	��y4�,7Cܹi�M�.���S��)��f�cQ1o@���8�(��K��B(6�k�VU4�����k�3Y�34 =7O۹��5��	.1�Sn���d�AsMa7�Mg�.f�6sf{��[�����{`�<��;E�g3�gh Yn�4�4�?�q����/,a8G�A羜aa����
'qg���~s���oV���j�KK��7��m�>�%�<`An��LCq���|���>�������|��͚j��``�ۡ�[�MO��y�:�4�u�h<wz,�Kd� eh��ن��޹_>8�LQ,I���AN|}��EWc{\��-��mt���A�PmOn�*G����%+9��8ȉd���v��? t#˥� ��� ��=�l��-nZuM,+J���:N��
u���7p���
B2��� �<�Ӵ��F� �{�Ϙ��@�����4$�M�c�rPY�TQ/����Q��~W+�T"B%�v��e�h?g	&��lҍ09����Op�[�H��k�^g
4��ƜwKr�a���S���uLl�΢��\���Xv�Z�ؒk$����_���������8M�}x7��3�9Z�X��XS�-���+o;��m�U��$�ӡ�d�Kq�b��)*�V�s�;]�`$��x�^ͬ2_և����@B����	Ekّ��?���'�S�?XQ�PM�{������j`�RR��v^]c�.�h��^lq{���V/��ؾ�n���#����1�c��{��pu���C0��!��;��q�Y?����_NRX�ָ�3�`�M�P�CߔǮ���R/����1&bK,�j�v�3&^�G�s��4NT2=nݐ�����ꚛ��O>�`{$�&�*@�Ya)�����}�Bw��V���ʡ�N��7�g��q�zZJ��xn0,׎J�}��Izn�t짓�fB0�\䃘>C�i��o��*Zg`
>Z�Q�ܪ/6����[m��x���	LZ��ux�V�W��n��T~�0	J����+j��C"]kGPeO���W�i�!9x�qp�R� ��q\�����(�����Ϟ77�n����z�D�qQ>L������z'�gM34���Д,og:�m)�'����U��69F�Ea�U�D(+-�y��j����������k&Wk=�E��(s#��Q����wJ�����f�1�vR���Gґ��ڑ�a�d&9�+�Ҡވ�F�ՠUv&u4���0��^�������rޓ~��u(��{�~�!�,�P�K�D1,�[�����K��b�]�8����/�kB����)��X9�ĭb>b�U��+ZG��g^.�g?���T��w6}�Y�.ٻ�,��$͢�q_�e�i��VWlj"0�zp����K�n�sƸ�WE��=;k-�Ŷ\Aa�?g��-1\TT��كrnY�����d�h���Y(�C���      �   L  x��X�n�6]�~�>�)tI�K�����̮z�:�-ِ���=�eٱe& �	t��}S,t(d"n����}Q/�������y�׏�����P��0�
e�qe��>����gS��Lm��jn�eW_f@YZܖ�i��rQ�L�*W�K�G�#-4�0�
�s<2�aY.������ڻ��Ve�N�X�Zp�S_7m�=�n�vgV�5bmI�0���DđP�S�n��nӘ�dd�o�\��,F���fK�H�0�D	�����;�;��b��� L�3F�$� IS+4�r��H�0bI&T҇�˦�jdV[:���6Mev@��fn�	R�a�È�HC�R�hW��ik� ��y0�֖S�xbF�D�"{S�k��2�9�a�Z�R���O�ߊM�������}q$R%8=�.�(�nѭ�2D�w����m9eD��|�STcx��Tvp�Y�uogm��M�GT�Yt���ྫྷwݔM��s�9�8iti�Z��d:`��٥$�H�(�aę�.�6HYp�Pd�䙗I��� 2������"�`AkևFTvq.Y��c!r��⏰�_�0��`��ŬX5��4�ԬI�3i.sN|�	2J�g�ɻQ���j��<�׶X��<6����uռh`��w���������#�U�ޞ�3�H��`����[wv��g9�eD���.#[��-�������e�\u���r�m�8�<�bŬ�����-�ږ�8�_[�G�V���]Q#0�1� cF���f�a0���_��_��:��?���rϘ��>�pV����Y w�J���1��Ϲ�6�i�*�ad��bqo�';A��0�Gd#(g,E|ȢG8di# GMj�����qW(�
�sl�ͱܞ1��7t���\���j�: �v��8_��}\���'��#g���X��}/[��Mc��;Ů��0�^ƍ��]>�6����T�z�7f�� ��u�0��]8�W��n��l�Oc��0r"�@���A
n6�M� ��)��χ���-�s�Dx#0��K�a�l���X������)�0�Lָ7�1iYu�����b����t���r2�ɸ�:���5���̽��f���Xg�p��m^]���RO�[��������@4
���9B���V����hhkSĸ'�q��=	l^x����Y���ɜ}��_�e���к3z��a��0���:y��=��*����	Ѩ;;��I��}+�x�U��g;���J60�ӛ�	F����T:Hn�M���}"�~�um�_�\�.�P�𖊏�A���{���C�C'U���}Q˄������e�����=���V�2�F�JD�%%      �      x���[o�Ȗ�����@��<d���79�i;�ӂ��� ���j�-K2tI����]d"R.�8�qp�Z���jUծ)�,���a����v_�/վ�W�z���c�*Q�]���I,Ӊ����H͈߮�o�f~?�Əl�d<$�DZ�C��%��R��>-2Db�p���%"�-�f�� ��|Z$b�0���8�$��=����K�y������R���H*a$��u,���i���h��?g7c<:�?L��s?��I1�z�X�IC�L\��2��-�A�0���$ѝ�>i�L	�.U�OۋX�N�ԧE2'�p��
8rz{ (=��O�TB�BL7ϫ2�-w�k��o����8�$�T�"�%�`��?�������8����]���EY��[�����UV?���n݄�˗�*�=�p�O�TF8VL��iɈ���P��x�\m_�o!"��ܧE��h��C0�^ f< x��Ha�e��</_�(�&~]����:��1�}Z��LV`�v�e�qy�>-�rw2�J��i�gSt&ؓi� ���<�vT(f��)1yU��\`�*���*^4�L�9�i:��9F�Q�}|�\W�U�)7�h��h���n�\�|A�ojp5H�&6�ir˙��X����{E���ON��|Bn�9�eeA<�ޠŇ��ѹ��O�U���$Ą!R�aj'����1�P�����0���C�|֜�DP��a�E��5[���͡�//�� ����U���@��"�-kB�sִ��D���iz4 ����#�M-�c�"MQ��|h��Id�׷#	9X��"����UhG���Q��qI^A�!J�o� F��
�E�&�pA�bl� ��U̩9Ua;�ԓ�U�Oȉ�+H?�4	!z��q(W%��q_��	�g-�D�ӢV�r@�
>�L��'���(�]S.s����(SD�l�pۿ�
�h�~, n�X�P�ڧE�%
R�O�0��� ���!ܚ$�M�3_��ݭ�}_U��j׻�e�$�X���9iQ�;n*�g�`�B ����a�0�3�Xډ64�G���12�~�S�C ��n;m�]�-����-}Z�+B �>m���h�����q��qd��K1���E�u���˗�j�]�!�S����6&a���(��2d �Ҭ0��z� -�B7��@Z��&����Ha��^��������O��e��էE�t������8څb��rWaz�~�Jk	N����$s�W�Ӣ�b�:C88��R����yW�y�IK�����@�Ņ�iQ?g���(ǘr��z0�ar�i���rMMv��c4�;���j�>-�I�8E�I�˛���[ʚ��[�'8L8��4D���ON�<a��R��q���
���z�/����P�Fp)�d���}8���SL���ɉ��f�Z�Y�HJ!0�2�io4w�HbV�jUn�V�+���r�˪88)0�#)]���!~_�	��D�׶� R���=��?��OL��:p`���ӣv�d>8X�7��3�i���I�c�̿h��GI۵ޕUb|����p� �@����Gc��i��al3x�w�#aQ@k�^����OB:DZ/���M,a�ȳ�c��s�Ӏ���%2�#u�a~j@X���X������HX*��ǃf5���9�nB�\ a�Ũ�B��Q8�u�a�����9���tf݄@���b=[#�C\R7|�r⡫.>�ܺ	���Kvڤ�h@d��% ��s:x�c��{/x��!��U�D���@~p�~��!e��m��Y^�SC	ʰYu�4�����,��}FPJ�`d�hG�
ʣ��ηA�����]��]�vL:Ƞ���]�LP��b����T�̙���No40
Ays��Z��7ˢ��qG�$] Ģ�~���H�� �С��i�(I���><�[$�u$�1Ɔqp-� 0p�V�]�ԧ��"�ϔ�ήz�e]>�v�j'��fh1��Ed5�2�W��d��3�xɁ{������wR�R�S�̧E2�y�za00
)��t�,�Ǳ��儑�nlt40ȹ)��Z1�40Ȯ����Zd��I¯�c���,��Avu;X�i �$W>��F�l��ל!��`W���`����Hb|tV�BYsxe
��[%e�om�4 t���)�15�Y� ���=\&:��i@�5�W� '�9�2@01ֿ�2�s�:��Fp���ɓ��.S/�Ll�Vȼ'l~^L��p�4 �C�_�&���p06�$�}�i@�@=#�x>~I���-R�D&Ni�����
67��q>�:���H1S7�	@��hC���#9�ጮE�/�_˧�{&V?d��,mh�Hd���-|@d���gKV�	�٥hi ��Z�A��O~֊J���- X��B6$}@p4����L������� �{�)T�w�F��|g�h�E*��5��t��<"�gk���jo)�4��l�v6:��> 8۰�Mc�����m��ƟzS4� �����Λ���m�Φ�2�]�i �ن�l�q�#�g���~S4� ��>�Ζ��a�i�ª�6M����̜!1���	�{��C(,�i���o*�4��l�wvq��6@p��;�h�ֵ4��l�v6�9�> 8۲���ތGNζlg+�v�u@p�e;�m��4���#��U�h��E
�m��Ao|�4��l����Cύ����q�km�4`T�1�ׯ��s�i�.��+�t��bQ��u|��~��������u�_#�0<�v��7=ɩnھu��Ҁ�5�҆pi��A���Q0��_ki@�5⢂pE�c���u9E���YϾw�{sz���!k+� >O��
�~"��n']SX�0�����������x~��ݟG�׆Wv���рIk���bg5��m��.}os0y��c�B�����Ig��E���sI���a��Pn��U��B���n@�������A QXu��������sw�3�8Y<c�c:Y�I&u,����e�~� %�)�B8y���f��l�c�!;�O;;.'-R���Y��t�ϛ��r���>������TM�k����.��$��^E坬�bj
��[c.=�����n��G�`��袳�{Ҁ�k�E����i�~
�$f�����*��#k#�E9J�	��X||�6K�-�a�1�%�;�f'�0�Q2D�5�5J�8pv�̺���ԧ��Ns(����Zn����a�����sPy�"E��/
QdM����p�ኢ5}Y����J�B����T��8i�ؚ�j� &uf���碆�r���l?y�q,�����|DsZ���0�uC#���]��{V)�f�)�&�g���lC���	�р1F��1�^_�f�l�2."8��r�0��sJg݇92�L�`X������6�~9DF/�E3�mؓ):�t�T��Χ��7���n�Y��6^��e�r�ϫ�*�������C�����t������7P�d<=_�70��p�4�2*�vY_��C聾A�O��xy�t��6��码U�?˧�>0�v�c�9�1ț�������E*=F�����a
�8������oU�����}(�֦!�c�1-�t���>�u3]�]�=�}�w������#��!bf�׳b8g���'���/�����R�0��������1��C�BM1D=�2�����F���Ŭ�U?�}�_�(�)3��s�wҀ�j��KJ߫�x^���w�������}x��Q�ku}\U�����3}?z;u�,|�Y��N���I�:H΂tW�'-�tpJ󑴨����}���3�]>��[�.�>��x*��I�E<���P#�_p��4yq�j]`oi`��1"�^&�M��q����F�0.�+�D�(4/.S�40򆑉Ŵ���a
\I��p$�}���E�OË$@!o[^$o��}iGӱ�c�"饐�SN$��lhi`����$@!��`�X�����4@Ra��1�#����}l�� �䲘   8rq��I�����"�΁�_��D�{�i�H	��3����e�����14��S��i��x'p��6n����kY�<M��q����;%�i�R�q�΍��jZSURv�T�TMr����:�o�~������Ա�ȅ#�����"lm��4��#�#��P����U����@-]s05Ơ&Răa((g���/�u4�l������ӧ�P�h�>}��u����9����]X�(u�����❪�/2������;��� ]�Ɓ      �   �  x���M��0@�3��G8,��c;��U�
���J�&Ai����J�o��*99�(o<�O��"��c~��۰m�F}l�~��}���?��6�F�m��b��ĐFC���U^�^�,�;x,,�Mӽ(ɘ�B�������4K�A���m��/���`�Sݯ�l`�cr6iWb���~�"��}V�'�������~�[����28{ѩ/��˸�ђKڗ��Zc�C2�a�6�a�Z�j��J�%�$�>�ֆ$}*0$N��2'�JɃ���(��PJ�X�g���<w���~��&��D�މ!U��|oHR�C	�x���֧���h��_�^�)�/1�-��K5dO�<24����8�����z�kZ��^7C/���q�ԛ��o���6��o�3�J%��/�j�eݯ��>��?�%:Mkb(1\�rZ1i.14��n0�OW瑡������ħe�� �\q���Okbh*���i�{v�>1�pˇ[H�?�>�O�1ra������r9      �   �   x���=��0��z�� ��ؓ�n�6�v4SD�)��"�$��[<�d301��r��U۟|��@)Q���|�_��q���^�U�9QV-5��-
�~�Z{?��(�K��XoF�Tj��v�{���)Z�oܘ�rZ�ƥ�3��� 7��� ^g����ip?�8�E���DNZjhKys[<��'�Rz+3e�R�}@�+Ns�      �      x��������/�����cb��kq+.=�|'P�*^�A�������� g^�<�T*�h�Z���1�W�6���dfee�%������j���Yc3�V�P��S���x��?�������S��|��G��^�?��$GS,A�l��z�������:ЫR�<r2�,1��Z�N��We�0Iǔ�~o�[��Mr�EA��~�R�w�{'9�>h� �ho �m��)V3���k�r�ꎭE�)�rh�c�`�.V� ���:��Ǳ���Bdam����1��oM���m6^������c�?������V�O� � �`�^���iTŻ=��/-7������������jc��m���ϿR�??����"Eh?!\y�>���W�S�?=�6�Mw���������sը����>7w�;,��\��+�]�DB�������?#mFZ�$��H�z��8����>p�E{D���>u۲�ژj���/�/I������),��G����>W��kaR>�AF�^/���]+��+��ݺ����ڏ,'G�G��N��)��Pޡ2�?�7��O�6�Z!���f*�iSd��yy#�.W�øJ*��xl�^�ԗI�lM��Ʀoc���f81l��0E��ǆv`bK˱����ll|~�]���N}Pd��9��n�.']��n��}]�Eӱ����LՋ�=���.��1��Q��A��Y�-TV�~�57��[��e�O��9�H�Ə쮌te�t&+in���db���j�Z6?Ϸ�q0�,X����9������΀� 8ۀ}��O���dv�lm����D�9�{*��a�_%u+�����Ǿթ��g�ho���̹��S@��@��`[+�Z���J����&*�}�>C���^�����^�Q������_tx~��g���>6���R��^���2i���dsf_���L}k�BX���X����~g���'��"�~�꧘�f0���	r4��k����*оM^b ���Б�x����Aw�~���ho���|�'E�ͥ�i�b�}�,=)�|%ғ4X�{�����|���̭/l�H4ژC��I�����O�q_W�>s�;c�����_������-~q?q� ?K���(4��n���5�|S}�o�_�Ч�-��S��9����c�	am.�Y>&��E;��д������^�"�R��_D5A�:��J����ُ`�8sS�� x�8�Ⓠ��a��W��������;��_m�7�.p+ƬVz��g�7�~ŏ����{����3G� d�� �D�̩0AsL�w�'����O�����J��l�P%���/��c�+�yJmx��E{|�i�)��`��������XH�dH�LQQ�sQ�9��+žf�n&��/*�����|���j��g�Tp6�*8:D�D��Mӿ��﷫�"�g:�?�ێ��<�t�H��$kl�>�>�G��<�d�pq*6��z��_4����OT��G|C���Z~����2�7��/��=鏴��~�p+���9@g�(�F�S��gء�()�J��oP�߬�7%�����L�3e�v�"W�]��������*�u�">p�v�3��1d�tϪ�K�b���q���Q+~�O�XB�G%�P&*�=������}n��q��Э�����Yh"��:����[��-�w���J���ky���P�ɢ�1T\�7*�X.Z�{��<���NE��^.�7�	 V�J_y���pY	dN��ÿ���J��X�{mz�Cԭ����:��\es®�o��0DN�RT�X=��ь�R�0_�����߁����S%��xx��$u����'�����y$��ʪ��:�s��xUh ���=��$�W.��ƀ�V`	6ˣ��u9����c`Orي�̀���K*�U�!���=�����a�as���!��P�ܷ[��� Tp�_!�<^-�`c/�}kj�\�y��WO���s��f[�a9�ܮ^$�7��1D˥,ݾ�v���M\��^-�:=5�wn����u��1��Z���z�_����_j�S��}з�9��1|�!?)W��~Y�bcE#6 ��=V���Y�O���h�s�ho,�c�ORPN� 0*VDʁv�M��j����J֊1�o�ǰ.�>��0��:R:/�'D��+����6>����ho,�c����BE����1r�,�a�&�ɞ��긡;���3Xc�b�;��v"�y�1���w�`�M���2��a4 �'��hQ�K�����SE�ϭ1�Q�;[r���7�^�8a�z �m�(Lz��p[�L=ql��L� *��ƒ9&�$���g-`�77'~V����B��U��aŪ"�X�$40U�Tۘ*)EL*�BB#��ҋ�_$��B��+�Z���f�]��;�w+����;]:?N���$E� a� f6MDm�ZuӨ�k��O>>�s�^�6�Y#}W�PyXw
�6m+b�-h�[Oʊ��?�
��X*�07��������$�������Z[;�T��>����>���&&Y�������Ɯ%Ea!����ets�g���:�v3��h x29��~�I-��K�by��̆�%3���І]�o���
{i��ڠ�ʿҿ�p;	��X��2Ú�������MKN"�j�-7��9�5d�"Bj�9���>wB���!�_���,�rL<gu}X�Q$L����4Uh7*�V�c����C�\��R�R�L�g�D�&`�X�jZG�(�*�߉�M]���rSM���~��~�	Gt��-��x��ik��b�l�=���/z��A����tKڼ�u��r�9��}��8���ho,�c�)ͱ�V*����9����,)��N�}s�O�"&6����Eb��m��ͮ�*��ˬ�|+�vE�CƦ?�(��9h�=o��i�r��'���ѯ�g��5�3[�)ȵ�W���4W�hK,]�$]a���"~�K�tx C�E{c��_��gI�g����2لc��S;@S�b��)6t�f�>�y�k謼��r`w�-�mW�F�Rm*��ҩ)�ª7�7֓]���A���,���>Ct�Q@���@[��k�����"׵f��!���D>�ٲ#�}�(7��3]c��XĶ?����Xt��l�y�k`H�v�[���~Z��+�;������N��W|δǼ�?H:����96+��+��a�[.:
��:c��_��?a#h��H(ҨcՌ��`qc
�q�#R�T���8����'�^qz�ܫ��n�I�q���(�E{��Kdp�X~t��R~�Z`ZUP��#����K��$M�j��D����/�o6���F���O�Ԍ%F�ՏkO��qU�nêYUЩ���K�n��FZ�5C��,�G|'3���.C@�Gh����R8C��]�Cw��R�0�VY����tѓ������a�l1n������F	"8hv�ho	Mg4DW�⹓5%h�㡠�|	�]Z�z
S�g���'��P4����~��Թh�4���۹�f1��}� 8�f���{�\X��z>��\�?w.��0���#2�g��8*�F�g[p�2�`�!D��"h��58��:�Q�@|~��o�4MP �٘�϶ �l�K�h��g�]��^^��h] We�����6�_b6�A~b6��qt����J+?\F�ly�>��0:[z
�.2��W�|5k7����c�ZM��h��	?'+gl��Obmz]�]�Z�>0ft�![M�t��M��0��G�����6\ڷH���X΂S����KB��\�����x.��3������o�2/��=�<�Y�8�;�va�N�⢫�S�8!&������%'�G�@0���fq�F�m|�crl4G�~����DY���q�9 &X�/���`>~���4O&�=�iBt������1���oZ5]kYZw\"C)���{��w[���{���|DhG	�E{����(��TkUN� zgbYE�\�vL�!�4�Ј���K��cX�4{�զ�=� u�<�ʝ�rY<bd�P��O��<    �U͢�*v �g��3sA�A�p.�rg�w�K�}���i1�n�m	R��Oh�wL�r���W�$`�y���r����R�����$Ē�`��ɔ����;-G��|їļ9���Pm&^j6p05վ_�u���H�Ȣ�q0��U�`�Z��duJa�PܚN��C�%�s����@�Rm�}!�yQ��/^�-�@�3�4g.����x��$c��E��c�豿��ѿ�hx����P�.;�����2^q�:�ެ=���"G�Co��h�Ϣ��x�ïE����'�X	7������\��s�e����4sƐ����`�
�K�4{顩�?8�Es$-�E�L����@��9�cWSw���{�Z��-0:�ׅywx����=�)��s��d��`9���Dmjb��B-�*-7eMk����>�M��/���ah
��D³����K��0�3~;�l����@���M��KZG��*ǭW*Z���2Ir�pY�7��q�n�j�,�T��bS�ACU����Y��F�b5�E��J���|���Q�'�,��a�	4cf(�{���M�sb��2�`c3-�`�A��.��C��_t�s�hRe�c��lV�V-?�E�$�'�ho<�㨤������l�$ A�DY�\&Y����z�Q��(�P�v�-]�������� �702jQ���&��V.�x�?'+���A>��GKȅ���@Y��ť�ED�f�kw1�J^��mSۛ5�BY ɑd�{����+"J�(����t��7Q(uEהK���� ��c!M���-5�:b-���-U5,����A�B�F�x켨�A�q-;���������z�������OS4�'B]��+؛r�M�ݦJc��
ɝK�Ż�}gW�}�P�u�՛��]|Z���K%?�[c��x��@F���N[���fH����#G1 �s��.D���(L�f�6ό�m�IZ3���u�ڬ�A�2s뵎r�l�ho<���jպҖ���ъUV���3��<M��,Ћln�v�P3��#���4{cܛ����R�1 �Q*v��o�$^W8�+5�O�^b/���,���(>W���`Uw�P���&�c�T��h�����c�6oǌ@@Lg-|h1�Z����w�3 �8M�gq�<�α�J�P�M+�nm�X����خf���V��{*�x�6�m���s9����h��"�?���E��]�eh N�3��r�x^�z�j(�ˇ��f�-L+��H�n�
nw��Bբ��΢��|��ZM�caGB���2�]���Pp�g�x���E+x�X��a�R-t��KvTPܭ	eRU��N���JE*��ZAFg�E{#�]�L��j�S�H�l$A2	�38�d�UGo�m\&z�xy�`�|I�{N�_�fxi�(9��dG-��Q���^�"l_��R��hohj��Za�@D�Ct����|����rp �A~3��1�m���x�r��:����jY���%ްt�U�Y�>�%oF�r����i��Wڌ�$O��
3�P�������v��f^���V9��=�|�=Ȓ��������ҷx#�K�x��l�OH��Y�Y������)�_�V����f��\м���
����)�l���h���Y@�!��4p�N��$K'���m�Q�ޟ����ۏ����MA�(�³���ភ&�9���f�~E�i��D'4��,��*S��3����N�L6-�y�"4n@�n杝�%����y⅕ݒ�u�
��Y��ӱ^2@�4�㳼�:*��B��% �L�\/��y��0%�X�z��ۓ[��I��P�b���֙�
�uW��^��|�lBds|�
��r����!(�$Ӄ/|)����f
*�����f<�qk��7K�͠T/�u��n�&��ԑ��w��z*{91xE�p���������ȜE����34���*�bW���9;2}���9k�:`l{�p��v[]�r��խJ��0�����rn���9>s�g����}����������=o��J{�h�lVin+ō?q�^��:R[�*��Ų����k�Az �C�@��S��;��o���,��Ő���=�T�NqZ��E�ݞ!�#wfvز��]�|��
|��"�O�-�Y��>�,�b&��f�bbs&�G�}mX�6����j��\T��r0ﯧ����Н=��ھ�����<� J2�ܯ�(�>�	�>M4 O�=�L_k��F��;��x�S��أB^�+z�nv4�Y
����U�%y;*E4���!��>J���ݗ��r�ʫ�
k3֔�<
�Z�W�3��B9��9�EST����Pz"�e� D:�����;�wQ��)@\����Ib�;��ӝ�Xל��vڨX��p����v��q�궬}�P�8�֊?���e7�b9�mea�*��11��CX�+��w��s�� ��C�������ȀQ��^�(�1��ڰ73���Qq[����"�7���^z�a���39�R�GƦ����$�S�E]��S"c��+����@���	7ܒ+ϯ�c+z}Z���8��O��v��5�A`l9R��qi`��aP4�2W��.�����"[��X�)=pEm|��UG��zhe�����Md� ..�<����4��C;��W���g�:)U
���`�+]��	��x�x�X�N��ԉ���Wp�^`B���r�м��
z[�f��C)w�Ѭ����Tq�����ьr�"ҭ�尫��/�;�Sq��ԛ������(���n$�*
MOq�7� XU
���v ˍ�`�:v�ck"�'jS+rG�O>EAβ�3�W�7����B�F�`�R^љ�J��"����`R�<	�!br*��)$�>rb������fWk)^�ʓ��δ����V��NjS'/0emY4���dJM9���)J��?�^�ei*3�3z� �o8�pZ��81x�q�z���	�U�����1�:�����;as�L6ힾ[�r{�/�������,��	�w��Q������j�&��x%�ξ����ֽ�$C>���j[��"Z�TE�ih����p������F�A�]��F�����]�@MZ�.������/oe�]���F�[�lc���04��\mѸ� �)tD7ۏ��6�&�T�tWߠ[�v�Á}Bo%�F�:ΐ ��F�8N&�aSo�ؚ�ъ3���J&�I����-���$FPQ
 6�Q����5�~�ͥg#5�z��aY�DFG�א$���mx�9(�P�Q9,��;�׎���.�[�z�m�!��}IpB/�̢A��{$���44���Ez�$�9�݌�0F�9m{����[i���`rl,�Mag��}(c�3=�o%��K��+	I"ۑL!:G{i�}�-�C�i��0�`�X7kEW��Q��{Ik�)i���s��1���M���(����A�Щ$���4"�3����U����@y����%�Xk��e}خ��v�+���ZiLkL�����[��+(��ƾ���ѱ/Q��Q>���x��ej����U���-�b�64J��;�Hg�m�jo
�tA���3��΀����"	�M"s�-9�{O��)8n�G���":�+���x�%�Y��귎��&eU����{�F椺��7�^e���7��l?jf8-���t�J�i�e��/�1��t�ڭ�>���=��W���@>��fCkʘ�.Vu�2C�b=�8�W��;�(��&}}T�M�|��tlO�'�+����ˢA�C?�̜p�%���z�{�h���w���JBAĐN���i��1�p`6A	�@��z�ٶ*��Z�K�=p$�N�I�/a2�yl�h��$ʧ�m�'���3�\� ����*��/�\-��u<?'�c{c��qт���k��(K����K7��%���iU��NoL
%��7+ZG8]���QN�,d0���L���,2D��u�8/(~G9ϰ��	��Y�����JIq�2Z�n���c��.�)@�d�^֛Zu�{Gf�    ��׸nMwD�\�><�����<X]-0����#����{�Ap�P��WF��,V���ʮ⯶�S'/����:�bW�W�Ƥ��k�	^�� V�����ve�=��&��P�aD�,��=���+�?�j��ܥv�E�>{��Y����gK10���qK�V��"��R��1p��Z�	t/�����xn�B� ���PD�L���*H�܆_>\����Gn4��6-ĩ����I�9���M���)5���N.
�9�*��d-�y����� ���Ous᯲�zO���d�����n�-(e~�
M�,����l�C�s�dQY֜W�>�ۘ=�A9�hH��
f02Ƕ;�4k3�2ސ�*h"�$~`z6��5B�Li����3y:���W�iZ�-�^m�|;�V�w_%B���Hh!������p���o�x�$�I`�f_�Ha�Ru�1Gŝ���I���ڎ�n'Ş��燚����k��E��a�Cf>hyIъ�k�1��j;�����^0^�0�Ƹ��'+�E�����Q�R,���v�))��t6d�^�H��U�hoBQ��c������E�D,U��_� �p�6&��"6g��(o&k|�W�Ŏ���
U�����Ͼ�j+X�Cמ�r�S�$E��
)�v���D�ѣ)x�1kr��,G�,̏�㡥H�cw���n�g��D���$ȢAxЁ����r�N�ܗ �4:�����8�pDOo����wTz�OH[g&�����ˇ�V�&�L��3&��?E���Je9�iI��zQ(W�<�̈2/�rPY�b��ͩ��SF{��ᨥ.�r�Q�ڼ�o:ڸ;w�X*p�s۞t�Ad� b�HRY����V�k��� �,�$���=��G�U톓��mh 7ñ�s��qd�{X7��ik���5%P8���ht
�,�0%8�nD	)��qr�Q��M��$�u�Sف����4��t�nA9lKa�SgbtJ����L#$4�oT���6���
n+�=�ԃ�ą%dU��)��Qb��6�sN�țr�n}f�a�d�������|}��WP�NjB�(Y���!�5��i ����$�\G@�h��OAe��F��d}5���qHu�=�-��k��ҥ�R�w�A�Y�Hh*��^PQ�nHy �O���@���q�f9nqҼERl��Td@(<X7,�]��}���j{��(!i���Q�v��<�΢=>�G�l
o\�������5��CI�d�l��N�*-l��
>2��?E{# tv�/�����������dy&锱�?AK��R`Ƌ��A�nv�US��A��'ٮ¹3w���]~��F�j*��BO�~��h��[9ح�s&?p �%r�NO��y)����ת5�1͠�h����8�ZŪ��e;��h�\���h.�|�/<;���O���Ɩ�:e#�§S(I��bk��-)qO�	K҄X�M�8���nO���D^AI_��]� J�я]�82�&1n�������\�WW�F4e�{�OF#gi/��d�s�Ѫ]�!P�8ߚ3�!o^	�p��{E�p��C?�bT�嘛��Ɲ��̂Nn�G��IA�y:(�:�����G�� 3|WQ�dE.L*��n����=�����u�"�}"��O��}X�Ԝe!��yF�$� o��)�����^�ۍd��q���D�h��NA0�pቋ�+��6>�h1t���nR��^�~&`��Sx���
�i�P'�r^Z��������̍���	��[�_�F�z%�ШO:�D3� �K�c)F�F9'��F_�?�G��&I�6L͡Ь�]5���~��v�jGgl�7j�x���x�Ze�o����E4�C'�����ܘ� ��Y:��$M���T�FsS-o�v0J���Ը�o�ʡ�(��
W�Zy��ڵWS��c�Ǝ��}���ώ>�ݟ)��d�IG~>|8�7��8,QD�p�bm�9l�~���^Yi+aR�R0y�F���'�D�T*�A=v~� 7�oD|p�'���$r����	R���ݞ���yE�����/�ڜ�L/�K)\���Hh�^�Y.} 2E�@	�'�K7qu�9�#g���2��O%�:�%�q�r��m��k��Xm��!�{.�]�	Π���ք�B�	|�2���܂=}qo/G1�Fw2v�N��@��S��q��g^��z�~yR ���w��6p�u���ŧOg�h4����H�"�o��l����+��<o�re��^n/Jw*a��ԧNi19�LY�G��8jG$j���(�E���{MHW-?\.��g%N����4��z,+r�J
4mLM����eg<c۲>+	��Z������K�������F4�M��I��s������i����B���L�s ��^Sf�: t�w�fs9R7�m�9>��3*{W��Wf�H��T=�d���p��(Y�Ƙ�h�$ib��=�Oey?�IO�9Oյ��,+Kڃ'��զ��T���+@���`D�@���p�̅�%Hvޢ�|s2K�L<J!)�J
6k�Zm�6��P���]��>�g�yXv�-���i�2�F^%aJ� l�y[��*0�<p|�ȡ�b�7L�/g]S_P��<_�-�Ɍ���zS�m
mq�_t�kq3cz��$�yX%�<t�@����ЅA���\������z8g�d9�oF��Z���o���Ī�j���^�̥���Э%J=�i[S��En�_�l�����uDZ�h�}�Q�%7�=h�'�x߇'xt�@.`���b�[J	�7��j6T���U)ו�B�]9�h���Tļ�Y��	�J|��i��`��d9�E�x�,ő:��on"����C3$��!A��Tڹ���PgR�`W��t��E�����.=.Q*��AMo��pTb�S	�R4�)e��S�s���gXu~���+˲Dr���Υ��E��e���i���I V:��Y����N+Z��x�ߦ#�:瑢��Ԕ��t&�A�*�V���Ʒ�K=W3I�����&�9�9���V�t���Ōn"S�J*�9Di�������p�/����@�YZ�R�UUL�\
�h�7�y�]&�ە�Z5ʃ�(�I����a���*D�QE���EI�jB�Yu!/J�^��X��j>��W�6�E�?S�}�@�;�|H6�W@iz�X=g�H��`(�hzsY\�A{iG�����_��Pl�?�����I�����م� =r&sC��/�8��蔱�)>Q��<X3�aZrD�;*J	�6� �\mf�q�Ҷm�0i8�"���[c�՚W�n� ��'W4(��.����okQ�bSB�t��uL�.���҆�d(>E��w�R� �K���[�(���5N�>�,���km�J���UM�%P�Q�+e����;��¯΃\h��0alJ,�1�\`�b宖6Vhv��w�����5a!]��wj��s/<8�����J��c?�#c�J��yl��C58f�|oT'T^���vJ7;4�O���|���d胡N�xZ�o-��@���cX����Ǫ��s��-�ǩx����R�b=�G��ؗ�<o�k+o۶��U�WF��@\4^a}5Uu�AFqh+�}��Q������]�mt�\��1�O/�/��`{1T�d�+.���o�1�;��$H�5 (*uL���Z��m���(oX�h�ܥV�-5�.�49P��k�G����ڸz�A�,�j�� ^��Ut]nƷU�զ]�pu�q+VoR�W�v�b;uwE�1�����?�׉�ժV������L���b�I�Ύm�����Ϸ�*�j��+�����7��PY��\j�~[it��0Ђ^�!ko��|��<y�5f:w5�s����2��'��ߔȝr���)x�G�"-��z#1�EpF�B&9%@����FyT����E�%���;������ƻGu 6Znc4X�"��$ǅ�#��K��QC�aO`����������(��u�`��obR�R����0t9��\Fn��N�us�N��G��PZ�!��Eq�外� �  lC��+mp����i�f�f�z��_�}�*�J7����_�M\�-^hP60nd���h,C����� ��fp0}ݕ��G�ݪc�ʚ��Bn�����E<pV�k��6Ȯ��*R�'��@M�-�%��W�6S����bK��$$�Jk��6�''hc&SW�6/4�xT�YAej��|1N�4�؋�O�q�	��Gw�:���>>}�5�]����'o�r7R�'V/V�p|����I1��]�r�-ǁk�HvEB�>!�N�<�ş/F�Sn����Y~��\�'�蛓v��|�A%�R���j��3W`���n���4�T���na#�%��?�o#9�Zf�Р����
j/3 /�Ew��)���c�ۄ���Ёv�@we�'l=��Ck�b3m,�jI���`ӕ���7��r4	lkFI�1�`8E&�W��\��� �a��f�����Ӝ�l�����4�h�]��֤��ʺ[��<W˅�t=v�F`
婯�N�vƳf{���1���5�2�A�2�~�hZlӪE!}�5�~>[Gw���
\�R��ķ��f�B�.J���7k��X^�E����ɫa��Q)�1FX��Βpe�� ~Ӫ��M�^T�Z��Ҡ?`(]%^������ �X�_&sFwk[;4�m,�s⛿e�9�����+/<�/��8E�>M���X��zSj�u�ʚ���1P�`(6{.� �B(�}���n �֭)�h�a0�e���p8خX7��|O<v?%���)�A�G3t;�yz��`$TW�����=�:�L��;kQV��_�;%iX�>=�P����4�AV����
IŊ*�R�"�7���<�XQΧ����^&�/�a�7�+C{Z��em"LKn�oYA�U�#hh��q����Of|���W;R.�7��������C�Eۅ����ㄯ����bq�f@��q.L�pCj�0�Xy��i�5(�
%q��p�8k.7�ƞe^as;1����5~��Z.q�b��4��[6��L� ;q�v�%�����o���?8�I�8ß�AoRAKש�Q�Ô��Ʋ(L��ɽ_�uV�zE���-Ss�1=XjNO{.W_�<�*�ÅJ��Yk��Z��4M�M$|�]��3�c}��!N�i�R�fj��Hc�΂J~�m�^b�դn�I���^k���]�����.+�����
�����Y:Vᰝ����1��ԇ!�$:b�ko<X��q�!�dk6I���Qn�j1��;��˝y�J�P_������C,ꝼ���(��2�a�3"�������8t*PX����YU��?}��(IA2�FH�������RE��^}���&G�<Ge,���jtil����:ݞb.x}y��$�=�m���Is���Rz'�A:m�9]�̛���qz�Y�<ӷwv`�u����}���i��
y���pD��tjrUQ��5ա�1��q �k`������ǣ�ܲ�W�L�� �F+���&�h*M����YҘ��m4�� ��Ǟy����B�{,#Or�ؙ�D�������P��yL���{c4	���s�R�.��e�(9�a3�[�]�x�7_��]�*>IMT��<�/��hk���G[�y�#�b�)3-���ruM�C7��<>&9c���q�\��������ɱ�xN
<ߩ5 7oi^�'k˪~e�i�Z���k�'7` 2
g�l]h�гϼZ��t4��c�]�I�a`�p�{O�~���2�3zn/������Ѧ��W�g+��j+_�G,�O*O�O��za�B���;�}?���lu�1���|2���.g���p�us��֜�C�[��0�kS3��m�F+��N��q{ �19�ţ_�;��C�}bl��(Gd^2&WDL�G�ln�����oġ��7�d�z1�<aP�����t=
=�p{��������,�*W��Ar}n���&9
�&���S�7t҃ȼa��R���T�{�)����:�`����馳��SO�:}�5�P��ך�޸��{�%<�-<AB�s��h�)3�C�ӄS���f~�`�� y�<��(cu<n�$M:[�;���� �l�Q��W˝{,,�
�8>�����h�4ZF��$��W_>XM�*�ܩw��hc6�m��
��N�7��@�.톧��,tT�P��H՟;�`��Y�R4��fW��CVw����)e 8��̇;�Bm�����u���[L���P��b= -On�JGP$�ϥ19c��驸�C�,�.��ƒ��?������o�P2ƞU��Ȯ��5�Z�@������z�������Z}%���L� J�*e^G�eR��DS��=�g+�&)�H�6�/���ʳ\�2��,�N��J��R!.ٕce .G�9�*f*��i8R4�h�Se^��E�����Ґ�A���ZF�׬9��N�p���PeIk�Ĉ��k�<m	���e�%2/J��)c�
��vۯ��z���'7޾����/{��ք�G}�w��v��k��U�q2��s	/�� Y�72���QO׊`�����Sk�����C���(�s��J��A��ǻ?�T��3 ���)�?W�Y���A-$u�lg�q��.��&���uPlv��N�K�:��,��vĈhP�hF%�K>�+���T�b��b��&
0�X<^�8S������]w�� ��؛�B�C�\G��l��^�L�"�'ؐ+p������� ��c#2��G���3ц
	��������6��gld�_�]M}UXJ[H#��i_/1m��U]�t@9 7��}q7�-�8�Ok��͢A�!-�t���Gܱۤ�����Y�'T1���$���V�'I��N���u`��J�Y~�"�jwD�~K:|X�-T҃�MXPʤ�6��p�I�4�h��h�B���4�k�ʼ�5Y<Q��%A�*��Ԙ�m���kk�δ9ي�2U���D�E��p�m�OZ����'�`��z�hoȯ#���K ��o�>��75��E>Xkb&I���u^��@��B�~�*6k�u�l/6��6X`w5���~~��J~�Lj�i���渳��I1���t5u<�G�7�X�a8�s�>��+�'���<I����k�&�p$�G�7�,A�F���]B�{����՚ҢJ�� G��a�4||U�^eI*}�:E�F!\�I��3���VZ��2J9!��Mt�U)�5��Ɂ��.�3񻚹Irh���z�y�.���Scn��5�cs���vA���՝eз��٪OԤ���v�g�n�Ѵ����퍌b�(�"�-�S)�!\?�A�����Sꦦ�y����W����IX*�L�|�����8A���|K^�5�[Y쬣�C���EoEISΙ:E��󌍎xIW�P�h��h6-�e��|6��Y�KU��Sٟ��I��� M3�w�u|F�"_mv��
C�.���
��ƛD��1j��*�o�=7m.�)������ԛ��֖�K�+��:�,��d|;�A�Y��������� �$��      �      x������ � �     