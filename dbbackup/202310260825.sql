PGDMP     %                	    {         
   datindrive    15.4    15.4 �                0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            !           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            "           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            #           1262    16398 
   datindrive    DATABASE     �   CREATE DATABASE datindrive WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'English_Indonesia.1252';
    DROP DATABASE datindrive;
                postgres    false                        2615    16399    v1    SCHEMA        CREATE SCHEMA v1;
    DROP SCHEMA v1;
                postgres    false            �            1259    16400    failed_jobs    TABLE     "  CREATE TABLE v1.failed_jobs (
    id bigint NOT NULL,
    uuid character varying(255) NOT NULL,
    connection text NOT NULL,
    queue text NOT NULL,
    payload text NOT NULL,
    exception text NOT NULL,
    failed_at timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);
    DROP TABLE v1.failed_jobs;
       v1         heap    postgres    false    6            �            1259    16406    failed_jobs_id_seq    SEQUENCE     w   CREATE SEQUENCE v1.failed_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE v1.failed_jobs_id_seq;
       v1          postgres    false    6    215            $           0    0    failed_jobs_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE v1.failed_jobs_id_seq OWNED BY v1.failed_jobs.id;
          v1          postgres    false    216            �            1259    16407 
   migrations    TABLE     �   CREATE TABLE v1.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);
    DROP TABLE v1.migrations;
       v1         heap    postgres    false    6            �            1259    16410    migrations_id_seq    SEQUENCE     �   CREATE SEQUENCE v1.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE v1.migrations_id_seq;
       v1          postgres    false    6    217            %           0    0    migrations_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE v1.migrations_id_seq OWNED BY v1.migrations.id;
          v1          postgres    false    218            �            1259    16411    mst_collection    TABLE     �  CREATE TABLE v1.mst_collection (
    id bigint NOT NULL,
    nama_collection text NOT NULL,
    judul text,
    tipe character varying(50),
    default_chart character varying(50),
    referensi_data text,
    catatan text,
    route_name text,
    table_name text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    ref_organisasi_id integer,
    organisasi text,
    satuan character varying(100),
    definisi text,
    rumus_perhitungan text,
    cara_memperoleh_data text,
    urusan text,
    is_multidimensi boolean DEFAULT false NOT NULL
);
    DROP TABLE v1.mst_collection;
       v1         heap    postgres    false    6            �            1259    16416    mst_collection_id_seq    SEQUENCE     z   CREATE SEQUENCE v1.mst_collection_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE v1.mst_collection_id_seq;
       v1          postgres    false    6    219            &           0    0    mst_collection_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE v1.mst_collection_id_seq OWNED BY v1.mst_collection.id;
          v1          postgres    false    220            �            1259    16417    mst_dashboard    TABLE     �  CREATE TABLE v1.mst_dashboard (
    id bigint NOT NULL,
    nama_dashboard text NOT NULL,
    frame text NOT NULL,
    link text NOT NULL,
    catatan text,
    is_hidden boolean DEFAULT false NOT NULL,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.mst_dashboard;
       v1         heap    postgres    false    6            �            1259    16423    mst_dashboard_id_seq    SEQUENCE     y   CREATE SEQUENCE v1.mst_dashboard_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE v1.mst_dashboard_id_seq;
       v1          postgres    false    221    6            '           0    0    mst_dashboard_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE v1.mst_dashboard_id_seq OWNED BY v1.mst_dashboard.id;
          v1          postgres    false    222            �            1259    16424    mst_jenis_dok    TABLE     [  CREATE TABLE v1.mst_jenis_dok (
    id bigint NOT NULL,
    jenis_dokumen character varying(200) NOT NULL,
    ref_grup_dok_id integer,
    keterangan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.mst_jenis_dok;
       v1         heap    postgres    false    6            �            1259    16429    mst_jenis_dok_id_seq    SEQUENCE     y   CREATE SEQUENCE v1.mst_jenis_dok_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE v1.mst_jenis_dok_id_seq;
       v1          postgres    false    223    6            (           0    0    mst_jenis_dok_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE v1.mst_jenis_dok_id_seq OWNED BY v1.mst_jenis_dok.id;
          v1          postgres    false    224            �            1259    16430    password_reset_tokens    TABLE     �   CREATE TABLE v1.password_reset_tokens (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);
 %   DROP TABLE v1.password_reset_tokens;
       v1         heap    postgres    false    6            �            1259    16435    personal_access_tokens    TABLE     �  CREATE TABLE v1.personal_access_tokens (
    id bigint NOT NULL,
    tokenable_type character varying(255) NOT NULL,
    tokenable_id bigint NOT NULL,
    name character varying(255) NOT NULL,
    token character varying(64) NOT NULL,
    abilities text,
    last_used_at timestamp(0) without time zone,
    expires_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
 &   DROP TABLE v1.personal_access_tokens;
       v1         heap    postgres    false    6            �            1259    16440    personal_access_tokens_id_seq    SEQUENCE     �   CREATE SEQUENCE v1.personal_access_tokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE v1.personal_access_tokens_id_seq;
       v1          postgres    false    226    6            )           0    0    personal_access_tokens_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE v1.personal_access_tokens_id_seq OWNED BY v1.personal_access_tokens.id;
          v1          postgres    false    227            �            1259    16441    ref_data_tag    TABLE     &  CREATE TABLE v1.ref_data_tag (
    id bigint NOT NULL,
    data_tag text NOT NULL,
    keterangan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.ref_data_tag;
       v1         heap    postgres    false    6            �            1259    16446    ref_data_tag_id_seq    SEQUENCE     x   CREATE SEQUENCE v1.ref_data_tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE v1.ref_data_tag_id_seq;
       v1          postgres    false    6    228            *           0    0    ref_data_tag_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE v1.ref_data_tag_id_seq OWNED BY v1.ref_data_tag.id;
          v1          postgres    false    229            �            1259    16447    ref_grup_dok    TABLE     <  CREATE TABLE v1.ref_grup_dok (
    id bigint NOT NULL,
    grup_dokumen character varying(200) NOT NULL,
    keterangan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.ref_grup_dok;
       v1         heap    postgres    false    6            �            1259    16452    ref_grup_dok_id_seq    SEQUENCE     x   CREATE SEQUENCE v1.ref_grup_dok_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE v1.ref_grup_dok_id_seq;
       v1          postgres    false    230    6            +           0    0    ref_grup_dok_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE v1.ref_grup_dok_id_seq OWNED BY v1.ref_grup_dok.id;
          v1          postgres    false    231            �            1259    24617    ref_kode_wilayah    TABLE     �  CREATE TABLE v1.ref_kode_wilayah (
    id bigint NOT NULL,
    kemendagri_provinsi_kode character varying(20),
    kemendagri_kota_kode character varying(50),
    kemendagri_provinsi_nama character varying(50),
    kemendagri_kota_nama text NOT NULL,
    bps_provinsi_kode character varying(50),
    bps_kota_kode character varying(50),
    bps_provinsi_nama character varying(50),
    bps_kota_nama text NOT NULL,
    latitude text,
    longitude text,
    kode_pos character varying(200),
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
     DROP TABLE v1.ref_kode_wilayah;
       v1         heap    postgres    false    6            �            1259    24616    ref_kode_wilayah_id_seq    SEQUENCE     |   CREATE SEQUENCE v1.ref_kode_wilayah_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE v1.ref_kode_wilayah_id_seq;
       v1          postgres    false    253    6            ,           0    0    ref_kode_wilayah_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE v1.ref_kode_wilayah_id_seq OWNED BY v1.ref_kode_wilayah.id;
          v1          postgres    false    252            �            1259    16453    ref_organisasi    TABLE     P  CREATE TABLE v1.ref_organisasi (
    id bigint NOT NULL,
    organisasi character varying(200) NOT NULL,
    singkatan text,
    keterangan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.ref_organisasi;
       v1         heap    postgres    false    6            �            1259    16458    ref_organisasi_id_seq    SEQUENCE     z   CREATE SEQUENCE v1.ref_organisasi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE v1.ref_organisasi_id_seq;
       v1          postgres    false    232    6            -           0    0    ref_organisasi_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE v1.ref_organisasi_id_seq OWNED BY v1.ref_organisasi.id;
          v1          postgres    false    233            �            1259    16459 	   ref_topik    TABLE     2  CREATE TABLE v1.ref_topik (
    id bigint NOT NULL,
    topik character varying(200) NOT NULL,
    keterangan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.ref_topik;
       v1         heap    postgres    false    6            �            1259    16464    ref_topik_id_seq    SEQUENCE     u   CREATE SEQUENCE v1.ref_topik_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE v1.ref_topik_id_seq;
       v1          postgres    false    6    234            .           0    0    ref_topik_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE v1.ref_topik_id_seq OWNED BY v1.ref_topik.id;
          v1          postgres    false    235            �            1259    24601    trx_collection    TABLE       CREATE TABLE v1.trx_collection (
    id bigint NOT NULL,
    mst_collection_id integer,
    tahun integer NOT NULL,
    target text,
    realisasi text,
    sumber text,
    catatan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    kemendagri_kota_kode character varying(50) DEFAULT '32.71'::character varying NOT NULL,
    kemendagri_kota_nama text DEFAULT 'KOTA BOGOR'::text NOT NULL
);
    DROP TABLE v1.trx_collection;
       v1         heap    postgres    false    6            �            1259    24600    trx_collection_id_seq    SEQUENCE     z   CREATE SEQUENCE v1.trx_collection_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE v1.trx_collection_id_seq;
       v1          postgres    false    6    251            /           0    0    trx_collection_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE v1.trx_collection_id_seq OWNED BY v1.trx_collection.id;
          v1          postgres    false    250            �            1259    16465    trx_data    TABLE     4  CREATE TABLE v1.trx_data (
    id bigint NOT NULL,
    mst_jenis_dok_id integer,
    sumber_data text NOT NULL,
    jenis_data character varying(100) NOT NULL,
    nama_data text NOT NULL,
    deskripsi_data text NOT NULL,
    format_data character varying(100) NOT NULL,
    media_type character varying(100),
    size_data character varying(100),
    ekstensi_data character varying(100),
    nama_file text,
    url_file_upload text,
    url_upload text,
    is_shared boolean DEFAULT false NOT NULL,
    webbappeda_visible boolean DEFAULT false NOT NULL,
    satudata_visible boolean DEFAULT false NOT NULL,
    catatan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.trx_data;
       v1         heap    postgres    false    6            �            1259    16473    trx_data_id_seq    SEQUENCE     t   CREATE SEQUENCE v1.trx_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE v1.trx_data_id_seq;
       v1          postgres    false    6    236            0           0    0    trx_data_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE v1.trx_data_id_seq OWNED BY v1.trx_data.id;
          v1          postgres    false    237            �            1259    16474    trx_data_organisasi    TABLE     R  CREATE TABLE v1.trx_data_organisasi (
    id bigint NOT NULL,
    trx_data_id integer,
    ref_organisasi_id integer,
    organisasi text NOT NULL,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
 #   DROP TABLE v1.trx_data_organisasi;
       v1         heap    postgres    false    6            �            1259    16479    trx_data_organisasi_id_seq    SEQUENCE        CREATE SEQUENCE v1.trx_data_organisasi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE v1.trx_data_organisasi_id_seq;
       v1          postgres    false    6    238            1           0    0    trx_data_organisasi_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE v1.trx_data_organisasi_id_seq OWNED BY v1.trx_data_organisasi.id;
          v1          postgres    false    239            �            1259    16480    trx_data_tag    TABLE     G  CREATE TABLE v1.trx_data_tag (
    id bigint NOT NULL,
    trx_data_id integer,
    ref_data_tag_id integer,
    data_tag text NOT NULL,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.trx_data_tag;
       v1         heap    postgres    false    6            �            1259    16485    trx_data_tag_id_seq    SEQUENCE     x   CREATE SEQUENCE v1.trx_data_tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE v1.trx_data_tag_id_seq;
       v1          postgres    false    6    240            2           0    0    trx_data_tag_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE v1.trx_data_tag_id_seq OWNED BY v1.trx_data_tag.id;
          v1          postgres    false    241            �            1259    16486    trx_data_topik    TABLE     C  CREATE TABLE v1.trx_data_topik (
    id bigint NOT NULL,
    trx_data_id integer,
    ref_topik_id integer,
    topik text NOT NULL,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.trx_data_topik;
       v1         heap    postgres    false    6            �            1259    16491    trx_data_topik_id_seq    SEQUENCE     z   CREATE SEQUENCE v1.trx_data_topik_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE v1.trx_data_topik_id_seq;
       v1          postgres    false    6    242            3           0    0    trx_data_topik_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE v1.trx_data_topik_id_seq OWNED BY v1.trx_data_topik.id;
          v1          postgres    false    243            	           1259    24754 )   trx_jml_angkatan_kerja_tingkat_pendidikan    TABLE     P  CREATE TABLE v1.trx_jml_angkatan_kerja_tingkat_pendidikan (
    id bigint NOT NULL,
    mst_collection_id integer,
    tahun integer NOT NULL,
    kategori text NOT NULL,
    target text,
    realisasi text,
    sumber text,
    catatan text,
    kemendagri_kota_kode character varying(50) DEFAULT '32.71'::character varying NOT NULL,
    kemendagri_kota_nama text DEFAULT 'KOTA BOGOR'::text NOT NULL,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
 9   DROP TABLE v1.trx_jml_angkatan_kerja_tingkat_pendidikan;
       v1         heap    postgres    false    6                       1259    24753 0   trx_jml_angkatan_kerja_tingkat_pendidikan_id_seq    SEQUENCE     �   CREATE SEQUENCE v1.trx_jml_angkatan_kerja_tingkat_pendidikan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 C   DROP SEQUENCE v1.trx_jml_angkatan_kerja_tingkat_pendidikan_id_seq;
       v1          postgres    false    265    6            4           0    0 0   trx_jml_angkatan_kerja_tingkat_pendidikan_id_seq    SEQUENCE OWNED BY     }   ALTER SEQUENCE v1.trx_jml_angkatan_kerja_tingkat_pendidikan_id_seq OWNED BY v1.trx_jml_angkatan_kerja_tingkat_pendidikan.id;
          v1          postgres    false    264                       1259    24701    trx_jml_museum_bds_pengelola    TABLE     C  CREATE TABLE v1.trx_jml_museum_bds_pengelola (
    id bigint NOT NULL,
    mst_collection_id integer,
    tahun integer NOT NULL,
    kategori text NOT NULL,
    target text,
    realisasi text,
    sumber text,
    catatan text,
    kemendagri_kota_kode character varying(50) DEFAULT '32.71'::character varying NOT NULL,
    kemendagri_kota_nama text DEFAULT 'KOTA BOGOR'::text NOT NULL,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
 ,   DROP TABLE v1.trx_jml_museum_bds_pengelola;
       v1         heap    postgres    false    6                       1259    24700 #   trx_jml_museum_bds_pengelola_id_seq    SEQUENCE     �   CREATE SEQUENCE v1.trx_jml_museum_bds_pengelola_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 6   DROP SEQUENCE v1.trx_jml_museum_bds_pengelola_id_seq;
       v1          postgres    false    6    259            5           0    0 #   trx_jml_museum_bds_pengelola_id_seq    SEQUENCE OWNED BY     c   ALTER SEQUENCE v1.trx_jml_museum_bds_pengelola_id_seq OWNED BY v1.trx_jml_museum_bds_pengelola.id;
          v1          postgres    false    258                       1259    24717    trx_jml_penduduk_bds_jk    TABLE     >  CREATE TABLE v1.trx_jml_penduduk_bds_jk (
    id bigint NOT NULL,
    mst_collection_id integer,
    tahun integer NOT NULL,
    kategori text NOT NULL,
    target text,
    realisasi text,
    sumber text,
    catatan text,
    kemendagri_kota_kode character varying(50) DEFAULT '32.71'::character varying NOT NULL,
    kemendagri_kota_nama text DEFAULT 'KOTA BOGOR'::text NOT NULL,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
 '   DROP TABLE v1.trx_jml_penduduk_bds_jk;
       v1         heap    postgres    false    6                       1259    24716    trx_jml_penduduk_bds_jk_id_seq    SEQUENCE     �   CREATE SEQUENCE v1.trx_jml_penduduk_bds_jk_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE v1.trx_jml_penduduk_bds_jk_id_seq;
       v1          postgres    false    261    6            6           0    0    trx_jml_penduduk_bds_jk_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE v1.trx_jml_penduduk_bds_jk_id_seq OWNED BY v1.trx_jml_penduduk_bds_jk.id;
          v1          postgres    false    260            �            1259    24645    trx_laju_pertumbuhan_pdrb    TABLE     O  CREATE TABLE v1.trx_laju_pertumbuhan_pdrb (
    id bigint NOT NULL,
    mst_collection_id integer,
    tahun integer NOT NULL,
    kategori text NOT NULL,
    urutan character varying(5),
    target text,
    realisasi text,
    sumber text,
    catatan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    kemendagri_kota_kode character varying(50) DEFAULT '32.71'::character varying,
    kemendagri_kota_nama text DEFAULT 'KOTA BOGOR'::text
);
 )   DROP TABLE v1.trx_laju_pertumbuhan_pdrb;
       v1         heap    postgres    false    6            �            1259    24644     trx_laju_pertumbuhan_pdrb_id_seq    SEQUENCE     �   CREATE SEQUENCE v1.trx_laju_pertumbuhan_pdrb_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE v1.trx_laju_pertumbuhan_pdrb_id_seq;
       v1          postgres    false    255    6            7           0    0     trx_laju_pertumbuhan_pdrb_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE v1.trx_laju_pertumbuhan_pdrb_id_seq OWNED BY v1.trx_laju_pertumbuhan_pdrb.id;
          v1          postgres    false    254            �            1259    16492 $   trx_luas_wilayah_kecamatan_kelurahan    TABLE       CREATE TABLE v1.trx_luas_wilayah_kecamatan_kelurahan (
    id bigint NOT NULL,
    mst_collection_id integer NOT NULL,
    tahun integer NOT NULL,
    kecamatan character varying(50) NOT NULL,
    jml_kelurahan integer NOT NULL,
    luas_wilayah double precision NOT NULL,
    satuan character varying(50),
    catatan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
 4   DROP TABLE v1.trx_luas_wilayah_kecamatan_kelurahan;
       v1         heap    postgres    false    6            �            1259    16497 +   trx_luas_wilayah_kecamatan_kelurahan_id_seq    SEQUENCE     �   CREATE SEQUENCE v1.trx_luas_wilayah_kecamatan_kelurahan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 >   DROP SEQUENCE v1.trx_luas_wilayah_kecamatan_kelurahan_id_seq;
       v1          postgres    false    244    6            8           0    0 +   trx_luas_wilayah_kecamatan_kelurahan_id_seq    SEQUENCE OWNED BY     s   ALTER SEQUENCE v1.trx_luas_wilayah_kecamatan_kelurahan_id_seq OWNED BY v1.trx_luas_wilayah_kecamatan_kelurahan.id;
          v1          postgres    false    245                       1259    24685    trx_pdrb_adhk_2010_lap_usaha    TABLE     C  CREATE TABLE v1.trx_pdrb_adhk_2010_lap_usaha (
    id bigint NOT NULL,
    mst_collection_id integer,
    tahun integer NOT NULL,
    kategori text NOT NULL,
    target text,
    realisasi text,
    sumber text,
    catatan text,
    kemendagri_kota_kode character varying(50) DEFAULT '32.71'::character varying NOT NULL,
    kemendagri_kota_nama text DEFAULT 'KOTA BOGOR'::text NOT NULL,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
 ,   DROP TABLE v1.trx_pdrb_adhk_2010_lap_usaha;
       v1         heap    postgres    false    6                        1259    24684 #   trx_pdrb_adhk_2010_lap_usaha_id_seq    SEQUENCE     �   CREATE SEQUENCE v1.trx_pdrb_adhk_2010_lap_usaha_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 6   DROP SEQUENCE v1.trx_pdrb_adhk_2010_lap_usaha_id_seq;
       v1          postgres    false    257    6            9           0    0 #   trx_pdrb_adhk_2010_lap_usaha_id_seq    SEQUENCE OWNED BY     c   ALTER SEQUENCE v1.trx_pdrb_adhk_2010_lap_usaha_id_seq OWNED BY v1.trx_pdrb_adhk_2010_lap_usaha.id;
          v1          postgres    false    256                       1259    24738    trx_pdrb_adhk_jenis_pengeluaran    TABLE     F  CREATE TABLE v1.trx_pdrb_adhk_jenis_pengeluaran (
    id bigint NOT NULL,
    mst_collection_id integer,
    tahun integer NOT NULL,
    kategori text NOT NULL,
    target text,
    realisasi text,
    sumber text,
    catatan text,
    kemendagri_kota_kode character varying(50) DEFAULT '32.71'::character varying NOT NULL,
    kemendagri_kota_nama text DEFAULT 'KOTA BOGOR'::text NOT NULL,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
 /   DROP TABLE v1.trx_pdrb_adhk_jenis_pengeluaran;
       v1         heap    postgres    false    6                       1259    24737 &   trx_pdrb_adhk_jenis_pengeluaran_id_seq    SEQUENCE     �   CREATE SEQUENCE v1.trx_pdrb_adhk_jenis_pengeluaran_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE v1.trx_pdrb_adhk_jenis_pengeluaran_id_seq;
       v1          postgres    false    6    263            :           0    0 &   trx_pdrb_adhk_jenis_pengeluaran_id_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE v1.trx_pdrb_adhk_jenis_pengeluaran_id_seq OWNED BY v1.trx_pdrb_adhk_jenis_pengeluaran.id;
          v1          postgres    false    262            �            1259    16498    trx_riwayat_data    TABLE     U  CREATE TABLE v1.trx_riwayat_data (
    id bigint NOT NULL,
    trx_data_id integer,
    mst_jenis_dok_id integer,
    sumber_data text NOT NULL,
    jenis_data character varying(100) NOT NULL,
    nama_data text NOT NULL,
    deskripsi_data text NOT NULL,
    format_data character varying(100) NOT NULL,
    media_type character varying(100),
    size_data character varying(100),
    ekstensi_data character varying(100),
    nama_file text,
    url_file_upload text,
    url_upload text,
    is_shared boolean DEFAULT false NOT NULL,
    webbappeda_visible boolean DEFAULT false NOT NULL,
    satudata_visible boolean DEFAULT false NOT NULL,
    catatan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
     DROP TABLE v1.trx_riwayat_data;
       v1         heap    postgres    false    6            �            1259    16506    trx_riwayat_data_id_seq    SEQUENCE     |   CREATE SEQUENCE v1.trx_riwayat_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE v1.trx_riwayat_data_id_seq;
       v1          postgres    false    6    246            ;           0    0    trx_riwayat_data_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE v1.trx_riwayat_data_id_seq OWNED BY v1.trx_riwayat_data.id;
          v1          postgres    false    247            �            1259    16507    users    TABLE     t  CREATE TABLE v1.users (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    email_verified_at timestamp(0) without time zone,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.users;
       v1         heap    postgres    false    6            �            1259    16512    users_id_seq    SEQUENCE     q   CREATE SEQUENCE v1.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
    DROP SEQUENCE v1.users_id_seq;
       v1          postgres    false    248    6            <           0    0    users_id_seq    SEQUENCE OWNED BY     5   ALTER SEQUENCE v1.users_id_seq OWNED BY v1.users.id;
          v1          postgres    false    249            �           2604    16513    failed_jobs id    DEFAULT     h   ALTER TABLE ONLY v1.failed_jobs ALTER COLUMN id SET DEFAULT nextval('v1.failed_jobs_id_seq'::regclass);
 9   ALTER TABLE v1.failed_jobs ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    216    215            �           2604    16514    migrations id    DEFAULT     f   ALTER TABLE ONLY v1.migrations ALTER COLUMN id SET DEFAULT nextval('v1.migrations_id_seq'::regclass);
 8   ALTER TABLE v1.migrations ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    218    217            �           2604    16515    mst_collection id    DEFAULT     n   ALTER TABLE ONLY v1.mst_collection ALTER COLUMN id SET DEFAULT nextval('v1.mst_collection_id_seq'::regclass);
 <   ALTER TABLE v1.mst_collection ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    220    219            �           2604    16516    mst_dashboard id    DEFAULT     l   ALTER TABLE ONLY v1.mst_dashboard ALTER COLUMN id SET DEFAULT nextval('v1.mst_dashboard_id_seq'::regclass);
 ;   ALTER TABLE v1.mst_dashboard ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    222    221            �           2604    16517    mst_jenis_dok id    DEFAULT     l   ALTER TABLE ONLY v1.mst_jenis_dok ALTER COLUMN id SET DEFAULT nextval('v1.mst_jenis_dok_id_seq'::regclass);
 ;   ALTER TABLE v1.mst_jenis_dok ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    224    223            �           2604    16518    personal_access_tokens id    DEFAULT     ~   ALTER TABLE ONLY v1.personal_access_tokens ALTER COLUMN id SET DEFAULT nextval('v1.personal_access_tokens_id_seq'::regclass);
 D   ALTER TABLE v1.personal_access_tokens ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    227    226            �           2604    16519    ref_data_tag id    DEFAULT     j   ALTER TABLE ONLY v1.ref_data_tag ALTER COLUMN id SET DEFAULT nextval('v1.ref_data_tag_id_seq'::regclass);
 :   ALTER TABLE v1.ref_data_tag ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    229    228            �           2604    16520    ref_grup_dok id    DEFAULT     j   ALTER TABLE ONLY v1.ref_grup_dok ALTER COLUMN id SET DEFAULT nextval('v1.ref_grup_dok_id_seq'::regclass);
 :   ALTER TABLE v1.ref_grup_dok ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    231    230            �           2604    24620    ref_kode_wilayah id    DEFAULT     r   ALTER TABLE ONLY v1.ref_kode_wilayah ALTER COLUMN id SET DEFAULT nextval('v1.ref_kode_wilayah_id_seq'::regclass);
 >   ALTER TABLE v1.ref_kode_wilayah ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    252    253    253            �           2604    16521    ref_organisasi id    DEFAULT     n   ALTER TABLE ONLY v1.ref_organisasi ALTER COLUMN id SET DEFAULT nextval('v1.ref_organisasi_id_seq'::regclass);
 <   ALTER TABLE v1.ref_organisasi ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    233    232            �           2604    16522    ref_topik id    DEFAULT     d   ALTER TABLE ONLY v1.ref_topik ALTER COLUMN id SET DEFAULT nextval('v1.ref_topik_id_seq'::regclass);
 7   ALTER TABLE v1.ref_topik ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    235    234            �           2604    24604    trx_collection id    DEFAULT     n   ALTER TABLE ONLY v1.trx_collection ALTER COLUMN id SET DEFAULT nextval('v1.trx_collection_id_seq'::regclass);
 <   ALTER TABLE v1.trx_collection ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    250    251    251            �           2604    16523    trx_data id    DEFAULT     b   ALTER TABLE ONLY v1.trx_data ALTER COLUMN id SET DEFAULT nextval('v1.trx_data_id_seq'::regclass);
 6   ALTER TABLE v1.trx_data ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    237    236            �           2604    16524    trx_data_organisasi id    DEFAULT     x   ALTER TABLE ONLY v1.trx_data_organisasi ALTER COLUMN id SET DEFAULT nextval('v1.trx_data_organisasi_id_seq'::regclass);
 A   ALTER TABLE v1.trx_data_organisasi ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    239    238            �           2604    16525    trx_data_tag id    DEFAULT     j   ALTER TABLE ONLY v1.trx_data_tag ALTER COLUMN id SET DEFAULT nextval('v1.trx_data_tag_id_seq'::regclass);
 :   ALTER TABLE v1.trx_data_tag ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    241    240            �           2604    16526    trx_data_topik id    DEFAULT     n   ALTER TABLE ONLY v1.trx_data_topik ALTER COLUMN id SET DEFAULT nextval('v1.trx_data_topik_id_seq'::regclass);
 <   ALTER TABLE v1.trx_data_topik ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    243    242                       2604    24757 ,   trx_jml_angkatan_kerja_tingkat_pendidikan id    DEFAULT     �   ALTER TABLE ONLY v1.trx_jml_angkatan_kerja_tingkat_pendidikan ALTER COLUMN id SET DEFAULT nextval('v1.trx_jml_angkatan_kerja_tingkat_pendidikan_id_seq'::regclass);
 W   ALTER TABLE v1.trx_jml_angkatan_kerja_tingkat_pendidikan ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    265    264    265                       2604    24704    trx_jml_museum_bds_pengelola id    DEFAULT     �   ALTER TABLE ONLY v1.trx_jml_museum_bds_pengelola ALTER COLUMN id SET DEFAULT nextval('v1.trx_jml_museum_bds_pengelola_id_seq'::regclass);
 J   ALTER TABLE v1.trx_jml_museum_bds_pengelola ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    259    258    259            	           2604    24720    trx_jml_penduduk_bds_jk id    DEFAULT     �   ALTER TABLE ONLY v1.trx_jml_penduduk_bds_jk ALTER COLUMN id SET DEFAULT nextval('v1.trx_jml_penduduk_bds_jk_id_seq'::regclass);
 E   ALTER TABLE v1.trx_jml_penduduk_bds_jk ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    261    260    261                        2604    24648    trx_laju_pertumbuhan_pdrb id    DEFAULT     �   ALTER TABLE ONLY v1.trx_laju_pertumbuhan_pdrb ALTER COLUMN id SET DEFAULT nextval('v1.trx_laju_pertumbuhan_pdrb_id_seq'::regclass);
 G   ALTER TABLE v1.trx_laju_pertumbuhan_pdrb ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    255    254    255            �           2604    16527 '   trx_luas_wilayah_kecamatan_kelurahan id    DEFAULT     �   ALTER TABLE ONLY v1.trx_luas_wilayah_kecamatan_kelurahan ALTER COLUMN id SET DEFAULT nextval('v1.trx_luas_wilayah_kecamatan_kelurahan_id_seq'::regclass);
 R   ALTER TABLE v1.trx_luas_wilayah_kecamatan_kelurahan ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    245    244                       2604    24688    trx_pdrb_adhk_2010_lap_usaha id    DEFAULT     �   ALTER TABLE ONLY v1.trx_pdrb_adhk_2010_lap_usaha ALTER COLUMN id SET DEFAULT nextval('v1.trx_pdrb_adhk_2010_lap_usaha_id_seq'::regclass);
 J   ALTER TABLE v1.trx_pdrb_adhk_2010_lap_usaha ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    256    257    257                       2604    24741 "   trx_pdrb_adhk_jenis_pengeluaran id    DEFAULT     �   ALTER TABLE ONLY v1.trx_pdrb_adhk_jenis_pengeluaran ALTER COLUMN id SET DEFAULT nextval('v1.trx_pdrb_adhk_jenis_pengeluaran_id_seq'::regclass);
 M   ALTER TABLE v1.trx_pdrb_adhk_jenis_pengeluaran ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    262    263    263            �           2604    16528    trx_riwayat_data id    DEFAULT     r   ALTER TABLE ONLY v1.trx_riwayat_data ALTER COLUMN id SET DEFAULT nextval('v1.trx_riwayat_data_id_seq'::regclass);
 >   ALTER TABLE v1.trx_riwayat_data ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    247    246            �           2604    16529    users id    DEFAULT     \   ALTER TABLE ONLY v1.users ALTER COLUMN id SET DEFAULT nextval('v1.users_id_seq'::regclass);
 3   ALTER TABLE v1.users ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    249    248            �          0    16400    failed_jobs 
   TABLE DATA           ]   COPY v1.failed_jobs (id, uuid, connection, queue, payload, exception, failed_at) FROM stdin;
    v1          postgres    false    215   �      �          0    16407 
   migrations 
   TABLE DATA           6   COPY v1.migrations (id, migration, batch) FROM stdin;
    v1          postgres    false    217         �          0    16411    mst_collection 
   TABLE DATA           )  COPY v1.mst_collection (id, nama_collection, judul, tipe, default_chart, referensi_data, catatan, route_name, table_name, created_by, updated_by, created_at, updated_at, ref_organisasi_id, organisasi, satuan, definisi, rumus_perhitungan, cara_memperoleh_data, urusan, is_multidimensi) FROM stdin;
    v1          postgres    false    219   [      �          0    16417    mst_dashboard 
   TABLE DATA           �   COPY v1.mst_dashboard (id, nama_dashboard, frame, link, catatan, is_hidden, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    221   ��      �          0    16424    mst_jenis_dok 
   TABLE DATA           �   COPY v1.mst_jenis_dok (id, jenis_dokumen, ref_grup_dok_id, keterangan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    223   �      �          0    16430    password_reset_tokens 
   TABLE DATA           E   COPY v1.password_reset_tokens (email, token, created_at) FROM stdin;
    v1          postgres    false    225   _�      �          0    16435    personal_access_tokens 
   TABLE DATA           �   COPY v1.personal_access_tokens (id, tokenable_type, tokenable_id, name, token, abilities, last_used_at, expires_at, created_at, updated_at) FROM stdin;
    v1          postgres    false    226   |�      �          0    16441    ref_data_tag 
   TABLE DATA           l   COPY v1.ref_data_tag (id, data_tag, keterangan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    228   ��      �          0    16447    ref_grup_dok 
   TABLE DATA           p   COPY v1.ref_grup_dok (id, grup_dokumen, keterangan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    230   -�                0    24617    ref_kode_wilayah 
   TABLE DATA             COPY v1.ref_kode_wilayah (id, kemendagri_provinsi_kode, kemendagri_kota_kode, kemendagri_provinsi_nama, kemendagri_kota_nama, bps_provinsi_kode, bps_kota_kode, bps_provinsi_nama, bps_kota_nama, latitude, longitude, kode_pos, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    253   �      �          0    16453    ref_organisasi 
   TABLE DATA           {   COPY v1.ref_organisasi (id, organisasi, singkatan, keterangan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    232   ]�      �          0    16459 	   ref_topik 
   TABLE DATA           f   COPY v1.ref_topik (id, topik, keterangan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    234   R�                0    24601    trx_collection 
   TABLE DATA           �   COPY v1.trx_collection (id, mst_collection_id, tahun, target, realisasi, sumber, catatan, created_by, updated_by, created_at, updated_at, kemendagri_kota_kode, kemendagri_kota_nama) FROM stdin;
    v1          postgres    false    251   �                 0    16465    trx_data 
   TABLE DATA           -  COPY v1.trx_data (id, mst_jenis_dok_id, sumber_data, jenis_data, nama_data, deskripsi_data, format_data, media_type, size_data, ekstensi_data, nama_file, url_file_upload, url_upload, is_shared, webbappeda_visible, satudata_visible, catatan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    236   �m                0    16474    trx_data_organisasi 
   TABLE DATA           �   COPY v1.trx_data_organisasi (id, trx_data_id, ref_organisasi_id, organisasi, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    238   ��                0    16480    trx_data_tag 
   TABLE DATA           ~   COPY v1.trx_data_tag (id, trx_data_id, ref_data_tag_id, data_tag, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    240   ��                0    16486    trx_data_topik 
   TABLE DATA           z   COPY v1.trx_data_topik (id, trx_data_id, ref_topik_id, topik, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    242   ��                0    24754 )   trx_jml_angkatan_kerja_tingkat_pendidikan 
   TABLE DATA           �   COPY v1.trx_jml_angkatan_kerja_tingkat_pendidikan (id, mst_collection_id, tahun, kategori, target, realisasi, sumber, catatan, kemendagri_kota_kode, kemendagri_kota_nama, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    265                   0    24701    trx_jml_museum_bds_pengelola 
   TABLE DATA           �   COPY v1.trx_jml_museum_bds_pengelola (id, mst_collection_id, tahun, kategori, target, realisasi, sumber, catatan, kemendagri_kota_kode, kemendagri_kota_nama, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    259   7                0    24717    trx_jml_penduduk_bds_jk 
   TABLE DATA           �   COPY v1.trx_jml_penduduk_bds_jk (id, mst_collection_id, tahun, kategori, target, realisasi, sumber, catatan, kemendagri_kota_kode, kemendagri_kota_nama, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    261   T                0    24645    trx_laju_pertumbuhan_pdrb 
   TABLE DATA           �   COPY v1.trx_laju_pertumbuhan_pdrb (id, mst_collection_id, tahun, kategori, urutan, target, realisasi, sumber, catatan, created_by, updated_by, created_at, updated_at, kemendagri_kota_kode, kemendagri_kota_nama) FROM stdin;
    v1          postgres    false    255   q                0    16492 $   trx_luas_wilayah_kecamatan_kelurahan 
   TABLE DATA           �   COPY v1.trx_luas_wilayah_kecamatan_kelurahan (id, mst_collection_id, tahun, kecamatan, jml_kelurahan, luas_wilayah, satuan, catatan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    244   A                0    24685    trx_pdrb_adhk_2010_lap_usaha 
   TABLE DATA           �   COPY v1.trx_pdrb_adhk_2010_lap_usaha (id, mst_collection_id, tahun, kategori, target, realisasi, sumber, catatan, kemendagri_kota_kode, kemendagri_kota_nama, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    257   	                0    24738    trx_pdrb_adhk_jenis_pengeluaran 
   TABLE DATA           �   COPY v1.trx_pdrb_adhk_jenis_pengeluaran (id, mst_collection_id, tahun, kategori, target, realisasi, sumber, catatan, kemendagri_kota_kode, kemendagri_kota_nama, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    263   �      
          0    16498    trx_riwayat_data 
   TABLE DATA           B  COPY v1.trx_riwayat_data (id, trx_data_id, mst_jenis_dok_id, sumber_data, jenis_data, nama_data, deskripsi_data, format_data, media_type, size_data, ekstensi_data, nama_file, url_file_upload, url_upload, is_shared, webbappeda_visible, satudata_visible, catatan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    246   �                0    16507    users 
   TABLE DATA           q   COPY v1.users (id, name, email, email_verified_at, password, remember_token, created_at, updated_at) FROM stdin;
    v1          postgres    false    248   C      =           0    0    failed_jobs_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('v1.failed_jobs_id_seq', 1, false);
          v1          postgres    false    216            >           0    0    migrations_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('v1.migrations_id_seq', 33, true);
          v1          postgres    false    218            ?           0    0    mst_collection_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('v1.mst_collection_id_seq', 710, true);
          v1          postgres    false    220            @           0    0    mst_dashboard_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('v1.mst_dashboard_id_seq', 3, true);
          v1          postgres    false    222            A           0    0    mst_jenis_dok_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('v1.mst_jenis_dok_id_seq', 3, true);
          v1          postgres    false    224            B           0    0    personal_access_tokens_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('v1.personal_access_tokens_id_seq', 1, false);
          v1          postgres    false    227            C           0    0    ref_data_tag_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('v1.ref_data_tag_id_seq', 173, true);
          v1          postgres    false    229            D           0    0    ref_grup_dok_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('v1.ref_grup_dok_id_seq', 2, true);
          v1          postgres    false    231            E           0    0    ref_kode_wilayah_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('v1.ref_kode_wilayah_id_seq', 28, true);
          v1          postgres    false    252            F           0    0    ref_organisasi_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('v1.ref_organisasi_id_seq', 61, true);
          v1          postgres    false    233            G           0    0    ref_topik_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('v1.ref_topik_id_seq', 36, true);
          v1          postgres    false    235            H           0    0    trx_collection_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('v1.trx_collection_id_seq', 3016, true);
          v1          postgres    false    250            I           0    0    trx_data_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('v1.trx_data_id_seq', 326, true);
          v1          postgres    false    237            J           0    0    trx_data_organisasi_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('v1.trx_data_organisasi_id_seq', 95, true);
          v1          postgres    false    239            K           0    0    trx_data_tag_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('v1.trx_data_tag_id_seq', 554, true);
          v1          postgres    false    241            L           0    0    trx_data_topik_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('v1.trx_data_topik_id_seq', 32, true);
          v1          postgres    false    243            M           0    0 0   trx_jml_angkatan_kerja_tingkat_pendidikan_id_seq    SEQUENCE SET     Z   SELECT pg_catalog.setval('v1.trx_jml_angkatan_kerja_tingkat_pendidikan_id_seq', 1, true);
          v1          postgres    false    264            N           0    0 #   trx_jml_museum_bds_pengelola_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('v1.trx_jml_museum_bds_pengelola_id_seq', 1, true);
          v1          postgres    false    258            O           0    0    trx_jml_penduduk_bds_jk_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('v1.trx_jml_penduduk_bds_jk_id_seq', 2, true);
          v1          postgres    false    260            P           0    0     trx_laju_pertumbuhan_pdrb_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('v1.trx_laju_pertumbuhan_pdrb_id_seq', 90, true);
          v1          postgres    false    254            Q           0    0 +   trx_luas_wilayah_kecamatan_kelurahan_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('v1.trx_luas_wilayah_kecamatan_kelurahan_id_seq', 6, true);
          v1          postgres    false    245            R           0    0 #   trx_pdrb_adhk_2010_lap_usaha_id_seq    SEQUENCE SET     N   SELECT pg_catalog.setval('v1.trx_pdrb_adhk_2010_lap_usaha_id_seq', 81, true);
          v1          postgres    false    256            S           0    0 &   trx_pdrb_adhk_jenis_pengeluaran_id_seq    SEQUENCE SET     P   SELECT pg_catalog.setval('v1.trx_pdrb_adhk_jenis_pengeluaran_id_seq', 1, true);
          v1          postgres    false    262            T           0    0    trx_riwayat_data_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('v1.trx_riwayat_data_id_seq', 336, true);
          v1          postgres    false    247            U           0    0    users_id_seq    SEQUENCE SET     7   SELECT pg_catalog.setval('v1.users_id_seq', 1, false);
          v1          postgres    false    249                       2606    16531    failed_jobs failed_jobs_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY v1.failed_jobs
    ADD CONSTRAINT failed_jobs_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY v1.failed_jobs DROP CONSTRAINT failed_jobs_pkey;
       v1            postgres    false    215                       2606    16533 #   failed_jobs failed_jobs_uuid_unique 
   CONSTRAINT     Z   ALTER TABLE ONLY v1.failed_jobs
    ADD CONSTRAINT failed_jobs_uuid_unique UNIQUE (uuid);
 I   ALTER TABLE ONLY v1.failed_jobs DROP CONSTRAINT failed_jobs_uuid_unique;
       v1            postgres    false    215                       2606    16535    migrations migrations_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY v1.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY v1.migrations DROP CONSTRAINT migrations_pkey;
       v1            postgres    false    217                       2606    16537 "   mst_collection mst_collection_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY v1.mst_collection
    ADD CONSTRAINT mst_collection_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY v1.mst_collection DROP CONSTRAINT mst_collection_pkey;
       v1            postgres    false    219                       2606    16539     mst_dashboard mst_dashboard_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY v1.mst_dashboard
    ADD CONSTRAINT mst_dashboard_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY v1.mst_dashboard DROP CONSTRAINT mst_dashboard_pkey;
       v1            postgres    false    221                       2606    16541     mst_jenis_dok mst_jenis_dok_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY v1.mst_jenis_dok
    ADD CONSTRAINT mst_jenis_dok_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY v1.mst_jenis_dok DROP CONSTRAINT mst_jenis_dok_pkey;
       v1            postgres    false    223                       2606    16543 0   password_reset_tokens password_reset_tokens_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY v1.password_reset_tokens
    ADD CONSTRAINT password_reset_tokens_pkey PRIMARY KEY (email);
 V   ALTER TABLE ONLY v1.password_reset_tokens DROP CONSTRAINT password_reset_tokens_pkey;
       v1            postgres    false    225            !           2606    16545 2   personal_access_tokens personal_access_tokens_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY v1.personal_access_tokens
    ADD CONSTRAINT personal_access_tokens_pkey PRIMARY KEY (id);
 X   ALTER TABLE ONLY v1.personal_access_tokens DROP CONSTRAINT personal_access_tokens_pkey;
       v1            postgres    false    226            #           2606    16547 :   personal_access_tokens personal_access_tokens_token_unique 
   CONSTRAINT     r   ALTER TABLE ONLY v1.personal_access_tokens
    ADD CONSTRAINT personal_access_tokens_token_unique UNIQUE (token);
 `   ALTER TABLE ONLY v1.personal_access_tokens DROP CONSTRAINT personal_access_tokens_token_unique;
       v1            postgres    false    226            &           2606    16549    ref_data_tag ref_data_tag_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY v1.ref_data_tag
    ADD CONSTRAINT ref_data_tag_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY v1.ref_data_tag DROP CONSTRAINT ref_data_tag_pkey;
       v1            postgres    false    228            (           2606    16551    ref_grup_dok ref_grup_dok_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY v1.ref_grup_dok
    ADD CONSTRAINT ref_grup_dok_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY v1.ref_grup_dok DROP CONSTRAINT ref_grup_dok_pkey;
       v1            postgres    false    230            @           2606    24624 &   ref_kode_wilayah ref_kode_wilayah_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY v1.ref_kode_wilayah
    ADD CONSTRAINT ref_kode_wilayah_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY v1.ref_kode_wilayah DROP CONSTRAINT ref_kode_wilayah_pkey;
       v1            postgres    false    253            *           2606    16553 "   ref_organisasi ref_organisasi_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY v1.ref_organisasi
    ADD CONSTRAINT ref_organisasi_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY v1.ref_organisasi DROP CONSTRAINT ref_organisasi_pkey;
       v1            postgres    false    232            ,           2606    16555    ref_topik ref_topik_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY v1.ref_topik
    ADD CONSTRAINT ref_topik_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY v1.ref_topik DROP CONSTRAINT ref_topik_pkey;
       v1            postgres    false    234            >           2606    24608 "   trx_collection trx_collection_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY v1.trx_collection
    ADD CONSTRAINT trx_collection_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY v1.trx_collection DROP CONSTRAINT trx_collection_pkey;
       v1            postgres    false    251            0           2606    16557 ,   trx_data_organisasi trx_data_organisasi_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY v1.trx_data_organisasi
    ADD CONSTRAINT trx_data_organisasi_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY v1.trx_data_organisasi DROP CONSTRAINT trx_data_organisasi_pkey;
       v1            postgres    false    238            .           2606    16559    trx_data trx_data_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY v1.trx_data
    ADD CONSTRAINT trx_data_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY v1.trx_data DROP CONSTRAINT trx_data_pkey;
       v1            postgres    false    236            2           2606    16561    trx_data_tag trx_data_tag_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY v1.trx_data_tag
    ADD CONSTRAINT trx_data_tag_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY v1.trx_data_tag DROP CONSTRAINT trx_data_tag_pkey;
       v1            postgres    false    240            4           2606    16563 "   trx_data_topik trx_data_topik_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY v1.trx_data_topik
    ADD CONSTRAINT trx_data_topik_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY v1.trx_data_topik DROP CONSTRAINT trx_data_topik_pkey;
       v1            postgres    false    242            L           2606    24763 X   trx_jml_angkatan_kerja_tingkat_pendidikan trx_jml_angkatan_kerja_tingkat_pendidikan_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY v1.trx_jml_angkatan_kerja_tingkat_pendidikan
    ADD CONSTRAINT trx_jml_angkatan_kerja_tingkat_pendidikan_pkey PRIMARY KEY (id);
 ~   ALTER TABLE ONLY v1.trx_jml_angkatan_kerja_tingkat_pendidikan DROP CONSTRAINT trx_jml_angkatan_kerja_tingkat_pendidikan_pkey;
       v1            postgres    false    265            F           2606    24710 >   trx_jml_museum_bds_pengelola trx_jml_museum_bds_pengelola_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY v1.trx_jml_museum_bds_pengelola
    ADD CONSTRAINT trx_jml_museum_bds_pengelola_pkey PRIMARY KEY (id);
 d   ALTER TABLE ONLY v1.trx_jml_museum_bds_pengelola DROP CONSTRAINT trx_jml_museum_bds_pengelola_pkey;
       v1            postgres    false    259            H           2606    24726 4   trx_jml_penduduk_bds_jk trx_jml_penduduk_bds_jk_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY v1.trx_jml_penduduk_bds_jk
    ADD CONSTRAINT trx_jml_penduduk_bds_jk_pkey PRIMARY KEY (id);
 Z   ALTER TABLE ONLY v1.trx_jml_penduduk_bds_jk DROP CONSTRAINT trx_jml_penduduk_bds_jk_pkey;
       v1            postgres    false    261            B           2606    24652 8   trx_laju_pertumbuhan_pdrb trx_laju_pertumbuhan_pdrb_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY v1.trx_laju_pertumbuhan_pdrb
    ADD CONSTRAINT trx_laju_pertumbuhan_pdrb_pkey PRIMARY KEY (id);
 ^   ALTER TABLE ONLY v1.trx_laju_pertumbuhan_pdrb DROP CONSTRAINT trx_laju_pertumbuhan_pdrb_pkey;
       v1            postgres    false    255            6           2606    16565 N   trx_luas_wilayah_kecamatan_kelurahan trx_luas_wilayah_kecamatan_kelurahan_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY v1.trx_luas_wilayah_kecamatan_kelurahan
    ADD CONSTRAINT trx_luas_wilayah_kecamatan_kelurahan_pkey PRIMARY KEY (id);
 t   ALTER TABLE ONLY v1.trx_luas_wilayah_kecamatan_kelurahan DROP CONSTRAINT trx_luas_wilayah_kecamatan_kelurahan_pkey;
       v1            postgres    false    244            D           2606    24694 >   trx_pdrb_adhk_2010_lap_usaha trx_pdrb_adhk_2010_lap_usaha_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY v1.trx_pdrb_adhk_2010_lap_usaha
    ADD CONSTRAINT trx_pdrb_adhk_2010_lap_usaha_pkey PRIMARY KEY (id);
 d   ALTER TABLE ONLY v1.trx_pdrb_adhk_2010_lap_usaha DROP CONSTRAINT trx_pdrb_adhk_2010_lap_usaha_pkey;
       v1            postgres    false    257            J           2606    24747 D   trx_pdrb_adhk_jenis_pengeluaran trx_pdrb_adhk_jenis_pengeluaran_pkey 
   CONSTRAINT     ~   ALTER TABLE ONLY v1.trx_pdrb_adhk_jenis_pengeluaran
    ADD CONSTRAINT trx_pdrb_adhk_jenis_pengeluaran_pkey PRIMARY KEY (id);
 j   ALTER TABLE ONLY v1.trx_pdrb_adhk_jenis_pengeluaran DROP CONSTRAINT trx_pdrb_adhk_jenis_pengeluaran_pkey;
       v1            postgres    false    263            8           2606    16567 &   trx_riwayat_data trx_riwayat_data_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY v1.trx_riwayat_data
    ADD CONSTRAINT trx_riwayat_data_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY v1.trx_riwayat_data DROP CONSTRAINT trx_riwayat_data_pkey;
       v1            postgres    false    246            :           2606    16569    users users_email_unique 
   CONSTRAINT     P   ALTER TABLE ONLY v1.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);
 >   ALTER TABLE ONLY v1.users DROP CONSTRAINT users_email_unique;
       v1            postgres    false    248            <           2606    16571    users users_pkey 
   CONSTRAINT     J   ALTER TABLE ONLY v1.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 6   ALTER TABLE ONLY v1.users DROP CONSTRAINT users_pkey;
       v1            postgres    false    248            $           1259    16572 8   personal_access_tokens_tokenable_type_tokenable_id_index    INDEX     �   CREATE INDEX personal_access_tokens_tokenable_type_tokenable_id_index ON v1.personal_access_tokens USING btree (tokenable_type, tokenable_id);
 H   DROP INDEX v1.personal_access_tokens_tokenable_type_tokenable_id_index;
       v1            postgres    false    226    226            M           2606    16623 7   mst_collection mst_collection_ref_organisasi_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.mst_collection
    ADD CONSTRAINT mst_collection_ref_organisasi_id_foreign FOREIGN KEY (ref_organisasi_id) REFERENCES v1.ref_organisasi(id);
 ]   ALTER TABLE ONLY v1.mst_collection DROP CONSTRAINT mst_collection_ref_organisasi_id_foreign;
       v1          postgres    false    219    3370    232            N           2606    16573 3   mst_jenis_dok mst_jenis_dok_ref_grup_dok_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.mst_jenis_dok
    ADD CONSTRAINT mst_jenis_dok_ref_grup_dok_id_foreign FOREIGN KEY (ref_grup_dok_id) REFERENCES v1.ref_grup_dok(id);
 Y   ALTER TABLE ONLY v1.mst_jenis_dok DROP CONSTRAINT mst_jenis_dok_ref_grup_dok_id_foreign;
       v1          postgres    false    223    3368    230            V           2606    24609 7   trx_collection trx_collection_mst_collection_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_collection
    ADD CONSTRAINT trx_collection_mst_collection_id_foreign FOREIGN KEY (mst_collection_id) REFERENCES v1.mst_collection(id);
 ]   ALTER TABLE ONLY v1.trx_collection DROP CONSTRAINT trx_collection_mst_collection_id_foreign;
       v1          postgres    false    219    3353    251            O           2606    16578 *   trx_data trx_data_mst_jenis_dok_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_data
    ADD CONSTRAINT trx_data_mst_jenis_dok_id_foreign FOREIGN KEY (mst_jenis_dok_id) REFERENCES v1.mst_jenis_dok(id);
 P   ALTER TABLE ONLY v1.trx_data DROP CONSTRAINT trx_data_mst_jenis_dok_id_foreign;
       v1          postgres    false    223    3357    236            P           2606    16583 A   trx_data_organisasi trx_data_organisasi_ref_organisasi_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_data_organisasi
    ADD CONSTRAINT trx_data_organisasi_ref_organisasi_id_foreign FOREIGN KEY (ref_organisasi_id) REFERENCES v1.ref_organisasi(id);
 g   ALTER TABLE ONLY v1.trx_data_organisasi DROP CONSTRAINT trx_data_organisasi_ref_organisasi_id_foreign;
       v1          postgres    false    3370    238    232            Q           2606    16588 1   trx_data_tag trx_data_tag_ref_data_tag_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_data_tag
    ADD CONSTRAINT trx_data_tag_ref_data_tag_id_foreign FOREIGN KEY (ref_data_tag_id) REFERENCES v1.ref_data_tag(id);
 W   ALTER TABLE ONLY v1.trx_data_tag DROP CONSTRAINT trx_data_tag_ref_data_tag_id_foreign;
       v1          postgres    false    228    3366    240            R           2606    16593 2   trx_data_topik trx_data_topik_ref_topik_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_data_topik
    ADD CONSTRAINT trx_data_topik_ref_topik_id_foreign FOREIGN KEY (ref_topik_id) REFERENCES v1.ref_topik(id);
 X   ALTER TABLE ONLY v1.trx_data_topik DROP CONSTRAINT trx_data_topik_ref_topik_id_foreign;
       v1          postgres    false    3372    242    234            \           2606    24764 i   trx_jml_angkatan_kerja_tingkat_pendidikan trx_jml_angkatan_kerja_tingkat_pendidikan_mst_collection_id_for    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_jml_angkatan_kerja_tingkat_pendidikan
    ADD CONSTRAINT trx_jml_angkatan_kerja_tingkat_pendidikan_mst_collection_id_for FOREIGN KEY (mst_collection_id) REFERENCES v1.mst_collection(id);
 �   ALTER TABLE ONLY v1.trx_jml_angkatan_kerja_tingkat_pendidikan DROP CONSTRAINT trx_jml_angkatan_kerja_tingkat_pendidikan_mst_collection_id_for;
       v1          postgres    false    3353    219    265            Y           2606    24711 S   trx_jml_museum_bds_pengelola trx_jml_museum_bds_pengelola_mst_collection_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_jml_museum_bds_pengelola
    ADD CONSTRAINT trx_jml_museum_bds_pengelola_mst_collection_id_foreign FOREIGN KEY (mst_collection_id) REFERENCES v1.mst_collection(id);
 y   ALTER TABLE ONLY v1.trx_jml_museum_bds_pengelola DROP CONSTRAINT trx_jml_museum_bds_pengelola_mst_collection_id_foreign;
       v1          postgres    false    259    219    3353            Z           2606    24727 I   trx_jml_penduduk_bds_jk trx_jml_penduduk_bds_jk_mst_collection_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_jml_penduduk_bds_jk
    ADD CONSTRAINT trx_jml_penduduk_bds_jk_mst_collection_id_foreign FOREIGN KEY (mst_collection_id) REFERENCES v1.mst_collection(id);
 o   ALTER TABLE ONLY v1.trx_jml_penduduk_bds_jk DROP CONSTRAINT trx_jml_penduduk_bds_jk_mst_collection_id_foreign;
       v1          postgres    false    219    261    3353            W           2606    24653 M   trx_laju_pertumbuhan_pdrb trx_laju_pertumbuhan_pdrb_mst_collection_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_laju_pertumbuhan_pdrb
    ADD CONSTRAINT trx_laju_pertumbuhan_pdrb_mst_collection_id_foreign FOREIGN KEY (mst_collection_id) REFERENCES v1.mst_collection(id);
 s   ALTER TABLE ONLY v1.trx_laju_pertumbuhan_pdrb DROP CONSTRAINT trx_laju_pertumbuhan_pdrb_mst_collection_id_foreign;
       v1          postgres    false    255    219    3353            S           2606    16598 c   trx_luas_wilayah_kecamatan_kelurahan trx_luas_wilayah_kecamatan_kelurahan_mst_collection_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_luas_wilayah_kecamatan_kelurahan
    ADD CONSTRAINT trx_luas_wilayah_kecamatan_kelurahan_mst_collection_id_foreign FOREIGN KEY (mst_collection_id) REFERENCES v1.mst_collection(id) ON UPDATE CASCADE ON DELETE CASCADE;
 �   ALTER TABLE ONLY v1.trx_luas_wilayah_kecamatan_kelurahan DROP CONSTRAINT trx_luas_wilayah_kecamatan_kelurahan_mst_collection_id_foreign;
       v1          postgres    false    3353    244    219            X           2606    24695 S   trx_pdrb_adhk_2010_lap_usaha trx_pdrb_adhk_2010_lap_usaha_mst_collection_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_pdrb_adhk_2010_lap_usaha
    ADD CONSTRAINT trx_pdrb_adhk_2010_lap_usaha_mst_collection_id_foreign FOREIGN KEY (mst_collection_id) REFERENCES v1.mst_collection(id);
 y   ALTER TABLE ONLY v1.trx_pdrb_adhk_2010_lap_usaha DROP CONSTRAINT trx_pdrb_adhk_2010_lap_usaha_mst_collection_id_foreign;
       v1          postgres    false    3353    257    219            [           2606    24748 Y   trx_pdrb_adhk_jenis_pengeluaran trx_pdrb_adhk_jenis_pengeluaran_mst_collection_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_pdrb_adhk_jenis_pengeluaran
    ADD CONSTRAINT trx_pdrb_adhk_jenis_pengeluaran_mst_collection_id_foreign FOREIGN KEY (mst_collection_id) REFERENCES v1.mst_collection(id);
    ALTER TABLE ONLY v1.trx_pdrb_adhk_jenis_pengeluaran DROP CONSTRAINT trx_pdrb_adhk_jenis_pengeluaran_mst_collection_id_foreign;
       v1          postgres    false    219    263    3353            T           2606    16603 :   trx_riwayat_data trx_riwayat_data_mst_jenis_dok_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_riwayat_data
    ADD CONSTRAINT trx_riwayat_data_mst_jenis_dok_id_foreign FOREIGN KEY (mst_jenis_dok_id) REFERENCES v1.mst_jenis_dok(id);
 `   ALTER TABLE ONLY v1.trx_riwayat_data DROP CONSTRAINT trx_riwayat_data_mst_jenis_dok_id_foreign;
       v1          postgres    false    223    3357    246            U           2606    16608 5   trx_riwayat_data trx_riwayat_data_trx_data_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_riwayat_data
    ADD CONSTRAINT trx_riwayat_data_trx_data_id_foreign FOREIGN KEY (trx_data_id) REFERENCES v1.trx_data(id) ON UPDATE CASCADE ON DELETE CASCADE;
 [   ALTER TABLE ONLY v1.trx_riwayat_data DROP CONSTRAINT trx_riwayat_data_trx_data_id_foreign;
       v1          postgres    false    3374    246    236            �      x������ � �      �   F  x�u�뎣 ���l8��.�,LkEm@�ݷ߃�U�l3i:��q� r���|��[pfvz�.D=�ֻ�A�����=���n��Ի�E�k��3��t�Y��������O�g���vs1^)2Qh^�f�ov3�/}�K۩'�<HE��:R⬟n��7Z( ��ӫKXs�j�����4�#x+^�l�l��{B!G����s�����x�E��ƞ5��\6?�-���X�
K��=�&>����S�0���/����-Ҵ�h ����k��W�Jȧv��w����B2���@�������{w3�_�y��P�N6�_-k��X{귎f^H��/��K��+���Z�����6�W�b(��W�M�u?w�h㻫Y3T�	B�:�{���FNىZ�5�ސ��a�+i���'�v}�gK���X+Y>�<�t��eh�~��@�L��*j�IL���)b�y�;E+ �๫ ���<�1�eЭ�~�;?�ͅ������%�.v�W�ٯ~�ԗ�K��t�Ge���@+�	��T�5����r�']�n�M���OI��c�Bю      �      x�̽�n�H�.���)�̆(.�7]�����j���=��� �*�l���صk���p��<�Έ�;�)if`��*��L23��E8�]�R�Wٛ���,�E]\������g�?��}J�T��n^>���Ga�f~�x��u^''-�]�囬��2F�*�6rނ��~����k��.w�͗l�����oI]�����{����㟟6�eQ1e�U��CV}�vY�����sF~0�F��`rO�4����ƽ^���>{c�x^��>���޻˖;6��m��F=���U�fc��e�ef�ʿ��._��ܻ�/����G�=��xߗ���g������6��pU���z��V�_����5[3~ޯ�W6.\��oY��~�bMZ��k��|��V�l��vWl��ư���:�:i��r�"/غ=۝Z�����l���b^/`�.��_�-��_�gx�s�̼�-��n�l��E�^�������Cϯg4����ш��Is2��o�,�`_p����Z�{���[�����_�q_"5ԔQ];3����#vҎۙOY�o��DAK�%+3`/�{ܮ2�1ۼ2��lC�쿗����蛪^W�=�Rr�+��/�7�maڪA
R� �\�g��(v�&|�a�4�.�n�m�Ü�:���%;
�%�Rɶ�{7�>��]����_|�J��Ǧb��!qx�Wl�l�_���a�a�� 9,{��4~�7����Ȋ}���پf�w���J��K�%_/�t]~�'��9��+�;x��?���6���;x����3N�#M��x$?�u���ړ��l�/W��b��M;�۟�4���m��)��:~�n^+�i���w���j���r����܇+d���2�ي�����S�¤N)vJy�:��S�;9�Z�=cv
FNZ���)��w8�_�k�6lY�_�j�����L���ݖ��}�g�F�T����yY��̻|�n��0��GNZ�m�j�t�ɮ��ڳ�_|��H�#�eYl���k�894�xt�Lݴ�;�)����c���%~h�s��ŕ��y�^V�q�fx��}��S���iM}r��|K�[��������[��5�,���q�鱞�� ��~�z������6K�f�l��fNm��u��:i��ȶ�z��_�Q�eϼ_�yƦ��s��g�����n�]��~�c��n��o+���0��GN���e����L"|*��z�N�8�٦���*/�)��f����3v�m�����v��:��s��9��������>Rl�,�Lپ�g+8�ak���,��Ѐؓ��&'m�ay����f' >��v�N�=yl�)��ү�N�G\h=�j�{���/E�z�T6c�E*�9U�晌�����{t��-��m�����ي�1|*���_Y8���ίY���Q��wvX<����\3��E�=-_svf�ﱱ{J��� /�Rׂ�Ϻq�O�K�ݢ�]
�Ge��y��:-�WR�|CA�9�(��f�Ol��:�ʊ���lk���=ʤ�M����<ʄQ��N��@�D�h~����L��7�2tv�jCoJQf�@�g2�ێ)��,�ۮ�����7���9���:-ԇ�&���V����=�8L.�����姲pb�%��N�4ש��	����nI�����Z�ZǴ~v�x���`P|AE����r��b���a褝d�H�_�7E�z�ٝ���'��r�=�O�]2��^���L-�Iӈ���FU.���g��e��-������|i��D��3��wg�u���;�Cw���J��>�쓊>)�I�O�}:����M'���C[�>ę\'�e��d2ɫ"ߠz|�o����f�d���j�����(`[�I�!���,c�v�F��#xX5/7�}�ї��>��~�ui�/��~���+������Q*��QJ���TУ��6�/�1B��p���6Ū�H)3o"Da��:��h���fr�g+�9l���l�� ;��Y�\�8J�<0���h�/�tr�ܟ����R����c&2;i=n|R������]�3[b81��jzhlt+�r�Fs,�|��'̈́�����5I����ՋZ���D�i���8Q��r�DX�n��,�]5�b�V,M��i�/6�0'ciت�I]����L�S)��2�}��(�>�e�jӢ_��aҹw�-j0jz��T��^�0��|Ϧ����%��y�n�����x��p;�&��+�ĉ�$˼y9��(}~A��	[��s�:ƭ��"uN�1����5{�t�~�W�8�ִz��փT�����f�ތ46��1s�|�L_�����2n���[���pK��y���"%��u�@�����RkњZ�k�L�ijg-'7��R5�+�P���k*~=fя�9g�ۈ�U��Q$�'���,��������QS�¹�\Ad{�ɰ�C��8f��r��L��� ��ŞB�4HRj�O���x���S�{z����=��u�/�9�����������^��z���'��L<�ߕ��8c`�|z �_d��/XO�o���:���C	�^�M�o�|S��͵:��L�Žn��09��z�ۛǇ��]�\(�~��C�\���C�FfN�P�m8N��<CC�s����n���W�%����g���q����_�R��\�� .L�.��D]S�5S�(\q�cJ[^)���:NܴH.ή^���_�.r�I_�l{�,!�J4p�;����p�Br���������P����Jz_���9����=�B��SG;�:���4���)�N��u�Zw���}�NZO/�}�bZ��|eW�5��p⺲����ܢ�R�ѵô�o�8t�oҙ��܀�W�$�Ix���Ch]�W�q�fH���+i�_A�~g�S��,��IIS)vJY�T��aȟ��N�i����ˌ<���5}Iq��al.����&.��\�aVl�'.�J�95;��s*;�W)vn1���b����I;��\|�ْ���DS�����[è�X��8L�ӿ���W�o��\%�D��i'Y�����s�^�IS������"�$���8��{�ݲo`�]~���Wa�h���#T��Kv��+މ	��S�d��rz���x�����nR�}���k���-I���h��N�?�I��?3-��[h���q�˒}i�n���l�V�����L��Á��3���e=��M�.s]����n�뜽�q8�x�YV ���~b�6~~�Bv(�@+u�9��+D�����6�=�|���~��;=&�K�JB#���-���썊h��G���9�m}�vP`l�ԋ�)�O�7m#Lf;�!	[�-�}�˟�gC��������M.�^~�]��8g�Ģf;�.��+�j�5�.oY`+���~�͎�1b��A-ԉ�&?=���fm�����ܠ:�B'�0�Us��B�4@0�����_o{Ms|��Ƒ._�S�	1DP��
�CJ�J��_�r�%?ӗ֠�BNl{��)Ŋ�r��]j������eMA�M��Ϛ�_�)H~�{�~O��o��F`�GNکo��D���D۝S}��s�'&��0'n7K��A�12"ȸ�� +�nF��ǷzN�H��0f��/��{@9�r�w3ңju�8����X��=��B}��rδ�\��� ��t�+�P5�G�}�և�x<X�7��ް#vm"Þl��b�ا��7�+�B�Rt�Ý�� ���M+p6��Z�ӡ'�؞*1��Ň��g�õ�!� ����+�hGld�F^��ԦJ���*�wIb��ۮn�#"��i�xS�Uڿ�3nߙ3x��ASɛ�8rH��U�p�Tщvt4d��7�A�ȾX6Tn�(���+�Ԍ��)��m�O=�1��9�q�1'����u����,�i�0��xrH���#m��^8b����Mſq:K:���Gׇ8y[�'��Ѳ���1�g�h�C��{�QL�����Ƕ�~1k2�H��0�~��~���y�r�0������8i�tD[�p�������ڝ��kx�:��xϛ����{�	���\���v�n:��"��o    ��+ !'�(\j�Q�2P�}Cv��k>(C�k~���� �Q�`��_����]�D�Q��]��'`��z���y�Y�>�T�)"�-�������
:aL
�Q����fǖ)��jٕ~X"jZ�i�r���!�"B�3z@x���Z�+��VL�p9���S�5o��0��I;��Y����&�F�Wx��2�T��i�G{�c�ZV5&�>�Cϐ�H�Ď�oC�F��F;F�l��M�E�iYl!{i8øպ��]-���<���g�C>v��n�%nZL9�$Uܴ��EJ,R�B1�y����s��d�������o6����l�+�G����ލH �+����
v^w�P0��󗜽*���_�㔷+<�JO������q*��q")g�S�N2�ñL9��,�S�MK�F;�CƵ�/� �3�ۖkv����@�L4]o�;���X-����}���d����_�@"i���!>���<I����k|���P�?����#4����ȗ'Rp�)�M%_�z8_������#�à%�p�ʄ�\���Z���A_`k�$4����̦�.x���z^�W{������z@- �����'%y���I�ӆ$��n�ћN���B�g��TZ�(s��у]����s�nFc#w��'�>����p�1[�`7�2���;�!�԰�V�`�k�&,W�̼t�-b�D�=>P�9�"�Ga��@?�w�����=�����?�
� ���"�$�S+�J�SR������T<%UOi�}p�$��v�S�?I�r �V��#m�NI�#'j��(�驠��f"e!�h��G	�S)؂�w��{0�p�o9�Lg&�n��?��(�Co�>��򚄀K]R�=tIe��YN��D4g���>����³�1c��f��cǌ���o�3{�������Ci��@S�^��B]L�M&�u��vbRaI	�F�F��Kų��� i���v���z�g�����}�³j�$1�s4v��X�~ɿv�L�s����r�K^8��Vv�g�M�<�y,�3'm����Z ����5=3"#VKX��mĪ:v&�Z�>��`b�^@P,�cV�(C|o�z� \
ӈ�Ԫ^w���0oF
ӭ;�~v�
46�����Ɲi+J����A;"�b�3�*8��gE�&� 
����6k�]e���`�FA��mg��ԡ#2��M�#���W�S�`z(��2&d2fM�C��RPDh�G�v�����	U��Lx.�o�ID��;l�1�^h���$�d�J�0`+��aN�K�lIA��3rj������8E_�5GI�3�AK��_�CҤ�Ӥ���iR������r��@�M�����|��>��p'�w$�8��6�{䯍���w3u��~�4W���Ho��wEe�A�@3�Z����UT�"T4J�VG����?Ɓ�<yZ25�޽��x�a�n�����n%����Sb�?��(�	dk���g_9�ҞR��>j��+���R@G�."[��:e���[��u �]'�����b@Lז��!I*�u�P'_I)��Jё(���~s�Qڂ5|b*yc�ሬ�B=?s��#q/�#��n8�T�����e�=7�z����iOJT��E��i�m*i�̂~��@��i����ib�$�����>����馄��0�)z�&�Ec��Dt�0-L�[��hZ�F�|�[W+ttu[��ɟ/*���z�̦�9iCn$Æ�#N��D(Ի|b��}����2���;��F�)h��M�S�b��Yh"�N��c��ơAK���gA t(;t߳Z�b0r�cY���q{/,�<���m��+��Y�q���oun�It�Y���@��*�
fI�I�V�����pĮmH`��Np�l��2I��Rw�i��愳���Ԙ�7W�1;�r�8�w�Ρݹ���s����\�CC�*�`�o������}	¹�v#�9��]��t$���^��6<t4QЌb!Z �!��e�޲�c�eCX�.��M��]���j��D[�R���d;#�W��
F 1)5;iq�i����-+rp�6�B�j����]>�N���B��%/1���%Z#30M�)�OU��u��� �T��r��d��^�1���蘎oa�{62�5ڹ�O&���Hh�ʳ��6^���L��iw�1W����,�X�S��ӂ-I��T��Y�ך�n<%�Q���CHb+�R�����6N7y��K�oV�k����1'����C����F��Q$䯡5*o:�p 9���v
�WOCS���G�{M��56�a�*�SF?�i�΄�3��l�__�����*.�Y[}�U��Я�ą�$r��O�?��������Z����&�.S�~�`���S+�9��o^+�rƄ��m��Zc�n���XC�ZP0K�-������H�D��_��X���9�+z$0��t�cg��Ӽ�������_:���̉ߟou ��Al'��u5��4�|����ܐiG�_�?m�WJTc�	���C�l�NB�n�^��»�n��R�$^����GOW{�*&�$	�>2�m��]��Gv�7�ѻ�o�Ga�q�\���r� �R嘇:�N�6<�/Ir�b����5� 6t{-�I�ܳ��	�C�=����AF��NK�*��z�B<�eB`�VK߸�r6>��:J/�ܰE�qs��D�-�y�e�a#TH��@_��i��M;%uΰh>��զ=��{�	�Ŷ��h}�4Q$���-{�v�����\nC#V�kwGyH��ȮoB��p�;J��c4�</�+�!������k��v���4"�TB��r�x\�-+j(ox�o��ĺ܂]s��s�zE.R~26�E���$�,����_���J��T�馜7�CcM��\�xkX��2������2�׭�|0�[X͜�������CGs��e��Xb� @�6���C'�m��Q���$��3�vT���&�QsB������+�����T������<�~㙜�����m�=c��
H�+ůJߓ��Ns�Џ0,E�Lu��t��(o�N�'cP��m!�gS�ba�b�T��� �qъ��ln��m׺�7�n�����&&�U��@�oE4��/�/�!���w&"��J�:���e��J���L���H�8ߴ�>��;�����<'{�d��|ll�/NGO��v�{b�$*,�Լ�+��91�>uv�� 2��_6���~|�kI�U���i��:�G��Qd�引fm�a⧎aOf�6@�w��D�2"�⻖b�r��0	�@M�D�A���cr��Q褵��.+�(�xa���Tk��%��e�5�q��Ң�@פ�As�����C@Մ��%��������
�x{ݸ��K��/:��yޏ�F���q
����'������~��cԛwR.	���:�>����HP�dt��^Y��̀rD"4@�����I�>5�(���fb�w��ՋG����l=/�r�;�U�ΉAO'V2 �&O#ϻ?�
��hy�-�����(p�'��c��!Xb�p�!�G����D���@U���T8Ns^6"@ ݲ
��U�S����ش��4lDD;h#rY[�c��a�-�?�/��,v�l+������.�Ƌ�]����dָ���	�a������!����ef���h=1��)R��-)f��^2|Èq�[����4:|?��*S�/� v�X@6�*��f��vD����4�J��.pH�bGO�<.�*��'�-�8��	����ݞǺ_�����N�.���D-Pg��虂I![�#E�}l�  �,3�+�괧�֨�݈qH,�ϣ
Ew�]�0���)�q( RC�ϖx+��?[tWA���T6�)��JɎ����hg�i	45�՝�^��)[7��w���v����Ӣ��z�O�m���	؜ �6��Y�͏KQ�>��I;CЙe��O;@_�r�v�ڲh���i�-��A��ⷒ]���=azʣG��pVl��?/?�2�    �V.ޑ�O�[��2-S�2�-��$4*Zi�h, ���+��bM��&�N�m;Q& ��Egh��Qx`��0y��3�l}�(}R�'�>=�Cr"�x�F;�J1�t�/K
w�"�p�g"h��u��(e�b]����B.܊+ҭx����Dz��*�Z���Z9e9���n�W	 o�_�\����e�/=�w�E;��8�h�LQ�`E��t�@3��A����M[��U��Rުc��
�N0�uh�U�3ɘ���O��[w�lL�ޟ������ޱ�e�R�J{�]sU�[�T��X
ڼ��=�Kϻ�-3(j��\�h

#�?'��MEò?	O�I��@�Hp��6�Di�s`2ф_v��
J�t�K?^�R��v��2dM]>��t�ԏ�c�=7!�u�N;�	)�:��9���E�d#vj�FxI�_R���qWr0r�zf�&�D����R~/���`	�(>�ӎ�t�����*��NT�
Ϯ���(�%ʰW���5MZs�U���1CRn��h�5aV�R��fr6��Ti�j �X����)�b|��N���?Q쎈�����/�D7ܔ�@��F\����㊭(�gذ�3�`:��{Z�H�4ߔ��\3ݖ���[}���bm7�<�7�yQﭒ�d5����~T���jЎ7'�����y����B_�]�0�v�?����zϋ 7A��4�M�r!E�х�5�������%�3*T���졩X����Y_��y_0Y�$�f�!��ꐪΉ�*��6ƈ}o���)�l�UJߝd�ܤwy�jn�~V>�t͍|�uA����$�57�~Vέ��q증�I����B]?��h��(1*�i��mGA(�៖��w������?��h��eK��˟S��SW
Q�PE+mZ3���RՇ^��6��6��C�h�jJ�6�.��� rҎM�TU�%VD���KM ����t@Q�}�Ak�r�`L"���tv����u�bX;E}���w��j= ������l�v'3e}�'g��Ϋ�҅�������FI;B�G�F�A�#���XJ��s�0���BN�Ew��ԽM��L��I;�;����RxFL�ы3��2h#�y���+r�É\��#�����&�o��4l�Klev�����D�\g��q:����Et:M͢D��s����U�����`H0���O�c��n�A=�i��Pᏻ�� �#���(�@�c��R"b;��hc��!��dd"�,߳y�Wz�q�Ю�����׻#4,�k⦝_Ê���!k��������m�J�8p��xL]r.��rWnq�+7L�0<�v&W�x��SyE*���?3}������i��2��*�L�V�K��^�g����i���(�Юʛj�З��l1�>@y�������VZ���.�ؙ_X8?^����B��!s�6_�5Ï6�R\~#���Й��L8�'j��C�x���!�F=�������))=�P�u�zJJOI��d�6�6�~t��C�vtu5�Յ��,��4�G�1��=@��;�� ��f߉�w���zo����?���.+��n�/?�u{���GEsL��en�;)��J|�c���������. :�Q�����v���p�.���V4���u�)E�|f!���b�9t�l髖���9U?wH����!hG�3������[Lcˡ�8fe�b�U��T��m�1c̫���E!�~��a��6�D6��yżA��t��"Y(ޢ�\lj������@~°(p-�߰Ɠӡ"���j턢�ʻ""+�;(G�p�b{��,�#kJ������z:lǡYM�2�B�K��u�6�i�N�y��3�s���ׯ��)�M{�ޡ�'��3tZ_����
rR����wY[��xlxq�y�/:��=��������M��j�GbyF���E����0�!Aڀ��J�i�$�j���%|�sm���fd��&���pJ�>!�N;�T$�\�&v�6 �D8�H��k�k�x3���PM�K0�
+A��1�XrW{��=(/���G���#�q�C��v�y��>��>]��b�ί�n�'*��x��4�
0�Ns'#X�N�70�����@G8��i�h�u�E|q/^ק��Or6��@��"6��!�1�ÝI�:��<g9��%��062�4��ƛL���<�������?�yBQ �:��N
��Ʉ�|^���Z($�n�^���)IL�������1��\%�7k�8/<x2Į��ً����>�B�J�t0s�z�؜��T q��h��!K�x��`p4vҘJ(�Z�f�l���l6�7���%G�B�˅���2^���^D���o�~���՘F;i�hk��Pm��MvS,W��ñqh%�f<)�N����1Z�C�`S������
�Κ`�x����S���`w���Jc��:l��um��h�N<�6�К�����]��G8��vTH�=�`,R?m��`�#.�БB~���-ڹLeQ`��i~�G��-l���b�ɠ�@jç����N�%Y:]�5�V>,��8a˼����� �V�R�z k���W| :��e��g=�-F������vT0@�� �����lv�g�w��R���0kϩ�8a�n��,�͛�[����i}#��Xf$%�%-k��]`R�{��?4kt�j�^�I?t�`�h;iC��Z�#"��~�Y��Ot�f���1ݯ�FO���m!Q3z\�1NM0ԋ#q��,�G����}���FN��V����ٺ��'B.���^��Uѱ���m5�+.h]�-&�i;&��cr�0n��8�
8��M��|��fs��ЁGIӎD�A� �_��.���)!a�Z@�:F���IX.&�M�bb�.��zU�ǰG��l�m,��W�*�{��1/������$���f�y�s�Ź�_�P�Kq�e��X�������]�p �Y�x0�2E���R���W�˔�L�e��'��R�!y��)��Z��*@s���n. �1�_�A;��%�p�X�H�`�2/�>�$���art)x��	ӊOG �̪j(i'�g��o&�����Z��<��ޢmM��ɵ���.#!͐��e��~���u�p��}���gx����RxV��R@|�_a�ba䤝�
�&�5�d`'���p�%p��j�hW{�m_w �u
Q�()؋J�Ȟ	���j�ٳ�k�e�CC�<E=X�r� �P��g�yj%��uJ��V��3�ξ�÷z�-j@����u�=�`ktJ�-3�`*# �D�j��ĢQ|������1����J�!������<�i�ImȪ�.h�
���3�,!؞���E��������z�"��Sܯp�l��78���W�-��Z���`cLa�)��m�Sc�Ǩ�(7��Of1ƔƘ�S#/�W�����_�B#n[���G�?��
Q�,_��a7(l{�C��Ї:�۳ڢw'\��%s�;ّ��wl��:�iK�Tǈ�ij&R�ֻ���ZEi�����`��9���!J��w�l�N�(ۯ@�� ��yN���_v;�d�i�y�|��KLGNZ��F���;�F�3Ӄ�uZ�Z��$Ђ��)�9ei8�, �D��p�7i8S�ͷqj������
#ӡ��\pe2o����Ee�Voa�n��|s�G��Y#�Π����d�H,��B��u���[@�(��`�5l&��Z�Βvt4������3�hH��{�?gCJ3��)֓��Ay��fWx! �?CA��b|݇I �&��Is&���D���V�y��xV?�-D�/z�l�Q�gK��d��T��wJ)vʞv�W�V��V)[�
��p>��������¦Sx�T�M�o
}S�W�esǒ����g/��n�0���#7���"i���n�M$r��1�VL�vP�D-�1Z��n�{&߼����)�gÔ��k��5$X�_�b���_�>,X�C�J�֏�-�א���^WN;��!M�����    ��-+G �P_��@�O$#�,��H��8����O-5�*���adӿ���(v��A;0��1\-V�g�?s!�Ca�?��"��Bz�QzbF�2��6�v��$a����ߠ"�����^��XX���m&�f47뒁N;K4�_�CV��0� �r2��&�v�c:�]b�]�mx��0v��jb�bKڹp��{��~ Ȑ���*�c�V ~�}�XU�1P��x-T)�.E��G;i'OFc[K1�Φ�����^u�$5L*du�Uu{�M���# �Ԛ�4O;q7�\+X�mRl�b���iyu`p���F�CS�����Il�	��>���s29k�U�9�1�?v*�,Z��3����Bq/ݿ<b�,��.[���>гؗ�ce��e�y�X�ZS��Fr<��[0#��u��y���k���7�3��%r�x���eQX�["��7L�a
�_we�4Zq@�iS��w�.gG�W�ͨ��B+F��;&K��E�+�@�z6�#.��X$��m�)�@��`����J���U�b�e�|^�v�nհ;\u���հc=d	�i��m;nRփ6)rҞ&G���)Q������2!��]v��I�n��:�Ҹ��-`��o.9Pq����hZ�F'�O{��(�9����9��Mq�TB�[ʻ��9�T7�^���X��,ڱ�;�T��f┽
<S��k�����Q�To ๻.U�ʢ�y�N�H@�W��YQ�CXxn�|�K=�d�`�,|΢�@&��F����o#�is}8�2]�d(Y$��M8��H�����@O;����b��1� �q{�P�=Ku�^������@�9�5N��֢���a�N����a�ڜ�����%��.s#�Q����Gt|A���d�m٢��F��D6H��#�5�@�=۴�۝eR�W���%ϏM��
��k�ƯJn��R�C�?t������4�
�#��^;,(9�R ��g%����h��l���x:�@�r����f%��y
�����M�oǓ�5���)l"��_f�Zw�n���Nڠ�4�)j���k~��C�����h�mO�1-Nc�#=��X�2)�,_D{�J1n4�X��6 �;��z�l��>�u�_�Mru':k�]��L���U��¬T�T�h�i���}���-�����z]b � ���֜�W���)+��@2�M�I*�`ţV�#��Z�ʿ#�����]�}٢ޢMiO���$N���@�5h=���H����=������L��w��k�Y&��R��Ъ�ac0h�W]��A�+^��;���������J:�^�o�3��P'��d����xZ�u�H�Y}���PY�v�=>֑�uZ�ڷ�WJ8�IX�ɣ�L�ڼE^q���L�� �D4��ɚ��d5K�̱������׎�G��W�$_�V�]<�^�z�KK�*��:��d��Le�N����_���{��0�f�f!�e��c�)���"6�fW奐4�#[�:	��%OؙdZ�!�R���6�*��Q6�8�3G��;�QQ���c���2s�%�yb����w��U<ɋ�'i�j\�um�qJڙ�Z�8�z;g�RrD����qXr�wa�= #h�xh(���G�>g�jx�#�
WK��Ȏ"g��I )�H9Uփ�0T�x��#E2<��$��O�6��^ϙ����d�'���˲ʯ����ͳW���z���̓��>`?�QT����l}�������V˘�l}`�#[��*���j�R�+^ZN5��4H�x���l,��-Z0S����P�9?�-���!:��(���p]�͇�{�����V񏎻&2̆���e$��~��m��t�����e,�2Z�b��������{&M�ZJ	k��F)k��Fi�n����M;��c@q9�6�DlH���p���]>>��z��pӔD輠0�\�@(`�o8�\/�.��ty���D�TtJE�:�Щ}��vI���3��-����J
�;v�����ٱ_�,�J�/��Q�G	����=��׻�������6�m�W$I :�v������	��˘��"E�V��K�X��� �3YKu��b��f�� ������K�X�qmSKl��hG��=/\'���<�X�K8f�˂�;2�ew�Z���/{�Zo�pѺ��K*��Z���HF2����3��H�h�t����eh�����9F��G��]��ʁ�<^�'z"�Ϟ�*.ӌ�\G��R��|Î`���6I��@�+$��@�e�]~*_��u����6��K8�
�C]-뵍�ɷe'�������/*�![gqK��%���  �m�D�G���e�F�7Fc�C��k�0��Ptl�.z�v��p�Bq���P-7�U���B��.�+�qϪ���u�Ț��8�����r��gܾ��ik��I�i��/4{t��%~���-1$m+a#�i�C�Z����
`{��ܨ�	����_�(�Y0t��螡�����n���_�Ȯ��w�{�|����v�Ȅv#�Ɍs�Y�̄�_�b��/��c:�7-�`�f܊���;�}	��6�8�kVp��]�$�2�[G���3:m ��d4�j�bF�#هL\���M`�����=������$�x;䲳q�72 5�2^QV�A�-L�����M�������dE�ąc�2U-S�2��-[�R烙�6<�4#��<�	�feM�o����t��9�xk 67{Rc5x�;K�6�m�;^��u�+�)��ôa1u#2�y�_��W�>&0`i��U��(;<�K����h����Ч����8�?~�.vY�_i�E�EiZ�����L�6ߴd�g�!R��I/���eO�$<1�8iҕp1쵚Ia������~�w�0H欒�ׇ�;�E�cG���5�5�N���j��-�@E]��I��$��g�0d�p���kE�p��,g�� �P?�	�
��"�����H��KCn�֊B0RP!���@�)jC\�M�c*9
QHr�7���)�8�S��4Pc�6,$	��B���s]`+�����89���V��R�G��2�ђD��x�`�yh�Y���V��G�Ǔ����E1�c�ӹ����vb�(1*_191�J��Q!����cV/�-��_���
�0W��1��Y��z�c`i"0y$S'�o1�hb��{)�����_=�_"�	�#�QP��ظ�2c����s�v�8ww�7����L�'�֥��g�ѳm=bDD��a1��p�\�����{����� o؉�V�ɞ:�
��<��Z�߽�i�i�!�LUEL1���
�����F0'9�$?u$6D��m�NFc�fSsO7�@>SѤCjl�+v�y���z�}j9rNj:�?�1^�F��N;}��T#;�-���w��%��E�5�~�\��nю΋��*33�56�ޭP]%��D?���u<B���w=@��~�=�Q��KX	�/qaM�BB����T{n
�E%f�x��&��6!	�}�	�F�:�v$8!;V��.�r)]�e�]�q����#G�܁���M8S��,f��{9iG@Ɖ-��;ܸ�����~�)z8i}���Đ%���1���PiS���[����W�ٴ���o�Xzz[U6��H�x ��N�EPH�C^�����P��Z6�J;�?w&�������d4����C5&���i��TE������'�OE������F�'����;�(T��I;�����FiS	m7h�n\�:���>�x\��~��cF�Ժ�$�;T����	o��n@�FY�(�:i�O��/Րe���Y/m��:/�C3�A"w��S��������ޑֶ{���(��I.����$䑐��=!����C��������(�h)+�~ţ���Ӫ%h+$���,�eц�ŋIv�������P���	���C��^���U�W�<�b�� ���	�7�-ȍ
)o����Lk*��w��y�6z3>/Iz�W�֖    �*��= [}���9'"ۿ������pI�x	��jR��#8O�)��c�a������;�?A�&mQ�A��G/�w�nI���R��`Kg�� ��Y�̗�XGl�Gs���ꊽ��y�&N�1�e4
LՕ�y�g��-�PJ#剞�h�P4y��l����dB�E��na뺧ӷ��51����GdE#�o�߅�4���+c��l��z�*RRE�U��l�D d0�-��Vc�8�R�!;2��aP.g�U���u����I;I�H���G�!�(�,�<Ek&��\
��|3��ʨ�Gkn���QH��*�ߑl��&*x���H*�v�oQb��V�!�J��Zh$=B�����!C/��ܴ>-�`��9`6�����(<�cm���6&���T���yf�2B�U�i��Nc�T�]}>M,ڡ���w^�؄s�htd?g(%�7~%�.J�G[�/H�6F���w�\����F<;�:i���
A�h��A�8ѐ�)nڑ5\̪9ʝ�W����	�|M���lc#�����@�c��S�6���fN�qi@�5��QF�/����X��Fz�kM���̓��v|VI��Ƽ�eP�����g���U�����XO��:�D/�xd��
��>2�k�%_��> `G��A����sU>�b���@��v���}	�y>+�-��OY��h}�ۂ�q�O�?8ٸ�����
T�b�)	t��A;��"�i`��Co�,�Y(�fM���b��@ňWhd�ط�F�,\Q��n&�,�l�i@~��I����aT!�=�)	�+bV~ J��#����8���� �R�q
�4�܈[ʹ�v�J��1W�oi�`lm3�:���wjˉDs��e����o���Uӹ`��)[>b�K+�t��L�7`�5)W��R�	�U�� |���"�Tpj����L��+�F.뗲����rx�Xky����5q<���[��U]4'��wi_�u�!�D%p�R"@S���cK@����?mwʊ�9���!6����ؘ��L��nwVn����L售��m�����3h�1�v�ɦĭ=p�]�l%����Ub�l<$�G�L��Jּ�|�cLR0PA�W[�$Z���t)�g��FM,:Ǎ4D�/B��q�d�gؒz�����hC���D|;��˱��|�=��0�j� ����G�|����s��
GT�M�:�zn{H�)��s��č0QɆK����/��Χ0A���}�p~~���η6<�%[�
g�
&��}�LD���8�`&3�m��)-
�qw?�|U��>p��go ��ɲf{[(���^��4W�a�v��c���|�!�&#�Aya��?�	v *$8���N]��ՆET�pjsj<�� x�_;�� �%��U�*ȿ����ZҦ[W�D��k������<ߛă�
�j�*��vnM7��Z5�g�-�!
ܛ�Q�S�%����]���>^�,j��v�ȃ�/_jq�������:mP�v05_(Nu����L�jD�O�⿤�/-Q* ,� ���rD2���F�:��B�L3g�g%2eM�����z�Is ŴD����$�_�k�A��JO3���v��m�����F;=�"4��2HX�>�.��76�������'�U@ZG�[Gqhk�X&�������ܨ|���_-x�B�V�Jw����O���#\�j���j!�c8��#��">�0��I�N��-V�@̢=��*�'vG��28���@��q����ʺ��f	��6�mSh��m6�e�0�uZOOZN�\��w[�s��q�Fe{��{��_PX��Rt�EN%��E;F��p�)G�Pm�@��.�i���/G�K~�����2��^�F���M�������T��	nbC�9�&�Y!|o'��b��X��� �#�[����W��3�|2���+ �W���r��!X����cd�%Kr��
0���
��ԯ8j��"��X8�u��A;��ػ_&_Ƞ���K�tW�3M
2�BD���3��Y�D7.�T	��ŀ(�u�9�����aќ��v���@Ef�®n�(d�^C�(K3RS�%X��/~bO%�������՝���l^Y/�{S�^~�정��Z���?S��g3(��.g�� >�ꊵ��^���Od� ��Ɔ������K+\�6�LE;3,�Q90M�'Cz��NY��x��4���O���^�Ȧ�ԟA1 �2L��u&�I`�a�n7�R�##-��	f뚃B��r����nˋ�#	b�y��*yZ�T�K�_��/֏�p��X��I�)Ў�Dh��1:��6������h<���y	a�6���/���z�Q�%�	��:���k���y��=�2�N:��")���E��P�
L�a�|�Yv�Q07[�E�ʛ��ȿ$����6p�cÏ��2�*�V+ۯ0�r�4��� =	,�E;Al7>Q�����2vH��u|���xy������HR��4���XB�iCt�{�<�5����&�k���Z�S�9�����t*�9��ڑ"u#��^>��1� ^CA��U��ʑ�.48d�h���R�g��#�]hr�?��)�owq&#C��h�sq�4*-\��}~�r�)��dBVh�Zj=\�/���/�0�>+	šK�1���g��^`m�R�At�O���Otڹ��v�{��Cx.��n���:�u�
v��U��-@��#�-٧ҁ�{S�Ӭ��K���8��`B�z��CIF$٢�XO����'7�~�� XD/������/PW�0l����1����.W�vM��2ػZ�(pF׀i��f&�j�h�����i��@�h��7V�p�I`�Fmx4\�$C=�ʕ��7 �x��h���3$�:��t�[�����&D݉ל���3��C1�F.�ʹ�_���]���i�k*�v��
�q:��T�v�G2_Ƈ�|�o��0��|�c"(/���h��R��c:�Wc�N��Ĝ�!g��q�b�wN�ؔ�����
{7�oHl)�j��>ui_�$�!C��e��l��KX�+蜃ha
��+a2f��j��
��a$Nڹ��(�9�g@QY�$�-Ǥ��b�1Ef�S�Xv�y��r�k��s*�I��1��B�#���+�/ӞΩ�&�����
q�Pٓ���I;->�J��(�9��s�hƎ�]��[�~~��dS	�_����ؘJ���q�
$�A�����}PZ?�Ki��Wy�k���a�1n����;�aڒ�$�% \�wñ�T��7Û�:c]*�%��N��G��Y����V�*�i��݂b�j��5 ��P��?k¬|*�������>?�\z��l���'�!ڳ?�T8w���k�K��\F z�����=>5O|X�� ћ��Q�v6+Ad�E{a���u�����!y�W�A�k��y� \�(H=x���K $�9i}�tC(S�A.��m������p�贳��$ �e��Io���/�]Q��9�To��������,T��3�X�+�~#B������]{�[��[E�61. }%9i'�:��5��q��\�D@�w��y3v�aC&���@�.Ӏ�� &N{�LEOD��2�myώ���X�v���LLW^����~���]�Hx�2ʄ	����J}lڱeb�paϔ��J_��Ψ$v�z
6&^���#�ˡz�l!���ޱ.��K���K����@c��!�^�.�\��HӵB�Q�P���9%�aJכ����ih�4���v��4��Q����L]4��{a��͂B��y&Z��S��N;=�ژ}3��Y�?o=$倂��&���'v��V����w��5�}F0\�6Gz��N���7%���T�1/��#�������d��w&M��߶��I0�����7��bK��h
g���ߡ��2��S�D�R0��E��z�Ể}��+O<���-K]|��w���N�v)�Ke������^���.    A��?�*�]��DW�?L�^���>X��� 7�sw��-u��	���5S�����>4�V���19�X�Β��L��^��v2�^
kC~���;����hWD럈V��4[�7��4k���J-�r�.��|����a�R��Z����х��_��FZ�l$�ՠ�0�rO�CK���آ������^�7.�ԫ����G�A��i�^�Y;�[��R��!yk#K
�h���$,$�%/�C>�՜���F;�����"�L�� ��ì.�B|'��N�H��%su�C��(�$���N;j� �+Bf��!����¡=h��J`�j��|��:�W�U+���:����I�q�cLsP�b�Y6��-�+M���j�\{�"'��7�����99�
`�:����B\����PA����� }1@����b5')�K���oZB���O�H�#$�Ǐ՜��c�	N�-��0�l��Ķ5�8po�{:<����J� ���agx2s�>�kؤ���E�[��a�|��'�8���$��ßа]�'��<����R��� e��б��e�zb�N�w�FZ�Qsv�yo�{V{e=_u���)�w��;5LV�L-Sl���sg�Z�iǝ,S�ضr��f�S�pc�����*!45�k��eA:�1��6�m`�>���CH#ȫ�</@��~�}dS�w�F;��m4��/ӭnu�6'����,�ȗa���gwK�N@�R��ג/�\�N�ҥ�+�jӘ��R���b�n�f�A��(�bV���
��>���_/�E���F;ٲ=3Fݡ����7��k^)u�\�ᦽ_�ۚp�9E:m�I2��=�5_e�nj��V�಑�~p�:m�MwlF��M��L�v�)j�I~��9�tlD��󠓚nH�N*�m<�
g ]K��p��{�$�(�r>�A��iu�Gk봾�V3�'�~ը�Ɋi��d55����Dh���n�T��rfZ���`�qhe��5Ϥ�h������^\�����fl���3PSFi�]2�h����ߡ#x	 *��~[-��{��	����43�l�G��M�y���f���/�d���|���=DDa�������DG���G�A���IXa��'�k�nI�n�	���ū�G��_-��k�i����ke�S�|ym&0��Ρ( ��g�H�U(�SJ���q;�]7�B�y��Oj�O��ܐ�ϟ���B\w�EJz��AE)g�2mig	dhr�A;s�Yd{�eF��ٖ�z��~i�#�L�S�r(W����m�e0^SP4uҎt^G�͆�O���P��[��}��1��N L!���֯އXa��oss���:V��p����E~ZҔY�]\�P� Bd�[!��|$�TI!6���D��H���M�{�����H�'�(��F6�x���ib�	i�
9��@+؊81�V�ض���z'��v�[,��Qϋ��Z�LX����e�hrY�Bw��-����Hʗ| ke������6h�qN%t�Q������P@�@�,+��ҿpR����9��:&D~�����ӎ:~����?g�v'.-���yCx�)��� �z8�N;7���R�`��:���-S?�z9��5�vlW�#�I���Ƒ'
������	�u��ݘ��DN�馷�V\bF|�MOi�GaK��lb���S���C�th�u�c�X�}\���T�CSI&��K�Y,X����6�)H�9��0nLT��_���yx�WP"����nk&�<;S"��H�Į.B��.���r���u�l��v�2�k��H�O��}�b�<P�*�ە��-ſ�o7_W����E��5֠%�x��X�\槥V[@�t�Z�/��y_7�!D����u7bN�5n�Ma�q������&�'��d����\�2
5����x,5��c����L�;ig�{�6`�D.9٧i���{���=��%�/�7Oe�Ce$�J��	�m Llc��:`?Z�/bx)�������<������~�-�8LҰp��Å�{��Y��ME��g*z>8p
|&NZ?#�q�	�,5}����N�I�=&�Y�e���l�$�Ol{�Q#��������JFv\:i���v���Q؟������{.�<��n�9��;&տ{I9��t�yFKUV��
��\�_G�ƨ B���I;Ri4$bGI��C�dt���n2�d����D�jtڹ�I?H�V�#u�MdX�S��L�&c���g�G�.��T)��|���;�X4���ц}`	@駀FK�J��+�daRX}�eҦ�[�����C�c��� T݌�����smgg���>vz?�F
���.(+h����9���y��C�~�����F;=����H��=+��o���t����`:�x��ɲ9���r�xfS�Ah,�ֽ���q��������.���c��'. 2�]S�b����+ĉAה�:����H&IZ�>�ӸQ�O^�
�Av�aida����8�<�>����
~�_�oٜsð�}V�\�Hy�u�G"<�ȱǺ�>�9�vF`�Y��^��NC��a��56�ZACKd �Wr��o�m����W.XJ����a�A=��(�h	'\S��7<����:]��,)�;��hB���c�m�Yy�hځ�OfD�*&3�P���c�٣�H �2F%�w�ذJ�
yp(&�����$7 �2^E����N�H�*�P�ݗ����j��gD�r�k!�ʓ�Tu[��}��-�3E���S<S��".Lp$��I;y�M��gu>tE+��k���"6m�����]�f�)FD~a'��/��u�?d�jG��X�B{������ \/g=c�<%�F��A8^�0dr��v>�q�����.�=���i�*KT�L|����2R�_.�؅��sJ�S�s�:��s��pbT��hl��yi+B>����)g�6��ׅV�x�=��Hq��:�lo����a�)(K8{ˇű�-�߰N;9�yf�E���Zԅ8d+�Ű�(mcH�Tڼ����8����)�B��.c�=պ�����r�-:>Ƿ����� i|º�Q�g�����j��{�cn��G]���͐�d�#�I���(9i۫Jؽe�A|�թ�����G���ʱ�E���r˃+!/�6��Y��Ơ���fV�_I;1� IZ�eR����:��z�̌*I;��5nǻ���2�l�����N,�<���@[�\.�L8·F?� ,+����׀6��ȨW�������=� p�;f�h'[�バ#vh%Юp
�8  p���W���s�I��<
ݴs���B`�2k���ukRI5C�njqX>�P��|Ec݋����i#��p?�+ `��U-�
�5^ϙ��ˇ���k��ާ+0Ϙ7g;��n�A�Q��5�ī��*sq���ЎWFi�*G��H7��3B�Dc�k�L���G�����p"����	����(Ӊ��h�e�GS�k�)(�5�g��lr� 8�0����Ve.I;�Nb��v��u�o�r���R��5�D#;�L�N7��)'�J��ֱX'�H��iQ\;�"����g1BdzYE~,��E���1vջ�0b%�hC#������<ZEĄ��}��s�� �zI�nH���v���D�*��&��e�}Ϳ9����.� f��?1��v��;���v��n�`fVh�~�_�)�������ˮ�U�/�_���T�x�s
?�G�EI3[�hCJ�YTm�n�� %&E��M2Uņ��N7_Y�ƦR!' <�b��n�A��nK�`jA��:S&U S/-Doyu�'�����9��S�}kf�h�!����5Y�*[KO��8y�_����7�d��]|$h�IFF��F;Ն[߈2�u�o!ts�B���=`�h�a*��5�Q�Ķ�4�>�֝ȑ;��x���Jh�E�7�-�}�n)ԯy�%4a��}�cK�-�A,1����g�YO�����K�cw397:�H����    ׻�R��^���;&��lZ���o[� )�=�u#�~�������5�y�=�`*-"6��%��K �8�i,��0���J^GV�'.׎5�Ċm�xK|1QdV�%��5�ȊݤJ�1Q�V�#�U��K�7���6+��;!V��7*F����J.r��Ĺ@��{	eW�2�D�]!���bE��b�NY�dl۹�Ny�P::�5?��#QlD�k���h�3��E�|��� ���$H�ɖn��	��Q|�DkDy�~��d�	/g�٠����k�F���lSc�<����h'�z	����y����)/i;J��9[US��Lckm1�+�Tѽ63�WK@+��ñ�vrY������?�z=Ǩu���m����.���W~L�S�cǴ�E;��T����%J���kv�3e��!�B�����Ԙ^��,[��EJ-:f75
k��n� %�\ ̀}9I�=پkF�QǤ�����^��Iͳ�9�cW{��g^�Ň.>ui��K�]J��g���o�z�.l���Y��5��*��˪�J7V@�&�� �QB�a���Xo�1*R�?��̥K�����W���a����� <nVU�D�>��z8mVԻ�]��kx�.���5�D�[���/94�����zLX���X�h�g:>0SҲ�M��9<Gްc�i/�h�'���sZ��9��p����|�_�DE�Tk�1٩a��h�';�ϝ;��tn���kfTg�h��فW���9��+�}ݰ�w������%]������h}_�Y�����F^��!m��ߧ
F��+* �2f��ٍ��O��Pi�{ �i�_p�{	���վ��2:S��	J�׌���s��fO�N�k��t� '�S!8���	V�k�~����	ѝ~��̜�ޟ@��������U��[�uc�{䥚�İ�h�޳sߞ��vK��������?���	_qa���ր���k
];�e&��,Z�e1q/oQ�~��+�n�]��O$��em�9��6:5H�AJ��8\%�ou]�o�6T�lJ���9�y3�U�~Cd�Of�6�G6_�`p��r�W���6T6h�;���F�g �-��*;��c6c��0��kƋL������9�r��?Z��?�*�JZQ@�^��r���N)v",c�W���WP��zTD�g¦@*���[L ��������i�jѠx��'���1j@pҎ��j��=�?�q:�PYT�
U)^����>p��e{��'Xb(�	��ʍ�^s�
���^=%H`�T�M��(���S!��Fp*Qta�~|�O_�7�&�JXy�/ �����痬 ��h��~'���+%_�+��k��_�B��>+@�!����i�b�*f)2K�YJ�R�,����(��l�ц�e�0�����Y���c������o�\Rհc�Q�`�i�2���.�8���ͯ�͂V@m�`�lJr��{Gh�~�R$05k���xC�u�nlx�5�)Μ 2��R��!L�n�z���=��5K�Y*�uη���hĀ���a�߱�;!_A��Lj�[*��ɒJ��[��d�Bb�#C_1�!��Sm9Y�����%U\R�����Nd9�l�렱�H;ݓ�uB(=u�y�\ŔP'��zW1%Բ��/f�<�v
�[�S2Dv���<G�o�����;�BF�.x,2��E?~^�F�.x�16NEc~�t�@l���v��af�-�g	Z!��!��u)*��+�d��H��h*������N�N)tJ�N�ީce���M;���Cy�@�Җ�ƾ~e�\�W��;����wn;<�G�{�z��Ŭ�
"�"}G��"$/�,������[��7OLf������/���!��q		ka�܊A��Ƞ�;�Lf�v�7k�y��:��k��n���\:�Qk��Y�V��s�&�uL8� 8���$6c��´t&�§i9�UB #�ڳ�q�<�J6r&�b��3if%����g�z��3�=;W+�����y�/d��[����f�C�^q�iz�)x�T���
�|Q�Ёp(rOk�R�T���iG7բT:&��60g`�0��U����m����=�M1K5fG@����'C��(��4�XIr�����Y@�Z<n�x&����,�A��-TԱP{�J��f�\\�K�V׋��i���Z��VG�3�EP�X|�)��9<ŧ���w����` �"�j�eut`�� ��u����c�4�F���1YH	d�B޳5"�����*�`aF\bY�����htq/ϳל�0 %ڡ�()��?Q�Et��ɂ�������(oux�O�U&%����W��4x
�|��\׮�S�L OI�)��-xc�X��z�����d9��6�s�Z�������:���10��b&Nډ�-�@��{�s�E��(�34ܼ��q��S��Uu˸5�7t/����"	�f�N��!I��[��~J�d�+��.y�J�+�t��S?Wupկc��XX�v�2Y�Le�mWPæ��z��骏�5߱��h'�7���e��{��?w���ϝ�#��ᴳ�� &��fbr�ϟ�qu���*e�3��Cn$\�)����F �F���P�������0l=KiW#^�Z�����ā4<~������"��G�T��{zy��^�)���O�4߰C�?2�I������P(Ȟ�����o��L{��M��hK����$0E���f�i���O�O���9�U-ک�a�� �=�cG��=������f���'�I��q�̀�9���w���a��h���H�)���˒�ԙ(����� �;�ٹK;l�L�2HF��6<!UDJ>͗O��ĉ6�䳇�+q����d���Pu���&elRdӱަ�ݠ�M��vP�����D�ҡh+���˺q�f�u�ڦԖC4��B��aт@��j����S��b�MP��O4O�B��EVp�3�wP޶c	�|�V��'~����V�5�N��R�Z��u4q�"�<�	�0m��d�%j$l��+w�*������k�\D��]�L�fGJ7j�x�VW�T5� "��	�z��<.�C��z���T���Tcڱ����V�v���ȁG��0�8�
-��k��@M�.�����kױ�5�ĵ�)���ȭ"�?�|۔9�wh@���]��R�ͻ����P�o�d�d�D0'P%�߿<�
������@��R��}�=�X'�_�����vU���^�s%C�J6o����F}��ˎl|�h�Bj�R�T�fk������a��h�|!��j[V��I�(�`��+b�K0����1���U+$���������(���ꖲn)uK�n)vC��wk�ʐUboe�����ϟ>w��*_�?*���G�7�����U+��M�<����J8hO�/�no�MA�T�ʹC\`MTAl�1��-Jڱ�3�*v[&���D�. �
9i��YוO�+UR�.#`�k,�[���W�H�B�/�����H�p�v�ڌ٫�(��f�_cd*E?��C7m���c���B��`�x�>�_f�����_@��C��n)�t��H~>���|��X�K%����%.)pi?�b3�F�U�0Ë[@ ��=Cz��r{������nke�Y�e�?�p,�X���8��Sc� �j=Kl_���.vM�k�?����UB�VK�5�K�P6�@���z�a`B���O�; ]��0>��R.�L!Z��_��[�R��2Y �5�9Ի��Mi]�zQ:�O��w���B~�b(>�D��(|��F�0�� T��'�h�Y)<K����R|V��հ�гT�.��L��h��'�b�Lt`��.�_�	}�e�,dWwei��%L����"4O����=2��:�$H�8�x�F(�b� 1�9�Aſ�du��NaSX��^eaV>g�k��l��*�A�piZ��)�j���܍��ѱv���m�y]�g�p*U���-���(J����9G��L�n�ϼ��>��6j��@ǜ#�)>��c7Ͽ��6/�lM�W�m`Sv!�f�Fݵȭ�;�93(���8�C뗒r�Ȣ��w�o[�p��8ګe��:�z��   i�',؝+��V�y=�Scߐd��Z[���Xa�g�*�F�N;ﶌC�o�:�@�z!�6�?(E����'4.��q���.���Q�rp��fj�2�֢�E�MO�/۳cBxM����p�M�4� *��\�ˑ)�n KC���\|��ʋ�#��_ZF�xª�P_��%��|,wMOX�K�R*�Kˤ���S�0`�E���w�(�W��l�����RD.S���8'�s�u/�{;�݄a ��)rl��,N�T����F��"���*�����8�h��x�!^�3�oڭl��jW��Vę��J�\���._,���'h��y ���&��G��⦅řDkޣ=;�2�6޻���m�'�J�����q��J��5��M]�*��)6B�aJ�nF��(N�����I:z�:2���n�SCn�6�(�d8O�)�'��B���yjX4޽��-Of�����qi�mӉM��?���O�R4��/��v=�ݛ̰\�
~n��-�w��x�j�5D��"����<�l�+"$��U�X劝)	�@ڕ-Ҟ3{sQ�늎�<�X��h�,#m�:�X���==#�3I��|ם�Ѷ�a�L���5@�k:���KY.�|�/��ޚ#��7à���!�4f��7?&�f������^��-�OBne�B��1v	i��F{�̬��Ւė(b�X#����H�,�m$g�0�c[<Ũ�[�7(Ay�A a23Û l���k����ʓ�c      �   j  x����J�@���S��{���#("�Ÿ�:]�q2顧��ot���p6u6��������-D�?����;H.�.����š�5�y6���ɥZ�Ch�|�q��b�~���n����~�U�w�,�"�އ8�}��EmE(@�`H*�h˅�8(��c�<���\�y��b}p���x|���OI�0M��;Nӡ���Eq�L΃�<�']��������rA�&�ʘ�����*KEr�p��%߂KI5p��k"��H�*I�.�Х+�-Oe֛���YE���[æ��,e�����g��q����[L�դ���,=9��*�4k���1�r7�������xU����<cU�˚��i���_S�[q      �   A   x�3��p�4���tq�FFƺf�F�
�&V&VF&�ĸ�8��|�2��W� <(�      �      x������ � �      �      x������ � �      �   �  x��Z�n۸}&�B���<�@�����8�I줂��`�yaj5UmK�d��ߟE���!�P�*��x[����L����fb+vV��m�������ߎ���vt���Nx��(s<�ԯ�l#�̚&+#Q`���&���@��A�kQZI��E.r�1˳��jI�}m;�-�'�a����@d=�"����Q8�	�B��( Ӵ��Z��.�𩇬:d#��>ß0��(�6��o����8��%�	�KVu)�-�m�����R;:<>��5�)6X�]�C�Lg���%���ԗN4�<҈����0q0[�g�Ķue�C���i1L�x��a ��2yx|2�Ӗx�AH�
[�W�8ܑ��a��6I��w�X��e/�a��N�,�pg��z�`�:_�'��A��8By�\o�:�)�}�c�Z��|��F��0��<���Z/�+��,}O���}��d?�o���e�O�б����G\�(U�P����>��Y�fd��DY4aL�Qf���N/q��R���Z�� q~���:�"��$�y�N�L��àd�Q�m�4{����jDC,��ק��1Dɳh{:d!tHn�Z����r�@�^:���S��Vj��M�	wth8V��8�I�����cXeϥ�*|t +�����^����I�V�<���u�$�jDv]�?:�e�Y���m8)�;q#
���dTs=��`�K�MfÂ�v]�� "�x>`���P4[�A��fo�QJi������co6�sr�G��u�B�L�o�L�*ό׭�G��a�3F���$��QQ�lp��x�i�VE��mV�ȿv"�qo�]FK��~��:�O���w��X��nU��Q���XWe��#���DLC�<
e*�w�*IiV�����èO�X�'�����Hm�ly�A��?�iԵ�ѐ|Mn�,��C<���D��0�PLF��vg�]����~܏qF�_F��?q?XK��G��O�VQ�B��Ǧ��9:��4��t�n������pE��a�jr����	) �֠��)�s�� h�n1t�#ϊ�ǠpC�[���ۧ�5�� n�1gMV�� "2
,֩5e=��?~��(���M
��_��(��%.�u)o��טk�_a7�(�ʚ����d�!0m�<>�/�Ka�v����k-Sd��b�>C�wuEX��J���h�&C�c��P�rQ�u��7Ț�2��1�è��2{������S���uƨ�
�e%#�*�w?�|Fe�<[NG̃�K}¨#ïB~���r�Ӳ>��B��7��V�}�Ր9����)���'=B��,��Q����nd�P;��0������74�a4d$qna���H:�&��c:����� 6BFu%��W��g71|�F��\�&�X��m�JL�8��df��hq!*LN	!;r��ݐӕ#�&U�c�u�ͨu��
)��}>[;�8`�:�"-;��2�^�;U���bSc=x*Ft&���(���<y2�$���ur�2�ȷ�2�@�Q,���b���%����a�pW��OzL�Qn���D)�XO�Q.c�[kZ��ak��nG�Q,��>N�/����(���e��d�+�&��耙+#[H���0�`�T!������t4��Q�<ǹ`�>.0�c\\�s*��(��
	�9l��N��(?��I��'�6����o/P�<<-�"T������J�a�Gd�Vº���ZDM��Ǩg��^�Lp�A��ؠP�Q\�U����:-��P�i͢���0
%^�Dy�n���a�����Q�|��ͷ�/!�]�KjpAlF=N>E"��J��(da1�7hXg���0�)6��f(�:�zY$�0�4����/����w��Fq�e]h��qu�m��};o��F}�����è��w���>��^��{�]�X�١(M%讁�a$Fe	����q�@�Q�#7��&�e�f��.ρ¨�C�Ǻ�GVC�(��0�G�}�*��q�H�QY\����0��zz���ܮw��g��dX99ֿ�'������p���h��9���Ӗ^��a4pI�[7e�F��`5������G��
���t0$~4Ĩ��񻧡�� $�e<l����`4Pg�~&����FC�����<�S��9G��K���*��r��-FC�O�����zNwb �h�:7����CTPҝu��29F����~fJ�
����0�dvk�R�H�C7+l1Bv�>u��FÐ<�~��_����Z�~���[n�4+�W���0Tb��A[q�a4��B�U��p�
�#7_���]c����t09��=5���`�y��c4r���ȋ���&:�F����{ZV��5����ѐw��F#����l-C��M�z���ɴ��Fn�Z{��J)ּ�f��]�l��6��a4
���G����+�FQgN��Ȍ�FF�m7DQ��]������ty?3��qp�4D�18�d7�9�c�� "W�M��E/�``QO��d��7&���)4��+�E��pD~y����-c��6��"W��[�����Zf�Ɗ�*�dK�aP�eN7��`������}Q��M/��`�	�Nw�w��d�1�J�:��j�ďc_�A_O�#�:�N���d.�%;��W��|�7B���(C ���xu?�_�?�m��_f��2�;��aAw���s����M:�rȶr�#�RlN8K�2��p�\z9Y���"c��,�e���;U/��;�Z�� 9����d���j��sC�P�Sa7��`�yO��WS9�=K��B�2��[��s����h�y@V�@���Y��z��
p�Խ�n��>x���\o�~
k0��ƪ�I3s���f�F�p�ɱD�D�iH=��	��o�9��ѝ��0�ɺϫ�t����*��<!�&�H��>ʖK��E��ؖ�$|=�2�&w��@SұW��z�Z���z*��Ơ�m۲{�S-^��U����q�r���r#�Q����@	1�_7�m��F�x��)4�&�*ym���m�`��ڦY#���q.����v~��n78��H�f�x:�2Ov>��"�ࣇ����%��@�U�n�m��:�/���m��ͤ�(��7���S�V�dw0P�d)�Ӑ/����Z��j�^�TUD�u���ϴ���U��Q8d�bP˶I�i�>F��/����|�      �   �   x�m��
�0E��+�$:T����RP���ɱKJ5���&E�{��S��p�d�	.֙���}�˸�5�c����;������ò'tP��o��AGxb�m@8"��*�P����Z�V	Gb��D'B���d��՞+г���'QMe���c�\s�ߧ�;}         l  x���Qn"1�����h5v�8yb)e��Q������Ĕ�LWE����c�P?�=to�3=��9]�CZ�>�O��yW��#]�C���I�;��* E�x@�6���������2�7���((̛J�X����!��@<��dXWa���6Z���B5�KN��Be.2aG=j@٠Tn6�+�Ǹo�Ȫ�1xMZ��PoP���t��-�Y�@=��,C`��%��P�+�5]��czOR�>j
�^�p����@ʿC[G.�����}�5k2R�g�7�-o4`���q��4䣠 o�ܿ�ѹ	JA�;���!Co��H��]���e:��*�����9k/�1��P��;�[z��C����&�N�g�yv0�%���
h�}��M��7�(�������9����;i�AAmP�asN��g,����)١������1�o�ͣ���˸��w���F�,��l��(rG�<5�fQP[��x�L�t�Z�\�N����FE䵸�h��9�N���'������܃��u=c�[5S6��گ�ۃQ�.XU�d�'4_���Ԝ^�)�m��|y4���4�5��n@������|j��Hُ6��xH��J�	�2��.��9i3�{��Y=]�����})���zѯ:K�i	V���b�$���+_�Э�#��E3+qu�7��	�K�]��Qx��Gs)��߮���&���Od��`�&@3'�:���Y+��iF5�H��������}����PhK�<��H\YI��T�o�hF$�8�c�m�-��(
�af�*��P�t�?��U��4yoѬ�\B�:@jI�$͂$�Z�-�.���L�1)��y���}}zz����      �   �  x��X˒�8]s��0=eI���puj��t�j:�l��cc��cR��s�'�**1'�ܫs�2[�ⴌkY*Y;�2.�a���<n^?r��7x�ü���|i��/�6dB�0�Oyu�Ӻ(�7a�M,�[<�\V�6��j�R���p���L�=�S\�Y�|yWw�|n�(��vE��Q�
2�i�%-G�jy��̝����׏q	w<�+'_��j�V���U��MK�Q훓e`�(|.�&G�*�z�)��(3Y�K�L�k��f��;�t�`���c/�Q��j���I�g'��٪�nl�V&�mf�^���9ʃ�"i��g�8xQp?U�$l���7�y�v�h����.���<{T�o���� �0b�B����B��2+��ٖ�*�.5F�O��Tp�fM���C$<��nbe�R��պ)+ut9A�Z����!�C���D"����o��VpB�k�M:2�5�
��:�������J`EUg���'屑%ť6�[�Wj��T��yk���A#�'���s����_+�N8�]u��9�q<�)�Є�S+xӅ��T�"�N�T`G]am�mr�޷'mZc�Ϫ���(LdM�2Մ�_k����d��E�}��PZ�n��3[��E��q���s7�?��R7���m����.�g�7�V6/rAc�v8�l����b�D��kӠj�Ԗ�=)�Ԁ_�W]?��wՍ�Zke�Aܠ���A�����(ug��ah�#��r�0zk�t��E�/��Hbu��4d�H�ʯXn���3���ť�*kJ+J��=�;�|E�����h�����N�/^t� S��n��g�o�T����G�&ͱ+����c�����byw�t>�7y]�Ճ�9n��j��3���l�$.�u���C���Z1-�;�#�
�����:��Jp���k��Ǆ�7����^���S;}G�e7+p������)ifyl�o�N��[��U��f�&�"��ο-��9��ܚ�&��q��:��w���x����~�f
+�I��dz�m/M��nl�o7�B�]��j�����e\b��\��1�pi�`;�q�xU�u��(~��[�Y��	#/�3�e��
U�S��6���`*4�e����&���T:7�T�m$pX�=b��FXf�ȼ��U!F	8_�_����&v�>�m�X��	#/��~�d�	�]Ns?�l?<o?�<n��Y��t����N1��h�k/$F�#�&�|]��\���66�s�|a�-�zE�Iw��6���8����P�Z�� ��k�L�q,��P7q�@w8>��#��?6i���y�Vv��}� |L���g0 ����4IK�YVW���Ħ�s������c�����<�ȗZT��=F���t���M~0�s��U�z��_!�lz�7�W��/n!]��E��~KǞ"8��3���r2�X_^u�/���1ã���I�a��z0�(`gwy�*�-��eRy�]RO�a	��o��!<��      �   �  x����N�0��>O�e���f&Bjj)�`�c��$��IT��{R�#P���W�����1�E�B;r�;�n;��P69�;��b��=<��������R��r����\��4i@�ݖNݙ�*��9�J�䴪M8��9�;��d��ʖ�E��D�e9_�4��L7����?_�y*B,�>&ʁҐ+t����c��3i�`M�Z�MgF�F�R׃r�n,p]L@F�ݵ3�W��<���-la
���5�gڴ��M51U�b�K�BPF&䧡P�#
y2p_���T�s?c,h��2�k@9�ڕc�:�Z�6���l˷�yҀJ�Q��w���k�񵗈��UG,���=��8| x����n����!��\j�nU2��d���tV^�����e:l�Qtv���~��9�@��l\�Ŕ���|�ҥi��p7�m�)>qG�4yw�v�G�ö�>*�y���<x-����j��d��n׺>�xN-�d����T���yk�(6KU�[��x
Su?����A�ɤW��1[�lM��R�4`~!赳���y:�^[�Ѝ�=�v�ϼx8��5��ٛ桴��e �Z�"����C����i{�'x��-K(v��O������������&Ƞ�f�U`��n��^�b�r��� C��� ��z�            x�Խk��u%��ί`�|�#���`�=�E�m�HjK݊����["UbW�&Y���;7v&Xǅ�u�;~\Y˥�8xl�V�����������2�޼}�����_�����[B������ի6���\�gk|i�K�B�诂z��o~��g_�����7��$C��&i}��O�?�|��ջ?~��w����W|�㋏�?�~|��ϯ_�����Oo�y��+��?����~��V_��7����������_�?���_��������D|�i��ghK���M���	r���m��������w߽����՛W/޿{��wo?�~���O�x�����wz��7��~C��o���7��o���߾����?����y��z�y(6�d{�����Y��ռTb��`�M$�l��<E�����_}��#�ɿ/���NA��e
����������?�귿|�?`� �>_�3�'*��b��Rz�-��o�E�a�&�uX����'3즇�~z\�Uƻ}z��M�}�������UT���.6Ι��T*ӄX�;o��;2�<�i�C�	��w>�t�g�[��͸N�rܱ��Յ�r�Q��]�4��L�-s�z�n���i�!�cN"�z�V9]�k��E�i�]����X�;��[vg��;��j�z޴J"r�VJL���;O�nOC�z�i-�Y��+�8����a�w,��͜5�H�/M։�z��e�!U��YC�]��J�K�B���wy|(��|�]�g_*�N���_o�+�y|f]2���o�MP��n��D�f�.A�'�]���.�a��ˇ��X�����վ��������o'���z��*�ϛم��Z��(�k�X���n��-=k���X?e+����;�ٝ���G� qM�b�Y�/Y~c���H2���}c�&�\��g�Z�=�g���R�N�)b����a�y��c35s����Wo����݇v}��}�z�����_�����������w�3����W�=���۟>��������������Ɇ�՟?����1��iN����U��+қeOb�t"W��1��;��I�!�.#��1��c(:�^���I�00��?�.F
7��ya���7o�<����ͻ�,��sa�e,,}ai��|E�K���d�LR_���bO*\���Amw�b3d�s�m�q�.T�z���˼�t'o��g� o˼yU�E��&5�[��ix��n��������{���&�}MF�O���[!���"lATt+_�_��^K�'�f��ѩ.ɲtNA�rE�6��ojuMV����X{u���^�uv0TX'k���}�Y�QY�N'L���������Z���V�!�Z��"ܯ6�E�o�r��+ݎ�'	b�VK�Vra|��7�������4M~�����?2�b��Ö�:���������!��C�~��~��Cd�i��"����f�
���kO�k�t��Irz��֣+�:��^���I�q�Y��#vx�x����Z1�-ܽ��x'z9�Kg ��N���������ٖYa�[���n�P�����a7�4p�)Ň誗|g]���31�p&&qɕ�m��""-5O
Ā+1�K��e�Υ+���a���
��X�y�5�'��${��W�=��Þ��x�x�|���*iG|À��YY(*+k�ؿ�!3�C]�)���}>v�q���9��*8��8����p�_yԷG��ߞ�^���S'E���5q_.v�I��Y�6n=U�?�-�·���Ғrǁ�r����?����0������s�5���
I[�<�ƻY{���upE����N�*�6����ӳ?<~�駉��ɤVa�>��q��Y�?��XO;�)���5��GT�(sۯt���ͺ�6�m�x[��g��H=�pm�+cX���I�00� cؔ)��)�!��e̫�ͫk�L�!O�_żj�?�b\Vy��7�SRJ��a(޷MS�+�o�q�am�0��R��g��Ϲ��C�[_2c��+^CVT�c|��v53�5�8�$\���0OH��smGް)��[��@8�t�eE�5����قu�\��
fї���3�1��y�B���Q*�^���*3{�P����Cj��k�')��]fۆ�)����Gi���+8�<;�����5�7;D�x��\�$(۔��U0�Ms�Ms�n{ը�����L[戍�l�%�3B��V�9�7\�#tb���5'�5���o0�,���>i��14�X�K�-�
��P�Kfe6�/�&Ɨ��?�+��WM%��p�yIUJ��
NLq�J��L�k<i���^���;���iNLu26WpbZ�,������tb����4ɹ��}9�k�b�W�c�{���M���P���1ˣ�LL�t/u�{À#SR9%�*�JAU/׵)��c��iő�WQXf�r�|��zg�$������Y�cK9��
1��[űX��|�Lt,淿s����E���.f�cѭ�E�x<R�cX&�oӳ�ǀ�K?���s��}��2�߅1��Z�C���2�^'�82�9B9I�a��e�s�Dj��M(�[m�G���'�ߟU*�9֖|Z֑t�/a���+�S���H�c��Uݝ�9T`��<�/���C�od�bC�D���̼Y{c�����>7�*v~�e�(h*Rы}-6�x͆����Mvǀ]�����k���&�aN�m
Xԋ�(̴nX����ï��_�Q�<x�D��e�"+^��������T�{�;�_�6/��~M�A�ڃ���Vq�x��T���H"�%���

��2+�Jl��W�%Y��J?}��Dָ�D�k"Ā����"�ʒf�z�!|VJ	lk�뙵�n�l00E���	s�$�kB�wx�B�@q�=�I�(u*GQa��Q�S_���J��Qa�Q�$kO��=��qE�u��#/�#_|G]G��X0\G⧒` u�rdV�l���P���ǥ�^�K#�����}7A	zѳ�jq�hW�@'����В�5�4bg��	�����3��� ό�ᙡ��XW�5TF�r<���F扟n]ü�z״6pƀ�^�2]PV�+!�u����kXc�d}Mc��͹�[��a��*ᅳ��aP'������a��L��=����U� ��%g��`:wI�a���Cu}�N�˵�m���1J!���On�Ҹ�����Odx&TIy*�7!?��U�b�O�
ܓxU�!��^�L�����0�U	�Ua[�]�L��m�pv���$l���=צ&0�A�a,�47VI�o�X�'Z��ث��3�6K|� �X�G�����㩤Z��l-W��x�<��'��o0���$iK?��O�d�,�u�?�M^j8��I[�$m�lЙ[������6�d��1��r�F�U7���)-qbqZk�t�뙧<�渆Ժĉ+��S��
��C��{kA����Ύ�Յ�汦���g���>�xq��v�WV뉓-�*�/^=��M���H[a�w޿������N���R��7�(s\�SySx������9L�*-zZr��T�1�<�C�xG������r�P?��;E��*dΡ���Va��-S΋ܩ`�]�r���5Qa����o[�w	%���{2@�,�6/�JE���_^׼������~��y���������~�?~���������������_�Ӌy��_���>��O�������~�{���Ge,�J6���UG��D%X!H�(�C��Ig�)�c�KDX`�z~x\uS�.C)��3Y�/�b�J�z%W,�TX?�T*<��u@��㴋L0`��%�3��S�h���b9�uq������}�M1\���k��,���AA[#$�'��s������VyWisÀW�����y��T{.��ũ��Xp1Lt>���$p>{q>;q>���xq4�)q��9I�?�^���$�p��0��Zr�J���.r�z��^�ZW�51E͟�l+�Ժ=�g��X��H*����\ٕ��l�g���⊬I    2�J:��v�r��sێ��QԘ���d釣�`�g����|���}8�E��Ā�r�j��GY��YN��Jm�J2���^���nagOZB�窗Nw�ɡ����2��V��le�K�Ҩ�O��.J]*J�5~fEI�,���T�d�p��'Z�ɓ��0v������L��ֳ�P;������-Y�)&�5r�8��ғ�ϲ���:��|y���jMvQ3�w�>΄1���j�y&��T�f8���P�}_���w*����vJ�0Ω�Tۺ���6E~��WR�3s��{�W`�;���D���<�v%c��������p$�Ñ���>��f��c�N�QM�=�[����)��R|y%;�<Ϥ:U�0�<�O�D -jک��OM�`�ħ�!��m�9%S��:�d�/�-6TeWR���i�K{�h�wTp�!��OJӁ�i�������$n٩.z���o�^*<.O������Ml��ϕ�Gk�����~�B�g(.vR^S�Vv�Nv�iV��.w�}���&t���c_��Rjd�뺢Y�g����\>����E�#O��֧� ����q�F���*v4��=�p4q4yq4��!�Z�F-'��J�N\�]�Cf9fM�����ZI~&+(~�i"�|u�[ƀH������5b�w �p��&ٙZ�3g9
��B��Q0�
���1.�։}�]�,x�$;SY��L�\����` �%�[*����Ŭ��d����3��#@��i�e5��4��;<K�$�{�%��O�f�e�'�Mn�E0wzT���6�:�B�ŜT�dw�[��$8������X�>��7�Vw��V�9�>7�Kc����:��${�t;ߌ���w��I;��VT���W���>��c��p�@���4C���5(%��5cJ\z���&�PMh�����������C[���Z��0paQl��ʇ�i����`��V�,�$+���
g���2k�Z�ʭ�iS�U0��EG,E��uQ��,�D֦���ks�}b`�,������毆p��,'�ו�w�b��w���'Q-�,uRi*̡Ǽ���*oT��T��V�rS�;a�����2Xm:��UX�J|�|O����PX{�˫�~�s�
^b%~��	A���L�#!�'=��Z��D�o޿~�엏���'2�@y��綰� 1��[KF�&�.-;�h"�o,y;\{kq"z6�\v"vUV&��89� ���D�Z�1�Z�$�B��jA0��k�1`+{�+\��W����i*�}��n}`^�Ͽ���wo_�}�3~���'Ш|��&W�8�����>��˅Y�g��ơ��}��y�lZ�N�*���9��'71,?��7�*����ƯT�a��сL6w#�-����WMUc.9S[bR0�`��t%CWc�����R��f8��ŕ�i?����ƀ�N������"��w|3�t�#�ޥ>,ӎK��O�a�x?+���>��ߕ��E��c�_p�:�9��Kt-q�@��*��"�m�O�kJ�9��0������xݟ��?W"�RE��%1عUTM_ϓ1����,�xb�I�%����l�|����_?���)��ۉ�se�
�^_�枀c3?<M�lg؏mq�������Ҙ�S�N�lծ0@;JE��ty=U�@aX�T�\�;	ϵ~*�Wq*W[�L'!�ޛ�{*no�#]��ގ;	�*�;���a��V����TA���Q6�B��Q�o�J�yUt�fz-�ɴ�y2�2w�`�
o-�H���T;�vz�(:���q͵ӷ�v����{E��g��_�q�Νk�Z�P�T��.̹U=���;��%��̬��g��\�2f�w�H�z}W�C$�C�b��8�b��32�B����C���K=̯:���N�yYgQ��n��D��.I�?�j�k�!i^���'򚸣�H�q�nǉh*�%皸�'�6p��cߘ��4NG@��z��~]�v�y��}E��%{u�$������~z��Ӌ����.o��?��ӛ���}C��o���7t$~�7���W�_��g|�������;���T\��t��9*lY%[���e�Z@ԟ¼*�_e6i����bZK[E}���M�%Kq3�i%�����U�	���	A,�ٶܱ�v�q�Q��/w��3Fۅ�6c��)[U�w)�<���K
?M���Rs1:5�,��/��CI����<=�;W��I�qw|^���2�7S7N�l��pL��Y{�<c�����Y�0�����2�F����:�!�b�yǀ�P�㓃d�2���jb�v�N���>�L޾Ϳ�=%��gf^B��})Sƀ�S��DnDE�U��N�H'���^�IeT��x.^�c�i�ɮ��#[����˰O��8�(�ì?7�0����� ������m8�ے9�U������1�u�%'�|�3+��t�Sa�x����qj��P�h`��]�f�����b8ݔ�K��y�h�5�-�Ņ,�7�G�J2�]��\�^�͹Ǳ+�6���`��j�Y3XG�^98i�{��@�M�X�.��08�-��Uy�X[�)JJ�S(�h蹘`A'Gu2�g3�]���	� �(��.�3]��_+������g\��$��A\���T?�u,\�J\�j��S��_��N�m��s��QW��W
c�թ��Wp+��T�g�Ш0���R=�XJ��=�Vz��K�ϸ4+��K�ס.�C��9<�zwxEP�AΜj�Wp�����lw�I}-O��U�cIǒg��X�rm�v�%�����FV�m+Q0�Q��]��P*�%Y���Ta`��T���^�եR2�˒
�٣^��QV?�ϡT�؍��_�uN���^�T�(q`�r�J�2`o�y�r�#��.�a8s�ˊ"KY�⚏�V"��J"Kb\��qœ���:Y���(\���W+�����zYZd�"�ī���2������EV�6.��ۮC�zYVd�"k����z���^�Y���k[��hSa�(/��l�pUV���e�Vֲ�õ�7׶Ee�e�Vֲ����Lv�n5�C*}b���p���>`�wǀ�RdIݥBJ����c@s)����]�7��:1�#{��[�΋���H[a ���J"���F*DC=T};H��p�w}sD��z��ժȂ{Q���ڱ��>T��fu����B��R��q�k�)P{MAeݔ2oEHWO��\5��@(Q�Ϡ�v�Spa{)3Bh�,����8�$J�V�g�(+�r����4c�@�K��DV�X-ϜZ�T�R)��l��څ��c+DhY{��������������F�_�Uha%\E6��]^Q>�J�z��[�ģ�@(�C��)Z��|�`N���\^4��������Z�;�F���$'��,��	WdsJ���^�Y\�1\}����0d���E��l��4yc���^�Y\��S����L�{�Ř�(��O}~)�k�<�iO(�zYNd��+﯋����k�zY^d�2��ڍb�1��
"+Y鲬���e^�	�,�ָv�~/\�"�'�.�^����:YQ΍R2Ē+�&*c�,97X��^&�W���գ��;���DQ���J0������^�w��4��a�e��Y�b��է�]�;��^�Y����˲\��ey�e������SZs����Ȳeյu�����e��E�Ң��)V�Z/c੬%��<���Y��N�7��w3��ض�ޫ��<�~}h�>�-�b���z7SÎV�e���DV,�
�{������8G�ᨦ�����������X/KΩ�����(C�[��KrN�������=��e�9U�論��]ۚ��e�9e��^���]��u0�[� ��B[ͧ�Z��`=�(��S��������6O����nA�ׅ7�h���,���iy3x+��ox�88E��pt�VP��D�#��p��6�1us�`~�"[�b�U�����nlH��Nx�ځ�H�V���[�x7f�    n|��V�:��)]禑6��Q0@ZnȒ�5���ִ��;(ܖ�s��%�wV1qz�B��X�w�/$�vou*m]a��ޥ����(��ΊR&�j��Dޮ]ߌ�Fx����[ߺ��	x[��Q&.�uҽ�C�#�E\��/��,���"m4Qv��ϛ�!x���}�k�K�>�����f9���Eq�W,>n�_M�e���^��6I�����&eE�2U�������ZnRֵ2ҵVH� o�IK�Z���Ҽ}oGa����a��� 4��5ZPሻѱ����y�n}�����Q8�xf�v�M`�CE&�pe�aڋ�*v�b� 2�#�(󞨙Ǘ������v�{�9�y<����
��?(A
I�/�)T	<BLP�	ڴE��Z�_~��' n[�(�u�E7sX��RRǄ�z������E>�p���MS�H1lw� o%�Sᝦ=K�x�PR�eڒ4]�Y� m�JV�J��x�v+�P��i.-n��nԩ�D�ޤ�]W�iQü���b������Vd�����o��ӧ��?~����OytT��g�@���y!��!�2o���g�``A����+�<�!�6�M00�(C0ef��q�!�&Ic��������{�������*Cpeq�ؿ���``J�pMۼ'}nr�hχ#w=����=GZu��*ۭ(:&�fҩ���!4�=��������; � sI�T�i���Z{�``r1���|�E�;��
	�++���9=�!�n;'�����f��]XH6k�?��~�,��4�Y00'c��а���c�P00�T�P��b��Ի��,��UV��g(��MU<���ya���7o�<����ͻ�,�k=�����t�҆/�2�*�z���2�T�2��_�*,c�2�t�����G���/U�q4���N�7�*Z✊\G��Qۂ��)��Ua����V��"X�{m����������E�QQ�J���	3������*'QS��Y}̾���W[d�"�R��7U��	dE�Ur�̵�X���m����3��6G���ǚ�D|��ܮ��^rLAT�0U�ͼ ��!���
�<桲¥ځ�ū����0PB�5+-�Z�4����Qa�B��P=�C�8�⽉�)˦� ��P=�Bm#��M~�)��@�_���Eә�
�2
�f�wԽ1�`ǉ�_��}��S�l��@BXʩ(e�0t�����K�5�I���`��_l?f�v�u�@s�O��py.mX�u�c�T-h�N��s[)i�+l֏�
�����i��-*��ʘ>��>�h'��
紗��+��zڤU�Cu��50J��!�0@���C��K���� o-�]:�O�] T�md�K�����\a�� o+��G���;���6�h(�'��&s:����^a��ڼ�'��ʃ�`�5��1�Eם$���(�L�%&�+���Sa�w��(�y�[�UXϛꍫ��dLc����Ɇ�Jx��|뺸E��ZXse����\k�� ���lMK�9�I�b<�U�m�v�N;��h�B�\�3o��o60h{��%5b�?���Q��#M��s`��]i�}�*Ц��ؕa۾(����jnsg��(�i���.�D}ʨS9�
����v�_�b*�N�6�Y�t�>�������c]a�6�(��/㉊���1��
s�aR�7ai�����3�g>��d��2x{��%�&u5d��\�
���歩�D�	���6gcju����uYg���~L��I��������Sf}���fnz�=c���J�O�~��^a���\*��l�$n�9��&U��Gm�`�L���S	�
̭0g�6/������usa2���,}�L�ǉ�M���⪂�<��P��[�ě(����`�9ǰi�+��܁G���褘2}�ȫ�NT�ݹ(b���y��4��'�S�i�XO�J���F��Mܠ��+�r�6u���H�p�G���Q@i�;��F�J.c���9/���>������V��)�@P�U�
�룼c�3�L�{1�{!^:aͼ��=Փ�0@<�'�u���3���x����	�i}�|et֔R��E�ˌ�Q$oUo���(��Y��Q�S��
�GA5���6U~5��gXy����D�Co�P2�"�h�Vk~:���wa�e�10-#���������B�����W00�������i���=�5̭0睼.�B�8���s� �P���S'~y殽u̣0g�LK�wQ.�����O�iSdu�XG�o7��g��n�X�U�]^����D9mk������W�ēО� W�m�vI-�H[�� m:��( 5qm���Kh;���r�[�E�p��2c�(�����+�B[=��F����� ���u��Ц��KS�i��#�1�ѷ~KX;����'m��;��0�\楗�[��^�k�S9����WU#��肣�w�X�0@�
��$����t8�YQ%BN�+�
�H��jw*�_a������?o��#)}<
�p�_GJ_�E`<~�a��ؼÿHl����Ax�R�3y�6.C0�;
ov���ȹ�[ި���G�:::�{xe�xSb�j"uw��M�M��2N[�ıU�w�V�wq������C~� s-��gq��9�a� m#��tڦ9OvЦ+�HN�<34�����;a>[`���bo)�A��(Tuk�	�}c�ݱ��Y�y)Np������Ua.A����D24��ĵg��}�.0?��`n�9���O��+(�ꌞ��XUNPuO�	�S��1@�	u��K��꺛�#`|&��%1tꁞN��*B\�'���n�c�x��I��+�G�o ���Yn��r��߱�����sY�V]a�V�u)l2�uhw���zj	f��ӛ1��k=��k�B� kb��ѪY!�t\��a�v[�Y�,�h�U��M�1�*.w��=��~��Ä1@=�)/f7q��6�r� �(S�j�_�&ra�M���
���Ve��۞��V,bݏ�-j�vZ~Qm+T���dޟ�VH$[=�1@\�'�f���b��]XqΟ3*�<cL�h1��Wg8B��Lۊns�v8�\�"x����^+!yV50��b����M�j�}mf�T����@�3����,,w��.�����7��/ԟ�b��G;�!�N`�T?�1@<
�p���Î�Y�HG��Oz��B69�ڙTX�ݯ_�v����$IDuo&f�W�|y�kk�Z'��(;�]5��Z��>���|�Y��� o�<?���SԳ�ƈ(8!��g�#��!,vV��M<���t��7]o�.`�E�ް�y/��ʬO��c���1�<Ȭ��1롷�2�������t�P�������ʊ��	���e��7������[Oa�;�	c��������N�]R|вb�jB���~��,�̍0��|V,3�t���R9-U��K��ѵBY�����wz
�m��ݍl���;V����['�%�$SU����[����Qx�����~j;��޹�s盔��i�Uhs�܉"z�1c��*�Kqę��wc�6];u��i���h�s�Ҏ�V��|Z�f"i�f;��B|�ɛ��n$�0H� q/�Y����5�nv��Z�gm�E��Y|�wN���ʹP(
<'n�`����W�����[�v:�i-̟�kSm�b����_=j�k�1�\�����e�/`N罳	Gy���^�!��c��g�d���T����0��pR�6�D=�/I�G��?���䎩�?I��p�U�\�}OO�(��C*�������Uv����~�q��74C'z���]j�=D��_[U�1�;	oN&􋙗��l�Ђ�I��Z��|B71�fcٽ�̕0�6g�s�{�\<\.a�¼l�4�z�\���BJA��6�d�m*�`���O��@}P}���*<���dwhS��<�j]���)��c�z(k�)h+�ͳ��eʅ�]�%�n4{1@1�I��	(Z�c��lX�[�2�o�.S    U�.R0�\	s-��ϳ�g����kY-O@��<��������.E&��;����Vx�Y>�v?ݸ�aP�о_��ڦۙ	��ȫNh�-��m�ݚ	BE��[=�NY�H��gЎ2ۓ�{l;�1@;�l�
�/f�����S�g��¼����U�I0�+�x��y�6��=P�������u�Jf�6�P�M1�J�жe���:���^���Nf�~1 �mZՄ1@�Rf�j�*H�*o{�w�������Q�c�����5u�w�N�{�S#��a��*3��\�m)��)I��z�7�akR?��Z�o;ӹ��+]m��Ma��f�ؙ"���7)��5C0x;�=of��FY���1��LC-9߃`�wޥd�]J�ob��c!^"'�2�7�l����I��
�5:$��}����/�M}�b��ԛ�� �|s��p�D־}�X�[ӧ'r�tn*� o#����4s�tn*� u+%�^(����ve���p�Ks#���T������n�z� � ӭ�'8w���'໇*3���9߱��(`�d��Nh?-[����0�\Ԑ�O�p=�5`�*�������O�A���cڵ����z��2qoڶ	Ў�0/g��K��8q���=�b�G��t���Q�m#�ĝ���r�V��]7�fE*3�c�C�2���4�����:�P��$1N��02�J��p�ZռLDӶqL��I���:�f���u�7�
Tj�W��򤻙�ԁ#��#�
�����)�m��`�w�D���Lv�������N��Qsŧ�H�7훂1@��AZ�L���=�����;�i.�ζbo�V����Z.�Se�I�u�0��xM� ڮ?�`�:ݟ�����7h1x���%�������;�i�*�m��`���U2ۍ�qt���1�[n��֟9�m���M��r�ܧ��8q�ĭ����8� �w�����>�w��p�S!w�9��O%�!�v��� �a���b�i�R׭�����|��F��x�$!��b�ve��D��'Nu����̋\O;�7�0�QdTJ��ͼ����žT�-�$`���\��\�K��9c��)���9-B#�8�ĭWe�O��ڗ���/ ��n�3�����X�tV���bf2��A��A���9s�w8�c�]v�4ӄ���fz2�Br�^;���a&Br�Z;�v���/6��/���,���@��W��峌Z(�����:^�*W��^,G)����}��f���^xsu����ׅ�M
��¼xl&�U��|[�[0@<	���wjM:μ���c����q�e^��FӴ�� u�<=���piè���m1�8�n�(h1�T���/n����6I0@�
�moZ?S��$u7��;!>��q��a�a%�HuO��$�։����� � ̟���ڔ	x�»T��{ov��ē���C����z�T���72�y�>7�������HUO���f�b'څ�k��
�ka�=m��:�Tu���;v��'�T7�N4�f��z�qB�nuǜ':�Gj{�~ܛ��D��r�����8|M�ᐪ�A7U�t%�..T!@-!]�'��,O��q��Q�|�j��9*><�pu{����V^E��uI�/dZb`][Y�R�i�u/������ہ
��#qљŮɔS����N�����5-���E��* �8G{MuH��v�'��`�2���Ʋ��0Oe�� x�N,1�,;�0A�G�9�����9囷�{� sU��%�'j�v�톌�Z�v����6�d��0���xT���^Q�`�2]#U�q��M���Hv��%��h�r]��Lfޚ����i+��L�
�V7h�}�Ff���^��� �Xh��D��򊺣P�8
���`���m�E�a=m*����L��B�E5Z��c�(�43�Ě�(�`�����:9�n�b��m
�b��I���� �|W�# ��?_l��՚̝0O��3��閊��ݑ���>�g(��#3h�2�e���.��Ǚ�G��,�ɱG4��KoYʔ��l��I�
WO�9�((+(T&�q0yo��oצ���;��y)�D�[)Q�
帣j�[���0 �*`��*帣���X��n=�[c
c���\�j�K��q�]L c���|�L�$SJN�T>�T�}"�)ζ9��Ax����|Ӵ�8c�w,��~�8��g�N�{vV�ƱS�yS��DbS2�~};���V�}͛h!����Zx�ޗ�f��M�=}_:�2����7�y���֮� o'������}i��ﮈ�`�w�O`��{^0�;�O`_��F�`�w޳�ˍc�N^'T����~񋩼?�`�w�/�at�f���
x�	�`��9��1jb��f��P7�z�aff�9rѫn�$��RaSe�;��i��f^(�NV�#�#�e��ӑ�rms� s/��ʸ�K�K��E����m�4c�A�f�UǱG�G�B�7���� ��x�3��s&;�w��\�uNr�t@;t��bUA&`N�h�JH���^`0��Q?���<l�S��^	�*�#�Э����&b0ϗ�>CS=�ns��j.c���9����8(��n�T.G��>Zf���&D*�����Tg�Ʋ��(`e�l�Ջ���ڮD�`�y�9��e�`�X�Z�b�>�S��D�-�-T1GOѯ'���,Xka��+_ϥ���{cG*���'�Tڱ��жB[�^#�n����NX�'0��!�!Hr�{��1!`�u���uO��TG��D֮+.!`�����vG6c=k*��ݓ؎�bg5dЦ7�r`�^�N�1o��Z�ss�	�n��l7%c�Q���������4�Wf�w������4M� � �K:Ѽ�i�����Ϫ��Z⼶��џV�������9�(��&�ltw(c��z�O�Q����['R�]�p�Tڝi�a�DZ�~�Z�ŧ
h�B{������C������B{v`�Fс����&�y"{ҷ����Ax?�M�۲��ޱ�.�ubI�����[;�$��c��)ML��,Ct(XϜ�hC���e��'c0'ݼ
;��:��N7�[kV̧�v݉�`zA�b;�9�ƚj�9b��VX�����`턵�ϺY!��tW�3����"gUG|� � �ӓ �(U���=�N���v����]�
��#s&_��	�h69��ԩ��9rg6�sg�v��@SV�*�����;��k�����NO�w�] �m
oޞ�y�nw:X�3Q�s�2���U��v�[?��:�7։���9$�R�s�gk�b&��P�:�ʯqR/�N�t+P�L��օ��D0�xj�Qa�z*�Y�ƚI!BSw;eSޟ��̤�5z�Z�'�J���!V����9!{I���*ׅx*�'�ZLR��
���L�b�u�
N����}�zE1�*����S��Eg$_�I�¨.
�{��Z.K���b�f	沰��.zI�z�j���]�o0T��HeWE]����ǋ����,c`M۲�Uٌ��m�~kSx���e3N���$�)��� �^F��8s��)�� �P�s��U9RH�V�b�L|�_H���10D<�|��y�Lҟ��+ׅ��0k!i���	�&>;�ZH�n�x|��|��?��O�Ղ�U��C�]�E�i[s�h5� �ȼK����1�	�F�����p$����ݭo� o]x�u�����c�����bR�yk1����d��'����
oN6�UK8:�N���y����K?+-y#ٿ-ӭ�eʻ5��.��3���y�#/��jRf/sԧ��
�y�&�JϨ��=�T��m�lm&5�b��1n��m�w)m=�d���{� k[XO��&A�v��԰<��O��*�·���n�u�p*Pa�w(�S�=sO�ShG�ޑy��O*%�ccA�1sn$�\=��$�;v�������$�Ƕ�}�6X�C��b*j��a:QK.}��Za��.��Q��I��    �����c��a�Ŗ�;�O$[�玁s*fE%u�y�c���;���˛�[r��H5IF����;�0 �ʊ�Ĳ\Z�Vd��uԟj0���b�<�E��Y�l�
{�qw���p'�y�F���f&��G���Zv-�����|�o�Z��Q����`��hsTkeRey�#��2Z���۬E���J]�晣7ޒt⍦<�rڳfs�<�uw�j�Ny������L�&@����w�k�H�S h��g��.�,��݌ㆿ)D&^���0����2Oz�����^� �bH�E�/��%��Ƶ�>	�FDQV���U0��:��;�c��.�y���k"�s-�
�3/�0����lL5��W��;ͩ�$�q8�t�~�/������pd�7��m�kIi��O�x�K\{�⑉��vZ��>����
�mo>�J��������z,3I���ž'�	z�њմ��1@]�츳k�t�e��07�\
g�n��n۪T��B|ۗ�G��3���.��
�ٻӵ�E;�Lw���$�+���4��|����=���� xG�g�N���$�P��S!.��Z��Ƽ X�"�|�S����P�X:�=����#�TM4Em��T��-L����Ew8����ۂIzpzy|z��૪�f�e�d��5�"n�x	�tS~���C�Ã�B�%�ܞ��1��9gU�EOT��C�C3���KZ�̻3��uU���B[��2����ϕt�\Q�0��|浟� �C���;��geT����z*�Xa��)��V`��ݚ�!ޖyϭ ��:R\����a��C���Y�(�n7��[���Q������!��l�VS���S��Mkֿ�4>{�4�3�o_xO-� �Ǆ`�w�V��˼���Tw���xE����i^^�4�Ѣa�hE����˼w7d�V��~VE������y���py&E���	f�����Z�!�Ya9\�kV��Y)��I��1D�ꉍ��Z?��o�d�!料��Ĕf�:=�߸@���J*V�$�H�1D����f��XZ�X,^,�]�����/;�����)ܡ=��D�!扙�;t�2'�o�X��Z�?�+T����p�ff�
�p���_�
wF����=LrzZ�����oSxsKE;s���*�m�7oN'�J7��;�x�»$M{�o�3E�:S|��W�bX�Ѵ�
c�wx��-N/i�!�f��"qޘ�j�0I���C��1j��^!g��?�f��'�^Ou0+�6�vQ��Ҷ�5�1D�2��]ٙڕVMb��!�Ys�j��eZ)Fb�S;���W��WҐY:�{T���)�����g�x����z� bS�q�� hX�����?e�S�����@��b ��Bǩ�P���I�֎)̥&�L&��t�ж]�*�H�nz3���G����I��ښ�A�,�<��:*o oh�w�-=g����<��v����ũ�J��=C���_]�T�v�H�^5�U��T�7Փ�QR�"�+xB7�Y�j$���2�'t�d� 滭�xĥ�fua���n���Ǉ�wQ�\��t�Ç1D==T�[R>���f���P�OʪQw�U��iO�P���0T�����8;�Gڅ���L�>߹��#��9I�`�U�3/Ex���`�o�Pu\ы�hC0���0��������	w�^��x����Yf�!����!���<Ld1,Z����� q���`�y*�9J���vŴw$m0@��SV������C�U��m��g�5�Ͻ�Ql0�\��L˼���x�C��?Y5D���T��Sc��-�yk�(�T�t
b��c�R]�~y+�A���O�!�We�L;W6��]�!�׼T�L��dި0D<�6G��32v��ډ����<�Ҙ��T��� s*�W��˄�����N�t�D���aFo՝�^�:1���5[3��V��x�!db�-H���I^5@v]����H�6\m�����LF�����'�VJ���z��¼�KM�A�g�g�u���dZ����1zbI/C10�Zf���{�3O�t*쨬������D]j.�'�NO�
��i��hG]��:!��Eܺ��*R��k��� o�N\L�t��B�����ƫ���@]�1/�tص��.L��Ɖ0��gi�%������`H��m;�\��T�����$WaH�����x���/K�@,��Uii�L��c�
bHZY�J�5&}Q�Q����ӛ�e]Z��96aO*�=�.�0p���*��f�+���UXϜ���0R�*�ͮs'Q�!K�z�Z͓�eZ�Y���6Ȇ�~��9��gM8�k;��0��8�OJ��]�<��tj	Tao��<9f[�������I�K����R�0�N�T[�#8������qN���AJ�9_v����<=�q �b�Tձ�Q$�L��A0Oka�Ѫk0s��e�+1W�Z$D�*�Oe!��k��j��:�5K�!����jBp���R�i���۲Z���Y&�²�s���ܕ9g_YV f��g07b�˜q���^���}�l	�C�W���l�������B�-p��	ſ��E����ԋ�t��K`��S ,.�E�6u/:�u�ӌi�n�[\�OQN�a�
n�
K�^�QK��L:gKzC��L�;o�)
88BCC~=�<�PuVu�jTU�:-j�׌YZ�1Q�rĴ�9���`y��l0�ܗ9��+�,�S�w[��@�f.	��T�������˯�i|5��y���"ߣ�9��֋�� �&�g� sr{��y�N�,��QUa�y�D�����L�ۡ��3�|�q�Զ�2Xgr7�U���
��g�{+&�Z1���u�WJ���]u7��7��wuD�o��i�L�?Ӎ�1�U��q���j���x#M���WjQ��GG��ۇ�z:�T��������lܚ^��qmTc@^�)ԑ5���b%i�|oW27���<�*mXk;-ro���'�4�n�zчM��L�	��1D��(X�'t�iZ�D���u���D�ؚ�&G�!�Y�?�RL��XWX�-e�b�TT�	�v�mx�!扙�����T�t�9>ԩ_��>�nFM���R&Js0
����\Ǆ��K�Z�C��/niC�\��i�DK�o{򨄣DOTKZ������]�����}x�����w���c���>������?~�����g�?�߽x�����K$�z�%���AoGN��^�Ī�����|�e�k�O�ɢ4*)���
��!a�ЗJ_h]�@�*{˒3��@j�����t��*	�����Q!Y��-��P�A8���B�z
�0T
ėR �:�艵�������$N��顦�CBRB*>9I���u'i�9[���luU�9 �Z���>P��AA��=����p�,��!F�A��eɡUp����\�KCIJE/���+�R۾Z0 -Ђ(w�_��L�hD�-�T��F��RuUf�!�xT�9�GU�:ص�?xt`o0$�!�gz~A��[�6�A3v������\a�f��F��T^�%R�ո6��N�8w �^�꣤��K�z�6�����>����g~z����X��)t��f.Q���D#;k�A��Ye�j|�z���u����.#�~E���)C��=+5�#����>/*	�5jc�������H�
C�R�WJ���%'Ǩ wR+�����U5��^�����|}��&`WEA����)���X$�}Im��˸4��J72XT0��}5-쭒A���9����++b^c?^�jp�!A�ʋ/�|�D��c돾kN�e�!Yt��D�H#����55�BZM�,�?*'��*�1E��أ�/��tvU��v�
�ʢSCRR���`Դ�]��Ik ���;8l那rb�_w�ɋ�J�S�-i]��8*̸�5��X�i��4�QI��,vH"=0�z��Ȉ{~�Wc�J�[h�����v�ҍ��ܸ�
�WT�oH����!A��[VS��|��qY�d�    �0 �|h�P�ZF�:���<Đi+�V9Q���j`�!�V�ֶ �υ2t�i�cȴ�iK��a؄F%�=Đ�ɈiK=8G��ǭ[��u� ���!�8�#�?�&.��N�!�.&��v4���CݰyЦǦ�b(R�P��Q��
Ga�q!�_l�Fg*�����	⢕H��aƐ�(V�}��+w��+�Ut1�Cᚡȶ&[Ɛ/���v�S�C����nʛ�.~L��^��=�cH��5S�j��C2���/�����!יnn�����ts��g]^^-v� ����n���?�l<ŻT�D�R����s<k�H7�@Rb3�fSs�KZaH�+F~v��c�?i�j�:1)/3_V3���\\f��m㍲B�Ζ���/O�DF��N�l�k��>n��s�}ƭ�$�
C֏�sJ��c�j�6cH��QVY.��l،!^F��U[��q��C���򌪭F�qq�6O/����Va�ڡŲ"�zܬ����z�2���.nw��4f���bHP��")Y���lg�p7�7��beU�f�1����z��c9#�B�'�ϸGu�w�\��LER�ű#��ZCb��Q� ����6�_���t�����7Z�2�z�U��)�7�^��B���-���0�F]^Oo�t8�ؕ�f4,xؾ���JK�һ�e�bċ�5�jQ��ŏ��\Ɛ$/<Ir�æBR,Z8cHP��,�m!°�N�Ym�j����
뵧|0D1F&j`���N:����#�� v�U��V���g:#ݍ����/XG�h�F��Q�n�+��^LU��n���A�1"G��1�C��\�j�m�C����dɠn�zX�ھ��q�(H��N:4��cMՍ6	�"$]b����#=�+B�ɞLM����ߣ2l���1$#�}Q�n��Ե%��w8}0�����(r���𭋌r�0��{�ӝh�F��F���ZDl%q�0�!IV$�@V�0�*_U����D��x�=�=]:T=EAI�"EZtG>Zo�
C�BTN �đ� �/�
C��RYhV�ҕՋ��L曒�r�r�j=J�j��F���P�M�T���,���p�[J�4Mµ(�qS��~�Bz��L"E]����I�"��qz��G�
1$(Ae/ű�t�h?o7,�~���S���*��A��[�?�mk	��zu4c�\��;��VdEP>P�C
��4*�\&� #������΄����������D`o��L����B̩LH�!A�d�t%� ��_��2���`˱�Aˉ�CL���-�ԝ��`+�&娺��4�bW��1$�)����B��d%�0$Ŋ�����wf	�?�(;�>bv�ShO�!9Z�\�X�o�@̂�tfV��1�TШ�������t�5�@\&��g�51�sH���(Ze���?�y���hm/Ɛ� �X��cT�=5��0$ȋ VՎ틪U�CR\����mM���niƐ +�D>��ޭ7��٦�����☿ʔ����A7�%*��A'�"y�rT��2��(�S
�^��o�[�"��]��Ս��r�kAC��~��0~)�I(�J�`�3|P���E/����$;�����ϙ1�$Y�T��.ȉ��@0$�9����O7�#r�.�(R$]�e��#���7�d��H��Z.�:���y\R�}Q�ل��9�T�EN�O��8,}cP�X�<��h��oe%QGɤ?c��ا
B�1���²���#T��7��E� �ԃ�]���%�R���JEA����%�b�2�ĸ"�8���ݸ��V7�EGWi ��p�Gc@�a$y"�kțt��Q��a	�"Hm��HY��(���������}�쵧c(z=J�����_K�F]�9c�Sԭ��@~�y��jNB�����xxh�s������̕ ��WGP7ޅ�_R���b����k�d�oTq�$	��P��M��3�bՍ�oB4�����oئy�áB]��7��1�ɕD�_����a�!!�D�ƒs�GTaHP��xYr6\�� ,�ʓw,��Y��+'%����!IA�\���Vtb(�\I`���]oA`<κ������bƣ�M5�cHP���k��q����t�݊mr���o2՚�CR�H�jHz��޲�!�QkP	,��u�����7Q2D��\r^�Fm��D囝Y�V!%E�[�/�R�*��jõC�BT��X��|���d	�"��0y�Z>.�3��7r�y�bqVJ�5�!sH���,�!�$Y���6/3l$�
x���i�U޸6�Z
�3��x�BE�k<|�����K0$F��X�3.'�_��:�r�$e��iS�sK��$�,E�]�cĐ�(r8��-C�F�Q*xSaHP(�.�'�j �7)J��7�i�QCB\�ȷy-��.ῇ��{������7?�y��MU��ǌ�����>�{���4,_N
s��^���-U�A	�"��O�}�q9���"��S��F�	�"D��i��|�vA�w����b��G�O��^�����<o��-�)·cH�\�? ��DRJ=Kq'Z�!)�H)~��B��E�M؍
���z�+��2�Ğ
C��G=�GҐ�%_tbH�.�)*å����ݎ!AF���5��_tCR�G=�m,�҈�I��0$ˉ,:m63�%Y�)$�cH��3��PO�!!A�\0��m���2K���E��A�/=M�Ik)Y���CI����|�c@
��k�O8�G�/�v���&C�z(�*R&����tQaH��I�[2��)M_lƐS�S<g��裪�k�cH�A�H�*�l;�1$ˉ,�Y�tS�eǐ,:
����B5���DP�8Z��>jON�
C�"%3�#҃!C�QAc�&���z�=��g���A��Q��6öZ���V�cH�*#�{���S���۵ -#REI0� 1j�w]Skfǐ,#��y�B�Z]�թs��:G	�ӆd�����s�zǐ�|J�*���`�b�;�y����#"�ٻ��ɀ��>�u�Fi?Mij������n�Ju�z�U��h�am�&�;���멺F���>��^�f	R2{�y���ч�Ï1$L�Q�;l���W��iU	Ɛ #���7�;�o]w�d�0$���g7]0C�۽a܍�<M.$�BH��>���/�n�RtN����2S���7 0��9q�3�p�-���Ӎ�Rd�+#7E_���C�R$�c%壝��W���9S�YC���HѼ9�3nCq��0�1$J�1I}�+c
���n����Ș�{f+d��N���$�b�p�R�������=���­_���m��u�ֈ`3����DW�/����9o�CQ��-��߹Tx��
��UG�Z��xmP�;�"v���Hcs�X���H[6���E���e+����!�d@��D��Đ0-#�b��>�l&��U��q!�=�CB�Q���f	�d8���k
�c����f��DѡP9e/� �D��D��a�ȏ�A���M��w���@�Zh��r�� 1$(ɀ��0����� X/�l��a.4g/����O�Qb�P̣)w�V\P�Iޤ�m3u�P��q�Fu��a(�B�I7V����O����X����rciQ�G�w�#j��Dզԍ�����nysO~�2":F��\}�� �Q�ڶ��ZMi�C����ʏ� ����� �*�t5�x��Q�n�lX#CW%`�I���]�U���1[Dé7���.���� -�����{/��UD{��	]T}P}V}̦������G]w>$�*7���0?]�j��wI�2$U�C��O=�����QE�lg�a~�<*$�
�f�n�d��i�eմ��1$-��cը/p�p�Սn�f�%aﳆ��G�`H�x=��0z�h�i	�"�W�]O��;��"H�X^d=Đ +�T��ǭ)����R�1$��JUЄ�u��nOuƐ$/������tiT��A�[N�MY?l������F���.���E��5?�t�C��H�4X�+?,_�fƀ�m�K_�m���ᯆ�*    �Dk��_qaA���.DK�*>�Ql�f�R�7�fsj�u?�ȴ����`6u�0�_����f��\P��C�|�hH���1$'�E�ΐ6����m�׎!YQdiz݄-y�%�M����.�f98L��Ű��	�l��p� �m+�cH�*Rx-\��=����4f���]����_�7Q����ï��eI���$ѱpX��x�E�xR���F�A����I*�`)p�ݯ�!A�*�Y��cдݐv��|��m1xj���YM74�L{�d��'Oa-�5u��Y6�x�+(�/� ���~�T�����D17O�(�4�κ�aؾ�YkC�2j;ɸ}��M7{���HrI��y�>P����`?~S�p����3E���:b��@�I12��wP�$��a(*��fbb�
��W;!cH�+�zK��F�
��x�(��Κ��I�z_
cHR��|�}���Z-�1$)�$��F�(��5��
cH�U����غ.�G0 ��SM �Ɗ��G�C��z�S|����W^���.pƐ ]FT��j8�c�h?"kDFF�>gD�%���*��K�B�b	�g�9�o�ȴ�6cH�!骐N�q7J\RQ�2e����Y���F�C���]Pgaf	I"$?@�X42���+��8T��|ٳ�����9;�d)�~��N�����ˁ1$���X��}������o���Ϳ����qH*�-kJ	����:�?=~�"�i�)�j�T^������g�������7?<�y|{A���1$։X���?BC5�͎!t���A݆�h�9��Buj�`����z|���ُ��~����>�y�^��o>m�������?����ǿ���)��>�����O���O?����g߿����=>��/������k�K���ݖ/v��������?�w��Ųc@
�ǚ�8X�i�`SK0$D�P�O���cH�.R.)��E�<:vI�#�0�])�D���2��f��{I��o_5@v�ST���BR�W��(����H��R|[������~� �>��_��*�Q��ZVs��j���X�!1I�H�j��_UMl͎Ik�.5��Ꮮ��;���P-Z�:�G}[:O0$H� �o�#����d�������B��1$(���-`���dt���D�ѵە1$�)�lS�����I	"E=\��7u�{��=�����L�qA�[k7J�
���ld�7j�$�u�R�=�4��E}�SaHP>����#[�mE�� -���������;��2u�ow0ܗ?��TƐ +�肥���w0-���:�w�re��BYfO�cHPA*_��jjn����E����3�C>�q#h����k�Lc��1 ��{XZ�
�7Ӎ��Vɮ%��5K/V�cH��1���,��K����nT�m�[v�6"[��@�%E���^�ݙ�2�^��J�����N���7V'�Vi���>�C�|TƋ�T�d��`%�k�rU������Ok�F^�&0����n-�(�ji����P@���n��VDFB{�<\�A���ܚ:�R��a�`�=E���޺��Dt)&�R.z�;���6Mnǐ(#�4uJ�ċ���]���"����q4v1��f���Eg�Q�d'ћsmriw��?�;�9fIn}������ŵ�x;��w�`�������"e�T<��o6�m��Z��ıĝ�>������O?~��óW�����7�~���7�߮������?��,�����ޤ�Ͻ&E!�-�aј�60��R���M|��bm��3����ek��=�����@�k�dT����rȽ�-�l&���!&��c�b&�4H$&n�eN7��3i��J�����z�X;���H<�($��ӆ��4��8����L)�ꎋ7�UB�^ VX�z+��tDe+�E�U���b�᜞����m��!Y�8t��j�馶�S1$I���^���Z�!I�H���ؾ��#`S��c��F��~��ˮ[��I-yV����s��� ;����U� �auRQ�����!AA�"hԭ�}Զ�.cHP,�ʺȂ��������%�HJ�6�P;���dƀ,
1pz?,�I��~�w�-��E����ÂZܵ�C�谸�f�?�VG�!!F�\iu�}�[t�!!���g�>�XmCB�����xAN��VƐ/r4��x}I�nOp}#�����N���v�MO0$(� ��#�Q�޳�!A�*g��|�M��1 �BT��#�ԧ��ш��jm)�!YJd���͕eƏ�wM��ou��?���g�!U��'�d唻���)�}��$9�g�ۗw�`H�i��>l_n�D�!i��Om�/H��Y1$)�����T��cHR*�>gvI�;$Qd�����A=%�q1��)���+�zK��cH��U��xC>��u�C������Al�h�k�[�F�#uG��O�PCS�h�Z	r"H�Q�.u�ܾ�m+Ɛ,/�4��J���w]��ɢ��0�]Yz�e��
��D�s5����&���D��,I�=h�T8K����k��tIV��R"K
�_��ǧ`H�.�>cAt�-�!9F�|Ƃp]��`H�IeA\�����!Q���^��&�fǐ /��uA��M�F�uK��p�X{MP�#�[~

��RBs�\�k������K񜾘�����$�N�n���3I[G��f�Nj�w�����N��F]h�]7��0G��K���l�ݼ�����r�=j��JbH��G�/�jvU���ݘnu�w���)1��ٴűw!SvPׅtxv	�ҁ[Vn���x�:� ���6��Ζ����*�Ch�l4�n7>&��/M�K�ѬYn�mە�D&�P=/�R����1��Xi������7/g���}���f��z��T0 �^K�Є�1"/ݶ�ZaH���|���"e�m�Q�A���kg�\ηS�l��#���5�����Х��<�'�0$&1[��SHo�� ���#����F��0$���f��o��5���P�!A�"�����㿍;�謰^��������:#�IC	�E]p�d�v0�d���ö�����'��Ra@9��rB���O=�0$�A���]�;*�4�mǐ,:�V�z�����SaHP*��P��ҟ�xL���`ı���:�TfeTιBQ�!9�HHU�����X�\n;���"e+��@��FŜ�)+��h8�#!�E���M��5i�tŖ@�ԅᜫ�T��tR�F�n5�e\a@���5H�{����/�F�5�
C�L�F��%����G�d�bɡ�}�ˇl®���bHJ,R�ᗏ�D�ص��ub@9@�n�>���K�$Sa@�?R��bd8������+IQ"E̙#~R�f�$T�����w븜s�z�!9F�䅝�z��|�V�+	�"HS?tAN�S+�qv=.�8>��_aH�)�@� ��4k�B�{/�1`4�_qo߻In����� ��	Ыy�N���$��؊�H` k'������X��}X$��M޴���A�ݽ���a7�H�EXN·Șr��Ƃ��$E�ǟ��oyZ���~�D�J��ȯ��rM;AC�.��Ou����Ĕ��NC�*|$*r���H�!�eH��D;YH�����NC ���i��L�<�7���3���^��nlԌ��[D}Ժ@��d�Ӹ���D*��Ze�S�v���>&���Y�j�^y�� (���R��o���T�NC�q>�i0BAt��C�lP^'&�E��%�D`�<2�B�e��C��S�����������PCϐͷo�y-kd���[���$2�����5��4�O�!�o���;m�ڀ}�oރN�q'/Ő�7�o��NC� �o~���� �!�� \e��2�,�v�~�>k�B������+���� �!o��PC�� ;o��d	PC�Ȑͷ_늼4�(1h���z,��(�����I1��J�t�(���/ڏ`��BQt���pDỈ�j��0�a�z�R�_    Z�8Te�a �nuc�^]�9��� ���� �!{`8��id��8,�OA"C6_��Z�b�A%�ԗ�/��j �#K8�4}�i��1(sj�����8�����|�����,�9�6�{�f�OA,C��W��ls�UC��᫽�$ϵ8��j����"X�� �A6�}6�F?Y5��{�v\��$��w�����Y5 �x�ng�e[v��F1f+�[MZ�Y�F7�^ط�#���0f'5�mk�c��p���_0��؟h�Z�������LI۔�Ѱ�(�Q�😒FO��D�����Dg]��}�j�����M:�-�ԛW�2K.`7�Wk_c��(������;���!�n�-�_Ne��0D	3���0�a1,3J1�7%�q�6ݸ cq���r�Ĕyqi׋K�R�Fi,��^�^\�lh�V8���Ũ�PC�� :.�TQ4AAC���lύ�j����ߍ��A3����)��}��:�Yt2�d��.��41������o�E�r��E�M�����e��ї;2,�5?��!�g����q�&4���Ϩ!�{jS�}�`��LfQS��{3f�`x����w�J<TAACv��j0BAt�l}���}+���0d�w%�;>��!�e��#��F8y-V�?k�D��v�ϕ��'H��3d��S}����3����`��D��|=l"�b$f���9���bu�[�H���+�G!EY�J6:�`�)�Y_8^����u�j�~a�m6j��� E�� �0��u���@�A���i��5 �����W�O<n�r�����0�1{��br��UC��0���b�;)�Os~�NT�؜<(i��P֟�҄�?����Pޟ�B�����RP�O�!�bPM����lԍ޴j��"���C�ѩ!�a��xV����jSO�(�b8�� u��[2�4�U�N�-��e��p�;^�%�Ҫ/x���Z��Ǣ֝�@��ZL��+o��0�b�"�6�B�i@�p��� �~���!^� Q�~P�0ѱw/�i����WU�Zu�
:�t#mV�b�����0i��r��oяNC4˴�l��qu�����z�.wd�!�g�v�(V����H���{�.��=�)2�Q�(���.�p�;��`�NCm�E�!ԛ��V��63�4DQL��=��m^�i��e�K�h�'�p�m�4B�wd�# +��id�߼>�Zwt_�t�w�B���÷��\)\�&��Ҫ�y�m�z�ޕ5
�����_}����_�}�?���~��3��!��~F䟡������ڽ6����NC��0Ͱ�)M�Ҩ;�(�P����A�f%�⍔����d^��F�ԢE�0R���~���^��@�A�-���^X���N_�^d�DJF�-FJ�����u?W�i�En�RR�!Q��jU߮)�4D�LJ��Du���{jj�!Rh��Ԟ(u���q���DJO�-hI���4Y�Z���띆h�i���_D)�2��j`�dE�X�����;���j�U�i2��@��ڡ�M�4ԋ\�H����������	jeՊ0�-��#�!Tv��k��G�;�<��>(��T�(4P�Ort����(2��s�"��4ϡ��@�A�]+�����͚E��HI���E+�*�d��j����`}j�׳v"�Fz�>�`�h+�Hٍ��E+ly�R�Պ�r�ËV��{��NC4�W|�K��V~}Xm��@�A��z7PC��H�p���6�:�"��~!��F�mW��(1�Q�(���U�?�>̩����$nU@;�������}���H7�^�#5c׋K�"%B��4n�;Е�	j�c��[�������<d��?�~.���A��3D���kD�Q�q=��D��]�uPC������Ͽ�������FJ�F	�%(+��G1͘ �i���RK��{t�8}=UC�(�����hk�b��ڼn#�Q�:��3J�4|΃��NK@��+)O��]��1��x�l�k���aWQB���~C}�!Jd��|�c�v���H9��E'}�����j.Gɒ&<��vأ�E1e���Q�NC�(�} ΃O��0�n ��`�v���&MF��莿�Y�NC0�q���j�v'k�♲���;5D��ҢMb�;��LY��F��z��� �rgM�IR|�=;N�XC�,[+m�6�����V��2��s�z��5����$>F���ډ�a
�Yղ�H���j��i��5Й�%J9�Fø]�y�L�<�
Z�c�v����X�k�W�B��kd��(�)��g8n�i�B����U���=��bi�!�b����m�J��U��q��bifib���L�cS�k28=R¡��Zx�C*��-���2�,׺6�XC �@W�V9ȍ��5����%� ���v���5�
̢��̒�ˏ�K�!��+da��AT�5�#/�@��j�F*�v�z�eDU�(��^�}�_V�J1����t�U�Sc|�-��E�A�-����獥�2)O�;/�!��+��{���|jW��&�j�������C��@���
O��-¡J?�-�� ��e�j���(�],��!V��o�g���[�ŝ�2���q�[�!��C��J�|h��na̸A�)��.���{2�T�5��o��%Qx%_�kg�k�,%#�+ҡ��T�qܡe�,�� ��b�gҸ�����6-V�a�j����W�y�Q5DL{�+�k�V��W\я��/J{�gUC�� �v��ݱQ;�ڪ%���_Х ��3B�2m�#�U�'UC4ʹ��{��!��+KJR/*@�n���H�I[�������^C$�H{=�j�6-�FI�hӣ��,�
��E�m��f9��z���^{M���U5@�l@�z�3�-���I5ғ��o�^z�4����~�IL�����Z�=C/��$�t]r�0�Y���{�����Oq{�|#�ժ,��4j O)/�=��j�o�z�"Ӟv�O�{��etgO�a��f4��ý@��Xgp�L2N�fj�jJHف�w�Cr�Z5J���_T�4��
���d�<W���$<�݊�����貳6��r�FYXM��̳>��N}�j��I�p���чA�G�'X�7��O��5Ċ�ҍ����>���V��M\�N-:��lުH��P^��{DZT�L������[J��Di��:�;�<lQ��<$�E��Dف�ٹ��r���5D#7q�87I�?�k���`RaoUf{�|#=�nZ0Q����
�ga��r�	�k����g�>��k�F��
s��82Zn"�j DI��
sY��4:�@�H5_�r㬂5���DI��;����,v�(��q���(n���'�z��Yl���^]?L\NACx����:��5�tv�-�MPC���Z�Ĉ2��0|I��@�A�du�N���ة!y�+�i͡uÚ��~�@�?�J��a�SC� ��l�\-���$\&Q���g�y5i���0�1���F�J��#����f1n���1E�ZsZ4�+�'Jt�{���?�8L�N��T�	��6P�kS���D���]���J����=�.*�%[VЭ*j�� Q΢��R�N�Ei�����@��Zb�&ȍ~T/n\N���[LJ�ö��R��;D��0�\�I��p$�צ�F�Z5����w�J�R�����5�q̩��D��ͦֳ��8�qڑbzQr����;��m�°l95D�L�=�nC�"�!:tj�T2t*ɿ��t厙�A� �r#��7�BZ�."���0�d�r��ˡ�H��� �+0�ueB��zm1�9|��\�j��f���YTGM��@S�;��\��7J��P�j�������?�,�L�O����VTO���7�$̜��_��(��_1��()2ܬ�>�� ��������9�@�N��My��;��^<1����夅_�tGߝm�c(e�UC�0�M�E7���!
y��z���    b�s/��kDN�Z���_~������̰Upj���0V�E�䳫�8����$�e�Q=�@UC��T;Ó�g�/v�rC�������׏������}����kǉV�&�~Q�����
��Fx�(�!�7P���}�e�!���a$O�(
)^����׽�X�Y����R3����u�]nծ�D�f����ߪ�8�ȫ��&��Q;�I�1�a6���&����1�1UכM?O���8�9U�O��/��fN�������u2��˽@���,���/V�v��U�T#���6�ݭhU�!�f�j��q��8�bf奟�G�Fhͦ9dS5�"�p�T�xS�5Oӫ�@�A�OhQQ�fT�3��!�o��rG����a��dP`Pv���8��3�5����D!`^wț�g^5�"/qR��f��W�(�2\�����:;��>O]�O�w�m�&oT5�R��Wyz�E]f2�>��AZ|(m��8O�!y����dUQ���?�"9&��:T�C?g�|#m�:T��<�5D
Lz��@����YC�ȴ'�ղ�PC4�W�.TIl�PJ�W�~R���䪆(�Q�,�e6XC�E7-I��-���2&C�E�&9�|Z�i�!
���JGq�D6j�8ue��v��F��"��{Q�d�b:Z��CFӸ���VkUXF�kƎ]5Ċ�:�Z�J���V��X��L�d�ɒdT�]��b��Eɒ��v�M����td�j�6}����z���@�A���4}�;�I�~���_�e��y��E�1��)Z�VѪ�"/q���D3
ܑ]�#J�WP�m��{n��|���M��"�jQ`U�*_�9$�ۛ]?�m��X�Y�״��	�\-8z��!y�G���j��6�5@����YM�I�^C$�HO&�������KY���)>�f�5���!�a��	�{�[��!Zv�T
3�JU�!���8����ҧ	j���������?������󏿕�m�u_`��30S��?�I���_���O�� O�(�2�=��� �`���CRI崩�Nda�p��i��=�<E�,��UG�<�d�vYC � �V����3c�ȑl�Un�SH�� �H�`�t��蝞��ټ��)5_k⡆ �Aڜfb �Ɣ^������ǉ�SeJ��~�{�p�]-N��_"�V[��:�'�5t{qT{f{i�������B)��pX����/t�]3}58>���#V튫v�p�@����8�l�'�pX���7����i�L\�!CC�CS^����fu
���H�I4Вd�� 	U#�$rW�ʄ�5��:-�jJ��W�ʾ��b�Q7�$�ߔA��O�2���[r(�2��>��S䧆 �=�V� J���k\���@��f��q�1�5q)���?i<�5�5()=�<?6���a����it㪆@�[Di�m����!P��7��7Z4�Y�S����!��a��:��~��˟��??�Ӈ�O?|��w����������/?|���{m�[�^���������×O�������>|���7�����O~�����?���Ѻ�M�NX��0W2?����5��s?tr{���-�΃��N�������o?|���/�.����5Z��8��2c�}���r�1I�w$�%��}˔�s�b�0�V�+}z�K1K+��`贉�5�f�US~l��oBgh�Ι������pn�5���֋�am��8�Pe�o�LY�kߜ�d���Зt�_,��%Δ��ɓo�ۚ�Ѳ�8�[S�����~����𙲆H��j'������$&M�}�f%�5�b�����.�^C � ��N���@���`Y1��� �(�5�� ��kbd��1���8��=u�z�8�#YC�](�Q�X4�gaa�LɎ ��>��XC�Ȑ�WO��yUAR�l�z3��f@(�5�t?�&~%tY���T13ʡ�C	'�Ť�S�̚�����\�R51�|�j<�����}�S�%k���^l�=_m�9�9�y�Ŧ�UC����ye��Yv����)2�����Ɏ~�j���E��AK~)i:QdW'�������es�Pw;:X3���!�bPݏ��d�YM����"5�H�M&[�hL/�-^嬦+Lj}~zb�t��.OT(�WMW�Ծ�����4�V��������H�F�1���D	��L3��Gt�]βS'x��(e5��AH�35���(�)[�A��0�dQR����K��厦��=vڻW1;5����[5N=YC�D�r����Y��"Y&m'�8������G�~A�fU�~�5D�Lz��S,��YC���31��+d���cǰ���o�J���ZsdԂ�eWo+1����z|n���%��+���Y����9���(�4q�Zh.��\I>pPC,�,�2U���K�k�ގ��e��.�/���Ot�v����2(5�h��F�Pv�!�k��)ҡ$�)l����4�Rt�DI8$�?5��}�NC��,���U����R��+��W�U^PT:���M�;ac6
<T�jlk ���2/�0}zO�!�bL)�\;��e����K3�o��;�v���NC,�Y�Qg�]��kc���m��4�q����XC���5]�(j����C�!V`�~+�D���e�s�s�nG~A?�
�6�v�$�lw� Z����
��i��]A��g�n���(������FVI��M�3��P�̓~�n���X����l� ^�{�0�� �tc?��x��Q1q?Pfv=UC���G҂�&e�{������}��-��i����m�� &�f�#�q�V7e�b-/�:B��t�h��v�Sz���շ+�{��#�'���]v��2f�#d�z�oUC�0�!���;a<cU�����r:qs4
44؍�A�yN�q�ۗӂ[�X��ϫ�v���/)���w����_~���/?���÷_~����/�}������/�W�����_���/����?���������������o������ݧo�̛���O���?���?~���?{Gj̻��lֿ����<���O_>|�D�N^4�w�������g�7�GP���t��cT�8E_v�[�˭����z���nx��?�{Y����T�����ԒV>�%u���\�!�Z*O
�7��i i��ߓ��,\W�[vu�!Wl�^�$��;�4��1l�j���9���@���1�M���SZ�Dˠ� n��_���!�c���J��r�4D���L1���W�!J`�|_ǃ�NC��(�b�b���r����d��ܫ�u@���B�-���/�]��A�'��~A@�!�f�n?��Q�!�i�ǗLO<gyM��H�K�)�2$K��ۉ�ԙNC,�,�.>���}Kƚ��)$��#�=����V5D	L��	��4���!Jl�]�`�w�v�$��J�ON��L�g�˲��eJE%���V��"��f��d�.����}��;�ƇoyFӌ�j����]��`�4�2�R'g�gƯ�,ʜ�e�n��R�oͮ�y��(�����O�̭ޙg�֎l5�oU�;�Bm��:'��(�)������ţ�	�a�������}l��D7b�];�+�zZ��xp!a���j���T�l�������@��637ت��m�	�Z&��~IH��+"Ǐ�jd��Aꖪ�iD��J��"��f9��u�3�3hsl�ˉbpH?��e/[��Z ���<�g�z0������P�Q���������'ʨ+�+n��<*��]49Uu�uE#+2�A�e�!�n��!�i.��}���0�W�1�3l׍�ͪ!�e�޽U�N�ĐNC0�WJ�ָ�^���y�O � �����@��Z��M��j�[-��շ̻NC��,�w�n�K�s���!7�.[�joUM:����?٪����6���L"'�E8Yä��C��ѣj�f��0r�ܼ�v�2�L��B_�CC��R��ߵ�9�9�n:������wpr���d�Ц�:�|��<m��DƔ"A    ac��[�h�}���y�s��dͫ��W�(y$o��o�Q���PC$�H��l�:L�XC$�$��`�����`��x���03�YC����j#;LVM�[�P�!�c̓>�nG,:�|#=��	ߏ�I�I��x9�2���h�i���+�@�!y�.�B�Z�/���{:P(sVw�;3���!�j�v���3S~C��8�#�
הx���H?��z�W��~x����}��Mz�f�}UC4í�t`��2;=A�o��Ә�eŪN�-�ٲ黪�9nYv���C>V� tbyn�+�<�p{-��,�NC��4_h���&�L_W\��(�V�'�pOpq5��dZ�|�iբ���\Z}��5�UC�E��۟R�w�YC��_��"����Z��X��p�vS�O����b����<[��V��-���1����M��嫆h�1ڡa���ߤ��߯r$�g�Q��l4�Em��f�"�Jv�ˎ~�R��m�)kʺ-��6^j*��Es��ӝh�|[iu3�l���'W�a��[ie\�s��vin\�Ź	M�����N�P�E���4D3L+~KG�6���5�4D�Lke(�
�&m3��h�i��N~�6���%�Ѹ�|ؤ�q�LiJ����%Ƙ��K�%��4eh�x�Sf
�����U/!_rE$����Q5@��L}E#7(n�䲆(�Q��:�;�ߪ!�aNvV���))�n��4D�L�M��Jɧ�;�UC ���4��&���)�5�|YC0�0_aa��-��j�Z�v��<�����0[a[�Oa�!���%���l��`� ,������2Z�{cRS��x@_�vj��z�A�8p��`��Uh�xl:<�	��-�(K�0�¡�u��� �e�J:�3>D:f]/
Ug��=���4��ߘ^�R�u����3��h��b��7�o-ۙ��S�׋��5UQ��
�؊���
�NC��`���>hm��5�J�U<U�G�|c��I:m��$U��
Tm6��eVC�uz������K1�\�:��NO�a�-�;#��VE�����r/z�N���J��G��h��E���ȷ9�h��w�a�[9���!�m0ϰ=_���)V�\��O:�]�Oɜ�']5c�_�cv�zo�D˱�W��<�v�G��䇫�h�i�i{�8�P��`�a��6'r���YW��e��=�ck�\���!�b���m�,�p6�go/�����y:\53��v������9�Q�Z���Fud&)�O�ѴH�?l���o8�g�Y�]\l�(bܛ1��ٶ
����9��F��؊�!S�5D�L�oNS��M�0�a��l��O ����E��F�|�z쓬!Xb�n}rk� �<��i��
C�JS���.����)��mkZW,ϯ�,^w�����@�a�!�aPMG������6P���$+�]�ާߝ�@�A�`|^YHN��Q;=�դ�rlmK4K�ZG��v�ӸNC��YW��8[�X��j����]�V�>�i��i3���:@�Y�rlm�8���������tCy�W��͎=C-���rm�~�3��H;������b_�P���O{���0;�����{ݼNC4ϴ�=�'�i�F>�JG4�rhj8���!NdN��
�6�m��4�I��v�d%Jئ�R� �rn-����0#����S�EiHZ�4�*��}��`�V�mר����ur�gb����41)�gr��%ov��F��`�ү�:QS!v n�ATA<C�ۿ�i�}ӿ���@w���t��LL��YPR�������.�t��J���M�r�v?��i D��opG�Z�6�٨�^lJ���5�.e1(�#ª��_���e���9fQ^�P>����vF��0~�UC�8�#�����4�q�9G�C�fsfQf�P���!�?#cn!�NC$rWF�����A�NC�Ȑ���ٍVQR�ȯ$��-߸� ��fm�ySܼ&��(������Hq3�,k�40�W3�+�;QS�	>lq�UC�(,��{)�NC�����Y�,�j��uH�����n��tT����k��u;k�$c�rUC�Ƞ�����v�:�!���K� Q�m_팺�]�^����u�!�b��u���ev�2���=n����NC$ä��jtFUC$�HO"
��p�"9&=�(�0�o�j��^�3�`n���"�OI��
BJ��W�q��V�"�Ҿ[J�����@��ZPz�����e����U�6�S�WHUC0Űg�½nǭ;��[<�?:7G��*o�rl������j�4D����[8?����䡡Z�?R��qjQ5D�L{8���~@��g�I��Ӟ�(�[��3T��=8�V��#N�3T�Ե�'��]���'uOE�e3��EzCJ��Sѡ]4�E��������?|��>����ӻ?}g�_�����ߕ��/>}��T܅�?�����?�(�az����~��ւ��9C�0g���eiC�M�Ǵ�����R`qsT��=�!�y���L$�2SaSǥ���d�y�U;=;�����۽� �}9j�q�A5��s��f�
69|߬�K��/k؝o;��:�ȓl�e�-6��5D�L�
0����!�i�� C�� zzK�L]�0:q�B�9�XCr]	7GY,���������������m� or>UC<����E���O���}�6�T�Ե]��#y�Ͷ)�3ԪgD�l'�<��橱��E�5K�M��J���6y��G�M�u�Q�ԑ��ǐ4k��y��9��d�r��WP�j�f��j�F�d�X%[T�S'�@o��:E;.�YC�(�Nю>��������~p�nႩܩ;�pny`7�UC�ȍj罶������%n�n�WisxI5d�k5� �
��.<t���S������)��KP�_G���k1�Ƶ�ZT_�4x�+Ld�B8���r�����RF�o�"_�慂9���وf��u2>Ϊ!�e�~�]�D��8T�Pi����Ϛ�a�4{��6��EW��@�A[�P٨?��!Ph��݇T*���Q��<ei�7�{�E�i���s�J7ª��n oQM�R��߽���4�1�5�Q��]ԙ)��5�����3S�kcS<E�8ݢ؝"#UC,r�7HV�SVGZfuX����7HV�z����0�a�_�����i���U�R,�8e�e��+<
�3O*���RK���QXĘ���EYxKy��QX$�t�?0�`hm�v��i�j�,��\��z�L���T�4t{���8�y���P���^Ͻ�}�!�c���إK���NC,r	����j���<p�~�)!ӇG/h�Tbac�W\�X�?�in\5��LI��r*\
��>/�]�NX
^�+E��igд��k�,�d��~�Ntl8�� <6P��o��L�˲he��خ{^����+A�&��q��❆h���n��Re�v��UC���ӻ1�ly�iV�"��CZOX��h�SW>U�ݠ���?5@���p�ߨ:�6M�fRfe��I:n���{�ǩ!�f�~L�{�SC�<�ս�c+�M�ð�>5���^\�Y�ɋ����1��:C�ۼ{��!�g�n����U�vz}���-ea�+<�py���)5l����k�Oi��);l��%�)�}�<��f)53���i:��s�FЩ!��k=w_^���O����k)k[�]5<���!�aXys�S��m��t��2�jJ����T5�#ϲ�"#�q�#85��ٚ��4�0��ٛ�W�zzh	(���Cjy�ڼ�XOqst�W���Y��'��g��u�:|�CJ������|Ew1W�N������� �ywj��9�l�l�_��q�\��[����%��=��9ː�\L�_@�f�Ct��D/�̟N��A�~�q/YA|��F_`G�-%_��] ��HcT��)���]�ը<8;O�� �A���b�Ѯj B��Z"k��۴���.n,��e���1)�_#�L�]\d)�2�b~��)}��y�SC,�,���8<W��q�\5D�~ ^�䘨�    ��ɹa����5F�^嵆ȟ�y�Q5��̨��Z���SC�����"U�14`�h�G�k'-3d3��!}�ݕ�[�k-��r���~����0�<�9J���Ɩ|c��Ʋ�o^q����R��e5:��V'�%GƖ[�)�����8�>5Ĳ��Ģy����r�<D<8J���Y�s�y�^5��=�P����U�B#m�i�V�h�[D��E�����4� Lw���Ls;�Q�?���T������7�~���?���S�w��R�r#��N��6�y���)nc-�i�*П��^�����ȣ�3 cVØ����Nal�l/�ݸ�|j��JݾW६��ũ!�g����a��ΫT�v��T�ȁ�G�!y3��0�1۝!9��0�a�;C -,�D��wq�č�S��8�9gG�R�w,O��)�G� �A�!�a�v?H�vީ!�m��~��Y��1FQ�ty�L���5����T���%��9j�%a��?H��~��r���0�1{�@�[R̬��o�R�b6�A19|<��8&����=��:5�Q��o��h*�>��:�����Q�cR����E�1MG��I=�vH�;5������facJ?�i�L���3��L���|\(f��ܧ�P��lny�2��1�5�ڬ%S�: ��Q����M�P�e:�{ y�z5�Ơ�4m��K�(�[\�����s���&�c��SCØm�0f���؆�wc�SC��y�,�u�(q��k,(l�a�1��b���cy;��N��"E&m�UT��'\UC��H�a�bU�O�j�D)��>��S��)�=
p�<E�|�,�t��j�)h�<52JO@f���*�K��Y��X�S_� %[���6�b>,YC4ϴ��"��W��<���QZ���1:�Eu[r�4 V�R��Krдn� Qve����3L���!�bG�^��5-9�j�IY���Ϸ�dTE�N��j-�p��̫ͩ���3t0�rx[{�A�Tvu���4�j�|���6dW�+��!�g�n��>���yFV5�"q%ӎ'���xX5���]�(��K�E���%SW�!�<:;/�@�%�Zh2���J���5�]��X�Y�-��r��7ũYC,r�#�[n���/��1=
�i?����0�1��29Nª�0�a�W7z,�yj��
c��s��j�;�Lk��!@̀+�%?�FF�`�%U�c�����i������TAmv���:��4�j�Tf'u� L�fߩ!�f�P��{)+��CZL"y�+&~xF��	�!�eH;^�����ؚ�!�k��G�	��$����0�Qă_O�ҫW���ɚ&�UC,��Aw0�|�j��;�y7�+�hC���t3�/��@�AϺÔ(�bif=�v(1tj�E��
�}���o�7/ӫ�H�Iu���J��i�P5r��^��&[�a�gR�i5fo�7?5�
̢����xٍd֏��!��+��Ʃό������Ÿ��jT�x�<����}������/RwN)�V��1q�>�B)F啌���̍��V�?@\�,�R4~4�&R^�z���)��oD��"RONʿ�X���6�xyϩ!�eN�%��𖃦i��5Ps�J�V�8pU�<�r��t��݉��a��UG�+0+?qO��uaJl�;����.����ay,;�<ni��oc�%F��1�+/GY����z5o�&���(;/���P�Q��Q���GޑVm���W�4��B9��tS��xV�=���!��+<���6�CqjdT��Z���f�]lvx��MA��_�Y>y���Cy����>���P�'0G�Ae꺞�cKY`qj�S��]\�{�C����J-��8�9�r��=��4RUp��q���o;P���f��8�9*C��w�Z��UC��TP������=��!Rv�[:�%�LۺUC˜�����Բ�0�a���|h�6�Y_��3'�3�r���i�[5�	��o?M5�s�o�:5�2�g��4MyʽhkC�47gYV�4�Jzjt�py�}�Ry1�ǧd���6���f��z���tS�����̩S�#��B�'f�'~�O�kN1�2,r�4FU�<�M!�?�{G �FX�&0G��Ԭ��X�;���b-�s��Qb��sħj����<;���enŴ��v\`��yUC � z���w�_ݳ�f�.W-Zʾ��6����b�_��rQ|��؜����Z.o-s�A���Zq�K¿��ⅵx������@�A�^&"���c�����%�QA�`R������u� ��pɤˉ����ޒk�/c�N��Xø���s�}F/�(Ql{�!�j��4o��f����4sr��ƾќa4�YC � ]@N<��r�AA��1ta����t|=u�E
�w�A�����bb�|mg��v�t�D��{s�X~�,��X�Y�!KMpQ����&�mLr��y��䓡�l�-	���4Nn��!��|jG�n��Mx��u@Mrr�y�����ఓ]���Ӗ�nN�$6�a�vdQ���5@�=�ֻ��9As�m�jt�[��@������P	����E�eg���6��T}Jw��k�c�U�8�d�?�v�{�,��%Q�|R��-��k�㘣�n���M8x�QKc�/�L^�鑤�Uˮ���Fk��k����m�_��e'��œ�(�M��ʱ�W��@mq�N��Y^�i�ɿ�ێr�!���*ݺC����?�BAs�tk��'�~��k�c�I��R�9�삷�]e[�k�FI��1���6�5r���~�]�^C�<�׾�v�����
��@����$#�17��ݙ^C�ȠZ�ۉ���nf�k�T�h�;���ӳ��&(/��{��3�>
�k��H=�Z �SsJ��׺3Q�f4�^���r.uw�&�2�O�n�`�":@��_�N�Ez��^��xJ���:��]��#��O���z�����V�!PdP-���� �֌�J%�%��r=�N�A�rJ(R_;ނ�W3���a�!�j�6Db/�L��5�ь�3���?43��UC � � 񧚍NSǪ!���F�6�_�E���G}mAˊl6���C{q<sT��=���8{q�C0�A���&B�!H�����frUC���V+�s�9��x��!s�O`^���iTMZ$�Jt4��&��T�F��tq�S��ƒ��GҲ�H�v�i9^5��\K<%	�5�<��Ѧ@9�����L���!�c����i���,b�tFs��v0a�dWa�t�J�z���u�Zl^�b4]��=���ZėJa4�ۈ��N�n򡋔�@���a�����NZ�J^4ײ.OG���h7>zq� P֢��u?�lt�\T���"�%ڸ<�j ����+�kY��avZ4-���@����*ET� ��st=UC ϠPAI��ތ�~;����µ�Fźr�5ĉ����PƁ�j�����Yc�s:67���@��h���h�2
Zg���͵H����GhF���Q5:�u�VQ��s���^tm�V4ݮ���!��m�5�!�p-�������깹֜�7*_�ɩ9f1:P�����fÕ�IC��([�t[�Ƀ��c���@��[ݹ�G�������j����|�isZiUph�\�
/���29���]��2<M���q;f�$Uq����
����i�`x��8͵�����m���^C �@mK҉�4�F��H��3�vl����n���j�L��Jm_{��(r�j��{�.��Ǻ8�B׳x��=#��>y�� ��+m���Mogq�H��J{�>���ϋ��!�fJ��.���&��(�#�Wic�jx�a��)��vi$v#nib���k����m��F����@��������'�ʩ�]ހ���4i��T�b�ս݈Y��p ����Oi�(�M�lt��b��J۹� J�F���^C ���:�[\�M�W��5�э�S��@�M �(����#�{����.��ʨ�}�l4���M��Ӥ.���ۯ�~�    �˟����_~��w�?���?���߾��Ï?|��W?~������/?����������/���ߩ������������_��w�7�������o?}�W�}��ϔ��I�� ��~�ot(��ފ)���e���5\�^�$���}Vp�!�gP-Ц6�����^C�� �n�{N��m�[.�����<tr���r������W�f�JTo�2
^9�]bP,󛐻7��U J��W��ǍQ�.� � )��^�V�@i}_��D7g�I�9m�1�W5�1�ia�$^�g��|�j�c��v��|lS��6eZ�+ I[��响s��]��
�7P&w ����i���.v��B���
nr�fQ2=Pneٶ2��y�Y5 ��J{��h� ��R�!���֜r�C����V5D�ܜ:��'JFǰZ�ȴ�lo��y�q���X��n��y���Ta\{lm��o���m�nUC Ϡ�Z?$E��Q5�=UC��f�xW={��(�� l�զNH���j�qt��B!�/x01�F��ê!M�_�g�E�eRګ�[�78S�֋.%R��2o��0[��UC�ZӢi{���cӸ�k�lM{��M�G�!��V�tܘ�ѩW�ż��5mʗ;f>�Q5D�����c3}�^C�<1p�0*D:�[�V�����M�������(�Ա@�;�v��h�$MwEn�xu{�Q��%� wEn7<��O�TQl��7ฒ[W
�����|��@V�[e�Qn��"{����UN�f�+b+O��B%ӈ��^������!��L;w���t]�v㡁E�[,�#�f�+V��av�۔p\5"�%��"�&�y��j���s½|�Ásnq�"Rn���8TBV��R��C�,��K����1�WV��?�B�P��k����x�)zܔdQS�6�MmѸ�W��L�e{�#H3��P>��)5R�|��ot73�Y$��tW��HҤ/W�N{q�F�M�v]v�8٬!����=Ø}����qb�LkO��tp�6��m��7N�a裘vYC���q��9h��L�/�c��g��a��'\�Q�1�j�AQbkO[��CKczŪ��T�W<��3]�������;k�B���[(�g�\��H�ۣv�����eQ4��O�8I���0`��?�]�1$����6H6 zjI��R�j��]�������DJ����J˝@ ^mq�|��L��49��tS/��c�tL-�6�dY�%1�~1��2����>6R.��V�k�Q:[;:�!�j�6wߡL=@-�D�����JR����FgV5D1Lig(�5.�YC������[�Q7���!�k��W���J��3e��L�\�%0��������b}HY���dW�4x/���#�x��]�:G��Tk�B)��Z��WSt�y���KDJ��e��ެ�1�&��KZ�؞ߐY�!J�����o�G��)���n��֘Hw"�z��C���h.��>������e�qK�5�q���[,�#�j�C	���7G5�K���w��j{����5Pާ��V�Cb��<�VV��鯕�8�WL:��������\��������7�'pe�5���O��r�Qc�eu�;��I�IL�%�_Q��[l�D�O�F���܍���-k��N���E�y3�}�"��O]�-�e��k�%o��g�UCǍʿ!��M�6̲k��w�~T��#%S�+��7��Y+�(���8������֜�)״���$�lm�SPD�6(�_�������#�g�@w+�r+���l^���3;�p��6:Ô����F�vCi<���X�����&��8��~=	x�E9�H鈡��Q<٢[���V�%4��,��"Ǡ�!JdJ����Q��@y�%{�p-X|W��F���ŐJ���<� t���j�b5���6�Oȋ�x<�N[Mz��D���ZHˋ�SH�jdTϮ�C>���p4kDn�������*�O�����jq���8�d�H����	���N����D���i�gS"���k}��!�/KS�y�Q52�Bf���ŗJ����q�Z�4�{���/��T�2t�c����1D�K��g����k��7.�a^@x^Eق�Z�n��0��UCǔv2Z\��ѽ�sOXTߌ�����:��#�"&c���@���<���" ��VJ���I^���h�*@K9c���ȃ�|�HEL��E<��bwxY����KW5D	�Ҝ�N[�p�YE�)�*^�'[�i�`�DYV�;L��8$g�Ӗ�!�����7�Aѭ5mY��klRk�"�&Q�S��t[��CE_&��U�%=�k5^�*�2Nz��8��t�䎗܇�� c�̧x-|�X� ^�]���8-w8l
t_��
L�>�k�HON�"7/��@���e����6���)&�{�����r`�h3�D�O������|ʀp�Y�e>�k�M�B�4i�܋* �R�bw�RHF�O�JՏqT5DrLj[^�ȷ��T}�5Db��g_���lt�L�nRR4���ju�d=.a�(*vk��<��LiŬ!Nj^�H��m6ü���PT���jg�<f�̓(*����vf��K������"\�bp��xm�|��,F��Ȫ!��+�O�	ʓa�t2�5DrܤvK��L@�9���������o�y�:�Ը�w�݌�8�!l��H2QJT�v�R�<���j@����E�k��.��X�9Y�j�D�Q�;���xG �m��D�Q�;wd�$��i�)��� ^wI��R�g�@UC����Yi��js!��8�95��D���F��Ǫ!���=D��ѥVq<7�,'�	��4FT�7��q�bGg�K6XC��Rw�+H���ӂ�j��B�F�����
k B�R�Z�!;�GgQT{f�H��Ғ���8�U�47g3��,NoF���D)R�Z�77��i�X5D���J[\�:�Ũ�UC � )�g=se2��L�(;*u��������D����d�B��D��]Ȕ�P5IѻZ��G: ��a��׸$��JH��ig��|�Ӹ�� �;���8�:�M�ť4�T�>G$(iV���VH�cK�C�Y��;��!�P@x�����.#)���̴���3&Q�]�vl���C:��)�UC$�Hm��-��Ry�q*Z5D�L�_��^z���be�2�j�D�Ow[:�g+������x���RW#K�%Yp�.��%J�K�Vt|Q�NN�����H��wN�����UC�����1;U�eah��.���j\tG�Xj�5 �t��%O�E��(��U ��Rw�\�{v�����S\�.�3�B��U5�o���@o$y�לG_5���=J�B�6���!�cN����A�SD�-NW%ʈKW��
�_U�B����kԳzcs�M�y��@��ZM�$����̜��y#�2����x?t������O��������ŝ�)�sx]+�r�M��YC �@m���ބSlN���!�aN��_���UC˔ͽu7݇������|��\O��V��7�u�����nq�6����^~�4h:o��mS^�4�Y�F<Wt`Du�5O�ë?�A��͉�Fi��^�K�9	eH��Aw�|<䛏n��inQ��f#]�M�YC �Z���3���Rkd�Eu�DN<�#��ǚpH!E�-j����}�*4��_���+\�Hi-�bq,��(�����@�BT�sR�(��Y�e
[�K�H�;�ĥN7A��@�}�Һ<Ũ ܱ��禶��� ����paHi�t1:,YC ����͙�W���N���[s�� ���8e��tY=�%j1i�ף1�������N��UP>�3�
9o@�+U��T�6�7�Sy���{ZA��E~h�O;�Ws֧��E:JJ���P���c0���(jg�[����2E�#�ߍt��{�!���l�ߋE^� �r5Wp�D�����_��i��y�갡[�++/ba�����=�A�5��^�eV7�^``l�P�m��!�J�lŦ��K���Ϝ:�xI�f��;�a�3�HuBC1�/��@3]�Swp��^���I��t�$6�����<L' �   8LgHb�ڂ�E/#�j �^��,'_S5�.�d�KL�B��W,�),�G3'։M�|4l���u���5�%���>��i�c��ܜh�[m��w�8�Tg�E�&�T�����
�4N�xjQMr���w���ǟ�ɟ�������             x�Խ뒪ʷ'�y�DwD��"vՄ�^��EE���**^���~�s^���d*ZX�sV��'b�\U��1�6�/G�����U��6�_MV����4�G�&Zk�	�dna 䏿��?������X�ay��Zg���F�M��@������_���@�/+rˢ���/�����v��@P��%�ZT�8?�����C>��J}���`��7�K��9@�X��R�^7��%�����A�,NRL��U��
J�"d�`t�5U|���vW��v'�.�<=bԍ���s!�ο,d8������F�9&��v�q�fO [�����*1X���n!Smwb/��o���}�Hj_T��bP�,N�b��J�]Sc
ŵ7�V�'�(�'�WḙM�ǉ���`�m�>x�K(�� �h9@�3Stܹ�{EԆ&�1]�aygd��j��ڄ�8���/=��E��������[!y�eX�mT�UJ$nN�A�C�Ƙn�zq1����E�|pֽ�q5��-)�>{	�G(�F0i4(%>�R0r旱+A�B�� �I�t��P�2�=$Wu]R�<|�U����
_��V��^i�[�kX�Qjh�h~_���_Y��o� ��;��D���a�<�0$�����p7V�`s�Xg~�/�ȷƛD���L�I�S/�+u>���ēuG�U�ig��Ae�/f�>�>�����������+��۠f���n��&��:�a���p>N���)��9{.������P�R� )7�ݹ�וg���b���&xY�������%l�k em���mo�����?����Epx�ů��_PB�|�wvB���ܞ3���&�>�W�E�/���h�]D�e���o8�F��E�Vm���ޞ^�j�Z-b�T�uA	G�
�rK@�@��V��S��q\����W�kuA�G��e���ň�&U��XE��p�)�M��9W�Z�_jR݀O�DMR[yE�br]l�%]N$ �d�������U��9��K�"v���Zp�F�\'͚��̻Ʊ?8r����b�I��F}M���~�}���yI�6��pq��H*��s���5�s��G�r���S'\���ZO�p�9�T�E��v��<��c���x(�!*�K����8���w`YW�1�ΰԱ�iwѩ���/g\Q�*��_hWYΙ��Mt��Dpo$�F���Ȅ=��rk�������X�.LHB�`c�a�#J(�Qh�Q���_�#�_� p�	\?�O~�8��	�-ݐ�P�Z����kHG����7��t�r^f�)�ж��5Bu�~�v�=S���}�M^$�F�i���y��k�ᦦ�U�ЂNCe�����Ϫ�A����+�:�>�6���o?�`�#ijC~Z��m����v �S4����n��v�E�{I�_P�fH����J��p�shq��^� ����[q��͘�>�T��3 �4Z�x��5�:p�v��3 :��׉3�6H���~� �0t��ir���ߎP���gGI�
Fr��q�<�ox1(��Rv���~+L�:�Ӊ4'�J�.e���e0o4�^�hPzl���WG{�v՚��	H�7�3�BZ�:B����_����,�Rpʅ��	QnD̚�h.�*S~���90�)U��1\M�6�BY�|��� ���=�z#@-GB;+EĚ��dCУ�_��:ײ�$���!h�NbsP���s�g����]#	̚l��6�뱩��~�eí'�^kUS���W�csu��E�Y�w�R�S�=Q/���5�����7iV�l��M��K���������2],��L]DR ��ʆJ�*݆���j-[���{�>�!O�P �5�8�� ��˅z*�*F����\����L�ŭ�1�mj��L�[za�	]5�A���o$�F�b�#1�IT��]��AYXH[��O�<lwh�!���>LshQ�b���Bs,$�O^y(4�#�`�Ph��o��~G�Xa$2��b���&�'jY��B�Qf'��&4#�P/��h90)c/n.�Pl�@������~ʈ6��ª�`�Y���-��'�����GA2t$F�5.��[��W���:c����ˍ�����������v��+'|��(9�ro�|��4Zg(�����X{���p���x�@.߃�<~��C0�X�և+a.�I�bt9�ؔ�n�4��C �#��`9��(���A�xJ�̂���ŒO��k�#HS9�=�*˸W��˥P$�N΍��Yzƺqz�pŹGXCZ�{��y�R��h�j�^�S��Qi��s���M@��tEi�OVL�U��Q4���E�0-���N��Ij��4Z���P+�jMf�z�T�"�5YJ���r�*�	�0�Q������,eTe���T�AMN^��AM!�`��J�4ZP��G��C��`��zQҰV�U���Jhj�B����20Uk��.��
��*R��9C��a�M)�?)���γ�!Ll*���j/��z��C@C��J���Vw�o�����e�c�v�:yPK�94��;�z�%��*lJ��_+m߁sH������j�n{�����L.ˋ��a��#U�76*��w_'�é1��c�۪ʺ�U��Ӥ2ک�`�[,�7�e9�\�p���=䖖p�M����('0�� �y̋q��͇���3|(�lEA���"&�B�t�qU��a�u���}���>͋R_����	�� �p�F��V�N�"�J��%\��_�_�K�`F��Ko���.�i2����l�q������rȚ7�b0�ij:�7��	�O���)���N���9P��GCZ�i���pL@p�rx��b�ݝ�C���<�@�����c�$`q�!�\�v�yḓ�kRzeJuN����V��u���&a]t�S�#�<9J�r��Y����B�,k�����˘&��8���娭�B�PG���EB��g����8a�sG�#4{~���Wr?��j��dx02�	��n;]�Y�ނ���W�̔�*�q�L��7�'eִJ^�N6���n�}!?��^GpO��-R+*�`b̞B>�7O�� O��3�_�8V��gVi��2�"e�f"lN�2_��NH��#LkX���d���HB���[mkuP�.Q`W�i᤬�`��{#q3��4(E�#�jo�f���[��������u�p��w#6��Ձ��wCn�c
rW�rP����iq+K��z�h�7O���#�+-G���(���qǫ��j��A��F��o��Z$m�����?���H_ͷ�-�PdG�t2�ry5��Rżq�\�~c.�67���q~��.��B�����OA;��q��Aro�FˑD�I��l�jc *���wt�����+�A���v�L�;��p�:Z�����B��+}|6ʐ�st܁��[���F�����5^0i�y��o�b�:���$ҁ�Wk�k�Go�;��3�(���	ΆR�.��}>W1M���G�l�ε��2�0+��Xql�#�$���r}ҟ�6EM,���*�/���Gٖ�	��Ev���� �,���Jw��Y��p��J�^�9�/�;�N��p���ٶ;؏�s[���Z��fY��D�{#�4Z�{X�}�� ���sa�3)��5~��jpi]�����,k���!J��fU����.��S���/��$x�Y�Ǜ��h�G�" +�Skb�y�L��0�QO�oN��t[%i�������vth�k=wo���F�b.����T�]
P����($6++:t(n�	\ٰ��(<����vj|��ݢ�;��jb��߼�x��b���ʝ�뾮��b��{a"m6��wmy�ͅr۪����*�]�S|l�D;�o͡�j�hϙ���,�&E�N���s�\[u���H����}n�[r`7X���h�B6�#ؠ��    ��r���1����!<����J�$�^]��F8m�}pU�9��|Q�;�k۾3���6�^G�$���g���<�y8g����?Ms���_	77tgLqbP����r�o<��#7Վޔ6EWo6��6d3C�r<��4]|��%��N��\1�U7Z�M�At����5A�<�[˟ᇵ��R������#�Va��7�]����տ����	Uȥg9hI��Rw��|��T�P�83+Z����a�C%j�p���v����`:���%&����>[XsX<���&cۿ������\���V�.�5�1>�����"M���^D;��
���K�<K�߉��ļ9�[Fe�4��3��D��3�j�+Gɞ��2�=�e/�/w��N�� � �� ���O�$B���Kpj��B&�ZF�ǅ�u�Є�w�-(̙y^Z�-j$����K��j��J���ٴ{kE�maP?
o8�F���d�ZE+9��VO�G�7D��Cg��0�`�O��}'�����W 	G_dJ���E'Th�����p�������>����U[p-#� Li�lT�Z5�8�2���+�߮�,k��4�q� �`��f�ˣa��W�VhWF݅J�j������ �F������5*TN��� Y���q�G�]���eAt0Ł��6���&q9.��U�hCPx�ag���)m��c_�����a����o�=Y����..m[�':�W�[Z�~k�F��xg���u��}��P��<#	Z�'Uu��>'��@�@����'���Z�q���P���v�	~���~ ٫A U�Bn���hPz|���qI�n>�R��8Ks���
o$Ǜ'�n
ڼ/��nq%�ٞ�*��%	��e�ԝ�#^ �S���}�-G�L�b��%�o�$E�\t����jP� ��2�r7�2�@5�R�����R8;_�j6ի�A�*��H�l�W���nk	Z�� ���&����VF��!v�U��U�^���D]����������g(*�Y�6Hѷ<��4��ܫ��\
{_W²����?`��fY*�Q��H����V�<��BYP1���a��sK;�x^n$m7�w�
1�*�N�)�P-y�b@��l�08jS\-G����H݇���[���cURZ�V���D�r�zN7��r*���3>����WUI]CHxѦ}��%Lo�S�j{�]���;���=����U�TF��c��{�<yL����h9@�0b����ܭ#�<�	FL�	��6Kn:�N*�x�~yϋ���h�����Z�V����P$�_l�[Z�{��+��&~\^���i6����5'�b�dv��Z����n{��u�鬹���Pڬ{����S`1H�A����w��u�ک0����7���)�JU�	%釳�&���gO~�����̛@�YzBĚ\����˯�)���i
�2��H+ql���C�8�����c��r����G�b�ᖖH�6��9/�r�nMn�ݔ�XPM�O���Įh���v��뇊<�и�����TW�.G̷w7��Y�/��ާ���I�eX�%%�B��a�*�(,�g�YO^�H�r�&��ó�O�0��� ��Y�PsЍ�P�����TH�4aN�E2�&p������W��!�+��x&)�U0���p�|�(��,���-
��rp�O;�⋾�G�{q%��B��V��E�{����jg�.���W w`8��Q~6������E�kJ��Ԇk��&*�0��_a?j����E}p#�e��sjk�D��n23k/���߷�}=�[�A�j��S�l� �oH��;}��� �F�}�fG`�
��&�L}�}�,�Q����i�
���<T��S��T���i��u�9)e�2z�)��f�K�֝ـ3|2�g��L��-X*�����>�z�������A�G�}pa�h�	F���\#�ޛ�<G'8wE4�~z����!�r�U�W�*Α��l{�zE�?��8��֮����lw#X� I��-�4�dq5G#5�Lp� � ��J��WO匯~��2 X��I&�P�ؔ�c2ǘC����ڂ�=~�l{���v&C1<��-~�ы�iF��P��vy�7���Ӵ�3�W�H���s($��OC��eQj�ء�~-=�O�����"�`�K*~.F�_��5��6��ZY-E����V���n'(]Q��f~~����GB	Z���.	�@�[d ]n����K4� �����f¦��22D
#9'(�l��Qu	�7����9S�Y��T���FC5h��������u�+f�S.�f���`�Airz�\V��d �X!<�Z���^��)����ǧ�G2\|�ѩa�j�= K��5{�^](���w��v�~�ߩ#gҴO�@o�(2	�yIF8(u��^i9�Hi�e����q�|�&^u��'ߨt,���PK���W��l`u{0�fC:rs�-�ݚ�Z��v�OIR\O:}u�IN�.?0�dL�-G���!M�
�l��H'YG�]����8iV갺���tM�P=x�C��?�[�T]�
]f������	<��k�D��GLG��p�{����ጐ�C���_��TO}���E�cg�ĥ����ly�(ն����N�o4�BC��Ll�{�MrY.	�<ɑ�`�\�dLam��?��cΦ��tG�[��ƵF�<yl\��/͉g�I8������c�O�PE���A�E�@!_�U� #i�l����cK��Dc
��Vl�қ�bg\���*π�p�zF"Z�iAeη��r>�v�
�p6>��v19y�8��,W�\N~�.��\��.�h$�D���o��)-������kC��cY_���e�V�J��J���˽��E��r���[w���L� g�x���Pa��_�R�A�`�����f�B)�e)6�%�D�"W�J�`�������Z�aٟ|U�|@m������ŧ�P����t��)̡AP���"�,tmi�j����ϟ�������_��P5�8z7�0w�z�+u��ik<�&>S������gN�pN�Z�� �T?�`�/^����@�m�w�����F�>z|���x@Gށ����k0*����O o�Iy6_X��l�4O\�4ލ���c���X��>7,��'�7�x��4Z���U�<��c�O�":$F>6�������b2|gL�cAU��Hz�3U}������Q[�d[LT���Jdq3OlԵ�	wH�l�k�x͓�
^�U�QC�K��l��J$o�]i������;���`�o�C�����/��(v2�����N�t|��us��7��v`-|c����o��EVkּ}�V{�\p�*Yc�gp+��zT-�����H�fh:����C}�����d�󚓛	ǚCE���/�E�=�mh�:��r��͵uc��fF��7�d�����)�RޙDg7h��-Ǎ�r��>��r����YX�)���u�w� ��4�9����.Ţ#���� ��/����hTcoP�	�����v�����������E�u��֍*�n8�	�܊���f;	��@��Ʊ`��̣�Vg���α�!�o���%�ǅ���F0���<eN�F�͗���ޖN���I�^s�d�w[�zI��1�m(8��%3����K��䚪DA(u�6�KIRJ�����p4��dt�7&��𾠜o��(����U�B7���\�˞�"�:~H�	��p�@���D-��/-�~�ඬҒ��p��Vj\)�a_ڣ�4�#.��^G]�p��6;J����m�ǒ�f�h�Ľs��,�����o>�#�����鳥&�P�:��srZ��v͊�ܵ��-)�h��8;��l���9e�|��ɳ�-G�����@�2R���`�
�:�$��/р�:� r�|K�r��TF>�HUC�*g>�~G����:���ch���RE(pf� Ăރf9���ntq'��ޛ��r�u�    ��-�1�8��gʘ�	�d�G���l\Zv��[�,���I���W��l���3x"�E��#A���#�g���
�K ԇ�>��B�k�{�Լ��� OI�d�����\v<����*�e��E�k6�w2^!��y���I�� �fOه�)��/�"Y|���	��5�YM�Z[���
��`�L�R���KY�i�Q�"W?m��g�S2=E4���'NK�q����#m��J����ԧI"�������yi�d�1�8Ykb��r��}N/{�ц;��2�m	f�Q�ܡ�O�)d[|äT�+�O�� �����O �G�eI�E�M_�G��f���_��yu��N�JI��v%�M��ʙ��~�oP�h�-�mR3����d�	�e�D��Jg��f~M�y��O��ʄS���	���h�/� �/��"S�k��ykE��ò!�������ָ�ե�\i��|e2).����t�J�*
�^�o{�/:ĿX�P�Q�&U�Dᢡd~���8�q8����i"�&e�������d\�
�/��:�J���u-�Z�}�?f��� c:y�,A�:T��p����w����d�߉��.��D�>���k�~�-�cO��ep�����s��a	�z�ll�Zͱ�'9q�4+|�37G�9�g�^���zZ�Xؔ�����9i��$���i�`�z1���
Ea�K�W	X�"_���x�o�X��K��՗�66���!l��,��i���
-e�R}��%�m�5�?0�lp	Z�i��Po<֒�G�,�oam稘(rM�#L�k��8j��/���܂�D����B���	li������q���r�;��c#q��쌶���\&�R�c�2孫v��a"7�.�@��v������_R�S����z^��o2�N����m�YUa�Y�W�9ܔ��Z[U��,S�eB�1�ґ�q�_�E��2:��2u�:�`�Ed�V�1P I��=�������T����&(g�~���^��A���uwqԺ��q�T㋄�ϗ|��`�(��;��
qk��~^/@�Z��k�,������F���"!�!|��2Z��<���r�7%��@aN��:��+&C�f�#ǚ�V{��E���_��iG�0m��Ƒ(��t|r��W��\��˕��J'GO��>:ֻ��v][�iG1����˓�/���;��ő�q�Dx-w�Ooi�>M�&pV�
���M*}�O>X�x��c;&?�q8y��6r����J�8yx�Y!�Y��UƳ��
�������AH%�7�A6��	�/Ki������D.�X�o �("�9��+�a7���\
i��9O�p�9-x'iiK�<9fq^#�u�/���W�Il��.Ad��PT�ʿ�\-G�Tg�?�8��zQtCkU���	�4Nm�����I�M��΃Z+�hyx���5Z;�l�5�̀)���]��Q��.59@��gI�b�6(�(�x*�S�7�镖��cBwL�r�Q$�~��1	�3�ƚ*�Z���X�
�r�&zw�����ж�R��$}�ani9 �;��=�Z3�ס�7��т�!��a�;�9�XpQ��1D��.y�RK�Y���J]F>Hx�@�%v�}����|͔�
n�OD�9�Oӆ����7m��e�N2�RZ�4����l�ǫj���Ǽ\-ȴ�:7�O���T�-��c?E��:	�ޟ�Wy\�7�Z�YtO�A���K��E����z�t����V��tt�'������F"�~�3�����J'�.A5��NC� �EY�0C�1R����Z����������z��
ٗn�KӦ���pko8����`�������Nk�Ԯ��:������̧�r�~EF��g	!p��z�~�LE���(x�Kai7O�aO�����)@���?��@��)6�t"a(J�M @�5�Z�5�Q�x��n�=6q�5�����~�MF��y�eˑ�Ť�F�7�L�� ��j��@�V��wr��m�;;��q(M�nDsx�G���p���\�}�69
}��׀�����?�`�͝���]m,/��d�vJ�6|A�q�5,�u�é�>� f!��x#�4Z��H!T��Ꮈٺ���2>	rey�{�/� Q`��:�KԞ���*I\��kQk��Mg<�����s����H(ܜ������Q}�T��꜠� �o���u^�v��P^��P�:'����ޥ���w>}�:����ЂY0����*z��Q'���Y���Ρ8 �Cst��K�!��v���i�V|�~<I�Z�3�6
Km��ޢc�k�+W�ߗ�gP�C .�������U!�=�2��)-;��℃3�1�r��M�2o�Ų���-��D��3��lA�|k���Be^�ev�D�Lr���Pt��!p��sE����n�`��P4����,0���\�ޕ���=���Ω1P$��6���::^����c<C��{o��{.`x���q
`�q���	�C�,u�t�Smb=:͗��R���\^���C�.d̦�T(�.�����9�\�Ì�n�W=2��l�l�e9.�i�Q��f�faQ%zL	y�����z�!VE����)��U:�,��E\�csID&AˑD*Ď0���5����
S� ����w0<�>���=G1ŉjf�?����}����a�e����z�c��N�4�v��`I(Ƭs�,ޅ������H�f-�?�7q��dV��+#��>��� ��x������+�P���#�&<t m���3��g����ýv���wV�%�Ѯ�H�l���n0� �w��L���}QQ ,�����(�*M�]�9�0��E:�k�ҕΠ��'wGwQfA�fTN{�B=�Fˡ�gw�an��4u��~��ݒ;�Ѐ�i�K	Ws���Q33��@�8s6��y{��5I�;ù� �����'Q�y��XVN,�a"\�-�&����d�u���z�A]x������f/�x�s��|�X��&-{
U��w(���H�5��[�-WgE�ɘ�5³��9X��T�0��+f��\oK�!��}g(	�-�W�e�|� ͂jx~�2��t����K_��h�|�Mt����G��8��#���.�P	t���
��L��r���h�m�UR5�W+��OxW1&(�N{%�ȃ�"���XF��>53�@�v�](�@-G�W�<<�֔dxZ�NzDzu)L,�����_/�髭?����x�����o����n6^����,�3�������r�����E���E#���KT�2	�xqF��e�8�̭�n����<�^�P�/��G�������AN��8\����a�I�;�a��{��;��U�m	,.�תQ�/#�e��������#�]9��:��WD����H/|���!ʇB���t-����~
��G���D@�p�@6d<����~�Oe0�(��E��߬F6��6,8�gK	���F������Tɟ6��2~s}�l>3��/*�--G��.9��i(H���x �6~KyA����]Ӳ����V\>��g�z�����J7t����b�� <��}��AS�%A'z����C�R�F�[�pҐ�CwV�(��ն�q$�W�l���l�B�nТ�4��[�1�=&���:(;������5�-C�jny�G��v<�p���e#�����@)A�k����@�͍f���mlO��۫%E�����<��?[�os��b>`&=�[��h���$��^�x2\!$3�R�y�m�V{�Vf{y5 
�٨��(����U ��N�#�Y�8/��J�o8�F�QLRU��n�p�zg��B�����>|�FžLQ��3e�����hJ�ԛ},T���4Ֆe(��)l-G�ɡ�Zn����[��6u��\�E��_����^`�P%�e�l�ӗXxg������Ug�$�"I6�$�~R�ྍ�n���S҃Ke��h㟿LQ_$��Eg�į��-w��q�d,X�ƙ��;^�    Q  +FP�����n��3wk���e�����։�(Xnx��,=��}�$�A������.��ı2��#W�����^֨-[`ʨ	����Sfו��"K�4��MS�_b5��r ���ބ�@��{~8	eZ��f�a��� P$s���f�O��ꂓfi��5�c]�	�oVOb����sxĐ�Ծ:n�-9�˽ .��7�Ѡ��<���Tu\۟Y�YKw���;�r8��^�Pc�(�]Go�'��N�Չ�6�.9M@T:�î1T���p�������r���k2��	�OU�+��F0Z�-��I�C�\uI��ި54���� �@Pm>WC��Pϰ����{d~����(Β�MFJꝦ��4o�r���	�8�ԉ6W�˭ڬ���\�$-�mvO^�5���]��8��i�`҆>�oe2J����P��n�L����%V�QԚ8t��|��*?ñD|D}�3K[���l��s�G7	]�����C����LT�<M����)�����K>׬Ì�����.y��7�� �8p����8��Ʌ���H/��h�,	�#׷7RE/���`(uwV��X���_h�զ�+ly2c4��ߍ�	Fv��(>�\�5�/�fY���g5�w���~l��c{�O�k��!��_8����W�2]��[{
ř@׏����Dk�H^�/��Eпh����Q^|�0��s�����>B3��%��--G��9����lt��9QX<;؆^t�9��_�-;޵&���2k�C�R�3��w��mh����]�A��P����OW%Vh/8����?�L�����u���ƴVz��=���~#���#��w�i�@��񖖋C��k\	e�Oh��������	*�e~)����)��q{w�H|R�ogf/�������*h��U�x8�����ue~������2+h���}K��drVħ)U;�;�X�O���6��$i~.6��3�*SY���#Ƀ_�Q�/�s�k��	+�P�8w~��ߞF�ҚLӉ}�e�r4�:�Э��y:9��7�'.�f�/�X�lG���5?�^d)���q%:��/�{��?�O+݅��i9�NN��v	��kx�Z؄ߛP�Ѐ|��_�A�_"�^|g�}�9�G/O̩/��O�:�N����I1��q��S��qKz�!����vK��7�'��G�v����>D-�Eu�=]�L����g&J���@S%�Z�X<���?��{v�?kS��D�o4�F��7�7�5ۢs��v9؆1�D-� 0��[����_�J"r���..�:^����=�����NX%�V�?H��l|}{Uߴ�A˲�^]�K��h�����-���%��Ku��' �����������e���:���cn�xPwb���e�r������/������}���g�ǧe�d[�țd��昖c�4yW?����*��fgy�κ� qb�gn��L���׬��X���zU����?��L#��$����nd���O��h���В�5���}q?f�5KP����r����a-&���"�?;�������=�v��5��#�����F� ���.�����+�����6f���h���.�|,���őG}o�=G�+b���MYY-�:�����t�O_C�^0�����q@@ӡ�4�ڎ��]���(����P��_�����mj��i�Ό@Y3��^,XԞ7�R���R��cI��M��rL��$V)���z�׳,h-eU�ʰ�j���-�0e�8��~�C�'`���u��ʷ���7e� �d��2�iR5
�Faw�Jch���5��ݮ-�*�w�2�Q��W�n�p(G�
nA���)�5�-��d@_�9�N2�@���p,C�t�.���e��j����z�.�Jǣ��ք�OO���P�l&*�t2_ x���J���n��c��&Ba,��f����KT�s�j���.*����8����C__֨~���X��Si�+���~���ĘU"���R�?�X�i]q�BaK.7�.^0f���*ҊP2#�m���?���7v[8���3��bIo	W-���RU+��uw�W��y�򎿚���u��O��B�(��hbX�>E�;H��9�pfq�\S8�[0���
��k%w���p��Jn2D�9I����|�h9OYK�x����/�?���w��;��@ǁr�G�_7��W��^�$7*_�:�E�_w�x�_/.J[�SZI�N���6��.Cd�x�(�ͤ�rLZ�"X�(T�(�v�k^tY�6�0��g/a��B�s>J�~΄^��&�V\�W�?��rg�s�<�{���tR,��lࢴ���T��P�v��2������5VYo%p:��Cw�c�ik���U�]Q˅Uw�o���@�O��~�`�F"�8�|��C���Xa�Z���jr�5�0��$��9��)T�3PR.d?Z��A�?���&�(@�Z��mW,���aR��+��T���ͽ��O̶:F��.��ni96m��.k����01n�(����{�8
�[�N�؟Qv�O��XY�\o�ͨՖH�*�f���q�NOn=B��+�Sɸ.�"��>E��r,��9�.W�b;�rC>�.Jy\/�{B�o��j	�lz� ��r�����\��E�#�G���.�9��Q>$���@{�{�[+ʯu��d��qQ(��v�0*�&�'�Ÿ;mM){N�[���[o�ԅO*y�-A˥��sY��(h!J!�T�>�گ���8t,\~�T48C�/���e����)*sh����<L�]% �z��u�rhl[%�qm��>1ّSm�
#BU���2ħ��ި��&ډ�p�)�aT<�e�!E5��yf{�Ko�A�8�D����X`j���w���tW0����7�������R�׳���τZ�xІ�2G�x-���F��e���k��D ׭7�6�5����~m���q�MF���II��Xz'��t]:X:s�����`U��<[W&�S��s���-ǆ8Nq�o�!>����[Q����9+�����������j5Yد��2"��#�~k�밭��f��9�ju�n���o�,'��o��ߘǝg��A&��c��T�~櫂x�]4{ny�rn�蝗�D�����/_<�����ʓc�6O���Uk�4������X���k_rJk��>�i���A"��K��964�_�Z/�@�kno��~"���8��|�D� ����/�4!�rմYƜ�㽂o�Co�zgT�A5ߦ�%u�E�4K�]��E�>���J���4�9�,W�H��
�T���F+�%Ԙ(�œ}��ð,O�\�H���6�˰&]JÉ�>.�b�C����-|2��AG�D�U�����e���^�t(m�F3�}����
�*���P�7�I��%��:M����RnrV~O���&X�H��g�F�(��Y����W��V���Z���b���K���iP�q��T'" �N���$U9�aJ��0�T���" ��2n��;*�G�@�0L��.�Բ��)����0'S~7���h�@��MW�z[Q'��V�[�Z-s��ц�4Z���p��5�ŵ�%��C�H�?v����j$�1:s�%�_GP�����!9���El��ef��d�d��KаY�^&�
�RK��؛��*� K>Ǯ�<h�;��;�A?�+�%i�--Ǒ�4m�C�s�����H�pNF)�5�.�FH�J(�eKlUo״�wY^ 
!FS
��7��r�6g_G� X�cb�/���4E���u�$��4�ͼM����ԼVv�`U�~� ��t�XdI�|�d�Z���V՚j��u�@;�((4��)�P�҆VCr��n�EYǢY �,�0��BVF�Bkr_��v|v����	��d���a_V�?0� �S$E�������
����js����I�#��܂�K�ͻ�C{_Q=�(:����z�>�k��-]{\�-��r\Z(�Vr���]�&t��?��p$C�s�~��"%�J��q�(����!�k7�=e\�聽hxr��[����    M�{��L�qi�=u����gr������}��)��n���]`3�wB�{�'\��ls��O�����Mx�ҕn�Y���r�+�űgd��}�#���i��6�f�M�H1�=+D�e��C@���WV��cu��7
S��VbvQG<��i;8���L� �Xs�=8 {��9��;�+Z.0Ub����[�Z����Ɍ��p��r�I�b����q�r�G�M�a��i^�j��o�j咫kr�#�]pܓ��_�8��Z{���k�*fI�|9�C8���]H��i���d�Э����qGR�r��?��|�~pI�C�a}��H'r�t�j���J�$�%//�3�ꬖ��rH	�V�3-ǧ��<�I$y&>�U< ��ć���ti��;����:��V�';��4�/ܣRu7Ա�<�+���{^CZ�O��ue/���=M�,��,�ܳ�IW�f��)O�>�M!�NcN�N�W��zxR�~{�#��Y��S�.(A�A�O�7�	�L����y
�T��~Tkiu�����̓w6�o	FYe��4��:���"l6��3#���]pbZ�OSL���.�2���p2�\A}���Qr�ô�w6�����
��:e�m�]���jz�.(�f-i����t
�t�pjJ�32�&�I�,A�(o�������f���lw�r� v,o �;W�VZ�ޞ��uG묞a���u-ǧ�J���?>a�A�X�����HEdӕ���f�'U7����]�Q�o��Z�g�n3�@�ӆ����������-ǧy�)�z�5t�I�?N��0�y�8~��6���ȑ���Ě6F�#0������ji+��z�.�m��g�����4q������T����~
�<GBu��w���5�ϛ�^n�}1�������n��W'r��%Н/+s�8�<�9��9��<͵"�E���ǧ���s~]�`KLҙm�D[��k�$	z`@i���4�BtV[�S{�upͨwCˡ�֓z�R���?H���G=��}o�IX���㫉�)To9�uqٟ�֓��j�U7Z��vs���u�A�t*%������0 (�{��ab��
�r�+�����'���v���1S���h�F�ЫҒ�T�9���� �����Zc
���+E2P����LNX��&���X����cG�Ur��5oH��?��>�c� {�񧓚R
{��$I�$4����^������,�c͜A�o�,���`f����n������6�C�X��9�֪?�������B)UYE3���������Ëo�?8^iN�� �����?�+�,*�'�C��e����fj7��8w⤻l���)H�u�QF��������4=����}<Q.���_Otc2��Z�d�P����s�Xy~��[ڨM�ե&Md%sW!��~�i��4���0.��x��I�a���O����C�	���H�I#p�>:�'y��d-O�%�������F�<��Aq��$O��
N6
7<%��4P-z���ɗ;�*�ю?�X��v�򴱋��gx"�5��y
u�k$��G7��E�ϙ��.q��}�EptGF7�_>$����͞������ǒ�I���%��4QHizBҼ٢�U��+�m�я�To���SG;5���Xk�"w�z �5��^o���r�T*�k��WX"9<�4���F��ņ�%} ��ej!�QJѫxWH��x���.�W��f_i��d�Ƴ�I��7�Svg�WLI?! �N�AA�ZjE��V�c������x?X�b� F��_$C�F^�NP.4N�_p�xE���2a6K�>�������[��^���x�iu���UZK~��e�x�2�A�����~Fxo�'������H���:>���دN7�V.�a�&�s�g�B5��<B�T��p��0+���f�|6��ḙ��Ұ���Ү_RZ�&�if���E�27G�W���pv�5�$��8�j:�脛5�}28��d�ه
�x<4�抭��.`#��b��
�����0��.�i4���0��r�}�������U�e��.2B��:?�$��P�י��-�dsX��2�녫���ۋu��.�z�5_�F�|�k�I���=�������r�G�9�(�kM�̀�U7+8�ǧ����ΰ֣u�h���t���L��LW�\��$鱲�!�I�����:m֪�u�ö��U�U#��uٙ1Ŷ�.Un����3���2��,&�!�3�X ���rq��h3���	��zUE�4�Ҳ6h��B+�%Y^��I��.��rq�_��3r�\M	�0]uL��s���F���y��	�����;�d�\��MgR����XdkS�N��﷞a��ߍQD��be�m�랁�Q��`D��|�K]8k�4�O����f�,�D[��	Мv�����7��f���z�[�����ɍ��>=Ǌ����z9 ��Zb�J�ʶ�E�%�7�J���
��������>�Z�����u��`BK��,%�pgj�Lv�:I��¸g�����4$6ۦZc��S�ɥѠ�S�ߒ��b'�6�ix/2�}�����!����I��@�i'pR`w� 
�M{�,w*�R��E���E/�:�x���:�*���:Ȓ8� ��a�hj��a,����j�v�l����tx�uN(y5K���ZR���z�%���)�:w�<^�j�g���p�Q�c�d>�
7UW��r�K5���}E9Y���NEbG"�^G�R&�f�����!:K�	�#CSŧ����"�A���!������:�A	��m�bx;ˣ$��)���M�Xk����w��3��s��bE{����jwN^y� e���ח:�����M�W���b�*��<i$#?w�}=�KK�:�F�+|!�#MeWm�`��j�C�x����H��X��Tଅ%F��&��Q���{
���0��^������VVZ��w ����{NH�K3���b�o�笩(��"�q ���?��m�o��8"��I���>Vb����&��Kk�E�D�[���_5k�ҫzI��p�K���S~���Xm-�*�������[���.+�g����໥�>J�b
V0�F�;�t{3
�.ݑQ����#^� ����5�N%�P��6jSe2�
SO��z��}`�ͽ����*���P �;t!�A��,�++��I7<&<^�x
����s�"�/g�ugȜTwFI���T��ZT��X���hv��*?�_�O�A~��+[�����[V#�'�xܝ�+6�{�h���7G͞?^�MJ��tF��R���w����O�b:m��ȤoC��#�4���K�V��W�;���7�Wlt���,����T�1m/%e3^㇞V�֚\�d�鴅���<��7�W	d6MeO0UU�;6!�#��$���`�0�����U��I���h`��թ3��|���j�n�gX�&d��z��ee�'����9��K��D��"��A��"��v52�r�$SEz��_���yҢ$��̬�_�� kU�q��}Ǚ�~<rC�S ~���MK�av Df�Y7�ó6�Xs�y&5缠;FuD>��të�+��&�rXt��RJ�#dI]�k����}џx1)�ȯJ0A��k3g�K��u�Փt��C�%��X5�ʸo�S����vHFz-Ԩ���E�h��*�ʕfi�-���������#kZb��YW���1PqN�7|!��;8m����z��X����*`�J�+��swC(�Ycp`��"�lZb���]t�H�����P�-�F��o�mF_
m��l��ڝ+��֪Բ�f����~��'�4Y��(
wOغ�!e7imhLFL`�YGnyJ2��>U�:�p�4cd��Q�B��+7��Y� �ϕp�ߍ��q���6�b��5/�Iۣ�0��$�:��f1���ﷆܺ5m��ƔZ�=����BAl>�nxM�?  �	{lN���6-��ػp�c�U��:�k�$�    ��Vq%��v:F�t�uk�kb\��\����eR��V�0"��#k[⍵Qfx��Ϊ��s�#f*i�,P���9�mn�=K�������K���Q���x�ξMϜ�x i�*a�������r�̟��j?ި�`������^��~����w@D"@�*�\.O�gm�ڽs!?9�I�)�(3I�@�XE�͑���xH���[�4��$�N�����Jfgz<���*q��d4����,�ɷ�'�*�m�$��'h]��1����p�8��IJst#��@(zdYK����{gfA>Wmr�6s>��/(�1t���f�����}j��=�N����|�2$e�p���Y����MT޽cF����X}���n �u��WO����Z��Bv7(�K��b�����OWg���K��ַX[n����a�ꧦ̼�xa�W�2`���S�+��֫��m��+c�!��h_▴��'B���$��Z��H�e�2N�bA�I��������&��D�>W
��0�+��_��+����e���y�U���qs��x*33�/�nH���U_��ˀ-��<�2�%�\�9�`V}����?�i�k�9MK~�����G?Ag�wľIy�p�/�6%�)M;�����w�+�F�kB��;ܯԜ��\Fh"�}1<sd���E06%�vJǲHJ���F=L;�m�� ��h퐥�yǰ�|W�\ix�=�|W���g��`��tvи� k3�Y��Q]�|g�?�&�2�氥)|[��-p�$��2
f�
]!��B�����N�͈O���[ŠJ��.)i	$b���̏Wm����-cE�T|�PJ7M|4�z˷�)sEF��&p� �ms��3�G���r�4/1��]b�9�^�<��,rG�ƪ�8r��F?�H��W��/���)
䏪�S�ѯ/��28U�c�R��H�a��ݸ/����Tf�F�y�$��u�t�0T4P��-͈-g6�h���>�&��'. =	�f�"���(�K���lt�-��͠�vE�����T���O�zN���v�13!�:UE�=׆��t�4�����-�x<y	
�Z�l>y�GJ���ڻAWfKڒ�e��zX:=#��ƞ��7�����f|f4�������>t��K�e�C�wJ6I�K�hh�E�$�: �N��i�Pg�&�I��6p���~w�g4��ꏓA������$��=y���-W1�ѝ`-n�JG��H�YD�E���$�z�I컎�� �?(>��&�����^�y�.�"T�ġ�Sz��2m�F��U��!5&U�ao#Gj�[������֨����w|^��L�X~�յ����'�,�I,8���&�9����1}��]�[?�/�(���rװ���ﯶ�nI��0����� oU���J��"��� aѓ��LW�&�A���ča��&�85q���x���$'� ��<=�Ov4��4�'�Kvnxy����!����]������ \��z��:9�"I��\M�j����J��ڢ�YN�}��cm��O�F�j�6��v�F�iB]�o��/��R�y���RU�/��\��х��V�)ۆ^l�X(�E��'i���KW)Q����*�����D��|�;xր�5q���q�썱֋7bx���J4 �*�����>īW�ti����i�H�~ �K/%I�v�Am�B35F'ͣ�����O�jWڞ��w������#Q�IU��%Xn����,Mv��$����Y�K�K�D
�T��=�d�\4Q+!�.�)/;1�:��O6�O.��I��ĉ��rw��搯A�2�W�U����b%�#f`mJ+ǿ|%���n:���C�7IULA2j�����)���
-����Y<]KT�����ǻ N��W�@ܗW�0L�/��	
�Q5Zɂo&uDH��A�T�X��q���I����q����{�+Ae��ƙ��L{�$�1ʳ�E?O�n��U�1��*a�ܘW7��y����Bs%�s@��&Nםɽ�JD�[�@_Q�oh`*��ed�-�JpS�� �����u5�#����>�0�'��h����u�l_�(����ɂ���v{�N�MGcOM�u�%*�Z���_��E�#79Wߓ�?�'�_��zz����O�����0i-+��{)����!������3.Ӑ����9�X��S=9��~}8��:kD3}�@'��L���(��*�9�8�L#���wy{�Gs<�Yjl���3�r��{�~@�s��+��D�fjNW�Y,�Fu�;�n���V�6b��<G��+���N�JB�2z��6!�b^#�2�ӂU�2��`ɤҼ{��'!�`��s;�<eC]���n��bm�$�㥴6����6�{zK��Ѥ�7�����(��]�!���&ko5}G��M@�DZek]�(�09[V�j ���Y����8w�/��g��U�����s��g������Tt������y��%��E��]�,.�}^J�i�f;���/���5kw�T�#t%)5�4�?!&��{�9E�p�C��bvh.p��Is�}��E������~��>�;`qr��a����2̆�]���p.�	��ƄF�֔OI��[	BO�u���,��z���XPzke:��&�JcRe���8�7k�7�&�O��
�-�)h;5)� �q�� V��:a�*ͼrU�����3�%)����r���WJ����j[�ﺫ�"U��,�n]9nؑ�6m���vyo�����]i(]ef��w�7��:�ɉX���o�ry3��:�Ձ�3�_��RԷ����7^����1[�&M�c.����Z�Q"�5|�n.��=k��f��/:Xs�7�Wz��X�y]ʔ�E���������B\���_��ޯ&�)�_X�7���z��ʿ��k��B�.v��Q�y��� �gT��Z���E���x��q������n�߰i�@��ta�������v� �z.A�^��=r���?8�t{qx�Rl�AS��3h������(��&o���40��իU�����P�;=	���U�H��mX���t��\��~IĄ��b��F|�ȋ@>���1��S��-qɟQ
�h2&�#b�&W�J"�2E�dj�}��d'����8J�Ŭ~(q�6%��v{;h�'J^魆���VI���Zad�P��<�/"��en4 �JЊb�A�ء��!�\9I�� �va)7��۲j?��s�Rw�(V��:V����Gl�$���͑|�R%�Qv�t�~��e���'b�.�|Q5zg��ƣ1��:��n ���O�$W	,��=� O��GA^�5��#�I߂Q�>�wx�C#�a�K����h{���v��-ݙ��9���d"��5ꓮ����3�L3L���3�aU��%���ﭝĽ�8Øx��c8 ���e�#.�.a}����f���ն�j}�w�����0�����I�����҄_v�����,h@U���ֹD)d����=
B2H�uT���٫s��-��r{�-Z��2�ܢ�㞛�{<8)Sg����Q}�x�(猸���>P�ʦ+��	f)��ejӍ�s��2�p�	�̄�U b�IG�f}�Ύv�F�%���s$�4f��K5�M�#M�# �ޱZ�>P�B�P�����(��!�<�u����z�4��fh\��;���ǟ�<,B�_	e@[���-t.��+)�j��D�|�Ŏq{y6iN�x���=�	�1�`h��GW��A?��>P����7>�g��t�{�����c۝Ɓ�R��ԋ���q���d��뇔�7�M
�̻7�aXs���:Q�Q2��Z������TY'E�v���QM�Վ�0kM�uQ�E��M��`�0���{���*�`�!��B���ω|!�O��Ãs�5`��hM���=8�@W�حC��moO-'
�(�z��j�m0Pko�K��WL�xG�������r�f�Y;/��ΔM������'��F�^qUg�bV��$����Ps �  |6ޞSV�TT�x�0ƻqW9���z�^��.8&�p�%�BeA�\m���/��ȉ���8�n�n�w�K���?���:yz+G1h�"��ʢ�qu���?k0jr_�3i���쏳ix�����E��|���rNLҜO��/s����eAr���p+�p`ɉ0�g����q��^>��k��}??�v���-F�8�x>�K�/e7��[L�R���8+W��`thY�c�\���q��o��BșJ�e"��nI�PU]�EH
��j�(g��
Q*I���P*ږ�G0�e��dV֙`�x�u�v��f��t��z18��!!�� �ȮJc��6<��[�ۜ�pn�CQ����}h�e��{�{��ڝ����[ysn2�6<�{��7�qd�~q��|�^F\i���U�:ɼ���?��E�b���C� ��b�����/��`��f�K�ګ��&&�@�3⍅	�V��5�y�/��P#H�������um��ׯ�t�?��x��8ߙ���;.�ح�61E�rΌ̄M�3�;K4�w��e�8�0�v�cL�'&êh��J��˫�w��oE`*_j�T>Zb����F�n����1G�����lw��DG9I�پ�z	�D�W�]����1�%J����ھ�fS�"����"2���-v���Gu6�u��t���.�2Rz�8�I�fp_8��Z�哫"c�ӕ�D�ۛ�yb��4T����C�u\m������6��_���K�U4 �J��ҋb��|��N}Q����%�r1�y��S�d����9=%2Rz���j����k�c�x�D2�D.�h����I�2I2�]9is��Q�3`�s�ϭ}z�����&d/��C�L��XɅ���y�o����7ƟUb������}��-:�ĭ�1�F�Z�R�[�<K�!��W�h���;el�8�R�~�Շ:Zv�Z�F�	(���E�a�,mSd���X&�f�!C�@N�C�o�x�H^���r�^�s��Ht�DB��x���Kj$��cǑ5;��[�Y�r�덙�1��dX:e�Vzi�ZW+рX*��{������o�����ܐ,��Gߞ-����iELLn�9��gs���"�Ŗ�m?�Ǯ¼qQ���E�U�����W!�J��vΞi�J������ q�HPZ\�(�.�<�BQ���y<�?�����"������8��(�K�Q�/[���V=D6�*{ĝm«�d`���z[g�����!��q��~/Q7=�A��A��}���e��K7m�O�wV��èw��N�k���Ƕ�t���7�E�3u�N����_�d�@s+���+���O�p"�nH0�g�{�x��m��G2@obnT?uV�%�]���t�6)����xol%�N��H8,X7F�c��Y�����Vр�*���/�<+��`d�:Zr���.���i�*@f~�o�x���lT�&a��x���6Z�����x�r$}��䇣��<�V����W������N	���U�6߿�Z0D�}�5G�����#���I,,�̹�^��c��/{W��j��!�F�U�-�-� {C�|��$X�i(1���7�/皖h@���)���	.a)�_��&N��6O���]*= S�<[v��p>�>h�?{��~__3/����b�(�^�n6�6?s�bZy�F�����vN�;O�J�;�H��vd��5?I.q%e�72�5x+�X�+��lB�/P�J�_.1�W�ܺ�awO�h�@I�;1iI��d��lO�b�ٸ��o�G3�v1���,��U���䈢7H�7�)k�UX��s�mc���{>�2��ݟO�Y�b�\9�Jy�,���E��4���:g^��݁s.�i�l�(�K��B��ɨk���Mi���=>^5�5w�ry�*�ay��/���}`E5͸L�x��M�&0r^w�M���q������`lb�i�ԙ, ��k��u�U
3ݹ�O���N#p��Y��~�:Bbw&���>��������j��x��-�#��L����ᎊ�WD��lbp�w��[�{�,j�M�e�}�~j����i
��z}�KI4����*p|��(>>սD�+�I&h{��5������+(�`5���f�
���ȗ�B�d_�T`z��0\`s8~�UޒjmK�����R��a�o�#��SB��V�sl#I
ζ�6д��f�����7��#���5��z"�~���I�h��#���m�&ꤻt^H8/��B��/��-����E��@M0�FS�k�p�pw.��Y�i)0jQ���N����-�3�O������=f0�;���}�+A-\Ѻ������d�Ol���.I�O�?yN4>y�Sl~�ܭ���v�k��n�vk|Tڱ�o����H�"��br����/�g/rǐ������,�t����Q)eֳ ���k]N��Z6Uk�*��g�/i]�p 7EC���)�E�	]`�7�?�0�z�~�;����"ɖ!6��&��8��|1�g�q�܉�r�� pc���XglmA��VJ��I��F���F���wο�Ț��6_k�q�D��EGaߵ��ܙp�__]~��hRI$)[��űg
푨���I'A[a����{�h�����pT�l�b.I�740�yrq	��+8	f?��������6���L恷��� ���;�V�{]<6�1�(*���^X�8��� ���yuN��,i��	Rغ�?N����:��vq���.m6X���Nn��H#@�(�b�d���H�2ka�&P��˪��KyN��e�����<�)���p	,�	�y+�ā�n_���1�'{g�UW9���̤1�h�,�%�������/xY��,�E�8����  �� �$ϥ���:�g���]�&p�ڬ,,}�7���ǆA�������]d���;ͺ��n��#&t��"A���~w=�e�h9������^LO_�=�E���^�1oixu�¦�{��K�����-�	�����Sx�e�c�ά�[ӂg �$���Է���R��g��L`&�zE/k4�>��A:���ʖ���%�R/����4��N�`��u�V�>�󏏏��<�A1         �  x��Z]o�H}��+��W�P�u��NOO�D��ۼT&��������SeƔq"Y�s"νu�o�T�D��쳭V�y�l�����l^�����Ͽw?E$�(���3m2)B	ϩz�:�������'[��j� �(���l�u.�����r%��NB)������aBi�%dOV�����vf(�MQ��:f�b<�?�M�?����`7��r$:ʴa��X3�>W�>�v�=�z����Ԅ�a��8~5[�i�ȰD3w��%o;�9?6�,5OWQ�hB�%ɂ M���ɸδ
a�9K�I�/���YM���\�wW�� d��}�����(
a�K#&S�h6�� ic ]}�O����!m=G�OK�rfn���G�H2��0⊥�I�ٿ���]�����5Y�Cq�R�xz�:�#���vӧ!�|���sB$>&��0E6FC�@� �W_w�v���ڟօ�#2�>��{hW�Euh�d�L\�c�����.�������@:a�3K:É*O$x�D�	��J'�RB��e�R��Hf�;HP
�a�eǒc�	�KP���!!F�a$4��g����.�zM����#�EƓF(�����u��6!��n���{����C!Z-�JQ[>�5������jQ���aJa�p��fwL��9�B�DW�k��M7�,C_��Q[��0ҳ�f	`�9f�S����֛�s�R<��9�8�&�uAc.1���({8�)������8�_űn�+8f��h`<�T�zЩ1l��.迗myr;��|k���1��C�0\�~������eZ�ool�?o����i#w$3�����:G�f<!sAs������Y;�P�J�0�f����H��O���!�~�%�7�v���#�3����M�h��p���*��qV��0���/Q��.h�:�����9v�O#�˱�`�rÇ��n[���rW�W����9H�e�z����ê�}�a6ǧ��By%%C�"�NZ��ֻzu�85J_#�Ǖb()���a�&v��#hɝQ̄��V���]���jr��INI�����rr|���*a(I����,gݞLK��:r�����o��٦��0�v\v�ivW���4�#��u��y/@O��0��\#n�Pw��^�[�o(ks��/���ȝ2�0=���3��S]#��`������=���mkk���7##��:y��;�[��p=rF4��#w������O�*q��q-=~�-Y� �7�3��.wwRi�rs,�wм9��������$~a��֒�5����h���+_t�Ix����a*�1��׻��ί��Vt�A�[�eT%!����h��[= ���x��6}���]�D&����*�BK��]P���0��ϿY��_-%#��bD��2z��+�F1B������*�Q�t��^�]�0�!��K��0��0�Qy0�.��z��q(t���X�&��ن6��>EQa#kł�=R����0��8�2zM�=F	�V,�ZPi�(�K�ҬU<S���%p�X��J腧���ҬU:�z�Q��K�V�û�F��:��Z�WF��#՛b�Q�Ū�*2�Q=��{3���d��x�Q����*����c�d�[�Sle^��S6�����^�5��t�6�(E����5����1F��FI0�\R>AW??cF��UόnY�@	a��`�4Y�e�����&��R�v�^�Ó��9�(u�ꅷ�ó�*�����ᨤ�������3���E��	�[a�(�������w�Q���N�jZ��E���1r/_0I���L<:1�F&b�#��\��r(7R}mcd8��ftz��l��ӄ02lh��[�cQ���n�+��=����#�n��n������;J��ϰ�;����cd�t�U݋o���ʅ�2���#����/Ǡ�;�����?�MD� ��a�            x���]o�8����_!`or.r ��>�Ύs&��LÝ�b�s��{��n�?̿?o��Ԧ�����^��T���GѺ$2:��1����z_U��S|���PEW_�?��oc�&�[��:��d��>MYg@4��8)g:;���d��3�}������2f�|���~��u��&3�|HYKr)��/Σ�T.�6��~T�x^=W��3�������e��3��t�Ӏ�[$�M�W]�Τ�L���������R~��չ��F�R��bW��M<_>V1�T����5Y'oC��ԧ���hQ\k�+���5�����~^������(��%�`@CM�4E�m�k�y��?,��ݏ�]��;���f���d��y4p�	��Զ�n��OK4����4�9'�̕3��4�&I�J�ڗ��j�{G��r��nUF������e�������L�m+���k���c��{>	���ר�4�è���n���ԫ]��Mـ��8��R�f�-����>����}���*~s�����(VϨ��5�S&�&"�yK�ˣ,���z~uw��脞@���OS)�ߠ����b����o�5�ϴ��c���2�x�E��)��>U�}�_����{�29]��U����v�=�F���S�4����S4w]h�̧�R�G�OgR��MA5˸,~j���J�J�"�\H�%�nn?xL	C�)�i+����Χ���3m�� Ӏd�3IC��$�[ g�;�~���z4��4�h((�Hg&�:�U��-��	�q��f�)��k���m����+Ȓ���c�;�$��[�h �
u���:}(�9�y1��O�2y�6&9i�	�r�a�QF�j�-���2��I�Yj}�rVGE�y}�v�z����Z�j�s��&ghGv���]���׫�����(�>ɴIG7�]�����L�˨,����x�~��zRS�>M9�7�L�J�Yp\��i�d�ѹ�1�ڭ7ǋgx�����ȼ)�.x��v�c�Y|�޴��|��U�@�4�ħ�K�GpjXh������׌@��@^߫@�4��3Yy���O�hI'�㴜h�,"���i��L9'"��H,U��G��Qm�W�� Щ�:m�jC<�G{�3��ԁ���z�tT߇4�Cq��3>,װ��Q���,A�med�U0k*�2h6.pi�����i�����|�1�uF�KO�^o�]���s��_z�x�������/����ݞ�?��~�~�l���b���$`������N��w���~�>��PWtfh��?��}�o��0�5D#	sD��8%�pĊ(����fʩ^���E!�����
JH'e�4V
�\:%V
в��DW5���?7�))V2���6�i�h�L�! G=:<Ly<��;��A�xf���b����� ��}�T�J�8yÙp���-y�4�4�~ܘl n@�`K�\��0�7��M�[R+���S8�K/f�lCӯeO'e�FS]~�י�@��ѵ�/F�����C�Q 1����B�t���)�sz<4�tIS4a<4
�Tw�BO�^��<�Pց`Tk��0R5�IK��!E	�<2���4QKҒN���i4wh���y���ߒ�I���6��`x��gDz��_<Ƶ��#_�8�R>_���o?�'�?��4�۴$e"���~�Y��U#�N�a�~����u'L�r@�9��`<\?���& 1	;��j�\H��	����pF�*�O���&|���m�#nL��!��Mg�>�&����J=��)���SX�OC*�!\���)�3��MqhsNX�	�tK�������0��b㠌
SJ$��-f�Q�4N�iy$�H���Y��b�r^�����5U�S�"[)	��U�~����)��1��\ܜ�(iie2�~��W!i�P����z�Pa0 �4��4��34�C�D�宂[�$��pB�ܧT� G������.pGF�I$:rK��N�\��i�-��.��z��n'Äq�� &����j������H��$}��1�7Y�#4�UE0�'r*��)�66����§�)'����=��i@�!9�p���(��1��N]�H����p�&��� �Oɴ��qMw'��̽��Iɴ��q�	������~=d�����ִ/0�ퟁ�����48�(gJ۳�p��2���zk ��(-_�=�O���j���q��/ZP7p�7���w��ʿ~�l��ۧ�麗��2]�:�൞%Ƨ���4=?b�{�X$�y='��]#j�#� Hs�b;��ipei���Zv8��i�@��KD�\�|z i���ڧ[HZ�q���]O(k@0��Oˑ9�)NI{^�Wg�|��F}"�(��X�Pڂ$;�Bǜ�v
��Y[<�VU�ڞ_SG�U�D<��V��IK ���h9 u@�����AŞ�@�����KZ�)�m G�r!�Y��H�e��zH��3B���\�酟�K��`������9y�9�L��ɼ� �(�{ލG:�]�iJG4M�j:^���h��M��sF������e��>~�¤�C"����	H9�x
��˨3�TJ$a�i%�h`8=���;6>M��̦����H�6�G����FY�����5�yõ��m����̔>M�c��SD������E����³���K�#t �4�Sb�F� �,� �$����4� q��,ش;�w�e��PS�[:Y��3ƫ$���! 0݆���0tPF����>�I�P����i�j��������_��p�@΀�&B4��P���]|[��x�#����!�oP�}����ˇ��~��3��I��o�)�NT� ��a)�^��z�ȑZ?��⺧��Fߡ��e�En�E`�@V�q����S͗����={���M|S=�>���|�vG�BW��`��������e�ٟJ#�&�2��i9`;M!�u�ܡ>o7T)��03bʚh�s��M4*"gd�q�a}�)|8�4�Vm�mܭ9�E#��bG׫{�Q���\%����$9?,�5��I��0&w�tţX�x���ȭ��c8�����i�@�q��O������O�NB��0A1��vb"\��������!��#��8�/�5-��VS�iZ��!�-�-�.Oh� ��G��/ڴS�4���A�\4�i:������}��2N�L(�V>��E�}h�6;ZM�9� ���Ù����@J:�(�H�E�M�M�PE<˷�&��"�CK��I��`Me��	xY}����Q���J����NStp�J�b�PAV�q��6kҗ����fY�[�J��Y�!��4E���HE��!�=L�5��\3)%?�j*7D�hy�a<�
,l��\ �5���&���4�;����(F� 8�3E�������[���<�h�Y?lF��/���O�):qn���?�G����/o��}��,���ak�PSE�C� ��$0k|�yj�]�M�S��墄��$x�ZJINQ���1e"���тh�}��%���h�����)�IFA[C$�(�ʇ�*JBL��)��I�S��7��W���6���@1�T��9]� ����p��0"��%��/�@�u����+U�Z�_�OS%o�0�),8��R���s�q�aP@(�X��\�>MQ>����n?�z�=�<ǘ�1�e	�]HE繝OS:I�sR�B8͸��X�'�yCԙLXzyj�Y��4�,����\y��2��I�sZ�8Ǹ2�Zn��]���]�HYThQ�)�§��G�s(ϖO� ƛ�sEkqgR���4��恦4b`p�/�j�G��r�y�|g�E8a�S�x�6;N&��9nm� 2�S�z���_40-3�m�t��S�G��8�P�.�P陬���#2ท��@�?V�(���Y�!�O8��V� ��!���p���4`J��<�;�F�?�9A��4 4#�&���߬������;�\'��a!,K�Fe YA�ss 2  ���h�v�"o�Gd��f�τe�����n�ck��}�F�6!PA��˒�.Ph���ℶ	�>���'=�ZM�WB�&�;IY���e�)m���3Iyh��OS�?�Ҷ	�`ù�,y7I=Ѐ�!-K TP�'�G��`Y\o��@S��,e�A J�-�|]z���(ay܎'u���ȯz�m�&[�0ǖ�6"�Z�H��(�'��F��Mўp���7Au�}�y/e�^hwL6ɠ�������L�=���i�Ε��,`�cKo5E{��o����G�,z �&�����q�v����L��͝OSt� �),E۔����0�Nj��U���ђ��(=(y��������l�u�����9N%B�"�aU*PhS��륌��S�������4E;�)z0���h�>1�<��t.�L�I��w;6���������)��N���Q��z�����e�~��A��3X�w�-gi���aaĄ�Ms��3e�� �K�L�)M�,���D���)��GV��ҼkA��k�eMіzF���GX� �9�9]'s81Ԁ���NNsC��Net��:�c�e��AH�4ơ�Z��GE�&�(z���9]�Ӏ0�p��48#4Mj�xN�%��C�&NN927���Τ4ge�U
�2>��'zx�;�>�@�����}�i����Z���N'�8%'6�����Q�����~�6���es�Xr�OHsVT)��#Ξ���DR�`���d�X��ݘ0� �lIF����= �7~HAywE�@�jKA�!w�P(��r����j |߈T��,��-�<�D[M����D��5�%b�&7�d�S�= ��j� ۏ�{@�S��A�e7o'K[6_��� ���b˦:��F@_w"��O�5�r>9/e�G�Tp�|9Ⱦ�i ���R
�_w#�)�+�[v�1HN��)�S�e���|���[vџ��i�S�;�e�Ǿo�)�^��]�g�z��4'Nlٴ��4�2�eEz�����1'�l�_���)�&�Ė͓m>Mq:F'�l3X�i��8�e�ڎXHsf*8���nd�P�{z��z��=M��i�I�9>=�W��/˧jl��4�68-��n�_��?i�c��*~����<	iE씳�%d<�ﾷ���g�!�)��'y!LΙ#�+f��k=M�5 �>�b\��D�P�������|d޻�}ڝh`�!*J c8k�D��<�\�NS|1QD�	`��>�����׻ˋ����߮�|&���&+W�m�h�4i4��zC�a��ݺ��|'�%���H�c�Rr�Q{�d0��i��4支.�ӌC�z[��ˇ`�8d듳����6/-E�FL���~����[���q��'xM�L�������rd>k��7Y�����׊0%'���l0��i��t$����&O{�F_W���f�䨯�g���X�6��}}_{ӄ�HM.QC���@I����`N�=��qev��.p��"\Η��*Yf~;Mqz��D�+�+���Sh%1�7�D�����Q�
���y�����c��^5,�Y�>@V3;�4�4�ɰ����!Js����q2��$��������U�H^W�R0%t�C�,���l�P�i��2FR� E7�S>��0�,{=ѷ�0��DNQ�C&�d-U�:MQ"E��^[ �1FX� 'o�sҋ�
��^��m�?F.�i�GcMq�H�H�F3���]�]_�,�NRLS����=�|t�?�]w)�PS�ޝ1��e�<{��P�E���[X���iY|!�����OS|�/cD����'|�U�]�	"C/�UKӰ�����E�?�]�oޯ~֛���rz|�����>���rK�Jw��[�������k��ST��8o5���#}�Tڢ��?�7��M�0�?u���ՕNS|�2c��NQ�����R\��e�B�1��|��?���0��j�!�Ɛi��1e��ѝX��xQ�(~���Ko��PCp�6uq�2��4�)c�f�����X\_^�^�w������guK�m�wX�ӯ��ˎϤҷ�
���;�{�⻫	C�v�#��3A�e8ay�c�����@m�y���۱_�(�.2a���
_��7M��~��_�Kn��Y/���I��)�T���6�����TYo����w������������5��N|���i�2��;MYZ8��H;J��y��<�)�|����h@��
�7��6�����fP�bI�	�^�r�6��4ŗ�3�r�2�M�p8].�������Z���
0�u�VV���i�2;7�<Z\��_���*��]h��m+WOSt�a��$@!�v���~ߜi�L3CT�Q
Ys&)I�z���!(I�B�;у5 ZI�?N�e�&��<�4�lw�Cl��e��H��������2�.�@S�Ή�/��!�PD3$xgBM��Wn��4C��i�؆88�������z�j�i"��*7S9�U"�*�{u�c����5T�4�`W��c�:����M�+o�?v��@-���K&�,(�a���'%�M��4�5��%���&]��c�;�z8��`j9�N)L�O&a(�gm>���4�\�'��@�[)(U��ouC)K{�x09c$�� ��JN@�3(���oMs�(=�]7Ԕ�:;��1���h�R�Erv�7{%��c��%�p��8��4��\Y�@4�ҖtB��i,����؋+3�@�;��5�w����o���[�;��]CMY�s���Ӯ�i���X�G��Po���-�Ou|�Ǐ�\���PS)ݚM��h��~S�����W�$��%X֧�6z�Y�Ș1��J�ɸRv�����={�J��%�n�Յ�Ұ��4L74��c�a������ւ�xM��]��(�!��,�!��"o�M�4������Nv?[6�����q�^��d-�˘�i]զe��i��JƔ�n`s�)X�����6�5p
���ʦ@�?%(���f(d7�'b��S���vPɠ��:M׿iɕ�]���fƩE��Jڝ����~���P�6ʖS���wZ7z�^;#���+�����         <  x���Mo�0@���q;d%J�t��a��A��v�!3��C>��h/@�TI#���3�(R%����uzJ���i��,ڦ]�p��qr��7�ؑ�F�(�хhM�!wFC0��Y��F� ��<&�e�:*��1�U%��N�"�2L(1�:�����7��9���1�_s3�벁��٨]�a �3���t��E=�����D�بi^���ىN}O�N����\ԾĐ���*1$\�Vm�k-�x���%�$}���̩��8=T��+1$�k��C)�3`�U��w����ωF�;G:��i��VQjX`(��R��p��R�����"��%��B�?WC�0�=Cc����v�[��v�7�z���ni��ֻm��ݦ9���;�?̈{\b(a�<�7�Ms3��]Z��d$�0����p��i�����8�����?ܝ{�ƃ�G>,c��T����uZ=C3o/��g�3����Br�T�>ad�� �#G�J��A:����W��n��=�G���;�_3�6�{�̨���9��`����ݑ%�׿"������<f)            x������ � �            x������ � �            x������ � �         �  x���Ko�8��ɯ�`
��.A��u;-�f�;�8��r�Ǣ�~��ȡx��7� m��=�(��K�&�P%��dy��~4�ֹnv}�^�n����Gw�m�������Jd������|�s���r�����_ߍ_U���̮d5��6�V*�i����O_�g7��~�riC���u���l��������~���?>)�,(#��I�CT1�2�k���S O@*�_GH�Z%5 *G � �²G�TI��H1���9��2Oj $�hV���nIH�\���y��0�ښ��2����(j��b�h�CCy1t4oϋamRC��sb�u5�5���]��ow��ۯ����4XL-�I�s+
�%00#��A����*%3��:�!B�?G0�0���-L�!A���zD�!�Y-�4 P���ZX�Mɇ�'��e��mǗ�D�[��TǱ=h2��# FP�2Al\�� ��<bEU�@dܝ���03"��8 ��T�YRC��A*Qr"�{�2�!G�����F��i�jH�u�Gw'�����v������CII�"�*.�QC�"5�_��˩|��`4���ej��SfBr2�3E�!g�*��rj��u��&Oj���T����]B�u��1�s�����~�Z���r��"�����,����C�c5����|)�K������s��,�e��Mi��˔q�`���^dlke�8��ғ��,�rh�o��������f��
�4�nӟz|\o��n��f�H�D~'�Z^=����Nb��Y5�9�\6�BS�"z�տ�zE��K�ߨ!�$��1	�i�a5`5���}V+4g��\F'5d��+1m״�}:8$��~��Y���d��%5D	������x[��!J�'��F�f�j�Ԑ%�X(�x$�6�b;hH� '�T�U��O�AC�i;|깷9s�(��AC�i�{*�%��T��5D�v��#i����De�de�iC{2���xä��6��w�?��j��W���wAK)y�1����8	��F!,GäG#Ⱦ#�
�|+6���r`��;�a�a֐�%į�#�#��gch�^�<[�A��m�l��O����>�G��1���G��� A�aN�<19�����(���:��UȨ!H�'�p�i^���4����(�U&*�p���q��q�7۝��f�L�<�����.��se�F�54"��9d��E�ۢAC� $�Y!�o��m�mꙃ\L��م�=ר�q���y����f��x�2hh��W�6���AC�8�����ȫ�c�AC�8��_��YfU�w���cG�S��~x��5tb�z�j�����>7�f�v��r�?�������o��ۖ��/�o���S˘� �ۣ��A�����s�N�AC� >�K�[��"��*3hHD�Yh��U��۾QC� ��3����k٠!,{�h�g|�μi!'��ACwL�Ƚ�>��5m�������m��c=r���y'x��S���X�I����U����Ja�,[��P�2�>�����QC"��2ax��CC?hH�a�u�P��po��!��M�	@�ۇ[Sg��kА���뤗�<0 w�i���]���s�>�l0�M2��8J�օ(�OmO���MYF�ajݷ�L���4��w�S�R��
�����5��l��S�����W���^]^^��{�         �   x���=��0��z�� ��ؓ�n�6�v4SD�)��"�$��[<�d301��r��U۟|��@)Q���|�_��q���^�U�9QV-5��-
�~�Z{?��(�K��XoF�Tj��v�{���)Z�oܘ�rZ�ƥ�3��� 7��� ^g����ip?�8�E���DNZjhKys[<��'�Rz+3e�R�}@�+Ns�         �  x��Z�n�8}�|�?�-x'��-�6��h�ؗ��ko�M,�<��w(Y%2�� 	�� 爜9:CF�+���!l��v��]hgs����֡�4W?��?�)yu����v�����j��k'�n?���|~���՛������\�W���j�}�m�U	����b�mZ�j�w�}�6���4��N��L�K��j&Lm|m�j�S���b��T��%�^��g�ц	K֓�O��z��z�g�KЃ��kiK�A�<[�uL��hY� ���|�ٮ�o�V���7N� E j!J��~F���0wqGȔ*aP=U��a�"
е�J�T��)�hj���6��Aܚ�*�S`9��J b F���>��(�k�wa�<G+�d��0#/="�9���;JS�aP��B�ɐ�iN��YMD��9�m�PLPk��2�Έ��1�b�j��ֺ��p��n֬��#v
���W�g�o�3�w�ބ�z��#��Ng��e��e��2_*�DJ�ݺZf�1U��/i7d��;!b y��/I�Z� ]�n�@�Rk��Z2M5U_�Ɉ�L��v�b�����4v���Fc(w1�qQ�@&n>!��b�b�8�����L�{Bl�&���D�2q�)��'�8cH�/a ��;��}���0�#W^��]?�,6a�U�ۿ똪�.~�u�4�O���c��o�_�y@`�+���LSG�F	rd�T�Nk��%j^�0�#��j���K�jd�T��ULQ��
W�@��\{�Z�"��	*1���t��
�UT�P�v�N|��(ő�i%N#%�z��Q��Q��a�Kd�1P���\6o8���*,��eK�'�TV0K����)yC�����LQ��4�$�q����7]lzX-W�Aҧ��6�n����VTSi$N0P��~�$�q좾T��T�T�~�$�n�œ���d�1��L~�$�0Kʘ>��Y�@��y���L\<�Uv>1Љm�o�Y������r�6a8&�3Ԓ��l�`��>*C�<���L	���1�k�2�/a�'>*Cx�$�6L�
��p5>*Ca����ljmJ���N���'m�{�|[l�-�ɝ"�f��N E-L	�X�)!�0M���T�&1�SB��9kO�m	����Ex���lö����z��Kf��$�5�u���2r`D�%LbT^Q��2{ވ�I�i��`�Ϋϫ�y+��:G��8p���"fz���s�	�a��>��3}	35�	y�+N'������݄\��R}EM��������/��:�񈁝���ɭ%\���d�0���]ϗMo.�k��b�X7�6^���u���i?��8��66�_���g_"�Ԧԓ#���]���y�c���:��&��b��酙��\��f_@������.��Y�Flb�/�5�xQ��4X����y3��<ѽK0>�f�H�6�󔼲8R��{&�K���~����+2��`5`�rC�W�v��3�i)b�rG��m-�A�d�0prRt����>l��p��k��V菴4m���1p*��N�q����g舁��XuJ�7�{���j��O���l�:��h�<u�q��1p6�N�g5Tu�,1pS�����F6���1��E��BSj�sT󓳻7���Sc�r���9+b��9zj�k�;��:󮈁�:gJ�p�%�D������� ��C�W            x������ � �      
      x����ʲ&��BVeV���ɕRh�:u�  $1�{M&@���ӯz�'��A�"Vf�mݶW�N)��/<��3��3�^��T�,��[��~��*uA�D0њ[Lp�38 3������?_�eT���<o����t_��(��4��\����o��y�2��(�M�V��6�ӳ|�=ܰj A��XmR�� z	�~�e�?D��O�Q�/8���� ���i�,  r��a��ݮ��뚠]u}9X��`���"C�,NRLă���`0E~Ϯ�pWm���pMm'.A=l\M�yzȨko���w���b8�F�W��в����7�P��0�a�rakg�i�v͚c9?\/���ni�by�;�\�X�)�{91��JK�kW$KRt����2> 7����nku�#ZaÔ�M:�5��4�~���ZY����F�������벚;�l�҇�f��T�݇A�"�8V�}k ���bUkf�o=>���)� h�A1��*�ԁj�j�4Ú�nT�Yp`������g�u��$�of�\T��\�D��scQ>�d����1�E����hY �- �he�n�C��}M��e���BM��^t���zS�~�B�/� �r3�R��	�bQ���
�J���40U���.�V�\S��Z����g��WJ��I%wG�gi�壱�w ��E�/gV��z_
p>�^-	�20$cvy���:�}C�;e9��q+Tr�P\K�+pF��������/����4�-	7���Ϊ���(�V�1ic�C��Y�r��J��n����	����x~���W/��(3��vt5Gƣ��w�m�.�XW�7���o��
l��~���Pi�,`�&G��,��"���o�}(�a�*!M]ΜhIS�=<1F��%�5w�{���n1-���^���3���A��������zLl{�������H��2�y��X�������wDB�^a�ͪ����p2i�ژ��j�����qk�	�� ů}�E2�Z���Ѳ��t&�!������{bd�����Y��
P|�B,��Xq��}�����%��1ٍ��t|�qǈ��SXXݵ1un��:.!?��s��د���G�{���S�e����Mk�D�/���MfjmeQ���V����(YMq�v��Prۺ4��:�r&��pF�i48>p�d��b�f���{�-��X�K.�,L�U��H5������q���Xǐ�MC��d$5�����S	�pf���y�J*=��b�������׭�J�*[��G��� �i�,���Ŧm_�۰~\c�Xpkv�pC�(@�"�(jn9X+V��z�	7����]�C�_� �����7�8j����<פ���~��ޛ��mPzyB5N�ٰʖ�6�u��r�a�]t��=�s�y#�4Z�Psԭ�&�u4;�Y�I-Ai�N�<������7k�2��IB�5 IJ�/M-��_ڻ�\�X����i���/U5���竉WD�-ij]~zġ������x'?f�A�5�wXv=��9jcBa4���l��9ӷ7�� ����Y��9}2^�2�����`��ym�=XB��H��l��1�_�t��h}Qo4�F˒8��x���SX*��h�&���u�����[�W WC���I�le9�TM���`$7[7K�ٚ��`�/�A��k��C_fڕq_���!�s�7[�'����4� ��l
�1��gp��ԚEL����CH~�f�����on?����*/,ő1s?nB܂+6�&9����w{=L�JůzW��˵=W�68-z������SoH�eI�ΐ8�.���ĉ�U{m�����Y�z�d�o�Ⱥ���;*C�<[��f�	'�!�.	�n�����t�b@��W9���g�~dsC`,�A	��Vg.�"/V�9M}j��w{����so$�F�i�,	-�)3���~d%Wl mb me,�.4����p����0�4ذP��v�Θ��v8T�eko���rR���J�!`H��9�8��\�tS(ZTIk���"a��e��sL_��:u����A�y��Vk�X�^�;a����fLpo8H�eI���I@#]�,�mw�F�4F���
�Z�Pq�%	��TH��P�2_rl�#&$���Rmܛv�M,:�
�+�dJ��D��	�����J�e 2� N�1q��p�l2�0�/(��C�|}G&8�`b�P��և82a΃q�lt8�XC�r�k� ���j��}c(>�#CՎ|#�4�
	�B>�#wqd�sX�bI�g�Opd"��Tv+��J2����v�(�r�I��3#'��������)}��]hY@���F<����J��R���g_N����@�Ygq���J &ajL���Fa%<hD~4^չ2�MmBݏm-Wk��Z��a����qD������;��[�DW�p�"t���	��c��4)�>t3�x�e���i)	��&!/�sk�~mF4'���)(�Q���m:�M�F�"��"�hY�6y2M�����k1��	Κ ��~ƽ �
JQpݭR�]��{�=�I��B�qb��[-7T��9�V9K��i�`v� ��A�Ѳ��3���t+�^�	3��Cj�&�Q>|���j�P���;��֋5U�)��S��8��:����9����7으њ$���B�� �	N�y�\�hY��e �Q���?��b�Ps��7㪽@/�i(���Hձ��q�k@cۅ�"�7�~�q>Q��iI38MAS"��b�X^��{�l��\�0��|r��I��W��Qc���[f��KN�����uQYK����f^���T$�7@�Ѳ n��!>Q����Ŋ���NJ"ܝ��� ց�~�>�X���ΗJ3>p����x�� ������Cc�	(�0)���b����Ɓ�F�Q�0Y���^�˕�����BKo8�F����ç�/>TT-/�k�M����'�� O�1���*��HEu'A%�7�b��ts%'��Ρ@)���]O�7U\��jO����J'Жi�,�X�����Z�M\_��*A�X� ������(�f�3w9�
7T_�߂hs��ek����
��5��Z ����tx�m�P&�������l?���T���&��4t֪P��-�x@3�7�c���.����Ә>6\% ��QƷ�p���eŪ�[�ag����c��!�3i�,� P��1�3Q�<[ �2�8��K?���v�O'7� :��@�=��T{���$�I��*�D�!(<>�|����p�ٕ�=]���
��
9d{�$���5�U<�q]\
��'\�iY�{|�xv��_��Qq��5�b~��� "�R�|Q�]\��ݍ��}݉��	Ӛ�|h5��ٓ;Jl�V�Z�T�C��eg�?(+;<�����ŧ� '9�I�#��b�q�c��
p���{A�d�t���+�qf���m~�p�.��;�Ȗ��oχN�[Zj��kF+�~Q��4Z�$��3���q��|Q��/�����n
u�
��0գÍ�Iઌ�W�Xoq~0�����Mu�rЭ�=�.��Ãƃ�5�P�7�K�eIh��~��	H!T%��E��"Ttd>U���T�jMQ�D�u+��H��Ku��5R�,�Y�?���t����r��z,�ι��u]�z�<�W��S��Bi���X��h$���KJWO}<Y�������ĜY��aG�R}'i�٬SS��V�KL�n�J�S�}��?��Ā,��(.��%I"C�$A�@cU��M�Y��fHP��V���t:ެA�^���֊5��Av�P\��/6P"�A4��f����E��T������ӛ�T���t�E��N��v�B�lV���(y~_�k��MC5���\-�wP��>�(�%�9nư4wr�Q�	��2���~����4L�8;���3�mw8��*h�t)�(�7�J�e	lp���N�_A)�� &�syĄ���LW�&^�`�F�Q\��mE�[M]�w�� W�L�P���J��er�"�    ���x�~��+�<��V����� ����jb&��9���6�#������/wD���Yu��L.5�`�lI�Lr
.u�˓T�ɑX#1_+
mAGju(B�R�Ҭ}>�){�y/��r��,K��G���W��{"�k��Ik�/����ə¬%P^a���i��طf4��z䎜)�"�l������ɤ�A��E�	�򅯫�o��kp�k-	*HU��^�0���z�ԒA�u����nDB��4"�U%��B)�F���w"����V�qM�BV�t�X�
p��i1,ւx,�m��}jaBM�D�?�����K*c�|��Ur��o)b�jx�e�!�n�Ps��D��T��i�,�J<��M����]�Xm��Ӆ����o� �W��HŁ�9��sF"�9C*���;��O�E;�u�'�]k�z�����ǆ�����g�y
���QF�fV�ڱ	ֹ	>j86<Vdh��	Ĕ2GsVv�쾁��i������+m<\�u��/������'S��d-X8�L����T}g�;k+�M-���a��A��B��W4`�#|�����?��cʎ-�|Ð%�;L��&�Tf*��iQ[��@
��i�H$��-8�W���@��r,�����,C頞�����W��0V����M�z8&��U%�g���>�<�a��-f�-&*�L�Ჸ�#��J���$W�GUr���B�m���.���j����W�V)�W��.�|��f��)>D>��o7��n��������D�M�xt������S8��_�1���K���iU�e��sd�;�\�@�� _�bj��f>�y�R� �4#n]=#Z�Pw��ܫO��+7����	���o��?�f�bh����lJҽ�c�M��uu��?u®����q�P��U����Bw�{u�_��G�,�����]��p�D�w���k�����Y����]����u}N�Tw�(%0���hN{Ӿ�`\c��Ue���/���${���S /H�J� y�J��;���R޻_~+I�����w+��o$�!h��P�:��<b�ֲX�%���bg�yOa��<~�eIhG 唓8�B���,��T��� ����:
b�|���;
��FR�p�c�v{W��9`fͮ�U�
>��\n���V���6��7�C��U����5c䗺�Z���rR�wd�Y�%�<f���U�rf|��,��"���ʱr�ip�c5{{��f]�s3�%�s���X�N8�'$)��'��:�$'O���TA�[��%�9F�4ɩA�P���+';��Q���U�B�J0�Z�+C��%'�HO�>�2g���#ߟ�$O��iGN���ӑ�/��^� ��nhn������T=���E�#'l�RIimp�4Y��J���������M��^hY '68N����/���A�!x�46�z�dq�M��,��7蕶���솖k����v�fi�s�<s���4Z ��z�~�ᒉuQ�[\FO�}�n��4E��i�Tm�%�M�	��7[�bu���Dq_�V��S`8P[�{�m�8�E^Ӳ �I�y(�x�MǶ#CGG��;��ȕʆ��'�B���K�s�=w�a�)l�ū(5>8xY6�U����s�+�h�)<�3�#^(G.�+lg�d��ID'�u��RD��k�;�Z%�,4��o`DQ��S
G���q%��;�����3k���e3kCmv��/�зF���$oNe��۵"�Tg����=Yw�M���66��T67����_��+�,A��� ���μd�^�4t�h�~1F�� ,4�y�c���.T_�� ^DC~Y�9ś�bM��ַ��.{�Zɯm
�L��
�&�.ns7_����l�4Z0p����N���:����~��_�%d4�k]��fN�+�\�5I��V��5���� ��P�H]��Z�l�#���=ـ~��[*ΒG ��c��I���3��~c�M�hq�J��FMun��Ei��N�s �*����
�`����hY����U���9)��>�����AȊ��o�>�:5���ŷ|�U���,2�ө���V|9;2(��2>聺z���V��ǉ��H^����jn�Nu,���6��~s�g��wj�K����F�i�,��vƦy��u�
�I~�؅�Y6%D��Nw�5�uuy�0I��[`���P0zN#A�n�3Q���:Qgc���3��s�s�A&A���?T~b��~�����-�>����?S��	ֹp|��u53xs�i;f����j��V�T`�F���l��6Jy���U&xw#�7�N�eхl��~�@[˚�����|�!�g���oƑp�x�󚓀ǚE��;/�y��&-h�����b��δU}���ƃ�q<�!oѲ$r��������<N�x�:i[�����ْE��٠�^��i���Ƞ"RҰ�%D������7z��¦�\��׋��H5��řݲ�X�b��i��+*#w������')�/��%���b(|��ȼ���ҵ'�*���Əa�tۅ���Ʉ9��2	>�8A�'3�K�%�7ܬ�fsz�XL�k�bmݛuՠuzhwV#�3hmW]�����4�;����� W������>u�p���q#�	�6��#���H>
R$����8�X�U�(ғ�|p,H<������&ϙA�_pNsWW��7J{_I�J�L�ٰU&4�����G��e��eI�d�#��Tݖ��y��5�������l"l��I˅F���
WAN"5�Y�\|J�}ٍrM�ʧ��9�7��_�F	�9�n-�{�Nju�,��͢p!�ޅ�r��ڮq5>�}o��?o'q��}�Ѓ�bv�,γ�8�Y�f>V��6�NG�E�|���=h$��5�|�#1":I8X%hYn $H�k�PM��R��oAK�{ߢ'�ڸ���xM)%̓��"��Ar��̯���+E�o<�.ؤ�^�O ��T$�4Z�S�x1|��rw�R�7J�'9���K ��)�����5��4��l�^��T��0̑�Vߕ���x)�[�״, �n	nx����k`����K9i�6��Ȕ2��a��^m���\�{o2m�m�5�֫����yo�d
R SC� �<���L�#������,
Gs���`i�թ�]��#����v��z�[N�Y��z�}�[��]�T��[#��.�#Rʜ���iY�bfQ������πZ�ڬ~Kի�p�:�x~���"�J)Xn6;s?��r3���=ܔj�N-���B��u�%J�]8�А��h��:���ZPq����@`�.v��C��y
�G%�M�;%���#(v9z��o�#FA+5�̞h���|Μ-sΦM(3�]��3c��͸����.7�����;��棘����Ѳ���}����s���� �R�ի2��+Z��z�^y�~Ý?'�tJzT���h��R�����W�㵀��A��Tn�P\(M��b�m�$/�^.�'��a�qh������D,�T}Q�J�"�c%ZweX������	��#���q��ψ4��EeG���UKO_���j%�4F3�֮�M�?0D�&�i{�x؟�j\a�s��7�۫�c�}�	5�I�ee#C���.l'#��4����	fѭQi�6�V����uD��o]s����t��`�+Xcgg�J�D�2���������{w??G�#e���d�m���5�ģw_��8���R#Zpp�fS�ˠ�F�{��xf��J;~也�9�D��b��t[;Q{Z�Vt�>	*�v-jEu�ɔg��7�y����ͨ�؋[��O�JI} Aˢ8�ȋ�2$����a�E!���}����!BQS��ll1�K�(flΣ�K.o�e��� �a�4������Ֆ�����R��mï�
k�˺���ᦜ;�� �U�S�L�	aʔ�{��QYjX}�E�8�5Q�ޘ-�.�#���n9��j��D��S�50,��P߉￾��*J,g� ���^�A���Ug��:}��q�X����    |�(�$�N�hY���ɞK��9g��FRR����?��}��vItpu�*nam�A��,ܜ� ��4�e�������"8��� �ra���#��{�S1�g�p!\��������D���\�n,T����1��:s;�#5�^;,ق�)~n9w���E7�/�Y�o���.���=�\�Ǣ��U�S�xf�[|ѩ�vX<(F��\�ƝZ�-�m�mVkEyF�c���P�d-KB#�H��!WUE�/i]�6�IJ]�nI����I�&�p��+{�Q��ER��ǵg��O���X�@)硘�����	�4w@�*�X-��+Ν^q�6�����`�	�U���"d��'M���8���^z����8I�G���;[{���|\ǯ�MW[�݂Z�)-���y��]����+�g�������G�ѹ7�4d-K6�����SB�w��L-���M�.�=����΢hBl�B���}X�UXp bX@�y���X2kN��AZ��!G�XF�U�^M�IAw���p�b�C��pL*r[��H.��%I*C�4��\d��V��К��EA�9���9����U����Z��� �E�A��V�C��gp�I�ϔ0h���3�V��V(q��-����a�R���g�g���B�tF����g�.��~��$A�,>����̴̚�R͐/��|��_:�T"6�B�yhT���a���L���d�?C
n]���劖~���ME= �d7�/ݸ��L�d��]w�[�V��Ϗ�BC���x�ۣR/�UP~p���w�3�e�dz r���bLB��AyM"rV���"��U�(e��5�_��=����'3��U��-#P��*�d�E��o��+l�)x�ˌVC�ȱ9B
�6I���ڪ
���:�>3�E/=6s�)���,@<��!d� �{�"ݺ �=�[���O�fm#;��O��+ܪ&�V_=�x��������u�'�\ԂX��7��ݻ���L�n��u�P��dE]�Z&R�'f���E���ө�|}F���vk������m6l��\Nn|UxL�;b��{�Ԉ'�0+�mt���`�\�J�������,�@8lXN��àxs01Z��/{�����\X���F���|Q�i,̫F�x-���NŢ�1�*�(K�P!�����׊�{$��	A-_,�bDY���X�'�^���&�W"	~�$�w)����8y�\�c�/(��YCht' f�,�d�5���������
��5j+�Yh���X�̊�33ܛ�4Z�p�f�D�̅�����5��"���;��%��h|��UB��$ 9���jG]��%���MvQ���3?`�[�[���ВrM���������2�>��Q�>{Zpp�e���%�kJ�o�q�2<�Ч>��yq�cIS������z�������X��|���v����&�䃎x1K��dDˢ8>�i�.p�a(DPt�D^��%
ŋ;:;-�4�nS��sc���:����[.Z�~ ��e�
�n��^���֝���Y?X�C�"�X�أ�n��;�T�I��H���J��� m���x�[+,���m�+�+��!ߓ�g`F�}�aQ#Z��F>��'jE�\NN[�;��U��Y�����ڈ��dp��A�nZA��6��@{Vfټ�	��v�׷��8}��y33I���eQ:$��Jp�h�Y|���*�P$��h�\S?%���y��7���<�Mh&KI��+��*����³ХVl9�'o7�����dhBV-�X���u!ۃ�{����ݴ��&�1�U�8%�	��kp���*������MW�k��6\U3�Q�Ū ���� ��Vg0��&ja�=��b��Kf.HвȪD�)Ҍ�4������P��
0$q�����OACŞ<^ˌ+N֚�2���|��K^y��]��k!��WCB#*Ӿ;���;�K�AD�)pt�D˒�� g�;�Vm�T�%�
=/D1�΀��`xty�[��9�Ap\����Y����[�ԛF�X���ܷ��T�I��l��bLۧ� /	���B��,	��a0,l���풩���H|����:P�>v���4��0c�l�x눷�yh�ۓ���);��-��.��D���B�(��VuJZ��1���^��-����<�+d�b��� G�4�	T�$�B�d�+�8mv{�}I
ۋŊ��ܩ�t[�n�B˚m��g� I��)㚖E�ґ�����~-�W�n�,�����;̐1gc�ݯf�媺;L�EI���7Q�Kg�wzy�N���3�!s^�״,J��܉�;a��
E)E�����/�9J)����e�f��[��KIrD��|���00�n�V�.���T�,�Ԡ�[Sj�[lӏ, ��{��HMв(#;r��,gW�Kt�v)_�*uM��Ĕ�������qH��g�r��w��V,��`�K%N�Tt��c��pU���q�qN̷7W��!c��l^��>���,M:M�*.(Q:�5k�Ti`����y�y�Ѳ(�;�MN=��=ْ�@y�M�&���Aɋ7)>�
L����_"���%���;�X�n��r��.A �W.:���Ӛ�8K����y�{�(E��n��'��NM/)��ڳ�]r]i�i]p�FC�h�a\�C��i&��E���u��H6&\8N�H�J8��Ǟ��6�gb�����buB9��L�U4_(���H�Yq��(��Z�Ѽ(���>�L	a���ؚ�eQ
{�$�ʼ<
�ڋpm�����pΌ�H�H�~�d�-�g�����!��Օ*�K�g��8����H���Πض�Igޮ���/�\A�(���oUXΙm��L��۾G+���>���<R"�z�ã���M�?�C��Rh�^ݩ��!��2>��J�Ֆ��k��+�7�zU�.��Ay�J��l�2wV�)W�}@���E�*�Wq��E�yϺ��skX���ş�%�Q����qx�3}����E�7�Q�V�;f�X���������I���]�	O��
�7J+IoUݾ��z�����`�6|v�x����.�,J������<��C@[nZb�`�3Ej��^C"�ѽ"b��w�D�1���vPl��l&��cHﵽ���n�g/N���y	��ޗ�t�p�y,�K���s%c���X܉5aȩ6�f��~!�%w&	��i��J�̽!jP/x-�.���	)��y3���ɤH��=����1�I��a����4�r�1�N`(��R@,#���B���l>����bM,x��58�S5T���x�y�I�si�,J�CiApn���,w�4g�J�,�v4���;�>�k;�,�r��$��v��;X������>4W�z%�G�K��-y�Vv��vI�JӨ�W�	��+��ۣ7cC���d��Dr	��A~���,=GV���B�)����upL�s1r���!��������#�8^�����xS��b�
C�ѵ��㜄�4A<P�|G��ˎ2=�E�����it�>�eI�A�e��� #�KF����zp^�Ww����⨑���},��Y}ENI���	�5�*�E�.�k�#u�)�����K�p���|�z�{1Rx�'$hY��t#?EG+I��;_�N|�?adE�a��s�`Ef1�
�Ҵ��-
L���'ļUvˢAX`nU�n��8��⨽�9��5-���������wȰ��Z����W�>c��{0�4����`�z����iϯJW���\����y�M�Q9��!�i�,�z�\\��:��U���5E�GjPsX��ȍ��*����e�Ep?�Nb��|4��_�\��T���TCp�1毪Xz�sUS�Td0��I�5��aRuqY�t'���(�H�[H+�f78ߒ��hY)=��"���FU.��Ey͑'�}���N�'%�t\J�6�Z�Zbk��to�//�m�9*q5i-���}#W��g X6;Aˢ\��y6	�ꤚVt����K!����S��#�n �83�������X��X��    �m��(K�m`������@����O��e��
%>���|met���S�,�>\ADˢl�ȇ�?9(���v{������;,բ#��<��]�8M��I��
��u�W���82�Y��M%%������Cq�(˿�x2�z��E9ꑃp�lt���b�����J
��(�^d���Qqb�������W�b�.6쾿+�6u��c=G2��8�m�V�����~�Hz�^�"K����>��8/��ѵ��1ֻD淸���`d�7�����\�x
�����0���c@��4�^D;���]��>9o���o�$@s���M���w�V��z�O��J5g�dϊr����`$l���u�hY���
t���Cīw�������+�%���o'R�dޯ/�A8�|���l>gMw��r!�45�����o�+�w{�W�#'O�
xp:�&n����iJ[3@` ��,��XyN��, NP���oꆔ����_�޴Yaȅ[�s2[/Mع�*,h������[��m7K�)��:���EM��t�+ܭ�o�g6���:�#�>���Wcp3����N��	8O�+p���O�����/i������/��G�ۻ&W�mE�z�4���r=�����l�]Ӳ(�:
1��M�k�q�TE(����(ׄ{��^L%�������hS�N����)g1���v��D=��F�Vo4��Rp�޸Q�S����eQ:�����y6vsa��1�y;����{���(g�5cC�_��G� y�<���n�	.�G�5����	̾�uk�����'���̈���ms*FM=����c��+�t�HвpQ�D0'�kMIb���t�v�b��g����Ϲ�/}�k��7�6�����/����^_�Q$���/;�W�"�,�W
�K��F$Xŋ���eU���/��y��0Ɨ)����򯿥��Z�@�=E�}�C$��P��n�S�����d�G�TuԖ��v���2t^��(~�#���ӿ��K�_� {�JkG�N�俿e>B~�()�FC͋N�e�K�F�,:��i�x�<�����|��W�uKyA���?������Yx,
���vϗ��<��t�ߋ��F��p�BkbLQ<
�^��y�(sW�T��=�:�%fי׊�r�P�e�v�á�{�;p�������j6�bX��4r�~�amdvH���v[D'�3�C6��8�b-4}�8J�~��}M�BK�����r.S��0:�7��Pt,�r�6Q�d}���v�����H���Y/�6*+|�ߚ������q�W�#>�s�H���u��ex��ѳ����$0-�6�u�jm��t+/�D�>6;��a�_Yi�<R�J\�8/V�G�BN^Ӳ4��.��p���	�����~	%�2}e�F)I_&q�/T�Ϭ�/T�Ǔ���z����
����lB���4Z��2��M�E��L&ݲ�Շ�sQ�]ԭ�
a�7��_��p2y9��Ļ?dןW�3�f�3�Ž_�U����?=�@(3�AGJ��e�
�&�-�����暖��L"l�i")��B�R�9�'�>R��jHO�9�e�q��쬏�������&�Vn���A�8�
�W��>��e2��ג];B67�}b��h͗�Ž��+A��,�z�Q���rZ��8�M�}��f�>�l�7�K�ei6ߗ9-ۤk�g#�ω�r�A���"f�x�#�R���E5��u��%�������}榎�����P�O3쫂�ǟe���/���H���y�$���aLk��y?#Ⱦ��+~$'ܗԳ?]���9�:G���ei.{��Ĝ-�1Yߡ�з*�*��5�g��c�*�jS?)�'��gx�д��E��ei>C��)Z�$� ���!_�l�1*�ev.�0˟��Ļ��O*����"?nyG�ީ��ٴ������7M��2�_����?�0hꜞ���e�M&�:�ؑb����p:pH#:�S���=3�*��(b>x%����oX�_��kV�!��|���?^I?Қ�V�F�i�,Cdh*uE�XY�ӊr���W�X���0M�X�6%�N��������%E|\�W���������)���B��n�=��,24�\Y�p1G^����ը����
�6�J|9e ~����;��e�l�q�<�������,t�^�S��b���qw���r�T�?�1=א�V�}M�2d��:oD!�"�_����@���:��#�t�_X'4�aeϬ�G����J q�[<�T��U�3{����Ȃ"�[%�H�2T��:rU�i��8��F�pD-w'��������J�v�����}��x	Q���NX%�V��O,�����U���-{d��9\-����||e�O�
��f<��'^:��SS����)�-{h
���״,�f��C4�X�/���h8���Z���x#��?�������1濳-���dnO/��,�e"��X��|%"w������^"_-t:=��wn����Α�G�mm��뭿�צ����&w��rM�2|��&W��~zR�Bl@��{���}�P>X�#�@�xD˲x�!ߍ�zr�t3[D�y���j�sk><(i'�P�w��o�n�㪮-��a1���p��2�?�Ƈ�}��qM˲D&N5�u���7�}/�M�Yp�}��h�������3Xm_�;���1�@�����(.��	>�������/�%Q���C�rn��]���v�;5e��Z!oQ[ިH�ъ�]N̤�7�L�eY��-�n��Y���uэܒ�5uL�UI+�*Q����4�7��r�d�G/b� ��C��fKߞ�_?s� �$_;F�Lk�T���P�M������^�+u�t@��Q"�J_��*���R���\J��y	|~E˲d&"��SpǛ�Gq�
��8!�Y(���U�)�b�S�}�H���@FT2D�O��Q�3����甒����U��D���ŘJ�Ӛ�I[a
a��}.U|�n�,V��*[��{�e��Ş�q\Ӳ,���1�ꕏ",�{�Y�ܰ%#)�9-�k���Oj�S�!�X:xޘ�C�U�%�t�9�hY����'ns��5Q�`(�pӀ�S���xI�e%�Qʐ����|E��d�	}����L���&׌(�y���}�� W/ֵ��DaA
���L���L,14�'阔����V]���t�Y�a�\��vA����n�X����p������4Z�e2L��sghc��],�X}�I���Ĝ��6̥}
˒s���?&����E` GqP�Gqtk�Qd�4��1�,�+
g6s&��^^ r[`-���X�< �0]��x �%E�����eY��������g���Gp�)�3�P�α}����@uo�s����*���N{�p��f��5l�!s����j)�������`0t ��i�,�e���y���0DeEW�[���}B!���I��}I��_@v�)S���y���H��]�dx���I��s�+�n}7.L	ƕV-�R�7��V�g�#��.��� �F˲|�MUz�]�v�U���X������	��'d �{�A�>iFBh���
���ْH�"v	f{v{�Q�mO��t��.?(4".RP%�hYϰiљ���GwJQt��rv�����*Ι����a��&��s��%����\S�i�&�O��������C�aY��9ݪ���V���8����9Z�i�,G|'3������>�;����%q��_���$օ@ev���Q��@\���Ηw��Y�7��F��b�Bu�$�hY@�m�([�i��+P��[A
�B���1R�%V�CO~�h�!P�����c�'�E����+o�Vֳ�&o��{aί׹I��)��������O��c��	򘜑��hY���I|/��:w`�pIɗH�F��C��g���A�8�&�~ނ$�I�O,���g:.��홿��~�kVy�,�x(OW����s��kZ��2q���ʖ��C��    >N�p���2��g�|��}gc�/���{�\Ą��r>����XY^�m��6k��	U�)v��=�e9A���3���c�w�.�[$hY��Ǳ�E�+WĿ.1�g��u���S&�'>|�X�7�2/�I�Q�əE��\k�̏�QW��u�#b$禥�/8�9�J�Ѵg��D!��O�e9&�F�K"��9(!wISJ��/���uL���3?d
R<��#zvӄ��E�esB��
�n��a��a���\�������(6��#j/w�G�<J�4Z�c3l��rJ�[��r��jgRQC�\�fLS�Ȥ(<_��g_�9�aY�F�k��v[mR�cM�*c��_,��#�@���m�=Vu��sm��\:��t�p.�rg�/���}��oZ|�z4���?0�%(M��(*�t4�a�����!H5h��r&u��_42����~/��g݊��zcmjM���N�-��&A���S��s��kZ������'�6�<Dx���'��VV�u�xM�$�u�(�b<HU����$g.���N<G�4 ����	�����r��"��&K���|A^�J�ai9f<q��G�IsB�3�%�ʅ�䩡��p<�����_4܇�ڙ��>rC�}�ㆰ�����:��=T��N�!o�Q)g�!���.�|tP���l]J<�ps�����;CcG؋*�yƠ��0m��;a6���	�~�/�ǤѲ��q��K�Ѻ.�M�BY+uE�rÞ���|�����K��b�8�r$<�.��^rp�9����zkGJ�ح;2_�[Ґ:��d܊hW���1��F�� g���ūx��f���YP�߈�"T��(Q��<��o�x����!"9����Q`�G�E��y��M.B	6��L��Ŧ8l�Y�;k���N}�]�xP�7v97r$�~j`�7�H�ey2�I��e�����yA+��H�ʤ]!��)V嚬�"�A�b�r��_�;���P��H�E�F�H{[���0|2��
|�ޣ#伬�-P�eqYB�!"Mc=k�+w6�5�U�M]�Y�9��Aőv��~|��!��hY��p��ZU]����P+p~�h��!U��[�kU�Z�$��[�Q�d� ��脼���Xy��\�;�9��Y�
���e���a�V�߰ �S$E�b��_cyg�G��l�����R搪5B�uIµyw�km�C�K�E��i����rUb�P�n�aD��t�K����LJ�ߤoH����;G2$MW��ʺI��f~ԋl0sxf@mZu`O��{`+�\/+A�2S���r�l�hY��pi	�t��6��ttb�f/~���1��(�#�����96�|':���Hg57�^�vF�ø�5}Hޠ
'v��k�7 ���
5q��S��p6������q���n�l��r�{Vt�$�l^r����{Vf�Q�ި��c��,�O��A�G���89Tg��:ǚK%��9Я�a{����fO.i9�T��r"��g5���C	�lc.n[W�,�e8.m�&�$O��?��Y��OIҪN�B�N�����z�.4������ʖO�K��@k��*��� �[*�����v8z�c�BB@���}�� !9�2l|u~�2+x�P-��"�����-����
Y���-�TJ��� ��H3i�,�Zj�Ԍ'��O��0(�Щs&z��UEo�e^��sy�d�\��t�%ՙ��^�0/���̵��*S��i���3�E�n�Ѳ��S����x��t��3��ҧ8�� w���� ́㰾�W�9S�|�E�~�{�����x͙Q5V{�$��[p�c�h� ç�o8u�H���l6�8����L|8��ti��;��f���'^���zpw;����^��kjߨ���/\��m#�/���Ҳ\��:��#N�,��,��v��!'�n�]=�;��;u_-��A���a�d��fIO����+d��X ��-r�,ut΋^�6��|��S�����������3����_m�p7����YMQ�"�H�]�3|j��s�RG<��T�p2!��i��	�8�d�Ӵ������h�2���ke䅥�P�U�C�a���y�~��tJ���L�O�>O�#�]%?W�%h'��q�8���b
��dw�Rg6,o �3S�Z��ޚn��U[k/��"��4�"���Nh��K�>�"�$ Hn��#i!�KwY�ӕ*�a����&W/�ֽB5�5v��Z��i��n�O�T�rc����e�4-MQ�^O;s�n�y�9Cq{V�ۂ"'0;�̘�s�hlM��ڞ
���\Yl6�%Pru�j�j�����nsx��4q��qE�]�3|���.w�N�Z8ϑЌ��=�{����7A�Ԝ2�|2)�7%q�ܢ�),�F'��3[�g�~�~��tJ�i�{8!�.�P�����w����t�w{d�%&�L�%�%�KU@���1�8pG�����P&�ːoW���%y�5v�@.�i�O��J���A2-���X�)�ők]Vh�5|9v����-f�..z��j<��eU�jƼLK�jl��%s|�h�� �ԯ��<��w��`M���idϽLf>ǁY�y�֪0��䞵Z� �3��Qt�-ݨzY\��?�Kp�+E4�K2�����$�����H����W��f�H������/���Qd������@��j��p�����Hqi4�E*C��S�UJ�/Q��Ih@\��%F`����;���5u���Sf����S��k�7�m�;�۰w�}a��t"X��D�Zv�'{�湢����m��]V�
c�?D���û��3��4���>�����{F�E�Dz@ⵞ��r�YO�T'�Fwu�V�y���^z�g|��c�d�Ju�gl�g��5��$�L���!cL�XK��쪴>�:#��Kϯ:C;���PYh�XV2d��J�#v�� E�~ǸdǸ��MR�\��K�57Sv�4@#�
<i�Lԇ{� ϕ~���h�(�\��r��"�h�_\iL���'�u����ppկ�>���*�|���;�R[�B97��{���֪Q�6rq��L��[-�H���3HB���	FS;�%N���U����j .����!��H[F�aW�_$������Л������K��'�8�-�(�4=�m�l����Ǖ]�c�=(֚�U����Qe:����.x܊��e/w$�hY�H�B�F	u���Zљ�j��"/g���(E�F"@�>��7���#'f���P|�߯��:ǋvq4��.{�e���Qy����@Q��4dTĈT�L-���N���	J��?8^�e)Nf�g�F^2�J�p��yC�D�ǉ����M���Ǯ����xy0�T����z?��c�`���_D/=�m�K�e�MH�YM�������}�	^q��dd?��������C�nh�
�٧&A�NG����q�S��B0jpT���b�0˖�L��l:�������Ҡ������WT���k�i<��ʡ�awd�\4.4�v]�Mף�	գ6�$ݝoP�G�p#G��[��F g Aӷ��4{Pq����X�e��T��|(ӝ�B����W���F � �@l��B�ϧ��^�;���4���/uX)"���e4	��$�ez�)]6 �bX�K\�o���$ڔo���޾mYQ%��yϯ�q�����ұ�D��"�x�A�xA@�����s~nɩ��Ntj�^�ۙB��*�Ffeen��N�p/�/N���2��#^�Qh��f�eN؋Ժ[�qڞNV���n������w��7m��Dw��|��* �ה�72 I��H"�.�*zL*�&8���D�<��#|s�mu�h�~l����k��x��VL�������5��ODd $ �x9�D�_A>�
$�VY�d����͍Ulu�F؞��u�W��,oum�b�P���b�;(94���<:�-J�+�'��$�37]�x_p�޴c��]��I���M��{�s�ޮ�,|��Ư�:� �a_�+���o⥾���u=��S`ݰ2F�U�<�+|���Œ�Y�w�]���|    yT���dx.\����Y�x[����5�u���2�����r҅�j̈́��z����/�1����2������?��b>��[���A�<���fvԊ�eK�z�����G��$c�$�~m>������kZ0�ѱK�1ћ�羽�~�\���\����[��_����њ<�k���ԅ�T�]��l6E������I����Q����ns��*j];ҁB�R0z��Jƿ���(���t����W<�J�k� `�����gY��u�R������ن�g�k��n(M�	��)���!��-e(qT1m^�띑�4��g�WsZ�ɀ��&J9���� �)�+�eC�O���YǨ\�P�Y�Oi�2iw�>g9�>�.��_�1����<a&�3sԧ�{Z���L�8<Q����Y�J
�6����s�w��-��4�PS�Wf����l�>��;�z�����deo7K���y���9V8�$�4��dU}�96_�)���{B�ɀJ �'ʨ�>���_/j�=Tѫ�>��&)��r��jc��h���vs��6Dm�
-�p�`�o��@SY�Mb����\A4��_�2P0�2SAqV�����yf?Zg����^�k\O$��77�z�I���>&���昛4�,tV)��?F���{I�W���M�O�@� �HD+��s�����ܤ�����(
5~�������O�/��f�d��XR�O5'�:��^T����2މU����)���$�Z!k�^+YPD�o���b��H>�fs-�Ƅ%wq���6���L�t�(d !��ь78�/�e<E���Y�w �9�.oS欯W�t���\�Z�^���sqc��ԲB����L�e2�>D��s�VUtSl���eOv۱�[ɟ��%n�`�m�;���K	�k2�`4��,������z� �1���`$Ѹ
D���%�0"��np^�OV(���^�V��W�$f�\<��Rg�%u7�bǑ��Ζ��d��a�<���P0D jO�Q{���wP��Y�Q�(�3�����GFz8eUauV��Nf��9���6#V����<�A�� <@��2��Q�w����d8��3�^��#�@��n�j��g}��֜�,f.xA{��zJ��F�P��� a�xD��J��W��zJ[�v߃��'Q���mYV?�Ê�,�5���{�]5I�]#4r��U�����;�JG�7��Cs�kaO:���� b@$�2"yE��t�+�<�-�3�D���.E�Q6?�����4�Ɯ����/EG�������T@���W&� )$�H!�q:)�u%�<�Ǒ���WM�'��G�l�/�jd�c�1O�wK��AM?��i�3�R��כ���s#2 �7������ڟ%�+�=	�����8�U��JƮ��М���w���iɈ��L�hq�QR��elG��J���2����T�-!kǟ�_�L�8$��u�Ȗ��r*k�v�;9)sP��b���	�o�u�m���Rz�?��n������Y��+���<��a=8��Rg�B��)�.A�Un�Ti\��6SX��=2*4z�7�e	I�d  <�s�c�=��<	gaF����]���m����sG��9ߒ+U��*1���9�}n�ݞ/��G������١�%;�OR��P�ŏ;���̩N~�-�١���$0�����M�L�q�$؁έ������>3��d��L�z�tLgm��=ݹ�����~�'�o�".no��֘��L'��=Z�G��j�9k�˰��gv<u߃K�3�L��C}�|�d�N��,�=�ly0�s�l֯�j%a�Y��2$�#u�g������ư�^oѓs#y%uMv#(��S��2Μw������[̹�M67^#��O�#`���4=�[�W<G3�< �\���qu��	��ל�72 ��9��Q�����7|��d �dBNf���Ÿ1�O{�1]���F/�C��M�6o�G�ij��*�Z�yO�2�8�����z�,��߼�����bږ��
Eq�Sf���mbM���$E��ݤ�z�G�?x�M�3"�M��Ӥ��q)`��Kx�����c�j��l����8.��T�\�/��n��6 �1�4�˘f2O(���/�z0�D����X�J�3`��hSćB��~��ӣ�`���;r�b�`8�X�Q������r�},��d�d ,O>~�;ܿ���ɍ�T>+d�;8��Fq6#7vZ�ף}�T�m���~������l�e���a
}%lN,x���;��l�} {�)"�"؁�2�.F@�67t�:�k��[��i7��Q_��]��0o�)��v
� ĕ��,Q#dP��o���f�''�0��% u��h� L���Ñ[���)��P���a��݂���*�N��Bw�֘���� �aL�4qs�r�gd�Ċer�.G��v���m���ۣw4;�zՆ@o�b}����Ӂ�JE�(�~�����2��R�K�{��_<���H�t'r��y���z;�K>�[Fn��ƼF�ļw�k�t�qL4В�-ţ�3 ��Iו�� �'!�<Q�y%��(�|�$Ҥ�GG���4Ӷ̴Aݟ��>�P㩵�ωї��;f*L@��� D����	��Y���~���2�`�o,���4e/�����]�=�Zօ^�H�'s$3���aR��v�=�_��L@�DÛA�����W��x�ʭs%�A�9������/m���|�u:��؝���{�q�4�k����&�WW=��d M���IN���c.| 	V�%�G��P�V�+��̘��lO����)G��)c7b�V_(�̼���3 
�Ar�Y� ��y����C�,�1���	���ڬ��|n�:�v�awcGf�]�����'ָ�h�;��ш�$L���`~����� �'ė&�9$�l�&���la�X>Ym����ϗ|�]}������ﯶ�n����uY%�< ItI�I��$�L.J��zI�^�JW鲢̂4v����d��o�p�T��f��>oS�0��&�YSOf{ڳ�Ϳ��[�����,���Y�ȔE��kD1WT�{�t�N�?z���1���r���Kj}+H��m�c_?̚���^��C��k�6����ZD�RU�o��:�,8r��}�, �L%���6���rNٴ�⥏C1U�!��$��vi�)�u<X�$9>�8��S�D��7��:iЊ1nY�1�xq�,�e$A"��a���5�'�!^���˵Ov_Y�ŋc��ui�nWk�v(��Qmd
�<w�y"5�ɴ�V����g���ȷ�b@�y ���gJ���n2o�k7Mv�����"eǳK/W�;9�i�cH������,NV�L�u$�9@iOEՉ�X�dc����Y�!�!OfI��ǉa��Z
�B�p��Lt�.S�v�%gy�F�T��y�/?�gYf�@Q�h�����P툒%T%%��kK�GϨf�~��m��;-?�H��>O>��y	(���%{z�!m��Tɷ�*&%^��g�m�O�6k�Q�)�����z�ʙ�=��t`�Li@��qC�ν�:��>�'��D;O����V��?�te��X�lv˳���}�͵�/ *�N���U�ݕ���:��V8�������E	��Q�̲��N�%�`�13+(�m��R|9����><�F�@K,A��"�9Q6�/������ɂ��fs�N�MK�Ou�q &���W_;����n΃\e@��a�%�`�q���o֛Ы�:}Y���"X&,�d���F�_����'�`�|x��k>��pd�C}<p�]����H��_N۸���k��dwP���EL�����,�>`�V�3�x?�.,ٱ�êm���#z�\��m�s<F��WB��VjMW;7VO�*�Y���V"6rl�:���k�(��qU]e@QEz<�,�,2/�\�ڃ��2#��\XOo��$����7nG��l�A쬦�M8A�6N�$��c8 (  ����g4�)Mj}_���|������d���凞�	\�ʀZ�MVZ�ﲈ�eU���N^�ʐ;FV������&�w��e�W��FjW��������?���dȦ�����'�c
�/6_���Bl�f���R�V�7��	o�>���o�n���G�J�j���k�	5I�d�e��)Z����O��J�n�;W��۪��࣎��{�A�?~��')x���2�V��To�Mx��qv��S2�/4���ٍ����a����3>��F���-68cI뮵�8|�_���W�`ɕ�+����7�7�>�&�O��Y��۔���ة()L!WL�	`q�f4���Z7���_��g^O�[$_�[��開_A���U�٣�d{�
ztG�[K�]Վ~��j�>H�i3��7���o��2�7�nd���l-����du���������QX[ՅT�Y�3H����o�	YԮ�6(��WQ<a�ĤN������Q=F&��?c���<��zw��k���\��M��U�J�̨D|e��8Š1���7�j1�0.6{V#6�G��%�ǧ?s�ߘx������Ư��
Z^�zٲ��K��&O�����F���kqI�	"������eU���N�_r��%�/��sE�p1v��݋hTs�Ud���i�r��l4z֍�Ô��5��䀤�Am'9u�o���w9}��t��a �Z�̨�z �6�,R���'�?���>�	���i �I�ʾ8l#0'3Q3e��\�����"P�n~��Tc�KR�]Fc8]%�w��ܜ�ʀ���ʖ��D;��>}�������:N���~�u�4��i�������FM^l݀�Gh,b��8U�L4[�3;�
�n���2�t�(Q��� `Ǝ*�,
��ע�8��k�J`��R������3v��.K���Z�J� M?��i㉛e���1vZ�1�_�ӧ�]W&�]����=�2��2�wfxF�~e�d@{,�/S���{�\�v�8
���	�2�O�6yg��z�'�B,��Q�i�����z-�� P�[�5	�sbT��D�׸O�|ALO֘{g��`h�O��(��l�y� ��v���,���=N��Ӏ�2���V����XXBm}��Gu�I/��[J�v+�����^��LD�e�S�ݙ�����l�I*5I�
a߉Bd`�L`be9���4����"��RmN�EC��\X��rܝ%��V�����xT.^�����n"R����Wf�!�j�Å��v�v��W=�A�I1t���\��a���T��Ϫ�h׭E[���0����mq�	w��2�(��w��X��JS�tn�����n��AO� ^⶷N��_�.�����?���N����P`�:�ʸM=�"$������F���1n.�+ȑ(5���gZ�R����k��
�q7��2�P�,ۃ��pߪb��}"�K���yl�$0q����Z�;����3Y&��!���v�!7�'�Q��ݰJG�p��7��L4x>�����ʬ�|R��=F�贤�U�K`ٮ�*�?w�F�d 3����O6l)�`��&��3��'���~�w<<8�J�|ׁ�f���Kh�q���H��3ˉ�9�D�3#��	�At��F�R��/�Q,LRP&�� �o_���<0SeC��z���C���Y��׳�h,�	����Pw|>ޞS^�upmt��u���m�8���ZJ�5���Ed@���VV+h�-b���"'�^��}�9���O^�_�����h��N��eD��e]`�V��Q��P��i{��umt��سF$��6��醬E�95](s1��x�4�+{o�2�k��K����U,�fL���N��������_�N*��5�����Q�v&ωJ�cY����b�����T�M��p�=F�F/�c�����7�e��e��v��q$ej��L3�U�nE�R�x��h��E�����y�\�͓�p�I�.�'��J�!�|���ܪw�x%�og�f7�.a��l��x1 �� Ea�́���0���j�G�k!~�18=#<�10n4�U~Y?Q��
���z�Rx�%=f��m�͹�y�8�ܭ6�)Kb���'�z��q���Η�G����[��,]0�����j�|���7�4��t�`��+��^�\�}g3��t�tD�yci¬��y-�c��7뉱e��G���S�RŨf�l��`��<��A�_*���z��y�&;<1��v�F���~������f�YCv������K�4�U@�S8�O�}��xi��������)��f�^��#l�פ��r��ZD�"9�Ŏt��q�nˌ��%��-�@B���@8ei=1�x��=��K�|�I�<y^z3��ޜ�����`�q��6mOO�j��j��o6	��Z(�2ŗɀ(����n�|���@QS��݈({����v�d���5=-2Sv����֒��k��
�$�5	�=�л��^��/��R�i�b����KO�BkT��x����>=�b$����C�L���Z��/=��􀛕�"�yX��g5�8��q{������ŧ����L����7:�-}v�m����6����J
�z"2�P��rd�&��l's��v�C�H� �@��X�y4��gm�0U�tժ9t����xh�5�L1�k��c����k�*W*��i8P4�T��оy�4�G�4 c���73
��[�Y�kj�퍹�1�Ɇb�|�-ҭ�Z��B8P��>`f]��P��\���߷���ǧgh��E���{�M,�F���v6�9�lM⁸��q���a�'���L�^KxQ�$~�t����2?�gY�
NU�fѲȞ�u��>(܂���м�)m�o!��8���<���j��~��Xq�����K_��8�+�Q�/��V]L���H:�D������m����~�+F&}	=*e,�WpS��Tp�T[M$̀f�|���L��!��>���щ}�����V���`w��Vm�HE���)W�pä����2���2 �ɟl<֜ȁ
��Y9��sW6��k�2r�Wq7n��CFaW#l��u�����xon$�N��H:,�Y��ǝ�G�+��-���QTJ��g�¤֮x�g�����r4e�$�Y�gQ� (�6�o�F�/}WI�����I�S<���Y�i��D������y���h�Aw�>\(e�/úS��'��||��é���G~�#��&ӵgv���|/a�B_w|ܔ�.����3�#e˾�/��E�z#����|���k�~7�n��^�0Er�?��7;(@q6��0N�b���������x�4{h��⇣�>�V���3H�)"�}����)sV���W��~U��=�
Fb��;��-���R<A��s��V'�Y�>�]�%�ծ�Cl���[j[���fG�e�U��`O�@���8{c�%H��5"
�&\f�)�������J}�a�r:?B���� %t��.'�	������^��9��'��<ޓ<rW+��0w���E�	�ի�+<��� LF3}�t����t�}kr��ҭ<t����2���>�����.����<$�;��(Hۄ�_`8!��.�-�:��^���=IMEfb�]0O��p�/g�]uK�n3c�}�<�X�M���,�`�T@����9:�%��dC���Boj����z6�˴�������/��I�Ǔ-@�b8,7�r|v�"���e͔�	1�Q��`]4�%�(�i�j�8�K�ܢ�s��%s�וᒝw��xU7��]�����H��q�_[&���??>>�/35]            x������ � �     