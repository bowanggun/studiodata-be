PGDMP                     	    {         
   datindrive    15.4    15.4 �    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    16398 
   datindrive    DATABASE     �   CREATE DATABASE datindrive WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'English_Indonesia.1252';
    DROP DATABASE datindrive;
                postgres    false                        2615    16399    v1    SCHEMA        CREATE SCHEMA v1;
    DROP SCHEMA v1;
                postgres    false            �            1259    16400    failed_jobs    TABLE     "  CREATE TABLE v1.failed_jobs (
    id bigint NOT NULL,
    uuid character varying(255) NOT NULL,
    connection text NOT NULL,
    queue text NOT NULL,
    payload text NOT NULL,
    exception text NOT NULL,
    failed_at timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);
    DROP TABLE v1.failed_jobs;
       v1         heap    postgres    false    6            �            1259    16406    failed_jobs_id_seq    SEQUENCE     w   CREATE SEQUENCE v1.failed_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE v1.failed_jobs_id_seq;
       v1          postgres    false    6    215            �           0    0    failed_jobs_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE v1.failed_jobs_id_seq OWNED BY v1.failed_jobs.id;
          v1          postgres    false    216            �            1259    16407 
   migrations    TABLE     �   CREATE TABLE v1.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);
    DROP TABLE v1.migrations;
       v1         heap    postgres    false    6            �            1259    16410    migrations_id_seq    SEQUENCE     �   CREATE SEQUENCE v1.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE v1.migrations_id_seq;
       v1          postgres    false    217    6            �           0    0    migrations_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE v1.migrations_id_seq OWNED BY v1.migrations.id;
          v1          postgres    false    218            �            1259    16411    mst_collection    TABLE     �  CREATE TABLE v1.mst_collection (
    id bigint NOT NULL,
    nama_collection text NOT NULL,
    judul text,
    tipe character varying(50),
    default_chart character varying(50),
    referensi_data text,
    catatan text,
    route_name text,
    table_name text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    ref_organisasi_id integer,
    organisasi text,
    satuan character varying(100),
    definisi text,
    rumus_perhitungan text,
    cara_memperoleh_data text,
    urusan text,
    is_multidimensi boolean DEFAULT false NOT NULL
);
    DROP TABLE v1.mst_collection;
       v1         heap    postgres    false    6            �            1259    16416    mst_collection_id_seq    SEQUENCE     z   CREATE SEQUENCE v1.mst_collection_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE v1.mst_collection_id_seq;
       v1          postgres    false    219    6            �           0    0    mst_collection_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE v1.mst_collection_id_seq OWNED BY v1.mst_collection.id;
          v1          postgres    false    220            �            1259    16417    mst_dashboard    TABLE     �  CREATE TABLE v1.mst_dashboard (
    id bigint NOT NULL,
    nama_dashboard text NOT NULL,
    frame text NOT NULL,
    link text NOT NULL,
    catatan text,
    is_hidden boolean DEFAULT false NOT NULL,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.mst_dashboard;
       v1         heap    postgres    false    6            �            1259    16423    mst_dashboard_id_seq    SEQUENCE     y   CREATE SEQUENCE v1.mst_dashboard_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE v1.mst_dashboard_id_seq;
       v1          postgres    false    6    221            �           0    0    mst_dashboard_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE v1.mst_dashboard_id_seq OWNED BY v1.mst_dashboard.id;
          v1          postgres    false    222            �            1259    16424    mst_jenis_dok    TABLE     [  CREATE TABLE v1.mst_jenis_dok (
    id bigint NOT NULL,
    jenis_dokumen character varying(200) NOT NULL,
    ref_grup_dok_id integer,
    keterangan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.mst_jenis_dok;
       v1         heap    postgres    false    6            �            1259    16429    mst_jenis_dok_id_seq    SEQUENCE     y   CREATE SEQUENCE v1.mst_jenis_dok_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE v1.mst_jenis_dok_id_seq;
       v1          postgres    false    223    6            �           0    0    mst_jenis_dok_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE v1.mst_jenis_dok_id_seq OWNED BY v1.mst_jenis_dok.id;
          v1          postgres    false    224            �            1259    16430    password_reset_tokens    TABLE     �   CREATE TABLE v1.password_reset_tokens (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);
 %   DROP TABLE v1.password_reset_tokens;
       v1         heap    postgres    false    6            �            1259    16435    personal_access_tokens    TABLE     �  CREATE TABLE v1.personal_access_tokens (
    id bigint NOT NULL,
    tokenable_type character varying(255) NOT NULL,
    tokenable_id bigint NOT NULL,
    name character varying(255) NOT NULL,
    token character varying(64) NOT NULL,
    abilities text,
    last_used_at timestamp(0) without time zone,
    expires_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
 &   DROP TABLE v1.personal_access_tokens;
       v1         heap    postgres    false    6            �            1259    16440    personal_access_tokens_id_seq    SEQUENCE     �   CREATE SEQUENCE v1.personal_access_tokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE v1.personal_access_tokens_id_seq;
       v1          postgres    false    226    6            �           0    0    personal_access_tokens_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE v1.personal_access_tokens_id_seq OWNED BY v1.personal_access_tokens.id;
          v1          postgres    false    227            �            1259    16441    ref_data_tag    TABLE     &  CREATE TABLE v1.ref_data_tag (
    id bigint NOT NULL,
    data_tag text NOT NULL,
    keterangan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.ref_data_tag;
       v1         heap    postgres    false    6            �            1259    16446    ref_data_tag_id_seq    SEQUENCE     x   CREATE SEQUENCE v1.ref_data_tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE v1.ref_data_tag_id_seq;
       v1          postgres    false    228    6            �           0    0    ref_data_tag_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE v1.ref_data_tag_id_seq OWNED BY v1.ref_data_tag.id;
          v1          postgres    false    229            �            1259    16447    ref_grup_dok    TABLE     <  CREATE TABLE v1.ref_grup_dok (
    id bigint NOT NULL,
    grup_dokumen character varying(200) NOT NULL,
    keterangan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.ref_grup_dok;
       v1         heap    postgres    false    6            �            1259    16452    ref_grup_dok_id_seq    SEQUENCE     x   CREATE SEQUENCE v1.ref_grup_dok_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE v1.ref_grup_dok_id_seq;
       v1          postgres    false    6    230            �           0    0    ref_grup_dok_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE v1.ref_grup_dok_id_seq OWNED BY v1.ref_grup_dok.id;
          v1          postgres    false    231            �            1259    24617    ref_kode_wilayah    TABLE     �  CREATE TABLE v1.ref_kode_wilayah (
    id bigint NOT NULL,
    kemendagri_provinsi_kode character varying(20),
    kemendagri_kota_kode character varying(50),
    kemendagri_provinsi_nama character varying(50),
    kemendagri_kota_nama text NOT NULL,
    bps_provinsi_kode character varying(50),
    bps_kota_kode character varying(50),
    bps_provinsi_nama character varying(50),
    bps_kota_nama text NOT NULL,
    latitude text,
    longitude text,
    kode_pos character varying(200),
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
     DROP TABLE v1.ref_kode_wilayah;
       v1         heap    postgres    false    6            �            1259    24616    ref_kode_wilayah_id_seq    SEQUENCE     |   CREATE SEQUENCE v1.ref_kode_wilayah_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE v1.ref_kode_wilayah_id_seq;
       v1          postgres    false    6    253            �           0    0    ref_kode_wilayah_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE v1.ref_kode_wilayah_id_seq OWNED BY v1.ref_kode_wilayah.id;
          v1          postgres    false    252            �            1259    16453    ref_organisasi    TABLE     P  CREATE TABLE v1.ref_organisasi (
    id bigint NOT NULL,
    organisasi character varying(200) NOT NULL,
    singkatan text,
    keterangan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.ref_organisasi;
       v1         heap    postgres    false    6            �            1259    16458    ref_organisasi_id_seq    SEQUENCE     z   CREATE SEQUENCE v1.ref_organisasi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE v1.ref_organisasi_id_seq;
       v1          postgres    false    232    6            �           0    0    ref_organisasi_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE v1.ref_organisasi_id_seq OWNED BY v1.ref_organisasi.id;
          v1          postgres    false    233            �            1259    16459 	   ref_topik    TABLE     2  CREATE TABLE v1.ref_topik (
    id bigint NOT NULL,
    topik character varying(200) NOT NULL,
    keterangan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.ref_topik;
       v1         heap    postgres    false    6            �            1259    16464    ref_topik_id_seq    SEQUENCE     u   CREATE SEQUENCE v1.ref_topik_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE v1.ref_topik_id_seq;
       v1          postgres    false    6    234            �           0    0    ref_topik_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE v1.ref_topik_id_seq OWNED BY v1.ref_topik.id;
          v1          postgres    false    235            �            1259    24601    trx_collection    TABLE       CREATE TABLE v1.trx_collection (
    id bigint NOT NULL,
    mst_collection_id integer,
    tahun integer NOT NULL,
    target text,
    realisasi text,
    sumber text,
    catatan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    kemendagri_kota_kode character varying(50) DEFAULT '32.71'::character varying NOT NULL,
    kemendagri_kota_nama text DEFAULT 'KOTA BOGOR'::text NOT NULL
);
    DROP TABLE v1.trx_collection;
       v1         heap    postgres    false    6            �            1259    24600    trx_collection_id_seq    SEQUENCE     z   CREATE SEQUENCE v1.trx_collection_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE v1.trx_collection_id_seq;
       v1          postgres    false    6    251            �           0    0    trx_collection_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE v1.trx_collection_id_seq OWNED BY v1.trx_collection.id;
          v1          postgres    false    250            �            1259    16465    trx_data    TABLE     4  CREATE TABLE v1.trx_data (
    id bigint NOT NULL,
    mst_jenis_dok_id integer,
    sumber_data text NOT NULL,
    jenis_data character varying(100) NOT NULL,
    nama_data text NOT NULL,
    deskripsi_data text NOT NULL,
    format_data character varying(100) NOT NULL,
    media_type character varying(100),
    size_data character varying(100),
    ekstensi_data character varying(100),
    nama_file text,
    url_file_upload text,
    url_upload text,
    is_shared boolean DEFAULT false NOT NULL,
    webbappeda_visible boolean DEFAULT false NOT NULL,
    satudata_visible boolean DEFAULT false NOT NULL,
    catatan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.trx_data;
       v1         heap    postgres    false    6            �            1259    16473    trx_data_id_seq    SEQUENCE     t   CREATE SEQUENCE v1.trx_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE v1.trx_data_id_seq;
       v1          postgres    false    6    236            �           0    0    trx_data_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE v1.trx_data_id_seq OWNED BY v1.trx_data.id;
          v1          postgres    false    237            �            1259    16474    trx_data_organisasi    TABLE     R  CREATE TABLE v1.trx_data_organisasi (
    id bigint NOT NULL,
    trx_data_id integer,
    ref_organisasi_id integer,
    organisasi text NOT NULL,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
 #   DROP TABLE v1.trx_data_organisasi;
       v1         heap    postgres    false    6            �            1259    16479    trx_data_organisasi_id_seq    SEQUENCE        CREATE SEQUENCE v1.trx_data_organisasi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE v1.trx_data_organisasi_id_seq;
       v1          postgres    false    6    238            �           0    0    trx_data_organisasi_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE v1.trx_data_organisasi_id_seq OWNED BY v1.trx_data_organisasi.id;
          v1          postgres    false    239            �            1259    16480    trx_data_tag    TABLE     G  CREATE TABLE v1.trx_data_tag (
    id bigint NOT NULL,
    trx_data_id integer,
    ref_data_tag_id integer,
    data_tag text NOT NULL,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.trx_data_tag;
       v1         heap    postgres    false    6            �            1259    16485    trx_data_tag_id_seq    SEQUENCE     x   CREATE SEQUENCE v1.trx_data_tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE v1.trx_data_tag_id_seq;
       v1          postgres    false    6    240            �           0    0    trx_data_tag_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE v1.trx_data_tag_id_seq OWNED BY v1.trx_data_tag.id;
          v1          postgres    false    241            �            1259    16486    trx_data_topik    TABLE     C  CREATE TABLE v1.trx_data_topik (
    id bigint NOT NULL,
    trx_data_id integer,
    ref_topik_id integer,
    topik text NOT NULL,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.trx_data_topik;
       v1         heap    postgres    false    6            �            1259    16491    trx_data_topik_id_seq    SEQUENCE     z   CREATE SEQUENCE v1.trx_data_topik_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE v1.trx_data_topik_id_seq;
       v1          postgres    false    6    242            �           0    0    trx_data_topik_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE v1.trx_data_topik_id_seq OWNED BY v1.trx_data_topik.id;
          v1          postgres    false    243            �            1259    16492 $   trx_luas_wilayah_kecamatan_kelurahan    TABLE       CREATE TABLE v1.trx_luas_wilayah_kecamatan_kelurahan (
    id bigint NOT NULL,
    mst_collection_id integer NOT NULL,
    tahun integer NOT NULL,
    kecamatan character varying(50) NOT NULL,
    jml_kelurahan integer NOT NULL,
    luas_wilayah double precision NOT NULL,
    satuan character varying(50),
    catatan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
 4   DROP TABLE v1.trx_luas_wilayah_kecamatan_kelurahan;
       v1         heap    postgres    false    6            �            1259    16497 +   trx_luas_wilayah_kecamatan_kelurahan_id_seq    SEQUENCE     �   CREATE SEQUENCE v1.trx_luas_wilayah_kecamatan_kelurahan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 >   DROP SEQUENCE v1.trx_luas_wilayah_kecamatan_kelurahan_id_seq;
       v1          postgres    false    244    6            �           0    0 +   trx_luas_wilayah_kecamatan_kelurahan_id_seq    SEQUENCE OWNED BY     s   ALTER SEQUENCE v1.trx_luas_wilayah_kecamatan_kelurahan_id_seq OWNED BY v1.trx_luas_wilayah_kecamatan_kelurahan.id;
          v1          postgres    false    245            �            1259    16498    trx_riwayat_data    TABLE     U  CREATE TABLE v1.trx_riwayat_data (
    id bigint NOT NULL,
    trx_data_id integer,
    mst_jenis_dok_id integer,
    sumber_data text NOT NULL,
    jenis_data character varying(100) NOT NULL,
    nama_data text NOT NULL,
    deskripsi_data text NOT NULL,
    format_data character varying(100) NOT NULL,
    media_type character varying(100),
    size_data character varying(100),
    ekstensi_data character varying(100),
    nama_file text,
    url_file_upload text,
    url_upload text,
    is_shared boolean DEFAULT false NOT NULL,
    webbappeda_visible boolean DEFAULT false NOT NULL,
    satudata_visible boolean DEFAULT false NOT NULL,
    catatan text,
    created_by character varying(50) NOT NULL,
    updated_by character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
     DROP TABLE v1.trx_riwayat_data;
       v1         heap    postgres    false    6            �            1259    16506    trx_riwayat_data_id_seq    SEQUENCE     |   CREATE SEQUENCE v1.trx_riwayat_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE v1.trx_riwayat_data_id_seq;
       v1          postgres    false    6    246            �           0    0    trx_riwayat_data_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE v1.trx_riwayat_data_id_seq OWNED BY v1.trx_riwayat_data.id;
          v1          postgres    false    247            �            1259    16507    users    TABLE     t  CREATE TABLE v1.users (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    email_verified_at timestamp(0) without time zone,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE v1.users;
       v1         heap    postgres    false    6            �            1259    16512    users_id_seq    SEQUENCE     q   CREATE SEQUENCE v1.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
    DROP SEQUENCE v1.users_id_seq;
       v1          postgres    false    248    6            �           0    0    users_id_seq    SEQUENCE OWNED BY     5   ALTER SEQUENCE v1.users_id_seq OWNED BY v1.users.id;
          v1          postgres    false    249            �           2604    16513    failed_jobs id    DEFAULT     h   ALTER TABLE ONLY v1.failed_jobs ALTER COLUMN id SET DEFAULT nextval('v1.failed_jobs_id_seq'::regclass);
 9   ALTER TABLE v1.failed_jobs ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    216    215            �           2604    16514    migrations id    DEFAULT     f   ALTER TABLE ONLY v1.migrations ALTER COLUMN id SET DEFAULT nextval('v1.migrations_id_seq'::regclass);
 8   ALTER TABLE v1.migrations ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    218    217            �           2604    16515    mst_collection id    DEFAULT     n   ALTER TABLE ONLY v1.mst_collection ALTER COLUMN id SET DEFAULT nextval('v1.mst_collection_id_seq'::regclass);
 <   ALTER TABLE v1.mst_collection ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    220    219            �           2604    16516    mst_dashboard id    DEFAULT     l   ALTER TABLE ONLY v1.mst_dashboard ALTER COLUMN id SET DEFAULT nextval('v1.mst_dashboard_id_seq'::regclass);
 ;   ALTER TABLE v1.mst_dashboard ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    222    221            �           2604    16517    mst_jenis_dok id    DEFAULT     l   ALTER TABLE ONLY v1.mst_jenis_dok ALTER COLUMN id SET DEFAULT nextval('v1.mst_jenis_dok_id_seq'::regclass);
 ;   ALTER TABLE v1.mst_jenis_dok ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    224    223            �           2604    16518    personal_access_tokens id    DEFAULT     ~   ALTER TABLE ONLY v1.personal_access_tokens ALTER COLUMN id SET DEFAULT nextval('v1.personal_access_tokens_id_seq'::regclass);
 D   ALTER TABLE v1.personal_access_tokens ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    227    226            �           2604    16519    ref_data_tag id    DEFAULT     j   ALTER TABLE ONLY v1.ref_data_tag ALTER COLUMN id SET DEFAULT nextval('v1.ref_data_tag_id_seq'::regclass);
 :   ALTER TABLE v1.ref_data_tag ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    229    228            �           2604    16520    ref_grup_dok id    DEFAULT     j   ALTER TABLE ONLY v1.ref_grup_dok ALTER COLUMN id SET DEFAULT nextval('v1.ref_grup_dok_id_seq'::regclass);
 :   ALTER TABLE v1.ref_grup_dok ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    231    230            �           2604    24620    ref_kode_wilayah id    DEFAULT     r   ALTER TABLE ONLY v1.ref_kode_wilayah ALTER COLUMN id SET DEFAULT nextval('v1.ref_kode_wilayah_id_seq'::regclass);
 >   ALTER TABLE v1.ref_kode_wilayah ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    252    253    253            �           2604    16521    ref_organisasi id    DEFAULT     n   ALTER TABLE ONLY v1.ref_organisasi ALTER COLUMN id SET DEFAULT nextval('v1.ref_organisasi_id_seq'::regclass);
 <   ALTER TABLE v1.ref_organisasi ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    233    232            �           2604    16522    ref_topik id    DEFAULT     d   ALTER TABLE ONLY v1.ref_topik ALTER COLUMN id SET DEFAULT nextval('v1.ref_topik_id_seq'::regclass);
 7   ALTER TABLE v1.ref_topik ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    235    234            �           2604    24604    trx_collection id    DEFAULT     n   ALTER TABLE ONLY v1.trx_collection ALTER COLUMN id SET DEFAULT nextval('v1.trx_collection_id_seq'::regclass);
 <   ALTER TABLE v1.trx_collection ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    250    251    251            �           2604    16523    trx_data id    DEFAULT     b   ALTER TABLE ONLY v1.trx_data ALTER COLUMN id SET DEFAULT nextval('v1.trx_data_id_seq'::regclass);
 6   ALTER TABLE v1.trx_data ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    237    236            �           2604    16524    trx_data_organisasi id    DEFAULT     x   ALTER TABLE ONLY v1.trx_data_organisasi ALTER COLUMN id SET DEFAULT nextval('v1.trx_data_organisasi_id_seq'::regclass);
 A   ALTER TABLE v1.trx_data_organisasi ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    239    238            �           2604    16525    trx_data_tag id    DEFAULT     j   ALTER TABLE ONLY v1.trx_data_tag ALTER COLUMN id SET DEFAULT nextval('v1.trx_data_tag_id_seq'::regclass);
 :   ALTER TABLE v1.trx_data_tag ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    241    240            �           2604    16526    trx_data_topik id    DEFAULT     n   ALTER TABLE ONLY v1.trx_data_topik ALTER COLUMN id SET DEFAULT nextval('v1.trx_data_topik_id_seq'::regclass);
 <   ALTER TABLE v1.trx_data_topik ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    243    242            �           2604    16527 '   trx_luas_wilayah_kecamatan_kelurahan id    DEFAULT     �   ALTER TABLE ONLY v1.trx_luas_wilayah_kecamatan_kelurahan ALTER COLUMN id SET DEFAULT nextval('v1.trx_luas_wilayah_kecamatan_kelurahan_id_seq'::regclass);
 R   ALTER TABLE v1.trx_luas_wilayah_kecamatan_kelurahan ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    245    244            �           2604    16528    trx_riwayat_data id    DEFAULT     r   ALTER TABLE ONLY v1.trx_riwayat_data ALTER COLUMN id SET DEFAULT nextval('v1.trx_riwayat_data_id_seq'::regclass);
 >   ALTER TABLE v1.trx_riwayat_data ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    247    246            �           2604    16529    users id    DEFAULT     \   ALTER TABLE ONLY v1.users ALTER COLUMN id SET DEFAULT nextval('v1.users_id_seq'::regclass);
 3   ALTER TABLE v1.users ALTER COLUMN id DROP DEFAULT;
       v1          postgres    false    249    248            �          0    16400    failed_jobs 
   TABLE DATA           ]   COPY v1.failed_jobs (id, uuid, connection, queue, payload, exception, failed_at) FROM stdin;
    v1          postgres    false    215   3�       �          0    16407 
   migrations 
   TABLE DATA           6   COPY v1.migrations (id, migration, batch) FROM stdin;
    v1          postgres    false    217   P�       �          0    16411    mst_collection 
   TABLE DATA           )  COPY v1.mst_collection (id, nama_collection, judul, tipe, default_chart, referensi_data, catatan, route_name, table_name, created_by, updated_by, created_at, updated_at, ref_organisasi_id, organisasi, satuan, definisi, rumus_perhitungan, cara_memperoleh_data, urusan, is_multidimensi) FROM stdin;
    v1          postgres    false    219   �       �          0    16417    mst_dashboard 
   TABLE DATA           �   COPY v1.mst_dashboard (id, nama_dashboard, frame, link, catatan, is_hidden, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    221   ��       �          0    16424    mst_jenis_dok 
   TABLE DATA           �   COPY v1.mst_jenis_dok (id, jenis_dokumen, ref_grup_dok_id, keterangan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    223    �       �          0    16430    password_reset_tokens 
   TABLE DATA           E   COPY v1.password_reset_tokens (email, token, created_at) FROM stdin;
    v1          postgres    false    225   Q�       �          0    16435    personal_access_tokens 
   TABLE DATA           �   COPY v1.personal_access_tokens (id, tokenable_type, tokenable_id, name, token, abilities, last_used_at, expires_at, created_at, updated_at) FROM stdin;
    v1          postgres    false    226   n�       �          0    16441    ref_data_tag 
   TABLE DATA           l   COPY v1.ref_data_tag (id, data_tag, keterangan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    228   ��       �          0    16447    ref_grup_dok 
   TABLE DATA           p   COPY v1.ref_grup_dok (id, grup_dokumen, keterangan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    230   t      �          0    24617    ref_kode_wilayah 
   TABLE DATA             COPY v1.ref_kode_wilayah (id, kemendagri_provinsi_kode, kemendagri_kota_kode, kemendagri_provinsi_nama, kemendagri_kota_nama, bps_provinsi_kode, bps_kota_kode, bps_provinsi_nama, bps_kota_nama, latitude, longitude, kode_pos, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    253   (      �          0    16453    ref_organisasi 
   TABLE DATA           {   COPY v1.ref_organisasi (id, organisasi, singkatan, keterangan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    232   �	      �          0    16459 	   ref_topik 
   TABLE DATA           f   COPY v1.ref_topik (id, topik, keterangan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    234   �      �          0    24601    trx_collection 
   TABLE DATA           �   COPY v1.trx_collection (id, mst_collection_id, tahun, target, realisasi, sumber, catatan, created_by, updated_by, created_at, updated_at, kemendagri_kota_kode, kemendagri_kota_nama) FROM stdin;
    v1          postgres    false    251   J      �          0    16465    trx_data 
   TABLE DATA           -  COPY v1.trx_data (id, mst_jenis_dok_id, sumber_data, jenis_data, nama_data, deskripsi_data, format_data, media_type, size_data, ekstensi_data, nama_file, url_file_upload, url_upload, is_shared, webbappeda_visible, satudata_visible, catatan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    236   �Y      �          0    16474    trx_data_organisasi 
   TABLE DATA           �   COPY v1.trx_data_organisasi (id, trx_data_id, ref_organisasi_id, organisasi, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    238   ܤ      �          0    16480    trx_data_tag 
   TABLE DATA           ~   COPY v1.trx_data_tag (id, trx_data_id, ref_data_tag_id, data_tag, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    240   �      �          0    16486    trx_data_topik 
   TABLE DATA           z   COPY v1.trx_data_topik (id, trx_data_id, ref_topik_id, topik, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    242   ,�      �          0    16492 $   trx_luas_wilayah_kecamatan_kelurahan 
   TABLE DATA           �   COPY v1.trx_luas_wilayah_kecamatan_kelurahan (id, mst_collection_id, tahun, kecamatan, jml_kelurahan, luas_wilayah, satuan, catatan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    244   8�      �          0    16498    trx_riwayat_data 
   TABLE DATA           B  COPY v1.trx_riwayat_data (id, trx_data_id, mst_jenis_dok_id, sumber_data, jenis_data, nama_data, deskripsi_data, format_data, media_type, size_data, ekstensi_data, nama_file, url_file_upload, url_upload, is_shared, webbappeda_visible, satudata_visible, catatan, created_by, updated_by, created_at, updated_at) FROM stdin;
    v1          postgres    false    246   �      �          0    16507    users 
   TABLE DATA           q   COPY v1.users (id, name, email, email_verified_at, password, remember_token, created_at, updated_at) FROM stdin;
    v1          postgres    false    248   �      �           0    0    failed_jobs_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('v1.failed_jobs_id_seq', 1, false);
          v1          postgres    false    216            �           0    0    migrations_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('v1.migrations_id_seq', 27, true);
          v1          postgres    false    218            �           0    0    mst_collection_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('v1.mst_collection_id_seq', 284, true);
          v1          postgres    false    220            �           0    0    mst_dashboard_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('v1.mst_dashboard_id_seq', 3, true);
          v1          postgres    false    222            �           0    0    mst_jenis_dok_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('v1.mst_jenis_dok_id_seq', 3, true);
          v1          postgres    false    224            �           0    0    personal_access_tokens_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('v1.personal_access_tokens_id_seq', 1, false);
          v1          postgres    false    227            �           0    0    ref_data_tag_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('v1.ref_data_tag_id_seq', 137, true);
          v1          postgres    false    229            �           0    0    ref_grup_dok_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('v1.ref_grup_dok_id_seq', 2, true);
          v1          postgres    false    231            �           0    0    ref_kode_wilayah_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('v1.ref_kode_wilayah_id_seq', 28, true);
          v1          postgres    false    252            �           0    0    ref_organisasi_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('v1.ref_organisasi_id_seq', 51, true);
          v1          postgres    false    233            �           0    0    ref_topik_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('v1.ref_topik_id_seq', 34, true);
          v1          postgres    false    235            �           0    0    trx_collection_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('v1.trx_collection_id_seq', 1131, true);
          v1          postgres    false    250            �           0    0    trx_data_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('v1.trx_data_id_seq', 249, true);
          v1          postgres    false    237            �           0    0    trx_data_organisasi_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('v1.trx_data_organisasi_id_seq', 85, true);
          v1          postgres    false    239            �           0    0    trx_data_tag_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('v1.trx_data_tag_id_seq', 398, true);
          v1          postgres    false    241            �           0    0    trx_data_topik_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('v1.trx_data_topik_id_seq', 29, true);
          v1          postgres    false    243            �           0    0 +   trx_luas_wilayah_kecamatan_kelurahan_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('v1.trx_luas_wilayah_kecamatan_kelurahan_id_seq', 6, true);
          v1          postgres    false    245            �           0    0    trx_riwayat_data_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('v1.trx_riwayat_data_id_seq', 259, true);
          v1          postgres    false    247            �           0    0    users_id_seq    SEQUENCE SET     7   SELECT pg_catalog.setval('v1.users_id_seq', 1, false);
          v1          postgres    false    249            �           2606    16531    failed_jobs failed_jobs_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY v1.failed_jobs
    ADD CONSTRAINT failed_jobs_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY v1.failed_jobs DROP CONSTRAINT failed_jobs_pkey;
       v1            postgres    false    215            �           2606    16533 #   failed_jobs failed_jobs_uuid_unique 
   CONSTRAINT     Z   ALTER TABLE ONLY v1.failed_jobs
    ADD CONSTRAINT failed_jobs_uuid_unique UNIQUE (uuid);
 I   ALTER TABLE ONLY v1.failed_jobs DROP CONSTRAINT failed_jobs_uuid_unique;
       v1            postgres    false    215            �           2606    16535    migrations migrations_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY v1.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY v1.migrations DROP CONSTRAINT migrations_pkey;
       v1            postgres    false    217            �           2606    16537 "   mst_collection mst_collection_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY v1.mst_collection
    ADD CONSTRAINT mst_collection_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY v1.mst_collection DROP CONSTRAINT mst_collection_pkey;
       v1            postgres    false    219            �           2606    16539     mst_dashboard mst_dashboard_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY v1.mst_dashboard
    ADD CONSTRAINT mst_dashboard_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY v1.mst_dashboard DROP CONSTRAINT mst_dashboard_pkey;
       v1            postgres    false    221            �           2606    16541     mst_jenis_dok mst_jenis_dok_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY v1.mst_jenis_dok
    ADD CONSTRAINT mst_jenis_dok_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY v1.mst_jenis_dok DROP CONSTRAINT mst_jenis_dok_pkey;
       v1            postgres    false    223            �           2606    16543 0   password_reset_tokens password_reset_tokens_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY v1.password_reset_tokens
    ADD CONSTRAINT password_reset_tokens_pkey PRIMARY KEY (email);
 V   ALTER TABLE ONLY v1.password_reset_tokens DROP CONSTRAINT password_reset_tokens_pkey;
       v1            postgres    false    225            �           2606    16545 2   personal_access_tokens personal_access_tokens_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY v1.personal_access_tokens
    ADD CONSTRAINT personal_access_tokens_pkey PRIMARY KEY (id);
 X   ALTER TABLE ONLY v1.personal_access_tokens DROP CONSTRAINT personal_access_tokens_pkey;
       v1            postgres    false    226            �           2606    16547 :   personal_access_tokens personal_access_tokens_token_unique 
   CONSTRAINT     r   ALTER TABLE ONLY v1.personal_access_tokens
    ADD CONSTRAINT personal_access_tokens_token_unique UNIQUE (token);
 `   ALTER TABLE ONLY v1.personal_access_tokens DROP CONSTRAINT personal_access_tokens_token_unique;
       v1            postgres    false    226            �           2606    16549    ref_data_tag ref_data_tag_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY v1.ref_data_tag
    ADD CONSTRAINT ref_data_tag_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY v1.ref_data_tag DROP CONSTRAINT ref_data_tag_pkey;
       v1            postgres    false    228            �           2606    16551    ref_grup_dok ref_grup_dok_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY v1.ref_grup_dok
    ADD CONSTRAINT ref_grup_dok_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY v1.ref_grup_dok DROP CONSTRAINT ref_grup_dok_pkey;
       v1            postgres    false    230                       2606    24624 &   ref_kode_wilayah ref_kode_wilayah_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY v1.ref_kode_wilayah
    ADD CONSTRAINT ref_kode_wilayah_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY v1.ref_kode_wilayah DROP CONSTRAINT ref_kode_wilayah_pkey;
       v1            postgres    false    253            �           2606    16553 "   ref_organisasi ref_organisasi_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY v1.ref_organisasi
    ADD CONSTRAINT ref_organisasi_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY v1.ref_organisasi DROP CONSTRAINT ref_organisasi_pkey;
       v1            postgres    false    232            �           2606    16555    ref_topik ref_topik_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY v1.ref_topik
    ADD CONSTRAINT ref_topik_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY v1.ref_topik DROP CONSTRAINT ref_topik_pkey;
       v1            postgres    false    234                       2606    24608 "   trx_collection trx_collection_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY v1.trx_collection
    ADD CONSTRAINT trx_collection_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY v1.trx_collection DROP CONSTRAINT trx_collection_pkey;
       v1            postgres    false    251                        2606    16557 ,   trx_data_organisasi trx_data_organisasi_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY v1.trx_data_organisasi
    ADD CONSTRAINT trx_data_organisasi_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY v1.trx_data_organisasi DROP CONSTRAINT trx_data_organisasi_pkey;
       v1            postgres    false    238            �           2606    16559    trx_data trx_data_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY v1.trx_data
    ADD CONSTRAINT trx_data_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY v1.trx_data DROP CONSTRAINT trx_data_pkey;
       v1            postgres    false    236                       2606    16561    trx_data_tag trx_data_tag_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY v1.trx_data_tag
    ADD CONSTRAINT trx_data_tag_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY v1.trx_data_tag DROP CONSTRAINT trx_data_tag_pkey;
       v1            postgres    false    240                       2606    16563 "   trx_data_topik trx_data_topik_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY v1.trx_data_topik
    ADD CONSTRAINT trx_data_topik_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY v1.trx_data_topik DROP CONSTRAINT trx_data_topik_pkey;
       v1            postgres    false    242                       2606    16565 N   trx_luas_wilayah_kecamatan_kelurahan trx_luas_wilayah_kecamatan_kelurahan_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY v1.trx_luas_wilayah_kecamatan_kelurahan
    ADD CONSTRAINT trx_luas_wilayah_kecamatan_kelurahan_pkey PRIMARY KEY (id);
 t   ALTER TABLE ONLY v1.trx_luas_wilayah_kecamatan_kelurahan DROP CONSTRAINT trx_luas_wilayah_kecamatan_kelurahan_pkey;
       v1            postgres    false    244                       2606    16567 &   trx_riwayat_data trx_riwayat_data_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY v1.trx_riwayat_data
    ADD CONSTRAINT trx_riwayat_data_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY v1.trx_riwayat_data DROP CONSTRAINT trx_riwayat_data_pkey;
       v1            postgres    false    246            
           2606    16569    users users_email_unique 
   CONSTRAINT     P   ALTER TABLE ONLY v1.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);
 >   ALTER TABLE ONLY v1.users DROP CONSTRAINT users_email_unique;
       v1            postgres    false    248                       2606    16571    users users_pkey 
   CONSTRAINT     J   ALTER TABLE ONLY v1.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 6   ALTER TABLE ONLY v1.users DROP CONSTRAINT users_pkey;
       v1            postgres    false    248            �           1259    16572 8   personal_access_tokens_tokenable_type_tokenable_id_index    INDEX     �   CREATE INDEX personal_access_tokens_tokenable_type_tokenable_id_index ON v1.personal_access_tokens USING btree (tokenable_type, tokenable_id);
 H   DROP INDEX v1.personal_access_tokens_tokenable_type_tokenable_id_index;
       v1            postgres    false    226    226                       2606    16623 7   mst_collection mst_collection_ref_organisasi_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.mst_collection
    ADD CONSTRAINT mst_collection_ref_organisasi_id_foreign FOREIGN KEY (ref_organisasi_id) REFERENCES v1.ref_organisasi(id);
 ]   ALTER TABLE ONLY v1.mst_collection DROP CONSTRAINT mst_collection_ref_organisasi_id_foreign;
       v1          postgres    false    219    232    3322                       2606    16573 3   mst_jenis_dok mst_jenis_dok_ref_grup_dok_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.mst_jenis_dok
    ADD CONSTRAINT mst_jenis_dok_ref_grup_dok_id_foreign FOREIGN KEY (ref_grup_dok_id) REFERENCES v1.ref_grup_dok(id);
 Y   ALTER TABLE ONLY v1.mst_jenis_dok DROP CONSTRAINT mst_jenis_dok_ref_grup_dok_id_foreign;
       v1          postgres    false    230    3320    223                       2606    24609 7   trx_collection trx_collection_mst_collection_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_collection
    ADD CONSTRAINT trx_collection_mst_collection_id_foreign FOREIGN KEY (mst_collection_id) REFERENCES v1.mst_collection(id);
 ]   ALTER TABLE ONLY v1.trx_collection DROP CONSTRAINT trx_collection_mst_collection_id_foreign;
       v1          postgres    false    251    219    3305                       2606    16578 *   trx_data trx_data_mst_jenis_dok_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_data
    ADD CONSTRAINT trx_data_mst_jenis_dok_id_foreign FOREIGN KEY (mst_jenis_dok_id) REFERENCES v1.mst_jenis_dok(id);
 P   ALTER TABLE ONLY v1.trx_data DROP CONSTRAINT trx_data_mst_jenis_dok_id_foreign;
       v1          postgres    false    3309    236    223                       2606    16583 A   trx_data_organisasi trx_data_organisasi_ref_organisasi_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_data_organisasi
    ADD CONSTRAINT trx_data_organisasi_ref_organisasi_id_foreign FOREIGN KEY (ref_organisasi_id) REFERENCES v1.ref_organisasi(id);
 g   ALTER TABLE ONLY v1.trx_data_organisasi DROP CONSTRAINT trx_data_organisasi_ref_organisasi_id_foreign;
       v1          postgres    false    3322    238    232                       2606    16588 1   trx_data_tag trx_data_tag_ref_data_tag_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_data_tag
    ADD CONSTRAINT trx_data_tag_ref_data_tag_id_foreign FOREIGN KEY (ref_data_tag_id) REFERENCES v1.ref_data_tag(id);
 W   ALTER TABLE ONLY v1.trx_data_tag DROP CONSTRAINT trx_data_tag_ref_data_tag_id_foreign;
       v1          postgres    false    3318    228    240                       2606    16593 2   trx_data_topik trx_data_topik_ref_topik_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_data_topik
    ADD CONSTRAINT trx_data_topik_ref_topik_id_foreign FOREIGN KEY (ref_topik_id) REFERENCES v1.ref_topik(id);
 X   ALTER TABLE ONLY v1.trx_data_topik DROP CONSTRAINT trx_data_topik_ref_topik_id_foreign;
       v1          postgres    false    242    3324    234                       2606    16598 c   trx_luas_wilayah_kecamatan_kelurahan trx_luas_wilayah_kecamatan_kelurahan_mst_collection_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_luas_wilayah_kecamatan_kelurahan
    ADD CONSTRAINT trx_luas_wilayah_kecamatan_kelurahan_mst_collection_id_foreign FOREIGN KEY (mst_collection_id) REFERENCES v1.mst_collection(id) ON UPDATE CASCADE ON DELETE CASCADE;
 �   ALTER TABLE ONLY v1.trx_luas_wilayah_kecamatan_kelurahan DROP CONSTRAINT trx_luas_wilayah_kecamatan_kelurahan_mst_collection_id_foreign;
       v1          postgres    false    3305    244    219                       2606    16603 :   trx_riwayat_data trx_riwayat_data_mst_jenis_dok_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_riwayat_data
    ADD CONSTRAINT trx_riwayat_data_mst_jenis_dok_id_foreign FOREIGN KEY (mst_jenis_dok_id) REFERENCES v1.mst_jenis_dok(id);
 `   ALTER TABLE ONLY v1.trx_riwayat_data DROP CONSTRAINT trx_riwayat_data_mst_jenis_dok_id_foreign;
       v1          postgres    false    3309    223    246                       2606    16608 5   trx_riwayat_data trx_riwayat_data_trx_data_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY v1.trx_riwayat_data
    ADD CONSTRAINT trx_riwayat_data_trx_data_id_foreign FOREIGN KEY (trx_data_id) REFERENCES v1.trx_data(id) ON UPDATE CASCADE ON DELETE CASCADE;
 [   ALTER TABLE ONLY v1.trx_riwayat_data DROP CONSTRAINT trx_riwayat_data_trx_data_id_foreign;
       v1          postgres    false    3326    236    246            �      x������ � �      �   �  x�e�ݎ� F��Ì�����H��2MCD�}�5ͦ	�(�rq�m'� 5p����Ct&;='���<�0܃P�O��+D��K.��n�DQ�^�N�o%^���?��r�K�\p��P=a2�6��R:)2Qh޼�F	ޯft}��S�p'��HE���Jz�������Pj@*������N�=��Й&yb�W���_`k������F���gA��q3h� P����j5,���K%�t�-6��gfM���������ː��?{B�l�n[����_��.�3n��P����ه�$�����Jg��ˏp�w7��L�7����tR�I�﷓Gm���['�g��C�q�ur5Y\�.e����|�Z�r�Q�X4Z��<fo��6�͎��L
վ��3l>0r�Nt��cց��Z)w���{�n�����x_Y�      �      x��}�n�H���+�2`�Ⱥ�$�5�nYAR��$X]�D�.Yl������_�q�+�d]��Y
FD�##"##��˳���*}��ͲY6�ٿ?{���gQ���z'&�������g2J�bt)�i4��J���E_��䛴Ƃ��Q��Ӎ�E��?n�U�V�it�U��t���z��Cr1>��vY��鎸Tٺl�eݧ�*[g�v�
���(�2�����hҠ�e.Z��I������(KL�׋�Z��S����*���<��7iq�:���:kuv��t�]�ஊn�;����-���{�~�e�������f\�xœ���aAF_~�/'g�.�FiQg�
��n�U� 4�(���*��x��W��U|��6��:�v&�����Hq"��:/)�k�M�{����ݲ�\�zÍ_%��?E����2��&,�e^����;���������.m}���
O� �
�Gg7ˬ�a@`��5�:�൉�����MS5��6]4e��6�8���Z�c�A�$>�Ni�5u��w0�]^8k�{�37&gv���"}N��)����}����|ڤ�v��Q4bG4�thp4��Q����8 ~.�&a�ۊ��3Zg�����̻CTZ,��`,n���͖��)B��n$�^�U���YA��{�y�A�`���;�)D�P��!5��M�hѤ�vJ[��j�mmš�nu�#����g���«����M��(�wl��Ϳ)���q6U���cf�Y�m���"��bD���W������.�&��D���,��r�n��#
�wK� [Nj�܌�V����H�z��|�%�q[��~��4��x�җ�Ӣ�Z)��T0r@ɔ�!�d�?��a�09��~m���/+�v�V�M��� RN����/4:���D��P߼g��5>��=�GT���Ͷh@a%9Os2�\E����w7,�v�2�A�%uNF����p��~޾�y�n�Ζ �\�ӺQ�_�K-q�m�@�e*����u��W*���+����8W,_�|%�k�&y	���A�Do&�h�5����&�*�*a�d6c˄��jN�[�.���\h�,B��b�}ń�;EaKƖK�Ɩ��P��&�MFV_��XÔ\���j�|�PwCI~���U~~�<��Vy�c�8m)���c�j���M�b!YTBѧm���6���(Q�uӑ���!j�sh���"V�4�"
�t+&��TjB���	�"�L8�w��I��66}W5k\�z���"}��Cgs��vs�]��<�89T+6{�b�i6����b�\����b�&�����"��X�K"��¡CFӫ�$;U��>5л��+�b_��~�H��v%܎���_k����c+vѷ��5j�iّ��$Ih$�>I�$����LP�M�W�� �eݻ�����Eܑ���yq���&YZD߱D;��u�ŤZ2�'A�!��Uӑ�Qϣ�t]��k�$(�PzNMtA��"k���0��N�A��x>�aK�C�lʡv�n��:wG�~��F�c	�n������LApa���"����6�I�H�lc7��
t��4�Hr�l��+��B?z�Дy�r�f��u�J7a����ߕ��(�Ż����{�̰���	t7 � ��7�d��v�k�B��'����$KS<3�������7`H��� ��6M��X|@5dQ�����Ȗ��e�tD@D�v���I�I������î�v�N�ҭU{�����Vkljc[o>5ʠH�b=kC�y�}a`o�>K�zd�6��a��
[ǯ���Q�\jx�ZOaiao�4�!����1��^w��/u���o�р�sw.�E���'�6�ׇ�]݋��S1�礙M���&���]�������'�o�3l��4x��mn�K��׊�W!�n'�K���I��U�t�BRvj|W^��ܺ
I9���|�Q�b�Ǡ�� �@e�*[|W)���B��6|�V�ӵ"|�vR��Wr����C��9T+z�޶���roE*nᨸ�+�>�c�i*�#��� �6_4�����POG[Z��
�,0_�Ѱ��-Ș^�$���x�x5�a'�����j7�]�A�3��*L�V���\j��-}�Wi�>� �l�iJ�D��-��H���$c��UVD`�|�;:�Z#�x!{PEP��?j8���$<&O�aC�B���ǿb@fg?Ao�)�F�4/ Pq���f~��o���i����m�VT��T���.,��h,�`I���E��� X��5U����9/U���5b�J��*���"��>��ՙd��	�"��wMh�omO}���<(�/�8�t{ʪX�%Gxh��"����4��W�?���&�G�������t��l*�x�(��$���ʎ���v�lx}�T������ �`f��.��2�QhFo�k��]���8d��b��N솃!	C�۸CB�i<6-�VSpG��[k��y]��G�QdC���ڵ��h�TT�P���V��gV� D���/�ן����4�b�����)��ooh����[��!�SV:�޼�������v�eg���,�W+��IN�s �B ɞU��@�<�Uι��.Q�[
a�A�|D�IM����8L��$W���KsH\�� ya<�Ί%����2��m<��[d`�֑�y�_l���vMױ��J�st���T��y�N�p���KH&����ni�p�Rw��Oq_B㨵vg� /%�
C댭�䝝���H ���w��ZL��;X%�� *�v�T{N@T�-���G St�Y��}]�y�al��˺YG�TMM�-��k��]dM��@C� �h�@�v�D*�D$ɤҐ4�+��C4�y�1,�Y�6F[v�M����s����`�����R�@�������!L���~�}����]\%�0�6�
��9��7�7��&h��g�$}�c�?��٦�D��sM}���@��^��;bT�sx��v��}��z��vۑ��P����n92�����]��Jk�B��[�lG+z�}^Ew���ּP�4�Ě�:qy�I���OfA؛���I���jާ����'X�Y�5�����Q�����	u��2I�Lc���E�$����cu��H���+7z_vV��A�
Wu`��6��)����l��K��p�pdi�a:��a���D_�z����"h�Ѣ������
M(��� E�΅\d��%"����<�-�}O�a�9H��-��`�sff������_e�������hWmt��:��Y�Bs[��"�X��H�F�Q�S��D��'l��RcJ�)���M�[��>֑���� l�����j��@{#���кLw�W��)��GhEH��gr���GH#+��I��^ĵ{��3#{���G�m��`R:�,���$��5W�Vdo�"Q��"E�<C�^��:���ǖ���s�f�����ÿД���A?vЧ^��{��'�q�<_4`�~ȿ�X��U�Ee�jTP&��`����HXRaI
k���CvDs�I��:X�Z	0���ݵ3���6� ���}�lg���X��1j+��� �C;E�%��l��JF>��C���=<=��1���z�U��ؑ�&�7f��"��wE w����	��%Fӯ��5�-T��E+V�����n�fJ�筑'C�2�0^sE�PL�f��°��D����R�����O�of��b��3�٥�Q֨cEw��sX�y�c砈��F��Q��9$&B1i]�t:�R�3C���r��J�8��N������[.<�&�}���>XEv�����p���5eN&�9Y�?�Vq *��0x&�& �^�^M:�D��X~q2՞�����`�]�ն���:5�4�XL��`>K��?V>����#���y��/�h��"u�)�;O`M,��G[m��+��Z#�vS�R�T��8k�}��nU�%<:�gZ4��.=7�����>wA'[G�����@��~�5��lT�*�V��&��\uEη{�7�a�;bD��Y[\A�    Z*�+��T	UD��bd�?.º��ʦ�"\���8)��-�ƹ;����w\�mɿ�ͽ�Wx-,_���x.i6;�α�����c�E�9�A��!ta�c�U��)L��#�a�Ӝe�J��$�LO�*AR	Ҕ ��q��#��J���i֕1�[����v;�y鑐� ����0uM��x���v1��H�-�uAf@T�o7�0���h+�wy5괙a�K�9h��4;� �o�u�j�BT�ݸy�T_���&�G�} v��6{��B��WB�g�i��y���|�w����[�М�'�*7KX��*�H�j󗁊�i��;���%r G�nؖ{%����ٸ���'I��>��a{���1��ޯ�dvM~�*�*��`
������~hи}t�a��KcX�`,�,��:Oaף�p0j�r`�T���R����E0A\r��q�E0X�T2�$R��Ґ�慤�In �@�a3�M�Tʀ��@5�_S�W¾I6 ޲~�#�E�oj�X��v����Gh>��p2���5��D�JM+���hZ����r��ϼE�¦&cP�=�T�m �s����-���n`R����A�t��w���;!R��*�r��g
�h���`��f��u�����v�����;�5���(���G_`�rN-\�#G��w��Wh�f&X�B��i�N�2ò��U���1ԇX&�~U���SW�}Ҥ{c��|���6]I�(PL�Jt���@1&+�-́�H��S�����V��W�d�'�j���m�,QMZ�^FMU}�Tw�^�ta�����9tm�H�91�(��x;�񩺤m���7.U��W�$;0I���yj>-�/�sf���p��z=� K��솹�SՃ��.�n�RNU�ǜ�n���-+��jC{<z��J�V��zu[ނx������ם��:��B�2��P�ƌ:ĥ<�{u��<l�-��+��T}nSrй���3-;Mޭ�n4��w�05��鰱w���^�a��q~|��'�������@}'����&c��s�����h�Yr���Ǐ����̣v���p�#���؍p��Mԉ�80���a��{U���h�}��vME�3&�v��-�s�����UA���0l[��[�rW{eh^R�W����^p�O�|.�m�3�~�N�SR��g�v���۝��N���]q���>�"h��p3'��>�W^V���n���f�'ht�K�L�SO�M�]ŝ�Z{e�P7���,0���/�:��q�/�F�Y��wީ��1�¹����8d�I�
���I@�
7���� 4��M��j�����{� �3����V�(����6�ދ���S�b� �=����6�+�'����UQ�?n7�&�Bn=�I��o�"��cFq���p��5le�!B�Y��<$J��E#Îj��� �	��Jp�&���N�27����!������&�� /V#�uc��!�C�j��2��3r��e&'-
���ړ �q���j�x�w2
�i�!;��w��3υ���j�lOk�}s\S��v*āF^x��F�7��Rȁl�^��y!]6�PM'���jd� 4x����Q����&�P�K���v��?9���垡Y��"�e5X  ƛ����BS�&M"5I��͝a��F�t?�!w��jG&L`z�?�#LI �Gr���7��Ykk4�Ib��>�w�1>��Dg�b���rf[���S��M��C=�Mo��3܈�҇8��gN�#D�=~f�D�:u�&��L>�Q�� 쨩��K%5��oӺ��'���s�8�.tQ ���Fwn��$I8R��ꢵ�Xh��G���
����8QhY{��[��eow=�L� �Q�������=}p�r�S�ѻ�U���[v��>@ә��LG�O�t�Y�jL�����A�,v�[&�m,@?������+�`�xl���EX��&k��y��#�|�%ð�ݼE{�/s�(�6��TZR����һg�����8M`C��_U�F�ڮ�-̘��ȷ��6��B�j(�4�,���	/tІB��(���{�^	;�Mz�J�4�B�hp�j74��C����P#:95l2f���7��)2���;Z��B��@�D�M�]��"���x\� ܖ�$#�^zĬ���7���������G1ҟ�``2k'�@G\ɹ#���>b��ʕ*y�a�#�EW��j��DH s�ޔ���T8#��-�'!�#�'� ��L�^l�.|
��l�_o����1�{�b�BӉ1�/�ǰvC��|�5��R9�{��������b�p�*/�ܤ����`�_�N@�g��n�ztԖ�rЇBq�s,߹�60�LgHE�GV{�<y8�Y���8���*��=ә�=�R笅���q�e���Rӫg�(�!ы���跆H�E��H��N�_��7RO���]�e��t����W��D�㦹�Џ�\�Za�ʫn�K�9>z��n�_��$�Nݎ����v#~c4�4�,�Z�E)���ѷ��`�K;��a�ƀ^��|��6IbgG�9 D�)��x2U�5g��Ãm�� �� �H!'��0o" �@��#�Zj�c!���VC'=:��l��m��9��v�-6�R� ���#lKD�l4C����Ɓ��&N�gh�5Ǐh��"�E��&;�}�0���N���Dln=
���R�߇0Mm;iv��Dɥ���a-Q+�ř�=!Ra,�n{�>�U�'�xd��[g�8�KzCI���{�ð�6�pf���[��GwHo(@��<э�v��������*�BP|�����^��;�����٧��l�ah����nT�
q�Z㈺T�i�A��>��=�����u�<>B�����C@L��林���g�Sy���I��������'���Df���� �3��Ļ���N�O��i��MC�k>mG�(ء��^���~-��S���A&՘q7TN���A�x<����C�K׃�4g�`���90��@g�E�׾�=�\I/Y����x6iUl{�L��y4E�Y�h�G����v���-�����L�g<��3��T�+G���b
�u�5� ��2��YK���a{y�;��Ei�pB�%-��@)p�}>�aoO:����G#k�E�>����a"�k���<�y���[�;4�Zo�)��Y����ku�Pd8H4���D4�Jˠ脦S�C�L
Y%XP�R#+�2���nG�W����4d	SO�0B��0KSNzZ�v��2c�=E�	�^������$I�H�D�㧫ra�:���J�j�g����~�m�p�����L�B�
��n4%�N�iœqk��
�J��%�5 I���n����x|�0�T�Q*��/�����m ��MiEr�;�,��z J�^�����WΥ���F��s=O	��-ʨ�s-O���_�v�0��4�@��n�!��ѣ���Q�+2|�	����*�/㤄g$�v���0��N�����!��3��R꜈�r�Ǧ���`��&3���\�֤M�@"�boHi�D_v�8����Я�w#&v��uq9�� �3x������������p�����2��2 `��<;�������X�}u23��=�ߘ�."n�vxg�:VF�!�K�nh��j���W�%q��Ћ���N�k�������K\�!�Ç
+�!��am�!k��!�:p�'^�*v�HcL������j�)v�N�v4 A!]��e�>�]qz�^��9�7�b�D�Zv�N%�F�bL�D�T�h�)U����@���+�b�괓j�(KQ��Q)�S�]K�\�>�Ga��;��G��3>>=�E�×!l|r�ƻV��H��2�?��<壑7���q]�I��oo�4؂�p�]��x���M��i������;��B����m�i�m{��y�f���R�Ct2�d&�g��7��i`Yt�߃��tnz?����tմ� D�� V  ��Z�Q�.�7j='��T�o�z�R���=�+�o�58�7i��y��Y��:�C��ط�ۺ��#;��aF����t�Ys�Lك�|�>�`�*���D2R����8��+�@U/��S,P�`��(�}���<�ͫ�z��
{_^Q�7�~	R#��u�C��4��^Ot[�����)vS��/u���q,���M�e��o3Gq3�ɫ�؇�C�9�<����ɐ�m���ލ�P��Dݻ����J����Q����~� :0��D�x��m�?;��dt�G�S�rO���^�)��?�|�%�wUS`��Y�*�4N�wP��N����J4�E�/�$��< ���y�^���D�~�L���yj�0���wA؁��kx2Ř��3�j#ޅ8��vV�� �|�.�8�b��o�m�~�T�ĉ	9�D��QY�^��S<k������g0u�n���lQ�E��Q��YWgu��>=�������RY�y�s�������`
��³��"�]���s���c�)N����I*����3�����%�?0�S/'�{�צ�ɡ���xD��$�#�Z|P��p��N!&���Jrʰ��M�7#N���x\x��mƃ�$u�C��'��"�+���擒�+������Wݏ��Җ&LiΓ��I0'^L����p�mb�F6�m&d#��	�FIl�|�}�L���@�z��� [ �K�EԾ�k�p�zB"�X\ɸ꽈=�A�ѤAX�c�FE��7���y�,y������'��ԟ�&��&z���l1�P�k�򩇅�<D�l�i�O=3l�$ɲ?뜮o��pc������h�>��t�y9�AA��/F.Rаh�L7��l���aU /�W��4�%[@�u<o�z�=��:%2�Ӂ1�y���M���c��?��g�82VJ���P��S;�@�ԕ�7���5��_<3��z� K�����W�o������)����f!y����2KA,Y�@�B�Y
d'������3��$��uK��!��Cv#����i�ͣŰcN����/��W~g0��t����u~�������A<��z��x*&��.}i:n:�2f�
������sn�
ŖO���`�����%^x����a�S9���kе��|V�Iu��ӕ�����DOL�S�U���M�d4>��Mw�;����Χ����])�a�{�|����E�-�S~8�t����kz�l��*z���Dϓ���n�y׭�`G�z�Xsc�ӛ~���׭�I)�4=�9U������|>��{�����4��fA�Y���(�\/7�#uD�,UIM$5�D"Y���j�|2	�b;� �)��n�LWJ � �� y�g݃j��=�;����)�O,�4(�~�B�� ��YE&\2a�|?�.�4��=2��AX�O��Wu^��;�<��/q��h�����:?���f3�C�a<}����_x)��1���D�Z1���$k�n�z-[��$�Ft�H�8N��b7��K|�	؉�p�%��vլ3��Nd�����~(��	�;�۷�����Փ;������(:���1;I29�E��j���ăʋz��l×�
f ���B.)9�����[�J"��9�=�Rޓ
y�A}Ӷ��a'-�$�{.�Ǜ;����#�'Ff��]�=�2�C��k� �0�¡n��DiH�C����U��0��ޠ�Q�7&�l�5�����O鐙4/=/�G?��m��6װS/�'xc��|�`TQ��j�+�x���c�a�=���?�B �,��B�%)�B4�2�d�!�DF�#E6�9��a��^��Ea|�O�F���3�H�����Իy�W��g������_�g}�m�%%�K°����ܡL�S�k�ipK��U�	qf�@�;�����v�@]t*n�+�<��;T�sA~��(�y>�^�^�D���j�\��s-2�ކO�p�dQ߆QĹ�,��,�â�mb:y.5��ߐN:t����N�O�(�+�'i�J��qxk��ў��7t(-RP���e���O?������B"i�H�"��D.����8��m��85��|�35�����J�N��[3��)ړ���g�q`���H��I����c؝Yu��Ͼ;�Ξ��z{��)ltA؉�Prv�>�D���	u��Oؙ�躱���|�F;_�,�����c�ߐ�`�DX>�o�CD*�T��9Q%Rɤ:L`Ug�T�m�;��Vv�5��C�o������A1��
�l���-��ƪ�&/�#/ P=pᑪ��w��7�kid�����8�qUA���cY�D+l�lK��)KbY�]�%�,i���rYEw$�nt���db<9{RF��/���$	�B�?��5��$>����ᦣD�,_��
���ʯz����}C��,HS��Q�L*f�a&��{
羪�'�lXל�a>��6���qx�"�x�Hwc(��0�&^�M��S��x�n� �Lz:��Pxz�9\ث����)�(���)E��B�kgi��
�Mٷ���.<i7$d%+��&�{���L��R�K��O��W�����Z�h\��O@������A%L��~񩐝>�e��=��`��y���?[57/�=�-��`�Q�i��J.q�/B1k?]U�7���#�����f�Q9ٓI��Ez��v���[���!�EU��M����þcFOۿ,��e��@OO�k�.�X���L��d;�]x�粣�f݄��)"�>QD�CD� �V�B�)�d̞����~����}��C�ȓޚ�0d ����6��9��Q�{��x��-����u�����"T!Zn���;�{o��[�����?����a��      �   j  x����J�@���S��{���#("�Ÿ�:]�q2顧��ot���p6u6��������-D�?����;H.�.����š�5�y6���ɥZ�Ch�|�q��b�~���n����~�U�w�,�"�އ8�}��EmE(@�`H*�h˅�8(��c�<���\�y��b}p���x|���OI�0M��;Nӡ���Eq�L΃�<�']��������rA�&�ʘ�����*KEr�p��%߂KI5p��k"��H�*I�.�Х+�-Oe֛���YE���[æ��,e�����g��q����[L�դ���,=9��*�4k���1�r7�������xU����<cU�˚��i���_S�[q      �   A   x�3��p�4���tq�FFƺf�F�
�&V&VF&�ĸ�8��|�2��W� <(�      �      x������ � �      �      x������ � �      �   �	  x��Z�n�]����lr�%��{ڎ��`'w0�l�XIԶ�@����(;z8����i�UdU�*��&�f/YNg�(�/A��#��~���i�ֵ�_�e|¼	�U0�&a�x����4����'�;1,�Dٕ"݋���Q��H�&N�D��̸6LjH��T0����\��s��y۝�.�d�q)�6*���V˸(�� �UoÙ0�$X,��r�N�11���=��n׺��*I|F���[O�n�0t�y�np���_�VEUh|�MLC�!�ݧ{H�d�̗�4l�lD�FwY�c\�ɹ6}y��=�
�1o��ÿ�*y�N�u��>�cSy�>��E.�1�=���r���9��?*��nζK��\�.f��aL�}}k�Pl�9�>���(�6�[-�*|R'2���y�SZ���M_\�c�A����b-1,�w7�o��(�Z��צ�a�*#������ʫg��^���>��������8�e7	�����z��VZL��l�/ѱ��-��J����1�I�}�K6Q��#:y�a�_�Uc�<�ս�!�<�̺[��\F��}��_c��C�qAWQ��8��'��q��ۖ�>:H��T���5�-���� �EC+s�뿢�B�0���V	z�A������t�!�`j`L����6~��m�1z��������>Q^Di)���m�Y�0�e}��j��ˠ>c`�$�3��Y�DR��i^��Z�H��x(�T�3� ���q5��������0@����lgp�!�WzsW���Hh�Ù�޻fX�da��
���oUQ�����*�p�lx_�b��<���&�Z2���m��a��x�(��*�����+��J&Y/Ɗ�\�U�Sa`Y�Fk�~�,����&^��c�a�r�l��s�'u��!�zH`K��E޲TXY���1sLI���[�M���6NX�,��*������:�pG���VW��`���
n�0;���f��K�V�H�Y��Z�-���27��-�i�0@9�g�W	&gT��J�x��YR�b*5(�M`���MK�Ɂx]9��;�� F�2�NC�pT�U؉��R�k8�iwU��Dޜj�V7fN@=�wvzM��dw ��|&fD�� �YTz#ҡn���M*l��ބ�G�6��T`���N�b��Q~D!����0�a��x����7q�{��bm)�a`^�����R���� �Pa`s�-y4�W<�֫�@�,�lv��Kt��s�K��7�m��1y��o�i�;���F�6�ս;o�s�@s��y�v�0p�|�Fo��I��{����,�ۊ�����&K�2���1��xAb�pI���]�MnD�$��=H!vyj�$�6U�j�!{���1p�	f��Q6Cz"_����Dc��vJ����s�����]�"b��a?V`�Z�2.����=W঍	3U�����+l�ڲ�*\���޺+�0pQpG���f}?Ƕ����A����!\����� �N�6t0p=���֖��~��K/��8�|1�a�d%~��onp̶��a�Yd��
׫����2�x���U���1�+�N�w0�lTE%t�f�9)Lz�[Ά���i�=<����]j��C�+l1�0�����j?�0�<��������n�?�?�4\
O�'Oc=��>��f��Nz�Y���:
��JQc�3r����5��8�j�>J�*����/�Sk|��Y�%:&*|N��=�l@
�C5C'C�U�|ق��x/%���iۡ�ӄ�l�
]a������wI=J����C�U>m��v|=|��I���꼚��5��y��H�F\�j�a4DkQ�x�n��`���t�����b�A�f=���ͺr��!�:�I.;�.��S Yx���X�oL�坫1��?yũ("�?ᗷ�Ɛ�!��6�W��=γ4�C�#]`i/_�6.4(��(��&q[*����`���ߝ߲�g����`��}����>��� ���ql �va����K~=c��]��^�c/��������n���Q/�0`(�����t�^��Mw������1�
C2v��:4��i�g�����llu[���&y(�|[�L6g9PK�<�@98<z9[8
��"5�{}��)uu�0�^N��({�-���&�!�_m����
5�$��ų������(��Y�ܦLQ� �0���}127=9����`h�]020hw<��� ���N�;���~����t>��o"����藠�m�4��q����P13�8}�L��:�OR�h�	l�]�D�|n���d��ΰU�ɹϣ��/���љn�0$s�j�'9F�w#bp�������/S�������oHx3I�cH`�~�B��yP��?'ievC^������g��<Z�4�~�����_ �?��z�      �   �   x�m��
�0E��+�$:T����RP���ɱKJ5���&E�{��S��p�d�	.֙���}�˸�5�c����;������ò'tP��o��AGxb�m@8"��*�P����Z�V	Gb��D'B���d��՞+г���'QMe���c�\s�ߧ�;}      �   l  x���Qn"1�����h5v�8yb)e��Q������Ĕ�LWE����c�P?�=to�3=��9]�CZ�>�O��yW��#]�C���I�;��* E�x@�6���������2�7���((̛J�X����!��@<��dXWa���6Z���B5�KN��Be.2aG=j@٠Tn6�+�Ǹo�Ȫ�1xMZ��PoP���t��-�Y�@=��,C`��%��P�+�5]��czOR�>j
�^�p����@ʿC[G.�����}�5k2R�g�7�-o4`���q��4䣠 o�ܿ�ѹ	JA�;���!Co��H��]���e:��*�����9k/�1��P��;�[z��C����&�N�g�yv0�%���
h�}��M��7�(�������9����;i�AAmP�asN��g,����)١������1�o�ͣ���˸��w���F�,��l��(rG�<5�fQP[��x�L�t�Z�\�N����FE䵸�h��9�N���'������܃��u=c�[5S6��گ�ۃQ�.XU�d�'4_���Ԝ^�)�m��|y4���4�5��n@������|j��Hُ6��xH��J�	�2��.��9i3�{��Y=]�����})���zѯ:K�i	V���b�$���+_�Э�#��E3+qu�7��	�K�]��Qx��Gs)��߮���&���Od��`�&@3'�:���Y+��iF5�H��������}����PhK�<��H\YI��T�o�hF$�8�c�m�-��(
�af�*��P�t�?��U��4yoѬ�\B�:@jI�$͂$�Z�-�.���L�1)��y���}}zz����      �   �  x��X�r�8�FO�(;�d�O:�i6]OH�z#�ר�?#[���m(
�3LbN��>��W��6�U�
%E�=�T����W�zؼ>?����_�Y��`�5[�0�.钗2�m�ǲ�Ӽ��o��-��%	f�����^�e.J ��#��)m҃h;�2O��|!�adq��V�G��,��d9:׊�(E�%���koSw%<�+'[�W���N��e���/�׵��h�*t� 5������R�h����U!��5B-~SF�?�t)`�ڋ�I��x��zMU-��ۊV{�,[�r0��a���f���3��Q��	^f9�Y�����)$a����`�]��Y�����i�����#˽nZ�Aܷa�3��a���NDQ���(��I~U�#4�S�*xo�.N��	/��9���#�C�nT#�A����Q��� AJ��f�Tnr�#t5���O�u>��5�
�����ɈE� rDSUe�"��7��;%�Q�ũ2�K�C]�J�6U�:�]���,�!atl=5�L#?u.�5�p�����sZ�x.S��#�M�`g
�T�9EZ�z*������6�����6m0�ΪXf&
���E5n��#,�����G�{.v(-m�����kz�"FXx�`r7?��R7����FJX{޳(5Ƨ�ͫ\0A;l��m��}�A`0��iP5Rw��N�,�`��F�������G�i��t� fуA�h�
��4��UV)ә���[Z���_9���[:^��i,�::QZ2�/gW�oXn��+�=�������ʉ�r� �(_Q��-*���<���ً�`�e�P�L-�R�S�#E3}�,�����*�8_�]!�/b'>V{5�Ϗ�]M��#|u��6�	�zObg�P���V�D��Nq��:y��L��ݕ�&/��l��#�4/�fEĢ��v���+nV�jN����)i.0�!����b��M�N�ʹK�Y�l��Z-EsJ=�en�"F�q��&��w�|�xd���a��܆V,�~e)�z�]�<����o7�B�_��j�� НU�bt�|(��ad�҆,�ݔ��UB,Թ�P|��Z�h�CF�ŝ	���?�S%��8��X�5�`*4�e�����6��R��R鶑�G�#�|����'�lSs�A�Q��4��������N���%��U��0D�x�Op��{!1��4��M��9���cg5�K����)��#:��/$F�#�6�����Kɿ�w��G\b�����ꤲ�      �   �  x���Mo�0�Ϟ_�#��GTU���t���֋ٸ�I⬜D��8�vi�Z1������yg,��
�����K�Y]'�vɵ9�<C�����9�x���N_�
�H��I���:O718z���w��ʳ�ܝg �UUۆT��X�%(v���꒝i�iW�o�C�mr��i��k�خ����,�S�`�NmH�c��Й{�S�vҨ��]���Ύ�;�JS�i"򻄂�{7޺^�	2B�
x�W��R�c�Q�xM�����&O�hjs�UL.�d�~(�OB@�/�����d{�t���� x��:8h��tƗ㬺�Z�6��Hv�K�eӀga�]������m/�����]��<�{=h������@�?�1p�l���L=3,���0[{��v8
I >;L�@`�mh��8A��o1L�Eۘ.��Ϧ��	�Cߒ����5�h��>���k�c�5����	�>x����C)AFVu�@dl��ݱ�=-2�GĒ��0���oqzlM�Y�Ċ}�S�j����i@[|!XL�^���P��i4k��JEdYL��NWT���ن����n��C�)���H~�֜�go���b�ۈ�j����!#J�4�Wg�O�/��B����<��4�} � ��]2      �      x�՝k��u�?�
�%�:u� XL{ ;�!� eR2#�䐔'��SիVwuջ�݁7מH���kW�e]�v6<��ÿ���ë7o_~|��돯����˷U��_����e�s���L�'瞌��5_D������Ͼ�����~aLy��Or���>}z�����ջ���w����������^�|���_����Q��͋���}�������S��o������_���������_���߾{��o
H�)+��RBmƶ۬����m�/����7�{����o^=������~z����O�xW^����w߾���6�/��b��^�?���7����������>y������u�C��&<���?k~R�I��O��A�\e�G-e����7/?|����4&�a6<y��aɴ�ѯ���Ӝz�SOv|3�����aL��q�7���U��ڧy�H���.?{����d��z��I9���e�6�D��Eĸc����]և:-�Rq��J�$7�&3e�frg�����l��b��w#9i3�WL�.oN��y]��6��:�뎩2-`Ɗq��Q�_'i3wt����(�u���z���u����j�))/F���WC�f�7�����}*c|����Ըۦ�-���'[�,j�f�t���Ԫ��ɓ�*g	�����h!�ڨ���{�{�j��\��&��䤬UK����6�FB��"|XO]�䚡6×����>�&Õܾ,6j3�-�!��k���g�b�_ᗅR7�N��c�q���w�+��}R	j3�+/��F�m�YN�����i3�/�L�6l��-��+�M�P�����iN�痂��Gj�&j]�E&������=�Ïo޾z�����}xt�Qg�������߿���^���˗o������y���_����O����޿�����z����~���;��C��v?e:m~o��]+�EŻxX$;�C�wH�wP�.ޡ\���w(3�ޡ{�R���!N���=��`���_�9����~����7�?�����yJ����L�24J�� ˪=Ni�f�hҰ	F��E���N��A�ۍ&�]��nV�z_'*i3w���'n�mret��	P܎�iV�E��Lt-�9X�:p�	�f�-w�r;������eN>�������|�y��a�N���A�uW>�0�����b{9R��=��jP�/|e5[�[4�u������5�,ߞ��a,�}�Y�54���2=���<+�s�:��;mzֺ˪m�]��G��w>`:x�|�V��L��s�����	j�[�;��
�;ϻ�/F�0��aZ�5�=L�U�4�ar�a"�L]���I�a��4��1���è_��}�ZC���c��<a�"/N�<[GV��MΪ�NM'%$�U�@8�6'[n�*A'[|�j�Y��Y�Noc�$�GSZ�gUy�d�ۺ.���K?��8�}7x�4?�Qe���=-_V�����j����P�Y�OVd�Ps��}�u���W8�|��%7��8��o�$G��z��U�jGְR�B�Y^�!dғ��W8h|sд���a�,�g48h��2�g���aBYvn�\���`��4�[��[�;�
�6�򵮥j;�p-yv-eyג>�;�ܫ� m�ŉ�o��^N�j����x��Ę��\�R~_bZnq5���G�up�����/��N�0��a4�6x�o�|_�o��f�j��#�:�{���g_����O���`��4�_~��-Qj���P��c��|�vP��[�]K��n�t�n�Wc�0��6mI�Q=A��,��o�������i�"��zi��.�!�e�d�|O�Y&�U^�����ߓþ�r\����!���F��(s�\���χ�F_i��g����Ϝ�f��E(��H��y��_Yna+���؛�:x�t�t72''�*���L����H�����4�qU ��;�8�����H�x�㇥�i<�0{��txXj9�޿����c�k�#gڳ?�W�^����M�����bN{'o-i�%���!Ӗ�u�'<��*m�������V���%��+[蔬�j`�{]��->�k��:|���������v�k�l�MV���{��dZ�jM���we�@=�@}3���D��#�i �ε1"���a�e�%�th��������H�N��6�ȇ����W�!s8�u�f�Y� ?��#m�t�W+��9�N�E�g�N����V
B���1��Z��$�i�����I;��5���'���iO�Ĕ3�Q�_.+��h���bok[H��o����I�N�mmGz+�7��Ľj�����t�.g6�<M���P�`Ӏ�ձ����H�;�'��Nc�����b��ۮ&+5�CT����2�UG��Yn��C��o3�vg��[����+��gM��zS\��e���4��3���<�L8uhqbv���q����--2�0�:�V��='��2�E廰��12�5���!\k��
<(�F{NZ��Rȋ�&�sx(&���A�:��.Pb�g���CŃN��ˬ��~� �(�B�z�r�>���6i����N+��m�ly&P�f���K���cI����ɳ�e<��pSN��6&��U���s��r�4c�i����a�Y��i���P���� ��bN�̔�d��=���VS����Յ`�[]�L�h�Y�=+�v+��7CxVl���p�N�3�H��ж/���2_�b�N����7��+�5��7D����4k�j���ZN=�<�5�ӷgi~���'��6Axyڳ?�̔�ˉ
P���$�s�K�*	�}w���h�9Y���\W���(g�f��]W�P��Ӏ�*6�%��p�w�_F�'P`'PhN��>�<�8H> v8q�>YձXܘsG0�+�L�h #U슂h���{T{T8���,�lm��D��y4ic���-W�&�3�^p�X����'NW���a��4���<Nt��JE�l�z��w�n�43}��ȕv,ۯ�����pmS�t�y8�wptl�p-�R�+\��p��4�����X���tt��q�Ӏ�����y!,7�v���a'M���M�ngU���%���_�4�ϣ�>{UK֞z��s]CҀ3D�\���˩Z,ᤞ9����i�=�#�|P&��$�?�tp�Dv���-Nғ���M#ϙl-�^�b�ǜҀʶ∭R��:cv�Ʀ�!��O��On���S+��"E$�ӡ�Z��46߲�h��G6X9�郤�l0��l�~��ojp赫.��W~1�|+n1b���r�fki���l��`�����\W<C�ځ�Aw!u��8��9/I�h��C����5���ېVK�).�:u /cn���`R�Y�g��Z@���%G]S�5�4@]���C����|X!m��8����w���N���������~+ȝ�͒����(�o���5"�@p�O��X�#���ףH� O����#bJ������zj����ᙽx�P�uBLWS�l׆�&���ٛt��ݨ�m�Ӏ?.57�:�o�~���z�"Ԁw��i�wG�o��������+�����?>��~�Ͽ��o��o߼�����_}x�~��}��W�z�����������|���o�~�3�63�7��R���\�������K'��اG��|6��5�68#?,�mݟ�V+2O�Fv�1�U�����5Z����=k�\�Zr%�ަ��u�&�?-i��م���Ji��B��\�����@#��K��{� wb��FX�St���q�p�v�zv����]:�%�K�9���f9WJ�z8��4�;4���${��#e.�HqNZ˷[�`nW8�Uv�朴-FD��i��<��=��S�-�p:\p5sJZ�pvb{S������M�҉=�m�q1�n�f��X/�4�
S\����h�'�,h.���#i�=57g�
��;���x�;��;�n��5�4��s��#k�M�d��)���i�fZIG��J�t���4���%��-��c7�L0��Փ��k*�(�i���&�����2    ������w�j»ۣ$[�Y}8]wp$��v{���-Y=3���r�wm�t����Hp"�渓�/�E��Ee��%d�����G�i�7�j��v�׋��'�^��}s��<�#�N��$��Ts�q�Z�N���x���?h�=�8]GOi�;��.�{���9P~$�<`����e��<`��{�i�<��E�,ZlUY�t6B�g��Q��*�Z���,���W��>ߪ���
�}kӀD��Io�9�a�4���qCuf˅�\85X�65r�p@�h�)Պ4�oj�4BiqB�,�K5a{��W��4]n�x%v�����2��2���U���nj�`h���>�zR�����&g����-q���3�f(���|�jW�f�hP'�f"�f�f���{�����Q�s/����T�I��+����f,H��p^�JY��lF���4�2���i��nb��;U���<�YyR����>�;d�z"�L��oZ��{2~t/�|=����"���c�D�@�Df_O+nb�'��Ґɼi c˶��d�(GM+��R�:v��4���-ٌ��>��.�|��:�g�R	w��)�t�REn=&o-\� �T���ˁ�K�L~2Èop�XvTe��a��i�ݣ8׌f�l�ٔ4D��쥢�k�H)3�7i��c�KEm%�ӡZA��ܼTm7�I���Qe�'��N/� Z�cg�����5���b���&�Ҵq��v��c�p���i1C[(����EV�mR�oZ�8�jM�=z9�Q|�Y�p��4���U8W�o��9�d|��,� J����g9�,���Y�1<�%���OC|;�,�������?�pq�<�^gj�w��p��V�Im�t�Y�p��40���Z�Q>%�](��jL�%H�:��B���z��.r�:LI;rZ����P�� u`?����$u��j���Cd�Osdi�;�Zf�:Mnҭ�\���ڦW�jN�@���N��0$i����S����Y���f�4�fR� ��4�f
C��M��0��1�g����D�RM]���}_��&1kӣt��{��K��G����w?������?S��g��N����lo�i櫣N�65N.uD���é�(9qK6��O�|t����L�*����'i�G��k��QT���.z���/$~%�.R��2�T�)�b�
�������jk�����ߤ��,��%+P�y- ��8��{DJVΌ�כ4�S\�r�����-��ݫ�I�j <���N���.��ԔH�j���	pd��~QRM�VG��I�K���->"�x:��*�u��ڞG�*٫tޙi�3��t\:�|%):�ҙv.�H��f��+)���^���l����/��U�(��kA�౜w�cohIlw������i�x욏�]�=':��IvhͮA#�}4iw�N\�r]]���'����2�J�Cx,��i����C�)V�v��sW׽�ຯ�ݾ�}����=�s�L������,S�X�� i�^�[���^~Mp��2+&�ª�a��c��\��%��H�N�n�5(������ #=:(�zҷ�$k��l0�6ҋ�D2�G���}�NdRLY�I�K9��l)G5���nǉlTTP-V����G�4PWPq9�$��p��~~wp@���!��=��l���~�2+}w�x�<����j�l�������O��~z����ʈ=�����7?���^����/��3�ŷo����_}�3�';�y6��t�?Q�Dn�__�~:��;x�4{�h�_��b����i��g�Q�x��kԜ\�.:ҀMF��;j�y7���Ɩ[��!F�nm�oӬ�:lglҀ��b���nR&�j��d���`-g�Jp�k�bYj�(��4�zGˍ�&%9;~�aM�U��Ki�/��͖��s����{ص�[m�^K��4�_��_j~�(X���%/I�%���V]Q��=}:tnp�h�/Q b^��!>d� wf��$���a�qd��8�:�E�%�Y3X$��4�{��<��ֽd2Ԁ��s���P�v�vlp�Xv��<���g�?癙����H~���&l�Ui�%�%#ϭ�����ZM}WnH�;�6Ҁsô��V�.HVI��P��� x��Z�~�243��y��hi��Us?ҀoC�W�u����H�i�\(��j�e�	d�ܤĉ�`Ә"P���z�LM�N�,Q����_���W;��=�X���T���Ӻˉ�&���ˠ�Y�Es|�E`�pY���=Ԥ�Ľ��r���8�Ҁg�rs0�_�k�5�٣$���5�G�k�3�٣��=JzL�ap{.0HkJ^n�|�G��3�4�Q�ܩ������#�W���-�������y9�|���}��`#$7�K�W�i�<��>��;�z7x��A�� o��;8�Ls����r�>��u���i��d�ci5_�%�TC0�����3��_"��B���I�(?k�n���q��e�#��@~VnF���>k�X#m~W1��=���O>kC�ǐ�l���=���i󳸊A�Q��Jڂ1�p6(�5���B�Ύa<,s�6?+�t{�?9�ӡ}B�����,%�'�+<,�6=k�?�����lu�|��v��,����Y6�}�T����Y��ڳ����0�yH��,�ϊ�Y�ܜ7���i�<?+��q�S�xt�������yųϚ>e��g�l�S����w�X����Y�)��ùO��%�14pky	��x�⩥�z��M��,NT:U�������4pri�j^+"�����P>���½�<�,0�v�z�:�g��c�H��h���WA}�?�ֿ�%j�@,�CN���ܓ�:.Q�6;c�CghЪ7N��1X���`!�jв�s�>���4<��VJ� ���P+�u%j��FP�[�k�߸)�Bhڳ��Av�q~�Q�p��4?������s�����4�Ҟ��Y�dE2{h��i J�=+�ǥ�m(���i B�=k˧W��Pj\�7DU<t6�kL��G�i:\m�}��6�jUړ�B�	��|�n�:����l�f:j���������)�z�?�,k��6?K�V]<{k(׏i�?�kչs�k��U���Y��E��BM�?�,;�^�;�%d��Ѻ�^㹅���W��gy~݆ʪq���󑗴�Y����s��=�Et����ϊ�Y����������;JkU�s��M���uWk�V�SD�[���+j�w�����F���{�䳦Z�����u�Ά��uȦy����t�u�u�g6��˴Fe��Q]��lk��?�}Y`m���PC�ޙ�ǳ���]|������,
#)g�����<$m~V�g�6��������6?+�\C}n��;�	%m~V�g��?/�ܞ��|%\_�x�l��Άx�e���\Cx��ֵ>�w����0p~�N��,��س{smT0�FI����Y��W<w+�N�����jrq�I��sk}����L��,^�Z��9{+�YKӼw�W�u�0����w�K,i�x�j	�欵��]7��H��U�)������{8�g.X=�j3vdl-��G?k3vbls�â�ڌ������lQ�7�M��wu��ܤn�����F�i�����M��h��I`����v-��ZÜ]�^����}���=ؐXܞ�[�41h3�XмK6��$���֗
vi�{$w����׏5 �;d˲���Ӝ^5 ���2ş�z蕪�\��P���%���l��Z)n}���i�[3w�T��r��1�໳�iq��~�Y�ۏ�4�m��5� 7��h�cp;�^�Q6-Jn�L�u� w`��L�V��3Pܑ��Gm��|n�cגN��W�V�֓�w���p�50H����%z�A���L��M��@�V+����z�����T�'���n��Zң�+8|n�0�8H�u��#KL\�����"�CE�PqS1��*����4�#Ldӵ�O�c^�����EX~MWkh��g
w;����!.a�L���=�p�ӓ��{� wdn��r���dp'    �nP3�k���7p}m���P�'���<@�i������t�;)��R�{�T�;��ڸ���5s�Ɲ�nqC�Nܼ1�,9��M;5�d`��U��������;���/7KV^��zk���B��/������6�}�����v��{���?}�����_~�������Ւ�?�
��9�Y��t{�`����4k�"��Z��r�`Ǩ/��+$~�^�^}��+�!�`��+d~ǯp�Y���;��W��`z�^!]����7.k�4�¹�-��D�����|�ӯ�/p�q�o�#��T	Z�`���Jɇ���08�X���(%���0t�f�o�-�O�d�;x�<�XX��sK ,��ն�۾��~�㦵���Ɯ����9�� �ޣ7�ɕ��ϜG����j���=����;P�d\��?�M�!�n5��;��-~�,��c�M�a��s���t9X���Z0ۮ���W0���G��ݛ�o�x��ͻ�ڏ�n� el��(]�l�q��6S��J���e��[��L�̈́5@�����?_mge�yL�ap��M�v�$STjetjrf�m�^g�����ϭ��
k3�	��S{Z�v�r��1b�5����92�kJ��faQ�ԃ�R�L��<�9��_
q7ʵg���S)��ީ��k�Y���R��TR�6���]�gR�6{Dq�Ŝ. !��vW��U��PE)�CW4�L�����-��@�}�pY��}	wC��MU Z]�V����,�N%�^>�Z[�J3rjl���(�:{@�o����O:�o�N��-����'��7Թ�)�j�*��I5�_ýi�N���7J�(�_�n��A��6=��u��N�E�KT����F���i �V̞-�}<gu�ΌMM�d�2bK����f?��d���A�4�]�ɦ��q�v^'�-� ���n��E�{y�l���jXI��c�N܎���@�n��r[\�޷�~g�2�(���v��M�[����w��LM)2�F��J���A��`EjA+��3s��Dr	<v봙����{q]-dl"����y�j�[3�>��%��Z	��SS����>��4�]~��/^�'!���! �� �c�,����Nؾa�mRrw/�Ǳ&?i ;06UhH}^���/x=	9�	~������ v�Q��WY#P�%i ;36��i�$m��a��R�<e��:Z�l���W���/E��C���$_+A�������<����r��r3<�0�H��)�=E��^Li�/3�b���y7{�T��2�"���4���*L	5�#�c~N�����i'h6�U��3c�q�fo���(�[�;�#�4@��<��t'm&�e}͞�"Mng�=i�\3�frQ�c��N��)��vR�wӘ;���1{=����ХCE�N���T[&��y�P�i���5e��i�1�$��<��.R�sZ�-�_�܍[(i�<29�x��	��<�t���se��
e�8���?���4@�ۘ�lH�^἞Ws�f�Z���u!b�^�=E�yȹܔ�dѳ���"�V�6{�=�{\m<rI�Ǽ�G,Zri��4@�x�����Q�_8*�:�f����A�@N7"� x`���Hr���P�� xd�;��z?[�I�տ��A�V�io����&B][SNJ���ϼE��ˤ�������m�Vo�bD�"*�v���$���:([�{�u��-ͫ_"ϡ7�����dE��E�U�:���w�&�:i����.7o�\���-�{�2�j4��^��Z]�BZ���o�v�,� �cr���"�GA�㘓�c#oa9�O����~�uY���VMK��S�k��R��r��)�>Bm�뎰��YY�!��5���t��\s��8�I؆���'��c��Nؖ�i;��jL2�캀�Q Zpn{5��Y؞�)^Np�x5O�s3Ú��EHb�gw����� �lT0.��� �mF�.k ����]��D�P��G߆5�j�Kw�ܩk����[�r��[k(����v�j<Sy�e�J���79^{��ǌ�N����n��6�����&K�%]B]7uגDz����{��k�n�q1bB�h|�,v�$�wi��L�z���fpG�n�]%��1��N�M! �XOp���h�z��{��t�U���C�i3w�J��6�Il~���j�[7��0�-Y'ȏ�Ϧr��7� 9�}��4�mۈc�a=�4�]��=}9+9y�t����=�Ko�9/)�8}�^���Oz�^5���΅b� b��R�aq�{�޳j��x=����N����^M�ap>l�Ln��",7��:A�1Ǖ��Ս��Z�<U2��M����8{����Ǥ�M��)�.A�OG��o�
zW�C��8��`��4���#oRm���F=��P@	�S�E7�|(��i <2���j��l On�܌#�q�������Υmk{�!~e�f���ǜ�R��xH�֍�����C(�j�ԢE@�Ќ�7i��2����㱐4@]���5:-4\�=�"�n�����K�b�э��4�]7�}�uZn��r��m�|�$��6�-D�Nq3�	o�N<�t�
�M�N��y�i =7�{8VH?}�;O�r'j[�ׄ>�X�YEk�m��<�wp*���9�4 n�N�r^\,^\�k�P���c�i ��:��j$m+f�^�4�홛�Bq�ɪ~}�1�	�.�F7��4 �7���4 ��?���4 ^�Ѽ�҆lT���5�Ћ��f����.�N�2X+��-��x�����8g��b��	�!�������@��쐴�i �2<�F�
9���

VHwa�e7�{�m���
0��a��4@^�ӝ|=�H��0�Ƣx�su��J������ȣ��c��lX$��ƾmQ�V�ڦp4f��s�1\�B��QN.w��U.���v�>H��ͽ��yO�-^B4<c��EZn�p�IC
ܦr��ɥ͉�O�0\���:�]�ћ��v���qe$p{������9�/���иoXz��4O<�^3�u߃F�[�ns� wbnsܓ�_paԞy�k�+x8�a�G�i3vR�M�~/DSP��lݰ[�J�ю�=�4�]�����X'�k��1�l� �c�v&��]!�P]n� �o�[��ؙv�"�X����L+H�JD��#Syj7M���T���O��P�w̪�4@��\���k�c=���P����6�	���5���z�Q6�9i��0���g�RX�u�u����I��'i��1�S(����T�7�[���܈����7��]�},�i�;2��q�s{0��V��mm?�ޤZ�u�5];ګ�<K���MI�a�r��+�p�dmB�J5��w�3(V��A �L�~��Ȓ0]�.Q&o�g�F��K�0]��i�d�c(?k��1����"<c���:�EQ�9mAޢ
N��"�$ơl̦����=�V�g��Nmș�-r	�+�|@���U�!���Va�,�6sk�C�r{��Y�T8� �fr��g�3��cc�N�g�o'7!k��l�q7ݤL����9� �cnZ�e��������v	N'���efXO��:ƾY��	l?�&E]w�=�V��ZA4j�����p�q:���3�v���(��1j����LnT#o_�0��m��e�w#�:�2�qA!p�'Il3ߒIؖ'��B��%g�]m�]�&덛%i ��h�.P��Ѷ�ф4�]S&�#��Q��tL��wdny{DM��(��Ը��nl�Py|� wfn��FNs�ȹV�ُ'��c1�M�5I��ۤ/�մyq� �a����^4�c���i��6�;�6�d� p;��6k��h� p{��6��(U/�z<Q�ÞikS��������J{ޤ����C@k <5�^���^9�cY�����Z5Vȩ�!k3�Sެ��6���G28X�e�]�OA�0��Y�e��N֔�M������fɒerS��O�,=Q�����m���P즹B΋xċx-�    �E\v����5@y�5�e�{�{"�{je���=����{�����΄v	b)�+����6c�?m����@�*��Z�'�~�M�+��ǎs�4�n=�\�6���h� �m�m-�7	4��px���x�^�)�&��Z�Ѧp��|�\nҒ�zt?��������	��B�"��b���B@��N_�1�	���t�R�񄽺�VZ��T�tck =3�n!�r�UVJ?}��mW��xB׺����J=X�=^�k5�з/��0�����&���gv�� Kz�Kz����$!�$@�zp;�)H஁�oI|����k�_��,�d[ql+��N�F�UEr%��3��#��!A���3�����d=L�A�4����|��?�k���)��,Ic�k [�,�v㯌~42��y�lw}����m��m���s��W׃��xH w.��\�NN���N�.�=�����̎��9ܦ��U�n-��#��f�3���ui�0Yੁ�ۄVH��\���B�t�je����^�����Y&�[�WJ�aY�����f��AlaqOڌ��X��y×s���vs� �m���)��B� � �cpݦ����=��;k �3�f2��܁i�.L�v���b%�'{k�<29ퟒ�|����N�}�b�	ғ����Z0���Zal�a&G���c�0���g`�ms�͂��d�hb���R�T Ǯ��mo^#��/�Ӎ�5@^o�{)����h����f���
�]0�^��q�PN�=i�<��S������\��c#o�\Р_�����Y��'K;a-Q�|܁,��kŠ��	7j�z=��e��D�j����q�\��iǛ>i �Zk�"Y�f�����n���V��OSEC+s�ł,�B֌1|�l�����M�@�i�W�{��֮�nZ����j���'Vg%z>�v>���#����9�vcp���ʥ���SEF� xfp6�J.*n6��6��JAa�YS��Z��������T+��������֏�Y�ɩp,P[+u��-�(�j��`��sQ���B9>�ZA�PK��Y(��a���Z.(�	o�/p5�j�DÅO��k��mX�X�����~���#�S������k ��<������+�MZ��܆����J�:=QN{���T+�=_�V���5����u#ߪ���N��8� ��ɲ��V�QA��i ���I/Zp�@����Z?��8/j�wSa/� ��!7mȭ0�x�'����u���`�ؑ�o��Zl���
i ;5���<�l�i)4����䘐m7M�U��k!��V$/�5ALx�����;�%�l�K���7U4��)j�4�mv�oJb���I�.{���T�_��V���>i��3yn|ar;MC�S��w\�\P���$`�6�m���&嗯'�����Z=��16�	$s5pr���3O�))9�P<>���An�*ߦ��;��k���3��A~�K�p Q�]JK�|��k��%J���">^��A~�
D�j{�јB wN�����3�|��$�{�EE�WX�����jX�;�>�n�$����-���e��q� wj���0WF=.����ҹb+�t0$m���ܝD�y~{<�k!w-��D=ʬn������8�7k��6n�����C�Y� w�1�]�8�k�8OHܞ���I� rP�e���N�{*�����}�=���Ը�໴S�� wfn��re�����{��W����<��_���f��K��k]�q'�&��9V�7_1�{&k �6��U�J�����|��<[�ٮ����$��C k �Zi�(��9s���K5.����N����,�>�"X�}k�,f� ��_:�5��摢\p�
9�H���[��EΙ쫿{����Z7��A�\[�#�X�u��'���� @ 7LN�h,E�W�xr��D�����敠�y��v"� y�D�~�t���3sI�ǜB�$��xP�_*��j!�_�D}�D9}�L����ٯr��Εr�-� O<[��ܙ�	s�T��5@�y��ݴD	fJrO����/���Ί֖�זZG��W�/����j����,��&8��j!�_AE��{��v����Hz2�}�4@����`ϋ``�d�]|�S�� ud�,O���υt�Z2��YAj?�`Pg����d�6SׂA����X'�!i ��1��+��{=Ƽ���S�j���iư� �}�{ V��4��A w]?�.��BZ0��N����aE�:r:v����V�8�N?-�����l�&W�@��(H O]C��E�?���9?t����RtU�cN5kx�ł�d;���ӑ�4 ��]tC�k|�4ñ�-+Ӱ�C<��yli���D�)ܚ5���|�AʛvLw̬|Ö��[=�����Ҵw�M�1�5���>�0��gp��ݼ�J��J��2�6\�	�{��3�echY��k� �� �I�,`�L����%�*A�����`���6LM�LQj?�(f��Z���bԵxј���vL��m���Lm䩇���{�n�P��RB�������.��Ck <5p�&�`da�!�j�,-,5�k= �ߊ��+{�b%X��kA �ߋWt�Qw�<��2�kE �G~���
r�18�5�m�;���,�ඍ�>Oin?}�͵��#�d��2��n�����y.̓�v�I���$��g*�]o3� zl��cA��j�f< �4����g�����f:t��4��:j�uV(b�1�4ap�A.�g�
��謑�����4��Rg
����@]!�:�t 7<7p��A�Â�i ���"B((�!{D�p��uq!�
C���Nཱྀ�6�B���b�;��=t�%�$�����NుS≖�vɐv�?I����-��U�°�ls6��;�o�����.f�eH3�*���ι���2r7:��.�ϐvZ�9K�ұ�駏3�8��\h'�x���4 ��p<;C�i�G|�te�܍BA��Ra�OF�7	� wlܭ�{����Dܭ,�d���8�"Xܹq��]r���X.
j3�W��������&B`뇸��y��=-�nӸ�g����\[���$^�&�����0�qPܮqӆ)�̌6@p��M9wR� �уy��>Ԣ��w��3���M"_�I��5��&>I��3�7z� x"�V�D8 Fs���4��7}�b7��PO���Ƞ5�c�Tm�4�B ׵دt�^~��Z��C`��M�����2���i��wk� u:!��n�i��5j��YF�j �7l��YB������^�e�_�v���i:p�Ɲ��7yly�i�;w;��Ę���ܸi��j����Bm措q��R-J�,H�n�(��[.�5v%����f��7`���M�x�uaȣ�d� �%��-ы�2�U�ѱ�i �5p�6��Ķ��LS�`�C9����S���i#� x(K��q&9K[����Nు������M#�{����e�n���ر�� wn���T��J�%�6�'��ɓ-;��x�']/�[���V��Q��.�i��4r�8�֢c��d��tr�D�|�Q��C�6� �k�Y��"$҃�Q@e�	{�f�\W�!�i �l��q6�%JY��{������ xlแ'Y��m� x"�v�͋,�1�w� wn���\�[���Y5h#���4CV@�گo���گ�S�8IH覡��ĩ��+�`�d�["���ʋ�!�X�� �k��w�B2I,��0�k �7t�ӏQ4����s� ����ڒ��NĽ�
��'�1�w� xn��k*�#�!�kxY�U�*��)T�)= GW]4���MXk�;=Mrxـv;mrBMO2�U%�U���NQ�v��~��������</z�A�	j��7rݜ^����(�i�#�����M��KN�x(V�i<>��9[�\�t�"O��R��bχ�=>��L�-'L� r  �����M�Z5lݦ�Pg����|a)׺�&�<i�!Ro��y�{�T:x^�;�C��NCܶq��; F=~��!nGܲ��Q[�!n߸E�0���`q��-Z���k��l��%D�ʢDiդ:��F�[����C=z�9n`����~w��ӡ����N��@߬��~���1ϒ�ł�9ҕ��n�h�+�a�A���s�H���P��Y�O��&��m��R5ŉ2��+��#����'J=X4ir���h���<W���|�B͒���y�t`�8<Y���f�C�MC䩑���Փw"�y&򶇊N��'i ܪ~[h�����!r���a�n�q����̓�mCF�rMe�c�;k��6n��$g�͟�!nG��q�$��k3��n����	�mW�yM1�֔иi�+)E�v<����c��w�d�E|̗�4�8}�r�H�M_�!�L��+GK�x��p�xk9�$G<�nC��n��X�4�v�`̓^6ͽ�f\Í*�l¶�iE��8MHC؎��y�I�g��7���[����i�z����C��X������4���t�H�"V��RơLݦ!������r�/��r쿪�}��ݛW/���W/���W�/��tO�{}�}���g}�0�kt�����L�D�����\-I��b2�;��ܱ���"H��tP'�;"���6�������賵j,;�z��^��>�k������أ[s�R�=��+��Fzz�z�Tt�x�ӏ�#�z~躩�r��˼-�v����U��Rѽ�۫`�q�!��-j�d�EI��9�4���$V.ixe���j��?t]I�b�����xx�ڒ��?���4���$�[Юa��%��r7Ǥ��%��2k�ڴպ4D�9ş�hG�[��Aص`�n�����{�0S�=o�5lx+b6h��4r2��En��>Zd�y�'��Aa.��c���N��D����E���=�s�f����;����ACࡁ����{H7�q�xl���dIp�/���f�y�A"1d��KV"ύ<7r���2oQ܃�k��?m�2�cVO�1_��}��Q���Fi�V�e��i�t��5�zJ6��Dnk���]Cj�p��d�֘���x$��j�Y8DCw��]�J���Mz�\�}��t����M����j�*e��i��7<�F����r��!����W�d��%����UB��1����T�Ǭ����J��i�ߗ;m&���.�U���:�����n���ki\��b��R�Fi󐗿e��'5���1�����yMi�7�,�rMɇ�6q�\ד}��Z���0��\W���<���λ�B(�(���ǀ�NC�:�;G�ĶI��\��B����S#��@�#ܦ!������1H�k5@�U#�P�,9Z"�4[���)sV�LPC�Ƽem� P���tj��6r�<��K)*��+��6[���ص�QNcn`-�B�ۘ���*I����"m��q��)��\Z[���P��Rv�5Ck+\5h=5t�5����E�JW�L���M���� W<��6�//&z%fx&L7�u��W�C�E�)O\���keڠS>Q��?I��ii�P���ny���,��{p������{�]mּh)�3Q:@��uu��ZQ���N��D4DژS���d�O4��MC��9�r=��-Yq�yj�T��I� ������.乑S�L��@��ȫ�bI,_���h�LOj�DW��M���%gK�L烆��&��R�E���"�>_G/�*���=���%�=��/�G�[eU3/$����fh-쾱�]����<��c�������kn����x!�B���P6�J~�V�i�<=teC��$ҏ6#�y~�ˆ.Z���N�g����ʚ��C�D��4�G���U5����S���H"/�7�d���a6�Z���ۆN�Qc�X�Z�̗xi�8Bo�:��DOC$Ԧ!t����Lh��a:=��nB���Ie-x�sv+0h��H��$���]��yj������c��!�L��
�$�YzJ�Q��r�P��� ^�w�t5%aXYC云�f�(�S�Ѭ��D��_�����^�      �      x�Ž뒪��(�y�S����ǵ�_z��M��"�x�����""��O�Aμ�y��T���ս�D��զPU�U�����1��5~U�W)����G[U���#���k-ޛ.,Gq��?�������P�a��댬���~�����0�b���_�	�?�p5Ce��Im�R�⮤S�R�;�2Gr���2R-��&ۇQ�����G�����7l��r�F0�G�Q6ƽ��R�!��]��\�W���)�����?�A	����CC"���5>��[*��l[�+�Y�G�!�5�Ս���:��D`�{��p�Ǳ"��j-a��Í�@4{
в\�D�Uf� uۋ Qmoj��X�c
�#R훚���8 3��1����˔���ѥ�ڟDrt԰�d�n�5��ۘz��Z��o<�5�/L	��cD�'�)8��O���jS���֑�3��)"ۡ=�6�q��Ӈ��S��p�I�7��Q,�۔a͢���Ԝ��VS�	�	��;j��=�C��w���a���m1\|�{1���
V(���y0@%.�R8v�+�K|�� �Q@tQn*|���fCU�j7��T����HQ�dQ���ߕ�������揍�3��l���7���q�\uuw�8��i�����dx+t���t-�Y\�u�d��ΜK(tb�.�m9����h[�Q�6�۫Lq*��s�������p����P�`l!�B_�l��j�FCkV�fh��2��0ޏ�ȳ�wJ�DfϞZ¨x�?�xLV�W�+�n��ϕo{��;YKkV��3�ǫQ�c�Z[�pfۛ��3��F�,�c��WF�3~��(d.VEg�Ŕ��m,�1:�v��hm_t�]y���Þ���Ĝ@Ɇ��hH��G ���Dj�0�͉�2����/�1���D�լ��P�c�N�5��zئy�q4l8���p<8�S�th��X����C}��<�������'�$�`������Sٯ}��J�_��ɲ�K�&��|�XE�����u������w�V��0 �o����'�`G�U�����
p��'�ͱ,�����(��	�̯���2*�N��h\���~V�Ӏ��ۣ��"@Qy0 ���E ���"��$C&�@���wRC��\�La�:2��]�Q�2]�nf�/+9=:hM2�u#��k2���`@�$6�=�NѤFIJd2�`�zYԐv�]G��n @Wx�h�D�b���D՚��K�d��VyCl��B��~�PZ�4
���{��8
4��Ej�7r,�������o�b�`�p)�ʓ���A$������=�V�^�୑�G�P�3{R�W$:�N1y0@y�Wځ��f�K�7 �x�鶿��C;�X��<x�:�2�����*j����p�hf��b�P�#UY��q��<Nk㭺�[wY��_V���>�,C�a@�������Wɯ��P,J��Ȑ�*���x�<l�`4��ْ�Qo�����ǵZ�[U]�Wd��́r+H�l��@�=���{�\f�r�cG����ܖ��-���v�2�P�o�.V94�5�$>���ľdIb�_�u�?#@�]�Ƈ2F|���_EԺ�@(��NF���p3���:N�]Wz�����v���vFQ�6�+��>/�k�h�>̫bXv^�6�?�2R��)0���?����D�>����`��g��jB�_=�y4y����V��?�}��+��7�\<�Ā�F�݇�||���U�?�fW�Ԧ�0^4�Nv)|XKُ�}������$�_`-V�H4mol�̡��˟������ȍ@_����ۙ%El�x����ŷ�u�ȏ���'�=)��w�΃vN=�΁�{��c�Z*Jc�	;Ǹ;�̊���Ѵ&Mp4��8AU�-%5��P��ە��5z�k@Ť�`���ʴ�P������Sg)���o3r�:i�[ U�Ԡ1��+��?��\���ͺ��{��L^��x_��N�����u\� X ?^:д�"�8�!��x.��T�+�giM�_sߞ��0���Ϗ�?����t%���U��RCҁ��K?���t��}ԣ'�Va�"&�b��P����+][?��Cz<�_z���gg�5��X��6�E�ػ.�*�D�j���MM䯸p�����b��� ���S�A#�����J�8�p��䘀y�\��W`�����68�r8����8)nVnU��TZ2���$��7����X���
'��w��`�a����pR�.'e����`�O8)�ᤴɯB�p�F0am��ζؠMm�l{�Cg��Ae���I�w�v��0�Io�ɗ�J�p�?���d�H����Q��N3��2�Vu��s[�үO5�T�j���t���j���uX{�uR���	a�u�\U���,�� 8 /j��'v}��!�C�i�o�?�=���(��s}U�Z�D���H HSt'>��B���:�3L���޲N���T�J���V��w�z'�<`ѩ��9h�X ���m�Ϊ�"Z�Z��c���˖w�G?�޿��I�aH&�ۯ�����mf7��.ZQx$]t<��]kU�U�ʅ��Pj��~�Q~�mc�<1�m����e����0�`P���d�6c��J�#���K��p�L;b��KI�(\#�e�q�Z�W�6�÷��o�
����º��L����9�?0p�\L�X/�պ�?�jQlص";Sܕ3�z��$���S��Y�gr�z����ZY<��/�������_�0����Z�Uhn�(,�TxI/BA*�q�Ek�	����S�š��w����6čTk�bm:-���q�D�`���bT�B?Ւ��4p�
0pqk�L��%x*��"��'���ߧ��LJ?�*漚q�f�+�z�M'U��P�*Um0��n=����5�k��Ns�z��<`��3̔��L�'"_p�$ 3�K�/eM��&+�vp<TŨ�\���ޛ{%^���n�w�ŶRy�V% �x)�^KI-�y)�)A|R�L>��9"[s���PSFP+�d��i���I^�sB'�]i�\��Ȼ��_�b���G��4XhhǦ�Ɲ�릸�Uh#8�f�$cN��)��J��g͊��4�_�k�b�(ב��d%W��$�^x���� ��?;(㕦�uD������%���HY�%�[N��7���8zxɅ2X��I�}���wN�0ܒn��f�7�DJQ8�+ce�h=yV�8�[�D
<3Or\˒""��#���:�>~�[�ZH#�M"[���U��x�B�j��\�wD���p*S�é�9C�.J[�M��� rOǴ���X0��S�/ۧl�t�MM=��Y�Di�{�������8����*�[c��t�� �b{��*��)Ze�W�=���y�7论f�׬2Ü�L��x��t_��p������c��r�,;`~Ϛ��-:�n���-�Xև�+�GA���k,kG��^���U��=�"�o4	���{�Q���A�����<͘1����jⲯ�%[���N+��z�CL�u�Pu^6��+\;ێ�0��cѫ?C� �FG���?�2"·���FM��W$��:�T�"���GWq��߁��}���'X����(AԥJH�� m��E���Ԋ ��*Q���@I��6�R��S���4�(B�j��e��1�m��	���
�+:6������S���hk
�J��"L=��u�Η����"�?����U�l����?2����o������Ö~���t���r�1��*�E��0��EA��g�(L�.
�xZX&̹����oml� �s΀��o��W�����~L!�0������c�a<h:��/�M�o˃���㱄V ��k�sa����*�S�����_p�����p��U�I�߿2��:S@I��)���`o��j�U xZ��XZ�u�ra4A��?��n��E�����Y�mH�(n8�G�&�߬�ok���|���C����\G�F�O������{#o��+��     ���K���	0z0�)h� ψW��Ӟk��l+mH�?ç^o����^]�V�2,������9�%����S�-��Ot~���E��A�X��-u4m���7�l��c�xK��/��6���E0� ���
D�HÊ&�����t��WEU����	-di�6�.�2>��]�����gA~{#o�J3R<@���б�/��4ˤ�M����osK#n��Ѽ��_m��1`~я����G�$%�����%����r �� RWI����lu�Lm��VC�Ԝ��=�D��έB���U�������NŨw�΃��tVUI��U�va�J���_�����k�f����6�g��:���TN�ϯ�G|�N�g�N�cL�d�K�������[<��9s�)�E�2��}3�!h�d��B4�R������v�%�J21%ُ���6�A��K��2�n(��l���s��g�'ЋO�
�Ʀ����j�x�Uh�^7���vʂ��L�= ;CZ�.������&Bb�(~�r#'�P�������Y��h�d��w
������ <¢��ӵ�������u2bJ�Au~�oFo{bOw��A��4��?:���yQ�'Gj���u҈��io	l��Z]���bY��o6q?h��q�����������	�feVܶ�CCꎰ h�Ga�E��B�}l�F%u�N:kKz���p6vx��(���j{%���`n}D����_��'���b$β/��=f�4[��s�VgڜlET��^s���±Zw��6G�/���+�Ü�^_��2�� �<I�op�0�w^ɕ�H�]'��>Vtn� !�e��5t�_0~�J�±_�~�]a�wT�k�F&��8���W����|�^�/���Q�(��uwPP�u�0w�k�����+��������l�B�{6{����@Ї�Z�va����ᨿ��i`�^��_z�E�XZ�^�Ͽ��o�14�to��
�7G|Z^_�J�*�
(���aS���oQ�Ә7J�����ޯ�]p_2w�>uX@^L\��(<��*V���l/���x��ш_4m�)l��87�EG��4�ں�9��>��7�>�o��ȇP������2y]�=������_�߿;~g��y�^u{���H=���؀����Z`�PK�΋S��^YWO����R�����/��7��/���:H�/��h�����Sۉ�x"��(2w;���i;9_8���I۽Z=�7��/��q�R����K�♶����ǝ|u?����K_\�o+�����<��QTv[բ%0{��l� �z<���P�� x����XH�*�0*̝ݽ���{�:�ʶ�GX�����>i���{����t/��E�kyj�p�/װ7��&R�S����p�����wO�˭a�P���^�(�4�M���Y|����u�g������nw
̓�QWg��5��]�M�FP�S�x�*��	�}��]�	�:)���A7���>����l�+���A�D^��0�{���w��X�7��Ջc�[�W�7��|�>�q��R��ݎ���.��C���������Ȟ�� �
�F_�����}��?��0�Kۏo���m�B���+�m��w��Ӳs,�Л�o��)����-?���l�cӞGF���>x�8��w^�H�^����ژ��zV����/���Ş�Q\��h������/���G���%}3���o��'{}f
H���ao4�a
��a-e�pY�X�)��v�Z����;�l���˹��8L����·z����;�����c|jmP�8�{��$���B�O�t~ƽ���Lc��XK�e#o%UB�� ���=��A&y,?}���`4�����8E�!��,�iܿ�x!��9��&M��n������{�ㄙ����I�\��g�b�n�
�Dtى��Ny�7:����J� }�QA����ARE�
���̿,�MV�����r�o�����H-�d�
�E����j��u����*�3����K����S�2 �!��C���Q��g������ ��j/Z[t��gT��2ĝ���C:�X�Jz�֓E\RW]��X6'JS_��^�K�Z4�s��s1�,C��1�����x^G]r��2UnW�xz��Հ�V�����LU��>X���(A���h�f��2i禜��!��e2�����q��j/����N)����9?+�1S����4�G9����4ɕ��c���5{�K�)cE��U��ƚ��S�K��{hɘ���(�
S���%杺=���w
L[��u��H�"�7
ek30)�Z���;�/a���N�Z�ۢqh��o�`�=Β,���ư�}
� Ct e�{�f��b�&Qz��%���K<V��֊m������WR�ƞ���T�l풨����VBI�^�d��YF��ܗ|@ǧq*M�^2�K�_u����e���CM�,�e��V�uo����[�8_q����9�쵪������^�`�`�΃��y�M@/#I��Tj��뒒H�OB.%�x�*��%��<"�Z����Hs����%�E����%+�$r� �=�N�T�`J.r�5�rE�*�^J�}F�1��(덢�Nw�r��vuh�l�!�[JV;5�ZZ�&��~^���(���w4�Q�t����[	@���f�QSt5�e+��6�S��D �J�%w\Aڏ���W8ǱK]�((���� �3�f	(�Z�h{B��5����=q�!��=�v����g���	A��^�ޘ�����z�oz&��j�iJ�{_g���	6H�8���L2�~2�_dPs�q�#�,�1zw��I��]_E>����ѭ=�c��%5��9U�Lo}���$�=_I��mڤ�c�Ti�a���EMn�d�:�n�8WRE��J����\��6�����8
Ǔ9��	�wn��Y�o23��Ń[*K�Nu?����/�=wқ�g�� �Hd�b��^�Ff��d`o���ۡ���a���jq� ����|p{���4�Q��E(M��.�{����ܔC�޷*�VY���z�Uu��oF�M{����^��0�6㟊��U� �2�O�*�wzS�&�D c�R�A�l��Ig��'���%F�������=gpS۬��'o�m�ؗ��m6�Y��+����烨����swρ��B�y�7&�<]��%��̫H2�XVy��#���<ܺH䏭����z���Ͷ�J�uD�A�҄9Ė��>�=�
�aw�zEc8d;�u	_U(4�����x��`�~�4��1���
�e�*�H�!�&��,3�|�+��g>�w�gk���VS��9Z-�����A�[[~�iW�6c�Yo(v��F�cX^��+
���c���x�{��|{c��VF�*ʼp�]4{a�);�E!|����H����ah��v�]S�y,v���D���EW��0�&Rq^]��Uڣ~�hM{rM�0M!�T�90��B�������^
c�T��N���C~'��):�;�!�rѴڜQ���nZ5Co�zw\�C�ء!)�ze��*�m����}��0�̃�1y�)�rU�I�F�K� :o�XF��i��KN���`)R��v��e����T��K!���� ��:�׋xW��Q���x^��#�����&;*S_��Ǿn�@�bj_����5I!���Z�ٲ��ʿP��)�A���A�zb�W
)k�����b�L�q��1-7(5��~P��u��/M"t Ry�7�62��uQhk`�@�W�
�qOw�u*�s��g_�慦����츐������b�t�m纃v��=*=c��I��w�1y�XvE{�`��OOH���̓����T������"6u�I����V����F��zg��z徝�
��/��b��a�4
7Q�f\i&V�&lyRvc+;+�ɴC����!#���\ƚD%`�u�Û�֮'�hP/�
wN�}{c�t�������4;�����X��l8X�Hlm�-_��{gZ� L7g^��7M6��Zy@����DQ,�1,K��_x    �TFPQ�(N�͓��46�����59�6��Us�]��8>�qo1
>xe~�Yn�`olr���UCW/;��e��i�u@O��x���!]�f �,�H� ��������Ck�@j\OxL����y�@J��Gޑo��ߨ@Js$Ab�c��l���9@��b�9��/�I�c�ъ�G`��y�}gW�}� 8�����.:��"��l���-�aol^� @�v�Y��6_�>��J��	
K��I���HY�Rfi6+L�p8zDn;Mܞ�u��`�R���C��׶ȭ��{c�ja�R]m+H�������̟�&G������Y���̭�����$>����Җ6ǽI�8mMS�?��%]�[�-��U��a��K���Q&��Ɔ������I`��-6Q�(ƺoŞ{�8���)n�c �dق~d=A���'��w��gY
M���� ds��}� �M;�nW(mĊ�J��mW3aC��U���=�4�/1W�76�$u�$5��>9�#�Т�|�w]L���c�Z�tM*u���vD(�֘:)�$�މv@�Ve�|���9o��b������L�#������|"I�cX��=�<��p��#�a���Gb�t9�Cp�a��*�R��h�Y�F{��|H��p���`�eq�>�U�9XN4�/��7=��N��ݷ�����t����b�;(��!���\��D���޸��o�rg{b�I�AQ���[�?�j��W�թ1��@�i.H�ij]��J1��m�4K|}X~����7.��h�'�E��<�#q�L�=��m�!]�Vr�y�����n8�7�*��m��sl����v��Q�,֯�d���Iao\nѽ3F���?��q �P"î�>�.�դHa�D��yggt1�ƶ��F��Q�G��~]�Q%�߬E��د L� L���J1#�h��'�`��}��K� G���v�������\�-TE�W\��;�]��j��+����΃�q��b�N��OУqVƳ���F*C��zA�1{}Qތ�j/��fy��륾�1���»�6
������d.�<�`o\^ԝ���9㠙<���c4�Og���"]�ɺ9r�9�Z��q C�qvO�mm�+Ŧ^��ۧ�yg�=��6�]bn�`o\n��G��;h�O���K u��w^�v��3�F�=�uw6�6�UaL���+���V'*xo��-����
�,��)�C�B+rP��~�t��9�b�ǘ_��I8�M��j'���i�2�&���[_ú����_A�Tߺ����Գz�"��{��lR��X*͐���^든C3j����-���ta9���SWVu�jn�}��ھ�!}�:� �y:��|���=�c$��f�af߂�jït���> ��qF��1W�����F�R�ʒ�T�5�[�� ��it�Zc���+I�@��.^mNЛ�&���P�Z��܉�H*��p$MQ5 ��C�5�>(J1����6�)�wK$J c����EA
H�v-on!]k�3x�f�ޕ�s�]lm���p�{�ײ��Cy>`u,\��L��W�f.��W0�w�
��j����1��]!�/V�땂ų��B\�~ ? +�,+�/P#m$���ff���8�´�l
��)��u�ғXq9��Kxm��tB��b�_<ރG��%�>��*ڤ���҉޾N�3�7a�[|�uglG�Q���&N%�驂��-Ma �<��406�{_TPI3�Nl'��Ι��[�Ø�#��>>pG�U�=�^��H���
_<~˃�rՠt`\�����D�+����5���~mxp�ծ���Zq���/TD�� }m�^���[�.��bX���b]��Q��:�M_�t�KtP�k@�^��J� ����V�Q"�ٟz39h�y�>e)�x�������=Ci�lS�ZB�}T��^i��ֱ�[y>њ���>��5��\n�\��0,V�TXi�*��D�8�6\��qz��lh1�I���x�H�x"����Ղ�fWk�.Z�+����t�_I��v'5����dRy0@�\��ZS�uX��$���Ð4X������h�r�Ȣ��B�ؠ���O��(cf�20���N�{u����9L&�v��-����KO�L�cP�0�2~���<���٧�B|rh	��NF��k�N����֖{��4�T7�P�p &�����@���(@ �f��|�-��MZ�*����ٯl�-��t�;�gb�Xx��髣����
X�;��Rtw�5Fta?���E��J�E�.2� �p2�Fk���TgܱD�V
6��|�/���S�����.0y0���0�Y8K_k�;q	���s���F�d�zN~�J����ߝS�fB�5�De��v�����6�%�]��"6d��')�@���` �|�0��q�Yl���l��nd4F���t��T����Y��?96�ަ�sF�>�c�3=�K��0�d�W0�`�����l�eA��Ex�p�"�5�0����q�V��u}���v���3��K]��D��g��dNn20�`�������}U� 'æeIax-�1�d��"�ZheY��e�]dˢ$��ƴFGKyQ-��%���!��뒁�U�,:�G�l�G_Ma�k�����o1t0��tӝV去؊�E�7%y� ���
��;�a�B@0�W�����d����F�q�e��u&���?�ޫ�*i���D*�i/\��Jߴ�+Ǚ�f�:o�M���s}�5+�
�F5$��Z�[J�zN�%�%�w�̃�ax���w���{�{i�24o6����mA2ċg)Cv5Qf�֫Dw��)M�ƨ<��F�����D� oO�W�N���d�u����';��u�{�I�ğH�/�$���1��3�:J9��<�-%���t��j����rcO�2I�n�Iu.%0�Qư7x:�]D����3Ef1�EQ4�2+����%!Q�����=.�4J�Y�� W�"W֒,\����n�&5y*l�"ژ��j�oU�]}�1q�l��7WW�=0��:������g��pJ&d�$���3�d(L��n��ֱ�u�hW4�͔�\֛�4���#�WG�k�nYu�=�V��v.R�E2�C?hSЉ����}����:�AO��W��O)���UV�S�������J����^c�����-w�J���^4��:ПiIQ�މ[�/�r��L�!Ǯ�5D���g�)$ñ�
�lc�Q&��Itܒ�m��Ro�4|L�kl��x�IJ{,��H�C���̉a�`y&�l�aݶb�8UX��,��C �\�5������X��#$�WZ�m���f��x�l/�y�H�� ��	�Ouk��*dt|��E�p�cq�ݖ�
�_E�a��R�_6��Ǻc\P�5��9�މ[�;��9ʳbJV8�Ǝ7Et{3��.݀�/Q�"�ġ��DB�l;ƌ�Z8%�jǨϔ�P�g�����'�!�vA��ث�,	�xR�.ς���͝t�c����#q<q��?��F_�,K��>�ޜ��ޝHd��veo;z:�j�R���Kqy0�o�AsA�(��P�F5�=8�F��$~"Å���8s����"��X��n�%T���m�j0����P��Gdc2�7��3.�dn�\�x�?�D8�%�I�D��.��Yc�J���RT6�5��ku�c���J��n��j��1� {z��d�T�R��ޠ	 ��7�t&���0s�d	_]����ŏ��*����ϣ�U�Wu�����Sy0�Z�b�AD��`�T���RP
������b�銾�*�A��hΘ��AOxǭ0U5Z	M���g$�lXt�SJ�3Ԗ�_��3w�?�b�,���`�.��[S��`K��ݦF����+��v�yi�t�qw$�,�\���υ��1����)��Զ�]�	!R�qt�P����H���n49�J݆N��a8�{�B�}�w����5��\,Q��Z����e&L%��)����0V�����fP�Lf�    M늯1��\Gm��5�aݒzؖ�*���*�)W_C���� jyjY�Yz;�e�� ��R�� ��Og0��2���a�u�G�R*N#�Ӌ]�kl��t�~Ŀ�!w�x�0��c���Z��!C��a���ED�xM��o��� ��N1V��z��D���ʸ�֎+C��%n��7g)��X�T�V@ ���4��>�A�57�q���-�`�즪P��Q�-3|��T_�ɥ���Z�1.P{��hz[�FB�V30"ƍ�?���31�w(n�M�YL�����ڊH�[k�LՉ��j0{7<0���a�J�o�h�ay&���pt�	&y��)��K�/���r���ZwH��=�tU֛yˎ����g@$�D�y�7�]��_�ȭs?��V
ǀ2&/�~U�\텵fzt3l.ڔ�]�)�u���]��g�v8>2���5T�[�&�T?�h�p?�(�=`�X�	�L<������SG��\Ζ��'��mP�[�MT��4�o��m�;��W0$o��)`�X�I����pc`\��?�(�2)o��pu��!	�P�{2�Kg�N's��h]��Ox�bkN˘�yŌ¨K��+@��^�`��.0�<dӯ�bJk�.�m7�.i*�A���em4?��5CwUm�W�Ҥ���F�v(+�s�gԈ[5.�t�:������^�Gl�o��[��n��l)Sԋ[r)ۍl���q�r�D!H��N�*E���/.��`{H��l�?)N�2
r��_<�+��d�e�6�I�R���Ay����`��c�#�<��V��-�y��%^1�ZćuL�K��`Ԇ���P����H�D��q�i��{���]5��h�Q��:��1h��Z�)Z�e#��Q�:�^A��hv�0��cMH�7�۝����(C&�'bG��ʘZ$k�ReӨ���v��B�+��:�:j�Pm�W6�]{Ű�|T�Dix�=\}T��g�`�`��uv�pA�d{�L`��ڪs��F�#����~RB'�<w�!E�����Z��V�f:��gc��[f(����P%��&�h	j����(�U��z]V�9����^4�r�*�ߚ��N�Kf�+@����V��1>Rn	4�H:����$1�Nvqkܪ{�q��P̐m+<��
�!�{D]����g��E���SU�S�����eY�N��D):��A7�r=��=�l��+�E��N*m���~�/iF\�fC��飪r�v�ЃpkO���������G&����'��%�>uˋɡ��+J02��1�:"V{�̄I��<@�6קD˅���p�/�/^�FO;�Ko�JaҜZjuӭ+�xƴcV��-<��D;���K���K�!����a �O�!�n}��}��H�!˚�
����deΚ{]�M�E9m6�#msؖ��C�:Ӱ��t{���c]�1 ���cũvn�K����4I0�jO�xI�T�s����5C��
���=Ub���g���T�ʲD��$�Qǋa �Oka������^���.��t�$��y%�2c2zm�5�c�R٨�]�����yT��-�aڴ�i���¯2�d` ��*PQ-�.Z y��Rxz���?�0�fY��ZƂ(����nLf��tכ�lSj����3_���{E7��b3����3+
�ܴ.L���.�h��v��ظj�v �C����Ý^������&O�ϓ��M��&M���{y~�T�A���K�.�V�y0:�.�K���c�Rdz8�#��b�Z�LgJ�j6Զ�]-T�S�R�]=�H��%WŢ�YE�M��7���F�)v�(�b��F�'�[��=��}��4N����E-?�vn�B%5U̝G1ؕX�,��H�Z;��%�eB�dmP3Z������L:�P)O3���=�?��3d:?���a,��}�P�V2���b�^���_��<�Ma��N8j���q��;cO��D��3w$20@���*i	��\A�7��M���V+MS����&�,��4��8��)zĊK�����$�4G�b�m�1\jfmJ�<.�p������\*ٔ��6���yZ��פ8Y[���N�ޒ�n�͇Y��f�e��Ŧ |Q��X��X�m���U�f|��gZ���aO��ྤ�n�霹!{g�&L���qa�ET������Aɮ����)�MO:�r�W�V��܀��5^���$�	1JnG;�n�ٕ^c���2�@�?����8n�����&����Zu{��Zs	�/Ms�*���'6�m=�p+4e�Nd`�=��.�u� ��=_���w���"(@!'�����O����h�t������)���j?������TeG�t�V��Vy��`B*��Sq��=��<���Xy�����I(`���DH\���L<�s/<�3��4�l��Z�T�8/C�ܕ]���(2���8�-��Q�8�k�)6���9|ҋ��,G����3a�(���y��Ds0NP*��θ�,�g���~�X%��3e�ĘFd��q��E�9��v�qo-�+)�\Z���P\4^!������g��3m���I�����6�"+�cgK&e����>�0�X%����-�f\w��q��(��\k�Ys�V'EG��#��::6���d��X�-���s�WHJ_�^`��y��YX�%E����fRD��Z\(�*�3���ْ�N��f��'Lo������@EQ�tI�����}��%���$rH�ϊk�#�s�>5XMky�:�I����̺��c�ˍR3h��ΐ�z�k�x��O�]��q�'o�&g��4��[$��$�8w�-�~��p��հT�Mj�S�!�I3;d��9��̜0�Y	��6sQl)�fs�Z�����h���F�k��W愻�q��9ɳ�R~!��p&`O�FY����3΂z��<X��$��/,V�Y�XU;٪��zx�3Ep93��(I��������j�:u{f��ͤz��_q}�.�j7D��w^��:����0&�������HU�X�
MV߄���PU���Ձ[��M<PJ��������l�㦊�T͘-�堲W�-jia3�YoaG�5\mؓք��Ux���g,f|^�2e�bќ�Y��b�~��g;<���&������S��+���zdӕ���K��T�N9],F�@�$�\���0Qy�Ri+�A�>��/ vǧ���R,�|a�r��$���H�c�l�B`M6��Ԋ	�bf	fߪKRgg���
���uJ�]�d���h����dE2��ձ��@��z��_6y���4�  �O�6$�#��7������ꃵ8����ʼ��MW���.�L���$j0�!/�t�fU�_!1}uW�$�3E��2�_'ߞbE����c$�������2��n�;��\����z�r���+�@g�"u\ڷg{�Ɍ	q٫s�<7iE���_ph�%��^M+Ȃg바�v�uY�f'��s��
r�8Q�0ٮ�������Q��W"�jP��fT������Pܦ%��&j/���:r�<�h�аW���w+�c�\nbY{��;(�6v�I��Z��'}��2�Q���E	��]2�e�Ř�֟뮫T�Lm�ď�V�C���e/x!:��eF�%u�:c V�ٚA0}ǳB��8����O�Q@?c��q����wv1,�r��ڙ{{e�&,י��zg-*���N�,�iW��ݘ�)�!����IQ
Q��E��	!Y4�u����݋s��-��fJu�L+�X�'���IA�;-H��`��_�M��t�`F^E�\`o�g�ef� ���lb�������4�	%H���6˅ ,jjaM�����7����ʜJ�C�m��:f��f��3�+�o�)�csS���<�Y��q9���.<شuǋ��_�������^p��P�;](ڢ
� �S�´~���e'T�1X���:;/��X�ږ��ܘ,u��?��.%{���{��<�GX-�9���m"�sn�� �
  ��o�30]�@��Z1�1���9����.���z�&V���w[8^�nz�e���^?�/�a,X�X�<�$�ڮ��.�GT�YKR�h.J
,jO��� �À�z��$��8��@��O��矓��n���;뀔u`��`e��]dDh��_;�Y�t��l�`���/m-t�!=��*�(����L��9L_!*q)e|D�=�����:�3�K�������EGg8��9�kR\�;C�Tw�nO�\�>F��nb�����>���������si�S��l:�P!� B��\c��˶o��	-x�|���qNz�?����F#O�h�S�NC�T��YG3�����Y�}�Δ�C;jc���G|�h�0���J�VDɟM��T�����˄�̭u�� �s�0\�9KXrbk��8��J��2Z8K��_�J��G����k[$,�N�|���_�,��a3ZG�T�]'8�7_��]��NL4��<j/�_��asq9�������išk����r(Es0́��'�L2��,����(E�7Y*YƔC�@q��:����G����ڶ�aP�uZ��/3�Z���
DB�K�+�]��~6�y�u��twmp8K����E
i6�,��-���z�����M�6���:�YG�kK�b�1:�>�n<�#.0�{n�*�c/sV��ioQ.��w��K�����\[l�3�+b ��0��Čko���*���)�G��C�kL��p��x/��{ø��uUъ��t��b���8�̗�/�C�Jq�ܶ���.�t���f�۩��U��,ܵd��{N��,�&&��` ��q�a7��o�"���5Sr_͠��De��}z3]�"�Rٽ�Y��b�Z��gW$�q���]��z&�uP���v%~����	x�)7���f�Isq73,w�Χ�>u�Vk�!�)ѯ5ǽqM�@�y.w��͔���r������)�{μ�~���9�t����x�.qw�({�5F�с�3�Fe?_-wޱ�Ыx��\`�]�˃�f�G����g:`�Ӫtw��Ѐ4g���Y��V����ݥ�����E�f`j�Q��s��R|�l�� 7�pv�y�;����)�%�4���)��m����l���b��F�Z����j�JsW>R.=�����MN$�<׷�(��G�X.�q�FC��3�9ҺYӥR��ԠҜ���8Z/Qo1_�}�_k�`Hf}��0O��4�>������3� A�X@I`�'Ɇ�r�IS.�(�*%�ZZ;j8��BI#"Tv���@\4��s�`BP
�&��� ��4�OF�u��FBf��f�'���j�f�Y��t�΀-�}���z��F�4Z��eZKI�e��20@�\%�V�G���}md\}u����h���û�9�����=�X�h���mG�~�����8Nv�}.�E�#�NQy�7<��ة����QM�3�J��r>\�<< �zN(Z��<��0���$��O���D�B4�����?�# ��Y۫�Ye�;���*F���	k
�n�5�}�[_�G���1�c��{�b�*�itz��}���h�5���s3U��M�T�;3`��
*5:t�0�|w�_/��f	lc"��b�Nm�O3�Ċ��` �\�7��,߂�2��[)��I����#Y�7�W��ˬ�,��@���t�mo�A7X-]���Ӿ��rv�ͧW	����� �r��t��i����%H�y:�h���M>��34�&���>��Հp&���e��fm�.U��f��+���]���Ų_R�����d��200S����)s�J�&�+�C"�kGPeO���W�;i�!9X��s�VS'��_��h��,
=�ô���k�E�zS^Hx�X�����+�~�o�D��i�+����,��Bʫ Z�)o�k�~��C��`	eW��a��?�x&y�R����fRZ�A�+�M�|��jdLal-���.)s̟x������Z�I����6<���LiG�YIr�+�� ߈�F0�`TN&u4@5�!Nc���ڗ��r�I���ڕ���=H��i�TȉQ,K�D1,C[��Z�����ff�����/z+B����H��/�g�V11�*S��G��g^.�w?���4U�;�>���.ٻ�YH�f�D9�Ky�qZn�ّ���.��x�R��&97������q�h�N�vJ�MOԇXY�͘IkK�e��obP�#�#�T� �I&����8T��(�<wVK;���=h��T��U��E c���6�:�N���Z�%�j��7�Imk	#2�Y�}�^fG��p��]�����o�j�MwF��:xе��V睟C��ta��wɈss/�4�M}]�#EgG�١=�#����g=�S��]��m%C�Ĥ�ܵ89l,���r���e*Ī�Kz�Wܖ�'2�`h����f���'�/e{	u�ipa����2a�����:�Sk
m���v=5BS*�y���}�! �ca� l$��l����N@(D�����XF&���_�-����E���'ʺ´[��>���p����i�O.�k���L�%j���ˢ�����\08���"�R`Ԣ�6Μ���*o��_���Ow[�� |l�?i���C9,uEk�7Jk�w����BsFaA0���KzA
R�`���@�x�]������Q۲���9E���G��JU~��{�Lw}'�<���|{{�� � ��      �      x��Y�n�6}�|�>�)D���-A�vw�� ɾ��^��jK6$���}iY�,ZN� F�Cg�s�3�3�;�ԥ���K��{|��Y�_����o�_��Ən|��0UIF�rʎ������{���)�	� ���R�V�|��uy�+J�HU��H2�_�x����ą�2\"�Ȫ��/8jk��Y��;�CJ�Ö����ݑv��{�֗��!Q~�Fا��e�����I��R9b3�,���J�0JX���@��5��֜�u6I�cϏ�F�gQ4#Hc����\�J�0�E	��e_�ES"�������{[z������!d��.��`�ς2����AZk�0h�/z�=5Ei�)r|BF<`1g�U�/�s?Q�cF\�X� i��ҁ��l[��Eo�x���.��bq�x|�:�!��e���n��
����)!>�#�a�l�����Af{_�K�i�l�OmB������������fJ�HŹ�,F<b�����<���hq2W��)H�.�x9����G,F�g	�	��JG�RB|���\��H��� %��-*�K�M9$(+A�$�	!�#��O�=e�*��*��5�G�G���G.�P9R��Y��02��^����V�?���hu��)EM��*������lP�����0����ޚ�2��#��ZW[�f�1g��2<�ښ;���M�0�#��1���y�Z7���|��8��9�`�y�i�wF���u����{�3�(?�C����p̴����#��a;��ccXg�?&�Mqt;��<6��r���.�p�~��b�g��i3��5�769��5�m����؅�9R2����^L�"C3���9�PďQ����`�iB)\A3�eߍ$\���O��h�����y�����q �?���|��~�Z%�9Nj�	FP��'��u쿼��ޡh,��c��|\A_����>�����A�r�=Tz�� ��yj12*[w���K��a6�'��\Y%�TE&���mW�ʻ��S�
�υ��R2��s�'0@;Oq����(Ɉ��Z���Ꟈ��jr��)��E�^����FP�ˈ�$�s~G��t{4.�#��%�c�s��Ʊo0�v\&��>m/��S�#�FЉ+���N�9�`չB��� W��?7(�(kS��ϛ��Ȝ2g�0��3��Q]!���f+s����{ #�-B�FW��w�)nB Fؓu�#s$�*��z�hT�-F�$�}wOU�p�cZz��[��A�n�'A]n��N��P��y}$S�1�M�I���9���� ^5��a��|�	F`1�7j	��)�������;�1�^��ɍ w`�Q�02�G��Y��X�{B&䵽�آm���x�Ff�G6���5�CsAi���(�>�n�#{���1
e(����<�.�ңZKF���L���Z`�Q�<�K/���Q�ʃ�v&}�,=�n0�ֈ�`"��f��(
.�Bd���������B�+6�^�cz�Q��s�TJ�0��17k%O�k-F\"�f�za�a317k�JՈ�`!k�ܬ�a��b��y���������v|_v�b!G�j��LaTwad�L`�x#�q?b!����,�����f��[E��������������E���Q/߆��24�׼��T� =�(67�(	I7��j�g�(����ѭ'�(.�bs��+\�lbn��sX�b)U�`/����I޽�b�d�����Y���Ⱦzxg�0T��������+����      �      x���[s۸����_��~�y�q�Eor��vl�UR2�Nռ0�:aے]�$��~���Ap�S��R��������P2R)q���y_�O�&�o�=��r���c��*Q�]R�Si,�L%���i��
Du1��b&���@�@��&j=�ҧE:�B)�1�㗧��W�If��4�҆d[R|�XM�eJ+.���6^�/�����?n��w~l'�L�3��4`S��q[�]�ۉ��Q$��q�+��q�}�Xn�Bs-������Pn�]�X?��1���զ|zK�ɻD%��4��5��Z\�ϟ��ù�~������_վz��D���E��5[���ϻ���z]>����?�w�M�+��h�:���TF|(������։O,�ٍGٙ>m�N��yvs�3��9��FD����(�O)oHg4�aZF���n�^}�(��Li7���T�B,�O���!�X� �u~Vw�6����NC�@/f�4��3��m�ߡ�h�g;9�"��(�,ؾ��ۣ�wIC��.D#�~{P���IC:�=�r��;k�J�k �2q�D\W_�����V��L��˫ːw�L>���T�]_������4�
����9@��`T�J�FE(ڧ���F�e�����k��jHgu��C��t���O�F�Z�Y�'�l��]̙!��ip�Ŀ� �r�ϛ`��M'�4UC���A7�Y4�L��"�5�;�}yX�x���ʧj�wL�N'�Ġa�ħE�hF��Ƙ����a���)dcRF����]��"C$f	�i(^"R���fq��	 ,ڣ�i�B0P�]��aAA�����z���~���2`�v�kf���y5�cI��L>-B��A .�>���<�0��/�,C�MاE�����C9��0 V`�� �G�ԭ�V��� �.U�O۳X��ԧE���b. ��SGί� ��}-°@!�8_#�|Ao~]=_��$�X�
@AΩ� �F�!��pe@� �������P�*�C|W~�|k�%4���!�7&B�y����c|}��
���§E�PǊ) ��1,���M�֏�O�4��6��ܧE貈h��]0�A F< x��Ha� 1[o����n
���ŷ�K�0�~ߘv}��E�j�d�hX&�ׇ�5&û�4����7-��l�� �jH��K��ݎ
�|�fJL^C2���A����5�s���,� �}�(w�>�]o��c�_��Q>BKR�f��~$�͎`N�F���&>-Bna9�x�cZ���h��;3<9K����"���!!�<M�Ň�Ŭf�p|8>bRC�$�,^��ah'����1�P�����0��C�|֘,i}��6Z��&ҘI�w���P��gTT�����]���"@P�]��i��5�!�9kؓ�b+O��-��q��2LЎ�7�<��
Z9�B���@�E�N" +���Hȉ��� ��F3^������1�����F�D�d�-��ȷU�(�D/H�`\��4@�D`UE sjNU��,�բ4#�*�	9x���F !DK}9��cI���}9�&�k1$j�e�*��f�1>`B��Z�עvM��ͧ�ӢL�����+����� �5c�C���}Z�Y� ��#H	\�#���@Rܤ>��_ϻ�[��vϏ��u,�V4eo٧բ,w�T��.>L#���@2�����n�g���3m^W��Z�'������
�ou�i��ǡI��������ȧ�Ŵi�BQbu3_��?�1��lTr)���Z�[G�U���|�/��=��9 �}*\�R��$l���!�K���V��k��(/1���8��6�6 ����cy�/K�_�������W6=-*�c�/�P��/��.#.ֻ
���+fZ�Hp�)TN/qh�*|ZTXB�Xg���R�'��|�����<Š���[`�, ��������3Z_\��cL9@c=�0��4Y�b��Gv��c4�;��0k�>-�I�8gE�I�˛�⠷ϔ5�+؉L8�87D���ON�Vn��ԧ�c缰8�p��\���}�=�vE�H��%��͒p����D��ڔ���ɉ��f�Rһ���B`�eF�]h�i����W�c��Za�\�ʝ/������t�)w��,R3uZ�NH	������i`j�d�Y�ch7�V���Y��4�`u^�J�I�	PO�hw8����J�?�?i{��;��B�O8s�W�  I��W��Q�g|0a�/�<�;ȑ0)�9� ­	�ԧ!"��]���v8�0H	�Y���-�i@h�`�����:i0�����"�cA"r�B��a�@�7��8E8-�M��b�M�$���N�{ZD{%	qF�`�> �d�cN�A�E���̺	��aˉ�z�Fh�8�n0�8��C[]|ZD;���� �8�d�"uO"sn,<����E0����Ӏ���z��H��$�jw��40���ۺE� #=ɫ~i`(A6��N��40�����0�O�J������40��<�Z0n|T��/<ϥ���IG�5�>�F#�=�F�400�#sf0���BPޜ%4~̲�Y�@��"I� �h�����ȀQ$#zin} �AR)����	CtIa��amRF,zJ�J��40�Zd�����@�����\���Hk���TDV#�X��y�((�A�6}�/9p/Z��u�N�Xj�3�i�La^�^�Bʧ�!#���~*��k9ad펍��v��Y�Ό���Av�8������� ���$�~�h��40Ȯn�n9 t��ʧ�Ո��s�3f�q�H��d0��}Z$��F���Ț�+SDg%�L�m����28��0� �gB��Do�|i���)����r� ��6�*�=��Ӏ(j�LC�<�9�2�������`�q:O�ӀPa�gpEh�T�f�&��-9�� XFGwߎ_&R��9�"�H�;1:V���zT�����)�*\5H^C�m�3�~"�p�;1B�/�_ˇ��N������'iC�E��O-�A��i Iw8���gGH�cy\Po�����A".�"��i �AA�������u�ù���O��G��\P�*�i e�0t2~6@�;������ ��5���i&�h�J�T�ٴ��4"�gk����.)v4��l�v6���> 8۰�M}�����m��vwn�4��l�v6�yS4� ���ٴY洉8 8۰�M{ N}�4��l�v6����4�����t�i7��ݟb���<�t�A������cH��ݿb��λ��-r7GX����ܷ�"wp��]tW�:Z�V[���S�� ��-���қ��i���Ʋ���ۑ�i��Tò���|Z�n"�lg�ޫ�����l����i ��ֽG��;�r���Y�����"w�a2���q�-��ޔC��q��Y�����&��>���������e�_�0<����^?�g�y���"w��6�Kk��0�{*S������PRyf���Ʊ
6��RE�ߗ��u���=����5�J ��zl�T#��n���a��
&�15&�W��}^^�?�˫?n��8H�6̸�v�fO&�1�[o����B,�ˇM�;a���5����՗NzK���k!sN���a�zWn���`�8���Հ��ƫ��K/�ª� �8&�������wI�q��3�˚Z��a0_w?��Y�����
��5G������fj8d��Z��H{+.�)w�9g�p #�0�������C����qr`�&�6��C�@��&������^V�j���ª� �֘�z� .u�"}��.p�}|,,�1s]�V~[��ƜU� ��q�8L�7�3�1�7Km5`d�a�(G)2�Ώŷǧj뽗���3f�dtoѬՀ1��%C[SX�d�gg̬���>���pW��;J�����k��o�ǐ���I{/*[-R��1�C!��)�4�N9\Qtz�/�0]�XIQ�C�����{� ���)�����,T���������h/�/�]y�
#�3�Gz�uڣ9-R��0�yC#���],o.^��RT��	 +  R�MF��>:�|�6o��i����}�w/OA��P�A1��j�E��9���b�̧�5V�¨�A��նܯ'����h���j���������<��j���=o7t��ӯ됗��1�������Ű��K��5>��)h�<YOO'��n0�g@��	w�����0�ia�S��*^�{��j��&��i`�����+�Վn�<n�u,�����h��E*=F���V��a
�:����[��-�ߪ�����m(�֦.�c��iz���>�u5_�\���|�O�����?��!��nY8g��'^��^ؑ1J��a�ŵݝ�]c��C�D�b"�Z�eƓ������1R�E��~V��_�(j)3�����Հ�j��MJ?���xY��ݳ^nk��]:�@O�?�}�n��]���r�oAob����w3K��I�{��j�P�Y�����"M/Ni<�����ǿ}�]�����[ܒ�K��y��u�O�40T�����/N\���0&�5�D���p<.������H��q�h���e
�F�02���_���E���󺻕��E�^��I�B޶�H�>owӎ�צ���d�BnN9����a#� ��;҃�_�lŜ6N��:�<�8��;�!6_�����A΋)�#��T{VO��K8G�4_03u��"$xgBU/�f����1���C��i��88�p7n���h���п�8\M�̽%�i��3�Ө�SԴ����v�P>UMr������o�=v��@�k��x�y
p\je�?v�;�ґ�r艥��U���;�:8��`j8�N)L��G/a((g�����z@��/��@���tU��OuCW�v���4`2��r�נ39�fP�)(ީ�|���̧E��0����q�<�ɦ��d��(���&ۛ_%qH�!���lC���J�q�m��@2錸�i9Lۍ���ɯT����gN�k0Jy~9m�Y���OM5Z���ӎ�i?5���I���a���-b��~SŨ�C���Z �v�m�����;�����4/      �   �  x���M��0@�3��G8,��c;��U�
���J�&Ai����J�o��*99�(o<�O��"��c~��۰m�F}l�~��}���?��6�F�m��b��ĐFC���U^�^�,�;x,,�Mӽ(ɘ�B�������4K�A���m��/���`�Sݯ�l`�cr6iWb���~�"��}V�'�������~�[����28{ѩ/��˸�ђKڗ��Zc�C2�a�6�a�Z�j��J�%�$�>�ֆ$}*0$N��2'�JɃ���(��PJ�X�g���<w���~��&��D�މ!U��|oHR�C	�x���֧���h��_�^�)�/1�-��K5dO�<24����8�����z�kZ��^7C/���q�ԛ��o���6��o�3�J%��/�j�eݯ��>��?�%:Mkb(1\�rZ1i.14��n0�OW瑡������ħe�� �\q���Okbh*���i�{v�>1�pˇ[H�?�>�O�1ra������r9      �   �   x���=��0��z�� ��ؓ�n�6�v4SD�)��"�$��[<�d301��r��U۟|��@)Q���|�_��q���^�U�9QV-5��-
�~�Z{?��(�K��XoF�Tj��v�{���)Z�oܘ�rZ�ƥ�3��� 7��� ^g����ip?�8�E���DNZjhKys[<��'�Rz+3e�R�}@�+Ns�      �      x��[��J(��֯ �����n(�k��&PPQT�Ι PQQD��t~�̟;�d���Хݫ�y����[S�����KUV& � ��^˫:V]�-,��,����4�-����k-0���-��|���O�/������7��]gh��������&�{C��_��~�����_T�E+#i[��y��m�5��UK����~�G�~��D��O��Vs8�FP ���,�;  R��upu��	�����^��u`[�7�eq�bb\H���!�)�{vF�ZS����ki;q	�ps����#F]��ޏ��}M����"��H� �!�,G��;\;sL�'5���A�^����^a�b��7�<�ZS���N����2y���dI�N�}�^��fe0r��к��6L�Ң�[�G�
mt�]��U�Y���!�t�Y08?��.�����q.8La>uL��}F@���	��XC�D��j��B��0>���i� h�A1��뾪�́_��Z�5ÚF^ܦYt�R��������u'�$���K.jÕ܃��|�5�KFω�Y�
H>��$���=�N���zA�(�LPUA��j���t�/Q���e]�b�Pn�1�^*	|vBa5���Z�j��\��U!�RC���6Mɓ��3��������Y�f�x�4����	�rn�N��+B~��-	�21$c�x���:��`C��J������)yq_,�%��9�Y�zb<����Ev_��M�������|�j}�4�p�.&m,7��0]��*�TU�O�������˓	���W/��(���wt-O'�Up�m�.�YO�7��Wh��
l[�~���PY0���䈓���p��X[��S�@�Q����}�Op�$R�b����0�pR�]�#֥ȫ����{`@p`D�.����O�~��h�d� U�����	�O�U��ך����g�/T?�"i*��OSe��}c;^u�� �$�ɹ�:�]�X5/�@_	�����m���Ć-$�1�܉r���JZY,N�����4�wSc
ŕ?���A#
�ɪ�U8nf�~bk�:X ��/L���1N�.�ǯ;��ϒMPH��j�;#h�`U;���:8K����fǓ,��뗭��Û�����P���jNX��3P�C�<+���m��M��j�2�>("�2����r�QOt��1Kg��Om?�y���{sBA)
�*�z1C�&���9MZ���KnW�j��Rk��	��["�Ί�{E��ʘ�ʘΘH���H�U�a1u�RC;K��O�D��I�"���X���[��e���an9;C{�F�[�}(F�Զ��G��7
JW��;7�}12��BH?�G�?PqӦ���pkk8����`�{�����Fk�Ԧۏ�z�ړ���-Uc�QP��3:
����'��N�_�(<��(s4_�7N0�J��tA���;؎�s[�v��J��fY����}T
C觃[?�;��QS�Z{�(�9�<��oJ���_>X�1m���VG��vK�ݴ4�7&�;�6�5e���g����"�U¶_>t��h�����?��;Y�M���'��9���c��EwXǗ;���jy�z��r.�[R8Eo������<;�Uʩ�5	�3CK��\��R�$t}�b���[�V�&iB9f�;�#����]2�,	x��_�fP-v�uT���Әn�k;_[�$g
�@��颱��vsߞӌ^�;r��Jl�9hjSE�@K�͂A}�K�^�j��Y��[��%�j"G�.aB�%���-�"�He�%�*)B��2b��$�١FN)�X��}'�p�k�Y��8�&!+JР�C�.(X@�%���XR1@B���<��� i?�F2&(�[%�ȃ�"���ZF��>53�Pt^q#�$k?à2�k���PF�	����g��B�MO�6K.7E��<��_�򢪲Z�7��������5|e/
6&����������������$�$���t�6��HsS<lB����g�X�m�Q�7���vQ�^r���_p���I�uMD<���U{��q�b)�����\�JzJZ�8jX�v���(�*Sܬ��_Ujrp��lj
A��ה�`�(З$��d8r���Y�N�Q����O��@�&J1�ڨ#������{Y����)*�7�3�q$6�m������7>Iޫ��oc�<Ðɞ�B��]���u,י_�9��k�N�o�d�:��T��ʦL|Yw�M�^�66���?�O�E]44 >me"�fP�Q��Y�R/J�K����w��?��v�	���'���\cFaN4d���zNsr�7K�����hi�K�]��7Ey��3�d�i�������{a> �A1Y0�� �`�Qu�]U�=��<ɑ_�:.��SXdu�O����if3����q�Q�'��'���9�g��U�������i���=��8t����(��@�H��Sh1������Yӵ��PZ�[�����%�2tHt�$x]�gt^�ik����$A~Ry���_�ԚX�h��tJ�s�����u*m��*IK��쯢ѮU�����ף����cλ�0(^�wJ�_��%b�]��8��j�O�W�C�1�ʉ�r�0��V���*?�p��أM�N��h!�Z��Q�Ŀ�ܘ����`����^/������K�����Fz2�5�Χfh�3GV[��ȳ�oJ�D�I1�L3?���n,űɶ�C��\���2�l:�2�;��=7fA;�L�}���n�-���h�u��\Dq.��g�����E��(9���c���IP$��/���r���j*Պ��W+�j��3iKeb�/d���F�\�5��h92Cˑ����-��l��s'W���z  (Hn�=�\�iJ��&�,o�=~Öz���
��]ҟ�bg\���
+��5������5�g�d֙���q\koM1a�p< ��86D�'���*ׄ���,���s��h�R����	����ё��nA��uY��]��_�_��HS�q��FZ�c�J����wT([?tfԏ_zR�q��G��fTrh+�R�Z�p!�A�ɱb�M �X�B��/P�Es�=��Ѓp%��SLT5�x��d���ID� !���?�}�����k�x�K�=��o�����3�zTW=�������n�(�O�"z���Wdd��'�׻�DW�|	���vr�����ŏ�g����T�>�ʵe�=	
�T_
���6�6�U>/�6�*$�=�m�ϮK����o��8��5;\8����E��nn,�y�=B���$�Á�5��1śé1��}ŏTeU�F���0��6�<�l�E��f�x2b��{����� :@p�h<��c��C EBk��cR
�3�Ɗ*�Z��a_���b�"z;o���ۉж�R����?�'��`�(��������λ��.K�}�i�4c�'��ۯ��媶;L+%I�ە<7Uܥ3��ߠ�����|P�b?���!����R����ARQ8m�fLCF� ��%h��Kف"5��ʔܱ�/���{�zm��{,3���Q�j~M��^�C[O�-�M���)t�)p>�K���FT�j�\(bՆ&	W:1�wLt�c�b^{����q߿����Hƚ�L�JC��〗pU����ր�y@b�����^fim@��K�N˰JJ�nk�*UB뫰p^�k�A�z�1�@/��|xn��/�ɗ���i����s����3����R~r�e8�[�JW�L	�]	w˒AX���R�;y"��1E| ��� q��ۂ����̦��}C��=�m�=�QX�C;P�=�"(]��r���2Z4�r�+z��  ��̐��(����K�����Ql
���3[������$��!8O��U����ޛ�<����O+>֜ЇiC��euӛ6JʢT'!�x���|[6V�e�l�l�|@+,��BȐ7��    � �WZ��G $m����$��n���|_Y�^k\���Z����f�2��W��A�Y0���Z��η7ψ��
U��d���y�8��z�ꐋ6�/[޵���{��R�(�E� i�WƆ�E�}�I�>��V(��t�\櫪P�Cj�/�WF�U/�����1��ߨS�׿�4�M�F�<Lf��:%����?���\�?�2
�6�
���������z�?�PK?~�c���~:�Qz��p���4�.r~���$�"�&�E"�ssk����r���t�p����n��iA���"l?a��e��G<݇8�a��C{��:H(����Zd�Kר������2��2�`��tw��'�d�?��s�nW@6I��Wf�.`s8�X�&?p:�No���c��L��sy����?)�'�s[ɡm�寲�s-�>ۇ��ϰ���?�k��>YY�9{7��7���>Rs
-d���C���?)���)�h���'��ug��x\-��#���hd�~��q�����;p0՚cu+C�sD>�0U�T�b'�����h[�w����v1"wx�%8y�����Cߑ�=OH������A��>/���X$��k�������v�����H���Y/G6j+����b��f�'�Oi�?��#��]��,�3DrHz1 !@�e��t���Co]���2���Qh�F��F�lT㗡@V;e��g�����J�8�{��/u���3ٶ-C�E�$eg��_B����-^5��&�榨�?�j��_fv��2�}��5���/��7�K��5읦�(6�M�E�0�nYh+��g�zJtQ��+�m�Lt�ι��\4���;�����]?EL6��lLL���>/����a��K�23�td�sS��7�n����/v�`�4�:��C;1�b-,�.]tF{O,=�R��jJO�	=`+&m��Y�o𱘹��C������Ú�8�?i���������kɇ���\���3�_1�ߢ���O��~��$�f��}�"�����W8��e�~P\�f�h<�lӛݾ�vc}t�.:�F�o���"f�x���	~r�'�]�^Yҿ��/��37}��7�F|��L<�*�r8���ߑT���ItѷӘ5J���� ��ޯ�f��2�~u�>#�(�r�
�Nso4�s�@����	}kr���By��_a����m�	��
m�b���5jc�;Ϳ� �6%+pB�j/�p�x��ɟ@>�5������m�_i�5�w��L|��/�]?����At�읮h���
j�XK�������~��+s�� ����k�;���dza�Ϊv|,���X#فC�q�p~j6��+��T[�(F��O�~�'�ay�f/��f`����������GF�Ԋb?h<��o4���|hMO+���C^1б�+������o(%�NO��_ϴ��K�x���.�כ�3��7Y�땅��{a�x���ʪDh�ȶQ��ij�(~mMA_�6�[D�L�b.�+8�(7s��θr/,�o��wV:�����E�E3�m�qw��]���bzm O��|m��ΐo��y#
ɎV�ϯ�'P�㽯S��=s/���&v��Zy���0Y	$N��ǿ���N��.��1=�����;��ꍾ:rU�Y��7��b!#O���;@�+(�����v�$�g�c������bÿ#�i �$����^��?� �ad�00 .��W�w�~�����%�_��x�e3^6R��/��7t��X�F�З�W�w�}c�Oьb����_x�!��q�{nt����wtrtG��5�x����3$'��Ӌ#���"���$]�q����� �\��N�'������{�6�d������KC����k�;ÿ1�se�헙*V"�M�#��l��^������c�;��1�YPOA � �s��	Ԯv�Z���I�:1��`��K��'>$]]{J��b��)�Ý˄��1>���`�,��Ļâ�AF����3?�C�[p�c��h�F�,JrX����KT>i2��L��|�F�X#(���:MǄ���K����s���Wʔ���v�޺�m�������łEmy�*��F+�O��<��?h2�΂7&�$̨�S�����z	+ZK�DY��2�*�Q9��iUU<��<%1���� �U"���������Ʌ�F�U�^�d�]#���v:�d��8��F @����C(fMBc�Gu3��k蟟mx�ƕ���b���(�ke J[���	�rT����'犼$����������)	Pf�%��8�Y�0���{ƚ��[X�fh�,����(�O���/41�W���������wI���m����W4k�m^�c*�n{: m�)F%:��-�T�ٺU�X���n�@�w��W��A�n�ǰw���Ĝa��;�$���/Ll6=sU"��R�?^[�i]q�B!"�C/37�XEZJ�>h*���oLV`؆(T�$2vJ�YtYI����a�l�B�,C�H軀U%�"���o�����r�(�K� �~�	5:�$�@x��iiȚ{�c5�.������p_v�e�v�����v�b��OC�{^��!�`�,���[2��l�X㋅Uk�n�+U��⩿o��ivL����N���K�h����箼���8(��[躵D(B1��F;Ù��bE���e
Je�"��Rn����	���d�'.�#Z�q~�˅�k�;�����ev=�?� �)�k(N]l�ҥB�$�~�ͽ)�o^-�UVkE	�Ψ���r_oښC�7tUmW�ra��k�ɥ�`�A�jeW�w�{c���;iՆf{h�	Q����iaXq��I�db���ЁX���#��a�~��w�4�y���R�]�.���&�(@�D��mO,��nR��'��T��o�ͭ����j�gZP��װw�c����2�C�����������;�1mϽ�S�˟� �S����b)����*֣V["���#��a����m_8t|q��ҕ�F��Ј�H����W�wc�ʶB;�|C��C)�jp�#����I�@���Z��a�����F�����G\S'6c	�ѧ�����3\ӡٰlȚK�kC���F�R�&����z��P�5�{��$��� �7Os�b6���K���:Uw� �b�2�fI�(��d�UB �ZY�+�F�*ٌWh��'2�#d��L�\"�,�;��U����E֨Bq���6�E9��-
���g���'�9s4A����O݋f���W�֫���M����˯��i��+����f���τ�s��	UH��ĳ`���ƛgYp)�TT,A!��UA/X��CE��{`Q�8��J�=�����Ʃc��gG�&6i����LףCwЙ�n���pU ��G�lU�/U�Ȃ�s�oE�A��w����ߢ��'��b!#�~��W�v����?&��ĵ���s�tGv�$V��a[��}sJ�����a��XN�%���B����͟!��m���r�>�i�|U�Ƶf�-K��bu��ɥV����}3���y92٣�ӜE��|{�*��qO�w�`�cb,�g��/8�5�J�Ѵ'9�C9��~�{�76�#ȥrF���a�%??�b!+��d��3��x@3�ݳ�!�7�.f/˘Sz�U�u�b�M[�>���t?��n�H��f�)v�1{�c>"QBe��9����XTˏ�u��݉�6�!�P�pL�VC���e�b��ҟ����iT�
Yh�ݦ�=֤kBi8Q���B}bh�p
t��jy��,�gU'DIY����ĥ3N���X�,�����~ږ�1֪�**���|B��bR[PZ��I�ģØNT���i�X@,Nd����L'�c������y�*��DEZ���b���K��iX�q���<����`��N���˅㍗�v�Jˍ�~���'&�,V����=!Y�@��eCЏ�%P�v�2�u�����s4O�L�]L�v�=�)x�W��M����BQ^�˻Qy9a|q���5��9�EG�#�    ��P� װw��멁���Y_���Gj`�Z�yC{Y�E���2k�����j���$_
�g�w>ں��;]x┱?U.��9�y���������FEl�1(�&�:�C�M�MW���3�/��,�;!G\�<U?[�Ė�w���"$E�*���9��/���a�d��F³���B�O��0'S~3���h�P��uW�z[Q�㖌W�Z��99��`�<x�h�t��}vIj��i���QP�ZJK�r�L�%/x���~����)"9����Q`�0��u�33NV���M�qbyr	J����`��P.��Qk���y{7��p�̓���+��#�Kß��_��y�#��e͇�︧	���H�Ʉʵ ��.؂��Ku�%��׺��ޙ�_7���B-ο5^���҃����� �rЀOv��rAV�
T-]3=E�i��mz��'}��j�i�;����H Vig�
�2E�EW�w�z� 
����*p-ԋ���ꐤRڸņVC��K�EYǒ� 	D'�-�'Ƌ&��������̡t�zM�� =�������2O�*zO��
�X�Y0_��6=��9���l=��l������Q{�1�fw֫u��\���&|�[a��y���J���JK����VH�����#�&�+��e]�4]i�0%6�;<3�6����G�}�_nT�0b����B�5�N�w�y���rMm)X.�c�����W��9���r��p �V\�b3+p⃚Q�t6s��e�Qw�?L[�G4�k��+�|��xM�_��c��_"/���Y�w�}�b�\�Bk���8� �E�u$Ʃ�[�i�l���(l"	ǌ��8]�Yt�'W���x����4��x�.>���u�5�J؃<0h�Qg�ba�'��\`��f9�޷��R��G�1���+�;Ͻq\�ƧuN���S�,�\�,C��I~ft��B��j�K�X�J[��^�D�YAc+�'�|}�u�w:Os!�j�cKe��y���:=n�&���_�Ex�]!��g����΅+x�P+���":���-�����CY���-�T�U�z�J����"�d��	(�2������n �7@ �A�Nș�%�z�o��sƗ�M���N�-��/��E���j%E;TgT��,�W�E�n�a�hk��:a��x�cz��|�S�r�����f6s�8l`�@Ε_m��|P�䒡��^w�T�՞9�>cn4�	) �x��Do�+n�q�@_�L<�q`z4�	��a�k��S?���F=x����W�ޚ�7��Eu�o�a_��*ByA1sƉ'f��Y�Y��E��)'�^�S�}B��Ɯ�F��������G2VS��H@�KD����*�	@q��"�)�R����ZK����<e�ݰ�k�����}49��v|u��An���+\MQ�"��(�o|fݙ3V���Y� K8��еh�rRZ&�4m����5:D�µ��Z�Q�K5�^M��EX�$�mۯ Mg M'H3o|��yL�J~1�4K�<���y^/��<c��bp(�]��݁�@��UE��\��۳ma��h��+(2�͂A�7>��������� �|�K�@�"�)(_�e�nO��G��|�8_���B� ��zf����-��R/.�a]�ϲ��:������w��r��:��B/vEN�فt5�3�k���T�8ζ��b��-��o�5�\�}���w���������ο�!h�Q$�N�Z8ϑЍ���=L\��MP/�f��N��Ʀ,���W�����d	t����O֯aOg`OC�!C [�)�����ph<��"��HLҙ��D[��k�$	z�c@i���4�Bt�ߩ��>�j��A�	2��~��V��8ߩ.�m��9:gJrq��ֆ�Q_N�N�������������Z��iV�p+������,����b	ސ�u�����5���=c�����\�K�Uq��=k��a!o�T��4ں�(�����5�Kp��bĒ|CF�},�;X�_��@���`y�Xao���}A�*��rǎ"��������7$������P��,D�zCF�}��/Q��I�@\���f`X�ۮ��,�c͜A
w�,���`f����f���QW�����/���N�+5�J��
��%��
q�ߐ������k/���k�v��.X*�N�,\�z!������3�,*�/�C��e����zj7�997⤻h���.H�U�ғ��2�Od0�L����i���k��Ipa���)cL�ZK���j�>��cn�K?�9#;��ڤX]h�DV��2���J�#"ƾ!C�>b\1��I�a�+��^����C�	���H�I#��>���U]�����Dr���a�Ed� ^�����ŧ�*(��(\���i�Z.�*���/w$~+T���}�$i�:�kc�J��E�Z�GċC����4�hi��p|�!e��5�/s��@\B9���QNND:2:�z��"A� N&~�So&'�7�ߧ.M�D�H�����B��S���.�\�EyF߃R��_5��Wgc����A��(�*�Sd은Z�_�ڨ�V_bt�*:U��S$w�;�A6�Q�b:��.���\țF��1��t)����JSu�]��m���<O��3��LI�@���΂Ab@C��� S+깶}�>ɔ��cY��̌>�o$C�&�N�64N�s8N�@���m�l��}v5}����Y�&�������x������"~�I��'��|Bd��#*��9����ӆ����#������~��o�UI�ǀh��ZU(*�OM����JO+g�	�`
j�`��4�B=�
	`V,]�ͼ�lN�q3�ťak��5z�M���DLֺ�|&��C��о#s�q�A���n�UI���4�.�A�E�CE������F g Aӷ�F�}h8���A�h.�J�Q����tw��uU�������d��͂A�)d�|�5�;kfk�;����/uX"��j��WR�2�Ό.F��a)*�e�30V�����i���|9�	�d���C�ȶӨ�g���o��ʍ�&hs8�Q�ߚH�����p����.l�a�G���m����[G��K��+D�D�!������A��Jt�L#�v����a�R�S�����Y���Ό)�-u�rc5x�:#�:OL� �Ш$���4F�g$�
$�VY�dR���8 X��U���K�ڠU.
�<W�dy��'&ZT�墻]��+Xr���bɡ�c_b	>c���#X@�<s5����3�� 76��2-(��ugR����Fr,��.T'sj����uvF���?�+�A$��Id���^�	���zÈ-�:��Ѕ]�Rx��M�ګߪ��L�M���.]g�4�+�Ǜ�F�<k�u�3}��+���z9$��Zb�J�zN�%4&��ʂ�C�����o��Q��ګ�Kw� ��{��7Lh��!]v�R����;�V&;}�$�va�3���x���n߅N�:j�5^!=��\�ڹ s���G�:���_<Oj����BA���hK�4�	s��:)��Y �պ�i�;�UU)�w�[�h��=i��L|֍1�|:�Fx���Թ�
 ��е-�;�bb^'�sB4r��(�38���_C\�R�<�Tq��Vo�����U'�����q��6zVYo����ȂAC�d�Կ&�!ֺTÈ��w���Y������q�Q���I���9�:t��oe���T	��~���f�=0;uȽFe�Vt�0HehÃ�3uU�� `yT(,>`D��x�ٿݷl9���S@��2~���`��=HNv�`�9݂8Y�2����K�Xv��֫��x�c��pP��w���;��\h�}���5c$	4�A�i���$`h�걖�]=���cl�by.1��m,E4ڄ~4���U}��KP���F��"VZW�����������@4�����]��*T��=�|4?]�Aُ�xg-$�����I�{�ه�����jf���\�js�U����f�    �#Aһ&�j�<Xfw�K�pH��C~����[F�a�B�_���ǹ# �����<�����<�($c�
V8�F�7�t{=�3ހ�*CSD�)H��B�l
C֌�J8�@�mԦ�dP������'������ ،���˗B��nv%�0�!���C����L�p_�� �&0��9�7R8sfQ^u��A�f���w�X�*͖[�6c��پb,���5�i>q���r|.�嫪.֮�M`N��㴒?���G���9j���lRB}�3b�7쐪�����E5� w:m��t\E
�N@��G8�n�]�y�?�P�"�Y�`���Iܬ��p�J�i{!)��
����ضV�J%�N�-�5M��W�
K� �д'�L�bUU�ABE���2?�GS��������0;([Zl��<t&���o�=VZ��5�
z���`=h@�Yt
�|��R�?�G#�>�?У)��J��,�~��v��`Ƃ�����+��.��L��3"L:�?��A��2^�3ժ�D�$�og��#�pTr,�Af�G�6�&��4[�u>��Mm��dW��®�o��P�ZR�$�-��>�tĎ� �,��d�!y�Lm��5�� �,�-�#~4�e�܉Ƈe�S�i��FBםK�ρZg����m�ѻ��5Q���˂A��QHf���SI�f*!�>z	��Dk�?У)�h�]���kl_`4�Q��h<�SP��bT洩T����k��l#a=h��Y�[Z�zs;��� ���&,A ���NaɘEV�������z�c�m&*\�|�^$��%k�aKES}�B<kn�H��H q#
b�?�B�5{ki4�Wqf��I���.u�4ꭴ�ҨN��}՛�#�
� �$��[�!P�a=4У)�8s�5��	H6ϭ�
M�<���Uv��{C-�V��ℤY0�4x������k��A88�b0b���)�x������P����H�UU[���fcm��:֣fK������)�;ACc�����`�>�j�?�,�eb�$�_��ps!0����!_7����ud�����r��[��hK՞?g�P��̂Al��C=�ttk�/]���9��4��=a���%��N\iv��4�ƼEW;�M�n�R�k��t�
G�?��.uk��0�.�|�/,'�F�O3a�-y8A���>����>���b� ��#l��������	ΰ�VkҒ;c-|K�4�
��&��J<�x��1��pbq��G�����j׈"�}i���p�.�97�x�ft�-��3&_$��+.A_r�^� �����?	V��Z���Ɲ��̂F�e��n
e�T&�R�=lU�����ߖ�(+�qy��#m_T��"(�葷&_�(C��zl�>lobM�0N~yf��^��SӦ��7�B7�]�MFe��sc��(v2��4S��4＂�U�s
1�f��L:��,� a��S�1�N��S�K��*�/��ݤnd�%]����8k�;�+.7D����L�JC{�zl/%X�PΉ,t���c��(��@�F�=�57�FK��!.Y^ے���a�?��LՈhX)O{�W�e?�l1"ˡ��_�moWn{�r�,�He@ƛc[+�Q��ȥu��jь�a;
9*��@ݷ]u�\��H/��i��c
1�lX��a��Dv_���a�w�H�� ��·���M�;��$Q'<^�,��� ��ڒ�-�u�\�%����s�0�q�D����L�,���o������n���^���/�u��$h����A���)w��(�3j=a��d,�ѲH��Bs��r��)D�@1�_�����y>2�I�8�Lb#��S�f�� k�,��Q�^��k�K�܈���D��3	�H񷧭GD�L��&�9�-�����r$st��tz%�40�b-hKs�5�\��J�M�qk+6Q���S��,(>};#�HC��bIU��[&��!�<8�yh����)#�&M��)��@n9��Lm���}~Δ�`ḧC��%�򊛊0Y0�"����V�U;�sg񙉏?�gd��O+�O��J
iʜXjyݩ)�hʶcZZM�zOf���¥��ck�א���0�4���/즪 �����W��x�J�sJs+��Ѵ9m�tu:��4��]��X��~Sh��E!hO����I��$@|ު�H�I�f�'���1>
���&��G��)DS��(���k�^�����;��U�#���BqR���+����`��B���@��v�� ��y��嗻(��${�$2�R���fMV�̵�|��*56�f/�w̬9�J޾��M*��+�h�*	S
��P~��h��[���/�rHt����˙B�2�d�6˗��`<����Z��BK�����JZO��+� ��*H�0�H���$��.
���c)T#��+�,�bUۍ+.�k�v��-�����^�Z��0�J=�i��I!����k{q~�T�H�E��KD1w���`t�N�o�O�8NS�r���!� ���	�M�YNڦ��˅��y�~[(���%%���Uʫ�e���G(u�>{'Xh 2Y�(����(RC��f�>KO>z�0�ciF�O��۸P��M�pgI��N��T ����iu�Q���֯�>(=y
G�r*A^
)E����S*r��C0{�T�g���,K���!ֹ��3_`V]���z�P���8�r;6j�ա��E;O���Q��y�`�(5%�d&�9V�ʼ%{�(\N\�s���);=z~���0�q�!��'?E�Xq%2UVS�!��PT����� KcNO��Ak�H ��J�j�j�ϥ0?��f� m���dY�U�"�y�$��r�̗��ӄ83J�Xm���c!�%C�K�8��&��\K���~%������K�f��͇c�aRJ7Z�|�^�����&��g�47�<.���"LM����`� 8�ބ���'7��3Wgd�h�3��Kk��z�I���ܨ��v�C7�xE�;:JMe�B_l,��èn�^�ݶ M�Zh���l�d��4���N��.����k�qq�QE�TRU��"U��� ਊ�W�~�ݻ��HR�Tr/��&�y�|A��MW��9�p͵Q.���~�����H��d�	�^�(�݆L�E�W�A.0HB�0�E�c�9&͗�r�`�F[s���2a��;o���>�!�R��|�gvd�m�߱M���H��<����������V��'w`�e�>�����YO����1����Q�Va�qF����y�����8��?�P�P�iD�p�ʾ���ne���VXʁ�2�߆Ҽ�
������C�H���@�"��˙Q�-T_.vF���K����>/��x��J7��[&�A̸��	  k4M��kzg�W�8�h%uH��B['f�oȉ5��@_m��3�BV�*p��d�>Yfi���Ɋ�.�+��Z�&7���U݅�&�7��e�uL���]�d�}������K������?����4��D7U��a�0,w�B��I`-��\;5��oެ���8�Y�^h-��P�P�MT��dѱ�.�~��kBt�js�{'8�[e��7�ߜ�;��~��)x�G��&��>NJ���D`��3�$ɩy�Aj^X�,ňֶ�$5�@�yw%k��x+�s��R��Շ��+��_����@O.���Qv�#(<�1\���Q���Tt���N�,�����-GZ�،9�Z7��zx��	g��3f�8�$�����4k�[��3�E7�te*���ݒ�vAR;a?*�����P�������l���2�^=m��f�{+�P���=�j���)ky#�	�z�כx`�@��m��ǳ]�L�tŘ�Š�S#�jan��iw�9Xn�ē�	�ɫ��:�l�S��+;�92���|���8Ʉq����n�ŧG��O�r�_�x��� ]1�d�_j#��ԋ�J�$�M�#��j�K���8��G�;D(��c9��VK�_�� �  �<7I��
�c�p1��k�f!j���j>��ٳj���Z�:m7��v����4Fd���Na-YE�m<��L9}u�t�A6�N-���^v ^v��(��i�A����S5at�;r��4Q����q`�Sd��9!�'��2q�m���qTg�S�:1u�SP}����=����l���-���:�	*V4��ߡ:AG�Ku4��i����=�̴RA��F._-�4	t��)r�e|{������:��� �t �觅��A���1]����c�q�l� ����R�h'��n3v!I'KB��Z���`���Cx��yp�����cFE�i/O�6ͯ�2��Qk.7����^�!�xE2�;���9�A�(�L��Ʊ�Hӭ��w���!�2ў�u��d_���8N�aӤ�K�5��?!WW)���`�������9m�1�}�v�!k݊�	�[6˽M!��Y�}�u�O���D�ih�Ac
}��U�=o/��j�ʙy;e�B�,XיJ�Z{%)ju�v�:姷�h����%=� )�K�f��RYR�]�7��(2(y&t��O�u��{��`}��R8��,���0)zrд�vS�4��D���廓'3>%�QW)�;�Cߏ���R�j@�E�Bq}�k?�����,NR.�8��"��v�ﭛEy���R�U(N����c�W���n�a兀�e��;���0g~�l�떍�S#�E\s�h����B�������pR<��KqКT�Q�u�a��0e�s��h�*S'�+��Ӄ�
�/J���m��ˏ��Bw��s�����2=\`�� ]��:�\�ѹ����D��܅_=s��gI���f�j�l�T<�ic0��mDo
�"C.�5G�4(n���"O�L�v�Dd"4<�b�,/&
�&�:BS�FE*XQ�j;/+��:Y�A�J��7�R8` y�������#�rh�/��+�ЁBkP���2�T\ojs��l���Yu	,l-t�3��2�,����Tj�L^!,JR����&_.���q(�b���T�~~pi�,ϑ�-C��Ʀ(A����֜V��W[����W���~��7v�`�y.��k:��6����{fe��Ys�qzϹj�V�l��B��72��d�����/4���,C�4dYU��5��s��EHњ6�x[�ov��� 6���_���pI��N�c1���K�Е��F�0Hkh�gV[��u�@���"�$Gqމ��l��h�,�ǿ4����u�o�x�� �v ��D%^N�e�,��~=XsG�T�U#y�;[m;�m�� ǚR_��2
�����8��gQIYO�iQ��c��k����8�@��3��c��4ɒ��y�p�7�<9֬�>���ە:��Z���m�à�m7��+��J�� ���٫[���3K����t!q�18��p݅+
�X��3M �� ����E�F���ׅ��<9G��g��|��$>.?�?	��������l/�w�ӆA�.�����w)�|�;��Kut�tE������^���Mc9\t��6D��;}i�x���������zbl����YdL)K������l�e�r_�É?Wr�|�"�y�$Kk��1��j�������L�׭g{vI����p���4�08��Χ�����Df���H�5��Se\�,B��S�����vjX��M|}����dMZ�Wi����정�\��#& ��=��A�2���.���g�o�i��g�3_N��4���f(�npn߭k�=�c���n�\l�Ca��A��\(�e�ςA2P��2 ��~|p�Y�XS���):P�t�];�%�l�a�R6}��p�����B��+Y{�^�g2�5)�t����Wt�Z�?>��L��|�ÝEO�Be�SdO�:�x����&�W�yO��t�W����\�3Ο=�,zh�e�"�,�����E�q�F
K�ܱ�:�Q��B��ՠԘ���(Z-po>[�=OXi�`I�w=S0�%4�2ˑ}��Y;���sO�Ê� 	)	c�b �V�5�����y�`�kK&�mY,hd�W��ܗ����M����i8R0HhhSeB�b�OP�[�4�x�3�5��Q�kM��]s�\q�d���,b�M�U��<mG2進�e�%2�}����`�_��W?ݿ=C�P�+޾�7�/�\@��1ωct�M+�w�����aܷ��s	/�x��΂��X)g���ʊ`��E�(��|/����*�#�^��4���*��@_�N�?�T��3h�ŏ���\�繄d5	r^�v�fZZ��0k⊑o�;�Z�b�vbW�V�W�� �#���D;*YV�9��<����7���{M$��f����L��&��jݝ�8sgs���`%=�]�Vs���D"�'؈+p��ӛ�S�B2�@"�<,�BU}3����l���>d�]�]m}�X�[����~uW+2-��Q��`�p�R/�'=i;��8�Os�G�f� �e��Ǳ�aR��r����y�OX%�dU���&�KQ ��N���}`��Z%��4�i���AЬ�>նs�P�D����+K~��L��!��G�����h]�Tf�������(�������Qf3z��lO㍄��k�	]r���t�Mc����ܞ�q>�t]���u �.��3>�[l�K�����&�5�sL���}^��@�
� j���lU(9��|����@��
IT�^~@J~��T�i
g�qg�e'�b(g��4u����M�Fg@N��iͼ����i�f)Pl���Z��M8��9�M�%�����Gh3ߙ�W4�o�Fu.�J�@C"��n=������d��u
	�\��U����r�!U�7���E�r:�B�G��
@�R�k������3ɻ��>�оIy�y��X���`n��5ő���F�����"����j��Ѷڠ6���r�ϔn�Ѷb�����>G�v�)�#sR)�!�A����:N�JM��]�|csR���131�]p�'K���/g�Me*Uni�Ӷq��`X�N8w⊔K��32:�%A\eC��A��ݴ������Ob�/M�N��?3�%�~�� E1��wq9>3N��5�r]���AQ��`\
4��C;(����wOC˒�
�t�l�tS�<��$C7���2�,��<G��Sp�a@#��8&V&��ĸb���� ��C      �      x������ � �     