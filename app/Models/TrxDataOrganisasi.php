<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrxDataOrganisasi extends Model
{
    use HasFactory;

    protected $table = 'trx_data_organisasi';

    protected static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->created_by = 'DATIN'; //auth('api')->user()->name;
            $model->updated_by = 'DATIN'; //auth('api')->user()->name;
        });

        self::updating(function ($model) {
            $model->updated_by = 'DATIN'; //auth('api')->user()->name;
        });
    }

    protected $fillable = [
        'trx_data_id',
        'ref_organisasi_id',
        'organisasi',

        'created_at',
        'updated_at',
        'created_by',
        'updated_by',
    ];

    public function trxData()
    {
        return $this->belongsTo(TrxData::class, 'trx_data_id');
    }

    public function refOrganisasi()
    {
        return $this->belongsTo(RefOrganisasi::class, 'ref_organisasi_id');
    }
}
