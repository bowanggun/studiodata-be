<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RefTopik extends Model
{
    use HasFactory;

    protected $table = 'ref_topik';

    protected static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->created_by = 'DATIN'; //auth('api')->user()->name;
            $model->updated_by = 'DATIN'; //auth('api')->user()->name;
        });

        self::updating(function ($model) {
            $model->updated_by = 'DATIN'; //auth('api')->user()->name;
        });
    }

    protected $fillable = [
        'topik',
        'keterangan',

        'created_at',
        'updated_at',
        'created_by',
        'updated_by',
    ];

    public function trxDataTopikLatest()
    {
        return $this->hasOne(TrxDataTopik::class, 'ref_topik_id')->latest();
    }
}
