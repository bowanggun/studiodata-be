<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RefOrganisasi extends Model
{
    use HasFactory;

    protected $table = 'ref_organisasi';

    protected static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->created_by = 'DATIN'; //auth('api')->user()->name;
            $model->updated_by = 'DATIN'; //auth('api')->user()->name;
        });

        self::updating(function ($model) {
            $model->updated_by = 'DATIN'; //auth('api')->user()->name;
        });
    }

    protected $fillable = [
        'organisasi',
        'singkatan',
        'keterangan',

        'created_at',
        'updated_at',
        'created_by',
        'updated_by',
    ];

    public function trxDataOrganisasiLatest()
    {
        return $this->hasOne(TrxDataOrganisasi::class, 'ref_organisasi_id')->latest();
    }
}
