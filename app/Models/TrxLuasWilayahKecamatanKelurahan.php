<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrxLuasWilayahKecamatanKelurahan extends Model
{
    use HasFactory;

    protected $table = 'trx_luas_wilayah_kecamatan_kelurahan';

    protected static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->created_by = 'DATIN'; //auth('api')->user()->name;
            $model->updated_by = 'DATIN'; //auth('api')->user()->name;
        });

        self::updating(function ($model) {
            $model->updated_by = 'DATIN'; //auth('api')->user()->name;
        });
    }

    protected $fillable = [
        'mst_collection_id',
        'tahun',
        'kecamatan',
        'jml_kelurahan',
        'luas_wilayah',
        'satuan',
        'catatan',

        'created_at',
        'updated_at',
        'created_by',
        'updated_by',
    ];
}
