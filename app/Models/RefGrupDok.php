<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class RefGrupDok extends Model
{
    use HasFactory;

    protected $table = 'ref_grup_dok';

    protected static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->created_by = 'DATIN'; //auth('api')->user()->name;
            $model->updated_by = 'DATIN'; //auth('api')->user()->name;
        });

        self::updating(function ($model) {
            $model->updated_by = 'DATIN'; //auth('api')->user()->name;
        });
    }

    protected $fillable = [
        'grup_dokumen',
        'keterangan',

        'created_at',
        'updated_at',
        'created_by',
        'updated_by',
    ];

    public function mstJenisDok()
    {
        return $this->hasMany(MstJenisDok::class, 'ref_grup_dok_id');
    }

    public function mstJenisDokLatest()
    {
        return $this->hasOne(MstJenisDok::class, 'ref_grup_dok_id')->latest();
    }
}
