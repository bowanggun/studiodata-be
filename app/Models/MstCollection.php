<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MstCollection extends Model
{
    use HasFactory;

    protected $table = 'mst_collection';

    protected static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->created_by = 'DATIN'; //auth('api')->user()->name;
            $model->updated_by = 'DATIN'; //auth('api')->user()->name;
        });

        self::updating(function ($model) {
            $model->updated_by = 'DATIN'; //auth('api')->user()->name;
        });
    }

    protected $fillable = [
        'nama_collection',
        'judul',
        'tipe',
        'default_chart',
        'referensi_data',
        'route_name',
        'table_name',
        'catatan',
        'ref_organisasi_id',
        'organisasi',
        'satuan',
        'definisi',
        'rumus_perhitungan',
        'cara_memperoleh_data',
        'urusan',
        'jml_dimensi',

        'created_at',
        'updated_at',
        'created_by',
        'updated_by',
    ];

    public function trxCollection()
    {
        return $this->hasMany(TrxCollection::class, 'mst_collection_id');
    }

    public function trxCollectionLatest()
    {
        return $this->hasOne(TrxCollection::class, 'mst_collection_id')->latest('tahun');
    }

    public function trxCollection1Latest()
    {
        return $this->hasOne(TrxCollection1Dimensi::class, 'mst_collection_id')->latest('tahun');
    }

    public function trxLuasWilayahKecamatanKelurahanLatest()
    {
        return $this->hasOne(TrxLuasWilayahKecamatanKelurahan::class, 'mst_collection_id')->latest();
    }
}
