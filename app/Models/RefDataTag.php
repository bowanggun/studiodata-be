<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RefDataTag extends Model
{
    use HasFactory;

    protected $table = 'ref_data_tag';

    protected static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->created_by = 'DATIN'; //auth('api')->user()->name;
            $model->updated_by = 'DATIN'; //auth('api')->user()->name;
        });

        self::updating(function ($model) {
            $model->updated_by = 'DATIN'; //auth('api')->user()->name;
        });
    }

    protected $fillable = [
        'data_tag',
        'keterangan',

        'created_at',
        'updated_at',
        'created_by',
        'updated_by',
    ];

    public function trxDataTagLatest()
    {
        return $this->hasOne(TrxDataTag::class, 'ref_data_tag_id')->latest();
    }
}
