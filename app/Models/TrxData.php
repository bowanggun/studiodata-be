<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class TrxData extends Model
{
    use HasFactory;

    protected $table = 'trx_data';

    protected static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->created_by = 'DATIN'; //auth('api')->user()->name;
            $model->updated_by = 'DATIN'; //auth('api')->user()->name;
        });

        self::updating(function ($model) {
            $model->updated_by = 'DATIN'; //auth('api')->user()->name;
        });
    }

    // protected $appends = ['UrlData'];

    protected $fillable = [
        'mst_jenis_dok_id',
        'sumber_data',
        'jenis_data',
        'nama_data',
        'deskripsi_data',
        'format_data',
        'media_type',
        'size_data',
        'nama_file',
        'ekstensi_data',
        'url_file_upload',
        'url_upload',
        'is_shared',
        'webbappeda_visible',
        'satudata_visible',
        'catatan',

        'created_at',
        'updated_at',
        'created_by',
        'updated_by',
    ];

    // public function getUrlDataAttribute()
    // {
    //     return Storage::url($this->url_file_upload);
    // }

    public function mstJenisDok()
    {
        return $this->belongsTo(MstJenisDok::class, 'mst_jenis_dok_id');
    }

    public function trxDataOrganisasi()
    {
        return $this->hasMany(TrxDataOrganisasi::class, 'trx_data_id');
    }

    public function trxDataTopik()
    {
        return $this->hasMany(TrxDataTopik::class, 'trx_data_id');
    }

    public function trxDataTag()
    {
        return $this->hasMany(TrxDataTag::class, 'trx_data_id');
    }

    public function trxRiwayatData()
    {
        return $this->hasMany(TrxRiwayatData::class, 'trx_data_id')->orderBy('id', 'desc');
    }
}
