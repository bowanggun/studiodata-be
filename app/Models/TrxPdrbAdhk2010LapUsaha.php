<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrxPdrbAdhk2010LapUsaha extends Model
{
    use HasFactory;

    protected $table = 'trx_pdrb_adhk_2010_lap_usaha';

    protected static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->created_by = 'DATIN'; //auth('api')->user()->name;
            $model->updated_by = 'DATIN'; //auth('api')->user()->name;
        });

        self::updating(function ($model) {
            $model->updated_by = 'DATIN'; //auth('api')->user()->name;
        });
    }

    protected $fillable = [
        'mst_collection_id',
        'tahun',
        'kategori',
        'target',
        'realisasi',
        'sumber',
        'catatan',
        'kemendagri_kota_kode',
        'kemendagri_kota_nama',

        'created_at',
        'updated_at',
        'created_by',
        'updated_by',
    ];
}
