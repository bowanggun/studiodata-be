<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RefKodeWilayah extends Model
{
    use HasFactory;

    protected $table = 'ref_kode_wilayah';

    protected static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->created_by = 'DATIN'; //auth('api')->user()->name;
            $model->updated_by = 'DATIN'; //auth('api')->user()->name;
        });

        self::updating(function ($model) {
            $model->updated_by = 'DATIN'; //auth('api')->user()->name;
        });
    }

    protected $fillable = [
        'kemendagri_provinsi_kode',
        'kemendagri_kota_kode',
        'kemendagri_provinsi_nama',
        'kemendagri_kota_nama',
        'bps_provinsi_kode',
        'bps_kota_kode',
        'bps_provinsi_nama',
        'bps_kota_nama',
        'latitude',
        'longitude',
        'kode_pos',

        'created_at',
        'updated_at',
        'created_by',
        'updated_by',
    ];
}
