<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MstDashboard extends Model
{
    use HasFactory;

    protected $table = 'mst_dashboard';

    protected static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->created_by = 'DATIN'; //auth('api')->user()->name;
            $model->updated_by = 'DATIN'; //auth('api')->user()->name;
        });

        self::updating(function ($model) {
            $model->updated_by = 'DATIN'; //auth('api')->user()->name;
        });
    }

    protected $fillable = [
        'nama_dashboard',
        'frame',
        'link',
        'catatan',
        'is_hidden',

        'created_at',
        'updated_at',
        'created_by',
        'updated_by',
    ];
}
