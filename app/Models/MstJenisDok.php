<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MstJenisDok extends Model
{
    use HasFactory;

    protected $table = 'mst_jenis_dok';

    protected static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->created_by = 'DATIN'; //auth('api')->user()->name;
            $model->updated_by = 'DATIN'; //auth('api')->user()->name;
        });

        self::updating(function ($model) {
            $model->updated_by = 'DATIN'; //auth('api')->user()->name;
        });
    }

    protected $fillable = [
        'jenis_dokumen',
        'ref_grup_dok_id',
        'keterangan',

        'created_at',
        'updated_at',
        'created_by',
        'updated_by',
    ];

    public function trxData()
    {
        return $this->hasMany(TrxData::class, 'mst_jenis_dok_id');
    }

    public function trxDataLatest()
    {
        return $this->hasOne(TrxData::class, 'mst_jenis_dok_id')->latest();
    }

    public function refGrupDok()
    {
        return $this->belongsTo(RefGrupDok::class, 'ref_grup_dok_id');
    }
}
