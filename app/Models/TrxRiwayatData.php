<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrxRiwayatData extends Model
{
    use HasFactory;

    protected $table = 'trx_riwayat_data';

    protected static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->created_by = 'DATIN'; //auth('api')->user()->name;
            $model->updated_by = 'DATIN'; //auth('api')->user()->name;
        });

        self::updating(function ($model) {
            $model->updated_by = 'DATIN'; //auth('api')->user()->name;
        });
    }

    protected $fillable = [
        'trx_data_id',
        'mst_jenis_dok_id',
        'sumber_data',
        'jenis_data',
        'nama_data',
        'deskripsi_data',
        'format_data',
        'media_type',
        'size_data',
        'nama_file',
        'ekstensi_data',
        'url_file_upload',
        'url_upload',
        'is_shared',
        'webbappeda_visible',
        'satudata_visible',
        'catatan',

        'created_at',
        'updated_at',
        'created_by',
        'updated_by',
    ];
}
