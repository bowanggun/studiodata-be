<?php

namespace App\Providers;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Response::macro('success', function ($data, $message = null) {
            return response()->json([
                'success' => true,
                'message' => $message,
                'result' => $data
            ]);
        });

        Response::macro('error', function ($error, $statusCode, $message = null) {
            return response()->json([
                'success' => false,
                'message' => $message,
                'error' => $error
            ], $statusCode);
        });
    }
}
