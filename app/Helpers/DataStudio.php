<?php

namespace App\Helpers;

class DataStudio
{
    public static function tipeCollection()
    {
        $data = [
            [
                'id' => 1,
                'tipe' => 'TABEL'
            ],
            [
                'id' => 2,
                'tipe' => 'GRAFIK'
            ]
        ];

        return $data;
    }

    public static function defaultChart()
    {
        $data = [
            [
                'id' => 1,
                'chart' => 'BAR'
            ],
            [
                'id' => 2,
                'chart' => 'PIE'
            ],
            [
                'id' => 3,
                'chart' => 'LINE'
            ],
            [
                'id' => 4,
                'chart' => 'COMBO BAR LINE'
            ],
            [
                'id' => 5,
                'chart' => 'STACKED'
            ],
            [
                'id' => 6,
                'chart' => 'DOUGHNUT'
            ]
        ];

        return $data;
    }

    public static function kategoriPdrb()
    {
        $data = [
            [
                'id' => 1,
                'tipe' => 'Pertanian, Kehutanan, dan Perikanan'
            ],
            [
                'id' => 2,
                'tipe' => 'Pertambangan dan Penggalian'
            ],
            [
                'id' => 3,
                'tipe' => 'Industri Pengolahan'
            ],
            [
                'id' => 4,
                'tipe' => 'Pengadaan Listrik dan Gas'
            ],
            [
                'id' => 5,
                'tipe' => 'Pengadaan Air, Pengelolaan Sampah, Limbah dan Daur Ulang'
            ],
            [
                'id' => 6,
                'tipe' => 'Konstruksi'
            ],
            [
                'id' => 7,
                'tipe' => 'Perdagangan Besar dan Eceran, Reparasi Mobil dan Sepeda Motor'
            ],
            [
                'id' => 8,
                'tipe' => 'Transportasi dan Pergudangan'
            ],
            [
                'id' => 9,
                'tipe' => 'Penyediaan Akomodasi dan Makan Minum'
            ],
            [
                'id' => 10,
                'tipe' => 'Informasi dan Komunikasi'
            ],
            [
                'id' => 11,
                'tipe' => 'Jasa Keuangan dan Asuransi'
            ],
            [
                'id' => 12,
                'tipe' => 'Real Estat'
            ],
            [
                'id' => 13,
                'tipe' => 'Jasa Perusahaan'
            ],
            [
                'id' => 14,
                'tipe' => 'Administrasi Pemerintahan, Pertahanan dan Jaminan Sosial Wajib'
            ],
            [
                'id' => 15,
                'tipe' => 'Jasa Pendidikan'
            ],
            [
                'id' => 16,
                'tipe' => 'Jasa Kesehatan dan Kegiatan Sosial'
            ],
            [
                'id' => 17,
                'tipe' => 'Jasa Lainnya'
            ]
        ];

        return $data;
    }

    public static function kategoriMuseum()
    {
        $data = [
            [
                'id' => 1,
                'tipe' => 'Wisata'
            ],
            [
                'id' => 2,
                'tipe' => 'Negeri'
            ]
        ];

        return $data;
    }

    public static function listKecamatan()
    {
        $data = [
            [
                'id' => 1,
                'tipe' => 'Bogor Selatan'
            ],
            [
                'id' => 2,
                'tipe' => 'Bogor Timur'
            ],
            [
                'id' => 3,
                'tipe' => 'Bogor Utara'
            ],
            [
                'id' => 4,
                'tipe' => 'Bogor Tengah'
            ],
            [
                'id' => 5,
                'tipe' => 'Bogor Barat'
            ],
            [
                'id' => 6,
                'tipe' => 'Tanah Sareal'
            ],
        ];

        return $data;
    }

    public static function jenisPengeluaranPdrbAdhk()
    {
        $data = [
            [
                'id' => 1,
                'tipe' => 'Pengeluaran konsumsi rumah tangga'
            ],
            [
                'id' => 2,
                'tipe' => 'Pengeluaran konsumsi pemerintah'
            ],
            [
                'id' => 3,
                'tipe' => 'Perubahan Inventori'
            ],
            [
                'id' => 4,
                'tipe' => 'Pengeluaran konsumsi LNPRT'
            ],
            [
                'id' => 5,
                'tipe' => 'Pembentukan Modal Tetap Bruto'
            ],
            [
                'id' => 6,
                'tipe' => 'Net Ekspor Barang dan Jasa'
            ],
        ];

        return $data;
    }

    public static function tingkatPendidikan()
    {
        $data = [
            [
                'id' => 1,
                'tipe' => 'Tidak/Belum Tamat Sekolah'
            ],
            [
                'id' => 2,
                'tipe' => 'Sekolah Dasar (SD)'
            ],
            [
                'id' => 3,
                'tipe' => 'SLTP'
            ],
            [
                'id' => 4,
                'tipe' => 'SLTA'
            ],
            [
                'id' => 5,
                'tipe' => 'Diploma/ Akademi/ Universitas'
            ],
        ];

        return $data;
    }

    public static function allKategori()
    {
        $data = [
            [
                'id' => 1,
                'tipe' => 'Pertanian, Kehutanan, dan Perikanan'
            ],
            [
                'id' => 2,
                'tipe' => 'Pertambangan dan Penggalian'
            ],
            [
                'id' => 3,
                'tipe' => 'Industri Pengolahan'
            ],
            [
                'id' => 4,
                'tipe' => 'Pengadaan Listrik dan Gas'
            ],
            [
                'id' => 5,
                'tipe' => 'Pengadaan Air, Pengelolaan Sampah, Limbah dan Daur Ulang'
            ],
            [
                'id' => 6,
                'tipe' => 'Konstruksi'
            ],
            [
                'id' => 7,
                'tipe' => 'Perdagangan Besar dan Eceran, Reparasi Mobil dan Sepeda Motor'
            ],
            [
                'id' => 8,
                'tipe' => 'Transportasi dan Pergudangan'
            ],
            [
                'id' => 9,
                'tipe' => 'Penyediaan Akomodasi dan Makan Minum'
            ],
            [
                'id' => 10,
                'tipe' => 'Informasi dan Komunikasi'
            ],
            [
                'id' => 11,
                'tipe' => 'Jasa Keuangan dan Asuransi'
            ],
            [
                'id' => 12,
                'tipe' => 'Real Estat'
            ],
            [
                'id' => 13,
                'tipe' => 'Jasa Perusahaan'
            ],
            [
                'id' => 14,
                'tipe' => 'Administrasi Pemerintahan, Pertahanan dan Jaminan Sosial Wajib'
            ],
            [
                'id' => 15,
                'tipe' => 'Jasa Pendidikan'
            ],
            [
                'id' => 16,
                'tipe' => 'Jasa Kesehatan dan Kegiatan Sosial'
            ],
            [
                'id' => 17,
                'tipe' => 'Jasa Lainnya'
            ],
            [
                'id' => 18,
                'tipe' => 'Wisata'
            ],
            [
                'id' => 19,
                'tipe' => 'Negeri'
            ],
            [
                'id' => 20,
                'tipe' => 'Pengeluaran konsumsi rumah tangga'
            ],
            [
                'id' => 21,
                'tipe' => 'Pengeluaran konsumsi pemerintah'
            ],
            [
                'id' => 22,
                'tipe' => 'Perubahan Inventori'
            ],
            [
                'id' => 23,
                'tipe' => 'Pengeluaran konsumsi LNPRT'
            ],
            [
                'id' => 24,
                'tipe' => 'Pembentukan Modal Tetap Bruto'
            ],
            [
                'id' => 25,
                'tipe' => 'Net Ekspor Barang dan Jasa'
            ],
            [
                'id' => 26,
                'tipe' => 'Tidak/Belum Tamat Sekolah'
            ],
            [
                'id' => 27,
                'tipe' => 'Sekolah Dasar (SD)'
            ],
            [
                'id' => 28,
                'tipe' => 'SLTP'
            ],
            [
                'id' => 29,
                'tipe' => 'SLTA'
            ],
            [
                'id' => 30,
                'tipe' => 'Diploma/ Akademi/ Universitas'
            ],
        ];

        return $data;
    }
}
