<?php

namespace App\Services;

use App\Models\TrxData;
use App\Models\TrxDataOrganisasi;
use App\Models\TrxDataTag;
use App\Models\TrxDataTopik;
use App\Models\TrxRiwayatData;

class TrxDataService
{
    public function store(array $trxData)
    {
        $data = TrxData::create($trxData);

        if ($data->jenis_data == 'UPLOAD') {
            $dataUpload =  $data->id . '_' . request()->file('url_file_upload')->hashName();
            $dataUpload = request()->file('url_file_upload')->storeAs($data->ekstensi_data, $dataUpload);
            $data->update(['url_file_upload' => $dataUpload]);
        }

        return $data;
    }

    public function update(array $trxData, TrxData $trxDatum)
    {
        $trxDatum->update($trxData);

        if ($trxDatum->jenis_data == 'UPLOAD') {
            $dataUpload =  $trxDatum->id . '_' . request()->file('url_file_upload')->hashName();
            $dataUpload = request()->file('url_file_upload')->storeAs($trxDatum->ekstensi_data, $dataUpload);
            $trxDatum->update(['url_file_upload' => $dataUpload]);
        }
    }

    public function storeRiwayatData($idTrxData)
    {
        $getData = TrxData::findOrFail($idTrxData);

        TrxRiwayatData::create([
            'trx_data_id' => $getData->id,
            'mst_jenis_dok_id' => $getData->mst_jenis_dok_id,
            'sumber_data' => $getData->sumber_data,
            'jenis_data' => $getData->jenis_data,
            'nama_data' => $getData->nama_data,
            'deskripsi_data' => $getData->deskripsi_data,
            'format_data' => $getData->format_data,
            'media_type' => $getData->media_type,
            'size_data' => $getData->size_data,
            'ekstensi_data' => $getData->ekstensi_data,
            'nama_file' => $getData->nama_file,
            'url_file_upload' => $getData->url_file_upload,
            'url_upload' => $getData->url_upload,
            'is_shared' => $getData->is_shared,
            'webbappeda_visible' => $getData->webbappeda_visible,
            'satudata_visible' => $getData->satudata_visible,
            'catatan' => $getData->catatan
        ]);
    }

    public function cariData(array $trxData)
    {
        $qTag = request('tag');
        $qTopik = request('topik');
        $qOrganisasi = request('organisasi');

        $result = TrxData::with([
            'mstJenisDok:id,ref_grup_dok_id,jenis_dokumen',
            'mstJenisDok.refGrupDok:id,grup_dokumen',
            'trxDataTag',
            'trxDataTopik',
            'trxDataOrganisasi',
            'trxRiwayatData',
        ])
            ->when(request('nama_data'), function ($result) {
                $result = $result->where('nama_data', 'ilike', '%' . request('nama_data') . '%');
            })
            ->when(request('sumber_data'), function ($result) {
                $result = $result->where('sumber_data', 'ilike', '%' . request('sumber_data') . '%');
            })
            ->when(request('deskripsi_data'), function ($result) {
                $result = $result->where('deskripsi_data', 'ilike', '%' . request('deskripsi_data') . '%');
            })
            ->when(request('ekstensi_data'), function ($result) {
                $result = $result->where('ekstensi_data', 'ilike', '%' . request('ekstensi_data') . '%');
            })
            ->when($qTag, function ($result) use ($qTag) {
                $result = $result->WhereHas('trxDataTag', function ($query) use ($qTag) {
                    $query->where('data_tag', 'ilike', '%' . $qTag . '%');
                });
            })
            ->when($qTopik, function ($result) use ($qTopik) {
                $result = $result->WhereHas('trxDataTopik', function ($query) use ($qTopik) {
                    $query->where('topik', 'ilike', '%' . $qTopik . '%');
                });
            })
            ->when($qOrganisasi, function ($result) use ($qOrganisasi) {
                $result = $result->WhereHas('trxDataOrganisasi', function ($query) use ($qOrganisasi) {
                    $query->where('organisasi', 'ilike', '%' . $qOrganisasi . '%');
                });
            })
            ->orderby('id', 'desc')
            ->get();

        return $result;
    }

    public function storeDataTag($idTrxData)
    {
        $dataTag = json_decode(request('data_tag'));

        if ($dataTag) {
            foreach ($dataTag as $row) {
                TrxDataTag::updateOrCreate(
                    [
                        'trx_data_id' => $idTrxData,
                        'ref_data_tag_id' => $row->value
                    ],
                    [
                        'trx_data_id' => $idTrxData,
                        'ref_data_tag_id' => $row->value,
                        'data_tag' => $row->label,
                    ]
                );
            }
        }
    }

    public function storeDataTopik($idTrxData)
    {
        $dataTopik = json_decode(request('data_topik'));

        if ($dataTopik) {
            foreach ($dataTopik as $row) {
                TrxDataTopik::updateOrCreate(
                    [
                        'trx_data_id' => $idTrxData,
                        'ref_topik_id' => $row->value
                    ],
                    [
                        'trx_data_id' => $idTrxData,
                        'ref_topik_id' => $row->value,
                        'topik' => $row->label,
                    ]
                );
            }
        }
    }

    public function storeDataOrganisasi($idTrxData)
    {
        $dataOrganisasi = json_decode(request('data_organisasi'));

        if ($dataOrganisasi) {
            foreach ($dataOrganisasi as $row) {
                TrxDataOrganisasi::updateOrCreate(
                    [
                        'trx_data_id' => $idTrxData,
                        'ref_organisasi_id' => $row->value
                    ],
                    [
                        'trx_data_id' => $idTrxData,
                        'ref_organisasi_id' => $row->value,
                        'organisasi' => $row->label,
                    ]
                );
            }
        }
    }
}
