<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TrxDataCariDataRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'nama_data' => [
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
            'sumber_data' => [
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
            'deskripsi_data' => [
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
            'ekstensi_data' => [
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
            'topik' => [
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
            'tag' => [
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
            'organisasi' => [
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],

        ];
    }
}
