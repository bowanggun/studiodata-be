<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class MstCollectionUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'nama_collection' => [
                'required',
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
            'judul' => [
             Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
            'tipe' => [
                'nullable',
                Rule::in(['TABEL', 'GRAFIK'])
            ],
            'default_chart' => [
                'nullable',
                Rule::IN(['BAR', 'PIE', 'LINE', 'COMBO BAR LINE', 'STACKED', 'DOUGHNUT']),
            ],
            'referensi_data' => [
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
            'route_name' => [
            // 'required',
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
            'table_name' => [
                // 'required',
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
            'catatan' => [
             Rule::notIn(['undefined', 'null', 'NULL', 'Null']),
            ],
            'ref_organisasi_id' => 'required|exists:ref_organisasi,id',
            'organisasi' => [
                'required',
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
            'satuan' => [
                'required',
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
            'definisi' => [
                Rule::notIn(['undefined', 'null', 'NULL', 'Null']),
            ],
            'rumus_perhitungan' => [
                Rule::notIn(['undefined', 'null', 'NULL', 'Null']),
            ],
            'cara_memperoleh_data' => [
                Rule::notIn(['undefined', 'null', 'NULL', 'Null']),
            ],
            'urusan' => [
                Rule::notIn(['undefined', 'null', 'NULL', 'Null']),
            ],
            'is_multidimensi' => [
                'boolean'
            ],
        ];
            
    }
}
