<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class MstJenisDokStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'jenis_dokumen' => [
                'required',
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
            'ref_grup_dok_id' => 'required|exists:ref_grup_dok,id',
            'keterangan' => [
                Rule::notIn(['undefined', 'null', 'NULL', 'Null']),
            ]
        ];
    }
}
