<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TrxCollectionUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'mst_collection_id' => 'exists:mst_collection,id',
            'tahun' => [
                'required',
                'date_format:Y',
                Rule::unique('trx_collection', 'tahun')->where('mst_collection_id', $this->input('mst_collection_id')),
                Rule::notIn(['undefined', 'null', 'NULL', 'Null']),
                'regex:/^\d{4}$/'
            ],
            'target' => [
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
            'realisasi' => [
                'required',
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
            'sumber' => [
                'required',
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
            'catatan' => [
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
            'kemendagri_kota_kode' => [
                'required',
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
            'kemendagri_kota_nama' => [
                'required',
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
        ];
    }
}
