<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TrxRiwayatDataUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'trx_data_id' => 'exists:trx_data,id',
            'mst_jenis_dok_id' => 'exists:mst_jenis_dok,id',
            'sumber_data' => [
                'required',
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
            'jenis_data' => [
                'required',
                Rule::in([
                    'UPLOAD',
                    'URL',
                ])
            ],
            'nama_data' => [
                'required',
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
            'deskripsi_data' => [
                'required',
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
            'format_data' => 'required',
            'media_type' => [
                'required_if:jenis_data,UPLOAD',
                Rule::in([
                    'application/pdf',
                    'application/msword',
                    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                    'application/vnd.ms-excel',
                    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                    'application/vnd.ms-powerpoint',
                    'application/vnd.openxmlformats-officedocument.presentationml.presentation',
                    'application/vnd.rar',
                    'application/gzip',
                    'application/zip',
                    'application/x-7z-compressed',
                    'image/jpeg',
                    'image/png',
                    'text/csv',
                    'video/mp4',
                    'video/webm',
                    'video/mpeg',
                ])
            ],
            'size_data' => 'required_if:jenis_data,UPLOAD',
            'nama_file' => 'required_if:jenis_data,UPLOAD',
            'ekstensi_data' => [
                'sometimes',
            ],
            'url_file_upload' => 'required',
            'is_shared' => [
                'required',
                'boolean'
            ],
            'catatan' => [
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
        ];
    }

    protected function prepareForValidation(): void
    {
        $mediaType = null;
        $sizeData = null;
        $namaFile = null;
        $formatData = null;

        $urlFile = $this->url_file_upload;

        if ($this->request->get('jenis_data') == 'UPLOAD') {
            $mediaType = $urlFile->getMimeType();
            $sizeData = $urlFile->getSize();
            $namaFile = $urlFile->getClientOriginalName();
        }

        if ($mediaType == 'application/pdf') {
            $formatData = 'PDF';
        } else if ($mediaType == 'application/msword' || $mediaType == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
            $formatData = 'DOCUMENT';
        } else if ($mediaType == 'application/vnd.ms-excel' || $mediaType == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
            $formatData = 'EXCEL';
        } else if ($mediaType == 'application/vnd.ms-powerpoint' || $mediaType == 'application/vnd.openxmlformats-officedocument.presentationml.presentation') {
            $formatData = 'POWERPOINT';
        } else if ($mediaType == 'image/jpeg' || $mediaType == 'image/png') {
            $formatData = 'IMAGE';
        } else if ($mediaType == 'application/vnd.rar' || 'application/gzip' || 'application/zip' || 'application/x-7z-compressed') {
            $formatData = 'RAR';
        } else if ($mediaType == 'text/csv') {
            $formatData = 'EXCEL';
        } else if ($mediaType == 'video/mp4' || $mediaType == 'video/mpeg' || $mediaType == 'video/webm') {
            $formatData = 'VIDEO';
        } else if ($mediaType == 'video/mpeg') {
            $formatData = 'AUDIO';
        }

        $this->merge([
            'media_type' => $mediaType,
            'size_data' => $sizeData,
            'nama_file' => $namaFile,
            'format_data' => $formatData,
        ]);
    }
}
