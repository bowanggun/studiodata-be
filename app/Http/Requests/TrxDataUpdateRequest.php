<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TrxDataUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'mst_jenis_dok_id' => [
                'exists:mst_jenis_dok,id',
                Rule::notIn(['undefined', 'null', 'NULL', 'Null']),
                'sometimes',
                'nullable',
            ],
            'nama_data' => [
                'required',
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
            'sumber_data' => [
                'required',
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
            'jenis_data' => [
                'required',
                Rule::in([
                    'UPLOAD',
                    'URL',
                ])
            ],
            'url_file_upload' => [
                'required_if:jenis_data,UPLOAD',
                'nullable',
                'mimes:pdf,docx,doc,ppt,pptx,xls,xlsx,csv,zip,rar,jpg,png,mp4',
            ],
            'url_upload' => [
                'required_if:jenis_data,URL'
            ],
            'deskripsi_data' => [
                'required',
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
            'format_data' => Rule::notIn(['undefined', 'null', 'NULL', 'Null']),
            'media_type' => [
                'nullable',
                // 'required_if:jenis_data,UPLOAD',
                // Rule::in([
                //     'application/pdf',
                //     'application/msword',
                //     'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                //     'application/vnd.ms-excel',
                //     'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                //     'application/vnd.ms-powerpoint',
                //     'application/vnd.openxmlformats-officedocument.presentationml.presentation',
                //     'application/vnd.rar',
                //     'application/gzip',
                //     'application/zip',
                //     'application/x-7z-compressed',
                //     'application/x-rar-compressed',
                //     'application/x-zip-compressed',
                //     'application/x-rar',
                //     'multipart/x-zip',
                //     'image/jpeg',
                //     'image/png',
                //     'text/csv',
                //     'video/mp4',
                //     'video/webm',
                //     'audio/mpeg',
                // ])
            ],
            'size_data' => 'sometimes',
            'nama_file' => 'sometimes',
            'ekstensi_data' => [
                'sometimes',
            ],
            'is_shared' => [
                'required',
                'boolean'
            ],
            'catatan' => [
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
            'data_tag' => 'nullable',
            'data_organisasi' => 'nullable',
            'data_topik' => 'nullable'
        ];
    }

    public function validated($key = null, $default = null)
    {
        $mediaType = 'Unknown';
        $sizeData = 'Unknown';
        $namaFile = null;
        $formatData = 'Unknown';
        $ekstensiData = 'Unknown';

        $urlFileUpload = $this->get('url_file_upload');
        $urlUpload = $this->url_upload;

        if ($this->request->get('jenis_data') == 'UPLOAD' && $this->hasFile($urlFileUpload)) {
            $mediaType = $this->url_file_upload->getMimeType();
            $sizeData = $this->url_file_upload->getSize();
            $namaFile = $this->url_file_upload->getClientOriginalName();
            $ekstensiData = $this->url_file_upload->getClientOriginalExtension();
            $urlUpload = null;

            if ($mediaType == 'application/pdf') {
                $formatData = 'PDF';
            } else if ($mediaType == 'application/msword' || $mediaType == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
                $formatData = 'DOCUMENT';
            } else if ($mediaType == 'application/vnd.ms-excel' || $mediaType == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                $formatData = 'EXCEL';
            } else if ($mediaType == 'application/vnd.ms-powerpoint' || $mediaType == 'application/vnd.openxmlformats-officedocument.presentationml.presentation') {
                $formatData = 'POWERPOINT';
            } else if ($mediaType == 'image/jpeg' || $mediaType == 'image/png') {
                $formatData = 'IMAGE';
            } else if ($mediaType == 'text/csv') {
                $formatData = 'EXCEL';
            } else if ($mediaType == 'video/mp4' || $mediaType == 'video/mpeg' || $mediaType == 'video/webm') {
                $formatData = 'VIDEO';
            } else if ($mediaType == 'audio/mpeg' || $mediaType == 'audio/mp4') {
                $formatData = 'AUDIO';
            } else if ($mediaType == 'application/vnd.rar' || 'application/gzip' || 'application/zip' || 'application/x-7z-compressed' || 'application/x-rar-compressed' || 'application/x-zip-compressed' || 'application/x-rar' || 'multipart/x-zip') {
                $formatData = 'RAR';
            }
        }

        if ($this->request->get('jenis_data') == 'URL') {
            $formatData = 'URL';
            $mediaType = null;
            $urlFileUpload = null;
            $sizeData = null;
            $namaFile = null;
            $ekstensiData = null;
        }

        return array_merge(parent::validated(), [
            'media_type' => $mediaType,
            'size_data' => $sizeData,
            'nama_file' => $namaFile,
            'ekstensi_data' => $ekstensiData,
            'format_data' => $formatData,
            'url_upload' => $urlUpload,
            'url_file_upload' => $urlFileUpload,
        ]);
    }
}
