<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RefGrupDokStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'grup_dokumen' => [
                'required',
                'unique:ref_grup_dok,grup_dokumen',
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
            'keterangan' => [
                Rule::notIn(['undefined', 'null', 'NULL', 'Null']),
            ]
        ];
    }
}
