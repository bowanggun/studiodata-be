<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TrxDataTagUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'trx_data_id' => 'required|exists:trx_data,id',
            'ref_data_tag_id' => 'required|exists:ref_data_tag,id',
            'data_tag' => [
                'required',
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
        ];
    }
}
