<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TrxDataTopikStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'trx_data_id' => 'required|exists:trx_data,id',
            'ref_topik_id' => [
                'required',
                'exists:ref_topik,id',
                // Rule::exists('trx_data_topik', 'trx_data_id')->where('ref_topik_id', $this->ref_topik_id)
                // 'unique:trx_data_topik,trx_data_id,null,id,ref_topik_id' . $this->ref_topik_id,
            ],
            'topik' => [
                'required',
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
        ];
    }
}
