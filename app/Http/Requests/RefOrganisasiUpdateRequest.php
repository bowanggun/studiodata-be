<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RefOrganisasiUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'organisasi' => [
                'required',
                'unique:ref_organisasi,organisasi',
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
            'singkatan' => [
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
            'keterangan' => [
                Rule::notIn(['undefined', 'null', 'NULL', 'Null']),
            ]
        ];
    }
}
