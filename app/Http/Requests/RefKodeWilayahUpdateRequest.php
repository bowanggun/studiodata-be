<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RefKodeWilayahUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'kemendagri_provinsi_kode' => [
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
            'kemendagri_kota_kode' => [
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
            'kemendagri_provinsi_nama' => [
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
            'kemendagri_kota_nama' => [
                'required',
                Rule::notIn(['undefined', 'null', 'NULL', 'Null']),
            ],
            'bps_provinsi_kode' => [
                Rule::notIn(['undefined', 'null', 'NULL', 'Null']),
            ],
            'bps_kota_kode' => [
                Rule::notIn(['undefined', 'null', 'NULL', 'Null']),
            ],
            'bps_provinsi_nama' => [
                Rule::notIn(['undefined', 'null', 'NULL', 'Null']),
            ],
            'bps_kota_nama' => [
                'required',
                Rule::notIn(['undefined', 'null', 'NULL', 'Null']),
            ],
            'latitude' => [
                Rule::notIn(['undefined', 'null', 'NULL', 'Null']),
            ],
            'longitude' => [
                Rule::notIn(['undefined', 'null', 'NULL', 'Null']),
            ],
            'kode_pos' => [
                Rule::notIn(['undefined', 'null', 'NULL', 'Null']),
            ],
        ];
    }
}
