<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TrxDataUbahInteroperabilitasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'is_shared' => [
                'required',
                'boolean'
            ],
            'webbappeda_visible' => [
                'required',
                'boolean'
            ],
            'satudata_visible' => [
                'required',
                'boolean'
            ],
        ];
    }
}
