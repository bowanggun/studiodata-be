<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TrxLuasWilayahKecamatanKelurahanStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'mst_collection_id' => 'exists:mst_collection,id',
            'tahun' => [
                'required',
                'date_format:Y',
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
            'kecamatan' => [
                'required',
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
            'jml_kelurahan' => ['required', 'numeric'],
            'luas_wilayah' => ['required', 'numeric'],
            'satuan' => [
                'nullable',
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
            'catatan' => [
                Rule::notIn(['undefined', 'null', 'NULL', 'Null'])
            ],
        ];
    }
}
