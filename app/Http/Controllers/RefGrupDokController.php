<?php

namespace App\Http\Controllers;

use App\Http\Requests\RefGrupDokStoreRequest;
use App\Http\Requests\RefGrupDokUpdateRequest;
use App\Http\Resources\RefGrupDokResource;
use App\Models\RefGrupDok;

class RefGrupDokController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $refGrupDokumen = RefGrupDok::with('mstJenisDokLatest:id,ref_grup_dok_id')->when(request()->q, function ($refGrupDokumen) {
                $refGrupDokumen = $refGrupDokumen->where('grup_dokumen', 'ilike', '%' . request()->q . '%');
            })
                ->orderby('id', 'desc')
                ->paginate(10);

            return response()->success($refGrupDokumen, 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(RefGrupDokStoreRequest $request)
    {
        try {
            RefGrupDok::create($request->validated());

            return response()->success(null, 'Data Tersimpan');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Tersimpan');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(RefGrupDok $grupDok)
    {
        try {
            return response()->success($grupDok, 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(RefGrupDok $refGrupDok)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(RefGrupDokUpdateRequest $request, RefGrupDok $grupDok)
    {
        try {
            $grupDok->update($request->validated());

            return response()->success(null, 'Perubahan Tersimpan');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Perubahan Gagal Tersimpan');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(RefGrupDok $grupDok)
    {
        try {
            $grupDok->delete();

            return response()->success(null, 'Data Terhapus');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal menghapus data');
        }
    }
}
