<?php

namespace App\Http\Controllers;

use App\Http\Requests\RefOrganisasiStoreRequest;
use App\Http\Requests\RefOrganisasiUpdateRequest;
use App\Models\RefOrganisasi;
use Illuminate\Http\Request;

class RefOrganisasiController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $paging = request()->paging;

            $query = RefOrganisasi::with('trxDataOrganisasiLatest:id,ref_organisasi_id')
                ->when(request()->q, function ($query) {
                    $query = $query->where('organisasi', 'ilike', '%' . request()->q . '%')->orWhere('singkatan', 'ilike', '%' . request()->q . '%');
                })
                ->orderby('id', 'desc');
            // ->simplepaginate(10);

            if ($paging == 'false') {
                $refOrganisasi = $query->get();
            } else {
                $refOrganisasi = $query->simplepaginate(10);
            }

            return response()->success($refOrganisasi, 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(RefOrganisasiStoreRequest $request)
    {
        try {
            RefOrganisasi::create($request->validated());

            return response()->success(null, 'Data Tersimpan');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Tersimpan');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(RefOrganisasi $organisasi)
    {
        try {
            return response()->success($organisasi, 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(RefOrganisasi $refOrganisasi)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(RefOrganisasiUpdateRequest $request, RefOrganisasi $organisasi)
    {
        try {
            $organisasi->update($request->validated());

            return response()->success(null, 'Perubahan Tersimpan');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Perubahan Gagal Tersimpan');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(RefOrganisasi $organisasi)
    {
        try {
            $organisasi->delete();

            return response()->success(null, 'Data Terhapus');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal menghapus data');
        }
    }
}
