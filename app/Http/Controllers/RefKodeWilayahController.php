<?php

namespace App\Http\Controllers;

use App\Http\Requests\RefKodeWilayahStoreRequest;
use App\Http\Requests\RefKodeWilayahUpdateRequest;
use App\Models\RefKodeWilayah;
use Illuminate\Http\Request;

class RefKodeWilayahController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $paging = request()->paging;

            $query = RefKodeWilayah::when(request()->q, function ($query) {
                    $query = $query->where('kemendagri_kota_nama', 'ilike', '%' . request()->q . '%')->orWhere('kemendagri_kota_kode', 'ilike', '%' . request()->q . '%');
                })
                ->orderby('id', 'desc');
            // ->simplepaginate(10);

            if ($paging == 'false') {
                $refWilayah = $query->get();
            } else {
                $refWilayah = $query->simplepaginate(10);
            }

            return response()->success($refWilayah, 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(RefKodeWilayahStoreRequest $request)
    {
        try {
            RefKodeWilayah::create($request->validated());

            return response()->success(null, 'Data Tersimpan');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Tersimpan');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(RefKodeWilayah $refKodeWilayah)
    {
        try {
            return response()->success($refKodeWilayah, 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(RefKodeWilayah $refKodeWilayah)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(RefKodeWilayahUpdateRequest $request, RefKodeWilayah $refKodeWilayah)
    {
        try {
            $refKodeWilayah->update($request->validated());

            return response()->success(null, 'Perubahan Tersimpan');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Perubahan Gagal Tersimpan');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(RefKodeWilayah $refKodeWilayah)
    {
        try {
            $refKodeWilayah->delete();

            return response()->success(null, 'Data Terhapus');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal menghapus data');
        }
    }
}
