<?php

namespace App\Http\Controllers;

use App\Http\Requests\MstDashboardStoreRequest;
use App\Http\Requests\MstDashboardUpdateRequest;
use App\Models\MstDashboard;
use Illuminate\Http\Request;

class MstDashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $paging = request()->paging;

            $query = MstDashboard::when(request()->q, function ($query) {
                $query = $query->where('nama_dashboard', 'ilike', '%' . request()->q . '%');
            })->when(request()->active && request()->active == 'active', function ($query) {
                $query = $query->where('is_hidden', false);
            })
                ->orderby('id', 'desc');
            // ->paginate(10);

            if ($paging == 'false') {
                $mstDashboard = $query->get();
            } else {
                $mstDashboard = $query->simplepaginate(10);
            }

            return response()->success($mstDashboard, 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(MstDashboardStoreRequest $request)
    {
        try {
            MstDashboard::create($request->validated());

            return response()->success(null, 'Data Tersimpan');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Tersimpan');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(MstDashboard $mstDashboard)
    {
        try {
            return response()->success($mstDashboard, 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(MstDashboard $mstDashboard)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(MstDashboardUpdateRequest $request, MstDashboard $mstDashboard)
    {
        try {
            $mstDashboard->update($request->validated());

            return response()->success(null, 'Perubahan Tersimpan');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Perubahan Gagal Tersimpan');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(MstDashboard $mstDashboard)
    {
        try {
            $mstDashboard->delete();

            return response()->success(null, 'Data Terhapus');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal menghapus data');
        }
    }
}
