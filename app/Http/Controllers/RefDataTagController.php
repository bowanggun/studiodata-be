<?php

namespace App\Http\Controllers;

use App\Http\Requests\RefDataTagStoreRequest;
use App\Http\Requests\RefDataTagUpdateRequest;
use App\Models\RefDataTag;
use Illuminate\Http\Request;

class RefDataTagController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $paging = request()->paging;

            $query = RefDataTag::with('trxDataTagLatest:id,ref_data_tag_id')
                ->when(request()->q, function ($query) {
                    $query = $query->where('data_tag', 'ilike', '%' . request()->q . '%');
                })
                ->orderby('id', 'desc');
            // ->get();
            // ->paginate(10);

            if ($paging == 'false') {
                $refDataTag = $query->get();
            } else {
                $refDataTag = $query->simplepaginate(10);
            }

            return response()->success($refDataTag, 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(RefDataTagStoreRequest $request)
    {
        try {
            RefDataTag::create($request->validated());

            return response()->success(null, 'Data Tersimpan');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Tersimpan');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(RefDataTag $dataTag)
    {
        try {
            return response()->success($dataTag, 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(RefDataTag $refDataTag)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(RefDataTagUpdateRequest $request, RefDataTag $dataTag)
    {
        try {
            $dataTag->update($request->validated());

            return response()->success(null, 'Perubahan Tersimpan');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Perubahan Gagal Tersimpan');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(RefDataTag $dataTag)
    {
        try {
            $dataTag->delete();

            return response()->success(null, 'Data Terhapus');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal menghapus data');
        }
    }
}
