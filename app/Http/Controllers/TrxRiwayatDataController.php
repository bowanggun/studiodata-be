<?php

namespace App\Http\Controllers;

use App\Http\Requests\TrxRiwayatDataStoreRequest;
use App\Http\Requests\TrxRiwayatDataUpdateRequest;
use App\Models\TrxRiwayatData;
use Illuminate\Http\Request;

class TrxRiwayatDataController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $trxData = TrxRiwayatData::
                // with([
                //     'mstJenisDok:id,ref_grup_dok_id,jenis_dokumen',
                //     'mstJenisDok.refGrupDok:id,grup_dokumen',
                //     'trxDataTopik',
                //     'trxDataOrganisasi',
                //     'trxDataTag',
                // ])->
                when(request()->q, function ($trxData) {
                    $trxData = $trxData->where('grup_dokumen', 'ilike', '%' . request()->q . '%');
                })
                ->orderby('id', 'desc')
                ->simplepaginate(10);

            return response()->success($trxData, 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(TrxRiwayatDataStoreRequest $request)
    {
        try {
            TrxRiwayatData::create($request->validated());

            return response()->success(null, 'Data Tersimpan');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Tersimpan');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(TrxRiwayatData $trxRiwayatDatum)
    {
        try {
            // $trxDatum = $trxDatum->load([
            //     'mstJenisDok:id,ref_grup_dok_id,jenis_dokumen',
            //     'mstJenisDok.refGrupDok:id,grup_dokumen',
            //     'trxDataTopik',
            //     'trxDataOrganisasi',
            //     'trxDataTag',
            // ]);

            return response()->success($trxRiwayatDatum, 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(TrxRiwayatData $trxRiwayatData)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(TrxRiwayatDataUpdateRequest $request, TrxRiwayatData $trxRiwayatDatum)
    {
        try {
            $trxRiwayatDatum->update($request->validated());

            return response()->success(null, 'Perubahan Tersimpan');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Perubahan Gagal Tersimpan');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(TrxRiwayatData $trxRiwayatDatum)
    {
        try {
            $trxRiwayatDatum->delete();

            return response()->success(null, 'Data Terhapus');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal menghapus data');
        }
    }
}
