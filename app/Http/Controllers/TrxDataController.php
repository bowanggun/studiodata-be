<?php

namespace App\Http\Controllers;

use App\Http\Requests\TrxDataCariDataRequest;
use App\Http\Requests\TrxDataStoreRequest;
use App\Http\Requests\TrxDataUbahInteroperabilitasRequest;
use App\Http\Requests\TrxDataUpdateRequest;
use App\Models\TrxData;
use App\Models\TrxRiwayatData;
use App\Services\TrxDataService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TrxDataController extends Controller
{
    /**
     * Display a listing of the resource.
     */

    public function __construct(private TrxDataService $trxDataService)
    {
    }

    public function index()
    {
        try {
            $trxData = TrxData::with([
                'mstJenisDok:id,ref_grup_dok_id,jenis_dokumen',
                'mstJenisDok.refGrupDok:id,grup_dokumen',
                'trxDataTopik',
                'trxDataOrganisasi',
                'trxDataTag',
                'trxRiwayatData',
            ])
                ->when(request()->q, function ($trxData) {
                    $trxData = $trxData->where('nama_data', 'ilike', '%' . request()->q . '%')->OrWhere('sumber_data', 'ilike', '%' . request()->q . '%');
                })
                ->orderby('id', 'desc')
                ->simplePaginate(10);

            return response()->success($trxData, 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(TrxDataStoreRequest $request)
    {
        try {
            DB::beginTransaction();

            $trxData = $this->trxDataService->store($request->validated());

            $this->trxDataService->storeRiwayatData($trxData->id);
            $this->trxDataService->storeDataTag($trxData->id);
            $this->trxDataService->storeDataTopik($trxData->id);
            $this->trxDataService->storeDataOrganisasi($trxData->id);

            DB::commit();

            return response()->success(null, 'Data Tersimpan');
        } catch (\Exception $e) {
            DB::rollback();

            return response()->error($e->getMessage(), 422, 'Gagal Tersimpan');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(TrxData $trxDatum)
    {
        try {
            $trxDatum = $trxDatum->load([
                'mstJenisDok:id,ref_grup_dok_id,jenis_dokumen',
                'mstJenisDok.refGrupDok:id,grup_dokumen',
                'trxDataTopik',
                'trxDataOrganisasi',
                'trxDataTag',
                'trxRiwayatData',
            ]);

            return response()->success($trxDatum, 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(TrxData $trxData)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(TrxDataUpdateRequest $request, TrxData $trxDatum)
    {
        try {
            DB::beginTransaction();

            $this->trxDataService->update($request->validated(), $trxDatum);

            $this->trxDataService->storeRiwayatData($trxDatum->id);
            $this->trxDataService->storeDataTag($trxDatum->id);
            $this->trxDataService->storeDataTopik($trxDatum->id);
            $this->trxDataService->storeDataOrganisasi($trxDatum->id);

            DB::commit();

            return response()->success(null, 'Perubahan Tersimpan');
        } catch (\Exception $e) {
            DB::rollback();
            return response()->error($e->getMessage(), 422, 'Perubahan Gagal Tersimpan');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(TrxData $trxDatum)
    {
        try {
            $trxDatum->delete();

            return response()->success(null, 'Data Terhapus');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal menghapus data');
        }
    }

    public function ubahInteroperabilitas(TrxDataUbahInteroperabilitasRequest $request, TrxData $trxDatum)
    {
        try {
            $trxDatum->update($request->validated());

            return response()->success(null, 'Perubahan Tersimpan');
        } catch (\Exception $e) {
            DB::rollback();
            return response()->error($e->getMessage(), 422, 'Perubahan Gagal Tersimpan');
        }
    }

    public function pencarianData(TrxDataCariDataRequest $request)
    {
        try {
            // $trxData = $request->all();

            $trxData = $this->trxDataService->cariData($request->validated());

            $message = 'Data tidak ditemukan';
            if (sizeof($trxData) < 1) {
                $message = 'Data tidak ditemukan';
            } else {
                $message = 'Data ditemukan';
            }

            // if ($request->all()) {
            //     $trxData = 'kosong';
            // }

            return response()->success($trxData, $message);
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Pencarian Gagal');
        }
    }
}
