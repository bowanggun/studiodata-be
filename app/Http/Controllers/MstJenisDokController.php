<?php

namespace App\Http\Controllers;

use App\Http\Requests\MstJenisDokStoreRequest;
use App\Http\Requests\MstJenisDokUpdateRequest;
use App\Models\MstJenisDok;
use Illuminate\Http\Request;

class MstJenisDokController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $q = request()->q;
            $idGrupDok = request()->idGrupDok;

            $query = MstJenisDok::with([
                'refGrupDok:id,grup_dokumen',
                'trxDataLatest:id,mst_jenis_dok_id',
            ])
                ->when($idGrupDok, function ($query) use ($idGrupDok) {
                    $query = $query
                        ->where('ref_grup_dok_id', $idGrupDok);
                })
                ->when($q, function ($query) use ($q) {
                    $query = $query
                        ->where('jenis_dokumen', 'ilike', '%' . $q . '%')
                        ->orWhereHas('refGrupDok', function ($query) use ($q) {
                            $query->where('grup_dokumen', 'ilike', '%' . $q . '%');
                        });
                })
                ->orderby('id', 'desc');

            if (!empty($idGrupDok)) {
                $mstJenisDok = $query->paginate(10);
            } else {
                $mstJenisDok = $query->get();
            }
            // ->paginate(10);

            return response()->success($mstJenisDok, 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(MstJenisDokStoreRequest $request)
    {
        try {
            MstJenisDok::create($request->validated());

            return response()->success(null, 'Data Tersimpan');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Tersimpan');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(MstJenisDok $jenisDok)
    {
        try {
            $jenisDok = $jenisDok->load(['refGrupDok:id,grup_dokumen']);

            return response()->success($jenisDok, 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(MstJenisDok $mstJenisDok)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(MstJenisDokUpdateRequest $request, MstJenisDok $jenisDok)
    {
        try {
            $jenisDok->update($request->validated());

            return response()->success(null, 'Perubahan Tersimpan');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Perubahan Gagal Tersimpan');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(MstJenisDok $jenisDok)
    {
        try {
            $jenisDok->delete();

            return response()->success(null, 'Data Terhapus');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal menghapus data');
        }
    }
}
