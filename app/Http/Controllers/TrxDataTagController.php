<?php

namespace App\Http\Controllers;

use App\Http\Requests\TrxDataTagStoreRequest;
use App\Http\Requests\TrxDataTagUpdateRequest;
use App\Models\TrxDataTag;
use Illuminate\Http\Request;

class TrxDataTagController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $trxDataTag = TrxDataTag::when(request()->q, function ($trxDataTag) {
                $trxDataTag = $trxDataTag->where('grup_dokumen', 'ilike', '%' . request()->q . '%');
            })
                ->orderby('id', 'desc')
                ->simplepaginate(10);

            return response()->success($trxDataTag, 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(TrxDataTagStoreRequest $request)
    {
        try {
            TrxDataTag::create($request->validated());

            return response()->success(null, 'Data Tersimpan');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Tersimpan');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(TrxDataTag $trxDataTag)
    {
        try {
            return response()->success($trxDataTag, 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(TrxDataTag $trxDataTag)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(TrxDataTagUpdateRequest $request, TrxDataTag $trxDataTag)
    {
        try {
            $trxDataTag->update($request->validated());

            return response()->success(null, 'Perubahan Tersimpan');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Perubahan Gagal Tersimpan');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(TrxDataTag $trxDataTag)
    {
        try {
            $trxDataTag->delete();

            return response()->success(null, 'Data Terhapus');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal menghapus data');
        }
    }
}
