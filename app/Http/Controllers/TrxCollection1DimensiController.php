<?php

namespace App\Http\Controllers;

use App\Http\Requests\TrxCollection1DimensiStoreRequest;
use App\Http\Requests\TrxCollection1DimensiUpdateRequest;
use App\Models\TrxCollection1Dimensi;
use Illuminate\Http\Request;

class TrxCollection1DimensiController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $q = request()->q;
            $idMstCollection = request()->idMstCollection;

            $query = TrxCollection1Dimensi::when($idMstCollection, function ($query) use ($idMstCollection) {
                $query = $query
                    ->where('mst_collection_id', $idMstCollection);
            })
                ->when($q, function ($query) use ($q) {
                    $query = $query
                        ->where('tahun', 'ilike', '%' . $q . '%')
                        ->orWhere('kategori', 'ilike', '%' . $q . '%')
                        ->orWhere('target', 'ilike', '%' . $q . '%')
                        ->orWhere('realisasi', 'ilike', '%' . $q . '%');
                })
                ->orderby('tahun', 'desc');

            if (!empty($idMstCollection)) {
                $trxCol = $query->paginate(10);
            } else {
                $trxCol = $query->get();
            }

            return response()->success($trxCol, 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(TrxCollection1DimensiStoreRequest $request)
    {
        try {
            TrxCollection1Dimensi::create($request->validated());

            return response()->success(null, 'Data Tersimpan');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Tersimpan');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(TrxCollection1Dimensi $trx_collection_1_dim)
    {
        try {
            return response()->success($trx_collection_1_dim, 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(TrxCollection1Dimensi $trxCollection1Dimensi)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(TrxCollection1DimensiUpdateRequest $request, TrxCollection1Dimensi $trx_collection_1_dim)
    {
        try {
            $trx_collection_1_dim->update($request->validated());

            return response()->success(null, 'Perubahan Tersimpan');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Perubahan Gagal Tersimpan');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(TrxCollection1Dimensi $trx_collection_1_dim)
    {
        try {
            $trx_collection_1_dim->delete();

            return response()->success(null, 'Data Terhapus');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal menghapus data');
        }
    }
}
