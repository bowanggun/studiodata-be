<?php

namespace App\Http\Controllers;

use App\Exports\TrxLuasWilayahExport;
use App\Http\Requests\TrxLuasWilayahKecamatanKelurahanDownloadRequest;
use App\Http\Requests\TrxLuasWilayahKecamatanKelurahanStoreRequest;
use App\Http\Requests\TrxLuasWilayahKecamatanKelurahanUpdateRequest;
use App\Models\TrxLuasWilayahKecamatanKelurahan;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Validator;

class TrxLuasWilayahKecamatanKelurahanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $q = request()->q;
            $idMstCollection = request()->idMstCollection;

            $query = TrxLuasWilayahKecamatanKelurahan::when($idMstCollection, function ($query) use ($idMstCollection) {
                $query = $query
                    ->where('mst_collection_id', $idMstCollection);
            })
                ->when($q, function ($query) use ($q) {
                    $query = $query
                        ->where('kecamatan', 'ilike', '%' . $q . '%')
                        ->orWhere('tahun', 'ilike', '%' . $q . '%');
                })
                ->orderby('tahun', 'desc');

            if (!empty($idMstCollection)) {
                $trxLuas = $query->paginate(10);
            } else {
                $trxLuas = $query->get();
            }

            return response()->success($trxLuas, 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(TrxLuasWilayahKecamatanKelurahanStoreRequest $request)
    {
        try {
            TrxLuasWilayahKecamatanKelurahan::create($request->validated());

            return response()->success(null, 'Data Tersimpan');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Tersimpan');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(TrxLuasWilayahKecamatanKelurahan $luasWilayahKecamatanKelurahan)
    {
        try {
            return response()->success($luasWilayahKecamatanKelurahan, 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(TrxLuasWilayahKecamatanKelurahan $trxLuasWilayahKecamatanKelurahan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(TrxLuasWilayahKecamatanKelurahanUpdateRequest $request, TrxLuasWilayahKecamatanKelurahan $luasWilayahKecamatanKelurahan)
    {
        try {
            $luasWilayahKecamatanKelurahan->update($request->validated());

            return response()->success(null, 'Perubahan Tersimpan');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Perubahan Gagal Tersimpan');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(TrxLuasWilayahKecamatanKelurahan $luasWilayahKecamatanKelurahan)
    {
        try {
            $luasWilayahKecamatanKelurahan->delete();

            return response()->success(null, 'Data Terhapus');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal menghapus data');
        }
    }

    public function exportLuasWilayah(TrxLuasWilayahKecamatanKelurahanDownloadRequest $request)
    {
        // $validator = Validator::make($request->all(), [
        //     'tahun' => 'required',
        // ]);

        // if ($validator->fails()) {
        //     return response()->json([
        //         'message' => $validator->errors(),
        //         // 'message' => 'Gagal Mengupload',
        //     ], 422);
        // }

        try {
            $tahun = $request->input('tahun');

            return Excel::download(new TrxLuasWilayahExport($tahun), 'luas-wilayah.xlsx');
            // return (new ApiPegawaiExport)->download('invoices.xlsx', \Maatwebsite\Excel\Excel::XLSX);
        } catch (\Exception $e) {
            // return response()->json([
            //     'message' => 'Gagal Export Data Excel',
            //     'detail' => $e->getMessage(),
            // ]);
            return response()->error($e->getMessage(), 422, 'Download Gagal');
        }
    }
}
