<?php

namespace App\Http\Controllers;

use App\Http\Requests\TrxJmlPendudukBdsJkStoreRequest;
use App\Http\Requests\TrxJmlPendudukBdsJkUpdateRequest;
use App\Models\TrxJmlPendudukBdsJk;
use Illuminate\Http\Request;

class TrxJmlPendudukBdsJkController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $q = request()->q;
            $idMstCollection = request()->idMstCollection;

            $query = TrxJmlPendudukBdsJk::when($idMstCollection, function ($query) use ($idMstCollection) {
                $query = $query
                    ->where('mst_collection_id', $idMstCollection);
            })
                ->when($q, function ($query) use ($q) {
                    $query = $query
                        ->where('tahun', 'ilike', '%' . $q . '%')
                        ->orWhere('kategori', 'ilike', '%' . $q . '%')
                        ->orWhere('target', 'ilike', '%' . $q . '%')
                        ->orWhere('realisasi', 'ilike', '%' . $q . '%');
                })
                ->orderby('tahun', 'desc');

            if (!empty($idMstCollection)) {
                $trxCol = $query->paginate(10);
            } else {
                $trxCol = $query->get();
            }

            return response()->success($trxCol, 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(TrxJmlPendudukBdsJkStoreRequest $request)
    {
        try {
            TrxJmlPendudukBdsJk::create($request->validated());

            return response()->success(null, 'Data Tersimpan');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Tersimpan');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(TrxJmlPendudukBdsJk $jumlahPendudukBdsJk)
    {
        try {
            return response()->success($jumlahPendudukBdsJk, 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(TrxJmlPendudukBdsJk $jumlahPendudukBdsJk)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(TrxJmlPendudukBdsJkUpdateRequest $request, TrxJmlPendudukBdsJk $jumlahPendudukBdsJk)
    {
        try {
            $jumlahPendudukBdsJk->update($request->validated());

            return response()->success(null, 'Perubahan Tersimpan');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Perubahan Gagal Tersimpan');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(TrxJmlPendudukBdsJk $jumlahPendudukBdsJk)
    {
        try {
            $jumlahPendudukBdsJk->delete();

            return response()->success(null, 'Data Terhapus');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal menghapus data');
        }
    }
}
