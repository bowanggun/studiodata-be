<?php

namespace App\Http\Controllers;

use App\Http\Requests\TrxDataTopikStoreRequest;
use App\Http\Requests\TrxDataUpdateRequest;
use App\Models\TrxDataTopik;
use Illuminate\Http\Request;

class TrxDataTopikController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $trxDataTopik = TrxDataTopik::when(request()->q, function ($trxDataTopik) {
                $trxDataTopik = $trxDataTopik->where('grup_dokumen', 'ilike', '%' . request()->q . '%');
            })
                ->orderby('id', 'desc')
                ->simplepaginate(10);

            return response()->success($trxDataTopik, 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(TrxDataTopikStoreRequest $request)
    {
        try {
            TrxDataTopik::create($request->validated());

            return response()->success(null, 'Data Tersimpan');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Tersimpan');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(TrxDataTopik $trxDataTopik)
    {
        try {
            return response()->success($trxDataTopik, 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(TrxDataTopik $trxDataTopik)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(TrxDataUpdateRequest $request, TrxDataTopik $trxDataTopik)
    {
        try {
            $trxDataTopik->update($request->validated());

            return response()->success(null, 'Perubahan Tersimpan');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Perubahan Gagal Tersimpan');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(TrxDataTopik $trxDataTopik)
    {
        try {
            $trxDataTopik->delete();

            return response()->success(null, 'Data Terhapus');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal menghapus data');
        }
    }
}
