<?php

namespace App\Http\Controllers;

use App\Http\Requests\TrxPdrbAdhk2010StoreRequest;
use App\Http\Requests\TrxPdrbAdhk2010UpdateRequest;
use App\Models\TrxPdrbAdhk2010LapUsaha;
use Illuminate\Http\Request;

class TrxPdrbAdhk2010LapUsahaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $q = request()->q;
            $idMstCollection = request()->idMstCollection;

            $query = TrxPdrbAdhk2010LapUsaha::when($idMstCollection, function ($query) use ($idMstCollection) {
                $query = $query
                    ->where('mst_collection_id', $idMstCollection);
            })
                ->when($q, function ($query) use ($q) {
                    $query = $query
                        ->where('tahun', 'ilike', '%' . $q . '%')
                        ->orWhere('kategori', 'ilike', '%' . $q . '%')
                        ->orWhere('target', 'ilike', '%' . $q . '%')
                        ->orWhere('realisasi', 'ilike', '%' . $q . '%');
                })
                ->orderby('tahun', 'desc');

            if (!empty($idMstCollection)) {
                $trxCol = $query->paginate(10);
            } else {
                $trxCol = $query->get();
            }

            return response()->success($trxCol, 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(TrxPdrbAdhk2010StoreRequest $request)
    {
        try {
            TrxPdrbAdhk2010LapUsaha::create($request->validated());

            return response()->success(null, 'Data Tersimpan');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Tersimpan');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(TrxPdrbAdhk2010LapUsaha $pdrb_adhk_2010_lap_usaha)
    {
        try {
            return response()->success($pdrb_adhk_2010_lap_usaha, 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(TrxPdrbAdhk2010LapUsaha $trxPdrbAdhk2010LapUsaha)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(TrxPdrbAdhk2010UpdateRequest $request, TrxPdrbAdhk2010LapUsaha $pdrb_adhk_2010_lap_usaha)
    {
        try {
            $pdrb_adhk_2010_lap_usaha->update($request->validated());

            return response()->success(null, 'Perubahan Tersimpan');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Perubahan Gagal Tersimpan');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(TrxPdrbAdhk2010LapUsaha $pdrb_adhk_2010_lap_usaha)
    {
        try {
            $pdrb_adhk_2010_lap_usaha->delete();

            return response()->success(null, 'Data Terhapus');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal menghapus data');
        }
    }
}
