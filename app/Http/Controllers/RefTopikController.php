<?php

namespace App\Http\Controllers;

use App\Http\Requests\RefTopikStoreRequest;
use App\Http\Requests\RefTopikUpdateRequest;
use App\Models\RefTopik;
use Illuminate\Http\Request;

class RefTopikController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $paging = request()->paging;

            $query = RefTopik::with('trxDataTopikLatest:id,ref_topik_id')
                ->when(request()->q, function ($query) {
                    $query = $query->where('topik', 'ilike', '%' . request()->q . '%');
                })
                ->orderby('id', 'desc');

            if ($paging == 'false') {
                $refTopik = $query->get();
            } else {
                $refTopik = $query->simplepaginate(10);
            }

            return response()->success($refTopik, 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(RefTopikStoreRequest $request)
    {
        try {
            RefTopik::create($request->validated());

            return response()->success(null, 'Data Tersimpan');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Tersimpan');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(RefTopik $topik)
    {
        try {
            return response()->success($topik, 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(RefTopik $refTopik)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(RefTopikUpdateRequest $request, RefTopik $topik)
    {
        try {
            $topik->update($request->validated());

            return response()->success(null, 'Perubahan Tersimpan');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Perubahan Gagal Tersimpan');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(RefTopik $topik)
    {
        try {
            $topik->delete();

            return response()->success(null, 'Data Terhapus');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal menghapus data');
        }
    }
}
