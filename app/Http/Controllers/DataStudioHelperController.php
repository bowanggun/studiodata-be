<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\DataStudio;

class DataStudioHelperController extends Controller
{
    public function getTipeCollection()
    {
        try {
            return response()->success(DataStudio::tipeCollection(), 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    public function getDefaultChart()
    {
        try {
            return response()->success(DataStudio::defaultChart(), 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    public function getKategoriPdrb()
    {
        try {
            return response()->success(DataStudio::kategoriPdrb(), 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    public function getKategoriMuseum()
    {
        try {
            return response()->success(DataStudio::kategoriMuseum(), 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    public function getListKecamatan()
    {
        try {
            return response()->success(DataStudio::listKecamatan(), 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    public function getJenisPengeluaranPdrbAdhk()
    {
        try {
            return response()->success(DataStudio::jenisPengeluaranPdrbAdhk(), 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    public function getTingkatPendidikan()
    {
        try {
            return response()->success(DataStudio::tingkatPendidikan(), 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    public function getAllKategori()
    {
        try {
            return response()->success(DataStudio::allKategori(), 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }
}
