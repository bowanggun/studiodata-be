<?php

namespace App\Http\Controllers;

use App\Http\Requests\MstCollectionStoreRequest;
use App\Http\Requests\MstCollectionUpdateRequest;
use App\Models\MstCollection;
use Illuminate\Http\Request;

class MstCollectionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $paging = request()->paging;

            $query = MstCollection::with('trxCollectionLatest:id,mst_collection_id','trxCollection1Latest:id,mst_collection_id')->when(request()->q, function ($query) {
                $query = $query->where('nama_collection', 'ilike', '%' . request()->q . '%')
                ->orWhere('satuan', 'ilike', '%' . request()->q . '%')
                ->orWhere('organisasi', 'ilike', '%' . request()->q . '%');
            })->when(request()->active && request()->active == 'active', function ($query) {
                $query = $query->where('is_hidden', false);
            })
                ->orderby('id', 'desc');
            // ->paginate(10);

            if ($paging == 'false') {
                $mstDashboard = $query->get();
            } else {
                $mstDashboard = $query->simplepaginate(10);
            }

            return response()->success($mstDashboard, 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(MstCollectionStoreRequest $request)
    {
        try {
            MstCollection::create($request->validated());

            return response()->success(null, 'Data Tersimpan');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Tersimpan');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(MstCollection $mstCollection)
    {
        try {
            return response()->success($mstCollection, 'Berhasil menampilkan data');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal Mengambil Data');
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(MstCollection $mstCollection)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(MstCollectionUpdateRequest $request, MstCollection $mstCollection)
    {
        try {
            $mstCollection->update($request->validated());

            return response()->success(null, 'Perubahan Tersimpan');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Perubahan Gagal Tersimpan');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(MstCollection $mstCollection)
    {
        try {
            $mstCollection->delete();

            return response()->success(null, 'Data Terhapus');
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), 422, 'Gagal menghapus data');
        }
    }
}
