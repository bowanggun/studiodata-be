<?php

namespace App\Exports;

use App\Models\TrxLuasWilayahKecamatanKelurahan;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Illuminate\Support\Facades\DB;

class TrxLuasWilayahExport

extends \PhpOffice\PhpSpreadsheet\Cell\StringValueBinder implements WithCustomValueBinder, FromCollection,  WithHeadings, WithMapping
{
    /**
     * @return \Illuminate\Support\Collection
     */

    use Exportable;

    private $tahun;

    public function __construct($tahun)
    {
        $this->tahun = $tahun;
    }

    public function collection()
    {
        // return TrxLuasWilayahKecamatanKelurahan::all();

        $query = TrxLuasWilayahKecamatanKelurahan::select(
            'tahun',
            'kecamatan',
            'jml_kelurahan',
            'luas_wilayah',
            'satuan',
            'catatan',
        );

        if ($this->tahun)
            $query->where('tahun', $this->tahun);

        return collect($query->get());
        return $query->get();
    }

    public function headings(): array
    {
        return [
            'Tahun',
            'Kecamatan',
            'Jumlah Kelurahan',
            'Luas Wilayah',
            'Satuan',
            'Catatan',
        ];
    }

    public function map($data): array
    {
        return [
            $data->tahun,
            $data->kecamatan,
            $data->jml_kelurahan,
            $data->luas_wilayah,
            $data->satuan,
            $data->catatan,
        ];
    }
}
