<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('mst_jenis_dok', function (Blueprint $table) {
            $table->id();

            $table->string('jenis_dokumen', 200);
            $table->unsignedInteger('ref_grup_dok_id')->nullable();
            $table->foreign('ref_grup_dok_id')->references('id')->on('ref_grup_dok');

            $table->text('keterangan')->nullable();

            $table->string('created_by', 50);
            $table->string('updated_by', 50);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('mst_jenis_dokumen');
    }
};
