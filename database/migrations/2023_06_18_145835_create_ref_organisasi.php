<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('ref_organisasi', function (Blueprint $table) {
            $table->id();

            $table->string('organisasi', 200);
            $table->text('singkatan')->nullable();
            $table->text('keterangan')->nullable();

            $table->string('created_by', 50);
            $table->string('updated_by', 50);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('ref_organisasi');
    }
};
