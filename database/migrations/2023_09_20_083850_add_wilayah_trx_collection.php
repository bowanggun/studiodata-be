<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('trx_collection', function (Blueprint $table) {
            $table->string('kemendagri_kota_kode', 50)->default('32.71');
            $table->text('kemendagri_kota_nama')->default('KOTA BOGOR');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
