<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('trx_data_topik', function (Blueprint $table) {
            $table->id();

            $table->unsignedInteger('trx_data_id')->nullable();
            $table->foreign('trx_data_id')->references('id')->on('trx_data')->onDelete('cascade')->onUpdate('cascade');

            $table->unsignedInteger('ref_topik_id')->nullable();
            $table->foreign('ref_topik_id')->references('id')->on('ref_topik');
            $table->text('topik');

            $table->string('created_by', 50);
            $table->string('updated_by', 50);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('trx_data_topik');
    }
};
