<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('mst_dashboard', function (Blueprint $table) {
            $table->id();

            $table->text('nama_dashboard');
            $table->text('frame');
            $table->text('link');
            $table->text('catatan')->nullable();
            $table->boolean('is_hidden')->default(false);
            $table->string('created_by', 50);
            $table->string('updated_by', 50);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('mst_dashboard');
    }
};
