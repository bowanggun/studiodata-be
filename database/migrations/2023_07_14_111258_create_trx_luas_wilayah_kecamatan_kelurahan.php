<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('trx_luas_wilayah_kecamatan_kelurahan', function (Blueprint $table) {
            $table->id();

            $table->unsignedInteger('mst_collection_id');
            $table->foreign('mst_collection_id')->references('id')->on('mst_collection')->onDelete('cascade')->onUpdate('cascade');

            $table->year('tahun');
            $table->string('kecamatan', 50);
            $table->integer('jml_kelurahan');
            $table->float('luas_wilayah');
            $table->string('satuan', 50)->nullable();
            $table->text('catatan')->nullable();

            $table->string('created_by', 50);
            $table->string('updated_by', 50);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('trx_luas_wilayah_kecamatan_kelurahan');
    }
};
