<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('mst_collection', function (Blueprint $table) {
            $table->id();

            $table->text('nama_collection');
            $table->text('judul')->nullable();
            $table->string('tipe', 50)->nullable();
            $table->string('default_chart', 50)->nullable();
            $table->text('referensi_data')->nullable();
            $table->text('catatan')->nullable();
            $table->text('route_name')->nullable();
            $table->text('table_name')->nullable();

            $table->string('created_by', 50);
            $table->string('updated_by', 50);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('mst_collection');
    }
};
