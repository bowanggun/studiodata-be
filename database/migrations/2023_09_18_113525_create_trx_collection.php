<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('trx_collection', function (Blueprint $table) {
            $table->id();

            $table->unsignedInteger('mst_collection_id')->nullable();
            $table->foreign('mst_collection_id')->references('id')->on('mst_collection');

            $table->year('tahun');
            $table->text('target')->nullable();
            $table->text('realisasi')->nullable();
            $table->text('sumber')->nullable();
            $table->text('catatan')->nullable();

            $table->string('created_by', 50);
            $table->string('updated_by', 50);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('trx_collection');
    }
};
