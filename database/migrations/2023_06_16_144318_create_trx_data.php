<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('trx_data', function (Blueprint $table) {
            $table->id();

            $table->unsignedInteger('mst_jenis_dok_id')->nullable();
            $table->foreign('mst_jenis_dok_id')->references('id')->on('mst_jenis_dok');
            $table->text('sumber_data');

            $table->string('jenis_data', 100); // url/dokumen
            $table->text('nama_data');
            $table->text('deskripsi_data'); //penjelasan isi substansi data

            $table->string('format_data', 100); // RAR,excel
            $table->string('media_type', 100)->nullable(); // application/rar
            $table->string('size_data', 100)->nullable(); // 4,408,323
            $table->string('ekstensi_data', 100)->nullable(); // 4,408,323
            $table->text('nama_file')->nullable(); // data.pdf
            $table->text('url_file_upload')->nullable(); // minio/herb001/data.pdf
            $table->text('url_upload')->nullable(); // minio/herb001/data.pdf

            $table->boolean('is_shared')->default(false);
            $table->boolean('webbappeda_visible')->default(false);
            $table->boolean('satudata_visible')->default(false);

            $table->text('catatan')->nullable();
            $table->string('created_by', 50);
            $table->string('updated_by', 50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('trx_data');
    }
};
