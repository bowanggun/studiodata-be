<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('mst_collection', function (Blueprint $table) {
            $table->text('definisi')->nullable();
            $table->text('rumus_perhitungan')->nullable();
            $table->text('cara_memperoleh_data')->nullable();
            $table->text('urusan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('mst_collection', function (Blueprint $table) {
            $table->dropColumn('definisi');
            $table->dropColumn('rumus_perhitungan');
            $table->dropColumn('cara_memperoleh_data');
            $table->dropColumn('urusan');
        });
    }
};
