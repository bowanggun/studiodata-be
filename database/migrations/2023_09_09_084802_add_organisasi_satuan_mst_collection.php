<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('mst_collection', function (Blueprint $table) {
            $table->unsignedInteger('ref_organisasi_id')->nullable();
            $table->foreign('ref_organisasi_id')->references('id')->on('ref_organisasi');
            $table->text('organisasi')->nullable();
            $table->string('satuan', 100)->nullable();
        });
            
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('mst_collection', function (Blueprint $table) {
            $table->dropColumn('ref_organisasi_id');
            $table->dropColumn('organisasi');
            $table->dropColumn('satuan');
        });
            
    }
};
