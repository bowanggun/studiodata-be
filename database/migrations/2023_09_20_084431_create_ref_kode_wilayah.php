<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('ref_kode_wilayah', function (Blueprint $table) {
            $table->id();

            $table->string('kemendagri_provinsi_kode', 20)->nullable();
            $table->string('kemendagri_kota_kode', 50)->nullable();
            $table->string('kemendagri_provinsi_nama', 50)->nullable();
            $table->text('kemendagri_kota_nama');
            $table->string('bps_provinsi_kode', 50)->nullable();
            $table->string('bps_kota_kode', 50)->nullable();
            $table->string('bps_provinsi_nama', 50)->nullable();
            $table->text('bps_kota_nama');
            $table->text('latitude')->nullable();
            $table->text('longitude')->nullable();
            $table->string('kode_pos', 200)->nullable();

            $table->string('created_by', 50);
            $table->string('updated_by', 50);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('ref_kode_wilayah');
    }
};
