<?php

use App\Http\Controllers\DataStudioHelperController;
use App\Http\Controllers\MstCollectionController;
use App\Http\Controllers\MstDashboardController;
use App\Http\Controllers\MstJenisDokController;
use App\Http\Controllers\RefDataTagController;
use App\Http\Controllers\RefGrupDokController;
use App\Http\Controllers\RefGrupDokumenController;
use App\Http\Controllers\RefKodeWilayahController;
use App\Http\Controllers\RefOrganisasiController;
use App\Http\Controllers\RefTopikController;
use App\Http\Controllers\TrxCollection1DimensiController;
use App\Http\Controllers\TrxCollectionController;
use App\Http\Controllers\TrxDataController;
use App\Http\Controllers\TrxDataOrganisasiController;
use App\Http\Controllers\TrxDataTagController;
use App\Http\Controllers\TrxDataTopikController;
use App\Http\Controllers\TrxJmlAngkatanKerjaTingkatPendidikanController;
use App\Http\Controllers\TrxJmlMuseumBdsPengelolaController;
use App\Http\Controllers\TrxJmlPendudukBdsJkController;
use App\Http\Controllers\TrxLajuPertumbuhanPdrbController;
use App\Http\Controllers\TrxLuasWilayahKecamatanKelurahanController;
use App\Http\Controllers\TrxPdrbAdhk2010LapUsahaController;
use App\Http\Controllers\TrxPdrbAdhkJenisPengeluaranController;
use App\Http\Controllers\TrxRiwayatDataController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::prefix('v1')->group(function () {
    Route::get('tipe-collection', [DataStudioHelperController::class, 'getTipeCollection']);
    Route::get('default-chart', [DataStudioHelperController::class, 'getDefaultChart']);
    Route::get('kategori-pdrb', [DataStudioHelperController::class, 'getKategoriPdrb']);
    Route::get('kategori-museum', [DataStudioHelperController::class, 'getKategoriMuseum']);
    Route::get('list-kecamatan', [DataStudioHelperController::class, 'getListKecamatan']);
    Route::get('jenis-pengeluaran-pdrb-adhk', [DataStudioHelperController::class, 'getJenisPengeluaranPdrbAdhk']);
    Route::get('tingkat-pendidikan', [DataStudioHelperController::class, 'getTingkatPendidikan']);
    Route::get('kategori', [DataStudioHelperController::class, 'getAllKategori']);

    Route::apiResource('grup-dok', RefGrupDokController::class);
    Route::apiResource('wilayah', RefKodeWilayahController::class);
    Route::apiResource('jenis-dok', MstJenisDokController::class);
    Route::apiResource('organisasi', RefOrganisasiController::class);
    Route::apiResource('topik', RefTopikController::class);
    Route::apiResource('data-tag', RefDataTagController::class);
    Route::apiResource('trx-data', TrxDataController::class);
    Route::put('ubah-interoperabilitas/{trxDatum}', [TrxDataController::class, 'ubahInteroperabilitas']);
    Route::post('pencarian-data', [TrxDataController::class, 'pencarianData']);
    Route::apiResource('trx-data-topik', TrxDataTopikController::class);
    Route::apiResource('trx-data-organisasi', TrxDataOrganisasiController::class);
    Route::apiResource('trx-data-tag', TrxDataTagController::class);
    Route::apiResource('trx-riwayat-data', TrxRiwayatDataController::class);
    Route::apiResource('mst-dashboard', MstDashboardController::class);
    Route::apiResource('mst-collection', MstCollectionController::class);
    Route::apiResource('luas-wilayah-kecamatan-kelurahan', TrxLuasWilayahKecamatanKelurahanController::class);
    Route::post('export-luas-wilayah-kecamatan-kelurahan', [TrxLuasWilayahKecamatanKelurahanController::class, 'exportLuasWilayah']);
    Route::apiResource('trx-collection', TrxCollectionController::class);
    Route::post('export-detail-koleksi', [TrxCollectionController::class, 'exportDetailKoleksi']);
    Route::apiResource('laju-pdrb-lapangan-usaha', TrxLajuPertumbuhanPdrbController::class);
    Route::apiResource('pdrb-adhk-2010-lap-usaha', TrxPdrbAdhk2010LapUsahaController::class);
    Route::apiResource('jumlah-museum-bds-pengelola', TrxJmlMuseumBdsPengelolaController::class);
    Route::apiResource('jumlah_penduduk-bds-jk', TrxJmlPendudukBdsJkController::class);
    Route::apiResource('pdrb-adhk-jenis-pengeluaran', TrxPdrbAdhkJenisPengeluaranController::class);
    Route::apiResource('jml-angkatan-kerja-tk-pendidikan', TrxJmlAngkatanKerjaTingkatPendidikanController::class);
    Route::apiResource('trx-collection-1-dim', TrxCollection1DimensiController::class);
});

// Route::fallback(function () {
//     return response()->error(null, 404, 'API Not Found');
// });
